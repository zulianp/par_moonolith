#ifndef INTERSECT_CL
#define INTERSECT_CL 

#include "kernel_base.cl"
#include "intersect_interface.cl"



#define INSIDE  1
#define ON_EDGE 2
#define OUTSIDE 0

static SizeType append_point(const SizeType current_n_points, const Vector2 point, Scalar *output)
{
	const SizeType n_output_elements = current_n_points*2;
	output[n_output_elements]   = point[0];
	output[n_output_elements+1] = point[1];
	return current_n_points + 1;
}

#ifdef DEBUG_KERNEL
#define print_isect_query(macro_x) 											\
{ 																			\
	const char * macro_symbols[3] = { "OUTSIDE", "INSIDE", "ON_EDGE" };		\
	printf("%s\n", macro_symbols[(macro_x)]);								\
}
#else
#define print_isect_query(x)
#endif

static bool intersect_convex_polygons(const SizeType n_vertices_1, const Scalar *polygon_1, const SizeType n_vertices_2, const Scalar *polygon_2, SizeType *n_vertices_result, Scalar *result, const Scalar tol)
{
	short is_inside    [MAX_N_ISECT_POINTS];
	Scalar input_buffer[MAX_N_ISECT_POINTS * 2];


	const SizeType n_elements_2 = n_vertices_2 * 2;

	SizeType n_input_points  = n_vertices_2;
	SizeType n_output_points = 0;


	Scalar * input  = input_buffer;
	Scalar * output = result;

	generic_copy(n_elements_2, polygon_2, input);


	Vector2 e1, e2, p, s, e;
	
	for(SizeType i = 0; i < n_vertices_1; ++i) {
		if(i > 0) {
			generic_ptr_swap(Scalar, input, output);
			n_input_points = n_output_points;
			n_output_points = 0;
		}

		const SizeType i2x 	= i * 2;
		const SizeType i2y  = i2x + 1;

		const SizeType i2p1x = 2 * (((i + 1) == n_vertices_1)? 0 : (i + 1));
		const SizeType i2p1y = i2p1x + 1;

		e1 = vec_2(polygon_1[i2x], polygon_1[i2y]);
		e2 = vec_2(polygon_1[i2p1x], polygon_1[i2p1y]);

		SizeType n_outside = 0;
		for(SizeType j = 0; j < n_input_points; ++j) {
			const SizeType jx = j * 2;
			const SizeType jy = jx + 1;

			p = vec_2(input[jx], input[jy]);
			is_inside[j] = inside_half_plane(e1, e2, p, tol);
			n_outside += is_inside[j] != INSIDE;	
		}

		if(n_input_points - n_outside == 0) return false;

		for(SizeType j = 0; j < n_input_points; ++j) {
			const SizeType jx = j * 2;
			const SizeType jy = jx + 1;

			const SizeType jp1 = (j + 1 == n_input_points)? 0 : (j + 1);
			const SizeType jp1x = jp1 * 2;
			const SizeType jp1y = jp1x + 1;

			s = vec_2(input[jx], input[jy]);
			e = vec_2(input[jp1x], input[jp1y]);

			if(is_inside[j]) {
				n_output_points = append_point(n_output_points, s, output);

				if( ( is_inside[j] != ON_EDGE ) && ( !is_inside[jp1] ) ) {
					n_output_points = append_point(n_output_points, intersect_lines(e1, e2, s, e), output);
				}
			} else if(is_inside[jp1]) {
				n_output_points = append_point(n_output_points, intersect_lines(e1, e2, s, e), output);
			}
		}

		if(n_output_points < 3) {
			return false;
		}

		n_output_points = collapse_quasi_equal_points(n_output_points, output, tol);

		if(n_output_points < 3) {
			return false;
		}
	}

	*n_vertices_result = n_output_points;

	if(output != result) {
		const SizeType n_intersection_elements = n_output_points*2;
		generic_copy(n_intersection_elements, output, result);
	}

	return n_output_points >= 3;
}


// static SizeType collapse_quasi_equal_points(const SizeType current_n_points, Scalar *points, Scalar tol)
// {
// 	bool keep[MAX_N_ISECT_POINTS];
// 	keep[0] = true;

// 	SizeType result_n_points = current_n_points;
// 	for(SizeType i = 1; i < result_n_points; ++i) {
// 		const SizeType ix = i * 2;
// 		const SizeType iy = ix + 1;

// 		const SizeType im1x = ix   - 2;
// 		const SizeType im1y = im1x + 1;

// 		if( distance( vec_2(points[ix], points[iy]), vec_2(points[im1x], points[im1y]) ) <= tol ) {
// 			result_n_points--;
// 			keep[i] = false;
// 		} else {
// 			keep[i] = true;
// 		}
// 	}

// 	const SizeType last = (current_n_points-1) * 2;

// 	if( distance( vec_2(points[0], points[1]), 
// 				  vec_2(points[last], points[last+1]) ) <= tol ) {
// 		result_n_points--;
// 		keep[current_n_points-1] = false;

// 	} else {
// 		keep[current_n_points-1] = true;
// 	}

// 	SizeType range_to_fill_begin = -1;
// 	bool must_shift = false;

// 	for(SizeType i = 1; i < current_n_points; ++i) {
// 		if(keep[i] && must_shift) {
// 			KERNEL_ASSERT(range_to_fill_begin > 0, "range_to_fill_begin > 0");
			
// 			const SizeType to = range_to_fill_begin * 2;
// 			const SizeType from = i * 2;
// 			generic_copy(2, &points[from], &points[to]);
			
// 			keep[i] = false;
			
// 			if(keep[++range_to_fill_begin]) {
// 				range_to_fill_begin = -1;
// 				must_shift = false;
// 			}
// 		} 

// 		if(!keep[i] && !must_shift) {
// 			must_shift = true;
// 			range_to_fill_begin = i;
// 		}
// 	}

// 	return result_n_points;
// }

static SizeType collapse_quasi_equal_points(const SizeType current_n_points, Scalar *points, Scalar tol)
{
	bool keep[MAX_N_ISECT_POINTS];
	keep[0] = true;

	SizeType result_n_points = current_n_points;
	for(SizeType i = 1; i < current_n_points; ++i) {
		const SizeType ix = i * 2;
		const SizeType iy = ix + 1;

		const SizeType im1x = ix   - 2;
		const SizeType im1y = im1x + 1;

		if( distance( vec_2(points[ix],   points[iy]),
				      vec_2(points[im1x], points[im1y]) ) <= tol ) {

			result_n_points--;
			keep[i] = false;
		} else {
			keep[i] = true;
		}
	}

	const SizeType last = (current_n_points - 1) * 2;

	if(keep[current_n_points - 1]) {

		if( distance( vec_2(points[0], points[1]), 
					  vec_2(points[last], points[last+1]) ) <= tol ) {
			result_n_points--;
			keep[current_n_points-1] = false;

		} else {
			keep[current_n_points-1] = true;
		}

	}

	SizeType range_to_fill_begin = -1;
	bool must_shift = false;

	for(SizeType i = 1; i < current_n_points; ++i) {
		if(keep[i] && must_shift) {
			KERNEL_ASSERT(range_to_fill_begin > 0, "range_to_fill_begin > 0");
			
			const SizeType to = range_to_fill_begin * 2;
			const SizeType from = i * 2;
			generic_copy(2, &points[from], &points[to]);
			
			keep[i] = false;
			
			if(keep[++range_to_fill_begin]) {
				range_to_fill_begin = -1;
				must_shift = false;
			}
		} 

		if(!keep[i] && !must_shift) {
			must_shift = true;
			range_to_fill_begin = i;
		}
	}

	return result_n_points;
}


static Vector2 intersect_lines(const Vector2 line_1a, const Vector2 line_1b, const Vector2 line_2a, const Vector2 line_2b)
{
	const Vector2 u = line_1b - line_1a;
	const Vector2 v = line_2b - line_2a;
	const Vector2 w = line_2b - line_1a;

	const Scalar lambda = -(-w[0]*v[1] + w[1]*v[0])/(u[0]*v[1] - u[1]*v[0]);
	return line_1a + (lambda * u);
}

static short inside_half_plane(const Vector2 e1, const Vector2 e2, const Vector2 point, const Scalar tol)
{
	const Vector2 u = e1 - e2;
	const Vector2 v = point - e2;

	const Scalar dist = (u.x * v.y) - (v.x * u.y);

	if(dist < -tol) {
		return INSIDE;
	}

	if(dist > tol) {
		return OUTSIDE;
	}

	return ON_EDGE;
}

//clean up macros
#undef INSIDE
#undef ON_EDGE
#undef OUTSIDE


#include "intersect_3.cl"

#endif //INTERSECT_CL

