#ifndef MOONOLITH_NTREE_MATCH_HPP
#define MOONOLITH_NTREE_MATCH_HPP

#include "moonolith_match.hpp"
#include "moonolith_node.hpp"
#include "moonolith_leaf.hpp"
#include "moonolith_branch.hpp"

namespace moonolith {

	template<class Tree>
	class NTreeMatch : public Match<Tree> {
	public:
		typedef typename Tree::Traits Traits;
		typedef typename Traits::DataType DataType;
		typedef moonolith::Node<Traits> Node;
		typedef moonolith::Branch<Traits> Branch;
		typedef moonolith::Leaf<Traits> Leaf;
		typedef moonolith::Root<Traits> Root;
		
		virtual bool match(Node &node) const override
		{
			return _obj.bound().intersects(node.bound());
		}

		virtual bool match(DataType &object) const override
		{
			return _obj.bound().intersects(object.bound());
		}

		NTreeMatch(const DataType &obj)
		: _obj(obj)
		{}

	private:
		DataType _obj;
	};

}

#endif //MOONOLITH_NTREE_MATCH_HPP
