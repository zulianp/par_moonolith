#ifndef MOONOLITH_NODE_NAVIGATOR_HPP
#define MOONOLITH_NODE_NAVIGATOR_HPP 

#include "moonolith_navigator_option.hpp"
#include "moonolith_visitor.hpp"
#include "moonolith_node.hpp"
#include "moonolith_leaf.hpp"
#include "moonolith_branch.hpp"
#include "moonolith_root.hpp"

namespace moonolith {

	template<class _Traits>
	class NodeNavigator : public Visitor<_Traits>  {
	public:
		typedef _Traits Traits;
		typedef moonolith::Node<Traits> Node;
		typedef moonolith::Branch<Traits> Branch;
		typedef moonolith::Root<Traits> Root;
		typedef moonolith::Leaf<Traits> Leaf;
		typedef moonolith::Visitor<Traits> Visitor;
		typedef moonolith::TreeMemory<Traits> TreeMemory;

		virtual ~NodeNavigator() {}
		virtual NavigatorOption walk(Root &root, Visitor &visitor, TreeMemory &memory) 
		{
			return walk(static_cast< Branch & >(root), visitor, memory);
		}

		virtual NavigatorOption walk(Branch &branch, Visitor &visitor, TreeMemory &memory) = 0;
		virtual NavigatorOption walk(Leaf &leaf, Visitor &visitor, TreeMemory &memory) = 0;
	};
}

#endif //MOONOLITH_NODE_NAVIGATOR_HPP
