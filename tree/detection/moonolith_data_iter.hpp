#ifndef MOONOLITH_DATA_ITER_HPP
#define MOONOLITH_DATA_ITER_HPP 

namespace moonolith {
	template<class Tree>
	class DataIter {
	public:
		typedef typename Tree::Traits Traits;
		typedef typename Traits::DataType DataType;

		inline bool operator==(const DataIter &other) const
		{
			return it_ == other.it_;
		}

		inline bool operator!=(const DataIter &other) const
		{
			return it_ != other.it_;
		}

		DataType &operator*()
		{
			return tree_.getData(*it_);
		}

		const DataType &operator*() const
		{
			return tree_.getData(*it_);
		}

		DataIter &operator++()
		{
			++it_;
			return *this;
		}

		DataIter(Tree &tree)
		: tree_(tree)
		{}


	private:
		typedef moonolith::Node<Traits> Node;
		typedef typename Node::DataContainer DataContainer;
		typedef typename DataContainer::constiterator_ IndexIter;
		
		Tree &tree_;
		IndexIter it_;
	};
}


#endif //MOONOLITH_DATA_ITER_HPP

