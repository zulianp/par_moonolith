#ifndef MOONOLITH_DESCRIBE_HPP
#define MOONOLITH_DESCRIBE_HPP

#include "moonolith_forward_declarations.hpp"
#include "moonolith_navigator_option.hpp"

#include <ostream>

namespace moonolith {

	template<class Tree>
	class Describe : public Visitor<typename Tree::Traits> {
	public:
		typedef typename Tree::Traits Traits;
		using Node = moonolith::Node<Traits>;
		using Root = moonolith::Root<Traits>;
		using TreeMemory = moonolith::TreeMemory<Traits>;

		Describe(std::ostream &os)
		: _os(os)
		{}

		NavigatorOption visit(Node &node, TreeMemory &) override
		{
			_os << "---------------------------------------------------\n";
			node.describe(_os);
			return CONTINUE;
		}

		NavigatorOption visit(Root &root, TreeMemory &memory) override
		{
			memory.describe(_os);
			_os << "---------------------------------------------------\n";
			root.describe(_os);
			return CONTINUE;
		}

	private:
		std::ostream &_os;
	};

	template<class Tree>
	class DescribeWithGeom : public Visitor<typename Tree::Traits> {
	public:
		typedef typename Tree::Traits Traits;
		using Node = moonolith::Node<Traits>;
		using Root = moonolith::Root<Traits>;
		using TreeMemory = moonolith::TreeMemory<Traits>;

		DescribeWithGeom(std::ostream &os)
		: _os(os)
		{}


		NavigatorOption visit(Node &node, TreeMemory &) override
		{
			_os << "---------------------------------------------------\n";
			node.describe(_os);
			_os << node.bound() << "\n";
			return CONTINUE;
		}

		NavigatorOption visit(Root &root, TreeMemory &memory) override
		{
			memory.describe(_os);
			_os << "---------------------------------------------------\n";
			root.describe(_os);
			_os << root.bound() << "\n";
			return CONTINUE;
		}

	private:
		std::ostream &_os;
	};

}

#endif //MOONOLITH_DESCRIBE_HPP
