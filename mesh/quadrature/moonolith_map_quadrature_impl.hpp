#ifndef MOONOLITH_MAP_QUADRATURE_IMPL_HPP
#define MOONOLITH_MAP_QUADRATURE_IMPL_HPP

#include "moonolith_affine_transform.hpp"
#include "moonolith_map_quadrature.hpp"
#include "moonolith_simplex_transform.hpp"

namespace moonolith {

    template <typename T, int Dim>
    static Integer n_volume_elements(const PMesh<T, Dim> &polyhedron) {
        Integer n_elements = static_cast<Integer>(polyhedron.n_elements());
        if (n_elements == 4) return 1;

        Integer result = 0;
        for (Integer e = 0; e < n_elements; ++e) {
            const Integer begin = polyhedron.el_ptr[e];
            const Integer end = polyhedron.el_ptr[e + 1];
            result += (end - begin) - 2;
        }

        return result;
    }

    template <typename T, int Dim>
    void map(const Quadrature3<T> &ref, const T &scale, const PMesh<T, Dim> &poly, Quadrature<T, Dim> &out) {
        // using Point = typename Quadrature3<T>::Point;

        const Integer n_ref_points = static_cast<Integer>(ref.n_points());
        const Integer n_elements = static_cast<Integer>(poly.n_elements());
        const Integer n_sub_elements = static_cast<Integer>(n_volume_elements(poly));
        const Integer total_n_quad_points = n_sub_elements * n_ref_points;

        out.resize(total_n_quad_points);

        TetrahedronTransform<T, Dim> trafo;

        if (n_sub_elements == 1) {
            assert(poly.n_nodes() == 4);

            const Integer v0 = 0;
            const Integer v1 = 1;
            const Integer v2 = 2;
            const Integer v3 = 3;

            trafo.o = poly.points[v0];

            trafo.reinit(poly.points[v1], poly.points[v2], poly.points[v3]);

            const auto rescaling = scale * std::abs(trafo.volume());
            assert(rescaling > 0.);

            for (Integer i = 0; i < n_ref_points; ++i) {
                out.weights[i] = rescaling * ref.weights[i];
                trafo.apply(ref.points[i], out.points[i]);
            }

            return;
        }

        trafo.o = poly.barycenter();

        Integer quad_index = 0;
        for (Integer e = 0; e < n_elements; ++e) {
            const Integer begin = poly.el_ptr[e];
            const Integer end = poly.el_ptr[e + 1];
            const Integer v1 = poly.el_index[begin];

            // treat also non-triangular surface elements
            const Integer n_local_tetra = (end - begin) - 2;

            for (Integer k = 1; k <= n_local_tetra; ++k) {
                const Integer node_offset = begin + k;
                const Integer v2 = poly.el_index[node_offset];
                const Integer v3 = poly.el_index[node_offset + 1];
                trafo.reinit(poly.points[v1], poly.points[v2], poly.points[v3]);

                const auto rescaling = scale * std::abs(trafo.volume());

                assert(rescaling > 0.);
                for (Integer i = 0; i < n_ref_points; ++i) {
                    out.weights[quad_index] = rescaling * ref.weights[i];
                    trafo.apply(ref.points[i], out.points[quad_index++]);
                }
            }
        }
    }

    template <int Dim, typename T>
    void map(const Quadrature2<T> &ref, const T &scale, const Polygon<T, Dim> &poly, Quadrature<T, Dim> &out) {
        const Integer n_ref_points = static_cast<Integer>(ref.n_points());
        const Integer n_nodes = poly.size();
        const Integer n_triangles = n_nodes - 2;
        const Integer n_quad_points = n_triangles * n_ref_points;

        TriangleTransform<T, Dim> trafo;
        trafo.o = poly[0];

        out.resize(n_quad_points);
        Integer quad_index = 0;
        for (Integer i = 2; i < n_nodes; ++i) {
            trafo.reinit(poly[i - 1], poly[i]);

            const auto rescaling = scale * std::abs(trafo.volume());
            // assert(rescaling > 0.);
            if (rescaling > 0) {
                for (Integer k = 0; k < n_ref_points; ++k, ++quad_index) {
                    trafo.apply(ref.points[k], out.points[quad_index]);
                    out.weights[quad_index] = ref.weights[k] * rescaling;
                }
            }
        }

        if (quad_index != n_quad_points) {
            out.resize(quad_index);
        }
    }

    template <int Dim, typename T>
    void map(const Quadrature2<T> &ref, const T &scale, const Storage<Polygon<T, Dim>> &poly, Quadrature<T, Dim> &out) {
        const Integer n_ref_points = static_cast<Integer>(ref.n_points());
        Integer n_triangles = 0;
        Integer n_quad_points = 0;

        for (const auto &p : poly) {
            const auto n = p.size();
            n_triangles += n - 2;
            n_quad_points += (n - 2) * n_ref_points;
        }

        TriangleTransform<T, Dim> trafo;

        out.resize(n_quad_points);
        Integer quad_index = 0;

        for (const auto &p : poly) {
            const auto n_nodes = p.size();
            trafo.o = p[0];

            for (Integer i = 2; i < n_nodes; ++i) {
                trafo.reinit(p[i - 1], p[i]);

                const auto rescaling = scale * std::abs(trafo.volume());
                assert(rescaling > 0.);

                for (Integer k = 0; k < n_ref_points; ++k, ++quad_index) {
                    trafo.apply(ref.points[k], out.points[quad_index]);
                    out.weights[quad_index] = ref.weights[k] * rescaling;
                }
            }
        }
    }

    // template<int Dim, typename T>
    // void map(
    // 	const Quadrature<T, 1> &ref,
    // 	const T &scale,
    // 	const Line<T, Dim> &line,
    // 	Quadrature<T, Dim> &out
    // )
    // {
    // 	auto n_points = ref.n_points();
    // 	out.resize(n_points);

    // 	const auto &o = line.p0;
    // 	auto v = line.p1;
    // 	v -= line.p0;

    // 	const auto rescaling = scale * length(v);

    // 	for(std::size_t k = 0; k < n_points; ++k) {
    // 		auto &pk = out.points[k];
    // 		pk = v;
    // 		pk *= ref.points[k];
    // 		pk += o;

    // 		out.weights[k] = ref.weights[k] * rescaling;
    // 	}
    // }

    template <int Dim, typename T>
    void map(const Quadrature<T, 1> &ref, const T &scale, const Line<T, Dim> &line, Quadrature<T, Dim> &out) {
        Integer n_points = ref.n_points();
        out.resize(n_points);

        const auto &o = line.p0;
        auto v = line.p1;
        v -= line.p0;

        const auto rescaling = scale * length(v);

        for (Integer k = 0; k < n_points; ++k) {
            auto &pk = out.points[k];
            pk = v;
            pk *= ref.points[k].x;
            pk += o;

            out.weights[k] = ref.weights[k] * rescaling;
        }
    }

    // template <int Dim, typename T>
    // void map(const Quadrature2<T> &ref,
    //          const T &scale,
    //          const Storage<Vector<T, Dim>> &points,
    //          const Storage<int> &tri,
    //          Quadrature<T, Dim> &out) {
    //     assert(false);
    // std::size_t n_qps_rule = q_points.size();
    // std::size_t n_qps = 0;
    // std::size_t n_islands = domain_of_integration.size();

    // for(std::size_t k = 0; k < n_islands; ++k) {
    // 	n_qps += triangulations[k].size()/3 * n_qps_rule;
    // }

    // composite_q_points.resize(n_qps);
    // composite_q_weights.resize(n_qps);

    // Vector2 u, v;

    // Polygon2 triangle;
    // triangle.points.resize(3);
    // triangle.points[0] = { 0., 0.};

    // std::size_t qp = 0;
    // for(std::size_t k = 0; k < n_islands; ++k) {
    // 	const auto &tri = triangulations[k];
    // 	const auto &points = domain_of_integration[k].points;

    // 	const auto n_triangles = tri.size()/3;

    // 	for(std::size_t i = 0; i < n_triangles; ++i) {
    // 		const auto i3 = i * 3;
    // 		const auto &o = points[tri[i3]];

    // 		u = points[tri[i3 + 1]] - o;
    // 		v = points[tri[i3 + 2]] - o;

    // 		triangle.points[1] = u;
    // 		triangle.points[2] = v;

    // 		auto scale = triangle.area() * weight;
    // 		assert(scale > 0.);

    // 		for(std::size_t j = 0; j < n_qps_rule; ++j) {
    // 			composite_q_points[qp] = o;
    // 			composite_q_points[qp] += q_points[j].x * u;
    // 			composite_q_points[qp] += q_points[j].y * v;

    // 			composite_q_weights[qp] = q_weights[j] * scale;
    // 			++qp;
    // 		}
    // 	}
    // }
    // }

}  // namespace moonolith

#endif  // MOONOLITH_MAP_QUADRATURE_IMPL_HPP
