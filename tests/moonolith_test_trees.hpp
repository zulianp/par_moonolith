#ifndef MOONOLITH_TEST_TREES_HPP
#define MOONOLITH_TEST_TREES_HPP

#include "moonolith_base.hpp"

#include "moonolith_aabb.hpp"
#include "moonolith_bounding_volume_with_span.hpp"
#include "moonolith_check_can_refine.hpp"
#include "moonolith_describe.hpp"
#include "moonolith_exchange.hpp"
#include "moonolith_lookup_table_builder.hpp"
#include "moonolith_n_tree_bound_synchronize.hpp"
#include "moonolith_n_tree_mutator_factory.hpp"
#include "moonolith_n_tree_with_span_mutator_factory.hpp"
#include "moonolith_tree.hpp"

#include <array>

namespace moonolith {
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    template <Integer Dimension, class Bound>
    class TestData : public Serializable, public Describable {
    public:
        Integer handle_;
        Bound bound_;

        TestData(const Integer handle = -1) : handle_(handle) {}

        const Bound &bound() const { return bound_; }

        Bound &bound() { return bound_; }

        void apply_read_write(Stream &stream) override {
            stream &handle_;
            stream &bound_;
        }

        void describe(std::ostream &os) const override { bound_.describe(os); }

        inline Integer handle() const { return handle_; }

        inline void set_handle(const Integer handle) { handle_ = handle; }

        inline Integer tag() const { return -1; }
    };

    template <Integer _Dimension>
    struct TestTraits {
        enum { Dimension = _Dimension };

        typedef moonolith::AABB<Dimension, Real> Bound;
        typedef moonolith::TestData<Dimension, Bound> DataType;
    };

    template <Integer _Dimension>
    struct TestTraitsWithSpan {
        enum { Dimension = _Dimension };

        typedef moonolith::AABBWithKDOPSpan<Dimension, Real> Bound;
        typedef moonolith::TestData<Dimension, Bound> DataType;
    };

    template <Integer Dimension>
    class TestTree : public Tree<TestTraits<Dimension> > {
    public:
        typedef TestTraits<Dimension> Traits;
        // using DynamicTreeT = Tree<TestTraits<Dimension> >;

        static std::shared_ptr<TestTree> New() {
            auto ret = std::make_shared<TestTree>();
            std::shared_ptr<NTreeMutatorFactory<TestTree> > factory =
                std::make_shared<NTreeMutatorFactory<TestTree> >();
            factory->set_refine_params(20, 6);
            ret->set_mutator_factory(factory);
            return ret;
        }

        TestTree() {}
    };

    template <Integer Dimension>
    class TestTreeWithSpan : public Tree<TestTraitsWithSpan<Dimension> > {
    public:
        typedef TestTraitsWithSpan<Dimension> Traits;
        // using DynamicTreeT = Tree<TestTraitsWithSpan<Dimension> >;

        static std::shared_ptr<TestTreeWithSpan> New() {
            auto ret = std::make_shared<TestTreeWithSpan>();
            std::shared_ptr<NTreeWithSpanMutatorFactory<TestTreeWithSpan> > factory =
                std::make_shared<NTreeWithSpanMutatorFactory<TestTreeWithSpan> >();
            factory->set_refine_params(20, 6);
            ret->set_mutator_factory(factory);
            return ret;
        }

        TestTreeWithSpan() {}
    };

    template <class Iterator, typename T>
    void fill_random(const Iterator &begin, const Iterator &end, const T &lower_bound, const T &upper_bound) {
        const T range = upper_bound - lower_bound;
        for (auto it = begin; it != end; ++it) {
            *it = lower_bound + static_cast<T>(rand()) / RAND_MAX * range;
        }
    }

    template <class TreeT, Integer Dimension>
    std::shared_ptr<TreeT> build_test_tree(const Integer n = 10) {
        typedef typename TreeT::DataType DataT;

        Communicator world;
        srand(world.rank());
        bool is_odd = world.rank() % 2;

        std::shared_ptr<TreeT> tree = TreeT::New();
        tree->reserve(n + 2);

        DataT d1(0), d2(1);

        std::array<Real, Dimension> min1, min2, max1, max2;
        std::fill(min1.begin(), min1.end(), -1.);
        std::fill(min2.begin(), min2.end(), -0.5);
        std::fill(max1.begin(), max1.end(), 0.5);
        std::fill(max2.begin(), max2.end(), 1.);

        d1.bound() += min1;
        d1.bound() += max1;

        d2.bound() += min2;
        d2.bound() += max2;

        DataHandle dh1 = tree->insert(d1);
        assert(!DataHandle::is_null(dh1));
        DataHandle dh2 = tree->insert(d2);
        assert(!DataHandle::is_null(dh2));

        std::array<Real, Dimension> x1, x2;

        for (Integer i = 0; i < n; ++i) {
            DataT d(2 + i);
            if (is_odd) {
                fill_random(x1.begin(), x1.end(), 0, 99);
                fill_random(x2.begin(), x2.end(), 0, 99);
            } else {
                fill_random(x1.begin(), x1.end(), -100, -2);
                fill_random(x2.begin(), x2.end(), -100, -2);
            }

            d.bound() += x1;
            d.bound() += x2;

            tree->insert(d);
        }

        return tree;
    }

    template <class Tree>
    class MakeImmutable : public Visitor<typename Tree::Traits> {
    public:
        using Traits = typename Tree::Traits;
        using Node = moonolith::Node<Traits>;
        using TreeMemory = moonolith::TreeMemory<Traits>;

        virtual NavigatorOption visit(Node &node, TreeMemory &) {
#ifndef NDEBUG
            // this is only enabled for debug mode
            node.make_immutable();
#else
            MOONOLITH_UNUSED(node);
#endif  // NDEBUG
            return CONTINUE;
        }
    };
}  // namespace moonolith

#endif  // MOONOLITH_TEST_TREES_HPP
