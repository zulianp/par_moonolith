#ifndef ALGEBRA_INTERFACES_CL
#define ALGEBRA_INTERFACES_CL



#include "kernel_base.cl"
#include "base_interfaces.cl"

//types
typedef struct {
	Scalar entries[MAX_N_DIMS * MAX_N_DIMS];
} Matrixdxd;

#ifndef CLIPP_HOST_CL



//functions
void identity_3(Scalar * matrix);
void matrixdxd_print(const SizeType n_dims, const SizeType n_matrices, Matrixdxd *matrices);
bool has_nan_entries(const SizeType n_entries, const Scalar *entries);


void solve_2x2(const Scalar*mat, const Scalar *rhs, Scalar *result);
void solve_3x3(const Scalar*mat, const Scalar *rhs, Scalar *result);

static Scalar det_3(const Scalar *mat);
static Scalar det_2(const Scalar *mat);
Scalar det_n(const SizeType n_dims, const Scalar *mat);
static void inverse_2(const Scalar *in, const Scalar det, Scalar *out);
static void inverse_3(const Scalar *in, const Scalar det, Scalar *out);
void inverse_n(const SizeType n_dims, const Scalar *in, const Scalar det, Scalar *out);
Scalar trace(const SizeType rows, const SizeType cols, const Scalar *matrix);
void set(const SizeType n_entries, const Scalar value, Scalar *values);
Scalar dot_2(const Scalar *left, const Scalar *right);
static Scalar dot_3(const Scalar *left, const Scalar *right);
static Scalar min_n(const SizeType n, const Scalar *values);
static Scalar max_n(const SizeType n, const Scalar *values);
Scalar add_n(const SizeType n_entries, const Scalar *entries);
Scalar matrix_dot_n_t(const SizeType n_rows, const Scalar *left, const Scalar *right);
void matrix_copy_upper_to_lower(const SizeType n_rows_and_columns, Scalar * matrix);
void block_matrix_copy_upper_to_lower(const SizeType n_rows_and_columns,  const SizeType block_rows_and_columns, Scalar * matrix);
static Scalar dot_n(const SizeType n, const Scalar *left, const Scalar *right);
static void vec_scale(const SizeType n_entries, const Scalar scale_factor, Scalar *vector);
static void vec_minus(const SizeType n_entries, const Scalar *left, const Scalar *right, Scalar *result);
static Scalar norm_n(const SizeType n_dims, const Scalar *vector);
Scalar distance_n(const SizeType n_dims, const Scalar *left, const Scalar *right);
static void householder_reflection_3(const Scalar *vec, Scalar *result_matrix);
static void householder_reflection_2(const Scalar *vec, Scalar *result_matrix);

void mat_plus_mat_transposed(const SizeType rows_and_cols, const Scalar scale_left, const Scalar *left, const Scalar scale_right, const Scalar *right, Scalar *result);
void mat_symmetrized(const SizeType rows_and_col, const Scalar *matrix, Scalar *result);
void transpose(const SizeType rows_and_cols, const Scalar *mat, Scalar *result);
void set_block_to_identity(const SizeType block_i, 	const SizeType block_j, 
						   const SizeType block_rows, const SizeType block_columns, 
						   const SizeType n_block_rows, const SizeType n_block_columns,
						   const Scalar diag_scaling, Scalar * matrix);

void mat_mat_mul(const SizeType left_rows, const SizeType left_columns, const SizeType right_columns,  const Scalar *left, const Scalar *right, Scalar *result);
static void mat_vec_mul(const SizeType left_rows, const SizeType left_columns, const Scalar *left, const Scalar *right, Scalar *result);
void col_average(const SizeType n_rows, const SizeType n_cols, const Scalar *matrix, Scalar *result);
void col_subtract(const SizeType n_rows, const SizeType n_cols, const Scalar *col_vector, Scalar *matrix);
void col_arg_min(const SizeType n_rows, const SizeType n_cols, const Scalar *matrix, SizeType *i_max);
void col_arg_max(const SizeType n_rows, const SizeType n_cols, const Scalar *matrix, SizeType *i_max);

static void row_average(const SizeType n_rows, const SizeType n_cols, const Scalar *matrix, Scalar *result);
static void row_subtract(const SizeType n_rows, const SizeType n_cols, const Scalar *row_vector, Scalar *matrix);
static void row_arg_min(const SizeType n_rows, const SizeType n_cols, const Scalar *matrix, SizeType *i_min);
static void row_arg_max(const SizeType n_rows, const SizeType n_cols, const Scalar *matrix, SizeType *i_max);

void mat_times_self_transposed(const SizeType rows_and_cols, const Scalar *mat, Scalar * result);
static SizeType symmetric_matrix_index(const SizeType i, const SizeType j);
static SizeType symmetric_matrix_n_entries(const SizeType n_rows);

static void axpy(const SizeType n_entries, const Scalar alpha, const Scalar *x, Scalar *y);
static void matrix_print(const SizeType rows, const SizeType cols, const Scalar *matrix);
void g_matrix_print(const SizeType rows, const SizeType cols, m_global__ const Scalar *matrix);
void g_int_matrix_print(const SizeType rows, const SizeType cols, m_global__ const SizeType *matrix);

void mat_symmetrize(const SizeType rows_and_col, Scalar *matrix);
void outer_product(const SizeType size_left, const SizeType size_right, const Scalar *left, const Scalar *right, Scalar * result);

void block_matrix_print(const SizeType n_block_rows, 
	const SizeType n_block_columns, 
	const SizeType block_rows, 
	const SizeType block_columns, const Scalar *matrix);

void reduce_add_private_values(const SizeType n_private_values, const Scalar * private_values, m_local__ Scalar *local_work, m_global__ Scalar *result);
void vec_repeat(const SizeType n_times_to_repeat, const SizeType n_elements_to_repeat, const Scalar *in, Scalar *out);

#endif //CLIPP_HOST_CL
#endif //ALGEBRA_INTERFACES_CL
