#ifndef MOONOLITH_ELEM_TRI_6_HPP
#define MOONOLITH_ELEM_TRI_6_HPP

#include "moonolith_elem_tri3.hpp"

namespace moonolith {

    template <typename T_, int PhysicalDim_ = 2>
    class Tri6 final : public Tri<T_, 2, PhysicalDim_> {
    public:
        static const int Order = 2;
        static const int Dim = 2;
        static const int PhysicalDim = PhysicalDim_;
        static const int NNodes = 6;

        using T = T_;

        using Vector = moonolith::Vector<T, Dim>;
        using CoVector = moonolith::Vector<T, PhysicalDim>;

        using Point = moonolith::Vector<T, Dim>;
        using CoPoint = moonolith::Vector<T, PhysicalDim>;

        using Super = moonolith::Tri<T_, 2, PhysicalDim_>;
        using Polygon = typename Super::Polygon;

    private:
        class Fun final {
        public:
            Fun() {
                f[0] = [](const Point &p) -> T {
                    return 1.0 - (3.0 * p.x) - (3.0 * p.y) + (2.0 * p.x * p.x) + (2.0 * p.y * p.y) + (4.0 * p.x * p.y);
                };

                f[1] = [](const Point &p) -> T { return (2.0 * p.x * p.x) - p.x; };

                f[2] = [](const Point &p) -> T { return (2.0 * p.y * p.y) - p.y; };

                f[3] = [](const Point &p) -> T { return (4.0 * p.x) - (4.0 * p.x * p.x) - (4.0 * p.x * p.y); };

                f[4] = [](const Point &p) -> T { return (4.0 * p.x * p.y); };

                f[5] = [](const Point &p) -> T { return (4.0 * p.y) - (4.0 * p.x * p.y) - (4.0 * p.y * p.y); };
            }

            std::array<std::function<T(const Point &)>, 6> f;
        };

        class Grad final {
        public:
            Grad() {
                f[0] = [](const Point &p, Vector &g) {
                    g.x = -3.0 + (4.0 * p.x) + (4.0 * p.y);
                    g.y = -3.0 + (4.0 * p.y) + (4.0 * p.x);
                };

                f[1] = [](const Point &p, Vector &g) {
                    g.x = (4.0 * p.x) - 1.0;
                    g.y = 0.0;
                };

                f[2] = [](const Point &p, Vector &g) {
                    g.x = 0.0;
                    g.y = (4.0 * p.y) - 1.0;
                };

                f[3] = [](const Point &p, Vector &g) {
                    g.x = 4.0 - 8.0 * p.x - 4.0 * p.y;
                    g.y = -4.0 * p.x;
                };

                f[4] = [](const Point &p, Vector &g) {
                    g.x = (4.0 * p.y);
                    g.y = (4.0 * p.x);
                };

                f[5] = [](const Point &p, Vector &g) {
                    g.x = -4.0 * p.y;
                    g.y = 4.0 - 8.0 * p.y - 4.0 * p.x;
                };
            }

            std::array<std::function<void(const Point &, Vector &)>, 6> f;
        };

        class Hessian {
        public:
            Hessian() {
                // g.x = -3.0 + (4.0 * p.x) + (4.0 * p.y);
                // g.y = -3.0 + (4.0 * p.y) + (4.0 * p.x);
                f[0] = [](const Point &, std::array<T, Dim * Dim> &H) {
                    H[0] = 4.0;
                    H[1] = 4.0;
                    H[2] = 4.0;
                    H[3] = 4.0;
                };

                // g.x = (4.0 * p.x) - 1.0;
                // g.y = 0.0;
                f[1] = [](const Point &, std::array<T, Dim * Dim> &H) {
                    H[0] = 4.0;
                    H[1] = 0.0;
                    H[2] = 0.0;
                    H[3] = 0.0;
                };

                // g.x = 0.0;
                // g.y = (4.0 * p.y) - 1.0;
                f[2] = [](const Point &, std::array<T, Dim * Dim> &H) {
                    H[0] = 0.0;
                    H[1] = 0.0;
                    H[2] = 0.0;
                    H[3] = 4.0;
                };

                // g.x = 4.0 - 8.0 * p.x - 4.0 * p.y;
                // g.y = -4.0 * p.x;
                f[3] = [](const Point &, std::array<T, Dim * Dim> &H) {
                    H[0] = 8.0;
                    H[1] = -4.0;
                    H[2] = -4.0;
                    H[3] = 0.0;
                };

                // g.x = (4.0 * p.y);
                // g.y = (4.0 * p.x);
                f[4] = [](const Point &, std::array<T, Dim * Dim> &H) {
                    H[0] = 0.0;
                    H[1] = 4.0;
                    H[2] = 4.0;
                    H[3] = 0.0;
                };

                // g.x = -4.0 * p.y;
                // g.y = 4.0 - 8.0 * p.y - 4.0 * p.x;
                f[5] = [](const Point &, std::array<T, Dim * Dim> &H) {
                    H[0] = 0.0;
                    H[1] = -4.0;
                    H[2] = -4.0;
                    H[3] = 8.0;
                };
            }

            // H = [ f_xx, f_xy; f_yx, f_yy ];
            std::array<std::function<void(const Point &, std::array<T, Dim * Dim> &)>, 6> f;
        };

    public:
        T fun(const Integer i, const Point &p) const override { return fun_.f[i](p); }

        void grad(const Integer i, const Point &p, Vector &g) const override { grad_.f[i](p, g); }

        void hessian(const Integer i, const Point &p, std::array<T, Dim * Dim> &H) { hessian_.f[i](p, H); }

        void jacobian(const Point &p, std::array<T, PhysicalDim * Dim> &J) const override {
            if (is_affine()) {
                affine_approx_jacobian(J);
                return;
            }

            IsoParametric<Tri6>::jacobian(*this, p, J);
        }

        void affine_approx_jacobian(std::array<T, PhysicalDim * Dim> &J) const override {
            Affine<Tri6>::jacobian(*this, J);
        }

        CoPoint &node(const Integer i) override { return nodes_[i]; }

        const CoPoint &node(const Integer i) const override { return nodes_[i]; }

        void point(const Point &p, CoPoint &q) const override { IsoParametric<Tri6>::point(*this, p, q); }

        Tri6() : is_affine_(false) {}

        bool is_affine() const override { return is_affine_; }
        inline bool is_simplex() const override { return true; }
        inline int order() const override { return Order; }

        inline void set_affine(const bool val) { is_affine_ = val; }

        inline int n_nodes() const override { return NNodes; }

        inline std::array<CoPoint, 6> &nodes() { return nodes_; }

        inline const std::array<CoPoint, 6> &nodes() const { return nodes_; }

        inline void make_reference() {
            Reference<Tri6>::points(nodes_);
            set_affine(true);
        }

        inline ElemType type() const override { return TRI6; }

        inline T approx_measure() const override {
            if (is_affine()) {
                return 0.5 * trapezoid_area(nodes_[0], nodes_[1], nodes_[2]);
            } else {
                return 0.5 * (trapezoid_area(nodes_[0], nodes_[3], nodes_[5]) +
                              trapezoid_area(nodes_[3], nodes_[1], nodes_[4]) +
                              trapezoid_area(nodes_[5], nodes_[4], nodes_[2]) +
                              trapezoid_area(nodes_[3], nodes_[4], nodes_[5]));
            }
        }

        inline T measure() const override {
            if (is_affine()) {
                return 0.5 * trapezoid_area(nodes_[0], nodes_[1], nodes_[2]);
            } else {
                Quadrature<T, Dim> q;
                Gauss::get(2, q);
                return IsoParametric<Tri6>::measure(*this, q);
            }
        }

        inline T reference_measure() const override { return 0.5; }

        inline void make(Polygon &poly) const override {
            if (is_affine()) {
                poly.resize(3);

                for (int i = 0; i < 3; ++i) {
                    poly[i] = node(i);
                }
            } else {
                poly.resize(6);

                for (int i = 0; i < 3; ++i) {
                    const auto i2 = i * 2;
                    poly[i2] = node(i);
                    poly[i2 + 1] = node(3 + i);
                }
            }
        }

    private:
        bool is_affine_;
        std::array<CoPoint, 6> nodes_;

        const Fun fun_;
        const Grad grad_;
        const Hessian hessian_;
    };

    template <typename T, int PhysicalDim>
    class Reference<Tri6<T, PhysicalDim>> {
    public:
        // using Point = typename Tri6<T, 2, PhysicalDim>::Point;
        static const int NNodes = 6;

        template <class Point, std::size_t Size>
        static void points(std::array<Point, Size> &nodes) {
            static_assert(Size >= 6, "size must be at least 6");

            Reference<Tri3<T, PhysicalDim>>::points(nodes);

            // p3
            nodes[3].x = 0.5;
            nodes[3].y = 0.0;

            // p4
            nodes[4].x = 0.5;
            nodes[4].y = 0.5;

            // p5
            nodes[5].x = 0.0;
            nodes[5].y = 0.5;
        }

        inline static T measure() { return 0.5; }
    };

    template <typename T, int PhysicalDim>
    class InvertOrientation<Tri6<T, PhysicalDim>> {
    public:
        inline static void apply(Tri6<T, PhysicalDim> &elem) {
            std::swap(elem.node(1), elem.node(2));
            std::swap(elem.node(3), elem.node(5));
        }
    };

    template <typename T, int PhysicalDim>
    class GaussQRule<Tri6<T, PhysicalDim>> {
    public:
        static bool get(const Integer order, Quadrature2<T> &q) { return Gauss::get(order, q); }
    };
}  // namespace moonolith

#endif  // MOONOLITH_ELEM_TRI_6_HPP
