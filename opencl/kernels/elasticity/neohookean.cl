#ifndef NEOHOOKEAN_CL
#define NEOHOOKEAN_CL 

#include "deformation.cl"

#include "fe/fe_object.cl"
#include "assembly/assemble_interfaces.cl"
#include "algebra/algebra_interfaces.cl"
#include "von_mises_stress.cl"
#include "neohookean_interfaces.cl"

Scalar compressible_neohookean_energy_at_quad_index(const SizeType quad_index, 
	const NeohookeanMaterial *material, 
	const FEObject *fe, 
	const Deformation *def)
{
	const SizeType n_dims = fe->n_dims;
	
	const Scalar * C = def->cauchy_strain;
	const Scalar J   = def->grad_determinant;
	const Scalar log_J = log(J);

	const Scalar mu = material->mu;
	const Scalar lambda = material->lambda;

	return ( 
		0.5 * mu * ( trace(n_dims, n_dims, C) - n_dims ) - 
		mu * log_J + 
		lambda * log_J * log_J 
		) * fe->dx[quad_index];
}

void compressible_neohookean_first_pk_stress(const SizeType quad_index,
	const NeohookeanMaterial *material, 
	const FEObject *fe, 
	const Deformation *def, 
	Scalar *result)
{
	Scalar temp[MAX_N_DIMS * MAX_N_DIMS];

	const SizeType dims2 = fe->n_dims * fe->n_dims;
	mat_plus_mat_transposed(fe->n_dims, 1, def->grad, -1, def->inverse_grad, result);
	
	vec_scale(dims2, material->mu, result);

	const Scalar lambda_log_det = material->lambda * log(def->grad_determinant);
	
	generic_copy(dims2, def->inverse_grad, temp);
	vec_scale(dims2, lambda_log_det, temp);

	mat_plus_mat_transposed(fe->n_dims, 1, result, 1, temp, result);
}

void compressible_neohookean_gradient_at_quad_index(const SizeType quad_index,
	const NeohookeanMaterial *material, 
	const FEObject *trial, const FEObject *test, 
	const Deformation *def, 
	const Matrixdxd *test_grad,
	Scalar *cummulative_result)
{
	const SizeType n_dims = test->n_dims;
	const SizeType dims2 = n_dims*n_dims;


	Scalar P[MAX_N_DIMS * MAX_N_DIMS];
	compressible_neohookean_first_pk_stress(quad_index, material, trial, def, P);

	SizeType index = 0;
	for(SizeType block_i = 0; block_i < test->n_shape_functions; ++block_i) {
		for(SizeType block_ik = 0; block_ik < test->n_dims; ++block_ik) {
			const SizeType i = block_get_i(block_i, test->n_dims, block_ik);

			cummulative_result[index++] += dot_n(dims2, test_grad[i].entries, P) * test->dx[quad_index];
		}
	}
}


void aux_compute_element_grad_inv_x_grad(const FEObject *element, const Matrixdxd *element_grad, const Deformation *def, const bool must_transpose, Matrixdxd * grad_inv_x_grad)
{
	const SizeType n_dims = element->n_dims;	
	for(SizeType block_i = 0; block_i < element->n_shape_functions; ++block_i) {
		for(SizeType block_ik = 0; block_ik < element->n_dims; ++block_ik) {
			const SizeType i = block_get_i(block_i, element->n_dims, block_ik);
			mat_mat_mul(n_dims, n_dims, n_dims, def->inverse_grad, element_grad[i].entries, grad_inv_x_grad[i].entries);
			if(must_transpose) transpose(n_dims, grad_inv_x_grad[i].entries, grad_inv_x_grad[i].entries);
		}
	}
}

void aux_compute_element_dots(const FEObject *element, const Matrixdxd *element_grad, const Deformation *def, Scalar *dots)
{
	const SizeType n_dims = element->n_dims;	
	const SizeType dims2 = n_dims * n_dims;
	for(SizeType block_i = 0; block_i < element->n_shape_functions; ++block_i) {
		for(SizeType block_ik = 0; block_ik < element->n_dims; ++block_ik) {
			const SizeType i = block_get_i(block_i, element->n_dims, block_ik);
			dots[i] = dot_n(dims2, def->inverse_grad_transposed, element_grad[i].entries);
		}
	}
}


void compressible_neohookean_hessian_symmetric_at_quad_index(const SizeType quad_index,
	const NeohookeanMaterial *material, 
	const FEObject *element,
	const Deformation *def, 
	const Matrixdxd *element_grad,
	Scalar *cummulative_result
	)
{
	Matrixdxd grad_inv_x_grad  [MAX_SHAPE_FUNCS * MAX_CODIM];
	Matrixdxd grad_inv_x_grad_t[MAX_SHAPE_FUNCS * MAX_CODIM];
	Scalar dots 			   [MAX_SHAPE_FUNCS * MAX_CODIM];


	const SizeType n_dims = element->n_dims;
	const SizeType dims2 = n_dims*n_dims;

	const Scalar mu = material->mu;
	const Scalar lambda = material->lambda;
	const Scalar log_J = log(def->grad_determinant);

	aux_compute_element_grad_inv_x_grad(element, element_grad,  def, false, grad_inv_x_grad);
	aux_compute_element_grad_inv_x_grad(element, element_grad,  def, true,  grad_inv_x_grad_t);
	aux_compute_element_dots(element, element_grad, def, dots);

	symmetric_assemble_block_ij(
		element, cummulative_result, 
		(
			(	
				mu * dot_n(dims2, element_grad[j].entries, element_grad[i].entries) 
				- 
				(lambda * log_J - mu) * 
				dot_n(dims2, grad_inv_x_grad[j].entries, grad_inv_x_grad_t[i].entries) 
				+ 
				lambda * dots[j] * dots[i]
			) * element->dx[quad_index]
		)
	);
}

void compressible_neohookean_hessian_at_quad_index(const SizeType quad_index,
	const NeohookeanMaterial *material, 
	const FEObject *trial, const FEObject *test, 
	const Deformation *def, 
	const Matrixdxd *trial_grad,
	const Matrixdxd *test_grad,
	Scalar *cummulative_result
	)
{

	Matrixdxd grad_inv_x_grad_trial[MAX_SHAPE_FUNCS * MAX_CODIM];
	Matrixdxd grad_inv_x_grad_test [MAX_SHAPE_FUNCS * MAX_CODIM];
	
	Scalar trial_dots [MAX_SHAPE_FUNCS * MAX_CODIM];
	Scalar test_dots  [MAX_SHAPE_FUNCS * MAX_CODIM];

	const SizeType n_dims = test->n_dims;
	const SizeType dims2 = n_dims*n_dims;

	const Scalar mu 	= material->mu;
	const Scalar lambda = material->lambda;
	const Scalar log_J = log(def->grad_determinant);
	const Scalar lambda_log_J_m_mu = (lambda * log_J - mu);

	aux_compute_element_grad_inv_x_grad(trial, trial_grad, def, false, grad_inv_x_grad_trial);
	aux_compute_element_grad_inv_x_grad(test, test_grad,   def, true,  grad_inv_x_grad_test);
	aux_compute_element_dots(trial, trial_grad, def, trial_dots);
	aux_compute_element_dots(test,  test_grad,  def, test_dots);

	assemble_block_ij(trial, test, cummulative_result, 
		(
			(
				mu * dot_n(dims2, trial_grad[j].entries, test_grad[i].entries) 
				- 
				lambda_log_J_m_mu * 
				dot_n(dims2, grad_inv_x_grad_trial[j].entries,  grad_inv_x_grad_test[i].entries) 
				+ 
				lambda * trial_dots[j] * test_dots[i]
			) * test->dx[quad_index]
		)
	);
}


Scalar compressible_neohookean_assemble_energy(
	const NeohookeanMaterial * material,
	const FEObject * trial, 
	const FEObject * test, 
	const Scalar * local_displacement)
{
	const SizeType n_dims = test->n_dims;
	

	FEFunction displacement_gradient;
	Deformation deformation;

	Matrixdxd trial_grad[MAX_SHAPE_FUNCS*MAX_CODIM];
	Matrixdxd test_grad[MAX_SHAPE_FUNCS*MAX_CODIM];

	Scalar result = 0;
	for(SizeType q = 0; q < test->n_quad_points; ++q) {
		make_grad_dxd_at_quad_index(q, trial, trial_grad);
		make_grad_dxd_at_quad_index(q, test, test_grad);

		interpolate_dxd_at_quad_point(q, trial_grad, trial, local_displacement, &displacement_gradient);
		make_deformation(n_dims, &displacement_gradient, &deformation);

		result += compressible_neohookean_energy_at_quad_index(q, material, test, &deformation);
		
	}

	return result;
}



void compressible_neohookean_assemble_gradient(
	const NeohookeanMaterial * material,
	const FEObject * trial, 
	const FEObject * test, 
	const Scalar * local_displacement,
	Scalar * local_gradient)
{
	const SizeType n_dims = test->n_dims;
	set(trial->n_shape_functions * n_dims, 0, local_gradient);

	FEFunction displacement_gradient;
	Deformation deformation;

	Matrixdxd trial_grad[MAX_SHAPE_FUNCS*MAX_CODIM];
	Matrixdxd test_grad [MAX_SHAPE_FUNCS*MAX_CODIM];

	for(SizeType q = 0; q < test->n_quad_points; ++q) {
		make_grad_dxd_at_quad_index(q, trial, trial_grad);
		make_grad_dxd_at_quad_index(q, test, test_grad);

		interpolate_dxd_at_quad_point(q, trial_grad, trial, local_displacement, &displacement_gradient);
		make_deformation(n_dims, &displacement_gradient, &deformation);

		compressible_neohookean_gradient_at_quad_index(q, material, trial, test, &deformation, test_grad, local_gradient);
	}
}


void compressible_neohookean_symmetric_assemble_local(
	const NeohookeanMaterial * material,
	const FEObject * element, 
	const Scalar * local_displacement,
	Scalar * local_hessian,
	Scalar * local_gradient
	)
{
	const SizeType n_dims = element->n_dims;
	const SizeType n_dims2 = n_dims * n_dims;
	set(element->n_shape_functions * element->n_shape_functions * n_dims2, 0, local_hessian);
	set(element->n_shape_functions * n_dims, 0, local_gradient);

	FEFunction displacement_gradient;
	Deformation deformation;

	Matrixdxd element_grad[MAX_SHAPE_FUNCS*MAX_CODIM];

	for(SizeType q = 0; q < element->n_quad_points; ++q) {
		make_grad_dxd_at_quad_index(q, element, element_grad);
		interpolate_dxd_at_quad_point(q, element_grad, element, local_displacement, &displacement_gradient);
		make_deformation(n_dims, &displacement_gradient, &deformation);
		compressible_neohookean_hessian_symmetric_at_quad_index(q, material, element, &deformation, element_grad, local_hessian);
		compressible_neohookean_gradient_at_quad_index(q, material, element, element, &deformation, element_grad, local_gradient);
	}

	block_matrix_copy_upper_to_lower(element->n_shape_functions, n_dims, local_hessian);
}

void compressible_neohookean_assemble_local(
	const NeohookeanMaterial * material,
	const FEObject * trial, 
	const FEObject * test, 
	const Scalar * local_displacement,
	Scalar * local_hessian,
	Scalar * local_gradient
	)
{
	if(trial == test) {
		compressible_neohookean_symmetric_assemble_local(material, test, local_displacement, local_hessian, local_gradient);
		return;
	}

	const SizeType n_dims = test->n_dims;
	const SizeType n_dims2 = n_dims * n_dims;
	set(trial->n_shape_functions * test->n_shape_functions * n_dims2, 0, local_hessian);
	set(trial->n_shape_functions * n_dims, 0, local_gradient);

	FEFunction displacement_gradient;
	Deformation deformation;

	Matrixdxd trial_grad[MAX_SHAPE_FUNCS * MAX_CODIM];
	Matrixdxd test_grad [MAX_SHAPE_FUNCS * MAX_CODIM];

	for(SizeType q = 0; q < test->n_quad_points; ++q) {
		make_grad_dxd_at_quad_index(q, trial, trial_grad);
		make_grad_dxd_at_quad_index(q, test, test_grad);

		interpolate_dxd_at_quad_point(q, trial_grad, trial, local_displacement, &displacement_gradient);
		make_deformation(n_dims, &displacement_gradient, &deformation);

		compressible_neohookean_hessian_at_quad_index (q, material, trial, test, &deformation, trial_grad, test_grad, local_hessian);
		compressible_neohookean_gradient_at_quad_index(q, material, trial, test, &deformation, test_grad, local_gradient);
	}
}

void neohookean_von_mises_stress_local(const NeohookeanMaterial *material, 
	const FEObject * trial, 
	const FEObject * test, 
	const Scalar * local_displacement,
	Scalar *result)
{
	FEFunction displacement_gradient;
	Deformation deformation;
	Scalar stress[MAX_N_DIMS * MAX_N_DIMS];
	Matrixdxd trial_grad[MAX_SHAPE_FUNCS*MAX_CODIM];
	
	const SizeType n_dims = test->n_dims;

	set(trial->n_shape_functions * n_dims, 0, result);

	for(SizeType q = 0; q < test->n_quad_points; ++q) {
		make_grad_dxd_at_quad_index(q, trial, trial_grad);
		interpolate_dxd_at_quad_point(q, trial_grad, trial, local_displacement, &displacement_gradient);
		make_deformation(n_dims, &displacement_gradient, &deformation);
		compressible_neohookean_first_pk_stress(q, material, trial, &deformation, stress);
		von_mises_stress_at_quad_index(q, test, stress, result);
	}
}

m_kernel__ void assemble_compressible_neohookean_with_values(			//input
							//A) basic data
							const SizeType n_dims,					//0)
							const SizeType n_codims,				//1)
							const SizeType n_elements,				//2)

						    //B) quadrature meta-data
						    m_global__ SizeType * quad_meta, 			//3)  (n_elements) x 4 [order, n_shape_fun_offset, n_quad_points_offset, element_matrix_offset]

							//C) Geometry
							m_global__ Scalar   * global_points,      //4)
							m_global__ Scalar   * jacobians,			//5)
							m_global__ Scalar   * dxs,				//6)

							//D) Basis functions
						    m_global__ Scalar   *	functions,			//7) 
						    m_global__ Scalar   * gradients,			//8)

    						//E) output
							m_global__ Scalar	  * hessian,			//9) (sum((el_ptr[1+1]-el_ptr[i])^2)*n_block_entries=1)
							m_global__ Scalar   * gradient,			//10)
							//F) Model paramters (EXTRAS)				
							const Scalar mu,						//11)
							const Scalar lambda,					//12)

							//G) Extra data
							const m_global__ SizeType * el_ptr, 		//13) //n_elements+1
						    const m_global__ SizeType * el_index, 	//14)
							const m_global__ Scalar * displacement 	//15)
)
{
	KERNEL_ASSERT(n_dims == n_codims, "n_codims is supported only for n_codims = n_dims");

	Scalar local_hessian[MAX_SHAPE_FUNCS * MAX_SHAPE_FUNCS * MAX_CODIM * MAX_CODIM];
	Scalar local_gradient[MAX_SHAPE_FUNCS * MAX_CODIM];
	Scalar local_displacement[MAX_SHAPE_FUNCS * MAX_CODIM];

	const SizeType n_dims2 = n_dims * n_dims;

	NeohookeanMaterial material;
	material.mu = mu;
	material.lambda = lambda;

	FEObject fe;
	for(SizeType i = get_global_id(0); i < n_elements; i+= get_global_size(0)) {
		const SizeType element_matrix_offset = get_element_matrix_offset(i, quad_meta);
		const SizeType element_vector_offset = get_element_vector_offset(i, quad_meta);
		

		make_fe_object_from_global_values(i, n_dims, quad_meta, global_points, jacobians, dxs,	functions, gradients, &fe);
		
		block_global_to_local(i, n_codims, &fe, el_ptr, el_index, displacement, local_displacement);

		compressible_neohookean_assemble_local(&material, &fe, &fe, local_displacement, local_hessian, local_gradient);

		const SizeType n_matrix_entries = fe.n_shape_functions * fe.n_shape_functions * n_dims2;
		const SizeType n_vector_entries = fe.n_shape_functions * n_dims;

		//local to global memory
		generic_copy(n_matrix_entries, local_hessian,  &hessian[element_matrix_offset]);
		generic_copy(n_vector_entries, local_gradient, &gradient[element_vector_offset]);		
	}
}

m_kernel__ void assemble_compressible_neohookean_gradient_with_values(
							//input
							//A) basic data
							const SizeType n_dims,					//0)
							const SizeType n_codims,				//1)
							const SizeType n_elements,				//2)

						    //B) quadrature meta-data
						    m_global__ SizeType * quad_meta, 			//3)  (n_elements) x 4 [order, n_shape_fun_offset, n_quad_points_offset, element_matrix_offset]

							//C) Geometry
							m_global__ Scalar   * global_points,      //4)
							m_global__ Scalar   * jacobians,			//5)
							m_global__ Scalar   * dxs,				//6)

							//D) Basis functions
						    m_global__ Scalar   *	functions,			//7) 
						    m_global__ Scalar   * gradients,			//8)

    						//E) output
							m_global__ Scalar   * gradient,			//9)
							//F) Model paramters (EXTRAS)				
							const Scalar mu,						//10)
							const Scalar lambda,					//11)

							//G) Extra data
							const m_global__ SizeType * el_ptr, 		//12) //n_elements+1
						    const m_global__ SizeType * el_index, 	//13)
							const m_global__ Scalar * displacement 	//14)
)
{
	KERNEL_ASSERT(n_dims == n_codims, "n_codims is supported only for n_codims = n_dims");

	Scalar local_gradient[MAX_SHAPE_FUNCS * MAX_CODIM];
	Scalar local_displacement[MAX_SHAPE_FUNCS * MAX_CODIM];



	NeohookeanMaterial material;
	material.mu = mu;
	material.lambda = lambda;

	FEObject fe;
	for(SizeType i = get_global_id(0); i < n_elements; i+= get_global_size(0)) {
		const SizeType element_vector_offset = get_element_vector_offset(i, quad_meta);
		
		make_fe_object_from_global_values(i, n_dims, quad_meta, global_points, jacobians, dxs,	functions, gradients, &fe);
		
		block_global_to_local(i, n_codims, &fe, el_ptr, el_index, displacement, local_displacement);

		compressible_neohookean_assemble_gradient(&material, &fe, &fe, local_displacement, local_gradient);

		const SizeType n_vector_entries = fe.n_shape_functions * n_dims;

		//local to global memory
		generic_copy(n_vector_entries, local_gradient, &gradient[element_vector_offset]);
	}
}

m_kernel__ void assemble_compressible_neohookean_energy_with_values(
							//input
							//A) basic data
							const SizeType n_dims,					//0)
							const SizeType n_codims,				//1)
							const SizeType n_elements,				//2)

						    //B) quadrature meta-data
						    m_global__ SizeType * quad_meta, 			//3)  (n_elements) x 4 [order, n_shape_fun_offset, n_quad_points_offset, element_matrix_offset]

							//C) Geometry
							m_global__ Scalar   * global_points,      //4)
							m_global__ Scalar   * jacobians,			//5)
							m_global__ Scalar   * dxs,				//6)

							//D) Basis functions
						    m_global__ Scalar   *	functions,			//7) 
						    m_global__ Scalar   * gradients,			//8)

    						//E) output
							m_global__ Scalar   * energy,				//9) min(n_elements, n_work_groups)
							//F) Model paramters (EXTRAS)				
							const Scalar mu,						//10)
							const Scalar lambda,					//11)

							//G) Extra data
							const m_global__ SizeType * el_ptr, 		//12) //n_elements+1
						    const m_global__ SizeType * el_index, 	//13)
							const m_global__ Scalar * displacement, 	//14)
							m_local__ Scalar * local_work				//15)
)
{
	KERNEL_ASSERT(n_dims == n_codims, "n_codims is supported only for n_codims = n_dims");

	Scalar local_displacement[MAX_SHAPE_FUNCS * MAX_CODIM];

	NeohookeanMaterial material;
	material.mu = mu;
	material.lambda = lambda;

	Scalar energy_private = 0;
	FEObject fe;
	for(SizeType i = get_global_id(0); i < n_elements; i+= get_global_size(0)) {		
		make_fe_object_from_global_values(i, n_dims, quad_meta, global_points, jacobians, dxs,	functions, gradients, &fe);
		block_global_to_local(i, n_codims, &fe, el_ptr, el_index, displacement, local_displacement);
		energy_private += compressible_neohookean_assemble_energy(&material, &fe, &fe, local_displacement);		
	}

	reduce_add_private_values(1, &energy_private, local_work, energy);
}



m_kernel__ void assemble_compressible_neohookean_von_mises_stress_with_values(
							//input
							//A) basic data
							const SizeType n_dims,					//0)
							const SizeType n_codims,				//1)
							const SizeType n_elements,				//2)

						    //B) quadrature meta-data
						    m_global__ SizeType * quad_meta, 			//3)  (n_elements) x 4 [order, n_shape_fun_offset, n_quad_points_offset, element_matrix_offset]

							//C) Geometry
							m_global__ Scalar   * global_points,      //4)
							m_global__ Scalar   * jacobians,			//5)
							m_global__ Scalar   * dxs,				//6)

							//D) Basis functions
						    m_global__ Scalar   *	functions,			//7) 
						    m_global__ Scalar   * gradients,			//8)

    						//E) output
							m_global__ Scalar   * stress,				//9)
							//F) Model paramters (EXTRAS)				
							const Scalar mu,						//10)
							const Scalar lambda,					//11)

							//G) Extra data
							const m_global__ SizeType * el_ptr, 		//12) //n_elements+1
						    const m_global__ SizeType * el_index, 	//13)
							const m_global__ Scalar * displacement 	//14)
)
{
	KERNEL_ASSERT(n_dims == n_codims, "n_codims is supported only for n_codims = n_dims");

	Scalar local_stress_and_weights[MAX_SHAPE_FUNCS*2];
	Scalar local_displacement[MAX_SHAPE_FUNCS * MAX_CODIM];

	NeohookeanMaterial material;
	material.mu = mu;
	material.lambda = lambda;

	FEObject fe;
	for(SizeType i = get_global_id(0); i < n_elements; i+= get_global_size(0)) {
		//Remove the offset given by the vector function since this is scalar
		const SizeType element_vector_offset = 2 * (get_element_vector_offset(i, quad_meta) / n_codims);
		
		make_fe_object_from_global_values(i, n_dims, quad_meta, global_points, jacobians, dxs,	functions, gradients, &fe);
		
		block_global_to_local(i, n_codims, &fe, el_ptr, el_index, displacement, local_displacement);

		neohookean_von_mises_stress_local(&material, &fe, &fe, local_displacement, local_stress_and_weights);

		const SizeType n_vector_entries = fe.n_shape_functions * 2;

		//local to global memory
		generic_copy(n_vector_entries, local_stress_and_weights, &stress[element_vector_offset]);	
	}
}

#endif //NEOHOOKEAN_CL
