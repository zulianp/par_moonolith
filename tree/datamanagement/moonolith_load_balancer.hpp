#ifndef MOONOLITH_LOAD_BALANCER_HPP
#define MOONOLITH_LOAD_BALANCER_HPP

#include "moonolith_node_and_proc_collector.hpp"

namespace moonolith {

    template <class Tree>
    class LoadBalancer {
    public:
        void set_use_linear_cost(const bool use_linear) { use_linear_ = use_linear; }

        Integer cost(const Integer weight_1, const Integer weight_2) {
            if (use_linear_) {
                return weight_1 + weight_2;
            } else {
                return weight_1 * weight_2;
            }
        }

        ///@bried Given a tree and a look up table returns an array assigning local cells to processes
        bool balance(Communicator &comm, Tree &tree, WorkTable &table) {
            // Compute local weights for each tree-node pairs (also self intersecting nodes)
            // Serialize them in a sorted way such that the ordering matches for both processes owning the node
            // Communicate and compute quadratic weights
            // create jobs for only one instance of the pairs'
            // Partitioning algotrithm as in the paper: Bottom-up construction and 2:1 balance refinement of linear
            // octrees in parallel linear octrees in parallel
            table.sort_work(tree);

            if (comm.is_alone()) {
                for (auto &w : table.work()) {
                    w->set_worker(0);
                }
                return true;
            }

            std::vector<std::vector<Integer> > is(comm.size());
            std::vector<std::vector<Integer> > os(comm.size());

            const auto &work = table.work();
            Integer n = 0;
            for (const auto &w : work) {
                os[w->rank()].push_back(tree.node(w->node())->global_handle());
                os[w->rank()].push_back(w->n_elements());
                n += 2;
            }

            // SynchDescribe(os, comm, logger());

            comm.clear();
            const Integer n_os = static_cast<Integer>(os.size());
            for (Integer r = 0; r < n_os; ++r) {
                if (os[r].empty() || r == comm.rank()) continue;

                is[r].resize(os[r].size());
                comm.i_send(&os[r][0], static_cast<int>(os[r].size()), r, r);
                comm.i_recv(&is[r][0], static_cast<int>(is[r].size()), r, comm.rank());
            }

            comm.wait_all();

            WorkTable::WorkArray work_ptrs;
            std::vector<Integer> weights;
            weights.reserve(n);
            work_ptrs.reserve(n);
            // delegated.reserve(n/2);
            std::vector<WorkTable::WorkArray> delegated(comm.size());

            std::vector<Integer> proc_offset(comm.size(), 0);

            for (const auto &w : work) {
                const Integer r = w->rank();
                const Integer i = proc_offset[r];

                Integer weight = 0;
                if (r == comm.rank()) {
                    weight = cost(w->n_elements(), w->n_elements());
                } else {
                    const auto &in = is[r];
                    const auto &out = os[r];
                    // const Integer nodeId = in[i];
                    weight = cost(in[i + 1], out[i + 1]);
                }

                if (weight != 0) {
                    assert(r == w->rank());
                    if (r >= comm.rank()) {
                        weights.push_back(weight);
                        work_ptrs.push_back(w);
                    } else {
                        delegated[r].push_back(w);
                    }
                }
                proc_offset[r] += 2;
            }

            is.clear();
            os.clear();

            // SynchDescribe(weights, comm, logger());
            // SynchDescribe(globalIds, comm, logger());

            const Integer n_weights = static_cast<Integer>(weights.size());
            std::vector<Integer> cum_sum(n_weights);
            comm.v_cum_sum(&weights[0], &cum_sum[0], n_weights);

            Integer total = 0;
            if (!cum_sum.empty()) {
                total = cum_sum.back();
            }

            if (total < 0) {
                std::cerr << "[Error] Integer overflow" << std::endl;
            }

            comm.all_reduce(&total, 1, MPIMax());

            const Integer average = static_cast<Integer>(total / Real(comm.size()));
            const Integer modulo = total % comm.size();

            std::vector<Integer> node_2_proc;
            Integer offset = 0;
            if (!weights.empty()) {
                node_2_proc.resize(cum_sum.size(), comm.rank());

                // Integer offset = 0;
                for (Integer r = 0; r < comm.size(); ++r) {
                    const Integer p = r + 1;

                    Integer lowbo = 0;
                    Integer upbo = 0;
                    if (p <= modulo) {
                        lowbo = (p - 1) * (average + 1);
                        upbo = p * (average + 1);
                    } else {
                        lowbo = (p - 1) * average + modulo;
                        upbo = p * average + modulo;
                    }

                    const Integer n2p_size = static_cast<Integer>(node_2_proc.size());
                    for (; offset < n2p_size; ++offset) {
                        if (lowbo <= cum_sum[offset] && cum_sum[offset] <= upbo) {
                            node_2_proc[offset] = r;
                        } else  // if( cum_sum[offset] > upbo)
                        {
                            break;
                        }
                    }
                }
            }

            const static int TUPLE_SIZE = 3;

            os.resize(comm.size());

            const Integer n2p_size = static_cast<Integer>(node_2_proc.size());
            for (Integer i = 0; i < n2p_size; ++i) {
                auto w = work_ptrs[i];
                const Integer nodeId = tree.node(w->node())->global_handle();
                const Integer rank = w->rank();
                const Integer worker = node_2_proc[i];
                const Integer weight = weights[i];

                w->set_worker(worker);
                w->set_weight(weight);

                if (rank == comm.rank()) continue;

                // TUPLE_SIZE
                os[rank].push_back(nodeId);
                os[rank].push_back(worker);
                os[rank].push_back(weight);
            }

            for (Integer i = 0; i < comm.size(); ++i) {
                if (os[i].empty()) continue;
                comm.i_send(&os[i][0], static_cast<int>(os[i].size()), i, i);
            }

            std::vector<Integer> incoming(comm.size(), 0);

            for (Integer r = 0; r < comm.size(); ++r) {
                auto &delegated_to_r = delegated[r];

                const Integer dtr_size = static_cast<Integer>(delegated_to_r.size());
                for (Integer i = 0; i < dtr_size; ++i) {
                    auto w = delegated_to_r[i];
                    const Integer rank = w->rank();
                    incoming[rank] += TUPLE_SIZE;
                }
            }

            is.resize(comm.size());

            const Integer i_size = static_cast<Integer>(incoming.size());
            for (Integer i = 0; i < i_size; ++i) {
                if (!incoming[i]) continue;

                is[i].resize(incoming[i]);
                comm.i_recv(&is[i][0], incoming[i], i, comm.rank());
            }

            /////////////////////////////////////////////////////////////////////////
            table.work() = std::move(work_ptrs);
            /////////////////////////////////////////////////////////////////////////

            comm.wait_all_recv();

            const Integer is_size = static_cast<Integer>(is.size());
            for (Integer r = 0; r < is_size; ++r) {
                if (is[r].empty()) continue;

                auto &input = is[r];
                auto &delegated_to_r = delegated[r];

                Integer delegated_index = 0;

                const Integer i_size = static_cast<Integer>(input.size());
                for (Integer k = 0; k < i_size; k += TUPLE_SIZE) {
                    // const Integer nodeId = input[k];
                    const Integer worker = input[k + 1];
                    const Integer weight = input[k + 2];
                    auto w = delegated_to_r[delegated_index];

                    assert(tree.node(w->node())->global_handle() == input[k]);  // nodeId = input[k]
                    assert(w->rank() == r);

                    w->set_worker(worker);
                    w->set_weight(weight);

                    delegated_index++;

                    table.work().push_back(w);
                }
            }

            comm.wait_all_send();
            // SynchDescribe(node_2_proc, comm, logger());
            // table.synchDescribe(comm, logger());
            return false;
        }

        LoadBalancer() : use_linear_(false) {}

    private:
        bool use_linear_;
    };
}  // namespace moonolith

#endif  // MOONOLITH_LOAD_BALANCER_HPP
