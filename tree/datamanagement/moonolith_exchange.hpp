#ifndef MOONOLITH_EXCHANGE_HPP
#define MOONOLITH_EXCHANGE_HPP

#include "moonolith_communicator.hpp"
#include "moonolith_data_handle.hpp"
#include "moonolith_input_stream.hpp"
#include "moonolith_lookup_table.hpp"
#include "moonolith_output_stream.hpp"

#include <memory>
#include <set>

namespace moonolith {

    template <class Tree>
    class Exchange {
    public:
        typedef typename Tree::Traits Traits;
        typedef typename Tree::Node Node;
        typedef typename Traits::DataType DataType;

        Exchange(const std::shared_ptr<LookUpTable> &table, const Communicator &comm)
            : comm_(comm), lookup_table_(table) {}

        template <class Resources>
        bool apply(const Tree &tree, Resources &resources) {
            std::vector<ByteInputBuffer> recv_buffers(comm_.size());
            std::vector<ByteOutputBuffer> send_buffers(comm_.size());

            for (std::size_t d = 0; d < lookup_table_->data_2_proc().size(); ++d) {
                for (auto r : lookup_table_->data_2_proc()[d]) {
                    if (r == comm_.rank()) continue;

                    ByteOutputBuffer &os = send_buffers[r];
                    os << tree.data(DataHandle(d));
                }
            }

            const Integer n_connections = comm_.i_send_recv_all(send_buffers, recv_buffers);
            resources.resize(comm_.size());

            for (Integer i = 0; i < n_connections; ++i) {
                Integer rank, index;
                while (!comm_.test_recv_any(&rank, &index)) {
                }
                ByteInputStream &is = recv_buffers[rank];

                while (is.good()) {
                    DataType d;
                    is >> d;
                    resources[rank].push_back(std::move(d));
                }
            }

            comm_.wait_all();
            return true;
        }

        template <class LocalResources, class RemoteResources>
        bool apply(const Tree &, const LocalResources &local_resources, RemoteResources &remote_resources) {
            std::vector<ByteInputBuffer> recv_buffers(comm_.size());
            std::vector<ByteOutputBuffer> send_buffers(comm_.size());

            for (Integer d = 0; d < lookup_table_->data_2_proc().size(); ++d) {
                for (auto r : lookup_table_->data_2_proc()[d]) {
                    if (r == comm_.rank()) continue;

                    ByteOutputBuffer &os = send_buffers[r];
                    os << local_resources[d];
                }
            }

            const Integer n_connections = comm_.i_send_recv_all(send_buffers, recv_buffers);
            remote_resources.resize(comm_.size());

            for (Integer i = 0; i < n_connections; ++i) {
                Integer rank, index;
                while (!comm_.test_recv_any(&rank, &index)) {
                }
                ByteInputStream &is = recv_buffers[rank];

                while (is.good()) {
                    typename RemoteResources::value_type::value_type d;
                    is >> d;
                    remote_resources[rank].push_back(std::move(d));
                }
            }

            comm_.wait_all();
            return true;
        }

    private:
        Communicator comm_;
        std::shared_ptr<LookUpTable> lookup_table_;
    };

}  // namespace moonolith

#endif  // MOONOLITH_EXCHANGE_HPP
