#ifndef FE_INTERFACES_3_CL


//Types
#include "fe_object.cl"


#ifndef CLIPP_HOST_CL
//Forward declarations of types

////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// general /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

void iso_parametric_transform_3(const FEObject *fe, const Scalar * points, Scalar * global_quad_points);
void iso_parametric_jacobian_3(const FEObject *fe, const Scalar * points, Scalar * jacobians);
static void make_inverse_affine_transform_3(const Scalar *A, const Scalar *b, Scalar *Ainv, Scalar *binv);
static void apply_affine_transform_3(const Scalar *A, const Scalar *b, const SizeType n_points, const Scalar *in, Scalar *out);
////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// tetrahedron /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

void tetrahedron_jacobian(const Scalar * points, const SizeType n_quad_points, const Scalar * quad_points, Scalar * jacobians);
void tetrahedron_linear_shape_fun(const SizeType number, const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);
void tetrahedron_linear_fun_0(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);
void tetrahedron_linear_fun_1(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);
void tetrahedron_linear_fun_2(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);
void tetrahedron_linear_fun_3(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);
static void tetrahedron_transform(const Scalar * points, const SizeType n_quad_points, const Scalar * quad_points, Scalar * global_quad_points);
void tetrahedron_inverse_transform(const Scalar * points, const SizeType n_quad_points, const Scalar * global_quad_points, Scalar * quad_points);
void tetrahedron_make_fe_object(const Scalar *points, const SizeType n_quad_points, const Scalar * quad_points, FEObject *object);


void tetrahedron_linear_grad_0(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads);
void tetrahedron_linear_grad_1(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads);
void tetrahedron_linear_grad_2(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads);
void tetrahedron_linear_grad_3(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads);
bool ref_tetrahedron_contains_all_points(const SizeType n_quad_points, const Scalar * quad_points);

////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// hexahedron /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

void hex_transform(const Scalar * points, const SizeType n_quad_points, const Scalar * quad_points, Scalar * global_quad_points);
void hex_inverse_transform(const Scalar * points, const SizeType n_quad_points, const Scalar * global_quad_points, Scalar * quad_points);
void hex_transform_jacobian(const Scalar * points, const SizeType n_quad_points, const Scalar * quad_points, Scalar * jacobians);

//needs the basis functions to be initialized
void hex_iso_parametric_transform(const FEObject *object, const Scalar * points, Scalar * global_quad_points);
void hex_iso_parametric_jacobian(const FEObject *object, const Scalar * points,  Scalar * jacobians);


void hex_make_fe_object(const Scalar *points, const SizeType n_quad_points, const Scalar * quad_points, FEObject *object);

//Shape functions
void hex_trilinear_fun_0(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);
void hex_trilinear_fun_1(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);
void hex_trilinear_fun_2(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);
void hex_trilinear_fun_3(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);
void hex_trilinear_fun_4(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);
void hex_trilinear_fun_5(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);
void hex_trilinear_fun_6(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);
void hex_trilinear_fun_7(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);


void hex_trilinear_grad_0(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads);
void hex_trilinear_grad_1(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads);
void hex_trilinear_grad_2(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads);
void hex_trilinear_grad_3(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads);
void hex_trilinear_grad_4(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads);
void hex_trilinear_grad_5(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads);
void hex_trilinear_grad_6(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads);
void hex_trilinear_grad_7(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads);

void trafo_from_tet_side_to_ref_triangle(
	const SizeType * local_index, 
	Scalar *A,
	Scalar *b);

#endif //CLIPP_HOST_CL
#endif //FE_INTERFACES_3_CL
