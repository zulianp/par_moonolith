#ifndef MOONOLITH_DUAL_ELEM_CHECKER_HPP
#define MOONOLITH_DUAL_ELEM_CHECKER_HPP

#include "moonolith_base.hpp"

#include "moonolith_assembly.hpp"
#include "moonolith_elem.hpp"
#include "moonolith_elem_dual.hpp"
#include "moonolith_empirical_tol.hpp"
#include "moonolith_gauss_quadrature_rule.hpp"
#include "moonolith_geo_algebra.hpp"
#include "moonolith_transformed_fe.hpp"

#include <cassert>
#include <limits>

namespace moonolith {

    template <class Elem>
    class DualElemChecker {
    public:
        static const int Dim = Elem::Dim;
        static const int PhysicalDim = Elem::PhysicalDim;
        static const std::size_t NNodes = Elem::NNodes;
        static const int Order = Elem::Order;

        static_assert(Dim == PhysicalDim, "check only done for Dim == Codim");

        using T = typename Elem::T;
        using Point = typename Elem::Point;
        using Vector = typename Elem::Vector;
        using CoPoint = typename Elem::CoPoint;

        static bool check(const T tol = GeometricTol<T>::value()) {
            auto ref_measure = Reference<Elem>::measure();

            Elem elem;
            elem.make_reference();

            Quadrature<T, Dim> q_rule;
            GaussQRule<Elem>::get(Order + Order, q_rule);

            FE<Elem> fe;
            fe.init(elem, q_rule, true, false, true);

            Dual<Elem> dual_fe;
            TransformedFE<Elem> trafo_fe;

            dual_fe.init(fe);
            trafo_fe.init(fe);

            Matrix<T, NNodes, NNodes> coupling_mat, mass_mat, inv_mass_mat;

            assemble_mass_matrix(fe, dual_fe, coupling_mat);
            assemble_mass_matrix(trafo_fe, dual_fe, mass_mat);

            auto mm_sum = mass_mat.sum();
            auto cm_sum = coupling_mat.sum();
            auto sum_trafo = trafo_fe.transformation().sum();

            moonolith_test_assert(approxeq(cm_sum, ref_measure, tol));
            moonolith_test_assert(approxeq(mm_sum, ref_measure, tol));
            moonolith_test_assert(approxeq(sum_trafo, static_cast<T>(NNodes), tol));

            inv_mass_mat.zero();
            for (std::size_t i = 0; i < NNodes; ++i) {
                inv_mass_mat(i, i) = static_cast<T>(1.) / mass_mat(i, i);
            }

            auto trafo = trafo_fe.transformation();
            make_transpose(trafo);
            auto transfer = trafo * inv_mass_mat * coupling_mat;

            moonolith::Vector<T, NNodes> sum_T;

            transfer.sum_cols(sum_T);

            for (std::size_t i = 0; i < NNodes; ++i) {
                bool ok = approxeq(sum_T[i], static_cast<T>(1.0), CheckTol<T>::half_value());
                if (!ok) {
                    std::cout << "---------------------------\n";
                    coupling_mat.describe(std::cout);
                    std::cout << "---------------------------\n";
                    mass_mat.describe(std::cout);
                    std::cout << "---------------------------\n";
                    trafo.describe(std::cout);
                    std::cout << "---------------------------\n";
                    transfer.describe(std::cout);
                    std::cout << "---------------------------\n";

                    moonolith_test_assert(false);
                    return false;
                }
            }

            for (std::size_t i = 0; i < NNodes; ++i) {
                for (std::size_t j = 0; j < NNodes; ++j) {
                    auto val = transfer(i, j);

                    bool ok = true;
                    if (i == j) {
                        ok = approxeq(val, static_cast<T>(1.0), CheckTol<T>::half_value());
                    } else {
                        ok = approxeq(val, static_cast<T>(0.0), CheckTol<T>::half_value());
                    }

                    if (!ok) {
                        std::cout << "---------------------------\n";
                        transfer.describe(std::cout);
                        std::cout << "---------------------------\n";
                        moonolith_test_assert(false);
                        return false;
                    }
                }
            }

            return true;
        }
    };

}  // namespace moonolith

#endif  // MOONOLITH_DUAL_ELEM_CHECKER_HPP
