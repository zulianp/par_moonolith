#ifndef MOONOLITH_POLYGONAL_ELEMENT_HPP
#define MOONOLITH_POLYGONAL_ELEMENT_HPP

#include "moonolith_elem.hpp"
#include "moonolith_polygon.hpp"

namespace moonolith {

    template <typename T, int PhysicalDim>
    class PolygonalElement : public Elem<T, 2, PhysicalDim> {
    public:
        using Polygon = moonolith::Polygon<T, PhysicalDim>;
        virtual ~PolygonalElement() {}
        virtual void make(Polygon &poly) const = 0;
    };

    // template<typename T, int Dim>
    // inline void make(const PolygonalElement<T, Dim> &elem, Polygon<T, Dim> &poly)
    // {
    //     elem.make(poly);
    // }

    template <class Elem>
    class MakePolygon {
    public:
        template <typename T, int Dim, class Any>
        static void apply(const PolygonalElement<T, Dim> &e, Any &poly) {
            static_assert(Any::Point::size() == Dim, "Must have same physical dimension");
            e.make(poly);
        }
    };

    template <typename T, int Dim>
    class MakePolygon<PolygonalElement<T, Dim>> {
    public:
        template <class Any>
        static void apply(const PolygonalElement<T, Dim> &e, Any &poly) {
            static_assert(Any::Point::size() == Dim, "Must have same physical dimension");
            e.make(poly);
        }
    };

    template <typename T, int Dim>
    class MakePolygon<Elem<T, 2, Dim>> {
    public:
        template <class Any>
        static void apply(const Elem<T, 2, Dim> &e, Any &poly) {
            static_assert(Any::Point::size() == Dim, "Must have same physical dimension");

            auto e_ptr = dynamic_cast<const PolygonalElement<T, Dim> *>(&e);
            if (e_ptr) {
                e_ptr->make(poly);
            }
        }
    };

    template <class Elem>
    inline void make(const Elem &elem, Polygon<typename Elem::T, Elem::PhysicalDim> &poly) {
        static_assert(Elem::Dim == 2, "only for area elements");
        MakePolygon<Elem>::apply(elem, poly);
    }

}  // namespace moonolith

#endif  // MOONOLITH_POLYGONAL_ELEMENT_HPP
