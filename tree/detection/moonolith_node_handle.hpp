#ifndef MOONOLITH_NODE_HANDLE_HPP
#define MOONOLITH_NODE_HANDLE_HPP

#include "moonolith_check_stream.hpp"
#include "moonolith_forward_declarations.hpp"
#include "moonolith_serializable.hpp"
#include "moonolith_input_stream.hpp"
#include "moonolith_output_stream.hpp"

namespace moonolith {
	class NodeHandle : public Serializable {
	public:
		Integer _id;

		NodeHandle(const Integer id = null_id())
		: _id(id)
		{}

		operator Integer() const
		{
			return _id;
		}

		inline static Integer null_id() 
		{
			return -1;
		}

		bool is_null() const 
		{
			return _id == null_id();
		}

		void read(InputStream &is) override
		{
			CHECK_STREAM_READ_BEGIN("NodeHandle", is);
			is >> _id;
			CHECK_STREAM_READ_END("NodeHandle", is);
		}

		void write(OutputStream &os) const override
		{
			CHECK_STREAM_WRITE_BEGIN("NodeHandle", os);
			os << _id;
			CHECK_STREAM_WRITE_END("NodeHandle", os);
		}

		inline Integer id() const
		{
			return _id;
		}
	};
}

#endif //CUTLIBPP_NODE_HANDLE_H
