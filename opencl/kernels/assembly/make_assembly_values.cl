#ifndef MAKE_ASSEMBLY_VALUES_CL
#define MAKE_ASSEMBLY_VALUES_CL

#include "fe/fe_interfaces.cl"

#include "quad_meta.cl"
#include "quadrature.cl"

m_kernel__ void make_assembly_values(
							//input

							//A) basic data
							SizeType n_dims,						//0)
							SizeType n_elements,					//1)

							//B) Mesh
							m_global__ SizeType * el_ptr, 			//2) //n_elements+1
						    m_global__ SizeType * el_index, 			//3)
						    m_global__ SizeType * el_type,			//4)

						    //C) point data
						    SizeType n_nodes,						//5)
						    m_global__ Scalar	* points,				//6) N_DIMS x n_nodes

						    //D) quadrature meta-data
						    m_global__ SizeType * quad_meta, 			//7) (n_elements) x 3 [order, n_shape_fun_offset, n_quad_points_offset, element_matrix_offset]

						    //E) output geomtry
							m_global__ Scalar   * jacobians,			//8)
							m_global__ Scalar   * global_points,      //9)
							m_global__ Scalar   * dxs,				//10)

							//F) output basis functions
						    m_global__ Scalar   *	functions,			//11) 
						    m_global__ Scalar   * gradients			//12)
)
{
	Mesh mesh;
	mesh.n_dims 	= n_dims;
	mesh.n_elements = n_elements;
	mesh.el_ptr 	= el_ptr;
	mesh.el_index 	= el_index;
	mesh.el_type 	= el_type;
	mesh.n_nodes 	= n_nodes;
	mesh.points 	= points;

	// mesh_print(&mesh);


	const SizeType n_dims_2 = n_dims * n_dims;


	FEObject fe;
	FEOBJECT_DEBUG(&fe);

	for(SizeType i = get_global_id(0); i < n_elements; i+= get_global_size(0)) {
		const SizeType i_meta = i * QUAD_META_SIZE;
		const SizeType order = quad_meta[i_meta];

		const SizeType n_shape_fun_offset   = quad_meta[i_meta + 1];
		const SizeType n_quad_points_offset = quad_meta[i_meta + 2];

		KERNEL_ASSERT(n_shape_fun_offset   < mesh.n_nodes * LARGE_INTEGER,    "n_shape_fun_offset is not bounded");
		KERNEL_ASSERT(n_quad_points_offset < mesh.n_elements * LARGE_INTEGER, "n_quad_points_offset is not bounded");

		
		make_fe_object(mesh, i, order, &fe);
		// fe_object_print(&fe);

		const SizeType index_point 	   = n_quad_points_offset * n_dims;
		const SizeType index_jacobian  = n_quad_points_offset * n_dims_2;

		const SizeType n_jac_entries   = n_dims_2   * fe.n_quad_points;
		const SizeType n_point_entries = n_dims  	* fe.n_quad_points;

		generic_copy(n_jac_entries,    fe.jacobian, &jacobians[index_jacobian]);
		generic_copy(n_point_entries,  fe.point,  	&global_points[index_point]);
		generic_copy(fe.n_quad_points, fe.dx,    	&dxs[n_quad_points_offset]);

		SizeType index_fun  = n_shape_fun_offset;
		SizeType index_grad = n_shape_fun_offset * n_dims;
		
		for(SizeType j = 0; j < fe.n_shape_functions; ++j) {
			for(SizeType q = 0; q < fe.n_quad_points; ++q) {
				const SizeType q_grad = q * n_dims;
				
				functions[index_fun++] = fe.fun[j][q];
				
				for(SizeType d = 0; d < n_dims; ++d) {
					gradients[index_grad++] = fe.grad[j][q_grad+d];
				}
			}
		}
	}
}

#endif //MAKE_ASSEMBLY_VALUES_CL
