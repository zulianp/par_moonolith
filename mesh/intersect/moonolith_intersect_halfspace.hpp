#ifndef MOONOLITH_INTERSECT_HALFSPACE_HPP
#define MOONOLITH_INTERSECT_HALFSPACE_HPP

#include "moonolith_halfspace.hpp"
#include "moonolith_line.hpp"
#include "moonolith_plane.hpp"
#include "moonolith_intersect_plane.hpp"
#include <cassert>

namespace moonolith {

	template<typename T, int Dim>
	class IntersectLineWithHalfSpace {
	public:
		using HalfSpace = moonolith::HalfSpace<T, Dim>;
		using Line      = moonolith::Line<T, Dim>;
		using Plane     = moonolith::Plane<T, Dim>;
		using Vector    = typename HalfSpace::Vector;

		static const short NO_INTERSECTION = 0;
		static const short SEGMENT_FROM_POINT_0 = 1;
		static const short SEGMENT_FROM_POINT_1 = 2;
		static const short INSIDE          = 3;
		static const short ON_BOUNDARY     = 4;


	private:
		using IsectLinePlane = moonolith::IntersectLineWithPlane<T, Dim>;

	public:

		short apply(const Line &line, const HalfSpace &half_space, Line &result, const T &tol = 1e-10);

	private:
		IsectLinePlane isect_line_plane;
	};

}

#endif //MOONOLITH_INTERSECT_HALFSPACE_HPP
