#ifndef MOONOLITH_ELEM_HPP
#define MOONOLITH_ELEM_HPP

#include "moonolith_affine_transform.hpp"
#include "moonolith_elem_type.hpp"
#include "moonolith_expanding_array.hpp"
#include "moonolith_map_quadrature.hpp"
#include "moonolith_vector.hpp"
#include "par_moonolith_config.hpp"

#include <array>
#include <functional>

namespace moonolith {

    template <typename T_, int PhysicalDim_>
    class PhysicalElem {
    public:
        using T = T_;

        // dyanmic number of nodes
        static const int NNodes = -1;
        static const int PhysicalDim = PhysicalDim_;

        using CoPoint = moonolith::Vector<T, PhysicalDim>;

        virtual ~PhysicalElem() {}

        virtual const CoPoint &node(const Integer i) const = 0;
        virtual CoPoint &node(const Integer i) = 0;
        virtual int order() const = 0;

        virtual bool is_affine() const = 0;
        virtual bool is_simplex() const = 0;
        virtual int n_nodes() const = 0;
        virtual ElemType type() const = 0;
        virtual T measure() const = 0;
        virtual T approx_measure() const { return measure(); }
        virtual int dim() const = 0;
        inline int codim() const { return PhysicalDim; }
    };

    template <typename T_, int Dim_, int PhysicalDim_>
    class Elem : public PhysicalElem<T_, PhysicalDim_> {
    public:
        using T = T_;

        // dyanmic number of nodes
        static const int NNodes = -1;

        static const int Dim = Dim_;
        static const int PhysicalDim = PhysicalDim_;

        using Vector = moonolith::Vector<T, Dim>;
        using CoVector = moonolith::Vector<T, PhysicalDim>;

        using Point = moonolith::Vector<T, Dim>;
        using CoPoint = moonolith::Vector<T, PhysicalDim>;

        virtual ~Elem() {}

        virtual T fun(const Integer i, const Point &p) const = 0;
        virtual void grad(const Integer i, const Point &p, Vector &g) const = 0;
        virtual void jacobian(const Point &p, std::array<T, PhysicalDim * Dim> &J) const = 0;
        virtual void affine_approx_jacobian(std::array<T, PhysicalDim * Dim> &J) const = 0;
        virtual void point(const Point &p, CoPoint &q) const = 0;

        inline int dim() const override { return Dim_; }

        virtual T reference_measure() const = 0;

        // virtual T reference_measure() const = 0;
    };

    template <class Elem>
    class Reference {};

    template <class Elem>
    class Affine {
    public:
        static const int Dim = Elem::Dim;
        static const int PhysicalDim = Elem::PhysicalDim;
        static const int NNodes = Elem::NNodes;

        using T = typename Elem::T;

        using Vector = moonolith::Vector<T, Dim>;
        using CoVector = moonolith::Vector<T, PhysicalDim>;

        using Point = moonolith::Vector<T, Dim>;
        using CoPoint = moonolith::Vector<T, PhysicalDim>;

        inline static void jacobian(const Elem &elem, std::array<T, PhysicalDim * Dim> &J) {
            const auto &p0 = elem.node(0);

            for (int i = 0; i < PhysicalDim; ++i) {
                const int i_offset = i * Dim;

                for (int j = 1; j < Dim + 1; ++j) {
                    J[i_offset + j - 1] = elem.node(j)[i] - p0[i];
                }
            }

            // jacobian(elem.nodes(), J);
        }

        template <class CoPointT>
        inline static void jacobian(const std::array<CoPointT, Dim + 1> &points, std::array<T, PhysicalDim * Dim> &J) {
            const auto &p0 = points[0];

            for (int i = 0; i < PhysicalDim; ++i) {
                const int i_offset = i * Dim;

                for (int j = 1; j < Dim + 1; ++j) {
                    J[i_offset + j - 1] = points[j][i] - p0[i];
                }
            }

            // jacobian(elem.nodes(), J);
        }

        template <class CoPointT, std::size_t Size>
        inline static void jacobian(const std::array<CoPointT, Size> &points,
                                    const std::array<int, Dim + 1> &node_idx,
                                    std::array<T, PhysicalDim * Dim> &J) {
            const auto &p0 = points[node_idx[0]];

            for (int i = 0; i < PhysicalDim; ++i) {
                const int i_offset = i * Dim;

                for (int j = 1; j < Dim + 1; ++j) {
                    J[i_offset + j - 1] = points[node_idx[j]][i] - p0[i];
                }
            }
        }

        template <class CoPointT, class PointT, std::size_t Size>
        inline static void point(const std::array<CoPointT, Size> &points, const PointT &in, CoPointT &out) {
            const auto &p0 = points[0];
            out = p0;

            for (int i = 0; i < Dim; ++i) {
                out += in[i] * (points[i + 1] - p0);
            }
        }

        template <class CoPointT, class PointT, std::size_t Size>
        inline static void point(const std::array<CoPointT, Size> &points,
                                 const std::array<int, Dim + 1> &node_idx,
                                 const PointT &in,
                                 CoPointT &out) {
            const auto &p0 = points[node_idx[0]];
            out = p0;

            for (int i = 0; i < Dim; ++i) {
                out += in[i] * (points[node_idx[i + 1]] - p0);
            }
        }

        inline static void jacobian(const Elem &elem,
                                    const std::array<int, Dim + 1> &node_idx,
                                    std::array<T, PhysicalDim * Dim> &J) {
            // const auto &p0 = elem.node(node_idx[0]);

            // for(int i = 0; i < PhysicalDim; ++i) {
            //     const int i_offset = i * Dim;

            //     for(int j = 1; j < Dim + 1; ++j) {
            //         J[i_offset + j-1] = elem.node(node_idx[j])[i] - p0[i];
            //     }
            // }

            jacobian(elem.nodes(), node_idx, J);
        }

        inline static void make(const Elem &elem, AffineTransform<T, Dim, PhysicalDim> &trafo) {
            elem.affine_approx_jacobian(trafo.mat);
            trafo.b = elem.node(0);
            init_inverse(trafo);
        }
    };

    template <class Elem>
    inline void make_transform(Elem &elem, AffineTransform<typename Elem::T, Elem::Dim, Elem::PhysicalDim> &trafo) {
        Affine<Elem>::make(elem, trafo);
    }

    template <class Elem>
    class IsoParametric {
    public:
        static const int Dim = Elem::Dim;
        static const int PhysicalDim = Elem::PhysicalDim;
        static const int NNodes = Elem::NNodes;

        using T = typename Elem::T;

        using Vector = moonolith::Vector<T, Dim>;
        using CoVector = moonolith::Vector<T, PhysicalDim>;

        using Point = moonolith::Vector<T, Dim>;
        using CoPoint = moonolith::Vector<T, PhysicalDim>;

        inline static void jacobian(const Elem &elem, const Point &p, std::array<T, PhysicalDim * Dim> &J) {
            std::fill(std::begin(J), std::end(J), 0.);

            Vector g;
            for (int k = 0; k < NNodes; ++k) {
                elem.grad(k, p, g);

                for (int i = 0; i < PhysicalDim; ++i) {
                    const int i_offset = i * Dim;

                    for (int j = 0; j < Dim; ++j) {
                        const int idx = i_offset + j;
                        J[idx] += elem.node(k)[i] * g[j];
                    }
                }
            }
        }

        // c := dot(nodes, dir)
        inline static void directional_second_derivative(const Elem &elem,
                                                         const Point &p,
                                                         const std::array<T, NNodes> &c,
                                                         std::array<T, Dim * Dim> &H) {
            std::fill(std::begin(H), std::end(H), 0.0);

            std::array<T, Dim * Dim> hessian;

            for (int i = 0; i < NNodes; ++i) {
                elem.hessian(i, p, hessian);

                for (int k = 0; k < Dim; ++k) {
                    const int k_offset = k * Dim;

                    for (int l = 0; l < Dim; ++l) {
                        const int kl_offset = k_offset + l;
                        H[kl_offset] += c[i] * hessian[k_offset];
                    }
                }
            }
        }

        inline static void directional_second_derivative(const Elem &elem,
                                                         const Point &p,
                                                         const CoVector &dir,
                                                         std::array<T, Dim * Dim> &H) {
            std::array<T, NNodes> c;

            for (int i = 0; i < NNodes; ++i) {
                c[i] = dot(dir, elem.node(i));
            }

            directional_second_derivative(elem, p, c, H);
        }

        inline static void point(const Elem &elem, const Point &p, CoPoint &q) {
            q = elem.node(0);
            q *= elem.fun(0, p);

            for (int i = 1; i < NNodes; ++i) {
                q += elem.fun(i, p) * elem.node(i);
            }
        }

        inline static T measure(const Elem &elem, const Quadrature<T, Dim> &q) {
            Matrix<T, PhysicalDim, Dim> J;

            T meas = 0.0;
            const Integer n_qp = q.n_points();
            for (Integer i = 0; i < n_qp; ++i) {
                elem.jacobian(q.point(i), J.values);
                meas += moonolith::measure(J) * q.weights[i];
            }

            return Reference<Elem>::measure() * meas;
        }
    };

    template <typename T, int Dim, int PhysicalDim>
    class Reference<Elem<T, Dim, PhysicalDim> > {
    public:
        static T measure() {
            assert(false && "implement me");
            return 0.0;
        }
    };

    template <class ElemT>
    class InvertOrientation {
    public:
        template <typename T, int PhysicalDim>
        inline static void apply(Elem<T, 2, PhysicalDim> &elem) {
            const int n_nodes = elem.n_nodes();
            for (int i = 0; i < n_nodes / 2; ++i) {
                int i_other = n_nodes - 1 - i;
                std::swap(elem.node(i), elem.node(i_other));
            }
        }
    };

    template <class Elem>
    inline void invert_orientation(Elem &elem) {
        InvertOrientation<Elem>::apply(elem);
    }

}  // namespace moonolith

#endif  // MOONOLITH_ELEM_HPP
