#ifndef MOONOLITH_MATRIX_BUFFERS_HPP
#define MOONOLITH_MATRIX_BUFFERS_HPP


#include "par_moonolith_config.hpp"
#include "moonolith_sparse_matrix.hpp"
#include "moonolith_communicator.hpp"
#include "moonolith_incomplete_intersection_remover.hpp"

namespace moonolith {

    template<typename T>
    class MatrixBuffers {
    public:
        MatrixBuffers(Communicator &comm)
        : B(comm), D(comm), Q(comm, false), incomplete_remover(comm)
        {}

        inline MatrixInserter<T> &measure()
        {
            return incomplete_remover.measure;
        }

        MatrixInserter<T> B, D, Q;

        //used to remove incomplete intersections
        IncompleteIntersectionRemover<T> incomplete_remover;
    };

}

#endif //MOONOLITH_MATRIX_BUFFERS_HPP
