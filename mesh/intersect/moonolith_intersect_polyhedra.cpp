#include "moonolith_intersect_polyhedra_impl.hpp"

namespace moonolith {
    template class IntersectPolyhedronWithHalfSpace<Real>;
    template class IntersectConvexPolyhedra<Real>;
    template class IntersectConvexSurfPolyhedra4<Real>;
}  // namespace moonolith
