#ifndef MOONOLITH_BREADTH_FIRST_NAVIGATOR_HPP
#define MOONOLITH_BREADTH_FIRST_NAVIGATOR_HPP

#include "moonolith_forward_declarations.hpp"
#include "moonolith_mutator.hpp"
#include "moonolith_navigator.hpp"
#include "moonolith_navigator_option.hpp"
#include "moonolith_verbose_object.hpp"

#include <deque>

namespace moonolith {

    template <class Tree>
    class BreadthFirstNavigator : public Navigator<Tree>, public VerboseObject {
    private:
        typedef Navigator<Tree> super;

    public:
        typedef typename Tree::Traits Traits;

        typedef moonolith::Node<Traits> Node;
        typedef moonolith::Branch<Traits> Branch;
        typedef moonolith::Root<Traits> Root;
        typedef moonolith::Leaf<Traits> Leaf;
        typedef moonolith::Visitor<Traits> Visitor;
        typedef moonolith::TreeMemory<Traits> TreeMemory;

    private:
        std::deque<NodeHandle> _nodes;
        Visitor *_visitor;
        // Tree * _tree;
    public:
        using Navigator<Tree>::walk;

        bool walk(Tree &tree, Visitor &visitor) override {
            this->_visitor = &visitor;

            if (!tree.is_visitable()) {
                if (!tree.make_visitable()) {
                    // tree is either empty or there is a bug
                    assert(false);
                    return false;
                }
            }

            assert(this->_visitor != nullptr);
            tree.root_accept(*this);
            return true;
        }

        NavigatorOption walk(Node &node, Visitor &visitor, TreeMemory &memory) {
            if (node.is_leaf()) {
                return walk(static_cast<Leaf &>(node), visitor, memory);
            }

            if (node.is_root()) {
                return walk(static_cast<Root &>(node), visitor, memory);
            }

            assert(node.is_branch());

            return walk(static_cast<Branch &>(node), visitor, memory);
        }

        NavigatorOption walk(Branch &branch, Visitor &visitor, TreeMemory &memory) override {
            this->_visitor = &visitor;

            NavigatorOption opt = branch.accept(*this, memory);
            if (CONTINUE == opt) {
                return iterate(memory);
            } else {
                return opt;
            }
        }

        NavigatorOption walk(Leaf &leaf, Visitor &visitor, TreeMemory &memory) override {
            this->_visitor = &visitor;
            return leaf.accept(*this, memory);
        }

        BreadthFirstNavigator() : _nodes(), _visitor(nullptr) {}

        inline bool can_refine(Leaf &leaf, TreeMemory &memory) const {
            std::shared_ptr<Mutator<Traits> > m_can_refine = memory.mutator_factory()->new_can_refine();
            leaf.accept(*m_can_refine, memory);
            return m_can_refine->success();
        }

        NavigatorOption visit(Leaf &leaf, TreeMemory &memory) override {
            NavigatorOption opt = _visitor->visit(leaf, memory);
            if (REFINE == opt) {
                // begin new
                std::shared_ptr<Node> parent = memory.node(leaf.parent());

                std::shared_ptr<Mutator<Traits> > refine = memory.mutator_factory()->new_refine();

                const NodeHandle handle = leaf.handle();

                leaf.accept(*refine, memory);
                std::shared_ptr<Node> refined = memory.node(handle);

                if (refine->success()) {
                    assert(!refined->is_leaf());
                    return refined->accept(*this, memory);
                } else {
                    assert(!can_refine(leaf, memory));
                    return CONTINUE;
                }
                // end new
            }
            return opt;
        }

        NavigatorOption visit(Branch &branch, TreeMemory &memory) override {
            if (!branch.is_root()) {
                switch (_visitor->visit(branch, memory)) {
                    case STOP:
                        return STOP;
                    case SKIP_SUBTREE:
                        return CONTINUE;
                    case SKIP_SAME_LEVEL_NODES: {
                        _nodes.clear();
                        break;
                    }

                    case SKIP_SIBLINGS: {
                        remove_siblings_from_queue(branch, memory);
                        break;
                    }

                    case NODE_SELECTION: {
                        _nodes.clear();
                        _nodes.push_back(branch.child_at(_visitor->selection()));
                        return CONTINUE;
                    }

                    default:
                        break;
                }
            }

            push_next_nodes(branch.begin(), branch.end());

            return CONTINUE;
        }

        template <class NodeIter>
        inline void push_next_nodes(const NodeIter &begin, const NodeIter &end) {
            for (NodeIter it = begin; it != end; ++it) {
                _nodes.push_back(*it);
            }
        }

        NavigatorOption visit(Root &root, TreeMemory &memory) override {
            _nodes.clear();

            NavigatorOption opt = _visitor->visit(root, memory);
            if (!root.is_visitable()) {
                return STOP;
            }

            if (STOP == opt || SKIP_SUBTREE == opt) {
                return STOP;
            }

            opt = visit(static_cast<Branch &>(root), memory);

            if (STOP == opt || SKIP_SUBTREE == opt) {
                return STOP;
            }

            return iterate(memory);
        }

        // ---------------------------------------------------- ITERATE THROUGH THE TREE METHODS
        // --------------------------------------- //

        NavigatorOption iterate(TreeMemory &memory) {
            while (!_nodes.empty()) {
                std::shared_ptr<Node> node = memory.node(_nodes.front());
                _nodes.pop_front();
                const NavigatorOption opt = node->accept(*this, memory);

                switch (opt) {
                    case STOP:
                        return STOP;
                    case SKIP_SAME_LEVEL_NODES:
                        return STOP;
                    case SKIP_SIBLINGS: {
                        remove_siblings_from_queue(*node, memory);
                        break;
                    }

                    default:
                        break;
                }
            }

            return STOP;
        }

        void remove_siblings_from_queue(const Node &node, TreeMemory &memory) {
            while (!_nodes.empty()) {
                std::shared_ptr<Node> temp = memory.node(_nodes.front());
                if (temp->parent().id() == node.parent().id()) {
                    _nodes.pop_front();
                } else {
                    return;
                }
            }
        }
    };

}  // namespace moonolith

#endif  // MOONOLITH_BREADTH_FIRST_NAVIGATOR_HPP
