#ifndef MOONOLITH_POLYGON_IMPL_HPP
#define MOONOLITH_POLYGON_IMPL_HPP

#include "moonolith_make_unique.hpp"
#include "moonolith_polygon.hpp"
#include "moonolith_remove_collinear_points.hpp"

#include <iostream>

namespace moonolith {

    template <typename T>
    T trapezoid_area(const Vector<T, 2> &p1, const Vector<T, 2> &p2, const Vector<T, 2> &p3) {
        const auto d12 = p2 - p1;
        const auto d13 = p3 - p1;
        return (d12.x * d13.y - d13.x * d12.y);
    }

    template <typename T>
    T trapezoid_area(const Vector<T, 3> &p1, const Vector<T, 3> &p2, const Vector<T, 3> &p3) {
        const auto u = p2 - p1;
        const auto v = p3 - p1;
        return length(cross(u, v));
    }

    template <typename T, int Dim>
    typename Polygon<T, Dim>::Point Polygon<T, Dim>::barycenter() const {
        Point b;

        for (const auto &p : points) {
            b += p;
        }

        b /= static_cast<T>(points.size());
        return b;
    }

    template <typename T, int Dim>
    void Polygon<T, Dim>::reverse() {
        using std::swap;

        const Integer n_points = size();
        const Integer half_n = n_points / 2;

        for (Integer i = 0; i < half_n; ++i) {
            const Integer i_inv = (n_points - 1 - i);
            swap((*this)[i], (*this)[i_inv]);
        }
    }

    template <typename T, int Dim>
    T Polygon<T, Dim>::area() const {
        std::size_t n_vertices = this->size();

        if (n_vertices == 3) {
            const auto &p1 = points[0];
            const auto &p2 = points[1];
            const auto &p3 = points[2];
            return 0.5 * trapezoid_area(p1, p2, p3);
        }

        Point b = barycenter();
        Point pi;
        Point pip1;

        T result = 0;
        for (std::size_t i = 0; i < n_vertices; ++i) {
            auto ip1 = (i + 1 == n_vertices) ? 0 : (i + 1);
            result += trapezoid_area(b, points[i], points[ip1]);
        }

        return 0.5 * result;
    }

    template <typename T, int Dim>
    bool Polygon<T, Dim>::check_convexity() {
        std::size_t n_vertices = this->size();

        if (n_vertices == 3) {
            convex = true;
            return true;
        }

        convex = true;

        for (std::size_t i = 1; i < n_vertices; ++i) {
            auto im1 = i - 1;
            auto ip1 = (i + 1 == n_vertices) ? 0 : (i + 1);
            const T a = trapezoid_area(points[im1], points[i], points[ip1]);
            if (a < 0.0) {
                convex = false;
                break;
            }
        }

        return convex;
    }

    template <typename T, int Dim>
    void Polygon<T, Dim>::remove_collinear_points(const T tol) {
        // std::size_t n_vertices = this->size();
        // std::size_t result_n_points = n_vertices;

        // std::vector<bool> remove(n_vertices, false);
        // Vector u, v;
        // for(auto i = 0; i < n_vertices; ++i) {
        // 	auto im1 = (i == 0? n_vertices : i) - 1;
        // 	auto ip1 = (i + 1 == n_vertices) ? 0 : i + 1;

        // 	u = (*this)[i] - (*this)[im1];
        // 	v = (*this)[ip1] - (*this)[i];

        // 	u /= length(u);
        // 	v /= length(v);

        // 	if(approxeq(dot(u, v), 1., tol)) {
        // 		remove[i] = true;
        // 		--result_n_points;
        // 	}
        // }

        // auto range_to_fill_begin = -1;
        // bool must_shift = false;

        // for(auto i = 0; i < n_vertices; ++i) {
        //     if(!remove[i] && must_shift) {
        //         assert(range_to_fill_begin >= 0);

        //         const auto to   = range_to_fill_begin;
        //         const auto from = i;

        //         (*this)[to] = (*this)[from];

        //         remove[i] = true;

        //         if(!remove[++range_to_fill_begin]) {
        //             range_to_fill_begin = -1;
        //             must_shift = false;
        //         }
        //     }

        //     if(remove[i] && !must_shift) {
        //         must_shift = true;
        //         range_to_fill_begin = i;
        //     }
        // }

        // this->resize(result_n_points);

        RemoveCollinearPoints<T, Dim>().apply(*this, tol);
    }

}  // namespace moonolith

#endif  // MOONOLITH_POLYGON_IMPL_HPP
