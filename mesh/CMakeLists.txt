list(
    APPEND
    MESH_MODULES
    .
    intersect
    project
    quadrature
    elements
    contact
    smoothing
    volumetransfer
    assembly
    lowerdimtransfer
    volume2surface
    interpolation
    io
    nocovering
    obstacle
    simple)

scan_directories(${CMAKE_CURRENT_SOURCE_DIR} "${MESH_MODULES}"
                 MOONOLITH_BUILD_INCLUDES MOONOLITH_HEADERS MOONOLITH_SOURCES)

set(MOONOLITH_BUILD_INCLUDES
    ${MOONOLITH_BUILD_INCLUDES}
    PARENT_SCOPE)

set(MOONOLITH_HEADERS
    ${MOONOLITH_HEADERS}
    PARENT_SCOPE)

set(MOONOLITH_SOURCES
    ${MOONOLITH_SOURCES}
    PARENT_SCOPE)
