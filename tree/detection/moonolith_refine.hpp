
#ifndef MOONOLITH_REFINE_HPP
#define MOONOLITH_REFINE_HPP

#include "moonolith_mutator.hpp"

#include <memory>
#include <iostream>
#include <assert.h>

namespace moonolith {

	template <class Tree>
	class Refine : public Mutator<typename Tree::Traits> {
	public:
		typedef typename Tree::Traits Traits;
		typedef typename Traits::DataType DataType;

		typedef moonolith::Node<Traits> Node;
		typedef moonolith::Branch<Traits> Branch;
		typedef moonolith::Root<Traits> Root;
		typedef moonolith::Leaf<Traits> Leaf;
		typedef moonolith::Visitor<Traits> Visitor;
		typedef moonolith::TreeMemory<Traits> TreeMemory;

		Refine()
			: _success(false)
		{}


		inline bool success() const override { return _success; }


		virtual NavigatorOption visit(Leaf &leaf, TreeMemory &memory) override { 
			if(!branch_leaf(leaf, memory)) {
				return SKIP_SUBTREE;
			}

			_success = true;
			return CONTINUE;
		}

		virtual NavigatorOption visit(Branch &branch, TreeMemory &memory) override {
			if(!branch.is_visitable()) {
				if(!make_branch(branch, memory)) {
					std::cerr << "[Warning] not able to make_branch in Refine. SKIP_SUBTREE is returned\n" << std::endl;
					//should never happen
					assert(false);
					return SKIP_SUBTREE;
				}				
			}

			_success = true;
			return SKIP_SUBTREE;
		}

		virtual NavigatorOption visit(Root &root, TreeMemory &memory) override {
			if(!root.is_visitable()) {
				if(!make_root(root, memory)) {
					std::cerr << "[Error] not able to make_root in Refine. STOP is returned\n" << std::endl;
					//should never happen
					assert(false);
					return STOP;
				}
			}

			_success = true;
			return SKIP_SUBTREE;
		}

		bool can_refine(Leaf &leaf, TreeMemory &memory) 
		{
			if(leaf.empty()) return false;
			std::shared_ptr< Mutator<Traits> > m_can_refine = memory.mutator_factory()->new_can_refine();
			leaf.accept(*m_can_refine, memory);
			return m_can_refine->success();
		}

		bool branch_leaf(Leaf &leaf, TreeMemory &memory)
		{
			if(!can_refine(leaf, memory)) {
				return false;
			}
			
			NodeHandle handle = leaf.handle();


			assert(  memory.node( leaf.parent() ) );

			std::shared_ptr<Branch> new_branch = new_branch_from(leaf, memory);
			assert(new_branch);

			if(!new_branch) return false;

			assert( new_branch->level() == leaf.level() );
			leaf.clear();
			memory.set_node(handle, new_branch);
			return true;
		}

		virtual std::shared_ptr<Branch> new_branch_from(Leaf &leaf, TreeMemory &memory) const
		{
			std::shared_ptr<Branch> branch = memory.new_branch_from(leaf);
			bool ok = this->make_branch(*branch, memory);
			assert(ok);
			//subclass must set the globalHandle in make_branch
			assert( !branch->global_handle().is_null() );

			if(!ok) return nullptr;

			ok = insert_all(leaf.data_begin(), leaf.data_end(), *branch, memory);
			
			assert(ok);
			return branch;
		}

		/**
		 * @brief use memory.newLeafFor(branch) to allocate the new leaf and call @a make_leaf yourself on the newly created leaf.
		 * this method requires you to set the globalHandle which has to be uniquely identifiable in all procs. If the node has aliases
		 * all aliases have to have the same global id in the handle
		 */
		virtual bool make_branch(Branch &branch, TreeMemory &memory) const = 0;

		/**
		 * @brief yourself on the newly created leaf in @a make_branch
		 * this method requires you to set the globalHandle which has to be uniquely identifiable in all procs. If the node has aliases
		 * all aliases have to have the same global id in the handle
		 */
		virtual bool make_leaf(Leaf &leaf, TreeMemory &memory) const = 0;

		virtual bool make_root(Root &root, TreeMemory &memory) const
		{
			return this->make_branch(root, memory);
		}

		template<class IteratorT>
		bool insert_all(const IteratorT &begin, const IteratorT &end, Branch &branch, TreeMemory &memory) const
		{
			std::shared_ptr< NodeNavigator<Traits> > nav_ptr = memory.mutator_factory()->new_node_navigator();
			bool success = true;
			for(IteratorT it = begin; it != end; ++it) {
				const DataHandle handle = *it;
				std::shared_ptr< Mutator<Traits> > mutator = memory.mutator_factory()->new_insert(handle, memory.data(handle));
				nav_ptr->walk(branch, *mutator, memory);
				success &= mutator->success();
			}

			return success;
		} 

		inline bool insert(DataType &data, Branch &branch, TreeMemory &memory) {
			std::shared_ptr< NodeNavigator<Traits> > nav_ptr = memory.mutator_factory().new_node_navigator();
			std::shared_ptr< Mutator<Traits> > mutator = memory.mutator_factory().new_insert(data);
			nav_ptr->walk(branch, *mutator, memory);
			return mutator->success();
		}


		virtual std::string name() const override
		{
			return "Insert";
		}

		private:
//			std::shared_ptr<Node> _node;
			bool _success;
	};
} /* moonolith */

#endif // MOONOLITH_REFINE_HPP
