#ifndef MOONOLITH_ELEM_TRI_HPP
#define MOONOLITH_ELEM_TRI_HPP

#include "moonolith_elem.hpp"
#include "moonolith_gauss_quadrature_rule.hpp"
#include "moonolith_polygon_impl.hpp"
#include "moonolith_polygonal_element.hpp"

namespace moonolith {

    template <typename T, int Order_, int PhysicalDim_>
    class Tri : public PolygonalElement<T, PhysicalDim_> {
    public:
        virtual ~Tri() {}
    };

    template <typename T, int Order, int PhysicalDim>
    class GaussQRule<Tri<T, Order, PhysicalDim> > {
    public:
        static bool get(const Integer order, Quadrature2<T> &q) { return Gauss::get(order, q); }
    };

}  // namespace moonolith

#endif  // MOONOLITH_ELEM_TRI_HPP
