#ifndef VON_MISES_STRESS_INTERFACE_CL
#define VON_MISES_STRESS_INTERFACE_CL 

#include "kernel_base.cl"

#ifndef CLIPP_HOST_CL

Scalar von_mises_stress(const SizeType n_dims, const Scalar * stress);
Scalar von_mises_stress_2(const Scalar *stress);
Scalar von_mises_stress_3(const Scalar *stress);
void von_mises_stress_at_quad_index(const SizeType quad_index, const FEObject *fe, const Scalar *stress, Scalar *cumulative_result);

#endif //CLIPP_HOST_CL

#endif //VON_MISES_STRESS_INTERFACE_CL

