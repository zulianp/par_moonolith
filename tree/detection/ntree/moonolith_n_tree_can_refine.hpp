#ifndef MOONOLITH_NTREE_CAN_REFINE_HPP
#define MOONOLITH_NTREE_CAN_REFINE_HPP

#include <assert.h>
#include "moonolith_can_refine.hpp"

namespace moonolith {

    template <class Tree>
    class NTreeCanRefine : public CanRefine<Tree> {
    public:
        typedef typename Tree::Traits Traits;
        typedef moonolith::Leaf<Traits> Leaf;
        typedef moonolith::TreeMemory<Traits> TreeMemory;

        NTreeCanRefine(const Integer max_leaf_size, const Integer max_depth)
            : max_leaf_size_(max_leaf_size), max_depth_(max_depth) {}

        inline bool can_refine(const Leaf &leaf, const TreeMemory &) const {
            assert(leaf.level() <= max_depth_);
            return (static_cast<Integer>(leaf.size()) > max_leaf_size_) && (leaf.level() < max_depth_);
        }

    private:
        Integer max_leaf_size_, max_depth_;
    };
}  // namespace moonolith

#endif  // MOONOLITH_NTREE_CAN_REFINE_HPP
