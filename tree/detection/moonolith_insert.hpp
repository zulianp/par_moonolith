#ifndef MOONOLITH_INSERT_HPP
#define MOONOLITH_INSERT_HPP

#include "moonolith_mutator.hpp"
#include "moonolith_node_handle.hpp"
#include "moonolith_data_handle.hpp"
#include "moonolith_forward_declarations.hpp"

namespace moonolith {

	template <class Tree>
	class Insert : public Mutator< typename Tree::Traits> {
	public:		
		typedef typename Tree::Traits Traits;
		typedef typename Traits::DataType DataType;

		typedef moonolith::Node<Traits> Node;
		typedef moonolith::Branch<Traits> Branch;
		typedef moonolith::Root<Traits> Root;
		typedef moonolith::Leaf<Traits> Leaf;
		typedef moonolith::Visitor<Traits> Visitor;

		typedef moonolith::TreeMemory<Traits> TreeMemory;

		virtual bool match(const Node &node) const = 0;
	
		virtual bool must_insert(Node &, TreeMemory &) {
			return false;
		}

		virtual bool must_insert(Branch &node, TreeMemory &memory) {
			return must_insert(static_cast<Node &>(node), memory);
		}

		virtual bool must_insert(Root &node, TreeMemory &memory) {
			return must_insert(static_cast<Branch &>(node), memory);
		}

		virtual bool must_insert(Leaf &node, TreeMemory &memory) {
			return must_insert(static_cast<Node &>(node), memory);
		}

		//allows to change node properties when data is sucessfully matches this node or in its sub-tree
		virtual void mutate(Node &, TreeMemory &) const {}

		virtual void mutate(Branch &node, TreeMemory &memory) const 
		{
			mutate(static_cast<Node &>(node), memory);
		}

		virtual void mutate(Root &node, TreeMemory &memory) const 
		{
			mutate(static_cast<Branch &>(node), memory);
		}

		virtual void mutate(Leaf &node, TreeMemory &memory) const 
		{
			mutate(static_cast<Node &>(node), memory);
		}

		virtual bool insert(Leaf &node, TreeMemory &memory) 
		{
			if(must_insert(node, memory)) {
				node.insert(object_handle());
				return true;
			}

			return false;
		}

		virtual bool insert(Branch &node, TreeMemory &memory) 
		{
			if(must_insert(node, memory)) {
				node.insert(object_handle());
				return true;
			}

			return false;
		}

		virtual bool insert(Root &node, TreeMemory &memory) 
		{
			if(must_insert(node, memory)) {
				node.insert(object_handle());
				return true;
			}

			return false;
		}

		explicit Insert(const DataHandle &handle, const DataType &obj)
		: handle_(handle), obj_(obj), success_(false)
		{}

		virtual NavigatorOption visit(Leaf &leaf, TreeMemory &memory) { 
			if (match(leaf)) {
				if(insert(leaf, memory) ) {
					success_ = true;
				}

				mutate(leaf, memory);
			}

			return CONTINUE;
		}

		virtual NavigatorOption visit(Branch &branch, TreeMemory &memory) {
			if(!match(branch)) { 
				return SKIP_SUBTREE;
			}

			if(insert(branch, memory) ) {
				success_ = true;
			}

			mutate(branch, memory);
			return CONTINUE;
		}

		virtual NavigatorOption visit(Root &root, TreeMemory &memory) {
			if(!match(root)) { 
				return SKIP_SUBTREE;
			}

			if(insert(root, memory) ) {
				success_ = true;
			}

			mutate(root, memory);
			return CONTINUE;
		}

		inline bool success() const 
		{
			return success_;
		}

		DataType &object()
		{
			return obj_;
		}
		
		DataHandle &object_handle()
		{
			return handle_;
		}
		
		const DataType &object() const
		{
			return obj_;
		}

		void clear()
		{
			handle_ = DataHandle();
			obj_ = DataType();
			success_ = false;
		}

		virtual ~Insert() {}

	private:
		DataHandle handle_;
		DataType obj_;
		bool success_;


	

	};

}
#endif //MOONOLITH_INSERT_HPP
