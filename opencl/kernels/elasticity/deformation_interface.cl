#ifndef DEFORMATION_INTERFACE_CL
#define DEFORMATION_INTERFACE_CL 

#include "fe/fe_function.cl"

typedef struct {
	Scalar grad_determinant;

	Scalar grad[MAX_N_DIMS * MAX_N_DIMS];

	Scalar inverse_grad[MAX_N_DIMS * MAX_N_DIMS];
	Scalar inverse_grad_transposed[MAX_N_DIMS * MAX_N_DIMS];
	
	Scalar cauchy_strain[MAX_N_DIMS * MAX_N_DIMS];
} Deformation;


#ifndef CLIPP_HOST_CL


void make_deformation(const SizeType dims, 
					  const FEFunction *displacement_gradient,
					  Deformation *deformation);

void deformation_print(const SizeType n_dims, Deformation *def);




#endif //CLIPP_HOST_CL
#endif //DEFORMATION_INTERFACE_CL

