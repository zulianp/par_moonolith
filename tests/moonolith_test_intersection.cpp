#include "moonolith_build_quadrature.hpp"
#include "moonolith_convex_decomposition.hpp"
#include "moonolith_elem_hexahedron.hpp"
#include "moonolith_elem_polymorphic.hpp"
#include "moonolith_elem_tetrahedron.hpp"
#include "moonolith_graham_scan.hpp"
#include "moonolith_intersect_polyhedra.hpp"
#include "moonolith_intersect_polyhedra_impl.hpp"
#include "moonolith_matlab_scripter.hpp"

#include <cmath>

#include <fstream>
#include <iostream>
#include <vector>

#include "gtest/gtest.h"

#define MOONOLITH_HAVE_DOUBLE_PRECISION
#define DEFAULT_TOLLERANCE 1e-12

static const int repeat_test = 1;
// static const int repeat_test = 1000000;

using namespace moonolith;

template <typename Int, typename T>
void make_polyhedron_1(Storage<Int> &el_ptr, Storage<Int> &el_index, Storage<Vector<T, 3>> &points) {
    el_ptr.resize(7);
    el_ptr[0] = 0;
    el_ptr[1] = 4;
    el_ptr[2] = 8;
    el_ptr[3] = 12;
    el_ptr[4] = 16;
    el_ptr[5] = 20;
    el_ptr[6] = 24;

    el_index.resize(24);
    el_index[0] = 0;
    el_index[1] = 1;
    el_index[2] = 5;
    el_index[3] = 4;
    el_index[4] = 1;
    el_index[5] = 2;
    el_index[6] = 6;
    el_index[7] = 5;
    el_index[8] = 3;
    el_index[9] = 7;
    el_index[10] = 6;
    el_index[11] = 2;
    el_index[12] = 0;
    el_index[13] = 4;
    el_index[14] = 7;
    el_index[15] = 3;
    el_index[16] = 2;
    el_index[17] = 1;
    el_index[18] = 0;
    el_index[19] = 3;
    el_index[20] = 6;
    el_index[21] = 7;
    el_index[22] = 4;
    el_index[23] = 5;

    points.resize(8);
    points[0] = {-0.30000000000000004, -0.30000000000000004, 0.30000000000000004};
    points[1] = {-0.19999999999999996, -0.30000000000000004, 0.30000000000000004};
    points[2] = {-0.19999999999999996, -0.19999999999999996, 0.30000000000000004};
    points[3] = {-0.30000000000000004, -0.19999999999999996, 0.30000000000000004};
    points[4] = {-0.30000000000000004, -0.30000000000000004, 0.39999999999999991};
    points[5] = {-0.19999999999999996, -0.30000000000000004, 0.39999999999999991};
    points[6] = {-0.19999999999999996, -0.19999999999999996, 0.39999999999999991};
    points[7] = {-0.30000000000000004, -0.19999999999999996, 0.39999999999999991};
}

template <typename Int, typename T>
void make_polyhedron_2(Storage<Int> &el_ptr, Storage<Int> &el_index, Storage<Vector<T, 3>> &points) {
    el_ptr.resize(7);

    el_ptr[0] = 0;
    el_ptr[1] = 4;
    el_ptr[2] = 8;
    el_ptr[3] = 12;
    el_ptr[4] = 16;
    el_ptr[5] = 20;
    el_ptr[6] = 24;

    el_index.resize(24);
    el_index[0] = 0;
    el_index[1] = 1;
    el_index[2] = 5;
    el_index[3] = 4;
    el_index[4] = 1;
    el_index[5] = 2;
    el_index[6] = 6;
    el_index[7] = 5;
    el_index[8] = 3;
    el_index[9] = 7;
    el_index[10] = 6;
    el_index[11] = 2;
    el_index[12] = 0;
    el_index[13] = 4;
    el_index[14] = 7;
    el_index[15] = 3;
    el_index[16] = 2;
    el_index[17] = 1;
    el_index[18] = 0;
    el_index[19] = 3;
    el_index[20] = 6;
    el_index[21] = 7;
    el_index[22] = 4;
    el_index[23] = 5;

    points.resize(8);
    points[0] = {-0.22651513583561636, -0.22651513583561636, 0.29887968534141401};
    points[1] = {-0.15241447521870757, -0.22651513583561642, 0.300986262333859};
    points[2] = {-0.15241447521870757, -0.15241447521870757, 0.30309283932630399};
    points[3] = {-0.22651513583561636, -0.15241447521870757, 0.300986262333859};
    points[4] = {-0.26504358749434054, -0.26504358749434054, 0.35174008807660145};
    points[5] = {-0.17834708691207959, -0.26504358749434054, 0.35421713095038032};
    points[6] = {-0.17834708691207959, -0.17834708691207959, 0.35669417382415919};
    points[7] = {-0.26504358749434054, -0.17834708691207959, 0.35421713095038032};
}

template <typename Int, typename T>
void make_polyhedron_CFD_1(Storage<Int> &el_ptr, Storage<Int> &el_index, Storage<Vector<T, 3>> &points) {
    el_ptr.resize(5);
    el_ptr[0] = 0;
    el_ptr[1] = 3;
    el_ptr[2] = 6;
    el_ptr[3] = 9;
    el_ptr[4] = 12;

    el_index.resize(12);
    el_index[0] = 0;
    el_index[1] = 2;
    el_index[2] = 1;
    el_index[3] = 0;
    el_index[4] = 3;
    el_index[5] = 2;
    el_index[6] = 0;
    el_index[7] = 1;
    el_index[8] = 3;
    el_index[9] = 1;
    el_index[10] = 2;
    el_index[11] = 3;

    points.resize(4);

    points[0] = {0.00033312550663274713, 0.0057999999999999996, -0.0022499999999275131};
    points[1] = {0.0001599548705488868, 0.0057999999999999987, -0.0022499999999275131};
    points[2] = {0.00026792204557377536, 0.0056242253690415387, -0.0022500000000000003};
    points[3] = {0.00025120864947890918, 0.0057999999999999996, -0.0021160408626812247};
}

template <typename Int, typename T>
void make_polyhedron_CFD_2(Storage<Int> &el_ptr, Storage<Int> &el_index, Storage<Vector<T, 3>> &points) {
    el_ptr.resize(5);
    el_ptr[0] = 0;
    el_ptr[1] = 3;
    el_ptr[2] = 6;
    el_ptr[3] = 9;
    el_ptr[4] = 12;

    el_index.resize(12);
    el_index[0] = 0;
    el_index[1] = 2;
    el_index[2] = 1;
    el_index[3] = 0;
    el_index[4] = 3;
    el_index[5] = 2;
    el_index[6] = 0;
    el_index[7] = 1;
    el_index[8] = 3;
    el_index[9] = 1;
    el_index[10] = 2;
    el_index[11] = 3;

    points.resize(4);
    points[0] = {0.00021779422597930769, 0.0057055620540888297, -0.00224994378149572};
    points[1] = {0.0001999999999524784, 0.0058000000000547509, -0.0022500000000000003};
    points[2] = {0.00014747395861539135, 0.0057361321773327584, -0.0022499451441991667};
    points[3] = {0.00015402519583211004, 0.0058000000000506622, -0.0021813382643999096};
}

void test_convex_aux(Storage<Polyhedron<Real>> &polys) {
    // MatlabScripter script;
    // script.close_all();
    for (std::size_t i = 0; i < polys.size(); ++i) {
        const auto &p = polys[i];
        // script.plot(p);
        // script.axis_equal();
        // script.wait_for_button_press();

        auto v = moonolith::measure(p);
        ASSERT_TRUE(v > 0.);

        if (!p.valid()) {
            ASSERT_TRUE(false);
        }
    }

    // script.save("convex_decomp.m");
}

inline void plot_case(const moonolith::Polyhedron<Real> &p1, const moonolith::Polyhedron<Real> &p2) {
    MatlabScripter script;
    script.close_all();
    script.hold_on();
    script.plot(p1, true);
    script.plot(p2, true);
    script.save("bug_isect.m");
}

inline void plot_case(const moonolith::Polyhedron<Real> &p1,
                      const moonolith::Polyhedron<Real> &p2,
                      const moonolith::Polyhedron<Real> &isect) {
    MatlabScripter script;
    script.close_all();
    script.hold_on();
    script.plot(p1, true);
    script.plot(p2, true);
    script.plot(isect, false);
    script.save("bug_isect.m");
}

TEST(IntersectionTest, oop_convex_decomposition_test) {
    using Point = moonolith::Vector<Real, 3>;
    std::array<Point, 10> points;
    Reference<Tet10<Real>>::points(points);

    Storage<Polyhedron<Real>> polys;
    ConvexDecomposition<Real, 3>::decompose_tet10(points, polys);
    test_convex_aux(polys);
}

TEST(IntersectionTest, oop_convex_decomposition_hex_test) {
    Hex1<Real> hex1;
    hex1.make_reference();

    Polyhedron<Real> poly1;
    make(hex1, poly1);

    ASSERT_TRUE(poly1.valid());

    using Point = moonolith::Vector<Real, 3>;
    std::array<Point, 27> points;
    Reference<Hex27<Real>>::points(points);

    Storage<Polyhedron<Real>> polys;
    ConvexDecomposition<Real, 3>::decompose_hex27(points, polys);
    test_convex_aux(polys);
}

TEST(IntersectionTest, oop_intersect_convex_polyhedra_with_halfspace_test) {
    moonolith::Polyhedron<Real> p1, p2, result;
    moonolith::HPolytope<Real, 3> h_poly;

    make_polyhedron_1(p1.el_ptr, p1.el_index, p1.points);
    make_polyhedron_2(p2.el_ptr, p2.el_index, p2.points);

    h_poly.make(p2);

    moonolith::Polyhedron<Real> in = p1;

    // MatlabScripter script;
    // script.close_all();
    // script.plot(in);
    // script.axis_equal();
    // script.save("poly.m");

    for (std::size_t i = 0; i < h_poly.half_spaces.size();
         // i < 2;
         ++i) {
        auto ret = intersect_convex_polyhedra_with_halfspace(in, h_poly.half_spaces[i], result);
        ASSERT_TRUE(ret > 0);

        // std::cout << ret << std::endl;

        if (ret == IntersectPolyhedronWithHalfSpace<Real>::INTERSECTED) {
            in = result;
        }
    }

    // plot_case(p1, p2, result);

    ASSERT_TRUE(result.n_elements() == 7);
    ASSERT_TRUE(result.n_nodes() == 10);
}

TEST(IntersectionTest, oop_intersect_convex_polyhedra_test) {
    moonolith::Polyhedron<Real> p1, p2, result;

    make_polyhedron_1(p1.el_ptr, p1.el_index, p1.points);
    make_polyhedron_2(p2.el_ptr, p2.el_index, p2.points);

    IntersectConvexPolyhedra<Real> icp;  // this has an optimized memory re-allocation strategy
    for (int i = 0; i < repeat_test; ++i) {
        // bool ok = moonolith::intersect_convex_polyhedra(p1, p2, result); //This is slower than
        bool ok = icp.apply(p1, p2, result);  // this

        ASSERT_TRUE(ok);
        ASSERT_TRUE(result.n_elements() == 7);
        ASSERT_TRUE(result.n_nodes() == 10);
    }
}

TEST(IntersectionTest, graham_scan_test) {
    Storage<Integer> order;
    GrahamScan<Real> graham;
    Polygon<Real, 2> p, hull;

    p.points = {{0, 0}, {0.0015, -0.0430}, {1.8168, -0.5624}, {1.8468, 0.5461}, {0.0383, -0.2600}};

    graham.apply(p.points, order);

    // print(order, std::cout);

    p.points = {{-0.2000, -0.2650},
                {-0.2000, -0.2000},
                {-0.2650, -0.2000},
                {-0.2650, -0.2650},
                {-0.2650, -0.2523},
                {-0.2650, -0.2131},
                {-0.2650, -0.2596},
                {-0.2000, -0.2650}};

    graham.apply(p.points, hull.points);

    EXPECT_TRUE(is_ccw(hull));

    Polygon<Real, 3> p3, hull3;

    p3.points = {{-0.2650, -0.2000, 0.3536},
                 {-0.2268, -0.2000, 0.3000},
                 {-0.2650, -0.2523, 0.3521},
                 {-0.2650, -0.2131, 0.3532},
                 {-0.2273, -0.2273, 0.3000},
                 {-0.2650, -0.2596, 0.3519},
                 {-0.2650, -0.2000, 0.3536}};

    RotatePointsToXYPlane<Real> rotate;
    rotate.apply(p3.points, {-0.8142, 0.0166, -0.5803}, p.points);
    MapToSphere<Real, 2>::apply(p.points);
    graham.apply(p.points, hull.points);

    EXPECT_TRUE(is_ccw(hull));

    p3.points = {{-0.0094, -0.0073, -0.0262},
                 {-0.0094, -0.0075, -0.0260},
                 {-0.0093, -0.0076, -0.0257},
                 {-0.0091, -0.0072, -0.0260},
                 {-0.0093, -0.0075, -0.0258},
                 {-0.0094, -0.0074, -0.0261},
                 {-0.0094, -0.0073, -0.0262}};

    rotate.apply(p3.points, {-0.1890, 0.1890, 0.1890}, p.points);
    MapToSphere<Real, 2>::apply(p.points);
    graham.apply(p.points, hull.points);

    // if (!is_ccw(hull))
    // {
    //     std::cout << "hull.area() : " << hull.area() << std::endl;
    //     MatlabScripter script;
    //     script.close_all();

    //     script.plot(hull);
    //     script.enumerate(hull.points);

    //     script.save("bug.m");
    // }

    EXPECT_TRUE(is_ccw(hull));

    p3.points = {{-0.0091828773067457996, -0.0077046331502902765, -0.026098547041098097},
                 {-0.009430673682427065, -0.0074145373371646093, -0.026124902851513723},
                 {-0.0092813076655166703, -0.0075990588703630721, -0.02569056735535117},
                 {-0.0094004501340267526, -0.0074518744477395623, -0.026037016997945587},
                 {-0.0093304898573554357, -0.0075383009173034124, -0.025833582293665197}};

    rotate.apply(p3.points, {-0.75960624461057991, -0.65021005885865413, -0.015007748253390199}, p.points);
    MapToSphere<Real, 2>::apply(p.points);
    graham.apply(p.points, hull.points);

    EXPECT_TRUE(is_ccw(hull));

    p3.points = {{-0.0093, -0.0076, -0.0257},
                 {-0.0094, -0.0075, -0.0260},
                 {-0.0094, -0.0074, -0.0261},
                 {-0.0092, -0.0077, -0.0261},
                 {-0.0093, -0.0075, -0.0258},
                 {-0.0093, -0.0076, -0.0257}};

    rotate.apply(p3.points, {0.0439, -0.0219, -0.0219}, p.points);
    MapToSphere<Real, 2>::apply(p.points);
    graham.apply(p.points, hull.points);

    EXPECT_TRUE(is_ccw(hull));

    // {
    //     MatlabScripter script;
    //     script.close_all();

    //     script.plot(hull);
    //     script.enumerate(hull.points);

    //     script.save("graham.m");
    // }

    // {
    //     MatlabScripter script;
    //     script.close_all();

    //     script.plot(p);
    //     script.enumerate(p.points);

    //     script.save("graham_in.m");
    // }

    // print(order, std::cout);
}

TEST(IntersectionTest, intersect_convex_polyhedra_2) {
    moonolith::Polyhedron<Real> p1, p2, result;

    p1.el_ptr = {0, 3, 6, 9, 12};
    p1.el_index = {0, 2, 1, 0, 3, 2, 0, 1, 3, 1, 2, 3};
    p1.points = {{-0.0000052915374908479862, -0.009353109635412693, 0.0046137399040162563},
                 {-0.001859034295193851, -0.010618886910378933, 0.0053355623967945576},
                 {0.00039854043279774487, -0.010933933779597282, 0.0046588876284658909},
                 {-0.00041067117126658559, -0.011335679329931736, 0.0059056836180388927}};

    p1.type = 2;

    p2 = p1;
    p2.points = {{-0.00049920539306739587, -0.010735264158190042, 0.004560105125175096},
                 {-0.00054776940338993726, -0.010478816300962493, 0.00452750260369395},
                 {-0.0007500504266989682, -0.010723795892132397, 0.0047472852311625322},
                 {-0.00056532768399013072, -0.010733213493140067, 0.0048106333763011299}};

    // plot_case(p1, p2);
    bool intersected;
    intersected = moonolith::intersect_convex_polyhedra(p1, p2, result);
    ASSERT_TRUE(!intersected);

    p1.points = {{-0.0091020790860056877, -0.0029834203887730837, -0.0078517710790038109},
                 {-0.0090176407247781754, -0.004971945658326149, -0.0085350228473544121},
                 {-0.010637923143804073, -0.0035409634001553059, -0.0078661171719431877},
                 {-0.010200618766248226, -0.004068759735673666, -0.0095903268083930016}};

    p2.points = {{-0.0097743707541887853, -0.0037713370798588969, -0.0090200884232420307},
                 {-0.0099287146027656932, -0.0033954396953150036, -0.0085722698476305668},
                 {-0.010048163527408574, -0.0037762604172237586, -0.0090804790124927366},
                 {-0.009975847849733677, -0.0039878155684308038, -0.0084373780699560919}};

    // plot_case(p1, p2);
    intersected = moonolith::intersect_convex_polyhedra(p1, p2, result);
    ASSERT_TRUE(intersected);

    p1.points = {{-0.00038958055665716529, 0.00069189077476039529, 0.002243520226329565},
                 {-0.0022000723984092474, 0.00040463369805365801, 0.0021782456897199154},
                 {-0.00082492706133052707, -0.00015399423136841506, 0.00026582085411064327},
                 {-0.00085747335106134415, -0.0018696390325203538, 0.0015599874313920736}};

    p2.points = {{-0.00058614232246077911, -0.00033644188340388334, 0.0020707077830150467},
                 {-0.00073749945967365924, -0.0004242830312003038, 0.0020706103130259091},
                 {-0.0009107468457039275, -0.00018522693045107158, 0.0020882583390361997},
                 {-0.00084766076951022732, -0.00026165796852735076, 0.0017746468281863255}};

    // plot_case(p1, p2);
    intersected = moonolith::intersect_convex_polyhedra(p1, p2, result);
    ASSERT_TRUE(intersected);

    p1.points = {{-0.010371915996074677, -0.0073180706240236759, -0.025658575817942619},
                 {-0.0092077143490314483, -0.006968300323933363, -0.025313040241599083},
                 {-0.0095449443906545639, -0.0083124013617634773, -0.025555862113833427},
                 {-0.0089649595320224761, -0.0073388349264860153, -0.026425173506140709}};

    p2.points = {{-0.0087338943851261868, -0.0066872332746404761, -0.02633454665969796},
                 {-0.0095377764005812674, -0.0072822263994270584, -0.026436342583201627},
                 {-0.008664600014282528, -0.008302090495320569, -0.026445960405798174},
                 {-0.0091090016761713275, -0.0078119196401080062, -0.02518952562630121}};

    // plot_case(p1, p2);
    intersected = moonolith::intersect_convex_polyhedra(p1, p2, result);
    ASSERT_TRUE(intersected);
}

TEST(IntersectionTest, intersect_cubes_test) {
    moonolith::Polyhedron<Real> p1, p2, result;

    make_cube({0., 0., 0.}, {1., 0.9, 1.}, p1);
    make_cube({0.0, 0., 0.1}, {0.9, 1., 1.}, p2);

    p1.fix_ordering();
    p2.fix_ordering();

    // plot_case(p1, p2);
    bool ok = moonolith::intersect_convex_polyhedra(p1, p2, result);

    ASSERT_TRUE(ok);
    ASSERT_TRUE(result.n_nodes() == 8);
    ASSERT_TRUE(result.n_elements() == 6);

    const auto vol = moonolith::measure(result);
    ASSERT_TRUE(std::abs(vol - static_cast<Real>(0.9 * 0.9 * 0.9)) < 1e-6);
}

TEST(IntersectionTest, quadrature_2) {
    using Polygon2 = moonolith::Polygon<Real, 2>;
    BuildQuadrature<Polygon2> build_q;

    Quadrature2<Real> in_q;
    Quadrature2<Real> out_q;
    Polygon2 p1, p2;
    build_q.apply(in_q, p1, p2, out_q);
}

TEST(IntersectionTest, quadrature_3) {
    using Polyhedron = moonolith::Polyhedron<Real>;
    BuildQuadrature<Polyhedron> build_q;

    Quadrature3<Real> in_q;
    in_q.points.push_back({0.5, 0.5, 0.5});
    in_q.weights.push_back(1.);

    Quadrature3<Real> out_q;
    Polyhedron p1, p2;

    make_cube({0., 0., 0.}, {1., 1., 1.}, p1);
    make_cube({0., 0., 0.1}, {0.9, 0.9, 1.}, p2);

    ASSERT_TRUE(p1.valid());
    ASSERT_TRUE(p2.valid());

    bool ok = build_q.apply(in_q, p1, p2, out_q);
    ASSERT_TRUE(ok);

    Real vol = 0.;
    for (auto w : out_q.weights) {
        vol += w;
    }

    Polyhedron isect;
    ok = moonolith::intersect_convex_polyhedra(p1, p2, isect);
    ASSERT_TRUE(ok);
    const auto vol_isect = moonolith::measure(isect);

    p1.save_obj("p1.obj");
    p2.save_obj("p2.obj");
    isect.save_obj("isect.obj");

    ASSERT_TRUE(isect.n_elements() == 6);
    ASSERT_TRUE(approxeq(vol_isect, static_cast<Real>(0.9 * 0.9 * 0.9), CheckTol<Real>::half_value()));
    ASSERT_TRUE(approxeq(vol, static_cast<Real>(0.9 * 0.9 * 0.9), CheckTol<Real>::half_value()));
}

TEST(IntersectionTest, quadrature_23) {
    using Polygon3 = moonolith::Polygon<Real, 3>;
    using Polyhedron = moonolith::Polyhedron<Real>;
    BuildQuadrature<Polygon3, Polyhedron> build_q;

    Quadrature2<Real> in_q;
    in_q.points.push_back({0.1767, 0.1767});
    in_q.weights.push_back(1.);

    Quadrature3<Real> out_q;

    Polygon3 p1;
    Polyhedron p2;

    make_quad<Real, 3>({0., 0., 0.}, {1., 0., 1.}, {1., 1., 1.}, {0., 1., 0.}, p1);

    make_cube({0.0, 0.0, 0.0}, {1., 1., 1.}, p2);

    bool ok = build_q.apply(in_q, p1, p2, out_q);
    ASSERT_TRUE(ok);
    ASSERT_TRUE(out_q.n_points() == 2);

    auto a = p1.area();
    auto b = out_q.weights[0] + out_q.weights[1];

    ASSERT_TRUE(approxeq(a, b, static_cast<Real>(1e-8)));

    make_cube({0.0, 0.0, 0.0}, {0.5, 0.5, 0.5}, p2);

    ok = build_q.apply(in_q, p1, p2, out_q);
    ASSERT_TRUE(ok);
    ASSERT_TRUE(out_q.n_points() == 2);

    // 1/4 of the area
    a = 0.25 * p1.area();
    b = out_q.weights[0] + out_q.weights[1];

    ASSERT_TRUE(approxeq(a, b, static_cast<Real>(1e-8)));
}

TEST(IntersectionTest, point_intersection_2) {
    using Polygon2 = moonolith::Polygon<Real, 2>;
    using HPolytope2 = moonolith::HPolytope<Real, 2>;
    using Point2 = moonolith::Vector<Real, 2>;

    Polygon2 p1;
    make_quad<Real, 2>({0., 0.}, {1., 0.}, {1., 1.}, {0., 1.}, p1);

    Point2 p2(0.5, 0.5);

    HPolytope2 h;
    h.make(p1);

    ASSERT_TRUE(h.contains(p2));
}

TEST(IntersectionTest, point_intersection_3) {
    using Polyhedron = moonolith::Polyhedron<Real>;
    using HPolytope3 = moonolith::HPolytope<Real, 3>;
    using Point3 = moonolith::Vector<Real, 3>;

    Polyhedron p1;
    make_cube({0.0, 0.0, 0.0}, {1., 1., 1.}, p1);

    Point3 p2(0.5, 0.5, 0.5);

    HPolytope3 h;
    h.make(p1);

    ASSERT_TRUE(h.contains(p2));
}

TEST(IntersectionTest, cfd_tets) {
    using AT3 = moonolith::AffineTransform<Real, 3>;
    using Point3 = moonolith::Vector<Real, 3>;

    moonolith::Polyhedron<Real> p1, p2, result;

    make_polyhedron_CFD_1(p1.el_ptr, p1.el_index, p1.points);
    make_polyhedron_CFD_2(p2.el_ptr, p2.el_index, p2.points);

    AT3 t;
    make(p1.points[0], p1.points[1], p1.points[2], p1.points[3], t);

    Point3 temp;
    for (auto &p : p1.points) {
        temp = p;
        t.apply_inverse(temp, p);
    }

    for (auto &p : p2.points) {
        temp = p;
        t.apply_inverse(temp, p);
    }

    // p1.scale(1. / max_m);
    // p2.scale(1. / max_m);

    auto m1 = measure(p1);
    auto m2 = measure(p2);

    auto max_m = std::max(m1, m2);

    IntersectConvexPolyhedra<Real> icp;  // this has an optimized memory re-allocation strategy
    for (int i = 0; i < repeat_test; ++i) {
        bool ok = icp.apply(p1, p2, result);

        for (auto &p : result.points) {
            temp = p;
            t.apply(temp, p);
        }

        ASSERT_TRUE(ok);
    }
}

template <int spacedim, int dim, class T1, class T2>
Quadrature<double, spacedim> comp_intersection(const Quadrature<double, dim> &ref_quad, const T1 &t1, const T2 &t2) {
    BuildQuadrature<T1, T2> intersect;
    Quadrature<double, spacedim> out;
    intersect.apply(ref_quad, t1, t2, out);
    return out;
}

TEST(IntersectionTest, check_hexa) {
    using Point3 = moonolith::Vector<Real, 3>;
    Polyhedron<Real> poly1, poly2, poly3, inter;

    // Unit cube
    poly1.el_ptr = {0, 4, 8, 12, 16, 20, 24};
    poly1.el_index = {0, 1, 2, 3, 1, 2, 6, 5, 2, 6, 7, 3, 0, 3, 7, 4, 0, 1, 5, 4, 4, 5, 6, 7};
    poly1.points = {{0, 0, 0}, {1, 0, 0}, {1, 1, 0}, {0, 1, 0}, {0, 0, 1}, {1, 0, 1}, {1, 1, 1}, {0, 1, 1}};
    poly1.fix_ordering();
    auto vol1 = moonolith::measure(poly1);
    ASSERT_NEAR(vol1, 1, 1e-10);

    // Shift of Poly 1 by x = 0.5
    poly2 = poly1;
    poly2.points = {
        {0.5, 0, 0}, {1.5, 0, 0}, {1.5, 1, 0}, {0.5, 1, 0}, {0.5, 0, 1}, {1.5, 0, 1}, {1.5, 1, 1}, {0.5, 1, 1}};
    poly2.fix_ordering();
    auto vol2 = moonolith::measure(poly2);
    ASSERT_NEAR(vol2, 1, 1e-10);

    // poly3 is a deformation of poly2 - Hexahedron: first vertex is (0,0,0) instead of (0.5,0,0)
    poly3 = poly1;
    poly3.points = {
        {0, 0, 0}, {1.5, 0, 0}, {1.5, 1, 0}, {0.5, 1, 0}, {0.5, 0, 1}, {1.5, 0, 1}, {1.5, 1, 1}, {0.5, 1, 1}};
    poly3.fix_ordering();
    auto vol3 = moonolith::measure(poly3);
    ASSERT_NEAR(vol3, 1.0833333333333333, 1e-10);

    // Intersection between the Unit Cube and the hexahedron poly3
    auto inter_check = moonolith::intersect_convex_polyhedra(poly1, poly3, inter);
    ASSERT_TRUE(inter_check);

    if (false) {
        std::array<Point3, 27> hex27;
        Storage<Polyhedron<Real>> decomposition;
        ConvexDecomposition<Real, 3>::make_hex27_from_hex8(poly3.points, hex27);
        // Unfortunately the decomposition is not really a set of convex polyhedra.
        ConvexDecomposition<Real, 3>::decompose_hex27(hex27, decomposition);

        Quadrature<Real, 3> ref_quad;
        moonolith::Gauss::get(3, ref_quad);

        MatlabScripter script;
        script.close_all();
        script.plot(poly1);
        script.plot(poly3);

        Real integral = 0;

        for (auto &p : decomposition) {
            auto quad = comp_intersection<3>(ref_quad, poly1, p);
            integral += moonolith::measure(quad);
            script.plot(p);
        }

        // Can visualize poly in matlab
        script.save("check_hexa.m");

        // Integration error is large
        std::cout << integral << "== " << (1.0833333333333333 - 0.5) << std::endl;
        ASSERT_NEAR(integral, (1.0833333333333333 - 0.5), 1e-10);
    }
}
