#ifndef FE_VALUES_CL
#define FE_VALUES_CL

#include "kernel_base.cl"
#include "algebra/matrix.cl"

#include "fe_object.cl"
#include "fe_values_interface.cl"
#include "assembly/quad_meta.cl"


void init_invalid(FEObject * fe)
{
	const Scalar invalid_value = -6;
	
	fe->n_dims = invalid_value;
	fe->n_quad_points = invalid_value;
	fe->n_shape_functions = invalid_value;
	fe->reference_volume = invalid_value;

	set(MAX_QUAD_POINTS * MAX_N_DIMS * MAX_N_DIMS, invalid_value, fe->jacobian);
	set(MAX_QUAD_POINTS * MAX_N_DIMS * MAX_N_DIMS, invalid_value, fe->inverse_jacobian);
	set(MAX_QUAD_POINTS, invalid_value, fe->jacobian_determinant);
	set(MAX_QUAD_POINTS, invalid_value, fe->dx);
	set(MAX_QUAD_POINTS * MAX_N_DIMS, invalid_value, fe->point);

	for(SizeType i = 0; i < MAX_N_DIMS; ++i) {
		set(MAX_QUAD_POINTS, invalid_value, fe->fun[i]);
		set(MAX_QUAD_POINTS * MAX_N_DIMS, invalid_value, fe->grad[i]);
	}
}

void transform_gradients(const SizeType dims, const SizeType n_quad_points, const Scalar *inverse_jacobians, Scalar *gradient)
{
	Scalar temp[MAX_N_DIMS];

	const SizeType dims2 = dims*dims;
	for(SizeType q = 0; q < n_quad_points; ++q) {
		const SizeType jac_offset  = dims2 * q;
		const SizeType grad_offset = dims  * q;
		mat_mat_mul(1, dims, dims, &gradient[grad_offset], &inverse_jacobians[jac_offset], temp);
		generic_copy(dims, temp, &gradient[grad_offset]);
	}
}

void make_dx_d(const Scalar * weights, FEObject * fe)
{
	for(SizeType q = 0; q < fe->n_quad_points; ++q) {
		fe->dx[q] = weights[q] * fabs(fe->jacobian_determinant[q]) * fe->reference_volume;
	}
}

void symmetrize(const SizeType n_dims, const SizeType n_matrices, Matrixdxd *matrices)
{
	for(SizeType i = 0; i < n_matrices; ++i) {
		mat_symmetrize(n_dims, matrices[i].entries);
	}
}

void compute_traces(const SizeType n_dims, const SizeType n_matrices, const Matrixdxd *matrices, Scalar *traces)
{
	for(SizeType i = 0; i < n_matrices; ++i) {
		traces[i] = trace(n_dims, n_dims, matrices[i].entries);
	}
}

// dimension specific functions
//2
void make_inverses_and_dets_2(const SizeType n_quad_points, const Scalar * jacobians, Scalar *inverse_jacobians, Scalar *dets)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {
		const SizeType offset = q*4;
		const Scalar det = det_2(&jacobians[offset]);
		dets[q] = det;
		inverse_2(&jacobians[offset], det, &inverse_jacobians[offset]);
	}
}

bool make_grad_dxd_at_quad_index(const SizeType quad_index, const FEObject *fe, Matrixdxd *result)
{
	switch(fe->n_dims) {
		case 2: {  make_grad_2x2_at_quad_index(quad_index, fe, result); return true; }
		case 3: {  make_grad_3x3_at_quad_index(quad_index, fe, result); return true; }
		default: { 
			KERNEL_ASSERT(false, "make_grad_dxd_at_quad_index: not defined for requested dimension");
			return false; 
		}
	}

	return false;
}

void make_grad_2x2_at_quad_index(const SizeType quad_index, const FEObject *fe, Matrixdxd *result)
{
	for(SizeType i = 0; i < fe->n_shape_functions; ++i) {
		const SizeType offset = i * fe->n_dims;

		const SizeType res_offset 	 =  offset;
		const SizeType res_offset_p1 = (offset + 1);;

		const SizeType q_grad2 = quad_index * 2;

		//Orginal
		const Scalar * grad_iq = &fe->grad[i][q_grad2];

		//New
		Scalar * block_grad_iq1 = result[res_offset].entries;
		Scalar * block_grad_iq2 = result[res_offset_p1].entries;

		//Copy the gradient in the two positions
		generic_copy(2, grad_iq, block_grad_iq1);
		generic_copy(2, grad_iq, &block_grad_iq2[2]);

		//Clean the other elements of the matrices
		set(2, 0, &block_grad_iq1[2]);
		set(2, 0, block_grad_iq2);
	}
}

void make_grad_3x3_at_quad_index(const SizeType quad_index, const FEObject *fe, Matrixdxd *result)
{
	for(SizeType i = 0; i < fe->n_shape_functions; ++i) {
		const SizeType offset = i * fe->n_dims;

		const SizeType res_offset_p0 =  offset;
		const SizeType res_offset_p1 = (offset + 1);
		const SizeType res_offset_p2 = (offset + 2);

		
		const SizeType q_grad3 = quad_index * 3;

			//Orginal
		const Scalar * grad_iq = &fe->grad[i][q_grad3];

			//New
		Scalar * block_grad_iq0 = result[res_offset_p0].entries;
		Scalar * block_grad_iq1 = result[res_offset_p1].entries;
		Scalar * block_grad_iq2 = result[res_offset_p2].entries;

			//Copy the gradient in the two positions
		generic_copy(3, grad_iq, block_grad_iq0);
		generic_copy(3, grad_iq, &block_grad_iq1[3]);
		generic_copy(3, grad_iq, &block_grad_iq2[6]);

			//Clean the other elements of the matrices
		set(6, 0, &block_grad_iq0[3]);

		set(3, 0, block_grad_iq1);
		set(3, 0, &block_grad_iq1[6]);

		set(6, 0, block_grad_iq2);
	}
}

bool make_grad_dxd(FEObject *fe, Matrixdxd *result)
{
	switch(fe->n_dims) {
		case 2: {  make_grad_2x2(fe, result); return true; }
		case 3: {  make_grad_3x3(fe, result); return true; }
		default: { 
			KERNEL_ASSERT(false, "make_grad_dxd: not defined for requested dimension");
			return false; 
		}
	}

	return false;
}

void make_grad_2x2(FEObject *fe, Matrixdxd *result)
{
	for(SizeType i = 0; i < fe->n_shape_functions; ++i) {
		const SizeType offset = i * fe->n_dims;

		const SizeType res_offset 	 =  offset 		* fe->n_quad_points;
		const SizeType res_offset_p1 = (offset + 1) * fe->n_quad_points;

		for(SizeType q = 0; q < fe->n_quad_points; ++q) {
			const SizeType q_grad2 = q * 2;
			
			//Orginal
			Scalar * grad_iq = &fe->grad[i][q_grad2];

			//New
			Scalar * block_grad_iq1 = result[res_offset + q].entries;
			Scalar * block_grad_iq2 = result[res_offset_p1 + q].entries;

			//Copy the gradient in the two positions
			generic_copy(2, grad_iq, block_grad_iq1);
			generic_copy(2, grad_iq, &block_grad_iq2[2]);

			//Clean the other elements of the matrices
			set(2, 0, &block_grad_iq1[2]);
			set(2, 0, block_grad_iq2);
		}
	}
}

void make_grad_3x3(FEObject *fe, Matrixdxd *result)
{
	for(SizeType i = 0; i < fe->n_shape_functions; ++i) {
		const SizeType offset = i * fe->n_dims;

		const SizeType res_offset_p0 =  offset 		* fe->n_quad_points;
		const SizeType res_offset_p1 = (offset + 1) * fe->n_quad_points;
		const SizeType res_offset_p2 = (offset + 2) * fe->n_quad_points;

		for(SizeType q = 0; q < fe->n_quad_points; ++q) {
			const SizeType q_grad3 = q * 3;
			
			//Orginal
			Scalar * grad_iq = &fe->grad[i][q_grad3];

			//New
			Scalar * block_grad_iq0 = result[res_offset_p0 + q].entries;
			Scalar * block_grad_iq1 = result[res_offset_p1 + q].entries;
			Scalar * block_grad_iq2 = result[res_offset_p2 + q].entries;

			//Copy the gradient in the two positions
			generic_copy(3, grad_iq, block_grad_iq0);
			generic_copy(3, grad_iq, &block_grad_iq1[3]);
			generic_copy(3, grad_iq, &block_grad_iq2[6]);

			//Clean the other elements of the matrices
			set(6, 0, &block_grad_iq0[3]);

			set(3, 0, block_grad_iq1);
			set(3, 0, &block_grad_iq1[6]);

			set(6, 0, block_grad_iq2);
		}
	}
}

// 3
void make_inverses_and_dets_3(const SizeType n_quad_points, const Scalar * jacobians, Scalar *inverse_jacobians, Scalar *dets)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {
		const SizeType offset = q*9;
		const Scalar det = det_3(&jacobians[offset]);
		dets[q] = det;
		inverse_3(&jacobians[offset], det, &inverse_jacobians[offset]);
	}
}

//Values
bool make_fe_object_from_values(
	const SizeType n_dims,
	const SizeType n_shape_functions,
	const SizeType n_quad_points,
	const m_global__ Scalar   * functions,		
	const m_global__ Scalar   * gradients,			
	const m_global__ Scalar   * jacobians,			
	const m_global__ Scalar   * global_points,     
	const m_global__ Scalar   * dxs,	
	FEObject * fe)
{
	FEOBJECT_DEBUG(fe);

	fe->n_dims = n_dims;
	fe->n_shape_functions = n_shape_functions;
	fe->n_quad_points = n_quad_points;

	// printf("d=%d, ns=%d, q=%d\n", n_dims, n_shape_functions, n_quad_points );
	

	const SizeType n_point_entries = n_quad_points * n_dims;
	const SizeType n_jac_entries   = n_point_entries * n_dims;

	generic_copy(n_jac_entries, jacobians, fe->jacobian);
	generic_copy(n_point_entries, global_points, fe->point);
	generic_copy(n_quad_points, dxs, fe->dx);

	SizeType index_fun  = 0;
	SizeType index_grad = 0;

	for(SizeType j = 0; j < fe->n_shape_functions; ++j) {
		for(SizeType q = 0; q < fe->n_quad_points; ++q) {
			const SizeType q_grad = q * fe->n_dims;

			fe->fun[j][q] = functions[index_fun++];

			for(SizeType d = 0; d < fe->n_dims; ++d) {
				fe->grad[j][q_grad+d] = gradients[index_grad++];
			}
		}
	}

	return true;
}

void make_fe_object_from_global_values(	const SizeType i, 
	const SizeType n_dims, 
	const m_global__ SizeType * quad_meta,
	const m_global__ Scalar   * global_points,     
	const m_global__ Scalar   * jacobians,		
	const m_global__ Scalar   * dxs,				
	const m_global__ Scalar   * functions,			
	const m_global__ Scalar   * gradients,		
	FEObject * fe	
	)
{
	const SizeType n_dims2 = n_dims * n_dims;
	const SizeType n_shape_fun_offset   = get_n_shape_fun_offset(i, quad_meta);
	const SizeType n_quad_point_offset  = get_n_quad_points_offset(i, quad_meta);

	SizeType n_shape_functions = get_n_shape_fun_offset(i+1, quad_meta)- n_shape_fun_offset;
	SizeType n_quad_points 	   = get_n_quad_points_offset(i+1, quad_meta) - n_quad_point_offset;

	n_shape_functions /= n_quad_points;

	const SizeType offset_fun  = n_shape_fun_offset; 
	const SizeType offset_grad = offset_fun * n_dims;

	const SizeType offset_jac  	= n_quad_point_offset * n_dims2;
	const SizeType offset_point = n_quad_point_offset * n_dims;
	const SizeType offset_dx 	= n_quad_point_offset;

	// printf("------------------------------\n");
	// printf("fun_offset = %d \n", offset_fun);
	// printf("grad_offset = %d \n", offset_grad);
	// printf("offset_jac = %d \n", offset_jac);
	// printf("offset_point = %d \n", offset_point);
	// printf("offset_dx = %d \n", offset_dx);
	// printf("n_shape_functions = %d \n", n_shape_functions);
	// printf("n_quad_points = %d \n", n_quad_points);
	// printf("n_dims = %d \n", n_dims);
	// printf("------------------------------\n");


		// /*bool ok =*/ 
	make_fe_object_from_values( n_dims, n_shape_functions, n_quad_points, 
		&functions[offset_fun], 
		&gradients[offset_grad],
		&jacobians[offset_jac],			
		&global_points[offset_point],     
		&dxs[offset_dx],	
		fe
		);

}

void fe_object_print(FEObject *fe) {
	printf("-------------------------\n");
	printf("FEObject:\n");
	printf("n_dims: %d\n", fe->n_dims);
	printf("n_quad_points: %d\n", fe->n_quad_points);
	printf("n_shape_functions: %d\n", fe->n_shape_functions);
	printf("reference_volume: %g\n", fe->reference_volume);
	

	printf("points:\n"); 
	matrix_print(fe->n_quad_points, fe->n_dims, fe->point);
	
	printf("jacobian_determinant:\n"); 
	matrix_print(1, fe->n_quad_points, fe->jacobian_determinant);

	printf("jacobian:\n"); 
	matrix_print(fe->n_quad_points, fe->n_dims*fe->n_dims, fe->jacobian);

	printf("inverse_jacobian:\n"); 
	matrix_print(fe->n_quad_points, fe->n_dims*fe->n_dims, fe->inverse_jacobian);

	printf("dx:\n"); 
	matrix_print(1, fe->n_quad_points, fe->dx);
	

	printf("fun:\n"); 
	printf("{\n"); //}"
	for(SizeType i = 0; i < fe->n_shape_functions; ++i) {
		matrix_print(1, fe->n_quad_points, fe->fun[i]);
	}
	//{}
	printf("}\n"); 

	printf("grad:\n"); 
	printf("{\n"); //}"


	for(SizeType i = 0; i < fe->n_shape_functions; ++i) {
		matrix_print(1, fe->n_quad_points*fe->n_dims, fe->grad[i]);
	}
	
	//{}
	printf("}\n"); 
	printf("-------------------------\n");
}

#endif //FE_VALUES_CL
