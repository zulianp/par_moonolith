# xSDK Community Policy Compatibility for ParMoonolith

This document summarizes the efforts of current and future xSDK member packages to achieve compatibility with the xSDK community policies. Below only short descriptions of each policy are provided. The full description is available [here](https://docs.google.com/document/d/1DCx2Duijb0COESCuxwEEK1j0BPe2cTIJ-AjtJxt3290/edit#heading=h.2hp5zbf0n3o3)
and should be considered when filling out this form.

Please, provide information on your compatibility status for each mandatory policy, and if possible also for recommended policies.
If you are not compatible, state what is lacking and what are your plans on how to achieve compliance.

For current xSDK member packages: If you were not fully compatible at some point, please describe the steps you undertook to fulfill the policy. This information will be helpful for future xSDK member packages.

**Website:** https://bitbucket.org/zulianp/par_moonolith

### Mandatory Policies

| Policy                 |Support| Notes                   |
|------------------------|-------|-------------------------|
|**M1.** Each xSDK-compatible package must support portable installation through Spack. |Full| ParMoonolith uses CMake and implements the documented spack variants with the flags: `+shared`, `precision=(single or double or quad)`, `+int64`, `build_type=(debug or release or relwithdebinfo or minsizerel)`, `+pic`. The installation can be tested with `make test_install`. |
|**M2**  Each xSDK-compatible package must provide a comprehensive test suite that can be run by users and does not require the purchase of commercial software. | Full | A comprehensive test suite is available. Users can run tests with `make test`. |
| **M3** Each xSDK-compatible package that utilizes MPI must restrict its MPI operations to MPI communicators that are provided to it and not use directly MPI_COMM_WORLD. | Full | Users can provide their own communicator. MPI 3 functionalities are guarded by preprocessors branches. |
| **M4** Each package team must do a “best effort” at portability to common platforms, including standard Linux distributions, and common compiler toolchains such as GNU, Clang, and vendor compilers | Partial | Code is tested on Linux, MacOS, and with Microsoft Visual Studio 16 (2019). |
| **M5** Each package team must provide a documented, reliable way to contact the development team; the mode of contact may be by email or a website. | Full | The ParMoonolith developers can be contact through the bitbucket issue system. |
| **M6** Each package should respect the decisions made by other previously called packages regarding system resources and settings. | Full | ParMoonolith does not interfere with system resources and settings, and signal handling of  other packages. |
| **M7** The xSDK collaboration has a strong preference for packages to use an OSI-approved, permissive open-source license (e.g., MIT or BSD 3-Clause). | Full |  ParMoonolith is open-source and available under a BSD 3-Clause license. |
| **M8** Each package must provide a runtime API, for example a function call, to return the current version number of the software and indicate what configure/CMAKE and compiler options were used to build the package. | Full | All version information is defined within `moonolith_version.hpp` and `moonolith_configuration_details.hpp`. |
| **M9** Each package should use a limited and well-defined symbol, macro, library, and include file name space. | Full | All macros have the prefix `MOONOLITH_` while all files `moonolith_`. |
| **M10** It is mandatory that each package have an xSDK-accessible repository (not necessarily publicly available), where the development version of the package is available. | Full | The repository is publicly accessible ([https://bitbucket.org/zulianp/par_moonolith](https://bitbucket.org/zulianp/par_moonolith)), contributions are welcome via pull requests |
| **M11**  No package should have hardwired print or I/O statements that cannot be turned off through a programmatic interface; output should never be hard-wired to stdout or stderr. | Partial | Only failure states generate hardwired IO, however they can be redirected to files with command line options. |
| **M12** If a package imports software that is externally developed and maintained, then it must allow installing, building, and linking against an outside copy of that software.  | Full | All the dependencies are defined externally. GTest is automatically fetched if none is available. |
| **M13** When configured with a prefix, a package must install its headers and libraries under `<install-prefix>/include/` and `<install-prefix>/lib/`, respectively. | Full | Supported via `-DCMAKE_INSTALL_PREFIX=<install-prefix>`. Version numbering is handled when building with the option `-DBUILD_SHARED_LIBS=ON`. |
| **M14** All xSDK-compatible packages must be buildable using 64 bit pointers (this is commonly the default). It is not required that they be buildable with 32 bit pointers. | Full | No assumptions are made on the size of pointers. |
| **M15** All xSDK compatibility changes should be sustainable (i.e, they should go into the regular development and release versions of the package and should not be in a private release/branch that is provided only for xSDK releases). | Full | All changes are and will be part of the `master` branch. |
| **M16** Any xSDK-compatible package that compiles code should have a configuration option to build in Debug mode. Debug mode must produce debugging symbols in compiled code and may contain further useful debugging information and additional error checking.| Full | Supported via `-DCMAKE_BUILD_TYPE=Debug` for cmake or `build_type=debug` for spack. Compiling with debug symbols will add a large number of checks in the whole code making it slower. |

### Recommended Policies

| Policy                 |Support| Notes                   |
|------------------------|-------|-------------------------|
| **R1.** | Full | See notes of **M10**. |
| **R2**   It is recommended that all packages make it possible to run their test suite under valgrind in order to test for memory corruption issues. | Full | Memory checks and general checks on the program can be performed with the following `CMAKE_BUILD_TYPE` values `ASAN`, `TSAN`, `UBSAN`, and `LSAN`, see `cmake/MoonolithSanitizer.cmake` for more details. |
| **R3** It is recommended that each package adopt and document a consistent system for propagating/returning error conditions/exceptions and provide an API for changing the behavior | Partial | Error states cause the program to crash with `MPI_Abort()`. |
| **R4** It is recommended that each package free all system resources it has acquired as soon as they are no longer needed. | Full | Resources are bounded to the lifetime of objects and released once the objects are destroyed. No `new` or `delete` functions are called directly, every allocated resource is handled with decorators (i.e., `make_shared` and `make_unique`), smart pointers, or STL containers. |
| **R5** It is recommended that each package provide a mechanism to export its ordered list of library dependencies so that any other package or executable linking to the package knows to include these dependencies when linking. | Full | ParMoonolith supports modern CMake. All dependencies are handle automatically when the client CMake project contains the following lines of code `find_package(ParMoonolith REQUIRED) add_executable(my_exe my_main.cpp) target_link_libraries(my_exe ParMoonolith::par_moonolith)`.  For users with Makefile projects the command `make install_all` has to be used instead of `make install`. This extrac command will install also the `moonolith-config.makefile` configuration file. |
| **R6** Each package should document the versions of packages with which it can work or upon which it depends, including software external to the xSDK, preferably in a machine-readable form. The developers of xSDK member packages will coordinate the needed versions of various packages for each xSDK release. | Full | Version number is embbded in the generate files in `<install dir>/lib/cmake`. There are few dependencies and their API is fixed. |
| **R7** It is recommended that each package should have a README or README.md, a SUPPORT or SUPPORT.md, a LICENSE or LICENSE.md, and a CHANGELOG or CHANGELOG.md file in its top directory. [Details](https://github.com/kmindi/special-files-in-repository-root/blob/master/README.md). | Full | All files are in the top folder. |
| **R8** Each xSDK member package should have sufficient documentation to support use and further development. | Partial | Usage examples of high level functionalities are available as well as basic documentation. Tests are a good source of usage for lower level functionalities. |
