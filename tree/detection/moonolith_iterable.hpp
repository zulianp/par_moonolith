#ifndef MOONOLITH_ITERABLE_HPP
#define MOONOLITH_ITERABLE_HPP 

#include <set>

namespace moonolith {
	
	template<class ValueType>
	class Iterable {
	public:
		virtual const ValueType &operator*() const
		{
			return value();
		}
	
		inline Iterable &operator++() 
		{
			increment();
			return *this;
		}
		virtual bool good() const = 0;
		virtual void reset() = 0;
		virtual void increment() = 0;
		virtual const ValueType &value() const = 0;


		inline operator bool() const
		{
			return good();
		}

		virtual ~Iterable() {}
	};

	template<class ValueType>
	class IterableSet : public Iterable<ValueType> {
	public:
		typedef std::set<ValueType> Container;
		
		std::set<ValueType> _container;
		typename std::set<ValueType>::iterator _it;
		
		IterableSet()
		: _it(_container.begin())
		{}

		void reset()
		{
			_it = _container.begin();
		}
		
		virtual const ValueType &value() const 
		{
			return *_it;
		}

		virtual void increment()
		{
			++_it;
		}

		virtual bool good() const 
		{
			return _it != _container.end();
		}

		std::set<ValueType> & container() 
		{
			return _container;
		}
	};

}

#endif //MOONOLITH_ITERABLE_HPP
