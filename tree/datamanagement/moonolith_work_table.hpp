#ifndef MOONOLITH_WORKTABLE_HPP
#define MOONOLITH_WORKTABLE_HPP

#include "moonolith_data_handle.hpp"
#include "moonolith_node_handle.hpp"

#include <algorithm>
#include <memory>
#include <set>
#include <vector>

namespace moonolith {

    class WorkBatch : public Describable {
    public:
        typedef std::set<DataHandle>::const_iterator ConstIter;

        inline void insert(const DataHandle &handle) { _handles.insert(handle); }

        template <class Iter>
        inline void insert(const Iter &begin, const Iter &end) {
            _handles.insert(begin, end);
        }

        inline Integer n_elements() const { return static_cast<Integer>(_handles.size()); }

        inline void clear() { _handles.clear(); }

        inline ConstIter begin() const { return _handles.begin(); }

        inline ConstIter end() const { return _handles.end(); }

        inline bool empty() const { return _handles.empty(); }

        inline void describe(std::ostream &os) const {
            os << "node: " << _node << "\trank: " << _rank << ",\tn_elements: " << n_elements()
               << ",\tweight: " << _weight << ",\tworker: " << _worker;
        }

        inline void set_node(const NodeHandle &node) { _node = node; }

        inline void set_rank(const Integer rank) { _rank = rank; }

        inline Integer rank() const { return _rank; }

        inline NodeHandle node() const { return _node; }

        void set_worker(const Integer worker) { _worker = worker; }

        void set_weight(const Integer weight) { _weight = weight; }

        void set_global_handle(const NodeHandle &globalHandle) { _globalHandle = globalHandle; }

        NodeHandle global_handle() const { return _globalHandle; }

        inline Integer worker() { return _worker; }

        WorkBatch() : _node(-1), _globalHandle(-1), _rank(-1), _ownerRank(-1), _weight(-1), _worker(-1) {}

        inline void set_owner_rank(const Integer ownerRank) { _ownerRank = ownerRank; }

        inline Integer owner_rank() const { return _ownerRank; }

    private:
        NodeHandle _node;
        NodeHandle _globalHandle;
        Integer _rank;
        Integer _ownerRank;
        Integer _weight;
        Integer _worker;
        std::set<DataHandle> _handles;
    };

    class WorkTable : public SynchedDescribable {
    public:
        typedef std::vector<std::shared_ptr<WorkBatch> > WorkArray;

        inline void add_batch(const NodeHandle &handle, const Integer rank, const std::shared_ptr<WorkBatch> &batch) {
            batch->set_node(handle);
            batch->set_rank(rank);
            _work.push_back(batch);
        }

        inline void describe(std::ostream &os) const override {
            for (std::size_t i = 0; i < _work.size(); ++i) {
                _work[i]->describe(os);
                os << "\n";
            }
        }

        inline void reserve(const std::size_t n) {
            if (_work.capacity() < n) _work.reserve(n);
        }

        inline const WorkArray &work() const { return _work; }

        inline WorkArray &work() { return _work; }

    private:
        class Comparator {
        public:
            // typedef std::make_unsigned<Integer>::type Unsigned;

            inline bool operator()(const std::shared_ptr<WorkBatch> &left, const std::shared_ptr<WorkBatch> &right) {
                auto l = left->global_handle();
                auto r = right->global_handle();
                if (l < r) return true;
                if (r < l) return false;
                return left->rank() < right->rank();
            }
        };

    public:
        template <class TreeT>
        inline void sort_work(const TreeT &tree) {
            // const Integer depth = tree.memory().stats().n_levels();
            auto hash = tree.memory().mutator_factory()->new_hash();
            for (auto w : _work) {
                const Integer id = hash->flattened_hash(*tree.node(w->node()), tree.memory());
                w->set_global_handle(id);
            }

            using std::sort;
            sort(_work.begin(), _work.end(), Comparator());
        }

        WorkArray _work;
    };

}  // namespace moonolith

#endif  // MOONOLITH_WORKTABLE_HPP
