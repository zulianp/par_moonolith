#ifndef CUTLIBPP_NTREE_WITH_SPAN_INSERT_H
#define CUTLIBPP_NTREE_WITH_SPAN_INSERT_H 

#include "moonolith_insert.hpp"

namespace moonolith {

	template<class Tree>
	class NTreeWithSpanInsert : public Insert<Tree> {
	public:
		typedef typename Tree::Traits Traits;
		typedef typename Traits::DataType DataType;

		typedef moonolith::Node<Traits> Node;
		typedef moonolith::Branch<Traits> Branch;
		typedef moonolith::Root<Traits> Root;
		typedef moonolith::Leaf<Traits> Leaf;
		typedef moonolith::Visitor<Traits> Visitor;

		typedef moonolith::TreeMemory<Traits> TreeMemory;

		virtual bool match(const Node &node) const override
		{
			return this->object().bound().static_bound().intersects(node.bound().static_bound());
		}

		virtual bool must_insert(Leaf &, TreeMemory &) override {
			return true;
		}

		virtual void mutate(Node &node, TreeMemory &) const override {
			assert(!this->object().bound().dynamic_bound().empty());
			node.bound().dynamic_bound() += this->object().bound().dynamic_bound();
		}

		explicit NTreeWithSpanInsert(const DataHandle &handle, const DataType &obj)
		: Insert<Tree>(handle, obj)
		{}
	};
	
}

#endif //CUTLIBPP_NTREE_WITH_SPAN_INSERT_H
