#include "moonolith_base.hpp"

#include <cmath>
#include <iostream>

namespace moonolith {
    void test_check_assertion(const bool expr,
                              const std::string &filename,
                              const int line,
                              const std::string &expr_string) {
        if (!expr) {
            std::cerr << filename << ": " << line << "\ntest failure: " << expr_string << std::endl;
            abort();
        }
    }
}  // namespace moonolith
