
#ifndef MOONOLITH_STREAM_HPP
#define MOONOLITH_STREAM_HPP

#include "moonolith_describable.hpp"
#include "moonolith_forward_declarations.hpp"
#include "par_moonolith_config.hpp"

#include <memory.h>
#include <cstring>
#include <iostream>
#include <string>

namespace moonolith {

    class Stream {
    public:
        virtual bool good() = 0;
        virtual void clear() = 0;

        virtual Stream& operator&(Serializable& obj) = 0;
        virtual Stream& operator&(int&) = 0;
        virtual Stream& operator&(unsigned int&) = 0;
        virtual Stream& operator&(short&) = 0;
        virtual Stream& operator&(unsigned short&) = 0;
        virtual Stream& operator&(long&) = 0;
        virtual Stream& operator&(unsigned long&) = 0;
        virtual Stream& operator&(unsigned long long&) = 0;

        virtual Stream& operator&(float&) = 0;
        virtual Stream& operator&(double&) = 0;

        virtual Stream& operator&(char&) = 0;
        virtual Stream& operator&(unsigned char&) = 0;
        virtual Stream& operator&(bool&) = 0;

        virtual Stream& apply(int* array, const Integer nElements) = 0;
        virtual Stream& apply(unsigned int* array, const Integer nElements) = 0;
        virtual Stream& apply(short* array, const Integer nElements) = 0;
        virtual Stream& apply(unsigned short* array, const Integer nElements) = 0;
        virtual Stream& apply(long* array, const Integer nElements) = 0;
        virtual Stream& apply(unsigned long* array, const Integer nElements) = 0;
        virtual Stream& apply(unsigned long long* array, const Integer nElements) = 0;

        virtual Stream& apply(float* array, const Integer nElements) = 0;
        virtual Stream& apply(double* array, const Integer nElements) = 0;

        virtual Stream& apply(char* array, const Integer nElements) = 0;
        virtual Stream& apply(unsigned char* array, const Integer nElements) = 0;
        virtual Stream& apply(bool* array, const Integer nElements) = 0;
        virtual Stream& apply(Serializable* array, const Integer nElements) = 0;

#ifdef MOONOLITH_HAVE_QUAD_PRECISION
        virtual Stream& operator&(long double&) = 0;
        virtual Stream& apply(long double* array, const Integer nElements) = 0;

#endif  // MOONOLITH_HAVE_QUAD_PRECISION

        template <typename T1, typename T2>
        inline Stream& operator&(std::pair<T1, T2>& data) {
            (*this) & data.first;
            (*this) & data.second;
            return *this;
        }

        template <typename T1, typename T2>
        inline Stream& operator&(std::pair<const T1, T2>& data) {
            (*this) & const_cast<T1&>(data.first);  // hack for stl maps to write without compiler error
            (*this) & data.second;
            return *this;
        }

        // virtual void describe(std::ostream &os) const
        // {
        // 	os << "Stream\n";
        // }
    };

    /// To be used in case of serializable collections to make external libraries elements compatible (serializable)
    template <class Stream, typename T>
    class GenericStreamer {
    public:
        static void Apply(Stream& stream, T* array, const Integer nElements) { stream.apply(array, nElements); }

        static void Read(Stream& stream, T* array, const Integer nElements) { Apply(stream, array, nElements); }

        static void Write(Stream& stream, const T* array, const Integer nElements) {
            Apply(stream, const_cast<T*>(array), nElements);
        }
    };

}  // namespace moonolith

#endif  // MOONOLITH_STREAM_HPP
