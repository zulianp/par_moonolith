#ifndef MOONOLITH_INTERSECT_POLYGONS_HPP
#define MOONOLITH_INTERSECT_POLYGONS_HPP

#include "moonolith_polygon.hpp"
#include "moonolith_affine_transform.hpp"
#include "moonolith_graham_scan.hpp"
#include "moonolith_line.hpp"

#include <memory>

namespace moonolith {

	template<typename T, class IndexT>
	bool sorted(
		const Storage<Vector<T, 3>> &points,
		const Vector<T, 3> &normal,
		IndexT *index,
		const std::size_t n_points
	);


	template<typename T, int Dim>
	class RemoveDuplicatePoints {
	public:
		using Point = moonolith::Vector<T, Dim>;
		void apply(Storage<Point> &polygon, const T tol);
	private:
		Storage<bool> keep;
		Storage<short> is_inside;
	};

	template<typename T, class IndexT>
	class SortPolygon {
	public:
		using Vector3 = moonolith::Vector<T, 3>;
		using Vector2 = moonolith::Vector<T, 2>;

		bool apply(
			const Storage<Vector<T, 3>> &points,
			const Vector<T, 3> &normal,
			IndexT *index,
			const std::size_t n_points
		);

		Storage<Vector2> local_point;
		Storage<T> 	     local_value;
		Storage<T> H;
		Integer i_min[2], i_max[2];
		Vector2 avg;
		Vector3 u, v, w;
		Vector2 proj_vec, perp_proj_vec;

		GrahamScan<T> graham_scan;
	};

	template<typename T, int Dim>
	class IntersectPolygons final {
	public:
		using Polygon = moonolith::Polygon<T, Dim>;
		using Point   = typename Polygon::Point;

		bool apply(const Polygon &poly1, const Polygon &poly2, Storage<Polygon> &result, const T &tol = 1e-10);

		IntersectPolygons();
		~IntersectPolygons();
	private:
		class Impl;
		std::unique_ptr<Impl> impl_;
	};

	template<typename T, int Dim>
	class IntersectConvexPolygons final {
	public:
		using Polygon = moonolith::Polygon<T, Dim>;
		using Point   = typename Polygon::Point;

		bool apply(const Polygon &poly1, const Polygon &poly2, Polygon &result, const T &tol = 1e-10);
		void remove_duplicate_points(Polygon &poly, const T tol = 1e-10);
		void remove_duplicate_points(Storage<Point> &poly, const T tol = 1e-10)
		{
			rdp.apply(poly, tol);
		}

	private:

		//buffers
		Storage<short> is_inside;
		Polygon input_buffer;
		RemoveDuplicatePoints<T, Dim> rdp;
	};


	template<typename T>
	class IntersectConvexPolygons<T, 3> final {
	public:
		using Polygon  = moonolith::Polygon<T, 3>;
		using Polygon2 = moonolith::Polygon<T, 2>;
		using Point    = typename Polygon::Point;
		using Point2   = typename Polygon2::Point;
		using Vector   = typename Polygon::Vector;

		bool apply(const Polygon &poly1, const Polygon &poly2, Polygon &result, const T &tol = 1e-10);

		//lower dim intersection
		bool apply(const Polygon &poly1, const Polygon &poly2, Line<T, 3> &result, const T &tol = 1e-10);

		void remove_duplicate_points(Polygon &poly, const T tol = 1e-10)
		{
			remove_duplicate_points(poly.points, tol);
		}

		void remove_duplicate_points(Storage<Point> &poly, const T tol = 1e-10)
		{
			rdp.apply(poly, tol);
		}

	private:
		Vector u, v, n;
		AffineTransform<T, 3> trafo;
		IntersectConvexPolygons<T, 2> isect;
		RemoveDuplicatePoints<T, 3> rdp;
		Polygon2 ref_poly_1, ref_poly_2, ref_isect;
	};
}

#endif //MOONOLITH_INTERSECT_POLYGONS_HPP
