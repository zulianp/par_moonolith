#ifndef MOONOLITH_AFFINE_CONTACT_HPP
#define MOONOLITH_AFFINE_CONTACT_HPP

#include "moonolith_map_quadrature.hpp"
#include "moonolith_project_convex_polygons.hpp"
#include "moonolith_project_line_segments.hpp"
#include "moonolith_project_polygons.hpp"
#include "moonolith_project_polylines.hpp"
#include "moonolith_shape.hpp"
#include "moonolith_transform.hpp"

#include <memory>

namespace moonolith {

    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////// AFFINE Algorithms ///////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////

    template <typename T, int Dim>
    class AffineContact;

    template <typename T, int Dim>
    class AffineContact {};

    namespace private_ {

        template <typename T, int Dim>
        class AffineContactAlgorithm {
        public:
            static bool compute(AffineContact<T, Dim> &o) {
                assert(o.trafo_master);
                assert(o.trafo_slave);

                if (!o.project.project_intersect_and_map_quadrature(o.master,
                                                                    o.slave,
                                                                    // ref-quad-rule
                                                                    o.q_rule,
                                                                    o.q_master_physical,
                                                                    o.q_slave_physical)) {
                    return false;
                }

                const Integer n_qp = o.q_master_physical.n_points();

                assert(n_qp > 0);
                assert(n_qp == o.q_slave_physical.n_points());

                o.q_master.resize(n_qp);
                o.q_slave.resize(n_qp);
                o.gap.resize(n_qp);

                auto n = o.normal();

                bool ok_master = false, ok_slave = false;
                for (Integer k = 0; k < n_qp; ++k) {
                    ok_master = o.trafo_master->apply_inverse(o.q_master_physical.points[k], o.q_master.points[k]);
                    assert(ok_master);
                    MOONOLITH_UNUSED(ok_master);

                    ok_slave = o.trafo_slave->apply_inverse(o.q_slave_physical.points[k], o.q_slave.points[k]);
                    assert(ok_slave);
                    MOONOLITH_UNUSED(ok_slave);

                    o.q_master.weights[k] = o.q_master_physical.weights[k];
                    o.q_slave.weights[k] = o.q_slave_physical.weights[k];

                    // compute gap
                    o.gap[k] = dot(o.q_master_physical.points[k] - o.q_slave_physical.points[k], n);
                }

                return true;
            }
        };

    }  // namespace private_

    template <typename T>
    class AffineContact<T, 2> {
    public:
        friend class private_::AffineContactAlgorithm<T, 2>;

        using Point = moonolith::Vector<T, 2>;
        using Vector = moonolith::Vector<T, 2>;

        //////////////////////////////////////////////////////////////////

        // INPUT
        Line<T, 2> master, slave;
        Quadrature<T, 1> q_rule;
        std::shared_ptr<Transform<T, 1, 2>> trafo_master, trafo_slave;
        bool invert_plane_dir;

        // OUTPUT
        Quadrature<T, 1> q_master, q_slave;
        Quadrature<T, 2> q_master_physical, q_slave_physical;
        Storage<T> gap;

        inline const Vector &normal() const { return normal_; }

        //////////////////////////////////////////////////////////////////

        AffineContact() : invert_plane_dir(false) {}

        bool compute() {
            normal_ = slave.p1 - slave.p0;
            std::swap(normal_.x, normal_.y);
            normal_.x = -normal_.x;
            normal_ /= length(normal_);

            if (invert_plane_dir) {
                normal_ = -normal_;
            }

            return private_::AffineContactAlgorithm<T, 2>::compute(*this);
        }

    private:
        ProjectLineSegments<T> project;
        Vector normal_;
    };

    template <typename T>
    class AffineContact<T, 3> {
    public:
        friend class private_::AffineContactAlgorithm<T, 3>;
        using Point = moonolith::Vector<T, 3>;
        using Vector = moonolith::Vector<T, 3>;

        //////////////////////////////////////////////////////////////////

        // INPUT
        Polygon<T, 3> master, slave;
        Quadrature<T, 2> q_rule;
        std::shared_ptr<Transform<T, 2, 3>> trafo_master, trafo_slave;
        bool invert_plane_dir;

        // OUTPUT
        Quadrature<T, 2> q_master, q_slave;
        Quadrature<T, 3> q_master_physical, q_slave_physical;
        Storage<T> gap;

        inline const Vector &normal() const { return project.normal(); }

        //////////////////////////////////////////////////////////////////

        AffineContact() : invert_plane_dir(false) {}

        ///@brief first initialize master, slave, and q_rule, objects
        bool compute() {
            assert(!invert_plane_dir);
            return private_::AffineContactAlgorithm<T, 3>::compute(*this);
        }

    private:
        ProjectConvexPolygons<T> project;
    };
}  // namespace moonolith

#endif  // MOONOLITH_AFFINE_CONTACT_HPP
