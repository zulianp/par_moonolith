#ifndef MOONOLITH_VECTOR_HPP
#define MOONOLITH_VECTOR_HPP

#include <array>
#include <cassert>
#include <cmath>
#include <iostream>

#include "moonolith_base.hpp"
#include "moonolith_empirical_tol.hpp"
#include "moonolith_expanding_array.hpp"

namespace moonolith {

    template <typename T, int Dim>
    class Vector {
    public:
        static_assert(Dim > 0, "vector size must be positive");
        using Iterator = typename std::array<T, Dim>::iterator;
        using ConstIterator = typename std::array<T, Dim>::const_iterator;

        enum { n_dims = Dim };

        inline static constexpr int size() { return Dim; }

        Vector() { std::fill(values_.begin(), values_.end(), 0.); }

        T operator[](const int i) const {
            assert(i < Dim);
            return values_[i];
        }

        T &operator[](const int i) {
            assert(i < Dim);
            return values_[i];
        }

        T operator()(const int i) const { return (*this)[i]; }

        T &operator()(const int i) { return (*this)[i]; }

        inline friend T dot(const Vector &left, const Vector &right) {
            T ret = 0.0;
            for (int d = 0; d < Dim; ++d) {
                ret += left[d] * right[d];
            }

            return ret;
        }

        inline friend T length(const Vector &left) {
            using std::sqrt;

            T ret = 0.0;
            for (int d = 0; d < Dim; ++d) {
                ret += left[d] * left[d];
            }

            return sqrt(ret);
        }

        inline friend Vector operator+(const Vector &left, const Vector &right) {
            Vector ret;
            for (int d = 0; d < Dim; ++d) {
                ret[d] = left[d] + right[d];
            }

            return ret;
        }

        inline friend Vector operator-(const Vector &left, const Vector &right) {
            Vector ret;
            for (int d = 0; d < Dim; ++d) {
                ret[d] = left[d] - right[d];
            }

            return ret;
        }

        inline friend Vector operator*(const T &left, const Vector &right) {
            Vector ret;
            for (int d = 0; d < Dim; ++d) {
                ret[d] = left[d] * right[d];
            }

            return ret;
        }

        inline friend Vector operator*(const Vector &left, const T &right) {
            Vector ret;
            for (int d = 0; d < Dim; ++d) {
                ret[d] = left[d] * right;
            }

            return ret;
        }

        inline friend Vector operator/(const Vector &left, const T &right) {
            Vector ret;
            for (int d = 0; d < Dim; ++d) {
                ret[d] = left[d] / right;
            }

            return ret;
        }

        // in-place
        inline Vector &operator+=(const Vector &other) {
            for (int d = 0; d < Dim; ++d) {
                (*this)[d] += other[d];
            }

            return *this;
        }

        inline Vector &operator-=(const Vector &other) {
            for (int d = 0; d < Dim; ++d) {
                (*this)[d] -= other[d];
            }

            return *this;
        }

        inline Vector &operator*=(const T &other) {
            for (int d = 0; d < Dim; ++d) {
                (*this)[d] *= other[d];
            }

            return *this;
        }

        inline Vector &operator/=(const T &other) {
            for (int d = 0; d < Dim; ++d) {
                (*this)[d] /= other[d];
            }

            return *this;
        }

        inline friend Vector operator-(const Vector &other) {
            Vector ret;
            for (int d = 0; d < Dim; ++d) {
                ret[d] = -other[d];
            }

            return ret;
        }

        inline friend T distance(const Vector &left, const Vector &right) {
            using std::sqrt;

            T ret = 0.0;
            for (int d = 0; d < Dim; ++d) {
                const T v = left[d] - right[d];
                ret += v * v;
            }

            return sqrt(ret);
        }

        inline friend Vector normalize(const Vector &v) { return v * (1.0 / length(v)); }

        inline void set(const T value) { std::fill(values_.begin(), values_.end(), value); }

        inline void zero() { set(0.0); }

        inline Iterator begin() { return values_.begin(); }
        inline Iterator end() { return values_.end(); }

        inline ConstIterator begin() const { return values_.begin(); }
        inline ConstIterator end() const { return values_.end(); }

    private:
        std::array<T, Dim> values_;
    };

    template <typename T>
    class Vector<T, 1> {
    public:
        T x;

        using Iterator = T *;
        using ConstIterator = const T *;

        enum { n_dims = 1 };

        inline static constexpr int size() { return 1; }

        inline Vector(const T x = 0.0) : x(x) {}

        inline T operator[](const int i) const {
            assert(i == 0);
            MOONOLITH_UNUSED(i);
            return x;
        }

        inline T &operator[](const int i) {
            assert(i == 0);
            MOONOLITH_UNUSED(i);
            return x;
        }

        T operator()(const int i) const { return (*this)[i]; }

        T &operator()(const int i) { return (*this)[i]; }

        inline friend T dot(const Vector &left, const Vector &right) { return left.x * right.x; }

        inline friend T length(const Vector &left) { return std::abs(left.x); }

        inline friend Vector operator+(const Vector &left, const Vector &right) { return Vector(left.x + right.x); }

        inline friend Vector operator-(const Vector &left, const Vector &right) { return Vector(left.x - right.x); }

        inline friend Vector operator*(const T &left, const Vector &right) { return Vector(left * right.x); }

        inline friend Vector operator*(const Vector &left, const T &right) { return Vector(left.x * right); }

        inline friend Vector operator/(const Vector &left, const T &right) { return Vector(left.x / right); }

        // in-place
        inline Vector &operator+=(const Vector &other) {
            x += other.x;
            return *this;
        }

        inline Vector &operator-=(const Vector &other) {
            x -= other.x;
            return *this;
        }

        inline Vector &operator*=(const T &other) {
            x *= other;
            return *this;
        }

        inline Vector &operator/=(const T &other) {
            x /= other;
            return *this;
        }

        inline friend Vector operator-(const Vector &other) { return Vector(-other.x); }

        inline friend T distance(const Vector &left, const Vector &right) { return length(left - right); }

        inline friend Vector normalize(const Vector &v) { return v * (1.0 / length(v)); }

        inline void set(const T value) { x = value; }

        inline void zero() { set(0.0); }

        Iterator begin() { return &x; }
        Iterator end() { return &x + 1; }

        ConstIterator begin() const { return &x; }
        ConstIterator end() const { return &x + 1; }
    };

    template <typename T>
    class Vector<T, 2> {
    public:
        T x, y;

        enum { n_dims = 2 };

        inline static constexpr int size() { return 2; }

        inline Vector(const T x = 0.0, const T y = 0.0) : x(x), y(y) {}

        inline T operator[](const int i) const {
            switch (i) {
                case 0: {
                    return x;
                }
                case 1: {
                    return y;
                }
                default: {
                    assert(false);
                }
            }
            return 0;
        }

        inline T &operator[](const int i) {
            static T null_object = 0;
            switch (i) {
                case 0: {
                    return x;
                }
                case 1: {
                    return y;
                }
                default: {
                    assert(false);
                }
            }
            return null_object;
        }

        T operator()(const int i) const { return (*this)[i]; }

        T &operator()(const int i) { return (*this)[i]; }

        inline friend T dot(const Vector &left, const Vector &right) { return left.x * right.x + left.y * right.y; }

        inline friend T length(const Vector &left) { return sqrt(left.x * left.x + left.y * left.y); }

        inline friend Vector operator+(const Vector &left, const Vector &right) {
            return Vector(left.x + right.x, left.y + right.y);
        }

        inline friend Vector operator-(const Vector &left, const Vector &right) {
            return Vector(left.x - right.x, left.y - right.y);
        }

        inline friend Vector operator*(const T &left, const Vector &right) {
            return Vector(left * right.x, left * right.y);
        }

        inline friend Vector operator*(const Vector &left, const T &right) {
            return Vector(left.x * right, left.y * right);
        }

        inline friend Vector operator/(const Vector &left, const T &right) {
            return Vector(left.x / right, left.y / right);
        }

        // in-place
        inline Vector &operator+=(const Vector &other) {
            x += other.x;
            y += other.y;
            return *this;
        }

        inline Vector &operator-=(const Vector &other) {
            x -= other.x;
            y -= other.y;
            return *this;
        }

        inline Vector &operator*=(const T &other) {
            x *= other;
            y *= other;
            return *this;
        }

        inline Vector &operator/=(const T &other) {
            x /= other;
            y /= other;
            return *this;
        }

        inline friend Vector operator-(const Vector &other) { return Vector(-other.x, -other.y); }

        inline friend T distance(const Vector &left, const Vector &right) { return length(left - right); }

        inline friend Vector normalize(const Vector &v) { return v * (1.0 / length(v)); }

        inline void set(const T value) {
            x = value;
            y = value;
        }

        inline void zero() { set(0.0); }
    };

    template <typename T>
    class Vector<T, 3> {
    public:
        T x, y, z;

        enum { n_dims = 3 };

        inline static constexpr int size() { return 3; }

        Vector(const T x = 0.0, const T y = 0.0, const T z = 0.0) : x(x), y(y), z(z) {}

        T operator[](const int i) const {
            switch (i) {
                case 0: {
                    return x;
                }
                case 1: {
                    return y;
                }
                case 2: {
                    return z;
                }
                default: {
                    assert(false);
                }
            }
            return 0;
        }

        T &operator[](const int i) {
            static T null_object = 0;
            switch (i) {
                case 0: {
                    return x;
                }
                case 1: {
                    return y;
                }
                case 2: {
                    return z;
                }
                default: {
                    assert(false);
                }
            }
            return null_object;
        }

        T operator()(const int i) const { return (*this)[i]; }

        T &operator()(const int i) { return (*this)[i]; }

        inline friend T dot(const Vector &left, const Vector &right) {
            return left.x * right.x + left.y * right.y + left.z * right.z;
        }

        inline friend T length(const Vector &left) { return sqrt(left.x * left.x + left.y * left.y + left.z * left.z); }

        inline friend Vector operator+(const Vector &left, const Vector &right) {
            return Vector(left.x + right.x, left.y + right.y, left.z + right.z);
        }

        inline friend Vector operator-(const Vector &left, const Vector &right) {
            return Vector(left.x - right.x, left.y - right.y, left.z - right.z);
        }

        inline friend Vector operator*(const T &left, const Vector &right) {
            return Vector(left * right.x, left * right.y, left * right.z);
        }

        inline friend Vector operator*(const Vector &left, const T &right) {
            return Vector(left.x * right, left.y * right, left.z * right);
        }

        inline friend Vector operator/(const Vector &left, const T &right) {
            return Vector(left.x / right, left.y / right, left.z / right);
        }

        // in-place
        inline Vector &operator+=(const Vector &other) {
            x += other.x;
            y += other.y;
            z += other.z;
            return *this;
        }

        inline Vector &operator-=(const Vector &other) {
            x -= other.x;
            y -= other.y;
            z -= other.z;
            return *this;
        }

        inline Vector &operator*=(const T &other) {
            x *= other;
            y *= other;
            z *= other;
            return *this;
        }

        inline Vector &operator/=(const T &other) {
            x /= other;
            y /= other;
            z /= other;
            return *this;
        }

        inline friend Vector operator-(const Vector &other) { return Vector(-other.x, -other.y, -other.z); }

        inline friend T distance(const Vector &left, const Vector &right) { return length(left - right); }

        inline friend Vector cross(const Vector &left, const Vector &right) {
            return Vector((left.y * right.z) - (right.y * left.z),
                          (left.z * right.x) - (right.z * left.x),
                          (left.x * right.y) - (right.x * left.y));
        }

        inline friend Vector normalize(const Vector &v) { return v * (1.0 / length(v)); }

        inline void set(const T value) {
            x = value;
            y = value;
            z = value;
        }

        inline void zero() { set(0.0); }
    };

    /////////////////////////////////////////////////////////////////////////////////////////////

    template <typename T>
    class Vector<T, 4> {
    public:
        T x, y, z, w;

        enum { n_dims = 4 };

        inline static constexpr int size() { return 4; }

        Vector(const T x = 0.0, const T y = 0.0, const T z = 0.0, const T w = 0.0) : x(x), y(y), z(z), w(w) {}

        T operator[](const int i) const {
            switch (i) {
                case 0: {
                    return x;
                }
                case 1: {
                    return y;
                }
                case 2: {
                    return z;
                }
                case 3: {
                    return w;
                }
                default: {
                    assert(false);
                }
            }
            return 0;
        }

        T &operator[](const int i) {
            static T null_object = 0;
            switch (i) {
                case 0: {
                    return x;
                }
                case 1: {
                    return y;
                }
                case 2: {
                    return z;
                }
                case 3: {
                    return w;
                }
                default: {
                    assert(false);
                }
            }
            return null_object;
        }

        T operator()(const int i) const { return (*this)[i]; }

        T &operator()(const int i) { return (*this)[i]; }

        inline friend T dot(const Vector &left, const Vector &right) {
            return left.x * right.x + left.y * right.y + left.z * right.z + left.w * right.w;
        }

        inline friend T length(const Vector &left) {
            return sqrt(left.x * left.x + left.y * left.y + left.z * left.z + left.w * left.w);
        }

        inline friend Vector operator+(const Vector &left, const Vector &right) {
            return Vector(left.x + right.x, left.y + right.y, left.z + right.z, left.w + right.w);
        }

        inline friend Vector operator-(const Vector &left, const Vector &right) {
            return Vector(left.x - right.x, left.y - right.y, left.z - right.z, left.w - right.w);
        }

        inline friend Vector operator*(const T &left, const Vector &right) {
            return Vector(left * right.x, left * right.y, left * right.z, left.w * right.w);
        }

        inline friend Vector operator*(const Vector &left, const T &right) {
            return Vector(left.x * right, left.y * right, left.z * right, left.w + right);
        }

        inline friend Vector operator/(const Vector &left, const T &right) {
            return Vector(left.x / right, left.y / right, left.z / right, left.w / right.w);
        }

        // in-place
        inline Vector &operator+=(const Vector &other) {
            x += other.x;
            y += other.y;
            z += other.z;
            w += other.w;
            return *this;
        }

        inline Vector &operator-=(const Vector &other) {
            x -= other.x;
            y -= other.y;
            z -= other.z;
            w -= other.w;
            return *this;
        }

        inline Vector &operator*=(const T &other) {
            x *= other;
            y *= other;
            z *= other;
            w *= other;
            return *this;
        }

        inline Vector &operator/=(const T &other) {
            x /= other;
            y /= other;
            z /= other;
            w /= other;
            return *this;
        }

        inline friend Vector operator-(const Vector &other) { return Vector(-other.x, -other.y, -other.z, -other.w); }

        inline friend T distance(const Vector &left, const Vector &right) { return length(left - right); }

        // inline friend Vector cross(const Vector &left, const Vector &right)
        // {
        // 	return Vector( (left.y * right.z) - ( right.y * left.z ),
        // 		(left.z * right.x) - ( right.z * left.x ),
        // 		(left.x * right.y) - ( right.x * left.y ) );
        // }

        inline friend Vector normalize(const Vector &v) { return v * (1.0 / length(v)); }

        inline void set(const T value) {
            x = value;
            y = value;
            z = value;
            w = value;
        }

        inline void zero() { set(0.0); }
    };

    template <typename T, int Dim>
    bool approxeq(const Vector<T, Dim> &left, const Vector<T, Dim> &right, const T tol = GeometricTol<T>::value()) {
        using std::abs;
        for (int i = 0; i < Dim; ++i) {
            if (abs(left[i] - right[i]) > tol) {
                return false;
            }
        }

        return true;
    }

    inline static double distance(const double &left, const double &right) { return std::abs(left - right); }

    inline static long double distance(const long double &left, const long double &right) {
        return std::abs(left - right);
    }

    inline static float distance(const float &left, const float &right) { return std::abs(left - right); }

    template <typename T, int Dim>
    inline void fill(Vector<T, Dim> &v, const T &value) {
        v.set(value);
    }

    template <typename T, int Dim>
    void print(const Vector<T, Dim> &v, std::ostream &os = std::cout) {
        const auto n = v.size();
        os << "[ ";
        for (int d = 0; d < n; ++d) {
            os << v[d] << " ";
        }
        os << "]\n";
    }

    //////////////////////////////////////////////////////////////////////////////////////

    template <typename T>
    class Vector<T, -1> {
    public:
        inline int size() const { return static_cast<int>(values_.size()); }

        Vector() { std::fill(values_.begin(), values_.end(), 0.); }

        T operator[](const int i) const {
            assert(i < size());
            return values_[i];
        }

        T &operator[](const int i) {
            assert(i < size());

            return values_[i];
        }

        T operator()(const int i) const { return (*this)[i]; }

        T &operator()(const int i) { return (*this)[i]; }

        inline friend T dot(const Vector &left, const Vector &right) {
            const auto n = left.size();
            assert(n == right.size());

            T ret = 0.0;
            for (int d = 0; d < n; ++d) {
                ret += left[d] * right[d];
            }

            return ret;
        }

        inline friend T length(const Vector &left) {
            using std::sqrt;

            const auto n = left.size();

            T ret = 0.0;
            for (int d = 0; d < n; ++d) {
                ret += left[d] * left[d];
            }

            return sqrt(ret);
        }

        inline friend Vector operator+(const Vector &left, const Vector &right) {
            const auto n = left.size();
            assert(n == right.size());

            Vector ret;
            ret.resize(n);

            for (int d = 0; d < n; ++d) {
                ret[d] = left[d] + right[d];
            }

            return ret;
        }

        inline friend Vector operator-(const Vector &left, const Vector &right) {
            const auto n = left.size();
            assert(n == right.size());

            Vector ret;
            ret.resize(n);

            for (int d = 0; d < n; ++d) {
                ret[d] = left[d] - right[d];
            }

            return ret;
        }

        inline friend Vector operator*(const T &left, const Vector &right) {
            const auto n = left.size();
            assert(n == right.size());

            Vector ret;
            ret.resize(n);

            for (int d = 0; d < n; ++d) {
                ret[d] = left[d] * right[d];
            }

            return ret;
        }

        inline friend Vector operator*(const Vector &left, const T &right) {
            const auto n = left.size();
            assert(n == right.size());

            Vector ret;
            ret.resize(n);

            for (int d = 0; d < n; ++d) {
                ret[d] = left[d] * right;
            }

            return ret;
        }

        inline friend Vector operator/(const Vector &left, const T &right) {
            const auto n = left.size();
            assert(n == right.size());

            Vector ret;
            ret.resize(n);

            for (int d = 0; d < n; ++d) {
                ret[d] = left[d] / right;
            }

            return ret;
        }

        // in-place
        inline Vector &operator+=(const Vector &other) {
            const auto n = size();

            for (int d = 0; d < n; ++d) {
                (*this)[d] += other[d];
            }

            return *this;
        }

        inline Vector &operator-=(const Vector &other) {
            const auto n = size();

            for (int d = 0; d < n; ++d) {
                (*this)[d] -= other[d];
            }

            return *this;
        }

        inline Vector &operator*=(const T &other) {
            const auto n = size();

            for (int d = 0; d < n; ++d) {
                (*this)[d] *= other[d];
            }

            return *this;
        }

        inline Vector &operator/=(const T &other) {
            const auto n = size();

            for (int d = 0; d < n; ++d) {
                (*this)[d] /= other[d];
            }

            return *this;
        }

        inline friend Vector operator-(const Vector &other) {
            const auto n = other.size();

            Vector ret;
            for (int d = 0; d < n; ++d) {
                ret[d] = -other[d];
            }

            return ret;
        }

        inline friend T distance(const Vector &left, const Vector &right) {
            using std::sqrt;

            const auto n = left.size();
            assert(n == right.size());

            T ret = 0.0;
            for (int d = 0; d < n; ++d) {
                const T v = left[d] - right[d];
                ret += v * v;
            }

            return sqrt(ret);
        }

        inline friend Vector normalize(const Vector &v) { return v * (1.0 / length(v)); }

        inline void set(const T value) { std::fill(values_.begin(), values_.end(), value); }

        inline void resize(const int n) { values_.resize(n); }

        inline void zero() { set(0.0); }

    private:
        Storage<T> values_;
    };

    // specialization of zero
    template <typename T>
    class Vector<T, 0> {};

    template <typename T, int N>
    class Resize<Vector<T, N> > {
    public:
        void static apply(const Vector<T, N> &, const std::size_t n) {
            assert(static_cast<int>(n) == N);
            MOONOLITH_UNUSED(n);
        }
    };

    template <typename T>
    class Resize<Vector<T, -1> > {
    public:
        void static apply(Vector<T, -1> &v, const std::size_t n) { v.resize(static_cast<int>(n)); }
    };

    template <typename T, int N>
    Vector<T, N> reciprocal(const Vector<T, N> &v, const T &num = 1.0) {
        const int n = v.size();
        Vector<T, N> ret;
        resize(ret, n);

        for (int i = 0; i < n; ++i) {
            ret[i] = num / v[i];
        }

        return ret;
    }

}  // namespace moonolith

#endif  //<MOONOLITH_VECTOR_HPP
