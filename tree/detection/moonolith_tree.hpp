#ifndef MOONOLITH_TREE_HPP
#define MOONOLITH_TREE_HPP

#include "moonolith_branch.hpp"
#include "moonolith_breadth_first_navigator.hpp"
#include "moonolith_detector.hpp"
#include "moonolith_forward_declarations.hpp"
#include "moonolith_iterable.hpp"
#include "moonolith_leaf.hpp"
#include "moonolith_mutator.hpp"
#include "moonolith_node.hpp"
#include "moonolith_refine_all.hpp"
#include "moonolith_root.hpp"
#include "moonolith_tree_memory.hpp"

#include <assert.h>
#include <memory>

namespace moonolith {

    template <class Traits>
    class Tree : public Detector<Traits> {
    public:
        typedef typename Traits::DataType DataType;
        typedef typename Traits::Bound Bound;

        typedef moonolith::Node<Traits> Node;
        typedef moonolith::Branch<Traits> Branch;
        typedef moonolith::Leaf<Traits> Leaf;
        typedef moonolith::Root<Traits> Root;

        typedef moonolith::TreeMemory<Traits> TreeMemory;
        typedef typename TreeMemory::DataContainer DataContainer;
        typedef typename Node::ConstDataIter DataHandleIter;

        typedef std::shared_ptr<Node> NodePtr;

        const NodePtr &root() const { return memory_.node(NodeHandle(0)); }

        inline const NodePtr &node(const NodeHandle &handle) const { return memory_.node(handle); }

        inline void set_node(const NodeHandle &handle, const NodePtr &node) const { memory_.set_node(handle, node); }

        inline DataType &data(const DataHandle &handle) { return memory_.data(handle); }

        inline const DataType &data(const DataHandle &handle) const { return memory_.data(handle); }

        inline DataType &operator[](const DataHandle &handle) { return memory_.data(handle); }

        inline const DataType &operator[](const DataHandle &handle) const { return memory_.data(handle); }

        void reserve(const Integer nData) { memory_.reserve(nData); }

        inline std::shared_ptr<Branch> new_branch() { return memory_.new_branch(); }

        inline std::shared_ptr<Leaf> new_leaf() { return memory_.new_leaf(); }

        inline std::shared_ptr<Root> new_root() { return memory_.new_root(); }

        bool can_refine_search_from(const NodeHandle &node_handle) {
            if (node(node_handle)->is_branch()) return true;

            std::shared_ptr<Leaf> leaf = std::static_pointer_cast<Leaf>(node(node_handle));
            return can_refine(*leaf);
        }

        bool can_refine(const Leaf &leaf) {
            std::shared_ptr<Mutator<Traits> > m_can_refine = memory_.mutator_factory()->new_can_refine();
            node(leaf.handle())->accept(*m_can_refine, memory_);
            return m_can_refine->success();
        }

        bool refine(const NodeHandle &handle) {
            std::shared_ptr<Mutator<Traits> > m_refine = memory_.mutator_factory()->new_refine();
            node(handle)->accept(*m_refine, memory_);
            assert(m_refine->success());
            return m_refine->success();
        }

        void refine() {
            std::shared_ptr<Mutator<Traits> > m = memory_.mutator_factory()->new_refine_all();
            apply_lazy_mutators();
            BreadthFirstNavigator<Tree> nav;
            nav.set_verbose(true);
            nav.walk(*this, *m);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////// Overridden Detector methods
        ///////////////////////////////////////////////

        ///@brief insert an element in the detector
        virtual DataHandle insert(const DataType &data) {
            DataHandle handle = memory_.add_data(data);

            std::shared_ptr<Mutator<Traits> > m_insert = memory_.mutator_factory()->new_insert(handle, data);
            lazy_apply(m_insert);

            std::shared_ptr<Mutator<Traits> > m_bound_expander = memory_.mutator_factory()->new_bound_expand(data);
            root()->accept(*m_bound_expander, memory_);
            return handle;
        }

        bool expand_bound(const Bound &bound) {
            std::shared_ptr<Mutator<Traits> > m_bound_expander = memory_.mutator_factory()->new_bound_expand(bound);
            root()->accept(*m_bound_expander, memory_);
            return m_bound_expander->success();
        }

        const Bound &bound() const { return root()->bound(); }

        void lazy_apply(const std::shared_ptr<Mutator<Traits> > &m) { mutators_.push_back(m); }

        inline Integer n_nodes() const { return memory_.n_nodes(); }

        ///@brief to be called when beginning to insert, modify or change inserted data or the structure contained by
        /// the detector
        virtual void editing_begin() {
            // TODO
            assert(false);
        }

        ///@brief to be called when editing is finished
        virtual void editing_end() {
            finalize();
            // TODO
            assert(false);
        }

        ///@brief find in the other processes where there is some matching data data
        virtual bool build_lookup_table() {
            // TODO
            assert(false);
            return false;
        }

        bool apply(const std::shared_ptr<Mutator<Traits> > &mutator) {
            apply_lazy_mutators();
            BreadthFirstNavigator<Tree> nav;
            nav.walk(*this, *mutator);
            return mutator->success();
        }

        void walk(Visitor<Traits> &visitor) {
            apply_lazy_mutators();
            BreadthFirstNavigator<Tree> nav;
            nav.walk(*this, visitor);
        }

        void root_accept(Visitor<Traits> &visitor) { root()->accept(visitor, memory_); }

        bool is_visitable() { return root() && root()->is_visitable(); }

        bool make_visitable() {
            if (is_visitable()) return true;

            if (!root()) {
                // should never happen
                assert(false);
                return false;
            }

            if (root()->is_visitable()) return true;

            return refine(0);
        }

        TreeMemory &memory() { return memory_; }

        const TreeMemory &memory() const { return memory_; }

        inline void set_mutator_factory(const std::shared_ptr<MutatorFactory<Traits> > &mf) {
            memory_.set_mutator_factory(mf);
        }

        inline void finalize() { apply_lazy_mutators(); }

        void clear() {
            memory_.clear();
            mutators_.clear();
        }

        const std::shared_ptr<MutatorFactory<Traits> > &mutator_factory() const { return memory_.mutator_factory(); }

    private:
        typedef std::shared_ptr<Mutator<Traits> > MutatorPtr;
        typedef std::vector<MutatorPtr> MutatorContainer;

        TreeMemory memory_;
        MutatorContainer mutators_;

        void apply_lazy_mutators() {
            for (typename MutatorContainer::iterator it = mutators_.begin(); it != mutators_.end(); ++it) {
                BreadthFirstNavigator<Tree> nav;
                nav.walk(*this, **it);
            }

            mutators_.clear();
        }
    };
}  // namespace moonolith

#endif  // MOONOLITH_TREE_HPP
