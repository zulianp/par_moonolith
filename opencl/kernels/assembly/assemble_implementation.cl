
#include "assemble_interfaces.cl"

#include "quad_meta.cl"
#include "quadrature.cl"

#include "local_and_global_blocks.cl"
#include "local_assembly.cl"

#include "make_assembly_values.cl"

#include "assembly_macros.cl"
#include "assemble_with_values.cl"




