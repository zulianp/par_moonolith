#ifndef MORTAR_SURFACE_CL
#define MORTAR_SURFACE_CL 

#include "mortar/mortar_interfaces.cl"
#include "fe/fe_interfaces.cl"
#include "assembly/quad_meta.cl"
#include "mortar/volume_interface.cl"
#include "mortar/intersect_and_mortar_assemble.cl"
#include "mortar_surface_interface.cl"

static void intersect_ray_with_plane(const SizeType n_dims, const SizeType n_rays, const Scalar *origin, const Scalar *dir, const Scalar *plane_normal,  const Scalar plane_distance, Scalar *distance)
{
	for(SizeType i = 0; i < n_rays; ++i) {
		const SizeType i_d = i * n_dims;

		switch(n_dims) {
			case 3: {
				if(!intersect_ray_with_plane_3(
					&origin[i_d], &dir[i_d], 
					plane_normal, plane_distance, 
					&distance[i] , DEFAULT_TOLLERANCE)) 
				{
					KERNEL_ASSERT(false, "intersect_ray_with_plane: should not ever happen");
				}

				break;
			}

			case 2: {
				if(!intersect_ray_with_plane_2(
					&origin[i_d], &dir[i_d], 
					plane_normal, plane_distance, 
					&distance[i] , DEFAULT_TOLLERANCE)) 
				{
					KERNEL_ASSERT(false, "intersect_ray_with_plane: should not ever happen");
				}
				break;
			}

			default: 
			{
				KERNEL_ASSERT(false, "intersect_ray_with_plane: n_dims not supported");
				break;
			}
		}
	}
}

void local_index_of_side(const SurfaceMesh surf, const SizeType element, SizeType * local_index)
{
	const SizeType parent_element = surf.parent_element[element];
	
	const SizeType begin = surf.el_ptr[element];
	const SizeType end   = surf.el_ptr[element + 1];

	const SizeType parent_begin 	= surf.parent_mesh->el_ptr[parent_element];
	const SizeType parent_end 		= surf.parent_mesh->el_ptr[parent_element + 1];
	const SizeType n_parent_nodes 	= parent_end - parent_begin;

	SizeType index = 0;
	for(SizeType k = begin; k < end; ++k) {
		const SizeType node = surf.el_index[k];

		for(SizeType i = 0; i < n_parent_nodes; ++i) {
			if(node == surf.parent_mesh->el_index[parent_begin + i]) {
				local_index[index++] = i;
				break;
			}
		}
	}
}

void compute_normals_at_points(const FEObject fe, const Scalar *reference_normal, Scalar *normals)
{
	const SizeType n_dims = fe.n_dims;
	const SizeType n_dims_2 = n_dims * n_dims;
	for(SizeType q = 0; q < fe.n_quad_points; ++q) {
		const SizeType q_d = n_dims   * q;
		const SizeType q_dxd   = n_dims_2 * q;

		mat_mat_mul(1, n_dims, n_dims, reference_normal, &fe.inverse_jacobian[q_dxd], &normals[q_d]);
		vec_scale(n_dims, 1.0/norm_n(n_dims, &normals[q_d]), &normals[q_d]);
	}
}

static void remove_last_dim(const SizeType n_dims, const SizeType n_points, const Scalar *in, Scalar *out)
{
	const SizeType n_dims_m1 = n_dims - 1;
	for(SizeType i = 0; i < n_points; ++i) {
		const SizeType i_sub = i * n_dims_m1;
		const SizeType i_stride = i * n_dims;

		for(SizeType d = 0; d < n_dims_m1; ++d) {
			out[i_sub + d] = in[i_stride + d];
		}
	}
}

static void polygon_make_ccw_2(const SizeType n_points, Scalar *polygon)
{
	KERNEL_ASSERT(n_points >= 3, "polygon_make_ccw_2: must be at least a triangle");

	const Scalar area = polygon_area_2(3, polygon);
	if(area >= 0) {
		return;
	}

	const SizeType half_n = n_points / 2;
	for(SizeType i = 0; i < half_n; ++i) {
		const SizeType i2 = i * 2;
		const SizeType i2_inv = (n_points - 1 - i) * 2;

		generic_swap(Scalar, polygon[i2], 	  polygon[i2_inv]);
		generic_swap(Scalar, polygon[i2 + 1], polygon[i2_inv + 1]);
	}
}


 // @param poly_slave 		 is element of n_points_slave  x 2
 // @param projection_master is element of return x 3
 // @param projection_slave  is element of return x 3
 // @return the number of intersection points
	static SizeType project_surface_poly_onto_ref_poly(const SizeType n_dims,
		const SizeType n_points_master, const Scalar * poly_master,
		const SizeType n_points_slave,  const Scalar * poly_slave,
		Scalar * projection_master, Scalar * projection_slave)
	{
		Scalar normal 		   [MAX_N_DIMS];
		Scalar projected_master[MAX_SHAPE_FUNCS * (MAX_N_DIMS-1)];
		Scalar projected_slave [MAX_SHAPE_FUNCS * (MAX_N_DIMS-1)];

		remove_last_dim(n_dims, n_points_master, poly_master, projected_master);

		polygon_make_ccw_2(n_points_master, projected_master);

		SizeType n_vertices_result = 0;
		if(!intersect_convex_polygons(
			n_points_slave, poly_slave, 
			n_points_master, projected_master, 
			&n_vertices_result, projected_slave, 
			DEFAULT_TOLLERANCE)) {
			return 0;
	}

	const Vector3 u = vec_3(poly_master[3] - poly_master[0], 
		poly_master[4] - poly_master[1], 
		poly_master[5] - poly_master[2]);

	const Vector3 v = vec_3(poly_master[6] - poly_master[0], 
		poly_master[7] - poly_master[1], 
		poly_master[8] - poly_master[2]);

	const Vector3 n = normalize(cross(u, v));

	normal[0] = n[0];
	normal[1] = n[1];
	normal[2] = n[2];

	const Scalar d = dot_n(n_dims, normal, poly_master);
	const Scalar line_proj = n[2];

			//project isect back to master
	for(SizeType i = 0; i < n_vertices_result; ++i) {
		const SizeType i2 = i * 2;
		const SizeType i3 = i * 3;

				////////////////////////////////////////////////////////////////////
		projection_master[i3] 	  = projected_slave[i2];
		projection_master[i3 + 1] = projected_slave[i2 + 1];
		projection_master[i3 + 2] = 0;

		const Scalar point_dist   = point_plane_distance(n_dims, normal, d, &projection_master[i3]);
		projection_master[i3 + 2] = -point_dist / line_proj; 

				////////////////////////////////////////////////////////////////////
		projection_slave[i3] 	 = projected_slave[i2];
		projection_slave[i3 + 1] = projected_slave[i2 + 1];
		projection_slave[i3 + 2] = 0;
	}

	return n_vertices_result;
}


SizeType surface_mesh_n_points(const SurfaceMesh mesh, const SizeType element)
{
	return mesh.el_ptr[element + 1] - mesh.el_ptr[element];
}

void surface_mesh_points(const SurfaceMesh mesh, const SizeType element, Scalar *points)
{
	const SizeType n_dims = mesh.parent_mesh->n_dims ;
	const SizeType begin  = mesh.el_ptr[element];
	const SizeType end 	  = mesh.el_ptr[element + 1];

	SizeType index = 0;
	for(SizeType k = begin; k < end; ++k, ++index) {
		const SizeType index_d = n_dims * index;
		const SizeType node_d  = n_dims * mesh.el_index[k];	
		generic_copy(n_dims, &mesh.parent_mesh->points[node_d], &points[index_d]);
	}
}

void line_normal(const Scalar *line, Scalar *normal) {	
	normal[0] = line[3] - line[1];
	normal[1] = -line[2] - line[0];

	const Scalar l = norm_n(2, normal);
	normal[0] /= l;
	normal[1] /= l;
}

static void line_make_affine_transform_2(const Scalar *line, Scalar *A, Scalar *b)
{
	b[0] = line[0];
	b[1] = line[1];

	//col 0
	A[0] = line[2] - b[0];
	A[2] = line[3] - b[1];

	//col 1
	A[1] =  A[2];
	A[3] = -A[0];

	const Scalar l = sqrt(A[1] * A[1] + A[3] * A[3]);
	A[1] /= l;
	A[3] /= l;
}

bool mortar_surface_line_line(
	const SurfaceMesh surf_master,
	const SizeType element_master,
	const SurfaceMesh surf_slave,
	const SizeType element_slave,
	const SizeType order,
	Scalar *local_matrix,
	Scalar *local_normals,
	Scalar *local_gap
	)
{
	Scalar A   [2 * 2], b   [2];
	Scalar Ainv[2 * 2], binv[2];

	Scalar normal_master[2];
	Scalar normal_slave [2];

	SurfaceMortarWorkspace w;

	const SizeType parent_element_master = surf_master.parent_element[element_master];
	const SizeType parent_element_slave  = surf_slave.parent_element [element_slave ];

	surface_mesh_points(surf_master, element_master, w.surf_el_master);
	surface_mesh_points(surf_slave, element_slave,   w.surf_el_slave);

	const SizeType n_points_master = surface_mesh_n_points(surf_master, element_master);

	//////////////////////////////////////////////////////////////////////////////////////////
	//computing geometric surface projection

	//moving from global space to reference space
	line_make_affine_transform_2(w.surf_el_slave, A, b);
	make_inverse_affine_transform_2(A, b, Ainv, binv);

	apply_affine_transform_2(Ainv, binv, n_points_master, w.surf_el_master, w.ref_points_master);

	Scalar x_min, y_min;
	Scalar x_max, y_max;

	if(w.ref_points_master[0] < w.ref_points_master[2]) {
		x_min = w.ref_points_master[0];
		y_min = w.ref_points_master[1];

		x_max = w.ref_points_master[2];
		y_max = w.ref_points_master[3];
	} else {
		x_min = w.ref_points_master[2];
		y_min = w.ref_points_master[3];

		x_max = w.ref_points_master[0];
		y_max = w.ref_points_master[1];
	}

	//check if there is any intersection
	if(x_max <= 0) {
		return false;
	}

	if(x_min >= 1) {
		return false;
	}

	const Scalar x_min_isect = max(x_min, (Scalar)0);
	const Scalar x_max_isect = min(x_max, (Scalar)1);

	const Scalar dx = (x_max - x_min);
	const Scalar dy = (y_max - y_min);

	const Scalar y_min_isect = (x_min_isect - x_min) / dx * dy + y_min;
	const Scalar y_max_isect = (x_max_isect - x_min) / dx * dy + y_min;

	//store intersection lines
	w.isect_master[0] = x_min_isect;
	w.isect_master[1] = y_min_isect; 	 
	w.isect_master[2] = x_max_isect;
	w.isect_master[3] = y_max_isect; 

	w.isect_slave[0] = x_min_isect;
	w.isect_slave[1] = 0;
	w.isect_slave[2] = x_max_isect;
	w.isect_slave[3] = 0;

	const Scalar inv_area_slave = 2.0/( det_2(A) );

	//////////////////////////////////////////////////////////////////////////////////////////
	//create master fe object from intersection

	//move back to global coordinates
	apply_affine_transform_2(A, b, 2, w.isect_master, w.projection_master);

	//go to reference master (2d)
	inverse_transform_2(*surf_master.parent_mesh, parent_element_master, 2, w.projection_master, w.ref_projection_master);

	const SizeType n_quad_points = 
	make_quadrature_points_from_polyline_2(
		2, w.ref_projection_master, inv_area_slave, 
		order, w.quad_points,  w.quad_weights);

	make_fe_object_from_quad_2(*surf_master.parent_mesh, parent_element_master, n_quad_points, w.quad_points, w.quad_weights, &w.fe_master); 

	//////////////////////////////////////////////////////////////////////////////////////////
	//create slave fe object from intersection

	//move back to global coordinates
	apply_affine_transform_2(A, b, 2, w.isect_slave,  w.projection_slave);

	//move to reference slave (2d)
	inverse_transform_2(*surf_slave.parent_mesh, parent_element_slave, 2, w.projection_slave, w.ref_projection_slave);

	make_quadrature_points_from_polyline_2(
		2, w.ref_projection_slave, inv_area_slave, 
		order, w.quad_points,  w.quad_weights);

	make_fe_object_from_quad_2(*surf_slave.parent_mesh, parent_element_slave, n_quad_points, w.quad_points, w.quad_weights, &w.fe_slave); 

	//////////////////////////////////////////////////////////////////////////////////////////

	mortar_assemble(&w.fe_master, &w.fe_slave, local_matrix);

	line_normal(w.surf_el_master, normal_master);
	line_normal(w.surf_el_slave,  normal_slave);

	vec_repeat(n_quad_points, 2, normal_slave, w.ray_directions);

	transform_2(*surf_slave.parent_mesh, parent_element_slave, n_quad_points, w.quad_points, w.ray_origins);

	intersect_ray_with_plane(2, n_quad_points, w.ray_origins, w.ray_directions, normal_master, dot_n(2, normal_master, w.surf_el_master), w.ray_intersections);

	//gap
	const SizeType n_entries = w.fe_slave.n_shape_functions;

	//normals
	const SizeType n_vector_entries = w.fe_slave.n_shape_functions * surf_slave.parent_mesh->n_dims;

	generic_set(n_vector_entries, 0, local_normals);
	generic_set(n_entries, 0, local_gap);

// #ifdef CLIPP_HOST_CL
// 	{
// 		using namespace express;
// 		using namespace geom;

// 		ConstMatrixView<Scalar> master(w.surf_el_master, 2, 2);
// 		ConstMatrixView<Scalar> slave(w.surf_el_slave, 2, 2);

// 		plotl(master.transposed(), "master", "0, 255, 0");
// 		plotl(slave.transposed(),  "slave",  "0, 0, 255");

// 	}
// 	{
// 		using namespace express;
// 		using namespace geom;

// 		ConstMatrixView<Scalar> master(w.projection_master, 2, 2);
// 		ConstMatrixView<Scalar> slave(w.projection_slave, 2, 2);
// 		ConstMatrixView<Scalar> slave_gpoints(w.fe_slave.point, n_quad_points, 2);
// 		ConstMatrixView<Scalar> master_gpoints(w.fe_master.point, n_quad_points, 2);

// 		plotp(master.transposed(), "0, 255, 0", "master");
// 		plotp(slave.transposed(), "0, 0, 255", "slave");
// 		plotp(slave_gpoints.transposed(),  "0, 0, 0", "slave_gpts");
// 		plotp(master_gpoints.transposed(), "0, 0, 0", "master_gpts");

// 		printf("projection_slave (line): %g\n", distance_n(2, w.projection_slave, &w.projection_slave[2])); //DBG
// 	}
// #endif	

	for(SizeType q = 0; q < n_quad_points; ++q) {
		integrate_vec_at_quad_index(q, &w.fe_slave, 2, &w.ray_directions[q*2],  local_normals);
		integrate_vec_at_quad_index(q, &w.fe_slave, 1, &w.ray_intersections[q], local_gap);
	}

	return true;
}

bool mortar_surface_triangle_triangle(
	const SurfaceMesh surf_master,
	const SizeType element_master,
	const SurfaceMesh surf_slave,
	const SizeType element_slave,
	const SizeType order,
	Scalar *local_matrix,
	Scalar *local_normals,
	Scalar *local_gap
	)
{
	Scalar A   [3 * 3], b   [3];
	Scalar Ainv[3 * 3], binv[3];

	Scalar normal_master[3];
	Scalar normal_slave [3];

	const Scalar ref_polygon[3 * 2] = 
	{
		0, 0, 
		1, 0, 
		0, 1 
	};

	const SizeType n_dims = surf_master.parent_mesh->n_dims;
	
	//////////////////////////////////////////////////////////////////////
	///////////////// To be moved to a workspace object //////////////////
	//////////////////////////////////////////////////////////////////////

	FEObject fe_master, fe_slave;

	Scalar surf_el_master		[MAX_SHAPE_FUNCS   * MAX_N_DIMS];
	Scalar surf_el_slave		[MAX_SHAPE_FUNCS   * MAX_N_DIMS];

	Scalar ref_projection_master[MAX_N_PROJ_POINTS * MAX_N_DIMS];
	Scalar ref_projection_slave	[MAX_N_PROJ_POINTS * MAX_N_DIMS];
	Scalar projection_master	[MAX_N_PROJ_POINTS * MAX_N_DIMS];
	Scalar projection_slave		[MAX_N_PROJ_POINTS * MAX_N_DIMS];
	Scalar ref_points_master	[MAX_N_PROJ_POINTS * MAX_N_DIMS];

	Scalar isect_master			[MAX_N_PROJ_POINTS * MAX_N_DIMS];
	Scalar isect_slave			[MAX_N_PROJ_POINTS * MAX_N_DIMS];

	Scalar quad_points 			[MAX_QUAD_POINTS   * MAX_N_DIMS];
	Scalar quad_weights			[MAX_QUAD_POINTS];

	Scalar ray_origins			[MAX_QUAD_POINTS   * MAX_N_DIMS];
	Scalar ray_directions		[MAX_QUAD_POINTS   * MAX_N_DIMS];
	Scalar ray_intersections 	[MAX_QUAD_POINTS];

	//////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////

	const SizeType parent_element_master = surf_master.parent_element[element_master];
	const SizeType parent_element_slave  = surf_slave.parent_element [element_slave ];

	surface_mesh_points(surf_master, element_master, surf_el_master);
	surface_mesh_points(surf_slave, element_slave,  surf_el_slave);

	const SizeType n_points_master = surface_mesh_n_points(surf_master, element_master);
	const SizeType n_points_slave  = surface_mesh_n_points(surf_slave, element_slave);

	//moving from global space to reference space
	triangle_make_affine_transform_3(surf_el_slave, A, b);
	make_inverse_affine_transform_3(A, b, Ainv, binv);
	apply_affine_transform_3(Ainv, binv, n_points_master, surf_el_master, ref_points_master);

	//project in the reference element
	const SizeType n_projection_points = project_surface_poly_onto_ref_poly(
		n_dims,
		n_points_master, ref_points_master,
		n_points_slave,  ref_polygon, 
		isect_master, 	 isect_slave); 

	if(!n_projection_points) {
		return false;
	}

	apply_affine_transform_3(A, b, n_projection_points, isect_master, projection_master);
	inverse_transform_3(*surf_master.parent_mesh, parent_element_master, n_projection_points, projection_master, ref_projection_master);

	apply_affine_transform_3(A, b, n_projection_points, isect_slave,  projection_slave);
	inverse_transform_3(*surf_slave.parent_mesh, parent_element_slave,  n_projection_points, projection_slave,  ref_projection_slave);

	const Scalar inv_area_slave = 1/polygon_area_3(n_points_slave, surf_el_slave);

	//make quad points for projection_master 
	const SizeType n_quad_points = 
	make_quadrature_points_from_polygon_3(n_projection_points, ref_projection_master, inv_area_slave, 
		order, quad_points,  quad_weights);

	make_fe_object_from_quad_3(*surf_master.parent_mesh, parent_element_master, n_quad_points, quad_points, quad_weights, &fe_master); 

	//make quad points for projection_slave
	make_quadrature_points_from_polygon_3(n_projection_points, ref_projection_slave, inv_area_slave, 
		order, quad_points, quad_weights);

	make_fe_object_from_quad_3(*surf_slave.parent_mesh, parent_element_slave, n_quad_points, quad_points,  quad_weights, &fe_slave);  

	//gap
	const SizeType n_entries = fe_slave.n_shape_functions;

	//normals
	const SizeType n_vector_entries = fe_slave.n_shape_functions * surf_slave.parent_mesh->n_dims;

	//assemble mass_matrix
	mortar_assemble(&fe_master, &fe_slave, local_matrix);

	//assemble gap 
	triangle_normal(surf_el_master, normal_master);

	triangle_normal(surf_el_slave, normal_slave);
	vec_repeat(n_quad_points, 3, normal_slave, ray_directions);

	transform_3(*surf_slave.parent_mesh, parent_element_slave, n_quad_points, quad_points, ray_origins);
	intersect_ray_with_plane(n_dims, n_quad_points, ray_origins, ray_directions, normal_master, dot_n(3, normal_master, surf_el_master), ray_intersections);

	generic_set(n_vector_entries, 0, local_normals);
	generic_set(n_entries, 0, local_gap);

	for(SizeType q = 0; q < n_quad_points; ++q) {
		integrate_vec_at_quad_index(q, &fe_slave, n_dims, &ray_directions[q*n_dims], local_normals);
		integrate_vec_at_quad_index(q, &fe_slave, 1, &ray_intersections[q], local_gap);
	}

	return true;
}

//Steps:
//Identify which have sides that are in contact, 
//Determine wich sides are masters and which are slaves
//Link the sides to their parent element
//Compute normal projection (housholder reflection, orthogonal projection, intersection)
//Generate quad-points
//Assemble:
//option 1: Compute transformations to reference element ( A) inverse_trafo of volume element, B) householder for x(y)-plane, C) Euclidian transformation to reference element)
//option 2: assemble with respect to the volume elements
//Compute Jacobian of volumetric element (orientation has to be taken care of depending on side number -> J = A * B * C)
//Assemble gap, coupling matrix, and mass matrix
m_kernel__ void mortar_surface_self_contact(
	//input
	//A) basic data
	const SizeType n_dims,			
	const SizeType n_codims,		
	const SizeType n_elements,		
	const m_global__ SizeType * quad_meta,

	//B) Mesh
	m_global__ SizeType * el_ptr, 
	m_global__ SizeType * el_index,
	m_global__ SizeType * el_type,

	//C) point data
	const SizeType n_nodes,
	m_global__ Scalar * points,

	//D) surf data
	const SizeType n_surf_elements,
	const m_global__ SizeType * surf_el_ptr,
	const m_global__ SizeType * surf_el_index,
	const m_global__ SizeType * side_to_element,

	//E) contact data [IN]
	const SizeType n_pairs,					
	const m_global__ SizeType * pairs,

	//F) contact data [OUT]	
	m_global__ SizeType * element_matrix_offsets,
	m_global__ SizeType * element_scalar_offsets,
	m_global__ Scalar * element_matrices,		
	m_global__ Scalar * weighted_normals,		
	m_global__ Scalar * weighted_gap,			
	m_global__ SizeType * pairs_intersect		
	)
{
	Mesh mesh;
	mesh.n_dims 	= n_dims;
	mesh.n_elements = n_elements;
	mesh.el_ptr 	= el_ptr;
	mesh.el_index 	= el_index;
	mesh.el_type 	= el_type;
	mesh.n_nodes 	= n_nodes;
	mesh.points 	= points;

	SurfaceMesh surf;
	surf.n_elements  	= n_surf_elements;
	surf.el_ptr 	 	= surf_el_ptr;
	surf.el_index 	 	= surf_el_index;
	surf.parent_element = side_to_element;
	surf.parent_mesh 	= &mesh;

	Scalar local_matrix			[MAX_SHAPE_FUNCS * MAX_SHAPE_FUNCS];
	Scalar local_normals		[MAX_SHAPE_FUNCS * MAX_N_DIMS];
	Scalar local_gap			[MAX_QUAD_POINTS];

	for(SizeType i = get_global_id(0); i < n_pairs; i+= get_global_size(0)) {
		const SizeType i2 = i * 2;
		const SizeType element_master = pairs[i2];
		const SizeType element_slave  = pairs[i2 + 1];

		const SizeType parent_element_master = surf.parent_element[element_master];
		const SizeType parent_element_slave  = surf.parent_element[element_slave ];

		const SizeType master_type = mesh.el_type[element_master];
		const SizeType slave_type  = mesh.el_type[element_slave];

		const SizeType order_master = get_order(parent_element_master, quad_meta);
		const SizeType order_slave  = get_order(parent_element_slave,  quad_meta);
		const SizeType order 		= mortar_quad_order(order_master, order_slave);

		bool intersected = false;
		switch(master_type) {
			case ELEMENT_TYPE_TETRA:
			{
				switch(slave_type) 
				{
					case ELEMENT_TYPE_TETRA:
					{
						intersected = mortar_surface_triangle_triangle(surf, element_master, surf, element_slave, order, local_matrix, local_normals, local_gap);
						break;
					}

					default:
					{
						KERNEL_ASSERT(false, "element type not supported yet");
						break;
					}
				} //slave_type

				break;
			}

			case ELEMENT_TYPE_TRIANGLE:
			{
				switch(slave_type) 
				{
					case ELEMENT_TYPE_TRIANGLE: 
					{
						intersected = mortar_surface_line_line(surf, element_master, surf, element_slave, order, local_matrix, local_normals, local_gap);
						break;
					}

					default:
					{
						KERNEL_ASSERT(false, "element type not supported yet");
						break;
					}
				} //slave_type

				break;
			}

			default: 
			{
				KERNEL_ASSERT(false, "element type not supported yet");
				break;
			}
		} //master_type

		if(intersected) {
			pairs_intersect[i] = true;

			const SizeType element_scalar_offset = element_scalar_offsets[i];
			const SizeType element_vector_offset = element_scalar_offset * mesh.n_dims;
			const SizeType element_matrix_offset = element_matrix_offsets[i];

			const SizeType n_entries 			 = element_scalar_offsets[i + 1] - element_scalar_offset;
			const SizeType n_vector_entries 	 = n_entries * mesh.n_dims;
			const SizeType n_matrix_entries 	 = element_matrix_offsets[i + 1] - element_matrix_offset;

			//private to global memory
			generic_copy(n_entries, 	   local_gap, 	  &weighted_gap    [element_scalar_offset]);
			generic_copy(n_vector_entries, local_normals, &weighted_normals[element_vector_offset]);
			generic_copy(n_matrix_entries, local_matrix,  &element_matrices[element_matrix_offset]);
		} else {
			pairs_intersect[i] = false;
		}
	}
}


#endif //MORTAR_SURFACE_CL

