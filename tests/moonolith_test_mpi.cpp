
#include <cmath>
#include "moonolith_sparse_matrix.hpp"
#include "moonolith_stream_utils.hpp"
#include "moonolith_synched_describable.hpp"

#include "gtest/gtest.h"

using namespace moonolith;

TEST(MPITest, allgatherv) {
    Communicator world;

    ByteOutputBuffer os;
    ByteInputBuffer is;

    int rank = world.rank();

    os << rank;

    world.all_gatherv(os, is);

    int recv_rank = -1;

    for (int i = 0; i < world.size(); ++i) {
        recv_rank << is;

        ASSERT_TRUE(i == recv_rank);
    }
}

TEST(MPITest, sparse_matrix) {
    Communicator world;

    srand(world.rank());

    const Integer local_rows = 4;
    const Integer rows = local_rows * world.size();
    const Integer local_cols = 3;
    const Integer cols = local_cols * world.size();

    const Integer nnz = 9;

    SparseMatrix<Real> mat(world, rows, cols, local_rows, local_cols);

    Real expected_total = 0;

    for (Integer k = 0; k < nnz; ++k) {
        const Integer i = static_cast<Integer>(static_cast<Real>(rand()) / static_cast<Real>(RAND_MAX * rows));
        const Integer j = static_cast<Integer>(static_cast<Real>(rand()) / static_cast<Real>(RAND_MAX * cols));
        const Real value = i * cols + j;

        mat.add(i, j, value);
        expected_total += value;
    }

    mat.finalize(AddAssign<Real>());

    world.all_reduce(&expected_total, 1, MPISum());

    const Real actual = mat.sum();

    root_describe("---------------", world, logger());
    root_describe(expected_total, world, logger());
    root_describe("---------------", world, logger());
    root_describe(actual, world, logger());
    root_describe("---------------", world, logger());

    // world.barrier();

    ASSERT_TRUE(std::abs(actual - expected_total) < 1e-16);
}
