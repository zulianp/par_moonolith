#ifndef MOONOLITH_GRAHAM_SCAN_HPP
#define MOONOLITH_GRAHAM_SCAN_HPP

#include "moonolith_expanding_array.hpp"
#include "moonolith_householder.hpp"
#include "moonolith_vector.hpp"
#include "par_moonolith_config.hpp"

namespace moonolith {
    template <typename T, int Dim>
    class MapToSphere {
    public:
        using Point = moonolith::Vector<T, Dim>;

        static void apply(Storage<Point> &points) {
            auto n = points.size();
            Point avg = points[0];

            for (std::size_t i = 1; i < n; ++i) {
                avg += points[i];
            }

            avg /= n;

            for (std::size_t i = 0; i < n; ++i) {
                points[i] -= avg;
                points[i] /= length(points[i]);
            }
        }
    };

    template <typename T>
    class RotatePointsToXYPlane {
    public:
        using Point2 = moonolith::Vector<T, 2>;
        using Point3 = moonolith::Vector<T, 3>;

        using Vector3 = moonolith::Vector<T, 3>;
        using Vector2 = moonolith::Vector<T, 2>;

        void apply(const Storage<Point3> &points, const Vector3 &normal, Storage<Point2> &xy) {
            // static const int SubDim = 2;
            const auto n_points = points.size();

            xy.resize(n_points);

            // default is xy-plane
            Integer coord_1 = 0;
            Integer coord_2 = 1;

            bool transformed = false;

            if (fabs(normal[1]) > 0.9999) {
                // xz-plane
                coord_2 = 2;
            } else if (fabs(normal[0]) > 0.9999) {
                // yz-plane
                coord_1 = 1;
                coord_2 = 2;
            } else if (!(fabs(normal[2]) > 0.9999)) {
                // also not xy-plane
                transformed = true;
            }

            if (transformed) {
                // The bug is inside this branch
                // printf("householder_reflection_3: ROTATING POLY\n");
                // Rotation to the xy-plane is necessary

                v[0] = normal[0];
                v[1] = normal[1];
                v[2] = normal[2] + 1.0;

                const auto norm_v = length(v);
                v /= norm_v;

                householder_reflection(v, H);

                for (std::size_t i = 0; i < n_points; ++i) {
                    mat_vec_mul(H, points[i], xy[i]);
                }

            } else {
                for (std::size_t i = 0; i < n_points; ++i) {
                    xy[i].x = points[i][coord_1];
                    xy[i].y = points[i][coord_2];
                }
            }
        }

    private:
        Vector3 v;
        Storage<T> H;
    };

    /**
     * @brief partial implementation of http://geomalgorithms.com/a10-_hull-1.html
     */
    template <typename T>
    class GrahamScan {
    public:
        using Point2 = moonolith::Vector<T, 2>;
        using Point3 = moonolith::Vector<T, 3>;
        using Vector3 = moonolith::Vector<T, 3>;

        class CCWComparator {
        public:
            CCWComparator(const Integer lowest_rightmost_point, const Storage<Point2> &points)
                : i0(lowest_rightmost_point), points(points) {}

            inline const Point2 &p0() const { return points[i0]; }

            inline T area(const Point2 &p1, const Point2 p2) const {
                return ((p1.x - p0().x) * (p2.y - p0().y) - (p2.x - p0().x) * (p1.y - p0().y));
            }

            inline bool less(const Point2 &p1, const Point2 p2) const {
                const auto a = area(p1, p2);

                if (approxeq(a, static_cast<T>(0.0), static_cast<T>(1e-10))) {
                    // We do not want to remove collinear points at this stage
                    // the order should be ok if the closest is put before the
                    // fartest

                    const bool p1_nearest = distance(p0(), p1) < distance(p0(), p2);
                    return p1_nearest;

                    if (p1_nearest) {
                        return p0().x < p1.x;
                    } else {
                        return p0().x > p1.x;
                    }
                }

                return a > 0;
            }

            inline bool operator()(const Integer &i1, const Integer i2) {
                if (i1 == i2) return false;
                if (i1 == i0) {
                    return true;
                }

                if (i2 == i0) {
                    return false;
                }

                return less(points[i1], points[i2]);
            }

            const Integer i0;
            const Storage<Point2> &points;
        };

        inline void apply(const Storage<Point2> &p, Integer *order) {
            apply(p, order_buffer);

            const auto n = p.size();
            mapped_index.resize(n);

            for (std::size_t i = 0; i < n; ++i) {
                mapped_index[i] = order[order_buffer[i]];
            }

            std::copy(mapped_index.begin(), mapped_index.end(), order);
        }

        inline void apply(const Storage<Point3> &p, const Vector3 &normal, Storage<Point3> &hull) {
            rotate_points.apply(p, normal, point_buffer_2);
            apply(point_buffer_2, order_buffer);

            const auto n = order_buffer.size();
            hull.resize(n);

            for (std::size_t i = 0; i < n; ++i) {
                hull[i] = p[order_buffer[i]];
            }
        }

        inline void apply(const Storage<Point2> &p, Storage<Point2> &hull) {
            apply(p, order_buffer);

            const auto n = order_buffer.size();
            hull.resize(n);

            for (std::size_t i = 0; i < n; ++i) {
                hull[i] = p[order_buffer[i]];
            }
        }

        inline void apply(const Storage<Point2> &p, Storage<Integer> &order) {
            const Integer n = static_cast<Integer>(p.size());
            order.resize(n);

            Integer lowest_rightmost_point = 0;
            order[0] = 0;

            for (Integer i = 1; i < n; ++i) {
                order[i] = i;

                const auto &p0 = p[lowest_rightmost_point];
                const auto &pi = p[i];
                if (pi.y < p0.y) {
                    lowest_rightmost_point = i;
                } else if (pi.y == p0.y && pi.x > p0.x) {
                    lowest_rightmost_point = i;
                }
            }

            // std::swap(order[0], order[lowest_rightmost_point]);
            // std::cout << "lowest_rightmost_point: " << lowest_rightmost_point << std::endl;

            CCWComparator comp(lowest_rightmost_point, p);
            std::sort(order.begin(), order.end(), comp);

            assert(order[0] == lowest_rightmost_point);
        }

    private:
        Storage<Integer> order_buffer;
        Storage<Integer> mapped_index;

        Storage<Point2> point_buffer_2;

        RotatePointsToXYPlane<T> rotate_points;
    };
}  // namespace moonolith

#endif  // MOONOLITH_GRAHAM_SCAN_HPP
