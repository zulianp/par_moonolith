#ifndef MOONOLITH_INTERSECT_HALFSPACE_IMPL_HPP
#define MOONOLITH_INTERSECT_HALFSPACE_IMPL_HPP

#include "moonolith_intersect_halfspace.hpp"
#include "moonolith_intersect_plane_impl.hpp"

namespace moonolith {

    template <typename T, int Dim>
    short IntersectLineWithHalfSpace<T, Dim>::apply(const Line &line,
                                                    const HalfSpace &half_space,
                                                    Line &result,
                                                    const T &tol) {
        auto d0 = half_space.signed_dist(line.p0);
        auto d1 = half_space.signed_dist(line.p1);
        T tol2 = 2.0 * tol;

        if (std::signbit(d0) && std::signbit(d1)) {
            // inside half-space
            result = line;
            return 3;
        }

        if (!std::signbit(d0) && !std::signbit(d1)) {
            if (d0 > tol || d1 > tol) {
                // no intersection or point intersection
                return 0;
            }
        }

        if (approxeq(d0, static_cast<T>(0.), tol2) && approxeq(d1, static_cast<T>(0.), tol2)) {
            // aligned with half-space boundary
            result = line;
            return 4;
        }

        Plane plane;
        plane.n = half_space.n;
        plane.p = half_space.n * half_space.d;

        T t = 0.;
        auto code = isect_line_plane.apply(line, plane, t, tol);

        if (code == IsectLinePlane::COPLANAR) {
            // line is collinear
            assert(false);
        }

        // no actual intersection with segment
        if (code == IsectLinePlane::OUTSIDE) {
            if (d0 <= tol || d1 <= tol) {
                result = line;
                return INSIDE;
            } else {
                return NO_INTERSECTION;
            }
        }

        assert(t >= -tol);
        assert(t <= 1. + tol);

        // if(approxeq(t, 1.0, tol)) {
        // 	result = line;
        // 	return 4;
        // }

        auto u = line.p1 - line.p0;

        if (d0 < d1) {
            result.p0 = line.p0;
            result.p1 = line.p0 + t * u;
            return SEGMENT_FROM_POINT_0;
        } else {
            result.p0 = line.p0 + t * u;
            result.p1 = line.p1;
            return SEGMENT_FROM_POINT_1;
        }
    }

}  // namespace moonolith

#endif  // MOONOLITH_INTERSECT_HALFSPACE_IMPL_HPP
