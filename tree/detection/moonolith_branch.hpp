#ifndef MOONOLITH_BRANCH_HPP
#define MOONOLITH_BRANCH_HPP

#include "moonolith_forward_declarations.hpp"
#include "moonolith_navigator_option.hpp"
#include "moonolith_node_handle.hpp"

#include <vector>

namespace moonolith {
    template <class Traits>
    class Branch : public Node<Traits> {
    private:
        typedef moonolith::Node<Traits> super;
        typedef std::vector<NodeHandle> NodeContainer;

    public:
        using super::accept;
        typedef typename NodeContainer::iterator NodeIter;
        typedef typename NodeContainer::const_iterator ConstNodeIter;

        NavigatorOption accept(Visitor<Traits> &visitor, TreeMemory<Traits> &memory) override {
            return visitor.visit(*this, memory);
        }

        inline NodeIter begin() { return children_.begin(); }

        inline NodeIter end() { return children_.end(); }

        inline ConstNodeIter begin() const { return children_.begin(); }

        inline ConstNodeIter end() const { return children_.end(); }

        void clear() override {
            super::clear();
            children_.clear();
        }

        NodeHandle child_at(const Integer index) const { return children_[index]; }

        void set_child_at(const Integer index, Node<Traits> &node) {
            children_[index] = node.handle();
            node.set_parent(this->handle());
            node.set_level(this->level() + 1);
            node.set_position_in_parent(index);
        }

        void set_number_of_children(const Integer n) { children_.resize(n); }

        bool empty() const override { return children_.empty(); };
        bool is_branch() const override { return true; }
        bool is_visitable() const override { return !empty(); }

        std::string get_class() const override { return "Branch"; }

    private:
        NodeContainer children_;
    };
}  // namespace moonolith

#endif  // MOONOLITH_BRANCH_HPP
