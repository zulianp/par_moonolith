#ifndef MOONOLITH_FUNCTION_SPACE_HPP
#define MOONOLITH_FUNCTION_SPACE_HPP

#include "moonolith_communicator.hpp"
#include "moonolith_elem_type.hpp"
#include "moonolith_expanding_array.hpp"
#include "moonolith_input_stream.hpp"
#include "moonolith_make_unique.hpp"
#include "moonolith_output_stream.hpp"

#include "moonolith_stream_utils.hpp"

#include <memory>

namespace moonolith {

    class Dofs final : public Serializable {
    public:
        Integer global_idx;
        Integer element_dof;
        Integer block;
        Storage<Integer> dofs;
        FEType type;

        Dofs() : global_idx(-1), element_dof(-1), block(-1) {}

        bool is_valid() const {
            bool valid = true;
            for (auto d : dofs) {
                if (d >= 1e12) {
                    valid = false;
                    break;
                }
            }

            if (!valid) {
                describe(error_logger());
                return false;
            }

            return true;
        }

        inline void describe(std::ostream &os) const {
            os << "global_idx  : " << global_idx << std::endl;
            os << "element_dof : " << element_dof << std::endl;
            os << "block       : " << block << std::endl;
            os << "type        : " << type << std::endl;
            os << "dofs        : [ ";

            for (auto d : dofs) {
                os << d << " ";
            }

            os << "]" << std::endl;
            os << std::flush;
        }

        inline bool empty() const { return dofs.empty(); }

        inline void read(InputStream &is) override {
            is >> global_idx;
            is >> element_dof;
            is >> block;

            int type_int;

            is >> type_int;
            type = moonolith::FEType(type_int);

            ////////////////////////////////////

            Integer n;
            is >> n;
            dofs.resize(n);

            for (auto &d : dofs) {
                is >> d;
            }
        }

        inline void write(OutputStream &os) const override {
            os << global_idx;
            os << element_dof;
            os << block;

            const int type_int = type;
            os << type_int;

            ///////////////////////////////////

            const Integer n = static_cast<Integer>(dofs.size());
            os << n;

            for (const auto &d : dofs) {
                os << d;
            }
        }
    };

    class DofMap {
    public:
        inline Storage<Dofs> &dofs() { return element_dofs_; }

        inline const Storage<Dofs> &dofs() const { return element_dofs_; }

        inline Storage<Integer> &dofs(const Integer &idx) { return dof_object(idx).dofs; }

        inline const Storage<Integer> &dofs(const Integer &idx) const { return dof_object(idx).dofs; }

        inline FEType &type(const Integer &idx) { return dof_object(idx).type; }

        inline const FEType &type(const Integer &idx) const { return dof_object(idx).type; }

        inline Integer &global_idx(const Integer &idx) { return dof_object(idx).global_idx; }

        inline const Integer &global_idx(const Integer &idx) const { return dof_object(idx).global_idx; }

        inline Integer &block(const Integer &idx) { return dof_object(idx).block; }

        inline const Integer &block(const Integer &idx) const { return dof_object(idx).block; }

        inline const Dofs &dof_object(const Integer &idx) const {
            assert(idx < static_cast<Integer>(element_dofs_.size()));
            return element_dofs_[idx];
        }

        inline Dofs &dof_object(const Integer &idx) {
            assert(idx < static_cast<Integer>(element_dofs_.size()));
            return element_dofs_[idx];
        }

        template <class Iterator>
        void serialize(const Iterator &begin, const Iterator &end, OutputStream &os) const {
            assert(is_valid());

            os << n_dofs_;
            os << n_local_dofs_;
            os << max_nnz_;

            Integer n = static_cast<Integer>(std::distance(begin, end));
            os << n;

            for (auto it = begin; it != end; ++it) {
                os << element_dofs_[*it];
            }
        }

        void build(InputStream &is) {
            is >> n_dofs_;
            is >> n_local_dofs_;
            is >> max_nnz_;

            Integer n;
            is >> n;

            element_dofs_.resize(n);

            for (Integer i = 0; i < n; ++i) {
                is >> element_dofs_[i];
            }

            assert(is_valid());
        }

        void resize(const std::size_t &n_local_elements) { element_dofs_.resize(n_local_elements); }

        inline Integer n_elements() const { return static_cast<Integer>(element_dofs_.size()); }

        inline void set_n_dofs(const Integer n) { n_dofs_ = n; }

        inline void set_n_dofs(const Integer n_local, const Integer n) {
            n_local_dofs_ = n_local;
            n_dofs_ = n;
        }

        inline void set_n_local_dofs(const Integer n) { n_local_dofs_ = n; }

        inline void set_max_nnz(const Integer n) { max_nnz_ = n; }

        Integer max_nnz() const { return max_nnz_; }

        inline Integer n_dofs() const {
            assert(n_dofs_ != -1 && "call set_n_dofs(....)");
            return n_dofs_;
        }

        inline std::size_t n_local_dofs() const {
            assert(n_local_dofs_ != -1 && "call set_n_local_dofs(....)");
            return n_local_dofs_;
        }

        DofMap() : n_dofs_(-1), n_local_dofs_(-1), max_nnz_(0) {}

        void set_uniform_block(const int block) {
            for (auto &d : element_dofs_) {
                d.block = block;
            }
        }

        void clear() {
            element_dofs_.clear();
            n_dofs_ = -1;
            n_local_dofs_ = -1;
            max_nnz_ = 0;
        }

        void make_elem_node_map(Communicator &comm, DofMap &out) const {
            const Integer n_elems = n_elements();

            out.resize(n_elems);
            out.n_local_dofs_ = 0;
            out.max_nnz_ = 0;

            for (Integer i = 0; i < n_elems; ++i) {
                Integer n_e_dofs = static_cast<Integer>(dofs(i).size());
                out.max_nnz_ = std::max(out.max_nnz_, n_e_dofs);
                out.n_local_dofs_ += n_e_dofs;
            }

            Integer dofs_begin = 0;
            comm.exscan(&out.n_local_dofs_, &dofs_begin, 1, moonolith::MPISum());

            Integer dof_id = dofs_begin;
            for (Integer i = 0; i < n_elems; ++i) {
                const auto &dof_in = dof_object(i);
                auto &dof_out = out.dof_object(i);
                dof_out = dof_in;

                for (auto &d : dof_out.dofs) {
                    d = dof_id++;
                }
            }

            out.n_dofs_ = out.n_local_dofs_;

            comm.all_reduce(&out.n_dofs_, 1, moonolith::MPISum());
        }

        bool is_valid() const;

    public:
        Storage<Dofs> element_dofs_;
        Integer n_dofs_;
        Integer n_local_dofs_;
        Integer max_nnz_;
    };

    class IFunctionSpace {
    public:
        virtual ~IFunctionSpace() = default;
    };

    template <class Mesh_>
    class FunctionSpace : public IFunctionSpace {
    public:
        using Mesh = Mesh_;

        virtual ~FunctionSpace() {}

        inline DofMap &dof_map() { return dof_map_; }

        inline const DofMap &dof_map() const { return dof_map_; }

        FunctionSpace() {}
        FunctionSpace(const std::shared_ptr<Mesh> &mesh) : mesh_(mesh) {}
        FunctionSpace(Mesh &mesh) : mesh_(moonolith::make_ref(mesh)) {}

        inline const Mesh &mesh() const {
            assert(mesh_);
            return *mesh_;
        }

        inline Mesh &mesh() {
            assert(mesh_);
            return *mesh_;
        }

        inline std::shared_ptr<Mesh> mesh_ptr() const { return mesh_; }

        void set_mesh(const std::shared_ptr<Mesh> &mesh) { mesh_ = mesh; }

        void separate_dofs(FunctionSpace<Mesh> &out) const {
            out.set_mesh(mesh_);
            dof_map_.make_elem_node_map(mesh_->comm(), out.dof_map_);
        }

        void make_iso_parametric() {
            const Integer n_elements = mesh_->n_elements();

            dof_map_.resize(n_elements);

            for (Integer i = 0; i < n_elements; ++i) {
                const auto &e = mesh_->elem(i);
                auto &dof = dof_map_.dof_object(i);
                dof.type = FEType(e.type);
                dof.block = e.block;
                dof.global_idx = e.global_idx;
                dof.element_dof = e.global_idx;
                dof.dofs = e.nodes;
            }
        }

    private:
        std::shared_ptr<Mesh> mesh_;
        DofMap dof_map_;
    };

}  // namespace moonolith

#endif  // MOONOLITH_FUNCTION_SPACE_HPP