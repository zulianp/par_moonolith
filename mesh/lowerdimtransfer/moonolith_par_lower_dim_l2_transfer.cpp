#include "moonolith_par_lower_dim_l2_transfer.hpp"

namespace moonolith {
    template class ParLowerDimL2Transfer<Real, 3, 2, 2>;
    template class ParLowerDimL2Transfer<Real, 2, 1, 1>;
}  // namespace moonolith
