#include "moonolith_interpolator.hpp"

namespace moonolith {

    template <class MasterElem, class SlaveElem>
    Interpolator<MasterElem, SlaveElem>::Interpolator() : trafo_master(std::make_shared<MasterAffineTrafo>()) {}

    template <class MasterElem, class SlaveElem>
    Interpolator<MasterElem, SlaveElem>::~Interpolator() {}

    template <class MasterElem, class SlaveElem>
    void Interpolator<MasterElem, SlaveElem>::clear() {}

    template <class MasterElem, class SlaveElem>
    void Interpolator<MasterElem, SlaveElem>::describe(std::ostream &) const {}

    template <class MasterElem, class SlaveElem>
    bool Interpolator<MasterElem, SlaveElem>::assemble(MasterElem &master, SlaveElem &slave) {
        contains_.init(master);
        if (!contains_.apply(slave, is_contained_)) {
            return false;
        }

        Resize<Matrix<T, NSlaveNodes, NMasterNodes> >::apply(coupling_mat_, slave.n_nodes(), master.n_nodes());

        coupling_mat_.zero();

        const bool is_affine = master.is_affine();

        if (is_affine) {
            make_transform(master, *trafo_master);
            iso_trafo_master = trafo_master;
        } else {
            make_transform(master, iso_trafo_master);
        }

        const Integer n = is_contained_.size();
        const Integer n_nodes_master = master.n_nodes();

        bool has_interp = false;
        for (Integer i = 0; i < n; ++i) {
            if (!is_contained_[i]) continue;
            if (!iso_trafo_master->apply_inverse(slave.node(i), p_ref)) continue;

            has_interp = true;
            for (Integer j = 0; j < n_nodes_master; ++j) {
                coupling_mat_(i, j) = master.fun(j, p_ref);
            }
        }

        return has_interp;
    }

}  // namespace moonolith
