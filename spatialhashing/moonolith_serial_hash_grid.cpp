#include "moonolith_serial_hash_grid_impl.hpp"

namespace moonolith {

    template class SerialHashGrid<1, Real>;
    template class SerialHashGrid<2, Real>;
    template class SerialHashGrid<3, Real>;
    // TODO
    // template class SerialHashGrid<4, Real>;
}  // namespace moonolith
