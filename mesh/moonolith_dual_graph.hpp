#ifndef MOONOLITH_DUAL_GRAPH_HPP
#define MOONOLITH_DUAL_GRAPH_HPP

#include "par_moonolith_config.hpp"
#include "moonolith_expanding_array.hpp"
#include "moonolith_polygon.hpp"
#include "moonolith_polyhedron.hpp"
#include "moonolith_geo_algebra.hpp"

#include <vector>

namespace moonolith {
	
	template<typename T, Integer Dim>
	class DualGraph {
	public:
		using Vector = moonolith::Vector<T, Dim>;

		bool sides_are_coplanar(
			const Polygon<T, Dim> &poly1,
			const Integer side_num_1,
			const Polygon<T, Dim> &poly2,
			const Integer side_num_2,
			const T tol
		)
		{
			auto i1p1 = (side_num_1 + 1) == poly1.size() ? 0 : side_num_1 + 1;
			auto i2p1 = (side_num_2 + 1) == poly2.size() ? 0 : side_num_2 + 1;

			const auto &p1 = poly1[side_num_1];
			const auto &p2 = poly1[i1p1];
			const auto &q1 = poly2[side_num_2];
			const auto &q2 = poly2[i2p1];


			u = p2 - p1;
			v = q2 - q1;

			u /= length(u);
			v /= length(v);

			auto cos_angle = dot(u, v);

			if(!approxeq(std::abs(cos_angle), 1., tol)) {
				return false;
			}

			w = q1 - p1;
			auto len_w = length(w);

			if(len_w == 0.) {
				return true;
			}

			w /= len_w;

			cos_angle = dot(u, w);

			if(!approxeq(std::abs(cos_angle), 1., tol)) {
				return false;
			}

			return true;
		}

		void add_adj(const Integer id1, const Integer id2) 
		{
			if(id1 >= dual_graph_.size()) {
				dual_graph_.resize(id1 + 1);
			}

			dual_graph_[id1].push_back(id2);
		}

		void clear()
		{
			for(auto &d : dual_graph_) {
				d.clear();
			}

			dual_graph_.clear();
		}

	private:
		Storage<Storage<Integer>> dual_graph_;
		Vector u, v, n, w;
	};


	template<typename T>
	class DualGraph<T, 3> {
	public:
		using Vector = moonolith::Vector<T, 3>;


		//sides are assumed to be planar
		bool sides_are_coplanar(
			const Polyhedron<T> &poly1,
			const Integer side_num_1,
			const Polyhedron<T> &poly2,
			const Integer side_num_2,
			const T tol
		)
		{
			const auto begin_1 = poly1.el_ptr[side_num_1];
			const auto begin_2 = poly2.el_ptr[side_num_2];

			assert(poly1.el_ptr[side_num_1 + 1] - begin_1 >= 3);
			assert(poly2.el_ptr[side_num_2 + 1] - begin_2 >= 3);

			const auto &p1 = poly1.points[poly1.el_index[begin_1]];
			const auto &p2 = poly1.points[poly1.el_index[begin_1 + 1]];
			const auto &p3 = poly1.points[poly1.el_index[begin_1 + 2]];

			const auto &q1 = poly2.points[poly2.el_index[begin_2]];
			const auto &q2 = poly2.points[poly2.el_index[begin_2 + 1]];
			const auto &q3 = poly2.points[poly2.el_index[begin_2 + 2]];

			//constructing frame information for side 1
			u1 = p2 - p1;
			v1 = p3 - p1;

			u1 /= length(u1);
			v1 /= length(v1);

			n1 = cross(u1, v1);
			n1 /= length(n1);

			u2 = q2 - q1;
			v2 = q3 - q1;

			u2 /= length(u2);
			v2 /= length(v2);

			n2 = cross(u2, v2);
			n2 /= length(n2);

			auto cos_angle = dot(n1, n2);

			if(!approxeq(cos_angle, -1., tol)) {
				//planes are not aligned and do not have opposite orientation
				return false;
			}

			w = q1 - q1;
			const T d = dot(n1, w);
			return approxeq(d, 0., tol);
		}

		void add_adj(const Integer id1, const Integer id2) 
		{
			if(id1 >= dual_graph_.size()) {
				dual_graph_.resize(id1 + 1);
			}

			dual_graph_[id1].push_back(id2);
		}

		void clear()
		{
			for(auto &d : dual_graph_) {
				d.clear();
			}

			dual_graph_.clear();
		}


	private:
		Storage<Storage<Integer>> dual_graph_;
		Vector u1, v1, n1;
		Vector u2, v2, n2;
		Vector w;

	};
}

#endif //MOONOLITH_DUAL_GRAPH_HPP
