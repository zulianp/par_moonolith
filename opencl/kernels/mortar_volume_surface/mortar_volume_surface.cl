
#include "kernel_base.cl"
#include "mortar/intersect_interface.cl"

#ifndef CLIPP_HOST_CL
static bool intersect_convex_polyhedron_with_polygon(const PMesh polyhedron, const SizeType n_vertices, const Scalar *polygon, PMesh *result);
static void compute_surface_normals(PMesh surf_mesh, Scalar *normals);
#endif

static void compute_surface_normals(PMesh surf_mesh, Scalar *normals)
{
	Vector3 o, u, v, n;
	for(SizeType i = 0; i < surf_mesh.n_elements; ++i) {
		const SizeType begin = surf_mesh.el_ptr[i];
		const SizeType offset_3 = i * surf_mesh.n_dims;

		KERNEL_ASSERT((surf_mesh.el_ptr[i+1] - begin) >= 3, "compute_normals: (end - begin) >= 3");

		const SizeType v_o = surf_mesh.el_index[begin] 	   * surf_mesh.n_dims;
		const SizeType v_u = surf_mesh.el_index[begin + 1] * surf_mesh.n_dims;;
		const SizeType v_v = surf_mesh.el_index[begin + 2] * surf_mesh.n_dims;;

		o = vec_3(surf_mesh.points[v_o], surf_mesh.points[v_o + 1], surf_mesh.points[v_o + 2]);
		u = vec_3(surf_mesh.points[v_u], surf_mesh.points[v_u + 1], surf_mesh.points[v_u + 2]);
		v = vec_3(surf_mesh.points[v_v], surf_mesh.points[v_v + 1], surf_mesh.points[v_v + 2]);

		u -= o;
		v -= o;

		n = cross(u, v);
		n = normalize(n);
		generic_copy(3, n, &normals[offset_3]);
	}
}

static bool intersect_convex_polyhedron_with_polygon(const PMesh polyhedron, const SizeType n_vertices, const Scalar *polygon, PMesh *result)
{
	Scalar normals_result 	   [MAX_LOCAL_ELEMENTS * 3];
	Scalar normals_half_spaces [MAX_LOCAL_ELEMENTS * 3];
	Scalar dist_half_spaces    [MAX_LOCAL_ELEMENTS];

	PMesh surface;
	surface.n_dims 		= 3;
	surface.n_nodes 	= n_vertices;
	surface.n_elements 	= 1;
	surface.el_ptr[0] 	= 0;
	surface.el_ptr[1] 	= n_vertices;

	generic_copy(n_vertices * 3, polygon, surface.points);

	for(SizeType i = 0; i < n_vertices; ++i) {
		surface.el_index[i] = i;
	}

	// p_mesh_print(&surface);

	compute_surface_normals(surface, normals_result);
	p_mesh_copy(&surface, result);

	const SizeType n_half_spaces = make_h_polyhedron_from_polyhedron(polyhedron, normals_half_spaces, dist_half_spaces);	
	
	hs_cut_polyhedron(n_half_spaces, normals_half_spaces, dist_half_spaces, result, normals_result);
 	return result->n_nodes >= 3 && result->n_elements >= 1; 

	
	// if(ok) {
	// 	SizeType valid_face = 0;
	// 	SizeType n_nodes	= result->el_ptr[1] - result->el_ptr[0];

	// 	for(SizeType i = 1; i < result->n_elements; ++i) {
	// 		const SizeType n_nodes_i = result->el_ptr[i + 1] - result->el_ptr[i];

	// 		if(n_nodes_i > n_nodes) {
	// 			valid_face = i;
	// 			n_nodes = n_nodes_i;
	// 		}
	// 	}

	// 	if(valid_face > 0)  {
	// 		const SizeType begin = result->el_ptr[valid_face];
	// 		generic_copy(n_nodes, &result->el_index[begin], result->el_index);
	// 	}

	// 	result->n_elements = 1;
	// 	return true;
	// } else {
	// 	return false;
	// }
}
