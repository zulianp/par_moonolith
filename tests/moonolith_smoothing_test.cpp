
#include "moonolith_matlab_scripter.hpp"
#include "moonolith_pn_triangle.hpp"

#include "gtest/gtest.h"

using namespace moonolith;

TEST(TestPNTriangle, PNTriangleWorks) {
    using Point2 = moonolith::Vector<Real, 2>;
    using Point3 = moonolith::Vector<Real, 3>;
    using Vector3 = moonolith::Vector<Real, 3>;

    PNTriangle<Real, 3> pn_triangle;

    std::array<Point3, 3> points{
        Point3(0., 0., 0.),
        Point3(1., 0., 0.),
        Point3(0., 1., 0.),
    };

    std::array<Vector3, 3> normals{Vector3(-1., -1., 1.), Vector3(1., -1., 1.), Vector3(-1., 1., 1.)};

    // std::array<Vector3, 3> normals {
    //     Vector3(0., 0., 1.),
    //     Vector3(0., 0., 1.),
    //     Vector3(0., 0., 1.)
    // };

    for (auto &n : normals) {
        n /= length(n);
    }

    pn_triangle.init(points, normals);

    Point2 p(0.5, 0.);
    Point3 q;

    pn_triangle.apply(p, q);

    // MatlabScripter script;

    // script.close_all();
    // script.hold_on();
    // script.plot_polygon(points);
    // script.quiver(points, normals);
    // script.plot(pn_triangle.points(), "r*");

    // script.plot(q, "g*");
    // script.axis_equal();
    // script.save("pntri.m");

    // write("pntri.m", points, normals, pn_triangle);
}
