#ifndef MOONOLITH_LOOKUP_TABLE_HPP
#define MOONOLITH_LOOKUP_TABLE_HPP

#include "moonolith_communicator.hpp"
#include "moonolith_node_handle.hpp"
#include "moonolith_synched_describable.hpp"

#include <set>
#include <vector>

namespace moonolith {

    class Node2ProcEntry : public Serializable {
    public:
        typedef std::vector<Integer> ProcList;
        typedef std::vector<NodeHandle> HandleList;

        typedef typename ProcList::const_iterator ProcIter;

        inline const ProcList &processes() const { return processes_; }

        inline ProcList &processes() { return processes_; }

        inline const HandleList &remote_nodes() const { return remote_nodes_; }

        inline HandleList &remote_nodes() { return remote_nodes_; }

        NodeHandle node_handle() const { return node_; }

        void set_node_handle(const NodeHandle &node) { node_ = node; }

        Node2ProcEntry(const NodeHandle &node = NodeHandle()) : node_(node) {}

        inline bool empty() const { return node_.is_null(); }

        void describe(std::ostream &os) const {
            os << "Node: " << node_ << "\nProcesses: ";
            for (auto p : processes_) os << p << " ";
            os << "\n";
            for (auto p : remote_nodes_) os << p << " ";
            os << "\n";
        }

        void read(InputStream &is) {
            CHECK_STREAM_READ_BEGIN("Node2ProcEntry", is);

            is >> node_;
            Integer n;
            is >> n;
            if (n) {
                processes_.resize(n);
                remote_nodes_.resize(n);
                is.read(processes_.begin(), processes_.end());
                is.read(remote_nodes_.begin(), remote_nodes_.end());
            }

            CHECK_STREAM_READ_END("Node2ProcEntry", is);
        }

        void write(OutputStream &os) const {
            CHECK_STREAM_WRITE_BEGIN("Node2ProcEntry", os);

            os << node_;
            Integer n = static_cast<Integer>(processes_.size());
            os << n;
            if (n) {
                os.write(processes_.begin(), processes_.end());
                os.write(remote_nodes_.begin(), remote_nodes_.end());
            }

            CHECK_STREAM_WRITE_END("Node2ProcEntry", os);
        }

    private:
        NodeHandle node_;
        ProcList processes_;
        HandleList remote_nodes_;
    };

    class LookUpTable : public SynchedDescribable, public VerboseObject {
    public:
        typedef std::vector<Node2ProcEntry> Node2Proc;
        typedef std::vector<std::set<Integer> > Data2Proc;
        typedef Node2Proc::const_iterator NodeToProcIter;

        void add_entry_for(const NodeHandle &handle, const Integer remote_rank, const NodeHandle &remote_handle) {
            if (handle.id() >= Integer(node_2_proc_.size())) {
                node_2_proc_.resize(handle.id() + 1);
            }

            assert(!handle.is_null());

            node_2_proc_[handle.id()].set_node_handle(handle);
            node_2_proc_[handle.id()].processes().push_back(remote_rank);
            node_2_proc_[handle.id()].remote_nodes().push_back(remote_handle);

            // logger() << "Adding " << handle.id() << ", " << remote_rank << ", " << remote_handle.id() << std::endl;
        }

        ///!!!! SUPER EXPENSIVE (assertion based in debug)
        bool check_consistency(Communicator & /*comm*/) {
            // assert(!comm.hasPendingRequests());

            // if(comm.isAlone()) return true;

            //          if(this->verbose()) {
            //              moonolith::RootDescribe("LookUpTable::check_consistency BEGIN", comm, logger());
            //          }

            // OutputBuffer os;
            // std::vector<InputBuffer> is(comm.size());
            // std::vector<Node2Proc> allTables(comm.size());

            // Integer n = node_2_proc_.size();
            // os << n;
            // os.write(node_2_proc_.begin(), node_2_proc_.end());

            // comm.unstructuredAllGather(os, is, true);

            // for(Integer r = 0; r < comm.size(); ++r) {
            // 	if(r == comm.rank()) continue;

            // 	is[r] >> n;
            // 	allTables[r].resize(n);
            // 	is[r].read(allTables[r].begin(), allTables[r].end());
            // }

            //          bool is_faulty = false;
            // for(Integer i = 0; i < allTables.size(); ++i) {
            // 	if(i == comm.rank()) continue;

            // 	for(Integer n = 0; n < node_2_proc_.size(); ++n) {
            // 		const Node2ProcEntry &localEntry = node_2_proc_[n];
            // 		//check connected nodes to see if they have the connection themselves
            // 		if(localEntry.empty()) continue;

            // 		for(Integer rnLocalIndex = 0; rnLocalIndex < localEntry.remote_nodes().size(); ++rnLocalIndex) {
            // 			const Integer nodeId = localEntry.remote_nodes()[rnLocalIndex];
            // 			const Integer processRank = localEntry.processes()[rnLocalIndex];

            // 			bool found = false;
            // 			const Node2ProcEntry &remoteEntry = allTables[processRank][nodeId];

            // 			for(Integer rnRemoteIndex = 0; rnRemoteIndex < remoteEntry.remote_nodes().size();
            // ++rnRemoteIndex) { 				const NodeHandle rNodeId =
            // remoteEntry.remote_nodes()[rnRemoteIndex]; 				const Integer rProcessRank =
            // remoteEntry.processes()[rnRemoteIndex];

            // 				if(rNodeId == localEntry.node_handle().id() && rProcessRank == comm.rank()) {
            // 					found = true;
            // 					break;
            // 				}
            // 			}

            // 			if(!found) {
            // 				std::ostream &dcout = moonolith::Express::Instance().logger().os();
            // 				dcout << "Missing entry for node " << nodeId << " in process " << processRank <<
            // std::endl; 				remoteEntry.describe(dcout); 				dcout <<
            // std::endl;
            // 			}

            // 			assert(found);
            // 			if(!found)
            //                      {
            //                          is_faulty = true;
            //                      }
            // 		}
            // 	}
            // }

            //          if(this->verbose()) {
            //              moonolith::RootDescribe("LookUpTable::check_consistency END faulty: " + ToString(is_faulty),
            //              comm, logger());
            //          }

            //          comm.barrier();

            return true;
        }

        const Node2ProcEntry &get_entries_for(const NodeHandle &handle) const { return node_2_proc_[handle.id()]; }

        NodeToProcIter node_2_proc_begin() const { return node_2_proc_.begin(); }

        NodeToProcIter node_2_proc_end() const { return node_2_proc_.end(); }

        bool empty() { return node_2_proc_.empty(); }

        void describe(std::ostream &os) const {
            const Integer n_data = static_cast<Integer>(data_2_proc().size());
            os << "----------------------------------------------\n";
            for (Integer n = 0; n < n_data; ++n) {
                os << "Data " << n << ": ";
                for (Integer r : data_2_proc()[n]) {
                    os << r << " ";
                }
                os << "\n";
            }
            os << "----------------------------------------------\n";

            os << "-----------\n";
            stats_.describe(os);
            for (auto &n2p : node_2_proc_) {
                if (n2p.empty()) continue;
                n2p.describe(os);
                os << "-----------\n";
            }
        }

        void describe_stats(std::ostream &os) const { stats_.describe(os); }

        void synch_describe_stats(Communicator &comm, std::ostream &os) const { stats_.synch_describe(comm, os); }

        void compute_statistics(Communicator &comm) { stats_.compute(comm, *this); }

        class Stats : public SynchedDescribable {
        public:
            std::vector<Integer> n_connections;
            std::string desc;
            // std::vector<Integer> depth;
            void compute(Communicator &comm, const LookUpTable &lut) {
                n_connections.resize(comm.size(), 0);

                if (lut.data_2_proc().empty()) {
                    desc = "Based on nodes";
                    for (auto &it : lut.node_2_proc_) {
                        for (auto p : it.processes()) {
                            n_connections[p]++;
                        }
                    }
                } else {
                    desc = "Based on data";
                    for (auto &s : lut.data_2_proc_) {
                        for (auto p : s) {
                            n_connections[p]++;
                        }
                    }
                }
            }

            void describe(std::ostream &os) const {
                os << "Connetions with other processes " << desc << "\n";

                for (auto c : n_connections) {
                    os << c << " ";
                }

                os << "\n";
            }
        };

        inline Data2Proc &data_2_proc() { return data_2_proc_; }

        inline const Data2Proc &data_2_proc() const { return data_2_proc_; }

    private:
        Node2Proc node_2_proc_;
        Data2Proc data_2_proc_;
        Stats stats_;
    };
}  // namespace moonolith

#endif  // MOONOLITH_LOOKUP_TABLE_HPP
