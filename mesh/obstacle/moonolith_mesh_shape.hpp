#ifndef MOONOLITH_MESH_SHAPE_HPP
#define MOONOLITH_MESH_SHAPE_HPP

#include "moonolith_mesh.hpp"
#include "moonolith_shape.hpp"

#include <memory>

namespace moonolith {

    template <typename T, int Dim, int PhysicalDim = Dim>
    class MeshShape final : public Shape<T, Dim, PhysicalDim> {
    public:
        using CoPoint = moonolith::Vector<T, PhysicalDim>;
        using Point = moonolith::Vector<T, Dim>;

        using CoVector = moonolith::Vector<T, PhysicalDim>;
        using Vector = moonolith::Vector<T, Dim>;

        ~MeshShape() {}

        bool intersect(const Ray<T, PhysicalDim> &ray, T &t) override {
            Point x_param;
            return intersect(ray, t, x_param);
        }

        bool intersect(const Ray<T, PhysicalDim> &ray, T &t, Point &x_param) { return false; }

    private:
        std::shared_ptr<Mesh<T, PhysicalDim>> mesh_;
    };

}  // namespace moonolith

#endif  // MOONOLITH_MESH_SHAPE_HPP
