#include "moonolith_volume_surface_l2_assembler.hpp"
#include "moonolith_map_quadrature_impl.hpp"

#include "moonolith_elem.hpp"

namespace moonolith {
    template class VolumeSurfaceL2Assembler<Elem<Real, 2, 2>, Elem<Real, 1, 2>>;
    template class VolumeSurfaceL2Assembler<Elem<Real, 3, 3>, Elem<Real, 2, 3>>;

    template class BuildQuadratureWithDuplicateAvoidance<Real, 2>;
    template class BuildQuadratureWithDuplicateAvoidance<Real, 3>;
}  // namespace moonolith
