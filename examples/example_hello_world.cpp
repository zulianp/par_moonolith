#include "par_moonolith.hpp"

/// Compile examples with `make moonolith_examples`
/// Run with  `./examples/example_hello_world` or `./examples/example_hello_world --use_log_file` if you want the log in
/// a file instead of standard ouput
int main(int argc, char *argv[]) {
    using namespace moonolith;
    // Call to MPI_Init, and parse configuration options
    Moonolith::Init(argc, argv);

    logger() << "Hello world!" << std::endl;

    // Call to MPI_Finalize
    return Moonolith::Finalize();
}
