#ifndef MOONOLITH_GEO_ALGEBRA_HPP
#define MOONOLITH_GEO_ALGEBRA_HPP

#include "moonolith_expanding_array.hpp"
#include "moonolith_vector.hpp"
#include "par_moonolith_config.hpp"

namespace moonolith {

    template <typename T>
    inline bool approxeq(const T &left, const T &right, const T &tol) {
        using std::abs;
        return abs(left - right) < tol;
    }

    template <typename T, int Dim>
    void arg_min(const Storage<Vector<T, Dim>> &points, Integer i_min[Dim]) {
        const int n_points = points.size();

        for (int j = 0; j < Dim; ++j) {
            i_min[j] = 0;
        }

        for (int i = 0; i < n_points; ++i) {
            for (int j = 0; j < Dim; ++j) {
                const auto min_value = points[i_min[j]][j];

                if (min_value > points[i][j]) {
                    i_min[j] = i;
                }
            }
        }
    }

    template <typename T, int Dim>
    void arg_max(const Storage<Vector<T, Dim>> &points, Integer i_max[Dim]) {
        const int n_points = points.size();

        for (int j = 0; j < Dim; ++j) {
            i_max[j] = 0;
        }

        for (int i = 0; i < n_points; ++i) {
            // const int offset_i = i * Dim;

            for (int j = 0; j < Dim; ++j) {
                const auto max_value = points[i_max[j]][j];

                if (max_value < points[i][j]) {
                    i_max[j] = i;
                }
            }
        }
    }

    inline Integer symmetric_matrix_n_entries(const Integer n_rows) { return (n_rows * (n_rows + 1)) / 2; }

    inline Integer symmetric_matrix_index(const Integer i, const Integer j) {
        if (j > i) {
            return (j * (j + 1)) / 2 + i;
        } else {
            return (i * (i + 1)) / 2 + j;
        }
    }

    template <int Dim>
    struct Perp {};

    template <>
    struct Perp<1> {
        template <typename T>
        inline static void apply(const Vector<T, 1> &u, Vector<T, 1> &perp_u) {
            // FIXME
            perp_u.x = u.x;
        }
    };

    template <>
    struct Perp<2> {
        template <typename T>
        inline static void apply(const Vector<T, 2> &u, Vector<T, 2> &perp_u) {
            perp_u.x = -u.y;
            perp_u.y = u.x;
        }
    };

    template <>
    struct Perp<3> {
        template <typename T>
        inline static void apply(const Vector<T, 3> &u, Vector<T, 3> &perp_u) {
            // FIXME
            perp_u.x = -u.y;
            perp_u.y = u.x;
            perp_u.z = u.z;
        }
    };

    template <typename T, int Dim>
    inline void perp(const Vector<T, Dim> &u, Vector<T, Dim> &perp_u) {
        Perp<Dim>::apply(u, perp_u);
    }

}  // namespace moonolith

#endif  // MOONOLITH_GEO_ALGEBRA_HPP
