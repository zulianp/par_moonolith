#ifndef MOONOLITH_ELEM_TET10_HPP
#define MOONOLITH_ELEM_TET10_HPP

#include "moonolith_elem_tet.hpp"

namespace moonolith {

    template <typename T_, int PhysicalDim_ = 3>
    class Tet10 final : public Tet<T_, 2, PhysicalDim_> {
    public:
        static const int Order = 2;
        static const int Dim = 3;
        static const int PhysicalDim = PhysicalDim_;
        static const int NNodes = 10;

        using T = T_;

        using Vector = moonolith::Vector<T, Dim>;
        using CoVector = moonolith::Vector<T, PhysicalDim>;

        using Point = moonolith::Vector<T, Dim>;
        using CoPoint = moonolith::Vector<T, PhysicalDim>;

    private:
        ///@brief from The Finite Element Method: Its Basis and Fundamentals, 6th edition. Zienkievicz
        class Fun final {
        public:
            Fun() {
                // corner points
                f[0] = [](const Point &p) -> T {
                    const auto L0 = 1 - p.x - p.y - p.z;
                    return (2.0 * L0 - 1.0) * L0;
                };

                f[1] = [](const Point &p) -> T {
                    const auto L1 = p.x;
                    return (2.0 * L1 - 1.0) * L1;
                };

                f[2] = [](const Point &p) -> T {
                    const auto L2 = p.y;
                    return (2.0 * L2 - 1.0) * L2;
                };

                f[3] = [](const Point &p) -> T {
                    const auto L3 = p.z;
                    return (2.0 * L3 - 1.0) * L3;
                };

                // mid-edge points
                f[4] = [](const Point &p) -> T {
                    const auto L0 = 1 - p.x - p.y - p.z;
                    const auto L1 = p.x;
                    return 4.0 * L0 * L1;
                };

                f[5] = [](const Point &p) -> T {
                    const auto L1 = p.x;
                    const auto L2 = p.y;
                    return 4.0 * L1 * L2;
                };

                f[6] = [](const Point &p) -> T {
                    const auto L0 = 1 - p.x - p.y - p.z;
                    const auto L2 = p.y;
                    return 4.0 * L0 * L2;
                };

                f[7] = [](const Point &p) -> T {
                    const auto L0 = 1 - p.x - p.y - p.z;
                    const auto L3 = p.z;
                    return 4.0 * L0 * L3;
                };

                f[8] = [](const Point &p) -> T {
                    const auto L1 = p.x;
                    const auto L3 = p.z;
                    return 4.0 * L1 * L3;
                };

                f[9] = [](const Point &p) -> T {
                    const auto L2 = p.y;
                    const auto L3 = p.z;
                    return 4.0 * L2 * L3;
                };
            }

            std::array<std::function<T(const Point &)>, NNodes> f;
        };

        class Grad final {
        public:
            Grad() {
                // corner points
                f[0] = [](const Point &p, Vector &g) {
                    // f:= (2.0 * 1 - x - y - z - 1.0) * 1 - x - y - z;
                    auto gxyz = 4.0 * (p.x + p.y + p.z) - 3.0;
                    g.x = gxyz;
                    g.y = gxyz;
                    g.z = gxyz;
                };

                f[1] = [](const Point &p, Vector &g) {
                    // f:= (2.0 * x - 1.0) * x;
                    g.x = (4.0 * p.x - 1.0);
                    g.y = 0.0;
                    g.z = 0.0;
                };

                f[2] = [](const Point &p, Vector &g) {
                    // f:= (2.0 * y - 1.0) * y;
                    g.x = 0.0;
                    g.y = (4.0 * p.y - 1.0);
                    g.z = 0.0;
                };

                f[3] = [](const Point &p, Vector &g) {
                    // f:= (2.0 * z - 1.0) * z;
                    g.x = 0.0;
                    g.y = 0.0;
                    g.z = (4.0 * p.z - 1.0);
                };

                // mid-edge points
                f[4] = [](const Point &p, Vector &g) {
                    // f:= 4.0 * (1 - x - y - z) * x;
                    g.x = -4.0 * (2.0 * p.x + p.y + p.z) + 4.0;
                    g.y = -4.0 * p.x;
                    g.z = -4.0 * p.x;
                };

                f[5] = [](const Point &p, Vector &g) {
                    // f:= 4.0 * p.x * p.y;
                    g.x = 4.0 * p.y;
                    g.y = 4.0 * p.x;
                    g.z = 0.0;
                };

                f[6] = [](const Point &p, Vector &g) {
                    // f:= 4.0 * (1 - x - y - z) * y;
                    g.x = -4.0 * p.y;
                    g.y = -4.0 * (p.x + 2.0 * p.y + p.z) + 4.0;
                    g.z = -4.0 * p.y;
                };

                f[7] = [](const Point &p, Vector &g) {
                    // f:= 4.0 * (1 - x - y - z) * z;
                    g.x = -4.0 * p.z;
                    g.y = -4.0 * p.z;
                    g.z = -4.0 * (p.x + p.y + 2.0 * p.z) + 4.0;
                };

                f[8] = [](const Point &p, Vector &g) {
                    // f:= 4.0 * x * z;
                    g.x = 4.0 * p.z;
                    g.y = 0.0;
                    g.z = 4.0 * p.x;
                    ;
                };

                f[9] = [](const Point &p, Vector &g) {
                    // f:= 4.0 * y * z;
                    g.x = 0.0;
                    g.y = 4.0 * p.z;
                    g.z = 4.0 * p.y;
                };
            }

            std::array<std::function<void(const Point &, Vector &)>, NNodes> f;
        };

    public:
        T fun(const Integer i, const Point &p) const override { return fun_.f[i](p); }

        void grad(const Integer i, const Point &p, Vector &g) const override { grad_.f[i](p, g); }

        void hessian(const Integer, const Point &, std::array<T, Dim * Dim> &H) {
            std::fill(std::begin(H), std::end(H), 0.0);
        }

        void jacobian(const Point &p, std::array<T, PhysicalDim * Dim> &J) const override {
            if (is_affine()) {
                affine_approx_jacobian(J);
            } else {
                IsoParametric<Tet10>::jacobian(*this, p, J);
            }
        }

        void affine_approx_jacobian(std::array<T, PhysicalDim * Dim> &J) const override {
            Affine<Tet10>::jacobian(*this, J);
        }

        CoPoint &node(const Integer i) override { return nodes_[i]; }

        const CoPoint &node(const Integer i) const override { return nodes_[i]; }

        void point(const Point &p, CoPoint &q) const override { IsoParametric<Tet10>::point(*this, p, q); }

        Tet10() : is_affine_(false) {}

        inline bool is_affine() const override { return is_affine_; }
        inline bool is_simplex() const override { return true; }

        inline void set_affine(const bool val) { is_affine_ = val; }

        inline int n_nodes() const override { return NNodes; }
        inline int order() const override { return Order; }

        const std::array<CoPoint, NNodes> &nodes() const { return nodes_; }

        inline void make_reference() {
            Reference<Tet10>::points(nodes_);
            set_affine(true);
        }

        inline ElemType type() const override { return TET10; }

        inline T approx_measure() const override {
            assert(false);
            return tetrahedron_volume(nodes_[0], nodes_[1], nodes_[2], nodes_[3]);
        }

        inline T measure() const override {
            if (is_affine()) {
                return tetrahedron_volume(nodes_[0], nodes_[1], nodes_[2], nodes_[3]);
            } else {
                Quadrature<T, Dim> q;
                Gauss::get(2, q);
                return IsoParametric<Tet10>::measure(*this, q);
            }

            assert(false);
        }

        inline T reference_measure() const override { return 1.0 / 6.0; }

        inline void make(Storage<Polyhedron<T>> &poly) const override {
            if (is_affine()) {
                poly.resize(1);
                ConvexDecomposition<T, PhysicalDim>::make_tet(nodes(), 0, 1, 2, 3, poly[0]);
            } else {
                ConvexDecomposition<T, PhysicalDim>::decompose_tet10(nodes(), poly);
            }
        }

        inline void make(Polyhedron<T> &poly) const override {
            assert(is_affine());
            ConvexDecomposition<T, PhysicalDim>::make_tet(nodes(), 0, 1, 2, 3, poly);
        }

    private:
        bool is_affine_;
        std::array<CoPoint, NNodes> nodes_;
        const Fun fun_;
        const Grad grad_;
    };

    template <typename T, int PhysicalDim>
    class Reference<Tet10<T, PhysicalDim>> {
    public:
        using Point = typename Tet10<T, PhysicalDim>::Point;
        static const int NNodes = 10;

        template <std::size_t Size>
        static void points(std::array<Point, Size> &nodes) {
            static_assert(Size >= NNodes, "size must be at least 10");

            Reference<Tet4<T, PhysicalDim>>::points(nodes);

            // p4
            nodes[4].x = 0.5;
            nodes[4].y = 0.0;
            nodes[4].z = 0.0;

            // p5
            nodes[5].x = 0.5;
            nodes[5].y = 0.5;
            nodes[5].z = 0.0;

            // p6
            nodes[6].x = 0.0;
            nodes[6].y = 0.5;
            nodes[6].z = 0.0;

            // p7
            nodes[7].x = 0.0;
            nodes[7].y = 0.0;
            nodes[7].z = 0.5;

            // p8
            nodes[8].x = 0.5;
            nodes[8].y = 0.0;
            nodes[8].z = 0.5;

            // p9
            nodes[9].x = 0.0;
            nodes[9].y = 0.5;
            nodes[9].z = 0.5;
        }

        static T measure() { return 1. / 6.; }
    };

    template <typename T, int PhysicalDim>
    class GaussQRule<Tet10<T, PhysicalDim>> {
    public:
        static bool get(const Integer order, Quadrature3<T> &q) { return Gauss::get(order, q); }
    };

}  // namespace moonolith

#endif  // MOONOLITH_ELEM_TET10_HPP
