set(MOONOLITH_VERSION_MAJOR ${PROJECT_VERSION_MAJOR})
set(MOONOLITH_VERSION_MINOR ${PROJECT_VERSION_MINOR})
set(MOONOLITH_VERSION_PATCH ${PROJECT_VERSION_PATCH})
set(MOONOLITH_VERSION_TWEAK ${PROJECT_VERSION_TWEAK})
set(MOONOLITH_VERSION "${PROJECT_VERSION}")

find_package(Git QUIET)

if(Git_FOUND)

    execute_process(
        COMMAND "${GIT_EXECUTABLE}" describe --always HEAD
        WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
        RESULT_VARIABLE res
        OUTPUT_VARIABLE MOONOLITH_GIT_VERSION
        ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)

    set_property(GLOBAL APPEND PROPERTY CMAKE_CONFIGURE_DEPENDS
                                        "${CMAKE_SOURCE_DIR}/.git/index")

endif()

configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/configuration/moonolith_version.hpp.in
    ${CMAKE_BINARY_DIR}/moonolith_version.hpp)

# configure_file(
# ${CMAKE_CURRENT_SOURCE_DIR}/moonolith_configuration_details.hpp.in
# ${CMAKE_BINARY_DIR}/moonolith_configuration_details.hpp)

install(FILES "${CMAKE_BINARY_DIR}/moonolith_version.hpp" DESTINATION include)

# install(FILES "${CMAKE_BINARY_DIR}/moonolith_configuration_details.hp"
# DESTINATION include)
