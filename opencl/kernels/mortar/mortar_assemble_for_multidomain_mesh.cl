#ifndef INTERSECT_MESH_KERNEL_CL
#define INTERSECT_MESH_KERNEL_CL 

#include "intersect.cl"
#include "fe/fe_interfaces.cl"

#include "volume.cl"
#include "assembly/quadrature.cl"
#include "intersect_and_mortar_assemble.cl"

m_kernel__ void intersection_volume(		
							//input
							//A) basic data
							SizeType n_dims,						//0)
							SizeType n_elements,					//1)

							//B) Mesh
							m_global__ SizeType * el_ptr, 			//2) //n_elements+1
						    m_global__ SizeType * el_index, 			//3)
						    m_global__ SizeType * el_type,			//4)

						    //C) point data
						    SizeType n_nodes,						//5)
						    m_global__ Scalar	* points,				//6) N_DIMS x n_nodes
							const SizeType n_pairs,					//7)
							m_global__ SizeType * pairs,				//8) //n_pairs*2
							m_global__ Scalar * volumes				//9) //global_size(0)
						    //D) quadrature meta-data
						    // m_global__ SizeType * quad_meta, 			//7) (n_elements) x 3 [order, n_shape_fun_offset, n_quad_points_offset, element_matrix_offset]
)
{
	Mesh mesh;
	mesh.n_dims 	= n_dims;
	mesh.n_elements = n_elements;
	mesh.el_ptr 	= el_ptr;
	mesh.el_index 	= el_index;
	mesh.el_type 	= el_type;
	mesh.n_nodes 	= n_nodes;
	mesh.points 	= points;

	Scalar points1[MAX_SHAPE_FUNCS * MAX_N_DIMS];
	Scalar points2[MAX_SHAPE_FUNCS * MAX_N_DIMS];
	Scalar intersection[MAX_N_ISECT_POINTS * MAX_N_DIMS];
	SizeType n_intersection_points = 0;

	Scalar volume = 0;
	for(SizeType i = get_global_id(0); i < n_pairs; i+= get_global_size(0)) {
		const SizeType i2 = i*2;
		const SizeType element1 = pairs[i2];
		const SizeType element2 = pairs[i2+1];

		switch(n_dims) {
			case 2: 
			{
				const SizeType n_points_1 = mesh_n_points(mesh, element1);
				mesh_points(mesh, element1, points1);

				const SizeType n_points_2 = mesh_n_points(mesh, element2);
				mesh_points(mesh, element2, points2);

				KERNEL_ASSERT( polygon_area_2(n_points_1, points1) > 0, "element1: does not have positive area");
				KERNEL_ASSERT( polygon_area_2(n_points_2, points2) > 0, "element2: does not have positive area");

				if(intersect_convex_polygons(n_points_1, points1, n_points_2, points2, &n_intersection_points, intersection, DEFAULT_TOLLERANCE)) {
					KERNEL_ASSERT( polygon_area_2(n_intersection_points, intersection) > 0, "intersection area should be positive");
					volume += polygon_area_2(n_intersection_points, intersection);
				} else {
					// printf("<%d, %d>\n", element1, element2);
					// printf("failed isect\n");
					// matrix_print(n_points_1, 2, points1);
					// matrix_print(n_points_2, 2, points2);
				}

				break;
			}

			case 3: 
			{
				PMesh poly1, poly2, intersection;
				if(!make_polyhedron_from_element(mesh, element1, &poly1)) {
					KERNEL_ASSERT(false, "assemble_coupling_operator returned false");
					break;
				}

				if(!make_polyhedron_from_element(mesh, element2, &poly2)) {
					KERNEL_ASSERT(false, "assemble_coupling_operator returned false");
					break;
				}

				// p_mesh_print(&poly1);
				// p_mesh_print(&poly2);

				if(intersect_convex_polyhedra(poly1, poly2, &intersection)) {
					// printf("%d, %d -> %g\n", element1, element2, p_mesh_volume_3(intersection));

					// p_mesh_print(&intersection);
					volume += p_mesh_volume_3(intersection);
					// return;
				}

				break;
			}

			default:
			{
				KERNEL_ASSERT(false, "n_dims not supported");
				break;
			}
		}
	}

	volumes[get_global_id(0)] = volume;
}

m_kernel__ void assemble_coupling_operator(		
							//input
							//A) basic data
							const SizeType n_dims,					//0)
							const SizeType n_codims,				//1)
							const SizeType n_elements,				//2)
							const m_global__ SizeType * quad_meta, 	//3) (n_elements) x 3 [order, n_shape_fun_offset, n_quad_points_offset, element_matrix_offset]

							//B) Mesh
							m_global__ SizeType * el_ptr, 			//4) //n_elements+1
						    m_global__ SizeType * el_index, 			//5)
						    m_global__ SizeType * el_type,			//6)

						    //C) point data
						    SizeType n_nodes,						//7)
						    m_global__ Scalar	* points,				//8) N_DIMS x n_nodes
							const SizeType n_pairs,					//9)
							const m_global__ SizeType * pairs,		//10) //n_pairs*2 [layout: (master, slave)]
							m_global__ Scalar * element_matrices,		//11) //for each n_pairs k add (n_shape_functions_slave_k *  n_shape_functions_master_k) * n_codims ^ 2
							m_global__ SizeType * pairs_intersect		//12) //n_pairs
)
{
	Mesh mesh;
	mesh.n_dims 	= n_dims;
	mesh.n_elements = n_elements;
	mesh.el_ptr 	= el_ptr;
	mesh.el_index 	= el_index;
	mesh.el_type 	= el_type;
	mesh.n_nodes 	= n_nodes;
	mesh.points 	= points;

	MortarWorkspace w;

	for(SizeType i = get_global_id(0); i < n_pairs; i+= get_global_size(0)) {
		const SizeType i2 = i*2;
		const SizeType element_master = pairs[i2];
		const SizeType element_slave  = pairs[i2+1];

		const SizeType order_master = get_order(element_master, quad_meta);
		const SizeType order_slave  = get_order(element_slave,  quad_meta);

		const SizeType element_matrix_offset = get_element_matrix_offset(element_slave, quad_meta);

		const SizeType n_entries = intersect_and_mortar_assemble(
			mesh, element_master, order_master, n_codims,
			mesh, element_slave,  order_slave,  n_codims,
			&w);

		if(n_entries) {
			pairs_intersect[i] = true;
			generic_copy(n_entries, w.local_matrix, &element_matrices[element_matrix_offset]);
		} else {
			pairs_intersect[i] = false;
		}
	}
}

#endif //INTERSECT_MESH_KERNEL_CL
