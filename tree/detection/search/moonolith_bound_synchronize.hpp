#ifndef MOONOLITH_BOUND_SYNCHRONIZE_HPP
#define MOONOLITH_BOUND_SYNCHRONIZE_HPP

#include "moonolith_describable.hpp"
#include "moonolith_tree.hpp"

namespace moonolith {

	template <class Traits>
	class BoundSynchronize : public Describable {
	public:
		typedef typename Traits::Bound Bound;

		virtual bool has_static_serial_size() const = 0;
		virtual bool intersecting(const Integer rank) const = 0;

		/////////////////////////////////////////////////////////////////////////////////////
		virtual bool read_merge_for(const Integer rank, moonolith::InputStream &is) = 0;
		virtual bool write_local(OutputStream &os) const = 0;
		
		/////////////////////////////////////////////////////////////////////////////////////

		virtual const Bound &global_bound() const = 0;
	
		virtual void initialize(const Node<Traits> &root, const Integer n_procs) = 0;


		virtual void describe(std::ostream &) const
		{

		}
		
		virtual ~BoundSynchronize() {}

	};

}

#endif //MOONOLITH_BOUND_SYNCHRONIZE_HPP
