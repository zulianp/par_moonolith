#ifndef LOCAL_AND_GLOBAL_H
#define LOCAL_AND_GLOBAL_H 


#include "assemble_interfaces.cl"

SizeType block_get_i(const SizeType block_i, const SizeType test_n_dims, const SizeType block_ik)
{
	return block_i * test_n_dims + block_ik; 
}

SizeType block_get_j(const SizeType block_j, const SizeType trial_n_dims, const SizeType block_jl)
{
	return block_j * trial_n_dims + block_jl;
}


void block_global_to_local(const SizeType i, const SizeType n_codims, const FEObject * fe, 
						   const m_global__ SizeType * el_ptr, 
						   const m_global__ SizeType * el_index, 
						   const m_global__ Scalar * displacement, 
						   Scalar * local_displacement)
{
	const SizeType begin = el_ptr[i];
	const SizeType end   = el_ptr[i+1];
	
	KERNEL_ASSERT(fe->n_shape_functions == (end - begin), "block_global_to_local: indexing does not match the fe->n_shape_functions");

	SizeType index = 0;
	for(SizeType i = begin; i < end; ++i) {
		const SizeType node = el_index[i];
		const SizeType offset = node * n_codims;

		for(SizeType d = 0; d < n_codims; ++d) {
			local_displacement[index++] = displacement[offset+d];
		}
	}
}

#endif //LOCAL_AND_GLOBAL_H
