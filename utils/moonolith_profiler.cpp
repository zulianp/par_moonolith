
#include "moonolith_profiler.hpp"
#include <mpi.h>

namespace moonolith {

    void Profiler::event_begin(const std::string &event_name) {
        if (event_active_) {
            std::cerr << "[Warning] previous event not ended ending it automatically" << std::endl;
            event_end();
        } else {
            Communicator().barrier();
        }

        active_event_ = event_name;
        elapsed_ = MPI_Wtime();
    }

    void Profiler::event_end() {
        // local time
        Event &event = events_[active_event_];
        event.occurence(MPI_Wtime() - elapsed_);

        Communicator().barrier();
        // global time
        elapsed_ = MPI_Wtime() - elapsed_;
        event.add_to_global_total_time(elapsed_);
    }

    void Profiler::describe(std::ostream &os) const {
        Communicator world;

        if (world.is_root()) {
            os << "Rank, Name, ";
            Event::describe_header_entries(os);
            os << "\n";

            for (std::map<std::string, Event>::const_iterator it = events_.begin(); it != events_.end(); ++it) {
                os << "All"
                   << ", ";
                os << it->first << ", ";
                it->second.describe_global(os);
                os << "\n";
            }
        }

        double total_time = 0;
        Integer total_occurrence = 0;
        for (std::map<std::string, Event>::const_iterator it = events_.begin(); it != events_.end(); ++it) {
            os << world.rank() << ", ";
            os << it->first << ", " << it->second << "\n";
            total_time += it->second.total_time_;
            total_occurrence += it->second.occurrence_;
        }

        os << world.rank() << ", ";
        os << "Total, " << total_occurrence << ", " << total_time << "\n";
    }

    bool Profiler::save(const std::string &path) {
        std::ofstream os;
        os.open(path.c_str());
        if (!os.good()) {
            os.close();
            return false;
        }

        describe(os);

        os.close();
        return true;
    }
}  // namespace moonolith
