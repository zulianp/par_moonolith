#ifndef MOONOLITH_SYNCH_LOOK_UP_TABLE_BUILDER_HPP
#define MOONOLITH_SYNCH_LOOK_UP_TABLE_BUILDER_HPP

#include "moonolith_assert.hpp"
#include "moonolith_bound_synchronize.hpp"
#include "moonolith_broad_phase_detector.hpp"
#include "moonolith_communicator.hpp"
#include "moonolith_lookup_table.hpp"
#include "moonolith_lookup_table_builder.hpp"
#include "moonolith_predicate.hpp"
#include "moonolith_traversal_algorithm.hpp"

namespace moonolith {

    template <class Tree>
    class SynchLookUpTableBuilder : public LookUpTableBuilder<Tree> {
    public:
        void setBoundSynchronize(const std::shared_ptr<BoundSynchronize<typename Tree::Traits> > &bound_synchronize) {
            bound_synchronize_ = bound_synchronize;
        }

        std::shared_ptr<BoundSynchronize<typename Tree::Traits> > bound_synchronize() { return bound_synchronize_; }

        bool build_for(Communicator &comm, Tree &tree, const std::shared_ptr<LookUpTable> &lookup_table) override {
            if (comm.is_alone()) return true;

            assert(!comm.has_pending_requests());

            clear();

            if (this->verbose()) {
                if (comm.is_root()) logger() << "Using SynchLookUpTableBuilder" << std::endl;
            }

            if (!bound_synchronize_) {
                bound_synchronize_ = tree.mutator_factory()->new_bound_synchronize();
            }

            BroadPhaseDetector<Tree> broad_phase_detector;
            broad_phase_detector.set_verbose(this->verbose());

            if (!broad_phase_detector.detect(comm, tree, *bound_synchronize_)) {
                assert(false);
                return false;
            }

            assert(!comm.has_pending_requests());

            MOONOLITH_DEBUG(std::vector<Integer> connected(comm.size(), 0));

            for (Integer rank = 0; rank < comm.size(); ++rank) {
                // Skip self
                if (rank == comm.rank()) continue;

                if (bound_synchronize_->intersecting(rank)) {
                    stats_.n_related_procs++;
                    traversals_.push_back(this->new_traversal_algorithm(tree, rank, lookup_table));

                    MOONOLITH_DEBUG(connected[rank] = 1);
                }
            }

            if (this->verbose()) root_describe("[Status] Broad-phase detection END", comm, logger());

            // MOONOLITH_DEBUG({ if(this->verbose()) { synch_describe(connected, comm, logger()); }});
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            // BEGIN: Middle-phase detection
            recv_buffers_.resize(comm.size());
            send_buffers_.resize(comm.size());

            std::vector<Integer> index_map(comm.size());
            MOONOLITH_DEBUG(std::fill(index_map.begin(), index_map.end(), -1););

            MOONOLITH_DEBUG(std::vector<bool> recv_check(comm.size()); std::vector<bool> send_check(comm.size()););

            Integer previous_n_traversals = -1;
            while (!traversals_.empty()) {
                stats_.n_levels++;
                const Integer level_tag = stats_.n_levels;

                MOONOLITH_DEBUG(std::fill(recv_check.begin(), recv_check.end(), false));
                MOONOLITH_DEBUG(std::fill(send_check.begin(), send_check.end(), false));

                comm.clear();

                if (this->verbose())
                    logger() << comm << "level " << stats_.n_levels << " nTraverals " << traversals_.size()
                             << std::endl;

                // Send all level info
                for (TraversalIter it = traversals_.begin(); it != traversals_.end(); ++it) {
                    const Integer rank = (*it)->rank();
                    ByteOutputBuffer &os = send_buffers_[rank];

                    MOONOLITH_ASSERT_EQUAL_MSG(rank, (*it)->rank(), "On rank: " + std::to_string(comm.rank()));

                    os.clear();
                    (*it)->write_next_level(os);
                    comm.i_send(os, rank, level_tag);

                    if (this->verbose())
                        logger() << comm << " Sending " << os.size() << " bytes to " << rank << std::endl;
                    assert(!send_check[rank]);
                    MOONOLITH_DEBUG(send_check[rank] = true);
                }

                /// remap index
                const Integer n_traversals = static_cast<Integer>(traversals_.size());
                if (previous_n_traversals != n_traversals) {
                    Integer index = 0;
                    for (TraversalIter it = traversals_.begin(); it != traversals_.end(); ++it, ++index) {
                        index_map[(*it)->rank()] = index;
                    }

                    previous_n_traversals = n_traversals;
                }

                for (Integer i = 0; i < n_traversals; ++i) {
                    Integer rank, size;
                    while (!comm.i_probe_any_with_tag<byte>(&rank, &size, level_tag)) {
                    }

                    assert(size > 0);
                    ByteInputBuffer &is(recv_buffers_[rank]);

                    MOONOLITH_DEBUG(if (recv_check[rank]) {
                        logger() << comm << " Already received " << is.size() << " bytes from " << rank << std::endl;
                    });

                    if (this->verbose())
                        logger() << comm << " Receiving " << size << " bytes from " << rank << " at level " << level_tag
                                 << std::endl;

                    MOONOLITH_ASSERT_MSG(!recv_check[rank], "On rank " + std::to_string(comm.rank()));
                    MOONOLITH_DEBUG(recv_check[rank] = true);

                    is.clear();
                    is.reserve(size);
                    comm.i_recv(is.raw_ptr(), size, rank, level_tag);
                }

                for (Integer i = 0; i < n_traversals; ++i) {
                    Integer rank, index;
                    while (!comm.test_recv_any(&rank, &index)) {
                    }

                    ByteInputBuffer &is(recv_buffers_[rank]);
                    const Integer mapped_index = index_map[rank];

                    if (this->verbose()) logger() << "Processing Traversal for " << rank << std::endl;
                    if (this->verbose())
                        logger() << comm << " reading next level, index=" << mapped_index << ", rank=" << rank
                                 << std::endl;
                    // if(this->verbose()) logger() << std::to_string(index_map.begin(), index_map.end()) << std::endl;

                    assert(mapped_index >= 0);
                    MOONOLITH_ASSERT_EQUAL_MSG(
                        rank, traversals_[mapped_index]->rank(), "On rank: " + std::to_string(comm.rank()));
                    assert(mapped_index < Integer(traversals_.size()));

                    traversals_[mapped_index]->read_next_level(is);
                    traversals_[mapped_index]->process_level();
                }

                comm.wait_all();

                //// clean-up buffers and remove terminated algorithms from _traversals
                if (this->verbose()) {
                    logger() << comm << " Cleaning up " << std::endl;
                }

                for (TraversalIter it = traversals_.begin(); it != traversals_.end(); /* inside loop */) {
                    if ((*it)->terminated()) {
                        if (this->verbose()) logger() << comm << " " << (*it)->rank() << " terminated " << std::endl;
                        // free unused memory
                        send_buffers_[(*it)->rank()].clear();
                        recv_buffers_[(*it)->rank()].clear();

                        if (this->verbose()) logger() << "Removed traversal" << std::endl;
                        it = traversals_.erase(it);

                    } else {
                        // reset read/write position without deallocating memory
                        send_buffers_[(*it)->rank()].reset_offset();
                        recv_buffers_[(*it)->rank()].reset_offset();
                        ++it;
                    }
                }
            }

            if (this->verbose() && comm.is_root()) {
                logger() << "[Status] Middle-phase detection END" << std::endl;
            }

            if (this->verbose()) stats_.describe(logger());
            // END: Middle-phase detection

            comm.barrier();
            return true;
        }

        void clear() {
            traversals_.clear();
            recv_buffers_.clear();
            send_buffers_.clear();
            stats_.clear();
        }

        SynchLookUpTableBuilder() {}

    private:
        typedef std::shared_ptr<TraversalAlgorithm<Tree> > TraversalAlgorithmPtr;
        typedef std::vector<TraversalAlgorithmPtr> TraversalList;
        typedef typename TraversalList::iterator TraversalIter;

        TraversalList traversals_;
        std::vector<ByteInputBuffer> recv_buffers_;
        std::vector<ByteOutputBuffer> send_buffers_;
        std::shared_ptr<BoundSynchronize<typename Tree::Traits> > bound_synchronize_;

        class BuilderStats : public Describable {
        public:
            Integer n_related_procs;
            Integer n_levels;

            BuilderStats() : n_related_procs(0), n_levels(0) {}

            void clear() {
                n_related_procs = 0;
                n_levels = 0;
            }

            void describe(std::ostream &os) const {
                os << "n_releated_procs: " << n_related_procs << "\n";
                os << "n_levels: " << n_levels << "\n";
            }
        };

        BuilderStats stats_;
    };
}  // namespace moonolith

#endif  // MOONOLITH_SYNCH_LOOK_UP_TABLE_BUILDER_HPP
