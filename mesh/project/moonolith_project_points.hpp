#ifndef MOONOLITH_PROJECT_POINTS_HPP
#define MOONOLITH_PROJECT_POINTS_HPP

#include "moonolith_plane.hpp"
#include "moonolith_polygon.hpp"
#include "moonolith_householder.hpp"
#include "moonolith_intersect_polygons.hpp"
#include "moonolith_triangulate.hpp"

namespace moonolith {

	template<typename T, int Dim, int SubDim>
	inline static void project_points(
		const HouseholderTransformation<T, Dim> &trafo,
		const Vector<T, Dim> &translation,
		const Storage<Vector<T, Dim>> &in,
		Storage<Vector<T, SubDim>> &out)
	{
		auto n_points = in.size();
		out.resize(n_points);

		Vector<T, Dim> temp;
		for(std::size_t i = 0; i < n_points; ++i) {
			temp = in[i] - translation;
			trafo.apply(temp, out[i]);
		}
	}

	template<typename T, int Dim, int SubDim>
	inline static void unproject_points(
		const HouseholderTransformation<T, Dim> &trafo,
		const Vector<T, Dim> &translation,
		const Storage<Vector<T, SubDim>> &in,
		Storage<Vector<T, Dim>> &out)
	{
		auto n_points = in.size();
		out.resize(n_points);

		for(std::size_t i = 0; i < n_points; ++i) {
			trafo.apply(in[i], out[i]);
			out[i] += translation;
		}
	}
}


#endif //MOONOLITH_PROJECT_POINTS_HPP