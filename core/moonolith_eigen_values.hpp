#ifndef MOONOLITH_EIGEN_VALUES_HPP
#define MOONOLITH_EIGEN_VALUES_HPP

#include "moonolith_invert.hpp"
#include "moonolith_matrix.hpp"

#include <array>
#include <cmath>

namespace moonolith {

    template <class Matrix>
    class EigenValues {};

    template <typename T>
    class EigenValues<std::array<T, 1>> {
    public:
        using Matrix = std::array<T, 1>;
        inline static void apply(const Matrix &m, T *result) { result[0] = m[0]; }
    };

    template <typename T>
    class EigenValues<std::array<T, 4>> {
    public:
        using Matrix = std::array<T, 4>;

        inline static void apply(const Matrix &m, T *result) {
            const T a = m[0], b = m[1], c = m[2], d = m[3];
            const T discr = (a + d) * (a + d) - 4.0 * (a * d - c * b);
            result[0] = (a + d - std::sqrt(discr)) / 2.;
            result[1] = (a + d + std::sqrt(discr)) / 2.;
        }
    };

    template <typename T>
    class EigenValues<std::array<T, 9>> {
    public:
        using Matrix = std::array<T, 9>;
        static constexpr T PI =
            static_cast<T>(3.141592653589793238462643383279502884197169399375105820974944592307816406286);

        inline static void apply(const Matrix &mat, T *result) {
            // Algorithm from https://en.wikipedia.org/wiki/Eigenvalue_algorithm#3%C3%973_matrices
            const T d1 = mat[0 * 3 + 0];
            const T d2 = mat[1 * 3 + 1];
            const T d3 = mat[2 * 3 + 2];

            const T a12 = mat[0 * 3 + 1];
            const T a13 = mat[0 * 3 + 2];
            const T a23 = mat[1 * 3 + 2];

            const T p1 = a12 * a12 + a13 * a13 + a23 * a23;

            // if(std::approxeq(p1, 0., std::epsilon<T>())) {
            if (p1 == 0.) {
                // diagonal matrix
                result[0] = d1;
                result[1] = d2;
                result[2] = d3;
            } else {
                const T q = (d1 + d2 + d3) / 3;
                const T d1mq = d1 - q;
                const T d2mq = d2 - q;
                const T d3mq = d3 - q;

                const T p2 = d1mq * d1mq + d2mq * d2mq + d3mq * d3mq + 2.0 * p1;
                const T p = std::sqrt(p2 / 6);

                Matrix temp;
                std::fill(temp.begin(), temp.end(), 0.0);

                temp[0 * 3 + 0] = 1.0;
                temp[1 * 3 + 1] = 1.0;
                temp[2 * 3 + 2] = 1.0;

                for (int i = 0; i < 9; ++i) {
                    temp[i] = (1.0 / p) * (mat[i] - q * temp[i]);
                }

                const T r = det3(temp) / 2;

                T cos_phi = 0.0;
                T cos_phi_shifted = 0.0;

                if (r <= -1.0) {
                    cos_phi = 0.5;          // cos(pi/3)
                    cos_phi_shifted = -1.;  // cos(pi/3 + (2*pi/3));
                } else if (r >= 1.0) {
                    cos_phi = 1;             // cos(0)
                    cos_phi_shifted = -0.5;  // cos(0 + (2*pi/3));
                } else {
                    const T acos_r = std::acos(r) / 3.0;
                    cos_phi = std::cos(acos_r);
                    cos_phi_shifted = std::cos(acos_r + 2.0 / 3.0 * PI);
                }

                result[0] = q + 2 * p * cos_phi;
                result[1] = q + 2 * p * cos_phi_shifted;
                result[2] = 3 * q - result[0] - result[1];
            }
        }
    };

    // template <typename T, int Rows, int Cols>
    // class EigenValues<Matrix<T, Rows, Cols>> {
    // public:
    //     using Matrix = moonolith::Matrix<T, Rows, Cols>;

    //     template <class Result>
    //     inline static void apply(const Matrix &mat, Result &result) {
    //         const auto rows = mat.rows();
    //         const auto cols = mat.cols();

    //         static_assert(rows == cols, "matrix must be squared!");
    //         assert(rows == result.size());

    //         switch (rows) {
    //             case 1: {
    //                 result[0] = mat(0, 0);
    //                 return;
    //             }
    //             case 2: {
    //                 apply_2(expr, result);
    //                 return;
    //             }
    //             case 3: {
    //                 apply_3(expr, result);
    //                 return;
    //             }
    //             default: {
    //                 assert(false);
    //                 result.set(0.0);
    //                 return;
    //             }
    //         }
    //     }

    // private:
    //     template <class Result>
    //     inline static void apply_2(const Matrix &m, Result &result) {
    //         const T a = m(0, 0), b = m(0, 1), c = m(1, 0), d = m(1, 1);
    //         const T discr = (a + d) * (a + d) - 4.0 * (a * d - c * b);
    //         result(0) = (a + d - std::sqrt(discr)) / 2.;
    //         result(1) = (a + d + std::sqrt(discr)) / 2.;
    //     }

    //     template <class Result>
    //     inline static void apply_3(const Matrix &m, Result &result) {
    //         // Algorithm from https://en.wikipedia.org/wiki/Eigenvalue_algorithm#3%C3%973_matrices
    //         const T d1 = m(0, 0);
    //         const T d2 = m(1, 1);
    //         const T d3 = m(2, 2);

    //         const T a12 = m(0, 1);
    //         const T a13 = m(0, 2);
    //         const T a23 = m(1, 2);

    //         const T p1 = a12 * a12 + a13 * a13 + a23 * a23;

    //         // if(std::approxeq(p1, 0., std::epsilon<T>())) {
    //         if (p1 == 0.) {
    //             // diagonal matrix
    //             result(0) = d1;
    //             result(1) = d2;
    //             result(2) = d3;
    //         } else {
    //             const T q = (d1 + d2 + d3) / 3;
    //             const T d1mq = d1 - q;
    //             const T d2mq = d2 - q;
    //             const T d3mq = d3 - q;

    //             const T p2 = d1mq * d1mq + d2mq * d2mq + d3mq * d3mq + 2.0 * p1;
    //             const T p = std::sqrt(p2 / 6);

    //             Matrix id;
    //             id.identity();
    //             const T r = det((1.0 / p) * (m - q * id)) / 2;

    //             T cos_phi = 0.0;
    //             T cos_phi_shifted = 0.0;

    //             if (r <= -1.0) {
    //                 cos_phi = 0.5;          // cos(pi/3)
    //                 cos_phi_shifted = -1.;  // cos(pi/3 + (2*pi/3));
    //             } else if (r >= 1.0) {
    //                 cos_phi = 1;             // cos(0)
    //                 cos_phi_shifted = -0.5;  // cos(0 + (2*pi/3));
    //             } else {
    //                 const T acos_r = std::acos(r) / 3.0;
    //                 cos_phi = std::cos(acos_r);
    //                 cos_phi_shifted = std::cos(acos_r + 2.0 / 3.0 * M_PI);
    //             }

    //             result[0] = q + 2 * p * cos_phi;
    //             result[1] = q + 2 * p * cos_phi_shifted;
    //             result[2] = 3 * q - result[0] - result[1];
    //         }
    //     }
    // };

}  // namespace moonolith

#endif  // MOONOLITH_EIGEN_VALUES_HPP
