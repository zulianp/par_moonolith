#ifndef MOONOLITH_TRANSFORM_HPP
#define MOONOLITH_TRANSFORM_HPP

#include "moonolith_expanding_array.hpp"
#include "moonolith_vector.hpp"

#include <array>

namespace moonolith {

    template <typename T, int Dim, int PhysicalDim = Dim>
    class Transform {
    public:
        using Vector = moonolith::Vector<T, Dim>;
        using CoVector = moonolith::Vector<T, PhysicalDim>;

        using CoPoint = moonolith::Vector<T, PhysicalDim>;
        using Point = moonolith::Vector<T, Dim>;

        virtual ~Transform() {}

        virtual bool apply(const Point &in, CoPoint &out) = 0;
        virtual bool apply_inverse(const CoPoint &in, Point &out) = 0;

        virtual T estimate_condition_number() const = 0;
    };

    template <typename T, int PhysicalDim>
    class Transform<T, 1, PhysicalDim> {
    public:
        using Vector = moonolith::Vector<T, 1>;
        using CoVector = moonolith::Vector<T, PhysicalDim>;

        using CoPoint = moonolith::Vector<T, PhysicalDim>;
        using Point = moonolith::Vector<T, 1>;

        virtual ~Transform() {}

        virtual bool apply(const Point &in, CoPoint &out) = 0;
        virtual bool apply_inverse(const CoPoint &in, Point &out) = 0;

        virtual bool apply(const T &in, CoPoint &out) {
            Point in_p = {in};
            return apply(in_p, out);
        }

        virtual bool apply_inverse(const CoPoint &in, T &out) {
            Point out_p;
            bool ok = apply_inverse(in, out_p);
            out = out_p.x;
            return ok;
        }

        virtual T estimate_condition_number() const = 0;
    };

}  // namespace moonolith

#endif  // MOONOLITH_TRANSFORM_HPP
