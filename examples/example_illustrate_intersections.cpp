// general
#include "par_moonolith.hpp"

// specific
#include "moonolith_mesh_io.hpp"
#include "moonolith_par_l2_transfer.hpp"

#include "moonolith_convex_decomposition.hpp"
#include "moonolith_matlab_scripter.hpp"

using namespace moonolith;

void show_1D() {}

template <class Elem>
void show_2D_affine(const std::string &name, Elem &e) {
    MatlabScripter script;
    script.close_all();
    script.hold_on();

    Polygon<Real, 2> poly;
    e.make(poly);

    script.plot(poly, "\'k\',\'LineWidth\',2");
    script.plot(e.nodes(), "\'k.\', \'MarkerSize\', 30");

    script.axis_equal();
    script.axis_tight();
    script.hide_axis();

    script.save_figure_as(name + ".bmp");
    script.save(name + ".m");
}

void show_2D() {
    Tri3<Real> tri3;
    tri3.make_reference();
    show_2D_affine("tri3", tri3);

    Tri6<Real> tri6;
    tri6.make_reference();
    show_2D_affine("tri6", tri6);

    tri6.node(4) *= 1.2;
    tri6.set_affine(false);

    show_2D_affine("warped_tri6", tri6);

    Quad4<Real> quad4;
    quad4.make_reference();
    show_2D_affine("quad4", quad4);

    Quad8<Real> quad8;
    quad8.make_reference();
    show_2D_affine("quad8", quad8);

    quad8.node(5)[0] *= 0.9;
    quad8.set_affine(false);

    show_2D_affine("warped_quad8", quad8);

    Quad9<Real> quad9;
    quad9.make_reference();
    show_2D_affine("quad9", quad9);

    quad9.node(5)[0] *= 0.9;
    quad9.set_affine(false);

    show_2D_affine("warped_quad9", quad9);
}

template <class Elem>
void show_3D_affine(const std::string &name, Elem &e) {
    MatlabScripter script;
    script.close_all();
    script.hold_on();

    Polyhedron<Real> poly;
    e.make(poly);

    script.plot(poly, false, "\'LineWidth\', 2, \'EdgeColor\', \'k\'");
    script.plot(e.nodes(), "\'.k\', \'MarkerSize\', 30");
    // script.plot(poly, false, "\'FaceAlpha\', 0.0,\'LineWidth\', 1, \'EdgeColor\', \'k\', \'LineStyle\',\'--\'");

    script.axis_equal();
    script.axis_tight();
    script.hide_axis();
    script.view(-48.3, 19.6975182481752);

    // This is still a MATLAB directive
    script.save_figure_as(name + ".bmp");

    // Save the matlab file
    script.save(name + ".m");
}

void plot_polyhedra(Storage<Polyhedron<Real>> &poly, MatlabScripter &script) {
    for (const auto &p : poly) {
        script.plot(p, false, "\'LineWidth\', 2, \'EdgeColor\', \'k\'");
    }
}

template <class Elem>
void show_3D_warped(const std::string &name, Elem &e) {
    MatlabScripter script;
    script.close_all();
    script.hold_on();

    {
        Storage<Polyhedron<Real>> decomposed_poly;
        e.set_affine(false);
        e.make(decomposed_poly);

        script.close_all();
        script.hold_on();

        const std::size_t n = decomposed_poly.size();

        for (std::size_t i = 0; i < n; ++i) {
            const auto &p = decomposed_poly[i];
            auto m = measure(p);
            assert(m > 0.);
            MOONOLITH_UNUSED(m);

            script.plot(p, false, "\'FaceAlpha\', 0.0,\'LineWidth\', 1, \'EdgeColor\', \'k\', \'LineStyle\',\'--\'");
        }

        script.plot(e.nodes(), "\'.k\', \'MarkerSize\', 30");

        Polyhedron<Real> poly;
        e.set_affine(true);
        e.make(poly);
        script.plot(poly, false, "\'FaceAlpha\', 0.4,\'LineWidth\', 3, \'EdgeColor\', \'k\'");

        script.axis_equal();
        script.axis_tight();
        script.hide_axis();
        script.view(-48.3, 19.6975182481752);

        // This is still a MATLAB directive
        script.save_figure_as(name + "_decomposed.bmp");
    }

    // Save the matlab file
    script.save(name + "_decomposed.m");
}

void show_3D() {
    ////////////////////////////////////////
    Tet4<Real> tet4;
    tet4.make_reference();

    show_3D_affine("tet4", tet4);

    ////////////////////////////////////////

    Tet10<Real> tet10;
    tet10.make_reference();

    tet10.set_affine(true);
    show_3D_affine("tet10", tet10);

    // FIXME these do not work properly (maybe we have to plot curves)

    tet10.node(1) *= 0.75;

    show_3D_affine("warped_tet10", tet10);

    tet10.set_affine(false);
    show_3D_warped("warped_tet10", tet10);

    ////////////////////////////////////////

    Hex8<Real> hex8;
    hex8.make_reference();

    hex8.set_affine(true);
    show_3D_affine("hex8", hex8);

    hex8.node(7) *= 0.75;

    hex8.set_affine(false);
    show_3D_warped("warped_hex8", hex8);
    ////////////////////////////////////////

    Hex27<Real> hex27;
    hex27.make_reference();

    show_3D_affine("hex27", hex27);

    ConvexDecomposition<Real, 3>::make_hex27_from_hex8(hex8.nodes(), hex27.nodes());

    show_3D_warped("warped_hex27", hex27);

    ////////////////////////////////////////

    Prism6<Real> prism6;
    prism6.make_reference();

    show_3D_affine("prism6", prism6);
}

// We have 4D but do not know who to show it

/// Compile examples with `make moonolith_examples`
/// Run for instance with  `./examples/example_illustrate_intersections`
/// Figures can be displayed using matlab and converted using potrace
/// Example
/// matlab -nodisplay -nodesktop -r "run('./warped_hex8_decomposed.m'); quit;"
/// potrace warped_hex8_decomposed.bmp
int main(int argc, char *argv[]) {
    Moonolith::Init(argc, argv);

    show_2D();
    show_3D();

    return Moonolith::Finalize();
}