#include "moonolith_elem_edge.hpp"
#include "moonolith_elem_edge1.hpp"
#include "moonolith_elem_edge2.hpp"
#include "moonolith_elem_edge3.hpp"
#include "moonolith_elem_triangle.hpp"
#include "moonolith_intersect_polyhedra_impl.hpp"
#include "moonolith_l2_assembler_impl.hpp"

#include "moonolith_elem_tri.hpp"
#include "moonolith_elem_tri1.hpp"
#include "moonolith_elem_tri3.hpp"
#include "moonolith_elem_tri6.hpp"

#include "moonolith_elem_quad.hpp"
#include "moonolith_elem_quad1.hpp"
#include "moonolith_elem_quad4.hpp"
#include "moonolith_elem_quad8.hpp"
#include "moonolith_elem_quad9.hpp"

#include "moonolith_elem_tet.hpp"
#include "moonolith_elem_tet1.hpp"
#include "moonolith_elem_tet10.hpp"
#include "moonolith_elem_tet4.hpp"

#include "moonolith_elem_hex.hpp"
#include "moonolith_elem_hex1.hpp"
#include "moonolith_elem_hex27.hpp"
#include "moonolith_elem_hex8.hpp"

namespace moonolith {
    template class BuildQuadratureAlgo<Real, 1, 1, 1>;
    template class BuildQuadratureAlgo<Real, 2, 2, 2>;
    template class BuildQuadratureAlgo<Real, 3, 3, 3>;
    template class BuildQuadratureAlgo<Real, 3, 3, 2>;
    template class BuildQuadratureAlgo<Real, 2, 2, 1>;
    template class BuildQuadratureAlgo<Real, 2, 3, 2>;

    /// Constant
    template class L2Transfer<Edge1<Real>, Edge1<Real>>;
    template class L2Transfer<Tri1<Real>, Tri1<Real>>;
    template class L2Transfer<Quad1<Real>, Quad1<Real>>;
    template class L2Transfer<Tet1<Real>, Tet1<Real>>;
    template class L2Transfer<Hex1<Real>, Hex1<Real>>;

    /// First order
    template class L2Transfer<Edge2<Real>, Edge2<Real>>;
    template class L2Transfer<Tri3<Real>, Tri3<Real>>;
    template class L2Transfer<Quad4<Real>, Quad4<Real>>;
    template class L2Transfer<Tet4<Real>, Tet4<Real>>;
    template class L2Transfer<Hex8<Real>, Hex8<Real>>;

    /// Second order
    template class L2Transfer<Edge3<Real>, Edge3<Real>>;
    template class L2Transfer<Tri6<Real>, Tri6<Real>>;
    template class L2Transfer<Quad8<Real>, Quad8<Real>>;
    template class L2Transfer<Quad9<Real>, Quad9<Real>>;
    template class L2Transfer<Tet10<Real>, Tet10<Real>>;
    template class L2Transfer<Hex27<Real>, Hex27<Real>>;

    // MIXED ORDER

    // Triangle
    template class L2Transfer<Tri6<Real>, Tri3<Real>>;
    template class L2Transfer<Tri3<Real>, Tri6<Real>>;
    template class L2Transfer<Tri6<Real>, Tri1<Real>>;
    template class L2Transfer<Tri3<Real>, Tri1<Real>>;

    template class L2Transfer<Tri1<Real, 3>, Tri1<Real, 3>>;
    template class L2Transfer<Tri3<Real, 3>, Tri3<Real, 3>>;
    template class L2Transfer<Tri6<Real, 3>, Tri6<Real, 3>>;

    // Quad
    template class L2Transfer<Quad8<Real>, Quad4<Real>>;
    template class L2Transfer<Quad4<Real>, Quad8<Real>>;
    template class L2Transfer<Quad8<Real>, Quad1<Real>>;
    template class L2Transfer<Quad4<Real>, Quad1<Real>>;

    // Tetrahedron
    template class L2Transfer<Tet10<Real>, Tet4<Real>>;
    template class L2Transfer<Tet4<Real>, Tet10<Real>>;
    template class L2Transfer<Tet10<Real>, Tet1<Real>>;
    template class L2Transfer<Tet4<Real>, Tet1<Real>>;

    // Hexahedron
    template class L2Transfer<Hex27<Real>, Hex8<Real>>;
    // template class L2Transfer<Hex8<Real>,    Hex27<Real>>;
    template class L2Transfer<Hex27<Real>, Hex1<Real>>;
    template class L2Transfer<Hex8<Real>, Hex1<Real>>;

    // MIXED Geometry
    template class L2Transfer<Tri1<Real>, Quad1<Real>>;
    template class L2Transfer<Tri1<Real>, Quad4<Real>>;
    template class L2Transfer<Tri1<Real>, Quad8<Real>>;

    template class L2Transfer<Tri3<Real>, Quad1<Real>>;
    template class L2Transfer<Tri3<Real>, Quad4<Real>>;
    template class L2Transfer<Tri3<Real>, Quad8<Real>>;

    template class L2Transfer<Tri6<Real>, Quad1<Real>>;
    template class L2Transfer<Tri6<Real>, Quad4<Real>>;
    template class L2Transfer<Tri6<Real>, Quad8<Real>>;

    template class L2Transfer<Quad1<Real>, Tri1<Real>>;
    template class L2Transfer<Quad1<Real>, Tri3<Real>>;
    template class L2Transfer<Quad1<Real>, Tri6<Real>>;

    template class L2Transfer<Quad4<Real>, Tri1<Real>>;
    template class L2Transfer<Quad4<Real>, Tri3<Real>>;
    template class L2Transfer<Quad4<Real>, Tri6<Real>>;

    template class L2Transfer<Quad8<Real>, Tri1<Real>>;
    template class L2Transfer<Quad8<Real>, Tri3<Real>>;
    template class L2Transfer<Quad8<Real>, Tri6<Real>>;

    template class L2Transfer<Quad9<Real>, Tri1<Real>>;
    template class L2Transfer<Quad9<Real>, Tri3<Real>>;
    template class L2Transfer<Quad9<Real>, Tri6<Real>>;

    template class L2Transfer<Tet1<Real>, Hex1<Real>>;
    template class L2Transfer<Tet1<Real>, Hex8<Real>>;
    // template class L2Transfer<Tet1<Real>, Hex27<Real>>;

    template class L2Transfer<Tet4<Real>, Hex1<Real>>;
    template class L2Transfer<Tet4<Real>, Hex8<Real>>;
    // template class L2Transfer<Tet4<Real>, Hex27<Real>>;

    template class L2Transfer<Tet10<Real>, Hex1<Real>>;
    template class L2Transfer<Tet10<Real>, Hex8<Real>>;
    // template class L2Transfer<Tet10<Real>, Hex27<Real>>;

    template class L2Transfer<Hex1<Real>, Tet1<Real>>;
    template class L2Transfer<Hex1<Real>, Tet4<Real>>;
    template class L2Transfer<Hex1<Real>, Tet10<Real>>;

    template class L2Transfer<Hex8<Real>, Tet1<Real>>;
    template class L2Transfer<Hex8<Real>, Tet4<Real>>;
    template class L2Transfer<Hex8<Real>, Tet10<Real>>;

    template class L2Transfer<Hex27<Real>, Tet1<Real>>;
    template class L2Transfer<Hex27<Real>, Tet4<Real>>;
    template class L2Transfer<Hex27<Real>, Tet10<Real>>;

    // MIXED Dimensions
    template class L2Transfer<Tri1<Real>, Edge1<Real, 2>>;
    template class L2Transfer<Tri1<Real>, Edge2<Real, 2>>;
    template class L2Transfer<Tri1<Real>, Edge3<Real, 2>>;

    template class L2Transfer<Tri3<Real>, Edge1<Real, 2>>;
    template class L2Transfer<Tri3<Real>, Edge2<Real, 2>>;
    template class L2Transfer<Tri3<Real>, Edge3<Real, 2>>;

    template class L2Transfer<Tri6<Real>, Edge1<Real, 2>>;
    template class L2Transfer<Tri6<Real>, Edge2<Real, 2>>;
    template class L2Transfer<Tri6<Real>, Edge3<Real, 2>>;

    ////////////////////////////////////////////////////////////////////

    template class L2Transfer<Quad1<Real>, Edge1<Real, 2>>;
    template class L2Transfer<Quad1<Real>, Edge2<Real, 2>>;
    template class L2Transfer<Quad1<Real>, Edge3<Real, 2>>;

    template class L2Transfer<Quad4<Real>, Edge1<Real, 2>>;
    template class L2Transfer<Quad4<Real>, Edge2<Real, 2>>;
    template class L2Transfer<Quad4<Real>, Edge3<Real, 2>>;

    template class L2Transfer<Quad8<Real>, Edge1<Real, 2>>;
    template class L2Transfer<Quad8<Real>, Edge2<Real, 2>>;
    template class L2Transfer<Quad8<Real>, Edge3<Real, 2>>;

    ////////////////////////////////////////////////////////////////////
    template class L2Transfer<Tet1<Real>, Tri1<Real, 3>>;
    template class L2Transfer<Tet1<Real>, Tri3<Real, 3>>;
    template class L2Transfer<Tet1<Real>, Tri6<Real, 3>>;

    template class L2Transfer<Tet1<Real>, Quad1<Real, 3>>;
    template class L2Transfer<Tet1<Real>, Quad4<Real, 3>>;
    template class L2Transfer<Tet1<Real>, Quad8<Real, 3>>;

    template class L2Transfer<Tet4<Real>, Tri1<Real, 3>>;
    template class L2Transfer<Tet4<Real>, Tri3<Real, 3>>;
    template class L2Transfer<Tet4<Real>, Tri6<Real, 3>>;

    template class L2Transfer<Tet4<Real>, Quad1<Real, 3>>;
    template class L2Transfer<Tet4<Real>, Quad4<Real, 3>>;
    template class L2Transfer<Tet4<Real>, Quad8<Real, 3>>;

    template class L2Transfer<Tet10<Real>, Tri1<Real, 3>>;
    template class L2Transfer<Tet10<Real>, Tri3<Real, 3>>;
    template class L2Transfer<Tet10<Real>, Tri6<Real, 3>>;

    template class L2Transfer<Tet10<Real>, Quad1<Real, 3>>;
    template class L2Transfer<Tet10<Real>, Quad4<Real, 3>>;
    template class L2Transfer<Tet10<Real>, Quad8<Real, 3>>;

    ////////////////////////////////////////////////////////////////////

    template class L2Transfer<Hex1<Real>, Tri1<Real, 3>>;
    template class L2Transfer<Hex1<Real>, Tri3<Real, 3>>;
    template class L2Transfer<Hex1<Real>, Tri6<Real, 3>>;

    template class L2Transfer<Hex1<Real>, Quad1<Real, 3>>;
    template class L2Transfer<Hex1<Real>, Quad4<Real, 3>>;
    template class L2Transfer<Hex1<Real>, Quad8<Real, 3>>;

    template class L2Transfer<Hex8<Real>, Tri1<Real, 3>>;
    template class L2Transfer<Hex8<Real>, Tri3<Real, 3>>;
    template class L2Transfer<Hex8<Real>, Tri6<Real, 3>>;

    template class L2Transfer<Hex8<Real>, Quad1<Real, 3>>;
    template class L2Transfer<Hex8<Real>, Quad4<Real, 3>>;
    template class L2Transfer<Hex8<Real>, Quad8<Real, 3>>;

    template class L2Transfer<Hex27<Real>, Tri1<Real, 3>>;
    template class L2Transfer<Hex27<Real>, Tri3<Real, 3>>;
    template class L2Transfer<Hex27<Real>, Tri6<Real, 3>>;

    template class L2Transfer<Hex27<Real>, Quad1<Real, 3>>;
    template class L2Transfer<Hex27<Real>, Quad4<Real, 3>>;
    template class L2Transfer<Hex27<Real>, Quad8<Real, 3>>;

    ////////////////////////////////////////////////////////////////////

    /// Generic versions
    template class L2Transfer<Elem<Real, 1, 1>, Elem<Real, 1, 1>>;
    template class L2Transfer<Elem<Real, 2, 2>, Elem<Real, 2, 2>>;
    template class L2Transfer<Elem<Real, 3, 3>, Elem<Real, 3, 3>>;

    template class L2Transfer<Elem<Real, 2, 2>, Elem<Real, 1, 2>>;
    template class L2Transfer<Elem<Real, 3, 3>, Elem<Real, 2, 3>>;

    template class L2Transfer<Elem<Real, 1, 2>, Elem<Real, 1, 2>>;
    template class L2Transfer<Elem<Real, 2, 3>, Elem<Real, 2, 3>>;
}  // namespace moonolith
