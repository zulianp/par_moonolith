#include "moonolith_check_stream.hpp"
#include "moonolith_input_stream.hpp"
#include "moonolith_output_stream.hpp"
#include "moonolith_stream_utils.hpp"

#include <iostream>
#include <string>

namespace moonolith {

    inline static bool read_string_with_max_length(const Integer max_length, std::string &str, InputStream &is) {
        Integer n;
        is >> n;
        if (n > max_length) {
            return false;
        }

        str.resize(n);
        is.read(&str[0], n);
        return true;
    }

    inline static bool write_string(const std::string &str, OutputStream &os) {
        os << Integer(str.size());
        os.write(&str[0], static_cast<Integer>(str.size()));
        return true;
    }

    void CheckStream::read(const std::string &expected, InputStream &is, const std::string &file, const int line) {
        std::string actual;

        if (!is.good()) {
            error_logger() << "[Error] expected " << expected << " but stream is not good. At " << file << ":" << line
                           << std::endl;
            error_logger() << std::flush;
            assert(false);
            return;
        }

        if (!read_string_with_max_length(static_cast<Integer>(expected.size()), actual, is)) {
            error_logger() << "[Error] expected " << expected << " length differs. At " << file << ":" << line
                           << std::endl;
            error_logger() << std::flush;
            assert(false);
            return;
        }

        if (expected != actual) {
            error_logger() << "[Error] expected " << expected << " read " << actual << " instead. At " << file << ":"
                           << line << std::endl;
            error_logger() << std::flush;
            assert(false);
            return;
        }
    }

    void CheckStream::write(const std::string &str, OutputStream &os) { write_string(str, os); }
}  // namespace moonolith
