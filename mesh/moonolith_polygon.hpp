#ifndef MOONOLITH_POLYGON_HPP
#define MOONOLITH_POLYGON_HPP

#include "moonolith_aabb.hpp"
#include "moonolith_expanding_array.hpp"
#include "moonolith_shape.hpp"
#include "moonolith_vector.hpp"
#include "par_moonolith_config.hpp"

#include <memory>
#include <vector>

namespace moonolith {

    static const short INSIDE = 1;
    static const short ON_EDGE = 2;
    static const short OUTSIDE = 0;

    template <typename T>
    T trapezoid_area(const Vector<T, 2> &p1, const Vector<T, 2> &p2, const Vector<T, 2> &p3);

    template <typename T>
    T trapezoid_area(const Vector<T, 3> &p1, const Vector<T, 3> &p2, const Vector<T, 3> &p3);

    template <typename T>
    static Vector<T, 2> intersect_lines(const Vector<T, 2> line_1a,
                                        const Vector<T, 2> line_1b,
                                        const Vector<T, 2> line_2a,
                                        const Vector<T, 2> line_2b) {
        const auto u = line_1b - line_1a;
        const auto v = line_2b - line_2a;
        const auto w = line_2b - line_1a;

        const auto lambda = -(-w[0] * v[1] + w[1] * v[0]) / (u[0] * v[1] - u[1] * v[0]);
        return line_1a + (lambda * u);
    }

    template <typename T>
    static short inside_half_plane(const Vector<T, 2> e1,
                                   const Vector<T, 2> e2,
                                   const Vector<T, 2> point,
                                   const T tol) {
        const auto u = e1 - e2;
        const auto v = point - e2;

        const auto dist = (u.x * v.y) - (v.x * u.y);

        if (dist < -tol) {
            return INSIDE;
        }

        if (dist > tol) {
            return OUTSIDE;
        }

        return ON_EDGE;
    }

    template <typename T, int Dim>
    class Polygon final : public Shape<T, 2, Dim> {
    public:
        using Vector = moonolith::Vector<T, Dim>;
        using Point = moonolith::Vector<T, Dim>;

        Polygon(const bool convex = true) : convex(convex) {}

        inline Integer size() const { return static_cast<Integer>(points.size()); }

        inline void resize(const std::size_t n) { points.resize(n); }

        inline Point &operator[](const std::size_t &i) {
            assert(i < size());
            return points[i];
        }

        inline const Point &operator[](const std::size_t &i) const {
            assert(i < size());
            return points[i];
        }

        inline bool empty() const { return points.empty(); }

        void reverse();

        T area() const;
        Point barycenter() const;

        bool check_convexity();

        void clear() {
            points.clear();
            convex = true;
        }

        void remove_collinear_points(const T tol);

        bool intersect(const Ray<T, Dim> &, T &t, moonolith::Vector<T, 2> &) override {
            // TODO implement me
            t = 0.;
            return false;
        }

        bool intersect(const Ray<T, Dim> &, T &t) override {
            t = 0.;
            return false;
        }

        Storage<Point> points;
        bool convex;
    };

    template <typename T, int Dim>
    void make_quad(const Vector<T, Dim> &p0,
                   const Vector<T, Dim> &p1,
                   const Vector<T, Dim> &p2,
                   const Vector<T, Dim> &p3,
                   Polygon<T, Dim> &poly,
                   const bool convex = true) {
        poly.resize(4);
        poly[0] = p0;
        poly[1] = p1;
        poly[2] = p2;
        poly[3] = p3;
        poly.convex = convex;
    }

    template <typename T, int Dim>
    T measure(const Polygon<T, Dim> &p) {
        return p.area();
    }

    template <typename T, int Dim>
    bool approxeq(const Polygon<T, Dim> &l, const Polygon<T, Dim> &r, const T tol) {
        AABB<Dim, T> aabb_l, aabb_r;

        for (const auto &p : l.points) {
            aabb_l += p;
        }

        for (const auto &p : r.points) {
            aabb_r += p;
        }

        return moonolith::approxeq(aabb_l, aabb_r, tol);
    }

    template <typename T>
    bool is_ccw(const Polygon<T, 3> &polygon, const Vector<T, 3> &normal) {
        std::size_t n_vertices = polygon.size();

        Vector<T, 3> u, v, n;

        for (std::size_t i = 1; i < n_vertices - 1; ++i) {
            const auto ip1 = i + 1;

            u = polygon[i] - polygon[0];
            v = polygon[ip1] - polygon[0];
            n = cross(u, v);

            if (dot(n, normal) < 0.0) {
                return false;
            }
        }

        return true;
    }

    template <typename T>
    bool is_ccw(const Polygon<T, 2> &polygon) {
        std::size_t n_vertices = polygon.size();

        Vector<T, 3> u, v, n;

        for (std::size_t i = 1; i < n_vertices - 1; ++i) {
            const auto ip1 = i + 1;

            if (trapezoid_area(polygon[0], polygon[i], polygon[ip1]) < 0.0) {
                return false;
            }
        }

        return true;
    }

}  // namespace moonolith

#endif  // MOONOLITH_POLYGON_HPP
