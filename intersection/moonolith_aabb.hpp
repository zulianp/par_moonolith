#ifndef MOONOLITH_AABB_HPP
#define MOONOLITH_AABB_HPP

#include "moonolith_describable.hpp"
#include "moonolith_forward_declarations.hpp"
#include "moonolith_geo_algebra.hpp"
#include "moonolith_serializable.hpp"
#include "moonolith_vector.hpp"

#include <array>
#include <cassert>
#include <cmath>
#include <limits>
#include <vector>

namespace moonolith {

    template <int Dimension, typename T>
    class AABB : public Describable, public Serializable {
    public:
        void read(InputStream &is) override {
            for (int i = 0; i < Dimension; ++i) {
                is >> min_[i];
                is >> max_[i];
            }
        }

        void write(OutputStream &os) const override {
            for (int i = 0; i < Dimension; ++i) {
                os << min_[i];
                os << max_[i];
            }
        }

        bool intersects(const AABB &bound) const {
            for (int i = 0; i < Dimension; ++i) {
                if (min(i) > bound.max(i) || max(i) < bound.min(i)) {
                    return false;
                }
            }

            return true;
        }

        inline T min(const int coord) const { return min_[coord]; }

        inline T max(const int coord) const { return max_[coord]; }

        inline void set_min(const int coord, const T value) { min_[coord] = value; }

        inline void set_max(const int coord, const T value) { max_[coord] = value; }

        inline void get_min(Vector<T, Dimension> &m) const {
            for (int i = 0; i < Dimension; ++i) {
                m[i] = min(i);
            }
        }

        inline void get_max(Vector<T, Dimension> &m) const {
            for (int i = 0; i < Dimension; ++i) {
                m[i] = max(i);
            }
        }

        // expands to contain the union of this and bound
        virtual AABB &operator+=(const AABB &bound) {
            for (int i = 0; i < Dimension; ++i) {
                set_min(i, std::min(min(i), bound.min(i)));
                set_max(i, std::max(max(i), bound.max(i)));
            }

            return *this;
        }

        virtual AABB &operator+=(const std::array<T, Dimension> &point) {
            for (int i = 0; i < Dimension; ++i) {
                set_min(i, std::min(min(i), point[i]));
                set_max(i, std::max(max(i), point[i]));
            }

            return *this;
        }

        virtual AABB &operator+=(const Vector<T, Dimension> &point) {
            for (int i = 0; i < Dimension; ++i) {
                set_min(i, std::min(min(i), point[i]));
                set_max(i, std::max(max(i), point[i]));
            }

            return *this;
        }

        virtual AABB &operator+=(const std::vector<T> &point) {
            assert(point.size() == Dimension);
            for (int i = 0; i < Dimension; ++i) {
                set_min(i, std::min(min(i), point[i]));
                set_max(i, std::max(max(i), point[i]));
            }

            return *this;
        }

        void enlarge(const T value) {
            auto abs_value = std::abs(value);
            for (int i = 0; i < Dimension; ++i) {
                min_[i] -= abs_value;
                max_[i] += abs_value;
            }
        }

        virtual bool empty() const { return max(0) < min(0); }

        // puts this in the intial state
        virtual void clear() {
            std::fill(std::begin(min_), std::end(min_), std::numeric_limits<T>::max());
            std::fill(std::begin(max_), std::end(max_), -std::numeric_limits<T>::max());
        }

        AABB() { clear(); }

        void describe(std::ostream &os) const override {
            for (int i = 0; i < Dimension; ++i) {
                os << "[";
                os << min_[i] << ", ";
                os << max_[i] << "]\n";
            }
        }

        static inline int n_dims() { return Dimension; }

        std::array<T, Dimension> min_, max_;
    };

    template <int Dimension, typename T>
    bool approxeq(const AABB<Dimension, T> &l, const AABB<Dimension, T> &r, const T tol) {
        for (int i = 0; i < Dimension; ++i) {
            if (!moonolith::approxeq(l.min(i), r.min(i), tol) || !moonolith::approxeq(l.max(i), r.max(i), tol)) {
                return false;
            }
        }

        return true;
    }
}  // namespace moonolith

#endif  // MOONOLITH_AABB_HPP
