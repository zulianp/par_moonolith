#ifndef MOONOLITH_N_TREE_BOUND_SYNCHRONIZE_HPP
#define MOONOLITH_N_TREE_BOUND_SYNCHRONIZE_HPP

#include "moonolith_bound_synchronize.hpp"
#include "moonolith_node.hpp"

#include <ostream>

namespace moonolith {

    template <class Tree>
    class NTreeBoundSynchronize : public BoundSynchronize<typename Tree::Traits> {
    public:
        typedef typename Tree::Traits Traits;
        typedef typename Traits::Bound Bound;

        virtual bool has_static_serial_size() const override {
            return false;
            // return true;
        }

        virtual bool intersecting(const Integer rank) const override { return intersecting_[rank]; }

        virtual void set_intersecting(const Integer rank, const bool value) { intersecting_[rank] = value; }

        /////////////////////////////////////////////////////////////////////////////////////
        virtual bool read_merge_for(const Integer rank, InputStream &is) override {
            CHECK_STREAM_READ_BEGIN("NTreeBoundSynchronize::{write_local|read_merge_for}", is);
            remote_bound_[rank] << is;
            CHECK_STREAM_READ_END("NTreeBoundSynchronize::{write_local|read_merge_for}", is);

            global_bound_ += remote_bound_[rank];

            if (remote_bound_[rank].empty() || local_bound_.empty()) {
                intersecting_[rank] = false;
                return true;
            }

            intersecting_[rank] = local_bound_.intersects(remote_bound_[rank]);
            return true;
        }

        virtual bool write_local(OutputStream &os) const override {
            CHECK_STREAM_WRITE_BEGIN("NTreeBoundSynchronize::{write_local|read_merge_for}", os);
            os << local_bound_;
            CHECK_STREAM_WRITE_END("NTreeBoundSynchronize::{write_local|read_merge_for}", os);
            return true;
        }

        /////////////////////////////////////////////////////////////////////////////////////

        virtual const Bound &global_bound() const override { return global_bound_; }

        virtual const Bound &remote_bound(const Integer rank) const { return remote_bound_[rank]; }

        virtual void initialize(const Node<Traits> &root, const Integer n_procs) override {
            local_bound_ = root.bound();
            global_bound_ += local_bound_;
            intersecting_.resize(n_procs);
            remote_bound_.resize(n_procs);
            std::fill(intersecting_.begin(), intersecting_.end(), false);
        }

        virtual void clear() {
            local_bound_.clear();
            global_bound_.clear();
            intersecting_.clear();
        }

        virtual void describe(std::ostream &os) const override {
            os << "Intersections on ranks: ";

            const Integer n = static_cast<Integer>(intersecting_.size());
            for (Integer r = 0; r < n; ++r) {
                if (intersecting_[r]) {
                    os << r << " ";
                }
            }

            os << "\n";
        }

    private:
        Bound local_bound_;
        Bound global_bound_;
        std::vector<bool> intersecting_;
        std::vector<Bound> remote_bound_;
    };
}  // namespace moonolith

#endif  // MOONOLITH_N_TREE_BOUND_SYNCHRONIZE_HPP
