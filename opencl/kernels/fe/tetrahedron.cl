#ifndef TETRAHEDRON_CL 
#define TETRAHEDRON_CL 
#include "kernel_base.cl"
#include "fe_object.cl"
#include "fe_interfaces_3.cl"

bool ref_tetrahedron_contains_all_points(const SizeType n_quad_points, const Scalar * quad_points)
{
	const SizeType n_entries = n_quad_points*3;
	for(SizeType q = 0; q < n_entries; q += 3) {
		bool outside = ( quad_points[q] < -DEFAULT_TOLLERANCE || quad_points[q+1] < -DEFAULT_TOLLERANCE || quad_points[q+2] < -DEFAULT_TOLLERANCE ) ||
		!(quad_points[q] + quad_points[q+1] + quad_points[q+2] <= 1 + DEFAULT_TOLLERANCE);
		
		if(outside) {
			printf("[%g, %g, %g]\n", quad_points[q], quad_points[q+1], quad_points[q+2] );
			return false;
		}  					  
	}

	return true;
}

void tetrahedron_make_fe_object(const Scalar *points, const SizeType n_quad_points, const Scalar * quad_points, FEObject *object)
{
	KERNEL_ASSERT(ref_tetrahedron_contains_all_points(n_quad_points, quad_points), "tetrahedron_make_fe_object: quad_points are not inside the ref element");

	object->n_dims = 3;
	object->n_quad_points = n_quad_points;
	object->reference_volume = 1.0/6.0;
	object->n_shape_functions = 4;

	tetrahedron_transform(points, n_quad_points, quad_points, object->point);
	tetrahedron_jacobian(points, n_quad_points, quad_points, object->jacobian);

	static_assert(MAX_SHAPE_FUNCS>=4, "check kernel_base for MAX_SHAPE_FUNCS");
	tetrahedron_linear_fun_0(n_quad_points, quad_points, object->fun[0]);
	tetrahedron_linear_fun_1(n_quad_points, quad_points, object->fun[1]);
	tetrahedron_linear_fun_2(n_quad_points, quad_points, object->fun[2]);
	tetrahedron_linear_fun_3(n_quad_points, quad_points, object->fun[3]);

	tetrahedron_linear_grad_0(n_quad_points, quad_points, object->grad[0]);
	tetrahedron_linear_grad_1(n_quad_points, quad_points, object->grad[1]);
	tetrahedron_linear_grad_2(n_quad_points, quad_points, object->grad[2]);
	tetrahedron_linear_grad_3(n_quad_points, quad_points, object->grad[3]);
}

static void tetrahedron_transform(const Scalar * points, const SizeType n_quad_points, const Scalar * quad_points, Scalar * global_quad_points)
{
	const Vector3 p1 = vec_3(points[0], points[1], points[2]);
	const Vector3 p2 = vec_3(points[3], points[4], points[5]);
	const Vector3 p3 = vec_3(points[6], points[7], points[8]);
	const Vector3 p4 = vec_3(points[9], points[10], points[11]);

	const Vector3 u1 = p2 - p1;
	const Vector3 u2 = p3 - p1;
	const Vector3 u3 = p4 - p1;

	const SizeType n_quad_points_x_d = n_quad_points*3;
	
	for(SizeType q = 0; q < n_quad_points_x_d; q+=3) {	
		const SizeType qp1 = q+1;
		const SizeType qp2 = q+2;

		const Vector3 global_point = p1 + u1 * quad_points[q] + u2 * quad_points[qp1] + u3 * quad_points[qp2];

		global_quad_points[q] 	= global_point[0];
		global_quad_points[qp1] = global_point[1];
		global_quad_points[qp2] = global_point[2];
	}
}

void tetrahedron_inverse_transform(const Scalar * points, const SizeType n_quad_points, const Scalar * global_quad_points, Scalar * quad_points)
{
	Scalar affine_trafo_A[3*3];  //linear map
	Scalar inv_affine_trafo_A[3*3];  //linear map
	Scalar temp[3];


	// matrix_print(4, 3, points);

	
	tetrahedron_jacobian(points, 1, &global_quad_points[0], affine_trafo_A); //input does not matter
	const Scalar detJ = det_3(affine_trafo_A);
	inverse_3(affine_trafo_A, detJ, inv_affine_trafo_A);

	KERNEL_ASSERT(detJ > 0, "tetrahedron_inverse_transform: detJ > 0");

	for(SizeType i = 0; i < n_quad_points; ++i) {
		const SizeType offset_3 = i * 3;
		temp[0] = global_quad_points[offset_3] 	   - points[0];
		temp[1] = global_quad_points[offset_3 + 1] - points[1];
		temp[2] = global_quad_points[offset_3 + 2] - points[2];

		mat_vec_mul(3, 3, inv_affine_trafo_A, temp, &quad_points[offset_3]);

		// matrix_print(1, 3, &quad_points[offset_3]);

		// KERNEL_ASSERT(quad_points[offset_3]     >= -DEFAULT_TOLLERANCE, "tetrahedron_inverse_transform: quad_points[offset_3] > 0");
		// KERNEL_ASSERT(quad_points[offset_3 + 1] >= -DEFAULT_TOLLERANCE, "tetrahedron_inverse_transform: quad_points[offset_3 + 1] > 0");
		// KERNEL_ASSERT(quad_points[offset_3 + 2] >= -DEFAULT_TOLLERANCE, "tetrahedron_inverse_transform: quad_points[offset_3 + 2] > 0");
		// KERNEL_ASSERT(quad_points[offset_3] + quad_points[offset_3 + 1] + quad_points[offset_3 + 2] <= 1 + DEFAULT_TOLLERANCE, "tetrahedron_inverse_transform: quad_points[offset_3] + quad_points[offset_3 + 1] + quad_points[offset_3 + 2] <= 1");
	}
}

//layout of point data [x1, y1, x2, y2, z1, z2, ...]
//layout of the jacobian data [ f1_x1, f2_x1, f1_x2, f2_x2, ... ]
//@param n_quad_points  should be 1
void tetrahedron_jacobian(const Scalar * points, const SizeType n_quad_points, const Scalar * quad_points, Scalar * jacobians)
{
	(void) quad_points;

	const Vector3 p1 = vec_3(points[0], points[1], points[2]);
	const Vector3 p2 = vec_3(points[3], points[4], points[5]);
	const Vector3 p3 = vec_3(points[6], points[7], points[8]);
	const Vector3 p4 = vec_3(points[9], points[10], points[11]);

	const Vector3 u1 = p2 - p1;
	const Vector3 u2 = p3 - p1;
	const Vector3 u3 = p4 - p1;

	const SizeType n_quad_points_x_d = n_quad_points * 9;
	
	for(SizeType q = 0; q < n_quad_points_x_d; q += 9) {	
		//row 0
		jacobians[q] 	 = u1[0];
		jacobians[q + 1] = u2[0];
		jacobians[q + 2] = u3[0];
		//row 1
		jacobians[q + 3] = u1[1];
		jacobians[q + 4] = u2[1];
		jacobians[q + 5] = u3[1];
		//row 2
		jacobians[q + 6] = u1[2];
		jacobians[q + 7] = u2[2];
		jacobians[q + 8] = u3[2];
	}
}

void tetrahedron_linear_shape_fun(const SizeType number, const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	switch(number)
	{
		case 0: 
		{
			tetrahedron_linear_fun_0(n_quad_points, quad_points, funs);
			break;
		}

		case 1: 
		{
			tetrahedron_linear_fun_1(n_quad_points, quad_points, funs);
			break;
		}

		case 2: 
		{
			tetrahedron_linear_fun_2(n_quad_points, quad_points, funs);
			break;
		}

		case 3: 
		{
			tetrahedron_linear_fun_3(n_quad_points, quad_points, funs);
			break;
		}

		default:
		{
			//TODO report error
			break;
		}
	}
}

//Node 0 = (0, 0, 0)
void tetrahedron_linear_fun_0(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		const SizeType offset = q*3;
		funs[q] = 1.0 - quad_points[offset] - quad_points[offset+1] - quad_points[offset+2];
	}
}

//Node 1 = (1, 0, 0)
void tetrahedron_linear_fun_1(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		const SizeType offset = q*3;
		funs[q] = quad_points[offset];
	}
}

//Node 2 = (0, 1, 0)
void tetrahedron_linear_fun_2(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		const SizeType offset = q*3;
		funs[q] = quad_points[offset+1];
	}
}

//Node 3 = (0, 0, 1)
void tetrahedron_linear_fun_3(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		const SizeType offset = q*3;
		funs[q] = quad_points[offset+2];
	}
}

void tetrahedron_linear_grad_0(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads)
{
	const SizeType n_quad_points_x_d = n_quad_points*3;
	for(SizeType q = 0; q < n_quad_points_x_d; q+=3) {	
		grads[q]   = -1;
		grads[q+1] = -1;
		grads[q+2] = -1;

	}
}

void tetrahedron_linear_grad_1(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads)
{
	const SizeType n_quad_points_x_d = n_quad_points*3;
	for(SizeType q = 0; q < n_quad_points_x_d; q+=3) {	
		grads[q]   = 1;
		grads[q+1] = 0;
		grads[q+2] = 0;
	}
}

void tetrahedron_linear_grad_2(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads)
{
	const SizeType n_quad_points_x_d = n_quad_points*3;
	for(SizeType q = 0; q < n_quad_points_x_d; q+=3) {	
		grads[q]   = 0;
		grads[q+1] = 1;
		grads[q+2] = 0;
	}
}

void tetrahedron_linear_grad_3(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads)
{
	const SizeType n_quad_points_x_d = n_quad_points*3;
	for(SizeType q = 0; q < n_quad_points_x_d; q+=3) {	
		grads[q]   = 0;
		grads[q+1] = 0;
		grads[q+2] = 1;
	}
}

//local index contains three indices which are either 0, 1, 2, or 3
void trafo_from_tet_side_to_ref_triangle(
	const SizeType * local_index, 
	Scalar *A,
	Scalar *b)
{
	const Scalar sqrt_of_3_inv = 0.577350269189626;

	Scalar n  [3];
	Scalar tri[3 * 3];

	generic_set(3, 0, n);	
	generic_set(9, 0, tri);

	bool has_node_1 = false;
	bool has_node_2 = false;
	bool has_node_3 = false;

	for(SizeType i = 0; i < 3; ++i) {
		const SizeType i3 = i * 3;
		if(local_index[i] == 1) {
			has_node_1 = true;
			tri[i3    ] = 1;
			tri[i3 + 1] = 0;
			tri[i3 + 2] = 0; 
		} else
		if(local_index[i] == 2) {
			has_node_2 = true;
			tri[i3    ] = 0;
			tri[i3 + 1] = 1;
			tri[i3 + 2] = 0; 
		} else
		if(local_index[i] == 3) {
			has_node_3 = true;
			tri[i3    ] = 0;
			tri[i3 + 1] = 0;
			tri[i3 + 2] = 1;
		} 
	}

	if(has_node_1 && has_node_2 && has_node_3) { 
		//diagonal face
		generic_set(3, sqrt_of_3_inv, n); 
	} else if(!has_node_3) {
	// } else if(has_node_1 && has_node_2) {
		n[2] = -1;
	} else if(!has_node_2) {
		// } else if(has_node_1 && has_node_3) {
		n[1] = -1;
	} else if(!has_node_1) {
		// } else if(has_node_2 && has_node_3) {
		n[0] = -1;
	} else {
		KERNEL_ASSERT(has_node_1 || has_node_2 || has_node_3, "invalid local_index");
	}
	
	b[0] = tri[0];
	b[1] = tri[3]; 
	b[2] = tri[6];

	A[0]= tri[1] - b[0]; 
	A[1]= tri[2] - b[0]; 
	A[2]= n[0]; 

	A[3]= tri[4] - b[1]; 
	A[4]= tri[5] - b[1]; 
	A[5]= n[1]; 

	A[6]= tri[7] - b[2];
	A[7]= tri[8] - b[2];
	A[8]= n[2]; 
}


#endif //TETRAHEDRON_CL

