#ifndef MOONOLITH_ELEM_EDGE_1_HPP
#define MOONOLITH_ELEM_EDGE_1_HPP

#include "moonolith_elem_edge.hpp"
#include "moonolith_elem_edge2.hpp"

namespace moonolith {

    template <typename T_, int PhysicalDim_ = 1>
    class Edge1 final : public Edge<T_, 0, PhysicalDim_> {
    public:
        static const int Order = 1;
        static const int Dim = 1;
        static const int PhysicalDim = PhysicalDim_;
        static const int NNodes = 1;

        using T = T_;

        using Vector = moonolith::Vector<T, Dim>;
        using CoVector = moonolith::Vector<T, PhysicalDim>;

        using Point = moonolith::Vector<T, Dim>;
        using CoPoint = moonolith::Vector<T, PhysicalDim>;

    private:
        class Grad final {
        public:
            Grad() {
                f[0] = [](const Point &, Vector &g) { g.x = 0.0; };
            }

            std::array<std::function<void(const Point &, Vector &)>, NNodes> f;
        };

        class Fun final {
        public:
            Fun() {
                f[0] = [](const Point &) -> T { return 1.0; };
            }

            std::array<std::function<T(const Point &)>, NNodes> f;
        };

    public:
        T fun(const Integer i, const Point &p) const override { return fun_.f[i](p); }

        void hessian(const Integer, const Point &, std::array<T, Dim * Dim> &H) {
            std::fill(std::begin(H), std::end(H), 0.0);
        }

        void grad(const Integer i, const Point &p, Vector &g) const override { grad_.f[i](p, g); }

        void jacobian(const Point &p, std::array<T, PhysicalDim * Dim> &J) const override {
            (void)p;
            affine_approx_jacobian(J);
        }

        void affine_approx_jacobian(std::array<T, PhysicalDim * Dim> &J) const override {
            Affine<Edge1>::jacobian(points_, J);
        }

        CoPoint &node(const Integer i) override { return nodes_[i]; }

        const CoPoint &node(const Integer i) const override { return nodes_[i]; }

        void point(const Point &p, CoPoint &q) const override { Affine<Edge1>::point(points_, p, q); }

        Edge1() {}

        bool is_affine() const override { return true; }
        inline bool is_simplex() const override { return true; }
        inline int order() const override { return Order; }

        inline int n_nodes() const override { return NNodes; }
        const std::array<CoPoint, NNodes> &nodes() const { return nodes_; }

        inline const std::array<CoPoint, 2> &points() const { return points_; }

        inline CoPoint &point(const Integer idx) { return points_[idx]; }

        inline const CoPoint &point(const Integer idx) const { return points_[idx]; }

        const Fun &fun() const { return fun_; }

        void make_reference() {
            Reference<Edge1>::points(nodes_);
            Reference<Edge2<T, PhysicalDim>>::points(points_);
        }

        inline ElemType type() const override { return EDGE1; }

        inline T measure() const override { return distance(points_[0], points_[1]); }

        inline T reference_measure() const override { return 1.0; }

        inline void make(Line<T, PhysicalDim_> &line) const override {
            line.p0 = point(0);
            line.p1 = point(1);
        }

        inline void make(PolyLine<T, PhysicalDim_> &poly_line) const override {
            poly_line.points.resize(2);
            poly_line.points[0] = point(0);
            poly_line.points[1] = point(1);
        }

    private:
        std::array<CoPoint, NNodes> nodes_;
        std::array<CoPoint, 2> points_;
        const Fun fun_;
        const Grad grad_;
    };

    template <typename T, int PhysicalDim>
    class Reference<Edge1<T, PhysicalDim>> {
    public:
        static const int NNodes = 1;

        template <class Point, std::size_t Size>
        static void points(std::array<Point, Size> &nodes) {
            static_assert(Size >= NNodes, "size must be at least 1");
            // p0
            nodes[0].x = 0.5;
        }

        static T measure() { return 1.0; }
    };

    template <typename T, int PhysicalDim>
    class GaussQRule<Edge1<T, PhysicalDim>> {
    public:
        static bool get(const Integer order, Quadrature1<T> &q) { return Gauss::get(order, q); }
    };

}  // namespace moonolith

#endif  // MOONOLITH_ELEM_EDGE_1_HPP
