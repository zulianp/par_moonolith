#ifndef MOONOLITH_DETECTOR_HPP
#define MOONOLITH_DETECTOR_HPP 



#include "moonolith_data_handle.hpp"
#include "moonolith_lookup_table.hpp"
#include "moonolith_forward_declarations.hpp"

#include <list>
#include <memory>

namespace moonolith {

	template<class _Traits>
	class Detector {
	public:
		typedef _Traits Traits;
		typedef typename Traits::DataType DataType;

		///@brief see @a insert
		inline Detector &operator+=(const DataType &data) 
		{
			 this->insert(data);
			 return *this;
		}

		///@brief insert an element in the detector
		virtual DataHandle insert(const DataType &data) = 0;

		///@brief finds all matching elements
		// virtual Detector & match(const DataType &data, const Predicate &predicate, std::list<DataType> &matches) = 0;

		///@brief to be called when beginning to insert, modify or change inserted data or the structure contained by the detector
		virtual void editing_begin() = 0;

		///@brief to be called when editing is finished
		virtual void editing_end() = 0;

		///@brief find in the other processes where there is some matching data data
		virtual bool build_lookup_table() = 0;

		inline const std::shared_ptr<LookUpTable> &lookup_table() const
		{
			return lookup_table_;
		}

		Detector() {}
		virtual ~Detector() {}

	private:
		std::shared_ptr<LookUpTable> lookup_table_;
	};
	
}

#endif //MOONOLITH_DETECTOR_HPP
