#ifndef MOONOLITH_NTREE_WITH_TAGS_BOUND_EXPAND_HPP
#define MOONOLITH_NTREE_WITH_TAGS_BOUND_EXPAND_HPP

#include "moonolith_n_tree_bound_expand.hpp"

namespace moonolith {

	template<class Tree>
	class NTreeWithTagsBoundExpand : public NTreeBoundExpand<Tree> {
	public:
		typedef typename Tree::Traits Traits;

		typedef typename Traits::Bound Bound;
		typedef typename Traits::DataType DataType;

		typedef moonolith::Node<Traits> Node;

		void mutate(Node &node) const override
		{
			if(_tag != -1 && !node.has_tag(_tag)) {
				node.tags().push_back(_tag);
			}
		}
		
		NTreeWithTagsBoundExpand(const DataType &obj)
		: NTreeBoundExpand<Tree> (obj.bound()), _tag(obj.tag())
		{}

		NTreeWithTagsBoundExpand(const Bound &bound)
		: NTreeBoundExpand<Tree>(bound), _tag(-1)
		{}
		
	private:
		Integer _tag;
	};
}

#endif //MOONOLITH_NTREE_WITH_TAGS_BOUND_EXPAND_HPP
