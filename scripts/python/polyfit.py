#!/usr/bin/env python3

import numpy as np
import sympy as sp
from sympy.utilities.codegen import codegen
import sympy.codegen.ast as ast
import textwrap

def c_code(expr):
	sub_expr, simpl_expr = sp.cse(expr)
	printer = sp.printing.c.C99CodePrinter()
	lines = []

	for var,expr in sub_expr:
	    lines.append(f'T {var} = {printer.doprint(expr)};')

	for v in simpl_expr:
	        lines.append(printer.doprint(v))

	code_string=f'\n'.join(lines)
	return code_string;

def optimizer_code(name, f, g, d):
	fun = 0

	n, _ = f.shape

	for i in range(0, n):
		# print(f[i])
		fun += f[i] * g[i]

	fun -= d

	grad = []

	for i in range(0, n):
		gfun = sp.simplify(sp.diff(fun*fun/2, g[i]))
		grad.append(gfun)

	grad = sp.Matrix(n, 1, grad)

	Hessian = []

	for i in range(0, n):
		for j in range(0, n):
			Hg = sp.simplify(sp.diff(grad[i], g[j]))
			Hessian.append(Hg)

	Hessian = sp.Matrix(n, n, Hessian)


	expr = []

	# Add grad code
	for i in range(0, n):
		vec = sp.symbols(f'grad[{i}]')
		expr.append(ast.AddAugmentedAssignment(vec, grad[i]))

	# Add hessian code
	for i in range(0, n):
		for j in range(0, n):
			mat = sp.symbols(f'Hessian[{i*n+j}]')
			expr.append(ast.AddAugmentedAssignment(mat, Hessian[i,j]))

	evaluation = c_code(expr)
	template = textwrap.dedent("""
		// template<typename T>
		void {name}(const T x, const T y, const T d, const T *g, T *grad, T *Hessian) 
		{{
		{evaluation}
		}}
	""").strip()

	# print(template)

	code = template.format(name=name, evaluation=evaluation)
	print(code)


d = sp.symbols('d')
g0, g1, g2, g3 = sp.symbols('g[0] g[1] g[2] g[3]')
x, y = sp.symbols('x y')

g = sp.Matrix(4, 1, [g0, g1, g2, g3])
linear = sp.Matrix(3, 1, [1 - x - y, x, y]) # linear
bilienar = sp.Matrix(4, 1, [(1 - x) * (1 - y), x * (1 - y), x*y, x * (1 - y)]) # bilinear


	# print("Fun")
	# print(fun)

	# print("Grad")
	# for i in range(0, n):
	# 	print(grad[i])

	# print("Hessian")
	# for i in range(0, n):
	# 	print(f'row {i})')
	# 	for j in range(0, n):
	# 		print(Hessian[i,j])

print("//Linear")
optimizer_code("diff_linear_fit", linear, g, d)
print("\n//Bilinear")
optimizer_code("diff_bilinear_fit", bilienar, g, d)
