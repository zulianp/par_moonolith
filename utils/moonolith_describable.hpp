#ifndef MOONOLITH_DESCRIBABLE_HPP
#define MOONOLITH_DESCRIBABLE_HPP

#include <iostream>

namespace moonolith {
	class Describable {
	public:
		virtual ~Describable() {}
		virtual void describe(std::ostream &os) const = 0;
		inline friend std::ostream &operator<<(std::ostream &os, const Describable &d)
		{
			d.describe(os);
			return os;
		}
	};

	inline void describe(const std::string &str, std::ostream &os)
	{
		os << str.c_str();
	}

	inline void describe(const long value, std::ostream &os)
	{
		os << value;
	}

	inline void describe(const int value, std::ostream &os)
	{
		os << value;
	}


	inline void describe(const double value, std::ostream &os)
	{
		os << value;
	}


	inline void describe(const float value, std::ostream &os)
	{
		os << value;
	}

	template<typename T1, typename T2>
	inline void describe(const std::pair<T1, T2> &pair, std::ostream &os)
	{
		os << "(";
		describe(pair.first, os);
		os << ", ";
		describe(pair.second, os);
		os << ")";
	}


	inline void describe(const Describable &d, std::ostream &os)
	{
		d.describe(os);
	}

	template<class DescribableIter>
	void describe(const DescribableIter &begin, const DescribableIter &end, std::ostream &os, const std::string &separator = " ")
	{
		for(DescribableIter it = begin; it != end; ++it) {
			Describe(*it, os);
			os << separator;
		}
		os << "\n";
	}
}

#endif //MOONOLITH_DESCRIBABLE_HPP
