// #ifndef MOONOLITH_ELEM_SEGMENT_HPP
// #define MOONOLITH_ELEM_SEGMENT_HPP

// #include "moonolith_elem.hpp"
// #include "moonolith_gauss_quadrature_rule.hpp"

// namespace moonolith {

//     template<typename T, int Order_, int PhysicalDim_ = 1>
//     class Segment : public Elem<T, 1, PhysicalDim_> {};

//     template<typename T_, int PhysicalDim_>
//     class Segment<T_, 0, PhysicalDim_> final : public Elem<T_, 1, PhysicalDim_> {
//     public:
//         static const int Order = 1;
//         static const int Dim   = 1;
//         static const int PhysicalDim = PhysicalDim_;
//         static const int NNodes = 1;

//         using T = T_;

//         using Vector   = moonolith::Vector<T, Dim>;
//         using CoVector = moonolith::Vector<T, PhysicalDim>;

//         using Point   = moonolith::Vector<T, Dim>;
//         using CoPoint = moonolith::Vector<T, PhysicalDim>;

//     private:

//         class Grad final {
//         public:
//             Grad()
//             {
//                 f[0] = [](const Point &, Vector &g) {
//                     g.x = 0.0;
//                 };
//             }

//             std::array<std::function<void (const Point &, Vector &)>, NNodes> f;
//         };

//         class Fun final {
//         public:

//             Fun()
//             {
//                 f[0] = [](const Point &) -> T {
//                     return 1.0;
//                 };
//             }

//             std::array<std::function<T (const Point &)>, NNodes> f;
//         };

//     public:

//         T fun(const Integer i, const Point &p) const override
//         {
//             return fun_.f[i](p);
//         }

//         void hessian(const Integer i, const Point &p, std::array<T, Dim*Dim> &H)
//         {
//             std::fill(std::begin(H), std::end(H), 0.0);
//         }

//         void grad(const Integer i, const Point &p, Vector &g) const override
//         {
//             grad_.f[i](p, g);
//         }

//         void jacobian(const Point &p, std::array<T, PhysicalDim*Dim> &J) const override
//         {
//             (void)p;
//             affine_approx_jacobian(J);
//         }

//         void affine_approx_jacobian(std::array<T, PhysicalDim*Dim> &J) const override
//         {
//             Affine<Segment>::jacobian(points_, J);
//         }

//         CoPoint &node(const Integer i) override
//         {
//             return nodes_[i];
//         }

//         const CoPoint &node(const Integer i) const override
//         {
//             return nodes_[i];
//         }

//         void point(const Point &p, CoPoint &q) const override
//         {
//             Affine<Segment>::point(points_, p, q);
//         }

//         Segment() {}

//         bool is_affine() const override { return true; }
//         inline bool is_simplex() const override { return true; }
//         inline int order() const override { return Order; }

//         inline int n_nodes() const override { return NNodes; }
//         const std::array<CoPoint, NNodes> &nodes() const { return nodes_; }
        
//         inline const std::array<CoPoint, 2> &points() const
//         {
//             return points_;
//         }

//         inline CoPoint &point(const Integer idx)
//         {
//             return points_[idx];
//         }

//         inline const CoPoint &point(const Integer idx) const
//         {
//             return points_[idx];
//         }

//         const Fun &fun() const { return fun_; }

//         void make_reference()
//         {
//             Reference<Segment>::points(nodes_);
//             Reference<Segment<T, 1, PhysicalDim>>::points(points_);
//         }

//         inline ElemType type() const override
//         {
//             return EDGE1;
//         }

//         inline T measure() const override
//         {
//             return distance(points_[0], points_[1]);
//         }

//         inline T reference_measure() const override { return 1.0; }

//     private:
//         std::array<CoPoint, NNodes> nodes_;
//         std::array<CoPoint, 2> points_;
//         const Fun  fun_;
//         const Grad grad_;
//     };


//     template<typename T, int PhysicalDim>
//     class Reference<Segment<T, 0, PhysicalDim>> {
//     public:
//         static const int NNodes = 1;
        
//         template<class Point, std::size_t Size>
//         static void points(std::array<Point, Size> &nodes)
//         {
//             static_assert(Size >= NNodes, "size must be at least 1");
//             //p0
//             nodes[0].x = 0.5;
//         }

//         static T measure() { return 1.0; }
//     };

//     template<typename T, int PhysicalDim>
//     inline void make(const Segment<T, 0, PhysicalDim> &elem, Line<T, PhysicalDim> &poly) {
//         poly.p0 = elem.points()[0];
//         poly.p1 = elem.points()[1];
//     }

//     template<typename T, int PhysicalDim>
//     inline void make(const Segment<T, 0, PhysicalDim> &elem, PolyLine<T, PhysicalDim> &poly_line) {
//         poly_line.points.resize(2);
//         poly_line.points[0] = elem.point(0);
//         poly_line.points[1] = elem.point(2);
//     }

//     //////////////////////////////////////////////////////////////////////////////////////////

//     template<typename T_, int PhysicalDim_>
//     class Segment<T_, 1, PhysicalDim_> final : public Elem<T_, 1, PhysicalDim_> {
//     public:
//         static const int Order = 1;
//         static const int Dim   = 1;
//         static const int PhysicalDim = PhysicalDim_;
//         static const int NNodes = 2;

//         using T = T_;

//         using Vector   = moonolith::Vector<T, Dim>;
//         using CoVector = moonolith::Vector<T, PhysicalDim>;

//         using Point   = moonolith::Vector<T, Dim>;
//         using CoPoint = moonolith::Vector<T, PhysicalDim>;

//     private:

//         class Grad final {
//         public:
//             Grad()
//             {
//                 f[0] = [](const Point &p, Vector &g) {
//                     g.x = -1.0;
//                 };

//                 f[1] = [](const Point &p, Vector &g) {
//                     g.x = 1;
//                 };
//             }

//             std::array<std::function<void (const Point &, Vector &)>, 2> f;
//         };

//         class Fun final {
//         public:

//             Fun()
//             {
//                 f[0] = [](const Point &p) -> T {
//                     return 1.0  - p.x;
//                 };

//                 f[1] = [](const Point &p) -> T {
//                     return p.x;
//                 };
//             }

//             std::array<std::function<T (const Point &)>, 2> f;
//         };

//     public:

//         T fun(const Integer i, const Point &p) const override
//         {
//             return fun_.f[i](p);
//         }

//         void hessian(const Integer i, const Point &p, std::array<T, Dim*Dim> &H)
//         {
//             std::fill(std::begin(H), std::end(H), 0.0);
//         }

//         void grad(const Integer i, const Point &p, Vector &g) const override
//         {
//             grad_.f[i](p, g);
//         }

//         void jacobian(const Point &p, std::array<T, PhysicalDim*Dim> &J) const override
//         {
//             (void)p;
//             affine_approx_jacobian(J);
//         }

//         void affine_approx_jacobian(std::array<T, PhysicalDim*Dim> &J) const override
//         {
//             Affine<Segment>::jacobian(*this, J);
//         }

//         CoPoint &node(const Integer i) override
//         {
//             return nodes_[i];
//         }

//         const CoPoint &node(const Integer i) const override
//         {
//             return nodes_[i];
//         }

//         void point(const Point &p, CoPoint &q) const override
//         {
//             IsoParametric<Segment>::point(*this, p, q);
//         }

//         Segment() {}

//         bool is_affine() const override { return true; }
//         inline bool is_simplex() const override { return true; }

//         inline int n_nodes() const override { return NNodes; }
//         const std::array<CoPoint, 2> &nodes() const { return nodes_; }
//         inline int order() const override { return Order; }

//         const Fun &fun() const { return fun_; }

//         void make_reference()
//         {
//             Reference<Segment>::points(nodes_);
//         }

//         inline ElemType type() const override
//         {
//             return EDGE2;
//         }

//         inline T measure() const override
//         {
//             return distance(nodes_[0], nodes_[1]);
//         }

//         inline T reference_measure() const override { return 1.0; }

//     private:
//         std::array<CoPoint, 2> nodes_;
//         const Fun  fun_;
//         const Grad grad_;
//     };


//     template<typename T, int PhysicalDim>
//     class Reference<Segment<T, 1, PhysicalDim>> {
//     public:
//         static const int NNodes = 2;
        
//         template<class Point, std::size_t Size>
//         static void points(std::array<Point, Size> &nodes)
//         {
//             static_assert(Size >= NNodes, "size must be at least 2");
//             //p0
//             nodes[0].x = 0.0;

//             //p1
//             nodes[1].x = 1.0;
//         }

//         static T measure() { return 1.0; }
//     };

//     template<typename T, int PhysicalDim>
//     inline void make(const Segment<T, 1, PhysicalDim> &elem, PolyLine<T, PhysicalDim> &poly_line) {
//         poly_line.points.resize(2);
//         poly_line.points[0] = elem.node(0);
//         poly_line.points[1] = elem.node(1);
//     }

//     //////////////////////////////////////////////////////////////////////////////////////////

//     template<typename T_, int PhysicalDim_>
//     class Segment<T_, 2, PhysicalDim_> final : public Elem<T_, 1, PhysicalDim_> {
//     public:
//         static const int Order = 2;
//         static const int Dim   = 1;
//         static const int PhysicalDim = PhysicalDim_;
//         static const int NNodes = 3;

//         using T = T_;

//         using Vector   = moonolith::Vector<T, Dim>;
//         using CoVector = moonolith::Vector<T, PhysicalDim>;

//         using Point   = moonolith::Vector<T, Dim>;
//         using CoPoint = moonolith::Vector<T, PhysicalDim>;

//     private:


//         class Fun final {
//         public:

//             Fun()
//             {
//                 f[0] = [](const Point &p) -> T {
//                     return 1.0 - 3.0 * p.x + 2.0 * (p.x * p.x);
//                 };

//                 f[1] = [](const Point &p) -> T {
//                     return -p.x + 2.0 * (p.x * p.x);
//                 };

//                 f[2] = [](const Point &p) -> T {
//                     return 4.0 * p.x - 4.0 * (p.x * p.x);
//                 };
//             }

//             std::array<std::function<T (const Point &)>, 3> f;
//         };

//         class Grad final {
//         public:
//             Grad()
//             {
//                 f[0] = [](const Point &p, Vector &g) {
//                     g.x = -3.0 + 4.0 * p.x;
//                 };

//                 f[1] = [](const Point &p, Vector &g) {
//                     g.x = -1.0 + 4.0 * p.x;
//                 };

//                 f[2] = [](const Point &p, Vector &g) {
//                     g.x = 4.0 - 8.0 * p.x;
//                 };
//             }

//             std::array<std::function<void (const Point &, Vector &)>, 3> f;
//         };

//         class Hessian {
//         public:

//             Hessian()
//             {   
//                 //g.x = -3.0 + 4.0 * p.x;
//                 f[0] = [](const Point &, std::array<T, Dim*Dim> &H) {
//                     H[0] = 4.0;
//                 };

//                 //g.x = -1.0 + 4.0 * p.x;
//                 f[1] = [](const Point &, std::array<T, Dim*Dim> &H) {
//                     H[0] = 4.0;
//                 };

//                 //g.x = 4.0 - 8.0 * p.x;
//                 f[2] = [](const Point &, std::array<T, Dim*Dim> &H) {
//                     H[0] = -8.0;
//                 };
//             }   

//             // H = [ f_xx, f_xy; f_yx, f_yy ];
//             std::array<std::function<void (const Point &, std::array<T, Dim*Dim> &)>, 3> f;
//         };

//     public:

//         T fun(const Integer i, const Point &p) const override
//         {
//             return fun_.f[i](p);
//         }

//         void grad(const Integer i, const Point &p, Vector &g) const override
//         {
//             grad_.f[i](p, g);
//         }

//         void hessian(const Integer i, const Point &p, std::array<T, Dim*Dim> &H)
//         {
//             hessian_.f[i](p, H);
//         }

//         void jacobian(const Point &p, std::array<T, PhysicalDim*Dim> &J) const override
//         {
//             IsoParametric<Segment>::jacobian(*this, p, J);
//         }

//         void affine_approx_jacobian(std::array<T, PhysicalDim*Dim> &J) const override
//         {
//             Affine<Segment>::jacobian(*this, J);
//         }

//         CoPoint &node(const Integer i) override
//         {
//             return nodes_[i];
//         }

//         const CoPoint &node(const Integer i) const override
//         {
//             return nodes_[i];
//         }

//         void point(const Point &p, CoPoint &q) const override
//         {
//             IsoParametric<Segment>::point(*this, p, q);
//         }

//         Segment() : is_affine_(false) {}

//         bool is_affine() const override { return is_affine_; }
//         inline bool is_simplex() const override { return true; }
//         inline int order() const override { return Order; }

//         inline void set_affine(const bool val)
//         {
//             is_affine_ = val;
//         }

//         inline int n_nodes() const override { return NNodes; }
//         const std::array<CoPoint, NNodes> &nodes() const { return nodes_; }

//         void make_reference()
//         {
//             Reference<Segment>::points(nodes_);
//             set_affine(true);
//         }

//         inline ElemType type() const override
//         {
//             return EDGE3;
//         }

//         inline T measure() const override
//         {
//             if(is_affine()) {
//                 return distance(nodes_[0], nodes_[1]);
//             } else {
//                 Quadrature<T, Dim> q;
//                 Gauss::get(1, q);
//                 return IsoParametric<Segment>::measure(*this, q);
//             }
//         }

//         inline T approx_measure() const override { 
//             if(is_affine()) {
//                 return distance(nodes_[0], nodes_[1]);
//             } else {
//                 return distance(nodes_[0], nodes_[2]) + distance(nodes_[1], nodes_[2]);
//             }
//         }

//         inline T reference_measure() const override { return 1.0; }

//     private:
//         bool is_affine_;
//         std::array<CoPoint, NNodes> nodes_;
//         const Fun  fun_;
//         const Grad grad_;
//         const Hessian hessian_;
//     };

//     template<typename T, int PhysicalDim>
//     class Reference<Segment<T, 2, PhysicalDim>> {
//     public:
//         static const int NNodes = 3;
        
//         template<class Point, std::size_t Size>
//         static void points(std::array<Point, Size> &nodes)
//         {
//             static_assert(Size >= NNodes, "size must be at least 3");
//             //p0
//             nodes[0].x = 0.0;

//             //p1
//             nodes[1].x = 1.0;

//             //p2
//             nodes[2].x = 0.5;
//         }

//         static T measure() { return 1.0; }
//     };

//     template<typename T, int PhysicalDim>
//     inline void make(const Segment<T, 2, PhysicalDim> &elem, PolyLine<T, PhysicalDim> &poly_line) {
//         poly_line.points.resize(3);
//         poly_line.points[0] = elem.node(0);
//         poly_line.points[1] = elem.node(2);
//         poly_line.points[2] = elem.node(1);
//     }

//     template<typename T, int PhysicalDim>
//     inline void make(const Elem<T, 1, PhysicalDim> &elem, PolyLine<T, PhysicalDim> &poly_line) {
//         auto p0 = dynamic_cast<const Segment<T, 0, PhysicalDim> *>(&elem);
//         if(p0) {
//             make(*p0, poly_line);
//             return;
//         }

//         auto p1 = dynamic_cast<const Segment<T, 1, PhysicalDim> *>(&elem);
//         if(p1) {
//             make(*p1, poly_line);
//             return;
//         }

//         auto p2 = dynamic_cast<const Segment<T, 2, PhysicalDim> *>(&elem);
//         if(p2) {
//             make(*p2, poly_line);
//             return;
//         }

//         assert(false);
//     }   

//     template<typename T, int Order, int PhysicalDim>
//     class GaussQRule< Segment<T, Order, PhysicalDim> > {
//     public:
//         static bool get(const Integer order, Quadrature1<T> &q) {
//             return Gauss::get(order, q);
//         }
//     };
// }

// #endif //MOONOLITH_ELEM_SEGMENT_HPP
