#include "moonolith_iso_parametric_transform.hpp"

#include "moonolith_elem_tri3.hpp"

namespace moonolith {
    template class IsoParametricTransform<Tri3<Real, 2>>;
    template class IsoParametricTransform<Tri3<Real, 3>>;
}  // namespace moonolith
