#ifndef MOONOLITH_LOWER_DIM_L2_ASSEMBLER_HPP
#define MOONOLITH_LOWER_DIM_L2_ASSEMBLER_HPP

#include "moonolith_assembly.hpp"
#include "moonolith_build_quadrature.hpp"
#include "moonolith_build_quadrature_non_convex.hpp"
#include "moonolith_elem_dual.hpp"
#include "moonolith_elem_shape.hpp"
#include "moonolith_fe.hpp"
#include "moonolith_iso_parametric_transform.hpp"
#include "moonolith_map_quadrature.hpp"
#include "moonolith_polygon.hpp"
#include "moonolith_project_convex_polygons.hpp"
#include "moonolith_project_line_segments.hpp"
#include "moonolith_project_polygons.hpp"
#include "moonolith_project_polylines.hpp"
#include "moonolith_shape.hpp"
#include "moonolith_transform.hpp"

#include "moonolith_elem_hexahedron.hpp"
#include "moonolith_elem_tetrahedron.hpp"
#include "moonolith_l2_assembler.hpp"
#include "moonolith_transformed_fe.hpp"

#include <memory>
#include <string>

namespace moonolith {

    template <class MasterElem, class SlaveElem = MasterElem>
    class BuildLowerDimQuadrature {};  // implement specialization

    template <typename T, int Dim>
    class BuildLowerDimQuadrature<Line<T, Dim>> {
    public:
        using Quadrature0 = moonolith::Quadrature<T, 0>;
        using Quadrature = moonolith::Quadrature<T, Dim>;
        using Line = moonolith::Line<T, Dim>;
        using Point = moonolith::Vector<T, Dim>;

        bool apply(
            // q_ref not needed
            const Quadrature0 &,
            const Line &line1,
            const Line &line2,
            Quadrature &out) {
            if (!intersect_lines.apply(line1, line2, isect)) {
                return false;
            }

            // equi-dimensional not supported here
            if (isect.size() > 1) return false;

            out.resize(1);
            out.weights[0] = 1.0;
            out.point(0) = isect[0];
            return true;
        }

    private:
        Storage<Point> isect;
        IntersectLines<T, Dim> intersect_lines;
    };

    template <typename T, int Dim>
    class BuildLowerDimQuadrature<Polygon<T, Dim>> {
    public:
        using Quadrature1 = moonolith::Quadrature<T, 1>;
        using Quadrature = moonolith::Quadrature<T, Dim>;
        using Polygon = moonolith::Polygon<T, Dim>;
        using Point = moonolith::Vector<T, Dim>;

        bool apply(const Quadrature1 &q_ref, const Polygon &poly1, const Polygon &poly2, Quadrature &out) {
            // equi-dimensional not supported here
            if (!intersect_polygons.apply(poly1, poly2, isect)) {
                return false;
            }

            map(q_ref, static_cast<T>(1.0), isect, out);

            return true;
        }

    private:
        Line<T, Dim> isect;
        IntersectConvexPolygons<T, Dim> intersect_polygons;
    };

    template <typename T, int PhysicalDim, int MasterDim = PhysicalDim, int SlaveDim = PhysicalDim>
    class BuildLowerDimQuadratureAlgo {};

    // @brief template method for building quadrature on physical element and mapping it to the reference element
    template <class MasterGeom,
              class SlaveGeom,
              class QReferenceMaster,
              class QReferenceSlave,
              class QPhysical,
              class QDomainOfIntegration,
              class MasterTransform_,
              class SlaveTransform_ = MasterTransform_>
    class BuildLowerDimQuadratureBase {
    public:
        using MasterTransform = MasterTransform_;
        using SlaveTransform = SlaveTransform_;

        // INPUT
        MasterGeom master;
        SlaveGeom slave;
        QDomainOfIntegration q_rule;
        std::shared_ptr<MasterTransform> trafo_master;
        std::shared_ptr<SlaveTransform> trafo_slave;

        // OUTPUT
        QPhysical q_physical;
        QReferenceMaster q_master;
        QReferenceSlave q_slave;

        virtual ~BuildLowerDimQuadratureBase() {}
        bool compute();

    private:
        BuildLowerDimQuadrature<MasterGeom, SlaveGeom> build_q;
    };

    // line domain of integration
    template <typename T, int PhysicalDim>
    class BuildLowerDimQuadratureAlgo<T, PhysicalDim, 1, 1> final
        : public BuildLowerDimQuadratureBase<Line<T, PhysicalDim>,
                                             Line<T, PhysicalDim>,
                                             Quadrature<T, 1>,
                                             Quadrature<T, 1>,
                                             Quadrature<T, PhysicalDim>,
                                             Quadrature<T, 0>,
                                             Transform<T, 1, PhysicalDim>> {};

    // polygonal domain of integration
    template <typename T, int PhysicalDim>
    class BuildLowerDimQuadratureAlgo<T, PhysicalDim, 2, 2> final
        : public BuildLowerDimQuadratureBase<Polygon<T, PhysicalDim>,
                                             Polygon<T, PhysicalDim>,
                                             Quadrature<T, 2>,
                                             Quadrature<T, 2>,
                                             Quadrature<T, PhysicalDim>,
                                             Quadrature<T, 1>,
                                             Transform<T, 2, PhysicalDim>> {};

    template <class MasterElem, class SlaveElem>
    class LowerDimL2Transfer : public Transfer<typename SlaveElem::T, SlaveElem::PhysicalDim>, public Describable {
    public:
        using T = typename SlaveElem::T;

        static const int MasterDim = MasterElem::Dim;
        static const int SlaveDim = SlaveElem::Dim;
        static const int PhysicalDim = SlaveElem::PhysicalDim;
        static const int NMasterNodes = MasterElem::NNodes;
        static const int NSlaveNodes = SlaveElem::NNodes;

        using CoVector = moonolith::Vector<T, PhysicalDim>;
        using Algo = moonolith::BuildLowerDimQuadratureAlgo<T, PhysicalDim, MasterDim, SlaveDim>;
        using MasterAffineTrafo = moonolith::AffineTransform<T, MasterDim, PhysicalDim>;
        using SlaveAffineTrafo = moonolith::AffineTransform<T, SlaveDim, PhysicalDim>;

        void set_quadrature(const Quadrature<T, SlaveDim - 1> &q_rule) { algo_.q_rule = q_rule; }

        void set_quadrature(Quadrature<T, SlaveDim - 1> &&q_rule) { algo_.q_rule = std::move(q_rule); }

        bool assemble(MasterElem &master, SlaveElem &slave);

        bool assemble_affine(MasterElem &master, SlaveElem &slave);

        // bool assemble_warped(
        //     MasterElem &master,
        //     SlaveElem  &slave
        // );

        void assemble(MasterElem &master,
                      SlaveElem &slave,
                      const Quadrature<T, MasterDim> &q_master,
                      const Quadrature<T, SlaveDim> &q_slave);

        LowerDimL2Transfer();

        inline void clear() { intersection_measure_ = 0.0; }

        inline const Matrix<T, NSlaveNodes, NMasterNodes> &coupling_matrix() const { return coupling_mat_; }

        inline const Matrix<T, NSlaveNodes, NSlaveNodes> &mass_matrix() const { return mass_mat_; }

        // inline const Matrix<T, NSlaveNodes, NSlaveNodes> &transformation() const
        // {
        //     return trafo_;
        // }

        inline T intersection_measure() const { return intersection_measure_; }

        void describe(std::ostream &os) const override;

    private:
        Algo algo_;
        T intersection_measure_;

        FE<MasterElem> master_fe_;
        FE<SlaveElem> slave_fe_;
        // Dual<SlaveElem> dual_fe_;
        // TransformedFE<SlaveElem> trafo_fe_;

        Matrix<T, NSlaveNodes, NMasterNodes> coupling_mat_;
        Matrix<T, NSlaveNodes, NSlaveNodes> mass_mat_;
        // Matrix<T, NSlaveNodes, NSlaveNodes>  trafo_;

        std::shared_ptr<MasterAffineTrafo> trafo_master;
        std::shared_ptr<SlaveAffineTrafo> trafo_slave;

        std::shared_ptr<Transform<T, MasterDim, PhysicalDim>> iso_trafo_master;
        std::shared_ptr<Transform<T, SlaveDim, PhysicalDim>> iso_trafo_slave;
    };

}  // namespace moonolith

#endif  // MOONOLITH_LOWER_DIM_L2_ASSEMBLER_HPP
