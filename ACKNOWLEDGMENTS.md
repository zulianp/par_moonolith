# Acknowledgments

Library for parallel intersection detection and automatic load-balancing developed at ICS (Institute of Computational Science, Universita della Svizzera italiana).

Funded partly by the Swiss National Science Foundation (SNF) under the project 156178 "Geometry-Aware FEM in Computational Mechanics".

Funded partly as part of the activities of the Swiss Centre for Competence in Energy Research on Supply of Electricity (SCCER-SoE) and the Future Swiss Electrical Infrastructure (SCCER-FURIES), which is financially supported by the Swiss Innovation Agency (Innosuisse - SCCER program).
