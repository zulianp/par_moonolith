#ifndef MOONOLITH_ELEM_HEX_27_HPP
#define MOONOLITH_ELEM_HEX_27_HPP

#include "moonolith_convex_decomposition.hpp"
#include "moonolith_elem_hex.hpp"
#include "moonolith_elem_hex8.hpp"

namespace moonolith {

    template <typename T_, int PhysicalDim_ = 3>
    class Hex27 final : public Hex<T_, 2, PhysicalDim_> {
    public:
        static const int Order = 2;
        static const int Dim = 3;
        static const int PhysicalDim = PhysicalDim_;
        static const int NNodes = 27;

        using T = T_;

        using Vector = moonolith::Vector<T, Dim>;
        using CoVector = moonolith::Vector<T, PhysicalDim>;

        using Point = moonolith::Vector<T, Dim>;
        using CoPoint = moonolith::Vector<T, PhysicalDim>;

    private:
        inline static T f0(const T &x) { return 1.0 - 3.0 * x + 2.0 * (x * x); }

        inline static T f1(const T &x) { return -x + 2.0 * (x * x); }

        inline static T f2(const T &x) { return 4.0 * x - 4.0 * (x * x); }

        inline static T d0(const T &x) { return -3.0 + 4.0 * x; }

        inline static T d1(const T &x) { return -1.0 + 4.0 * x; }

        inline static T d2(const T &x) { return 4.0 - 8.0 * x; }

        class Fun final {
        public:
            Fun() {
                // corner nodes
                f[0] = [](const Point &p) -> T { return f0(p.x) * f0(p.y) * f0(p.z); };

                f[1] = [](const Point &p) -> T { return f1(p.x) * f0(p.y) * f0(p.z); };

                f[2] = [](const Point &p) -> T { return f1(p.x) * f1(p.y) * f0(p.z); };

                f[3] = [](const Point &p) -> T { return f0(p.x) * f1(p.y) * f0(p.z); };

                f[4] = [](const Point &p) -> T { return f0(p.x) * f0(p.y) * f1(p.z); };

                f[5] = [](const Point &p) -> T { return f1(p.x) * f0(p.y) * f1(p.z); };

                f[6] = [](const Point &p) -> T { return f1(p.x) * f1(p.y) * f1(p.z); };

                f[7] = [](const Point &p) -> T { return f0(p.x) * f1(p.y) * f1(p.z); };

                // edge nodes
                f[8] = [](const Point &p) -> T { return f2(p.x) * f0(p.y) * f0(p.z); };

                f[9] = [](const Point &p) -> T { return f1(p.x) * f2(p.y) * f0(p.z); };

                f[10] = [](const Point &p) -> T { return f2(p.x) * f1(p.y) * f0(p.z); };

                f[11] = [](const Point &p) -> T { return f0(p.x) * f2(p.y) * f0(p.z); };

                f[12] = [](const Point &p) -> T { return f0(p.x) * f0(p.y) * f2(p.z); };

                f[13] = [](const Point &p) -> T { return f1(p.x) * f0(p.y) * f2(p.z); };

                f[14] = [](const Point &p) -> T { return f1(p.x) * f1(p.y) * f2(p.z); };

                f[15] = [](const Point &p) -> T { return f0(p.x) * f1(p.y) * f2(p.z); };

                f[16] = [](const Point &p) -> T { return f2(p.x) * f0(p.y) * f1(p.z); };

                f[17] = [](const Point &p) -> T { return f1(p.x) * f2(p.y) * f1(p.z); };

                f[18] = [](const Point &p) -> T { return f2(p.x) * f1(p.y) * f1(p.z); };

                f[19] = [](const Point &p) -> T { return f0(p.x) * f2(p.y) * f1(p.z); };

                f[20] = [](const Point &p) -> T { return f2(p.x) * f2(p.y) * f0(p.z); };

                f[21] = [](const Point &p) -> T { return f2(p.x) * f0(p.y) * f2(p.z); };

                f[22] = [](const Point &p) -> T { return f1(p.x) * f2(p.y) * f2(p.z); };

                f[23] = [](const Point &p) -> T { return f2(p.x) * f1(p.y) * f2(p.z); };

                f[24] = [](const Point &p) -> T { return f0(p.x) * f2(p.y) * f2(p.z); };

                f[25] = [](const Point &p) -> T { return f2(p.x) * f2(p.y) * f1(p.z); };

                // internal
                f[26] = [](const Point &p) -> T { return f2(p.x) * f2(p.y) * f2(p.z); };
            }

            std::array<std::function<T(const Point &)>, NNodes> f;
        };

        class Grad final {
        public:
            Grad() {
                // corner nodes
                f[0] = [](const Point &p, Vector &g) {
                    // f0(p.x) * f0(p.y) * f0(p.z)
                    g.x = d0(p.x) * f0(p.y) * f0(p.z);
                    g.y = f0(p.x) * d0(p.y) * f0(p.z);
                    g.z = f0(p.x) * f0(p.y) * d0(p.z);
                };

                f[1] = [](const Point &p, Vector &g) {
                    // f1(p.x) * f0(p.y) * f0(p.z)
                    g.x = d1(p.x) * f0(p.y) * f0(p.z);
                    g.y = f1(p.x) * d0(p.y) * f0(p.z);
                    g.z = f1(p.x) * f0(p.y) * d0(p.z);
                };

                f[2] = [](const Point &p, Vector &g) {
                    // f1(p.x) * f1(p.y) * f0(p.z)
                    g.x = d1(p.x) * f1(p.y) * f0(p.z);
                    g.y = f1(p.x) * d1(p.y) * f0(p.z);
                    g.z = f1(p.x) * f1(p.y) * d0(p.z);
                };

                f[3] = [](const Point &p, Vector &g) {
                    // f0(p.x) * f1(p.y) * f0(p.z)
                    g.x = d0(p.x) * f1(p.y) * f0(p.z);
                    g.y = f0(p.x) * d1(p.y) * f0(p.z);
                    g.z = f0(p.x) * f1(p.y) * d0(p.z);
                };

                f[4] = [](const Point &p, Vector &g) {
                    // f0(p.x) * f0(p.y) * f1(p.z)
                    g.x = d0(p.x) * f0(p.y) * f1(p.z);
                    g.y = f0(p.x) * d0(p.y) * f1(p.z);
                    g.z = f0(p.x) * f0(p.y) * d1(p.z);
                };

                f[5] = [](const Point &p, Vector &g) {
                    // f1(p.x) * f0(p.y) * f1(p.z)
                    g.x = d1(p.x) * f0(p.y) * f1(p.z);
                    g.y = f1(p.x) * d0(p.y) * f1(p.z);
                    g.z = f1(p.x) * f0(p.y) * d1(p.z);
                };

                f[6] = [](const Point &p, Vector &g) {
                    // f1(p.x) * f1(p.y) * f1(p.z)
                    g.x = d1(p.x) * f1(p.y) * f1(p.z);
                    g.y = f1(p.x) * d1(p.y) * f1(p.z);
                    g.z = f1(p.x) * f1(p.y) * d1(p.z);
                };

                f[7] = [](const Point &p, Vector &g) {
                    // f0(p.x) * f1(p.y) * f1(p.z)
                    g.x = d0(p.x) * f1(p.y) * f1(p.z);
                    g.y = f0(p.x) * d1(p.y) * f1(p.z);
                    g.z = f0(p.x) * f1(p.y) * d1(p.z);
                };

                // //edge nodes
                f[8] = [](const Point &p, Vector &g) {
                    // f2(p.x) * f0(p.y) * f0(p.z)
                    g.x = d2(p.x) * f0(p.y) * f0(p.z);
                    g.y = f2(p.x) * d0(p.y) * f0(p.z);
                    g.z = f2(p.x) * f0(p.y) * d0(p.z);
                };

                f[9] = [](const Point &p, Vector &g) {
                    // f1(p.x) * f2(p.y) * f0(p.z)
                    g.x = d1(p.x) * f2(p.y) * f0(p.z);
                    g.y = f1(p.x) * d2(p.y) * f0(p.z);
                    g.z = f1(p.x) * f2(p.y) * d0(p.z);
                };

                f[10] = [](const Point &p, Vector &g) {
                    // f2(p.x) * f1(p.y) * f0(p.z);
                    g.x = d2(p.x) * f1(p.y) * f0(p.z);
                    g.y = f2(p.x) * d1(p.y) * f0(p.z);
                    g.z = f2(p.x) * f1(p.y) * d0(p.z);
                };

                f[11] = [](const Point &p, Vector &g) {
                    // f0(p.x) * f2(p.y) * f0(p.z);
                    g.x = d0(p.x) * f2(p.y) * f0(p.z);
                    g.y = f0(p.x) * d2(p.y) * f0(p.z);
                    g.z = f0(p.x) * f2(p.y) * d0(p.z);
                };

                f[12] = [](const Point &p, Vector &g) {
                    // f0(p.x) * f0(p.y) * f2(p.z);
                    g.x = d0(p.x) * f0(p.y) * f2(p.z);
                    g.y = f0(p.x) * d0(p.y) * f2(p.z);
                    g.z = f0(p.x) * f0(p.y) * d2(p.z);
                };

                f[13] = [](const Point &p, Vector &g) {
                    // f1(p.x) * f0(p.y) * f2(p.z);
                    g.x = d1(p.x) * f0(p.y) * f2(p.z);
                    g.y = f1(p.x) * d0(p.y) * f2(p.z);
                    g.z = f1(p.x) * f0(p.y) * d2(p.z);
                };

                f[14] = [](const Point &p, Vector &g) {
                    // f1(p.x) * f1(p.y) * f2(p.z);
                    g.x = d1(p.x) * f1(p.y) * f2(p.z);
                    g.y = f1(p.x) * d1(p.y) * f2(p.z);
                    g.z = f1(p.x) * f1(p.y) * d2(p.z);
                };

                f[15] = [](const Point &p, Vector &g) {
                    // f0(p.x) * f1(p.y) * f2(p.z);
                    g.x = d0(p.x) * f1(p.y) * f2(p.z);
                    g.y = f0(p.x) * d1(p.y) * f2(p.z);
                    g.z = f0(p.x) * f1(p.y) * d2(p.z);
                };

                f[16] = [](const Point &p, Vector &g) {
                    // f2(p.x) * f0(p.y) * f1(p.z);
                    g.x = d2(p.x) * f0(p.y) * f1(p.z);
                    g.y = f2(p.x) * d0(p.y) * f1(p.z);
                    g.z = f2(p.x) * f0(p.y) * d1(p.z);
                };

                f[17] = [](const Point &p, Vector &g) {
                    // f1(p.x) * f2(p.y) * f1(p.z);
                    g.x = d1(p.x) * f2(p.y) * f1(p.z);
                    g.y = f1(p.x) * d2(p.y) * f1(p.z);
                    g.z = f1(p.x) * f2(p.y) * d1(p.z);
                };

                f[18] = [](const Point &p, Vector &g) {
                    // f2(p.x) * f1(p.y) * f1(p.z);
                    g.x = d2(p.x) * f1(p.y) * f1(p.z);
                    g.y = f2(p.x) * d1(p.y) * f1(p.z);
                    g.z = f2(p.x) * f1(p.y) * d1(p.z);
                };

                f[19] = [](const Point &p, Vector &g) {
                    // f0(p.x) * f2(p.y) * f1(p.z);
                    g.x = d0(p.x) * f2(p.y) * f1(p.z);
                    g.y = f0(p.x) * d2(p.y) * f1(p.z);
                    g.z = f0(p.x) * f2(p.y) * d1(p.z);
                };

                f[20] = [](const Point &p, Vector &g) {
                    // f2(p.x) * f2(p.y) * f0(p.z);
                    g.x = d2(p.x) * f2(p.y) * f0(p.z);
                    g.y = f2(p.x) * d2(p.y) * f0(p.z);
                    g.z = f2(p.x) * f2(p.y) * d0(p.z);
                };

                f[21] = [](const Point &p, Vector &g) {
                    // f2(p.x) * f0(p.y) * f2(p.z);
                    g.x = d2(p.x) * f0(p.y) * f2(p.z);
                    g.y = f2(p.x) * d0(p.y) * f2(p.z);
                    g.z = f2(p.x) * f0(p.y) * d2(p.z);
                };

                f[22] = [](const Point &p, Vector &g) {
                    // f1(p.x) * f2(p.y) * f2(p.z);
                    g.x = d1(p.x) * f2(p.y) * f2(p.z);
                    g.y = f1(p.x) * d2(p.y) * f2(p.z);
                    g.z = f1(p.x) * f2(p.y) * d2(p.z);
                };

                f[23] = [](const Point &p, Vector &g) {
                    // f2(p.x) * f1(p.y) * f2(p.z);
                    g.x = d2(p.x) * f1(p.y) * f2(p.z);
                    g.y = f2(p.x) * d1(p.y) * f2(p.z);
                    g.z = f2(p.x) * f1(p.y) * d2(p.z);
                };

                f[24] = [](const Point &p, Vector &g) {
                    // f0(p.x) * f2(p.y) * f2(p.z);
                    g.x = d0(p.x) * f2(p.y) * f2(p.z);
                    g.y = f0(p.x) * d2(p.y) * f2(p.z);
                    g.z = f0(p.x) * f2(p.y) * d2(p.z);
                };

                f[25] = [](const Point &p, Vector &g) {
                    // f2(p.x) * f2(p.y) * f1(p.z);
                    g.x = d2(p.x) * f2(p.y) * f1(p.z);
                    g.y = f2(p.x) * d2(p.y) * f1(p.z);
                    g.z = f2(p.x) * f2(p.y) * d1(p.z);
                };

                // internal
                f[26] = [](const Point &p, Vector &g) {
                    // f2(p.x) * f2(p.y) * f2(p.z);
                    g.x = d2(p.x) * f2(p.y) * f2(p.z);
                    g.y = f2(p.x) * d2(p.y) * f2(p.z);
                    g.z = f2(p.x) * f2(p.y) * d2(p.z);
                };
            }

            std::array<std::function<void(const Point &, Vector &)>, NNodes> f;
        };

    public:
        T fun(const Integer i, const Point &p) const override { return fun_.f[i](p); }

        void grad(const Integer i, const Point &p, Vector &g) const override { grad_.f[i](p, g); }

        void hessian(const Integer, const Point &, std::array<T, Dim * Dim> &H) {
            std::fill(std::begin(H), std::end(H), 0.0);
        }

        void jacobian(const Point &p, std::array<T, PhysicalDim * Dim> &J) const override {
            if (is_affine()) {
                affine_approx_jacobian(J);
            } else {
                IsoParametric<Hex27>::jacobian(*this, p, J);
            }
        }

        void affine_approx_jacobian(std::array<T, PhysicalDim * Dim> &J) const override {
            Affine<Hex27>::jacobian(*this, {0, 1, 3, 4}, J);
        }

        CoPoint &node(const Integer i) override { return nodes_[i]; }

        const CoPoint &node(const Integer i) const override { return nodes_[i]; }

        void point(const Point &p, CoPoint &q) const override { IsoParametric<Hex27>::point(*this, p, q); }

        Hex27() : is_affine_(false) {}

        bool is_affine() const override { return is_affine_; }

        inline int n_nodes() const override { return NNodes; }
        inline int order() const override { return Order; }

        const std::array<CoPoint, NNodes> &nodes() const { return nodes_; }
        std::array<CoPoint, NNodes> &nodes() { return nodes_; }

        inline void set_affine(const bool val) { is_affine_ = val; }

        inline void make_reference() {
            Reference<Hex27>::points(nodes_);
            set_affine(true);
        }

        inline ElemType type() const override { return HEX27; }

        inline bool is_simplex() const override { return false; }

        inline T approx_measure() const override {
            if (is_affine()) {
                return approx_hexahedron_volume(nodes_);
            } else {
                assert(false);
                return approx_hexahedron_volume(nodes_);
            }
        }

        inline T measure() const override {
            if (is_affine()) {
                return approx_hexahedron_volume(nodes_);
            } else {
                Quadrature<T, Dim> q;
                Gauss::Hex::get(2, q);
                return IsoParametric<Hex27>::measure(*this, q);
            }
        }

        inline T reference_measure() const override { return 1.0; }

        inline void make(Polyhedron<T> &poly) const override {
            assert(is_affine());
            ConvexDecomposition<T, PhysicalDim>::make_hex(nodes(), 0, 1, 2, 3, 4, 5, 6, 7, poly);
        }

        inline void make(Storage<Polyhedron<T>> &poly) const override {
            if (is_affine()) {
                poly.resize(1);
                ConvexDecomposition<T, PhysicalDim>::make_hex(nodes(), 0, 1, 2, 3, 4, 5, 6, 7, poly[0]);
            } else {
                ConvexDecomposition<T, PhysicalDim>::decompose_hex27(nodes(), poly);
            }
        }

    private:
        bool is_affine_;
        std::array<CoPoint, NNodes> nodes_;
        const Fun fun_;
        const Grad grad_;
    };

    template <typename T, int PhysicalDim>
    class Reference<Hex27<T, PhysicalDim>> {
    public:
        static const int NNodes = 27;

        template <class Point, std::size_t Size>
        static void points(std::array<Point, Size> &nodes) {
            static_assert(Size >= NNodes, "size must be at least NNodes");
            ///////////////////////

            Reference<Hex8<T, PhysicalDim>>::points(nodes);

            // p8
            nodes[8].x = 0.5;
            nodes[8].y = 0.0;
            nodes[8].z = 0.0;

            // p9
            nodes[9].x = 1.0;
            nodes[9].y = 0.5;
            nodes[9].z = 0.0;

            // p10
            nodes[10].x = 0.5;
            nodes[10].y = 1.0;
            nodes[10].z = 0.0;

            // p11
            nodes[11].x = 0.0;
            nodes[11].y = 0.5;
            nodes[11].z = 0.0;

            ///////////////////////

            // p12
            nodes[12].x = 0.0;
            nodes[12].y = 0.0;
            nodes[12].z = 0.5;

            // p13
            nodes[13].x = 1.0;
            nodes[13].y = 0.0;
            nodes[13].z = 0.5;

            // p14
            nodes[14].x = 1.0;
            nodes[14].y = 1.0;
            nodes[14].z = 0.5;

            // p15
            nodes[15].x = 0.0;
            nodes[15].y = 1.0;
            nodes[15].z = 0.5;

            ///////////////////////

            // p16
            nodes[16].x = 0.5;
            nodes[16].y = 0.0;
            nodes[16].z = 1.0;

            // p17
            nodes[17].x = 1.0;
            nodes[17].y = 0.5;
            nodes[17].z = 1.0;

            // p18
            nodes[18].x = 0.5;
            nodes[18].y = 1.0;
            nodes[18].z = 1.0;

            // p19
            nodes[19].x = 0.0;
            nodes[19].y = 0.5;
            nodes[19].z = 1.0;

            ///////////////////////

            // p20
            nodes[20].x = 0.5;
            nodes[20].y = 0.5;
            nodes[20].z = 0.0;

            // p21
            nodes[21].x = 0.5;
            nodes[21].y = 0.0;
            nodes[21].z = 0.5;

            // p22
            nodes[22].x = 1.0;
            nodes[22].y = 0.5;
            nodes[22].z = 0.5;

            // p23
            nodes[23].x = 0.5;
            nodes[23].y = 1.0;
            nodes[23].z = 0.5;

            ///////////////////////

            // p24
            nodes[24].x = 0.0;
            nodes[24].y = 0.5;
            nodes[24].z = 0.5;

            // p25
            nodes[25].x = 0.5;
            nodes[25].y = 0.5;
            nodes[25].z = 1.0;

            // p26
            nodes[26].x = 0.5;
            nodes[26].y = 0.5;
            nodes[26].z = 0.5;
        }

        static T measure() { return 1.0; }
    };

    template <typename T, int PhysicalDim>
    class GaussQRule<Hex27<T, PhysicalDim>> {
    public:
        static bool get(const Integer order, Quadrature3<T> &q) { return Gauss::Hex::get(order, q); }
    };

}  // namespace moonolith

#endif  // MOONOLITH_ELEM_HEX_27_HPP
