#ifndef BASE_INTERFACES_CL
#define BASE_INTERFACES_CL
#ifndef CLIPP_HOST_CL

//Forward declarations of types


//functions
bool approx_equal(const Scalar left, const Scalar right, const Scalar tol);


#endif //CLIPP_HOST_CL
#endif //BASE_INTERFACES_CL
