#ifndef MOONOLITH_LOOK_UP_TABLE_BUILDER_HPP
#define MOONOLITH_LOOK_UP_TABLE_BUILDER_HPP

#include "moonolith_predicate.hpp"
#include "moonolith_lookup_table.hpp"
#include "moonolith_traversal_algorithm.hpp"
#include "moonolith_collector.hpp"
#include "moonolith_verbose_object.hpp"

#include <memory>

namespace moonolith {

	template<class Tree>
	class LookUpTableBuilder : public VerboseObject {
	public:
		virtual ~LookUpTableBuilder() {}

		void set_must_build_data_lookup(const bool value)
		{
			if(value) {
				collector_.reset();
			} else {
				collector_ = std::make_shared< Collector<Tree> >();
			}
		}

		void add_traversal_listener(const std::shared_ptr< TraversalListener<Tree> > &listener)
		{
			listeners_.push_back(listener);
		}

		/// @brief !!!Use this to instatntiate the traversal algorithm
		std::shared_ptr< TraversalAlgorithm<Tree> > new_traversal_algorithm(Tree &tree, const Integer rank, const std::shared_ptr<LookUpTable> &lookup_table) 
		{
			std::shared_ptr< TraversalAlgorithm<Tree> > algorithm = std::make_shared< TraversalAlgorithm<Tree> >(tree, rank, lookup_table);
			if(collector_) {
				collector_->set_lookup_table(lookup_table);
				algorithm->add_listener(collector_);
				algorithm->add_listeners(listeners_.begin(), listeners_.end());
			}

			return algorithm;
		}

		virtual bool build_for(Communicator &comm, Tree &tree, const std::shared_ptr<LookUpTable> &lookup_table) = 0;
		
		LookUpTableBuilder()
		: collector_(std::make_shared< Collector<Tree> >())
		{}

	private:
		std::shared_ptr< Collector<Tree> > collector_;
		std::vector< std::shared_ptr< TraversalListener<Tree> > > listeners_;

	};
	
}

#endif //MOONOLITH_LOOK_UP_TABLE_BUILDER_HPP
