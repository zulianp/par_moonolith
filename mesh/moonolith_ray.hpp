#ifndef MOONOLITH_RAY_HPP
#define MOONOLITH_RAY_HPP

#include "moonolith_vector.hpp"

namespace moonolith {

	template<typename T, int Dim>
	class Ray {
	public:
		Vector<T, Dim> o, dir;

        inline void make(const T t, Vector<T, Dim> &p) const
        {
            p = dir;
            p *= t;
            p += o;
        }
	};

}

#endif //MOONOLITH_RAY_HPP
