#ifndef MOONOLITH_ONE_MASTER_ONE_SLAVE_ALGORITHM_HPP
#define MOONOLITH_ONE_MASTER_ONE_SLAVE_ALGORITHM_HPP

#include "moonolith_aabb.hpp"
#include "moonolith_asynch_lookup_table_builder.hpp"
#include "moonolith_bounding_volume_with_span.hpp"
#include "moonolith_check_can_refine.hpp"
#include "moonolith_collection_traits.hpp"
#include "moonolith_communicator.hpp"
#include "moonolith_data_handle.hpp"
#include "moonolith_describe.hpp"
#include "moonolith_exchange.hpp"
#include "moonolith_lookup_table_builder.hpp"
#include "moonolith_n_tree_bound_synchronize.hpp"
#include "moonolith_n_tree_mutator_factory.hpp"
#include "moonolith_n_tree_with_span_mutator_factory.hpp"
#include "moonolith_n_tree_with_tags_mutator_factory.hpp"
#include "moonolith_predicate.hpp"
#include "moonolith_tree.hpp"
// #include "moonolith_test_trees.hpp"
#include "moonolith_check_stream.hpp"
#include "moonolith_element_adapter.hpp"
#include "moonolith_make_unique.hpp"
#include "moonolith_polygon.hpp"
#include "moonolith_search_radius.hpp"
#include "par_moonolith.hpp"

#include <algorithm>
#include <cassert>
#include <map>
#include <numeric>

namespace moonolith {

    template <Integer Dimension_, class Collection>
    class OneMasterOneSlaveAlgorithm {
    public:
        typedef moonolith::AABBWithKDOPSpan<Dimension_, Real> Bound;
        using CM = moonolith::CollectionManager<Collection>;
        using Elem = typename CM::Elem;

        using Adapter = moonolith::ElementAdapter<Bound, Collection>;

        struct Traits {
            enum { Dimension = Dimension_ };

            typedef moonolith::AABBWithKDOPSpan<Dimension, Real> Bound;
            typedef Adapter DataType;
        };

        class OneMasterOneSlaveTree : public Tree<Traits> {
        public:
            static std::shared_ptr<OneMasterOneSlaveTree> New(const std::shared_ptr<Predicate> &pred) {
                std::shared_ptr<OneMasterOneSlaveTree> ret = std::make_shared<OneMasterOneSlaveTree>();
                // std::shared_ptr< NTreeWithSpanMutatorFactory<OneMasterOneSlaveTree> > factory = std::make_shared<
                // NTreeWithSpanMutatorFactory<OneMasterOneSlaveTree> >();
                std::shared_ptr<NTreeWithTagsMutatorFactory<OneMasterOneSlaveTree>> factory =
                    std::make_shared<NTreeWithTagsMutatorFactory<OneMasterOneSlaveTree>>(pred);

                factory->set_refine_params(20, 6);
                ret->set_mutator_factory(factory);
                return ret;
            }

            OneMasterOneSlaveTree() {}
        };

        using DataContainer = typename OneMasterOneSlaveTree::DataContainer;

        class MasterAndSlaveCollections {
        public:
            std::shared_ptr<CM> cm;
            std::shared_ptr<const Collection> collections[2];
            Storage<Integer> selections[2];

            MasterAndSlaveCollections(const std::shared_ptr<CM> &cm) : cm(cm) {}
            MasterAndSlaveCollections(const std::shared_ptr<CM> &cm,
                                      const std::shared_ptr<const Collection> &master,
                                      const std::shared_ptr<const Collection> &slave)
                : cm(cm) {
                collections[0] = master;
                collections[1] = slave;
            }

            template <class Iterator>
            void serialize(const Iterator &begin, const Iterator &end, OutputStream &os) {
                selections[0].clear();
                selections[1].clear();

                bool has_master = static_cast<bool>(collections[0]);
                bool has_slave = static_cast<bool>(collections[1]);

                auto n_master_elems = 0;

                if (has_master) {
                    n_master_elems = CM::n_elements(*collections[0]);
                }

                bool met_slave_selection = false;
                for (Iterator it = begin; it != end; ++it) {
                    int index = *it;

                    if (collections[0] && index >= n_master_elems) {
                        index -= n_master_elems;
                        selections[1].push_back(index);
                    } else if (!collections[0]) {
                        met_slave_selection = true;
                        selections[1].push_back(index);
                    } else {
                        assert(!met_slave_selection);
                        assert(index < n_master_elems);
                        selections[0].push_back(index);
                    }
                }

                has_master = !selections[0].empty();
                has_slave = !selections[1].empty();

                ////// WRITE
                os << has_master << has_slave;

                if (has_master) {
                    cm->serialize(*collections[0], std::begin(selections[0]), std::end(selections[0]), os);
                }

                if (has_slave) {
                    cm->serialize(*collections[1], std::begin(selections[1]), std::end(selections[1]), os);
                }
            }

            void build(InputStream &is) {
                bool has_master = false, has_slave = false;

                ////// READ
                is >> has_master >> has_slave;

                if (has_master) {
                    collections[0] = cm->build(is);
                }

                if (has_slave) {
                    collections[1] = cm->build(is);
                }
            }
        };

        class SerializerDeserializer {
        public:
            SerializerDeserializer(const Communicator &comm,
                                   const std::shared_ptr<MasterAndSlaveCollections> &local_collections_in,
                                   const SearchRadius<Real> &search_radius)
                : comm(comm),
                  cm(local_collections_in->cm),
                  search_radius(search_radius),
                  local_collections(local_collections_in) {}

            Communicator comm;
            std::shared_ptr<CollectionManager<Collection>> cm;
            SearchRadius<Real> search_radius;
            std::shared_ptr<MasterAndSlaveCollections> local_collections;
            std::map<Integer, std::shared_ptr<MasterAndSlaveCollections>> collections;
            std::map<Integer, std::vector<std::shared_ptr<MasterAndSlaveCollections>>> migrated_collections;

            void read(const Integer ownerrank,
                      const Integer /*senderrank*/,
                      bool is_forwarding,
                      DataContainer &data,
                      InputStream &in) {
                CHECK_STREAM_READ_BEGIN("serialize_deserialize", in);

                auto ms_collection = std::make_shared<MasterAndSlaveCollections>(cm);
                ms_collection->build(in);

                if (!is_forwarding) {
                    assert(!collections[ownerrank]);
                    collections[ownerrank] = ms_collection;
                } else {
                    migrated_collections[ownerrank].push_back(ms_collection);
                }

                Integer n_new_adapters = 0;
                for (auto s : ms_collection->collections) {
                    if (s) {
                        n_new_adapters += CM::n_elements(*s);
                    }
                }

                data.reserve(data.size() + n_new_adapters);

                int space_num = 0;

                Integer idx = 0;
                for (auto s : ms_collection->collections) {
                    if (s) {
                        auto e_begin = CM::elements_begin(*s);
                        auto e_end = CM::elements_end(*s);

                        //!!! libmesh might create problems in this context

                        Integer sub_idx = 0;
                        for (auto it = e_begin; it != e_end; ++it, ++idx, ++sub_idx) {
                            auto tag = cm->tag(*s, it);

                            // auto mode just return -1
                            if (tag < 0) {
                                tag = space_num;
                            }

                            data.push_back(Adapter(&(*s), cm->handle(*s, it), idx, tag, search_radius.get(tag)));
                            cm->data_added(sub_idx, data.back());
                        }
                    }

                    ++space_num;
                }

                CHECK_STREAM_READ_END("serialize_deserialize", in);
            }

            void write(const Integer ownerrank,
                       const Integer /*recvrank*/,
                       const std::vector<Integer>::const_iterator &begin,
                       const std::vector<Integer>::const_iterator &end,
                       const DataContainer & /*data*/,
                       OutputStream &out) {
                CHECK_STREAM_WRITE_BEGIN("serialize_deserialize", out);

                if (ownerrank == comm.rank()) {
                    local_collections->serialize(begin, end, out);
                } else {
                    auto it = collections.find(ownerrank);
                    assert(it != collections.end());
                    assert(std::distance(begin, end) > 0);
                    it->second->serialize(begin, end, out);
                }

                CHECK_STREAM_WRITE_END("serialize_deserialize", out);
            }
        };

        void add_master_and_slave_pairing(const Integer master, const Integer slave) { predicate->add(master, slave); }

        DataHandle add_elem(const Adapter &a) { return tree->insert(a); }

        DataHandle add_elem(const Collection *collection,
                            const Integer element_handle,
                            const Integer handle,
                            const Integer tag,
                            const Real blow_up) {
            return tree->insert(Adapter(collection, element_handle, handle, tag, blow_up));
        }

        bool compute(const std::function<bool(const Adapter &, const Adapter &)> &f) {
            SerializerDeserializer serializer_deserializer(comm, local_collections, *search_radius_);

            auto read = [&serializer_deserializer](const Integer ownerrank,
                                                   const Integer senderrank,
                                                   bool is_forwarding,
                                                   DataContainer &data,
                                                   InputStream &in) {
                serializer_deserializer.read(ownerrank, senderrank, is_forwarding, data, in);
            };

            auto write = [&serializer_deserializer](const Integer ownerrank,
                                                    const Integer recvrank,
                                                    const std::vector<Integer>::const_iterator &begin,
                                                    const std::vector<Integer>::const_iterator &end,
                                                    const DataContainer &data,
                                                    OutputStream &out) {
                serializer_deserializer.write(ownerrank, recvrank, begin, end, data, out);
            };

            Integer found_matches = 0, false_positives = 0;

            auto fun = [&](Adapter &master, Adapter &slave) -> bool {
                if (f(master, slave)) {
                    found_matches++;
                    return true;
                } else {
                    false_positives++;
                    return false;
                }
            };

            moonolith::search_and_compute(comm, tree, predicate, read, write, fun, settings);

            if (Moonolith::instance().verbose()) {
                logger() << "candidates: " << (found_matches + false_positives) << " matches: " << found_matches
                         << " false positives: " << false_positives << std::endl;
            }
            
            return found_matches > 0;
        }

        bool init(const Collection &master,
                  const Collection &slave,
                  const std::vector<std::pair<int, int>> &master_slave_tags,
                  const Real blow_up) {
            return init(master, slave, master_slave_tags, std::make_shared<SearchRadius<Real>>(blow_up));
        }

        bool init(const Collection &master,
                  const Collection &slave,
                  const std::vector<std::pair<int, int>> &master_slave_tags,
                  const std::shared_ptr<SearchRadius<Real>> &search_radius) {
            search_radius_ = search_radius;

            for (const auto &mst : master_slave_tags) {
                add_master_and_slave_pairing(mst.first, mst.second);
            }

            local_collections = std::make_shared<MasterAndSlaveCollections>(
                cm, moonolith::make_ref(master), moonolith::make_ref(slave));

            auto master_end = cm->elements_end(master);
            auto master_begin = cm->elements_begin(master);

            tree->reserve(cm->n_elements(master) + cm->n_elements(slave));

            Integer idx = 0;
            for (auto it = master_begin; it != master_end; ++it, ++idx) {
                if (cm->skip(master, it)) continue;

                const auto tag = cm->tag(master, it);
                if (predicate->select(tag)) {
                    auto data_handle = add_elem(&master, cm->handle(master, it), idx, tag, search_radius_->get(tag));
                    cm->data_added(idx, (*tree)[data_handle]);
                }
            }

            auto slave_begin = cm->elements_begin(slave);
            auto slave_end = cm->elements_end(slave);

            // auto offset = cm->n_elements(master);

            Integer slave_idx = 0;
            for (auto it = slave_begin; it != slave_end; ++it, ++idx, ++slave_idx) {
                if (cm->skip(slave, it)) continue;

                const auto tag = cm->tag(slave, it);
                if (predicate->select(tag)) {
                    auto data_handle = add_elem(&slave, cm->handle(slave, it), idx, tag, search_radius_->get(tag));
                    cm->data_added(slave_idx, (*tree)[data_handle]);
                }
            }

            //////////////////////////////////////////////////////
            //////////////////////////////////////////////////////

            tree->root()->bound().static_bound().enlarge(1e-8);
            return true;
        }

        bool init_simple(const Collection &master, const Collection &slave, const Real blow_up) {
            return init_simple(master, slave, std::make_shared<SearchRadius<Real>>(blow_up));
        }

        bool init_simple(const Collection &master,
                         const Collection &slave,
                         const std::shared_ptr<SearchRadius<Real>> &search_radius) {
            search_radius_ = search_radius;
            local_collections = std::make_shared<MasterAndSlaveCollections>(
                cm, moonolith::make_ref(master), moonolith::make_ref(slave));

            auto master_end = cm->elements_end(master);
            auto master_begin = cm->elements_begin(master);

            add_master_and_slave_pairing(0, 1);

            tree->reserve(cm->n_elements(master) + cm->n_elements(slave));

            Integer idx = 0;
            for (auto it = master_begin; it != master_end; ++it, ++idx) {
                if (cm->skip(master, it)) continue;

                auto data_handle = add_elem(&master, cm->handle(master, it), idx, 0, search_radius_->get(0));
                cm->data_added(idx, (*tree)[data_handle]);
            }

            auto slave_begin = cm->elements_begin(slave);
            auto slave_end = cm->elements_end(slave);

            // auto offset = cm->n_elements(master);

            Integer slave_idx = 0;
            for (auto it = slave_begin; it != slave_end; ++it, ++idx, ++slave_idx) {
                if (cm->skip(slave, it)) continue;

                auto data_handle = add_elem(&slave, cm->handle(slave, it), idx, 1, search_radius_->get(1));
                cm->data_added(slave_idx, (*tree)[data_handle]);
            }

            //////////////////////////////////////////////////////
            //////////////////////////////////////////////////////

            tree->root()->bound().static_bound().enlarge(1e-8);
            return true;
        }

        OneMasterOneSlaveAlgorithm(Communicator &comm,
                                   const std::shared_ptr<CollectionManager<Collection>> &cm,
                                   const SearchSettings &settings = SearchSettings())
            : comm(comm),
              predicate(std::make_shared<moonolith::MasterAndSlave>()),
              cm(cm),
              settings(settings),
              search_radius_(std::make_shared<SearchRadius<Real>>(0.0)) {
            tree = OneMasterOneSlaveTree::New(predicate);
        }

        OneMasterOneSlaveAlgorithm(Communicator &comm, const SearchSettings &settings = SearchSettings())
            : comm(comm),
              predicate(std::make_shared<moonolith::MasterAndSlave>()),
              cm(moonolith::make_unique<CollectionManager<Collection>>(comm, true)),
              settings(settings),
              search_radius_(std::make_shared<SearchRadius<Real>>(0.0)) {
            tree = OneMasterOneSlaveTree::New(predicate);
        }

    private:
        Communicator comm;
        std::shared_ptr<OneMasterOneSlaveTree> tree;
        std::shared_ptr<moonolith::MasterAndSlave> predicate;
        std::shared_ptr<LookUpTable> table;
        std::shared_ptr<MasterAndSlaveCollections> local_collections;
        std::vector<std::set<Integer>> candidates;
        std::shared_ptr<CollectionManager<Collection>> cm;
        SearchSettings settings;
        std::shared_ptr<SearchRadius<Real>> search_radius_;

        bool build_look_up_table() {
            table = std::make_shared<LookUpTable>();
            AsynchLookUpTableBuilder<OneMasterOneSlaveTree> builder;

            if (!builder.build_for(comm, *tree, table)) {
                return false;
            }

            assert(table->check_consistency(comm));
            return true;
        }
    };
}  // namespace moonolith

#endif  // MOONOLITH_ONE_MASTER_ONE_SLAVE_ALGORITHM_HPP
