#ifndef MOONOLITH_SIMPLEX_TRANSFORM_HPP
#define MOONOLITH_SIMPLEX_TRANSFORM_HPP

#include "moonolith_vector.hpp"
#include "moonolith_geo_algebra.hpp"
#include "moonolith_affine_transform.hpp"

namespace moonolith {

    template<typename T, int Dim>
    class TetrahedronTransform {};

    template<typename T>
    class TetrahedronTransform<T, 3> final {
    public:
        using Point = moonolith::Vector<T, 3>;

        TetrahedronTransform() {}

        TetrahedronTransform(const Point &p1, const Point &p2, const Point &p3, const Point &p4)
        : o(p1), u1(p2 - p1), u2(p3 - p1), u3(p4 - p1)
        {}

        inline void reinit(const Point &p2, const Point &p3, const Point &p4)
        {
            u1 = p2 - o;
            u2 = p3 - o;
            u3 = p4 - o;
        }

        inline void apply(const Point &in, Point &out) const {
            out.x = o.x + u1.x * in.x + u2.x * in.y + u3.x * in.z;
            out.y = o.y + u1.y * in.x + u2.y * in.y + u3.y * in.z;
            out.z = o.z + u1.z * in.x + u2.z * in.y + u3.z * in.z;
        }

        inline T volume() const {
            return dot(u3, cross(u1, u2)) / 6.0;
        }

        Point o;
        Point u1;
        Point u2;
        Point u3;
    };

    template<typename T>
    class TetrahedronTransform<T, 4> final {
    public:
        using Point  = moonolith::Vector<T, 4>;
        using Point3 = moonolith::Vector<T, 3>;

        TetrahedronTransform() {}

        TetrahedronTransform(const Point &p1, const Point &p2, const Point &p3, const Point &p4)
        : o(p1), u1(p2 - p1), u2(p3 - p1), u3(p4 - p1)
        {
            init_jacobian();
        }

        inline void reinit(const Point &p2, const Point &p3, const Point &p4)
        {
            u1 = p2 - o;
            u2 = p3 - o;
            u3 = p4 - o;

            init_jacobian();
        }

        inline void apply(const Point3 &in, Point &out) const {
            out.x = o.x + u1.x * in.x + u2.x * in.y + u3.x * in.z;
            out.y = o.y + u1.y * in.x + u2.y * in.y + u3.y * in.z;
            out.z = o.z + u1.z * in.x + u2.z * in.y + u3.z * in.z;
            out.w = o.w + u1.w * in.x + u2.w * in.y + u3.w * in.z;
        }

        inline T volume() const {
            return pseudo_det<T, 4, 3>(A)/24.;
        }

        Point o;
        Point u1;
        Point u2;
        Point u3;

        std::array<T, 4*3> A;

        void init_jacobian()
        {
            //row 1
            A[0] = u1.x;
            A[1] = u2.x;
            A[2] = u3.x;

            //row 2
            A[3] = u1.y;
            A[4] = u2.y;
            A[5] = u3.y;

            //row 3
            A[6] = u1.z;
            A[7] = u2.z;
            A[8] = u3.z;

            //row 4
            A[9]  = u1.w;
            A[10] = u2.w;
            A[11] = u3.w;
        }
    };

    template<typename T, int Dim>
    class TriangleTransform {};

    template<typename T>
    class TriangleTransform<T, 2> final {
    public:
        using Point = moonolith::Vector<T, 2>;

        TriangleTransform() {}

        TriangleTransform(const Point &p1, const Point &p2, const Point &p3)
        : o(p1), u1(p2 - p1), u2(p3 - p1)
        {}

        inline void reinit(const Point &p2, const Point &p3)
        {
            u1 = p2 - o;
            u2 = p3 - o;
        }

        inline void apply(const Point &in, Point &out) const {
            out = o + u1 * in.x + u2 * in.y;
        }

        inline T volume() const {
            return 0.5 * (u1.x * u2.y - u2.x * u1.y);
        }

        Point o;
        Point u1;
        Point u2;
    };

    template<typename T>
    class TriangleTransform<T, 3> final {
    public:
        using Point = moonolith::Vector<T, 3>;

        TriangleTransform() {}

        TriangleTransform(const Point &p1, const Point &p2, const Point &p3)
        : o(p1), u1(p2 - p1), u2(p3 - p1)
        {}

        inline void reinit(const Point &p2, const Point &p3)
        {
            u1 = p2 - o;
            u2 = p3 - o;
        }

        inline void apply(const moonolith::Vector<T, 2> &in, Point &out) const {
            out = o + u1 * in.x + u2 * in.y;
        }

        inline T volume() const {
            return 0.5 * length(cross(u1, u2));
        }

        Point o;
        Point u1;
        Point u2;
    };

    template<typename T, int Dim>
    class LineTransform final {
    public:
        using Point = moonolith::Vector<T, Dim>;

        LineTransform() {}

        LineTransform(const Point &p1, const Point &p2)
        : o(p1), u(p2 - p1)
        {}

        inline void reinit(const Point &p2)
        {
            u = p2 - o;
        }

        inline void apply(const moonolith::Vector<T, 1> &in, Point &out) const {
            out = o + u * in.x;
        }

        inline T volume() const {
            return length(u);
        }

        Point o;
        Point u;
    };

}

#endif //MOONOLITH_SIMPLEX_TRANSFORM_HPP
