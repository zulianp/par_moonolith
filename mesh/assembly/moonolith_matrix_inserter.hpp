#ifndef MOONOLITH_MATRIX_INSERTER_HPP
#define MOONOLITH_MATRIX_INSERTER_HPP

#include "moonolith_communicator.hpp"
#include "moonolith_expanding_array.hpp"
#include "moonolith_redistribute.hpp"
#include "moonolith_sparse_matrix.hpp"

#include <numeric>
#include <vector>

namespace moonolith {

    template <typename T>
    class MatrixInserter {
    public:
        virtual ~MatrixInserter() {}

        MatrixInserter(const Communicator &comm, const bool use_add = true)
            : comm(comm), m_matrix(comm), redist(comm), use_add(use_add) {}

        void finalize(const int n_local_rows, const int n_local_cols) {
            ownership_ranges_rows.resize(comm.size() + 1);
            ownership_ranges_cols.resize(comm.size() + 1);

            std::fill(ownership_ranges_rows.begin(), ownership_ranges_rows.end(), 0);
            std::fill(ownership_ranges_cols.begin(), ownership_ranges_cols.end(), 0);

            ownership_ranges_rows[comm.rank() + 1] = n_local_rows;
            ownership_ranges_cols[comm.rank() + 1] = n_local_cols;

            comm.all_reduce(
                &ownership_ranges_rows[0], static_cast<int>(ownership_ranges_rows.size()), moonolith::MPISum());
            comm.all_reduce(
                &ownership_ranges_cols[0], static_cast<int>(ownership_ranges_cols.size()), moonolith::MPISum());

            std::partial_sum(ownership_ranges_rows.begin(), ownership_ranges_rows.end(), ownership_ranges_rows.begin());
            std::partial_sum(ownership_ranges_cols.begin(), ownership_ranges_cols.end(), ownership_ranges_cols.begin());

            m_matrix.set_size(
                ownership_ranges_rows[comm.size()], ownership_ranges_cols[comm.size()], n_local_rows, n_local_cols);

            if (use_add) {
                redist.apply(ownership_ranges_rows, m_matrix, moonolith::AddAssign<T>());
            } else {
                redist.apply(ownership_ranges_rows, m_matrix, moonolith::Assign<T>());
            }
        }

        void finalize(const int n_local_rows) {
            ownership_ranges_rows.resize(comm.size() + 1);
            std::fill(ownership_ranges_rows.begin(), ownership_ranges_rows.end(), 0);
            ownership_ranges_rows[comm.rank() + 1] = n_local_rows;
            comm.all_reduce(
                &ownership_ranges_rows[0], static_cast<int>(ownership_ranges_rows.size()), moonolith::MPISum());
            std::partial_sum(ownership_ranges_rows.begin(), ownership_ranges_rows.end(), ownership_ranges_rows.begin());

            m_matrix.set_size(ownership_ranges_rows[comm.size()], 1, n_local_rows, 1);

            if (use_add) {
                redist.apply(ownership_ranges_rows, m_matrix, moonolith::AddAssign<T>());
            } else {
                redist.apply(ownership_ranges_rows, m_matrix, moonolith::Assign<T>());
            }
        }

        template <typename IDX, class ElementMatrix>
        void add(const std::vector<IDX> &rows, const std::vector<IDX> &cols, ElementMatrix &mat) {
            Integer n_rows = static_cast<Integer>(rows.size());
            Integer n_cols = static_cast<Integer>(cols.size());

            for (Integer i = 0; i < n_rows; ++i) {
                auto dof_I = rows[i];
                for (Integer j = 0; j < n_cols; ++j) {
                    auto dof_J = cols[j];
                    m_matrix.add(dof_I, dof_J, mat(i, j));
                }
            }
        }

        template <typename IDX, class ElementMatrix>
        void add(const moonolith::Storage<IDX> &rows, const moonolith::Storage<IDX> &cols, ElementMatrix &mat) {
            Integer n_rows = static_cast<Integer>(rows.size());
            Integer n_cols = static_cast<Integer>(cols.size());

            for (Integer i = 0; i < n_rows; ++i) {
                auto dof_I = rows[i];
                for (Integer j = 0; j < n_cols; ++j) {
                    auto dof_J = cols[j];
                    m_matrix.add(dof_I, dof_J, mat(i, j));
                }
            }
        }

        template <typename IDX>
        void insert(const IDX &idx, const T val) {
            if (use_add) {
                m_matrix.add(idx, 0, val);
            } else {
                if (val != 0.) {
                    m_matrix.set(idx, 0, val);
                }
            }
        }

        template <typename IDX>
        void insert(const std::vector<IDX> &idx, const T val) {
            if (use_add) {
                for (auto i : idx) {
                    m_matrix.add(i, 0, val);
                }
            } else {
                if (val != 0.) {
                    for (auto i : idx) {
                        m_matrix.set(i, 0, val);
                    }
                }
            }
        }

        template <typename IDX>
        void insert(const moonolith::Storage<IDX> &idx, const T val) {
            if (use_add) {
                for (auto i : idx) {
                    m_matrix.add(i, 0, val);
                }
            } else {
                if (val != 0.) {
                    for (auto i : idx) {
                        m_matrix.set(i, 0, val);
                    }
                }
            }
        }

        template <typename IDX, class ElementMatrix>
        void insert(const std::vector<IDX> &rows, const std::vector<IDX> &cols, ElementMatrix &mat) {
            if (use_add) {
                add(rows, cols, mat);
            } else {
                set_non_zero(rows, cols, mat);
            }
        }

        template <typename IDX, class ElementMatrix>
        void set(const std::vector<IDX> &rows, const std::vector<IDX> &cols, ElementMatrix &mat) {
            Integer n_rows = static_cast<Integer>(rows.size());
            Integer n_cols = static_cast<Integer>(cols.size());

            for (Integer i = 0; i < n_rows; ++i) {
                auto dof_I = rows[i];
                for (Integer j = 0; j < n_cols; ++j) {
                    auto dof_J = cols[j];
                    m_matrix.set(dof_I, dof_J, mat(i, j));
                }
            }
        }

        template <typename IDX, class ElementMatrix>
        void set_non_zero(const std::vector<IDX> &rows, const std::vector<IDX> &cols, ElementMatrix &mat) {
            Integer n_rows = static_cast<Integer>(rows.size());
            Integer n_cols = static_cast<Integer>(cols.size());

            for (Integer i = 0; i < n_rows; ++i) {
                auto dof_I = rows[i];
                for (Integer j = 0; j < n_cols; ++j) {
                    auto dof_J = cols[j];

                    if (std::abs(mat(i, j)) != 0.) {
                        m_matrix.set(dof_I, dof_J, mat(i, j));
                    }
                }
            }
        }

        template <typename IDX, class ElementVector>
        void set(const std::vector<IDX> &rows, ElementVector &vec) {
            Integer n_rows = static_cast<Integer>(rows.size());

            for (Integer i = 0; i < n_rows; ++i) {
                auto dof_I = rows[i];
                m_matrix.set(dof_I, 0, vec(i));
            }
        }

        template <typename IDX, class ElementVector>
        void set_non_zero(const std::vector<IDX> &rows, ElementVector &vec) {
            Integer n_rows = static_cast<Integer>(rows.size());

            for (Integer i = 0; i < n_rows; ++i) {
                auto dof_I = rows[i];

                if (std::abs(vec(i)) != 0.) {
                    m_matrix.set(dof_I, 0, vec(i));
                }
            }
        }

        template <typename IDX>
        void set_non_zero(const std::vector<IDX> &rows, std::vector<T> &vec) {
            Integer n_rows = static_cast<Integer>(rows.size());

            for (Integer i = 0; i < n_rows; ++i) {
                auto dof_I = rows[i];

                if (std::abs(vec[i]) != 0.) {
                    m_matrix.set(dof_I, 0, vec[i]);
                }
            }
        }

        template <typename IDX, class ElementVector>
        void add(const std::vector<IDX> &rows, ElementVector &vec) {
            Integer n_rows = static_cast<Integer>(rows.size());

            for (Integer i = 0; i < n_rows; ++i) {
                auto dof_I = rows[i];
                m_matrix.add(dof_I, 0, vec(i));
            }
        }

        template <typename IDX>
        void add(const std::vector<IDX> &rows, std::vector<T> &vec) {
            Integer n_rows = static_cast<Integer>(rows.size());

            for (Integer i = 0; i < n_rows; ++i) {
                auto dof_I = rows[i];
                m_matrix.add(dof_I, 0, vec[i]);
            }
        }

        template <typename IDX, class ElementVector>
        void insert(const std::vector<IDX> &rows, ElementVector &vec) {
            if (use_add) {
                add(rows, vec);
            } else {
                set_non_zero(rows, vec);
            }
        }

        template <typename IDX, class ElementVector>
        void add_tensor_product_idx(const std::vector<IDX> &rows, const int &tensor_dim, ElementVector &vec) {
            Integer n_rows = static_cast<Integer>(rows.size());

            IDX idx = 0;
            for (Integer i = 0; i < n_rows; ++i) {
                auto dof_I = rows[i];
                auto dof_I_x_d = dof_I * tensor_dim;

                for (IDX k = 0; k < tensor_dim; ++k, ++idx) {
                    m_matrix.add(dof_I_x_d + k, 0, vec(idx));
                }
            }
        }

        template <typename IDX, class ElementVector>
        void set_non_zero_tensor_product_idx(const std::vector<IDX> &rows, const int &tensor_dim, ElementVector &vec) {
            Integer n_rows = static_cast<Integer>(rows.size());

            IDX idx = 0;
            for (Integer i = 0; i < n_rows; ++i) {
                auto dof_I = rows[i];
                auto dof_I_x_d = dof_I * tensor_dim;

                for (IDX k = 0; k < tensor_dim; ++k, ++idx) {
                    if (std::abs(vec(idx)) != 0.0) {
                        m_matrix.set(dof_I_x_d + k, 0, vec(idx));
                    }
                }
            }
        }

        template <typename IDX, class ElementVector>
        void insert_tensor_product_idx(const std::vector<IDX> &rows, const int &tensor_dim, ElementVector &vec) {
            if (use_add) {
                add_tensor_product_idx(rows, tensor_dim, vec);
            } else {
                set_non_zero_tensor_product_idx(rows, tensor_dim, vec);
            }
        }

        //////////////////////////////////////////////////////

        template <typename IDX, class ElementMatrix>
        void insert(const moonolith::Storage<IDX> &rows, const moonolith::Storage<IDX> &cols, ElementMatrix &mat) {
            if (use_add) {
                add(rows, cols, mat);
            } else {
                set_non_zero(rows, cols, mat);
            }
        }

        template <typename IDX, class ElementMatrix>
        void set(const moonolith::Storage<IDX> &rows, const moonolith::Storage<IDX> &cols, ElementMatrix &mat) {
            Integer n_rows = static_cast<Integer>(rows.size());
            Integer n_cols = static_cast<Integer>(cols.size());

            for (Integer i = 0; i < n_rows; ++i) {
                auto dof_I = rows[i];
                for (Integer j = 0; j < n_cols; ++j) {
                    auto dof_J = cols[j];
                    m_matrix.set(dof_I, dof_J, mat(i, j));
                }
            }
        }

        template <typename IDX, class ElementMatrix>
        void set_non_zero(const moonolith::Storage<IDX> &rows,
                          const moonolith::Storage<IDX> &cols,
                          ElementMatrix &mat) {
            Integer n_rows = static_cast<Integer>(rows.size());
            Integer n_cols = static_cast<Integer>(cols.size());

            for (Integer i = 0; i < n_rows; ++i) {
                auto dof_I = rows[i];
                for (Integer j = 0; j < n_cols; ++j) {
                    auto dof_J = cols[j];

                    if (std::abs(mat(i, j)) != 0.) {
                        m_matrix.set(dof_I, dof_J, mat(i, j));
                    }
                }
            }
        }

        template <typename IDX, class ElementVector>
        void set(const moonolith::Storage<IDX> &rows, ElementVector &vec) {
            Integer n_rows = static_cast<Integer>(rows.size());

            for (Integer i = 0; i < n_rows; ++i) {
                auto dof_I = rows[i];
                m_matrix.set(dof_I, 0, vec(i));
            }
        }

        template <typename IDX, class ElementVector>
        void set_non_zero(const moonolith::Storage<IDX> &rows, ElementVector &vec) {
            Integer n_rows = static_cast<Integer>(rows.size());

            for (Integer i = 0; i < n_rows; ++i) {
                auto dof_I = rows[i];

                if (std::abs(vec(i)) != 0.) {
                    m_matrix.set(dof_I, 0, vec(i));
                }
            }
        }

        template <typename IDX>
        void set_non_zero(const moonolith::Storage<IDX> &rows, moonolith::Storage<T> &vec) {
            Integer n_rows = static_cast<Integer>(rows.size());

            for (Integer i = 0; i < n_rows; ++i) {
                auto dof_I = rows[i];

                if (std::abs(vec[i]) != 0.) {
                    m_matrix.set(dof_I, 0, vec[i]);
                }
            }
        }

        template <typename IDX, class ElementVector>
        void add(const moonolith::Storage<IDX> &rows, ElementVector &vec) {
            Integer n_rows = static_cast<Integer>(rows.size());

            for (Integer i = 0; i < n_rows; ++i) {
                auto dof_I = rows[i];
                m_matrix.add(dof_I, 0, vec(i));
            }
        }

        template <typename IDX>
        void add(const moonolith::Storage<IDX> &rows, moonolith::Storage<T> &vec) {
            Integer n_rows = static_cast<Integer>(rows.size());

            for (Integer i = 0; i < n_rows; ++i) {
                auto dof_I = rows[i];
                m_matrix.add(dof_I, 0, vec[i]);
            }
        }

        template <typename IDX, class ElementVector>
        void insert(const moonolith::Storage<IDX> &rows, ElementVector &vec) {
            if (use_add) {
                add(rows, vec);
            } else {
                set_non_zero(rows, vec);
            }
        }

        template <typename IDX, class ElementVector>
        void add_tensor_product_idx(const moonolith::Storage<IDX> &rows, const int &tensor_dim, ElementVector &vec) {
            Integer n_rows = static_cast<Integer>(rows.size());

            IDX idx = 0;
            for (Integer i = 0; i < n_rows; ++i) {
                auto dof_I = rows[i];
                auto dof_I_x_d = dof_I * tensor_dim;

                for (int k = 0; k < tensor_dim; ++k, ++idx) {
                    m_matrix.add(dof_I_x_d + k, 0, vec(idx));
                }
            }
        }

        template <typename IDX, class ElementVector>
        void set_non_zero_tensor_product_idx(const moonolith::Storage<IDX> &rows,
                                             const int &tensor_dim,
                                             ElementVector &vec) {
            Integer n_rows = static_cast<Integer>(rows.size());

            IDX idx = 0;
            for (Integer i = 0; i < n_rows; ++i) {
                auto dof_I = rows[i];
                auto dof_I_x_d = dof_I * tensor_dim;

                for (int k = 0; k < tensor_dim; ++k, ++idx) {
                    if (std::abs(vec(idx)) != 0.0) {
                        m_matrix.set(dof_I_x_d + k, 0, vec(idx));
                    }
                }
            }
        }

        template <typename IDX, class ElementVector>
        void insert_tensor_product_idx(const moonolith::Storage<IDX> &rows, const int &tensor_dim, ElementVector &vec) {
            if (use_add) {
                add_tensor_product_idx(rows, tensor_dim, vec);
            } else {
                set_non_zero_tensor_product_idx(rows, tensor_dim, vec);
            }
        }

        template <typename IDX, class ElementVector>
        void add_contiguous(const moonolith::Storage<IDX> &rows, const int &tensor_dim, ElementVector &vec) {
            Integer n_rows = static_cast<Integer>(rows.size());

            IDX idx = 0;
            for (Integer i = 0; i < n_rows; ++i) {
                auto dof_I = rows[i];

                for (int k = 0; k < tensor_dim; ++k, ++idx) {
                    m_matrix.add(dof_I + k, 0, vec(idx));
                }
            }
        }

        template <typename IDX, class ElementVector>
        void set_contiguous(const moonolith::Storage<IDX> &rows, const int &tensor_dim, ElementVector &vec) {
            Integer n_rows = static_cast<Integer>(rows.size());

            IDX idx = 0;
            for (Integer i = 0; i < n_rows; ++i) {
                auto dof_I = rows[i];

                for (int k = 0; k < tensor_dim; ++k, ++idx) {
                    if (std::abs(vec(idx)) != 0.0) {
                        m_matrix.set(dof_I + k, 0, vec(idx));
                    }
                }
            }
        }

        template <typename IDX, class ElementVector>
        void insert_contiguous(const moonolith::Storage<IDX> &rows, const int &tensor_dim, ElementVector &vec) {
            if (use_add) {
                add_contiguous(rows, tensor_dim, vec);
            } else {
                set_contiguous(rows, tensor_dim, vec);
            }
        }

        // void fill(USparseMatrix &mat)
        // {
        //     auto nnz = m_matrix.local_max_entries_x_col();
        //     auto n_local_rows = ownership_ranges_rows[comm.rank() + 1] - ownership_ranges_rows[comm.rank()];
        //     auto n_local_cols = ownership_ranges_cols[comm.rank() + 1] - ownership_ranges_cols[comm.rank()];
        //     mat = local_sparse(n_local_rows, n_local_cols, nnz);

        //     {
        //         utopia::Write<utopia::USparseMatrix> write(mat);
        //         for (auto it = m_matrix.iter(); it; ++it) {
        //             mat.set(it.row(), it.col(), *it);
        //         }
        //     }
        // }

        // void fill(UVector &vec)
        // {
        //     auto n_local_rows = ownership_ranges_rows[comm.rank() + 1] - ownership_ranges_rows[comm.rank()];
        //     vec = local_zeros(n_local_rows);
        //     {
        //         Write<UVector> w_g(vec);

        //         for(auto it = m_matrix.iter(); it; ++it) {
        //             assert(it.col() == 0);
        //             vec.set(it.row(), *it);
        //         }
        //     }
        // }

        // //remove row variants (incomplete intersections)
        // void fill(const std::vector<bool> &remove_row, USparseMatrix &mat)
        // {
        //     auto nnz = m_matrix.local_max_entries_x_col();
        //     auto n_local_rows = ownership_ranges_rows[comm.rank() + 1] - ownership_ranges_rows[comm.rank()];
        //     auto n_local_cols = ownership_ranges_cols[comm.rank() + 1] - ownership_ranges_cols[comm.rank()];
        //     mat = local_sparse(n_local_rows, n_local_cols, nnz);

        //     utopia::Write<utopia::USparseMatrix> write(mat);

        //     for (auto it = m_matrix.iter(); it; ++it) {

        //         const SizeType index = it.row() - ownership_ranges_rows[comm.rank()];
        //         assert(index < remove_row.size());

        //         if(!remove_row[index]) {
        //             mat.set(it.row(), it.col(), *it);
        //         }
        //     }
        // }

        // void fill(const std::vector<bool> &remove_row, UVector &vec)
        // {
        //     auto n_local_rows = ownership_ranges_rows[comm.rank() + 1] - ownership_ranges_rows[comm.rank()];

        //     vec = local_zeros(n_local_rows);

        //     {
        //         Write<UVector> w_g(vec);

        //         for (auto it = m_matrix.iter(); it; ++it) {
        //             const SizeType index = it.row() - ownership_ranges_rows[comm.rank()];
        //             assert(index < remove_row.size());
        //             assert(it.col() == 0);

        //             if(!remove_row[index]) {
        //                 vec.set(it.row(), *it);
        //             }
        //         }
        //     }
        // }

        moonolith::SparseMatrix<T> &get() { return m_matrix; }

        const moonolith::SparseMatrix<T> &get() const { return m_matrix; }

        void clear() {
            m_matrix.clear();
            // TODO
        }

        moonolith::Communicator comm;
        moonolith::SparseMatrix<T> m_matrix;
        moonolith::Redistribute<moonolith::SparseMatrix<T> > redist;
        std::vector<moonolith::Integer> ownership_ranges_rows, ownership_ranges_cols;
        bool use_add;
    };

}  // namespace moonolith

#endif  // MOONOLITH_MATRIX_INSERTER_HPP
