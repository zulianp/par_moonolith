#include "moonolith_intersect_polygons_impl.hpp"

namespace moonolith {

    template class IntersectPolygons<Real, 2>;
    template class IntersectPolygons<Real, 3>;
    template class IntersectConvexPolygons<Real, 2>;
    template class IntersectConvexPolygons<Real, 3>;
    template class RemoveDuplicatePoints<Real, 2>;
    template class RemoveDuplicatePoints<Real, 3>;
    template class SortPolygon<Real, Integer>;

    template bool sorted<Real, Integer>(const Storage<Vector<Real, 3>> &points,
                                        const Vector<Real, 3> &normal,
                                        Integer *index,
                                        const std::size_t n_points);

}  // namespace moonolith
