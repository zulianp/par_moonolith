#ifndef MOONOLITH_PAR_L2_TRANSFER_HPP
#define MOONOLITH_PAR_L2_TRANSFER_HPP

#include "moonolith_duplicate_intersection_avoidance.hpp"
#include "moonolith_many_masters_one_slave_algorithm.hpp"
#include "moonolith_one_master_one_slave_algorithm.hpp"
#include "moonolith_single_collection_one_master_one_slave_algorithm.hpp"
#include "moonolith_sparse_matrix.hpp"
#include "par_moonolith_config.hpp"

#include "moonolith_collection_manager.hpp"
#include "moonolith_function_space.hpp"
#include "moonolith_gauss_quadrature_rule.hpp"
#include "moonolith_incomplete_intersection_remover.hpp"
#include "moonolith_keast_quadrature_rule.hpp"
#include "moonolith_l2_assembler.hpp"
#include "moonolith_matrix_buffers.hpp"
#include "moonolith_matrix_inserter.hpp"

#include <tuple>
#include <vector>

#include <mpi.h>

namespace moonolith {

    template <class MasterElem, class SlaveElem>
    class LocalL2Assembler {
    public:
        using T = typename SlaveElem::T;
        static const int Dim = SlaveElem::Dim;

        using QuadratureT = moonolith::Quadrature<T, Dim>;
        using L2TransferT = moonolith::L2Transfer<MasterElem, SlaveElem>;

        template <class Elem>
        inline static int order_of_quadrature(const Elem &e) {
            if (e.is_simplex()) {
                return e.order();
            } else {
                return e.order() * Dim;
            }
        }

        void init_quadrature();

        template <class ElemAdapter>
        inline bool operator()(const ElemAdapter &master, const ElemAdapter &slave) {
            auto &e_m = master.elem();
            auto &e_s = slave.elem();

            auto &m_m = master.collection();
            auto &m_s = slave.collection();

            auto &dof_obj_s = m_s.dof_map().dof_object(slave.element_handle());

            assert(dof_obj_s.is_valid());
            assert(m_m.dof_map().dof_object(master.element_handle()).is_valid());

            auto &dof_m = m_m.dof_map().dofs(master.element_handle());
            auto &dof_s = dof_obj_s.dofs;

            make(m_m, e_m, master_elem);
            make(m_s, e_s, slave_elem);

            assert(master_elem);
            assert(slave_elem);

            init_quadrature();

            if (algo.assemble(*master_elem, *slave_elem)) {
                const auto &B_e = algo.coupling_matrix();
                const auto &D_e = algo.mass_matrix();

                buffers.B.insert(dof_s, dof_m, B_e);
                buffers.D.insert(dof_s, dof_s, D_e);
                buffers.measure().insert(dof_obj_s.element_dof, D_e.sum());

                if (dual_lagrange_multiplier()) {
                    const auto &Q_e = algo.transformation();
                    buffers.Q.insert(dof_s, dof_s, Q_e);
                }

                return true;
            } else {
                return false;
            }
        }

        void clear();

        LocalL2Assembler(MatrixBuffers<T> &buffers);

        void dual_lagrange_multiplier(const bool val);

        inline bool dual_lagrange_multiplier() const;

        L2TransferT algo;

    private:
        QuadratureT q_rule;
        std::shared_ptr<MasterElem> master_elem;
        std::shared_ptr<SlaveElem> slave_elem;
        MatrixBuffers<T> &buffers;
        int current_quadrature_order_;
    };

    template <typename T, Integer PhysicalDim, Integer MasterDim = PhysicalDim, Integer SlaveDim = PhysicalDim>
    class ParL2Transfer {
    public:
        using MeshT = moonolith::Mesh<T, PhysicalDim>;
        using SpaceT = moonolith::FunctionSpace<MeshT>;

        using SpaceAlgorithmT = moonolith::OneMasterOneSlaveAlgorithm<PhysicalDim, SpaceT>;
        using SingleSpaceAlgorithmT = moonolith::SingleCollectionOneMasterOneSlaveAlgorithm<PhysicalDim, SpaceT>;

        using ElemAdapter = typename SpaceAlgorithmT::Adapter;
        using SingleElemAdapter = typename SingleSpaceAlgorithmT::Adapter;

        using MasterElem = moonolith::Elem<T, MasterDim, PhysicalDim>;
        using SlaveElem = moonolith::Elem<T, SlaveDim, PhysicalDim>;

        ParL2Transfer(Communicator &comm);

        void post_process(const SpaceT &master, const SpaceT &slave);

        bool assemble(const SpaceT &master, const SpaceT &slave, const T box_tol = 0.0);

        bool assemble(const SpaceT &master,
                      const SpaceT &slave,
                      const std::vector<std::pair<int, int>> &tags,
                      const T box_tol = 0.0);

        bool assemble(const SpaceT &space, const std::vector<std::pair<int, int>> &tags, const T box_tol = 0.0);

        inline void remove_incomplete_intersections(const bool val) { remove_incomplete_intersections_ = val; }
        inline void use_reference_frame(const bool use_reference_frame) {
            local_assembler.algo.use_reference_frame(use_reference_frame);
        }

        void dual_lagrange_multiplier(const bool val);
        bool dual_lagrange_multiplier() const;

        inline const SparseMatrix<T> &coupling_matrix() const { return buffers.B.get(); }

        inline const SparseMatrix<T> &mass_matrix() const { return buffers.D.get(); }

        inline const SparseMatrix<T> &transformation_matrix() const { return buffers.Q.get(); }

        Communicator &comm;
        MatrixBuffers<T> buffers;
        LocalL2Assembler<MasterElem, SlaveElem> local_assembler;

    private:
        bool remove_incomplete_intersections_;
    };

}  // namespace moonolith

#endif  // MOONOLITH_PAR_L2_TRANSFER_HPP
