#ifndef MOONOLITH_CARTESIAN_TOPOLOGY_HPP
#define MOONOLITH_CARTESIAN_TOPOLOGY_HPP

#include "moonolith_communicator.hpp"
#include "moonolith_describable.hpp"

namespace moonolith {

    template <int Dimension>
    class CartesianTopology : public moonolith::Describable {
    public:
        void describe(std::ostream &os) const override {
            int r = rank();
            os << "[" << r << "]\n";
            os << "NDimensions: " << Dimension << "\n";
            std::array<int, Dimension> result;

            if (coords(r, result)) {
                os << "coords[" << result << "]\n";
            }
        }

        inline int rank() const {
            int r = -2;
            MPI_Comm_rank(comm_cart_, &r);
            return r;
        }

        bool create(const Communicator &comm, const std::array<int, Dimension> &dims, const bool periodic) {
            std::array<int, Dimension> periods;
            std::fill(periods.begin(), periods.end(), periodic);
            // const_cast<int *> for retro-compatibility
            bool ok = handle_error(
                MPI_Cart_create(comm.get(), dims.size(), const_cast<int *>(&dims[0]), &periods[0], false, &comm_cart_));
            assert(ok);
            return ok;
        }

        int shift(int direction, int disp) {
            int rank = -2;
            bool ok = handle_error(MPI_Comm_rank(comm_cart_, &rank));
            assert(ok);
            if (!ok) return MPI_PROC_NULL;
            return shift(direction, disp, rank);
        }

        int shift(int direction, int disp, int source) {
            int destination = -2;
            bool ok = handle_error(MPI_Cart_shift(comm_cart_, direction, disp, &source, &destination));
            assert(ok);
            if (!ok) return MPI_PROC_NULL;
            return destination;
        }

        bool coords(int rank, std::array<int, Dimension> &result) const {
            bool ok = handle_error(MPI_Cart_coords(comm_cart_, rank, result.size(), &result[0]));
            if (!ok) {
                std::cout << result << std::endl;
            }
            assert(ok);
            return ok;
        }

        int rank(std::array<int, Dimension> &coords) {
            int ret = -2;
            bool ok = handle_error(MPI_Cart_rank(comm_cart_, &coords[0], &ret));
            assert(ok);
            MOONOLITH_UNUSED(ok);
            return ret;
        }

        CartesianTopology() {}

        bool handle_error(const int code) const {
            switch (code) {
                case MPI_SUCCESS: {
                    return true;
                }
                case MPI_ERR_TOPOLOGY: {
                    std::cerr << "[Error] CartesianTopology: MPI_ERR_TOPOLOGY" << std::endl;
                    return false;
                }
                case MPI_ERR_DIMS: {
                    std::cerr << "[Error] CartesianTopology: MPI_ERR_DIMS" << std::endl;
                    return false;
                }
                case MPI_ERR_ARG: {
                    std::cerr << "[Error] CartesianTopology: MPI_ERR_ARG" << std::endl;
                    return false;
                }
                case MPI_ERR_COMM: {
                    std::cerr << "[Error] CartesianTopology: MPI_ERR_ARG" << std::endl;
                    return false;
                }
                default:
                    break;
            }

            std::cerr << "[Error] CartesianTopology: Unknown error code: " << code << std::endl;
            return false;
        }

    private:
        MPI_Comm comm_cart_;
    };
}  // namespace moonolith

#endif  // MOONOLITH_CARTESIAN_TOPOLOGY_HPP
