#ifndef MOONOLITH_ELEM_QUAD_1_HPP
#define MOONOLITH_ELEM_QUAD_1_HPP

#include "moonolith_elem_quad.hpp"
#include "moonolith_elem_quad4.hpp"

namespace moonolith {

    template <typename T_, int PhysicalDim_ = 2>
    class Quad1 final : public Quad<T_, 0, PhysicalDim_> {
    public:
        static const int Order = 1;
        static const int Dim = 2;
        static const int PhysicalDim = PhysicalDim_;
        static const int NNodes = 1;

        using T = T_;

        using Vector = moonolith::Vector<T, Dim>;
        using CoVector = moonolith::Vector<T, PhysicalDim>;

        using Point = moonolith::Vector<T, Dim>;
        using CoPoint = moonolith::Vector<T, PhysicalDim>;

        using Super = moonolith::Quad<T_, 0, PhysicalDim_>;
        using Polygon = typename Super::Polygon;

    private:
        class Fun final {
        public:
            Fun() {
                f[0] = [](const Point &) -> T { return 1.0; };
            }

            std::array<std::function<T(const Point &)>, NNodes> f;
        };

        class Grad final {
        public:
            Grad() {
                f[0] = [](const Point &, Vector &g) {
                    g.x = 0.0;
                    g.y = 0.0;
                };
            }

            std::array<std::function<void(const Point &, Vector &)>, NNodes> f;
        };

    public:
        T fun(const Integer i, const Point &p) const override { return fun_.f[i](p); }

        void grad(const Integer i, const Point &p, Vector &g) const override { grad_.f[i](p, g); }

        void hessian(const Integer /*i*/, const Point & /*p*/, std::array<T, Dim * Dim> & /*H*/) {
            // hessian_.f[i](p, H);
            assert(false);
        }

        void jacobian(const Point &p, std::array<T, PhysicalDim * Dim> &J) const override {
            (void)p;
            affine_approx_jacobian(J);
        }

        void affine_approx_jacobian(std::array<T, PhysicalDim * Dim> &J) const override {
            const auto &p0 = points_[0];

            for (int i = 0; i < PhysicalDim; ++i) {
                const int i_offset = i * Dim;
                J[i_offset] = points_[1][i] - p0[i];
                J[i_offset + 1] = points_[3][i] - p0[i];
            }
        }

        CoPoint &node(const Integer i) override { return nodes_[i]; }

        const CoPoint &node(const Integer i) const override { return nodes_[i]; }

        void point(const Point &p, CoPoint &q) const override { Affine<Quad1>::point(points_, {0, 1, 3}, p, q); }

        Quad1() : is_affine_(false) {}

        bool is_affine() const override { return is_affine_; }
        inline int order() const override { return Order; }

        void set_affine(const bool val) { is_affine_ = val; }

        inline void make_reference() {
            Reference<Quad1>::points(nodes_);
            Reference<Quad4<T, PhysicalDim>>::points(points_);
            set_affine(true);
        }

        inline int n_nodes() const override { return NNodes; }

        inline const std::array<CoPoint, 4> &points() const { return points_; }

        inline CoPoint &point(const Integer idx) { return points_[idx]; }

        inline const CoPoint &point(const Integer idx) const { return points_[idx]; }

        inline ElemType type() const override { return QUAD1; }

        inline bool is_simplex() const override { return false; }

        inline T measure() const override { return trapezoid_area(points_[0], points_[1], points_[3]); }

        inline T reference_measure() const override { return 1.0; }

        inline void make(Polygon &poly) const override {
            poly.resize(4);

            for (int i = 0; i < 4; ++i) {
                poly[i] = point(i);
            }
        }

    private:
        std::array<CoPoint, NNodes> nodes_;
        std::array<CoPoint, 4> points_;

        const Fun fun_;
        const Grad grad_;
        bool is_affine_;
    };

    template <typename T, int PhysicalDim>
    class Reference<Quad1<T, PhysicalDim>> {
    public:
        static const int NNodes = 1;

        template <class Point, std::size_t Size>
        static void points(std::array<Point, Size> &nodes) {
            static_assert(Size >= NNodes, "size must be at least 4");
            // p0
            nodes[0].x = 0.5;
            nodes[0].y = 0.5;
        }

        inline static T measure() { return 1.0; }
    };

    template <typename T, int PhysicalDim>
    class GaussQRule<Quad1<T, PhysicalDim>> {
    public:
        static bool get(const Integer order, Quadrature2<T> &q) { return Gauss::Quad::get(order, q); }
    };

}  // namespace moonolith

#endif  // MOONOLITH_ELEM_QUAD_1_HPP
