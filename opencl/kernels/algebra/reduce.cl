#ifndef REDUCE_CL
#define REDUCE_CL

#include "algebra_interfaces.cl"
#include "kernel_base.cl"

void reduce_add_private_values(const SizeType n_private_values,
                               const Scalar *private_values,
                               m_local__ Scalar *local_work,
                               m_global__ Scalar *result) {
    Scalar private_value = 0;
    for (SizeType i = 0; i < n_private_values; ++i) {
        private_value += private_values[i];
    }

    const SizeType l = get_local_id(0);
    const SizeType localSize = get_local_size(0);
    SizeType offset = localSize >> 1;

    local_work[l] = private_value;
    for (SizeType k = 0; offset > 0; ++k) {
        Scalar val = 0;
        SizeType index = k + offset;
        barrier(CLK_LOCAL_MEM_FENCE);
        if (index < localSize) {
            val = local_work[index];
        }
        barrier(CLK_LOCAL_MEM_FENCE);

        local_work[l] += val;
        offset >>= 1;
    }

    if (l == 0) result[get_group_id(0)] = local_work[0];
}

m_kernel__ void reduce_add(const SizeType n_values,
                           const m_global__ Scalar *values,
                           m_global__ Scalar *result,
                           m_local__ Scalar *local_work) {
    Scalar private_value = 0;
    for (SizeType i = get_local_id(0); i < n_values; i += get_global_size(0)) {
        private_value += values[i];
    }

    reduce_add_private_values(1, &private_value, local_work, result);
}

#endif  // REDUCE_CL