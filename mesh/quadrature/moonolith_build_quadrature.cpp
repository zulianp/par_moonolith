#include "moonolith_build_quadrature.hpp"
#include "moonolith_intersect_polyhedra_impl.hpp"
#include "moonolith_map_quadrature_impl.hpp"

#include "moonolith_intersect_polyhedra_impl.hpp"

namespace moonolith {

    template class BuildQuadrature<Polygon<Real, 2>, Polygon<Real, 2>>;
    template class BuildQuadrature<Polygon<Real, 3>, Polygon<Real, 3>>;
    template class BuildQuadrature<Polyhedron<Real>, Polyhedron<Real>>;
    template class BuildQuadrature<Line<Real, 2>, Polygon<Real, 2>>;
    template class BuildQuadrature<Polygon<Real, 2>, Line<Real, 2>>;
    template class BuildQuadrature<Polygon<Real, 3>, Polyhedron<Real>>;
    template class BuildQuadrature<Polyhedron<Real>, Polygon<Real, 3>>;
    template class BuildQuadrature<Polyhedron4<Real>>;

}  // namespace moonolith
