#ifndef MOONOLITH_SINGLE_COLLECTION_ONE_MASTER_ONE_SLAVE_ALGORITHM_HPP
#define MOONOLITH_SINGLE_COLLECTION_ONE_MASTER_ONE_SLAVE_ALGORITHM_HPP

#include "moonolith_aabb.hpp"
#include "moonolith_asynch_lookup_table_builder.hpp"
#include "moonolith_bounding_volume_with_span.hpp"
#include "moonolith_check_can_refine.hpp"
#include "moonolith_collection_traits.hpp"
#include "moonolith_communicator.hpp"
#include "moonolith_describe.hpp"
#include "moonolith_exchange.hpp"
#include "moonolith_lookup_table_builder.hpp"
#include "moonolith_n_tree_bound_synchronize.hpp"
#include "moonolith_n_tree_mutator_factory.hpp"
#include "moonolith_n_tree_with_span_mutator_factory.hpp"
#include "moonolith_n_tree_with_tags_mutator_factory.hpp"
#include "moonolith_tree.hpp"

#include "moonolith_check_stream.hpp"
#include "moonolith_element_adapter.hpp"
#include "moonolith_make_unique.hpp"
#include "moonolith_polygon.hpp"
#include "moonolith_predicate.hpp"
#include "moonolith_search_radius.hpp"
#include "par_moonolith.hpp"

#include <algorithm>
#include <cassert>
#include <map>
#include <numeric>

namespace moonolith {

    // FIXME add concept of global handle and local handle
    template <Integer Dimension_, class Collection>
    class SingleCollectionOneMasterOneSlaveAlgorithm {
    public:
        typedef moonolith::AABBWithKDOPSpan<Dimension_, Real> Bound;
        using CM = moonolith::CollectionManager<Collection>;
        using Elem = typename CM::Elem;
        using Adapter = moonolith::ElementAdapter<Bound, Collection>;

        struct Traits {
            enum { Dimension = Dimension_ };

            typedef moonolith::AABBWithKDOPSpan<Dimension, Real> Bound;
            typedef Adapter DataType;
        };

        class SingleCollectionTree : public Tree<Traits> {
        public:
            static std::shared_ptr<SingleCollectionTree> New(const std::shared_ptr<Predicate> &pred) {
                std::shared_ptr<SingleCollectionTree> ret = std::make_shared<SingleCollectionTree>();
                // std::shared_ptr< NTreeWithSpanMutatorFactory<SingleCollectionTree> > factory = std::make_shared<
                // NTreeWithSpanMutatorFactory<SingleCollectionTree> >();
                std::shared_ptr<NTreeWithTagsMutatorFactory<SingleCollectionTree>> factory =
                    std::make_shared<NTreeWithTagsMutatorFactory<SingleCollectionTree>>(pred);

                factory->set_refine_params(20, 6);
                ret->set_mutator_factory(factory);
                return ret;
            }

            SingleCollectionTree() {}
        };

        using DataContainer = typename SingleCollectionTree::DataContainer;

        class SerializerDeserializer {
        public:
            SerializerDeserializer(const Communicator &comm,
                                   const std::shared_ptr<CollectionManager<Collection>> &cm,
                                   const std::shared_ptr<const Collection> &local_collection,
                                   const SearchRadius<Real> &search_radius)
                : comm(comm), cm(cm), search_radius(search_radius), local_collection(local_collection) {}

            Communicator comm;
            std::shared_ptr<CollectionManager<Collection>> cm;
            const SearchRadius<Real> &search_radius;
            std::shared_ptr<const Collection> local_collection;
            std::map<Integer, std::shared_ptr<const Collection>> collections;
            std::map<Integer, std::vector<std::shared_ptr<const Collection>>> migrated_collections;

            void read(const Integer ownerrank,
                      const Integer /*senderrank*/,
                      bool is_forwarding,
                      DataContainer &data,
                      InputStream &in) {
                CHECK_STREAM_READ_BEGIN("serialize_deserialize", in);

                std::shared_ptr<const Collection> coll = cm->build(in);

                if (!is_forwarding) {
                    assert(!collections[ownerrank]);
                    collections[ownerrank] = coll;
                } else {
                    migrated_collections[ownerrank].push_back(coll);
                }

                const Integer n_new_adapters = cm->n_elements(*coll);
                data.reserve(data.size() + n_new_adapters);

                auto e_begin = cm->elements_begin(*coll);
                auto e_end = cm->elements_end(*coll);

                //!!! libmesh might create problems in this context
                Integer idx = 0;
                for (auto it = e_begin; it != e_end; ++it, ++idx) {
                    auto tag = cm->tag(*coll, it);

                    data.push_back(Adapter(&(*coll), cm->handle(*coll, it), idx, tag, search_radius.get(tag)));
                    cm->data_added(idx, data.back());
                }

                CHECK_STREAM_READ_END("serialize_deserialize", in);
            }

            void write(const Integer ownerrank,
                       const Integer /*recvrank*/,
                       const std::vector<Integer>::const_iterator &begin,
                       const std::vector<Integer>::const_iterator &end,
                       const DataContainer & /*data*/,
                       OutputStream &out) {
                CHECK_STREAM_WRITE_BEGIN("serialize_deserialize", out);

                if (ownerrank == comm.rank()) {
                    cm->serialize(*local_collection, begin, end, out);
                } else {
                    auto it = collections.find(ownerrank);
                    assert(it != collections.end());
                    assert(std::distance(begin, end) > 0);
                    cm->serialize(*it->second, begin, end, out);
                }

                CHECK_STREAM_WRITE_END("serialize_deserialize", out);
            }
        };

        void add_master_and_slave_pairing(const Integer master, const Integer slave) { predicate->add(master, slave); }

        DataHandle add_elem(const Adapter &a) { return tree->insert(a); }

        DataHandle add_elem(const Collection *collection,
                            const Integer element_handle,
                            const Integer handle,
                            const Integer tag,
                            const Real blow_up) {
            return tree->insert(Adapter(collection, element_handle, handle, tag, blow_up));
        }

        bool compute(const std::function<bool(const Adapter &, const Adapter &)> &f) {
            SerializerDeserializer serializer_deserializer(comm, cm, local_collection, *search_radius_);

            auto read = [&serializer_deserializer](const Integer ownerrank,
                                                   const Integer senderrank,
                                                   bool is_forwarding,
                                                   DataContainer &data,
                                                   InputStream &in) {
                serializer_deserializer.read(ownerrank, senderrank, is_forwarding, data, in);
            };

            auto write = [&serializer_deserializer](const Integer ownerrank,
                                                    const Integer recvrank,
                                                    const std::vector<Integer>::const_iterator &begin,
                                                    const std::vector<Integer>::const_iterator &end,
                                                    const DataContainer &data,
                                                    OutputStream &out) {
                serializer_deserializer.write(ownerrank, recvrank, begin, end, data, out);
            };

            Integer found_matches = 0, false_positives = 0;

            auto fun = [&](Adapter &master, Adapter &slave) -> bool {
                if (f(master, slave)) {
                    found_matches++;
                    return true;
                } else {
                    false_positives++;
                    return false;
                }
            };

            moonolith::search_and_compute(comm, tree, predicate, read, write, fun, settings);

            if (Moonolith::instance().verbose()) {
                logger() << "candidates: " << (found_matches + false_positives) << " matches: " << found_matches
                         << " false positives: " << false_positives << std::endl;
            }

            return found_matches > 0;
        }

        bool init(const Collection &collection,
                  const std::vector<std::pair<int, int>> &master_slave_tags,
                  const Real blow_up) {
            return init(collection, master_slave_tags, std::make_shared<SearchRadius<Real>>(blow_up));
        }

        bool init(const Collection &collection,
                  const std::vector<std::pair<int, int>> &master_slave_tags,
                  const std::shared_ptr<SearchRadius<Real>> &search_radius) {
            search_radius_ = search_radius;

            for (const auto &mst : master_slave_tags) {
                add_master_and_slave_pairing(mst.first, mst.second);
            }

            local_collection = moonolith::make_ref(collection);

            auto collection_end = cm->elements_end(collection);
            auto collection_begin = cm->elements_begin(collection);

            tree->reserve(cm->n_elements(collection));

            Integer idx = 0;
            for (auto it = collection_begin; it != collection_end; ++it, ++idx) {
                if (cm->skip(collection, it)) continue;

                const auto tag = cm->tag(collection, it);
                if (predicate->select(tag)) {
                    auto data_handle =
                        add_elem(&collection, cm->handle(collection, it), idx, tag, search_radius_->get(tag));
                    cm->data_added(idx, (*tree)[data_handle]);
                }
            }

            //////////////////////////////////////////////////////
            //////////////////////////////////////////////////////

            tree->root()->bound().static_bound().enlarge(1e-8);
            return tree->memory().n_data() > 0;
        }

        SingleCollectionOneMasterOneSlaveAlgorithm(Communicator &comm,
                                                   const std::shared_ptr<CollectionManager<Collection>> &cm,
                                                   const SearchSettings &settings = SearchSettings())
            : comm(comm), predicate(std::make_shared<moonolith::MasterAndSlave>()), cm(cm), settings(settings) {
            tree = SingleCollectionTree::New(predicate);
        }

    private:
        Communicator comm;
        std::shared_ptr<SingleCollectionTree> tree;
        std::shared_ptr<moonolith::MasterAndSlave> predicate;
        std::shared_ptr<const Collection> local_collection;
        std::shared_ptr<CollectionManager<Collection>> cm;
        SearchSettings settings;
        std::shared_ptr<SearchRadius<Real>> search_radius_;
    };
}  // namespace moonolith

#endif  // MOONOLITH_SINGLE_COLLECTION_ONE_MASTER_ONE_SLAVE_ALGORITHM_HPP
