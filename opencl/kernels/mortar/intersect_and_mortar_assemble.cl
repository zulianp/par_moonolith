#ifndef INTERSECT_AND_MORTAR_ASSEMBLE_CL
#define INTERSECT_AND_MORTAR_ASSEMBLE_CL 

#include "kernel_base.cl"
#include "fe/fe_interfaces.cl"
#include "mortar_interfaces.cl"
#include "volume_interface.cl"
#include "make_quadrature_from_poly_interface.cl"
#include "assembly/assemble_interfaces.cl"
#include "assembly/quadrature.cl"

//if not defined from outside
#ifndef mortar_assemble
#define mortar_assemble(macro_master, macro_slave, macro_matrix) { mass_matrix_assemble_local(macro_master, macro_slave, macro_matrix); }
#endif 

#ifndef mortar_quad_order
#define mortar_quad_order(macro_order_master, macro_order_slave) ((macro_order_master) + (macro_order_slave))
#endif

typedef struct {
	//Elements
	FEObject fe_master;
	FEObject fe_slave;

	//Intersection data

	//3d
	PMesh poly_master, poly_slave, intersection;

	//2d
	Scalar el_points_master 	[MAX_SHAPE_FUNCS * MAX_N_DIMS];
	Scalar el_points_slave 		[MAX_SHAPE_FUNCS * MAX_N_DIMS];
	Scalar polygon_intersection [MAX_N_ISECT_POINTS * MAX_N_DIMS];

	/////memory buffers

	//quadrature rule 
	Scalar ref_quad_points [MAX_QUAD_POINTS * MAX_N_DIMS];
	Scalar ref_quad_weights[MAX_QUAD_POINTS];

	//global quadrature points
	Scalar quad_points     [MAX_QUAD_POINTS * MAX_N_DIMS];
	Scalar quad_weights    [MAX_QUAD_POINTS];

	//barycenter for meshing
	Scalar barycenter_p	   [MAX_N_DIMS];

	//buffers for storing the results
	Scalar local_matrix_delta[MAX_SHAPE_FUNCS * MAX_SHAPE_FUNCS * MAX_CODIM * MAX_CODIM];
	Scalar local_matrix 	 [MAX_SHAPE_FUNCS * MAX_SHAPE_FUNCS * MAX_CODIM * MAX_CODIM];

} MortarWorkspace;

#ifndef CLIPP_HOST_CL
//result is written in w->local_matrix
SizeType intersect_and_mortar_assemble(const Mesh mesh_master, const SizeType element_master, const SizeType order_master, const SizeType n_codims_master,
								       const Mesh mesh_slave,  const SizeType element_slave,  const SizeType order_slave,  const SizeType n_codims_slave,
									   MortarWorkspace *w);

SizeType intersect_and_mortar_assemble_2(const Mesh mesh_master, const SizeType element_master, const SizeType order_master, const SizeType n_codims_master,
								         const Mesh mesh_slave,  const SizeType element_slave,  const SizeType order_slave,  const SizeType n_codims_slave,
									     MortarWorkspace *w);

SizeType intersect_and_mortar_assemble_3(const Mesh mesh_master, const SizeType element_master, const SizeType order_master, const SizeType n_codims_master,
								         const Mesh mesh_slave,  const SizeType element_slave,  const SizeType order_slave,  const SizeType n_codims_slave,
									     MortarWorkspace *w);
#endif


SizeType intersect_and_mortar_assemble(const Mesh mesh_master, const SizeType element_master, const SizeType order_master, const SizeType n_codims_master,
								       const Mesh mesh_slave,  const SizeType element_slave,  const SizeType order_slave,  const SizeType n_codims_slave,
									   MortarWorkspace *w)
{
	switch(mesh_master.n_dims) {
		case 2: 
		{
			return intersect_and_mortar_assemble_2(
									 mesh_master, element_master, order_master, n_codims_master,
							         mesh_slave,  element_slave,  order_slave,  n_codims_slave,
								 	 w);
		}

		case 3:
		{
			return intersect_and_mortar_assemble_3(
									 mesh_master, element_master, order_master, n_codims_master,
							         mesh_slave,  element_slave,  order_slave,  n_codims_slave,
								 	 w);
		}

		default:
		{
			KERNEL_ASSERT(false, "n_dims not supported");
			return 0;
		}
	}
}


SizeType intersect_and_mortar_assemble_2(const Mesh mesh_master, const SizeType element_master, const SizeType order_master, const SizeType n_codims_master,
								         const Mesh mesh_slave,  const SizeType element_slave,  const SizeType order_slave,  const SizeType n_codims_slave,
									     MortarWorkspace *w)
 {
 	SizeType n_intersection_points = 0;

 	const SizeType n_points_master = mesh_n_points(mesh_master, element_master);
 	mesh_points(mesh_master, element_master, w->el_points_master);

 	const SizeType n_points_slave = mesh_n_points(mesh_slave, element_slave);
 	mesh_points(mesh_slave, element_slave, w->el_points_slave);

 	KERNEL_ASSERT( polygon_area_2(n_points_master, w->el_points_master) > 0, "element_master: does not have positive area");
 	KERNEL_ASSERT( polygon_area_2(n_points_slave,  w->el_points_slave)  > 0, "element_slave: does not have positive area");

 	if(intersect_convex_polygons(n_points_master, w->el_points_master, n_points_slave, w->el_points_slave, &n_intersection_points, w->polygon_intersection, DEFAULT_TOLLERANCE)) {

 		KERNEL_ASSERT( polygon_area_2(n_intersection_points, w->polygon_intersection) > 0, "w->intersection area should be positive");

 		const SizeType quad_order = mortar_quad_order(order_slave, order_master); 

 		const SizeType n_quad_points = make_quadrature_points_from_polygon_2(n_intersection_points, w->polygon_intersection, 
 			1.0/polygon_area_2(n_points_slave, w->el_points_slave), 
 			quad_order, w->quad_points, w->quad_weights);
 		
 		make_fe_object_from_global_quad_2(mesh_master, element_master, n_quad_points, w->quad_points, w->quad_weights, &w->fe_master); 
 		make_fe_object_from_global_quad_2(mesh_slave,  element_slave,  n_quad_points, w->quad_points, w->quad_weights, &w->fe_slave);  

 		mortar_assemble(&w->fe_master, &w->fe_slave, w->local_matrix);

 		return w->fe_slave.n_shape_functions * w->fe_master.n_shape_functions * n_codims_slave * n_codims_master;
 	}

 	return 0;
 }


SizeType intersect_and_mortar_assemble_3(const Mesh mesh_master, const SizeType element_master, const SizeType order_master, const SizeType n_codims_master,
								         const Mesh mesh_slave,  const SizeType element_slave,  const SizeType order_slave,  const SizeType n_codims_slave,
									 	 MortarWorkspace *w)
{
	if(!make_polyhedron_from_element(mesh_master, element_master, &w->poly_master)) {
		KERNEL_ASSERT(false, "assemble_coupling_operator returned false");
		return 0;
	}

	if(!make_polyhedron_from_element(mesh_slave, element_slave, &w->poly_slave)) {
		KERNEL_ASSERT(false, "assemble_coupling_operator returned false");
		return 0;
	}

	if(!intersect_convex_polyhedra(w->poly_master, w->poly_slave, &w->intersection)) return 0;

	const SizeType quad_order 	  	   = mortar_quad_order(order_slave, order_master); 
	const SizeType n_sub_elements 	   = n_volume_elements(w->intersection);
	const SizeType rule_index 	  	   = find_compatible_quad_rule_3(max_order_for_tetrahedron_rules, tetrahedron_rules, quad_order);
	const SizeType ref_n_quad_points   = tetrahedron_rules[rule_index].size;
	const SizeType total_n_quad_points = n_sub_elements * ref_n_quad_points;

	KERNEL_ASSERT(ref_n_quad_points > 0, "make_quadrature_points_from_w->intersection: n_quad_points > 0");

	const Scalar inv_volume_slave = 1.0/p_mesh_volume_3(w->poly_slave);

	if(total_n_quad_points <= MAX_QUAD_POINTS) {

		const SizeType n_quad_points = make_quadrature_points_from_polyhedron(w->intersection, 
			inv_volume_slave, 
			rule_index, 
			w->quad_points, 
			w->quad_weights);

		make_fe_object_from_global_quad_3(mesh_master, element_master, n_quad_points, w->quad_points, w->quad_weights, &w->fe_master); 
		make_fe_object_from_global_quad_3(mesh_slave,  element_slave,  n_quad_points, w->quad_points, w->quad_weights, &w->fe_slave);  
		mortar_assemble(&w->fe_master, &w->fe_slave, w->local_matrix);

		const SizeType n_entries = w->fe_slave.n_shape_functions * w->fe_master.n_shape_functions * n_codims_slave * n_codims_master;
		
		KERNEL_ASSERT(!has_nan_entries(n_entries, w->local_matrix), "nan for pair");
		return n_entries;

	} else {

		generic_copy(ref_n_quad_points * 3, tetrahedron_rules[rule_index].points,  w->ref_quad_points);
		generic_copy(ref_n_quad_points,     tetrahedron_rules[rule_index].weights, w->ref_quad_weights);

		row_average( w->intersection.n_nodes, w->intersection.n_dims, w->intersection.points, w->barycenter_p);

		const SizeType max_n_sub_els = max_n_elements_from_facets(w->intersection);
		const SizeType max_n_sub_inc = max(1, MAX_QUAD_POINTS / (max_n_sub_els * ref_n_quad_points));

		SizeType n_entries = 0;
		for(SizeType begin_k = 0; begin_k < w->intersection.n_elements;) {							
			const SizeType end_k = min(begin_k + max_n_sub_inc, w->intersection.n_elements);
			KERNEL_ASSERT(end_k > begin_k, "end_k > begin_k");

			const SizeType n_quad_points = make_quadrature_points_from_polyhedron_in_range_around_point(
				w->intersection, 
				begin_k,
				end_k,
				inv_volume_slave, 
				ref_n_quad_points,
				w->ref_quad_points,
				w->ref_quad_weights,
				w->barycenter_p,
				w->quad_points, 
				w->quad_weights);

			make_fe_object_from_global_quad_3(mesh_master, element_master, n_quad_points, w->quad_points, w->quad_weights, &w->fe_master); 
			make_fe_object_from_global_quad_3(mesh_slave,  element_slave,  n_quad_points, w->quad_points, w->quad_weights, &w->fe_slave);  

			mortar_assemble(&w->fe_master, &w->fe_slave, w->local_matrix_delta);

			if(begin_k == 0) { 
				n_entries = w->fe_slave.n_shape_functions * w->fe_master.n_shape_functions * n_codims_slave * n_codims_master;
				set(n_entries, 0.0, w->local_matrix);
			}

			axpy(n_entries, 1, w->local_matrix_delta, w->local_matrix);

			begin_k = end_k;
		}

		KERNEL_ASSERT(!has_nan_entries(n_entries, w->local_matrix), "nan for pair");
		return n_entries;
	}

	return 0;
}

#endif //INTERSECT_AND_MORTAR_ASSEMBLE_CL
