#ifndef MOONOLITH_PROJECT_CONVEX_POLYGONS_HPP
#define MOONOLITH_PROJECT_CONVEX_POLYGONS_HPP

#include "moonolith_plane.hpp"
#include "moonolith_polygon.hpp"
#include "moonolith_householder.hpp"
#include "moonolith_intersect_polygons.hpp"
#include "moonolith_triangulate.hpp"
#include "moonolith_map_quadrature.hpp"
#include "moonolith_affine_transform.hpp"

namespace moonolith {
	template<typename T>
	class ProjectConvexPolygons {
	public:
		using Polygon2 = moonolith::Polygon<T, 2>;
		using Vector2  = typename Polygon2::Vector;

		using Polygon3 = moonolith::Polygon<T, 3>;
		using Vector3  = typename Polygon3::Vector;
		using Plane3   = moonolith::Plane<T, 3>;

		ProjectConvexPolygons(const T &accept_angle = -0.5)
		: accept_angle(accept_angle)
		{}

		//onto general plane
		bool project_intersect_and_map_quadrature(
			const Polygon3 &poly1,
			const Polygon3 &poly2,
			const Plane3   &plane,
			//ref-quad-rule
			const Quadrature2<T>  &ref_q,
			Quadrature3<T>        &plane_q
		);

		//using polygon plane
		bool project_intersect_and_map_quadrature(
			const Polygon3 &projected,
			const Polygon3 &onto,
			//ref-quad-rule
			const Quadrature2<T>  &q_ref,
			Quadrature3<T>  &projected_q,
			Quadrature3<T>  &onto_q
		);

        inline const Vector3 &normal() const
        {
            return onto_normal;
        }

	private:
		T accept_angle;
		AffineTransform<T, 3> trafo;
		HouseholderTransformation<T, 3> householder_trafo;
		Polygon2 isect, ref_poly_onto, ref_poly_projected;
		Polygon3 isect_onto, isect_projected;
		IntersectConvexPolygons<T, 2> isect_convex_poly;
		IntersectPolygons<T, 2> isect_poly;
		Vector3 u, v, onto_normal, projected_normal;
		Plane3 projected_plane;
	};

}

#endif //MOONOLITH_PROJECT_CONVEX_POLYGONS_HPP
