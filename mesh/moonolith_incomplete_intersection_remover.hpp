#ifndef UTOPIA_INCOMPLETE_INTERSECTION_REMOVER_HPP
#define UTOPIA_INCOMPLETE_INTERSECTION_REMOVER_HPP

#include <list>
#include "moonolith_communicator.hpp"
#include "moonolith_expanding_array.hpp"
#include "moonolith_matrix_inserter.hpp"

namespace moonolith {

    template <typename T>
    class IncompleteIntersectionRemover {
    public:
        IncompleteIntersectionRemover(Communicator &comm, const T tol = 1e-2) : measure(comm), tol(tol) {}

        inline void clear() {
            measure.clear();
            is_measure_complete.clear();
            true_measure.clear();
            touched.clear();
            has_incomplete = false;
            to_remove.clear();
        }

        template <class SpaceT>
        void determine_complete(const SpaceT &space) {
            const Integer n_elems = space.mesh().n_elements();

            space.mesh().measure(true_measure);
            measure.finalize(n_elems);

            is_measure_complete.resize(n_elems);
            std::fill(is_measure_complete.begin(), is_measure_complete.end(), false);

            touched.resize(n_elems);
            std::fill(touched.begin(), touched.end(), false);

            std::size_t offset = space.mesh().local_element_offset();

            has_incomplete = false;

            for (auto it = measure.get().iter(); it; ++it) {
                const std::size_t idx = it.row() - offset;
                assert(idx < n_elems);

                const T complete_measure = true_measure[idx];
                const T computed_measure = *it;
                const T ratio = std::abs((computed_measure - complete_measure) / complete_measure);

                // logger() << complete_measure << ", " << computed_measure << ", " << ratio << std::endl;

                is_measure_complete[idx] = approxeq(ratio, static_cast<T>(0.0), tol);
                touched[idx] = true;

                if (!is_measure_complete[idx]) {
                    has_incomplete = true;
                }
            }

            build_list(space);

            // print(buffers.is_measure_complete, std::cout);
        }

        void remove_incomplete(SparseMatrix<T> &mat) {
            if (!has_incomplete) return;
            mat.remove_rows(std::begin(to_remove), std::end(to_remove));
        }

        void remove_incomplete_tensor_product(const int dim, SparseMatrix<T> &mat) {
            std::list<Integer> to_remove_tp;

            for (const auto &t : to_remove) {
                for (int i = 0; i < dim; ++i) {
                    to_remove_tp.push_back(t * dim + i);
                }
            }

            mat.remove_rows(std::begin(to_remove_tp), std::end(to_remove_tp));
        }

        template <class SpaceT>
        void mark_complete(const SpaceT &space, SparseMatrix<T> &is_complete) {
            const auto &dof_map = space.dof_map();

            is_complete.clear();
            is_complete.set_size(dof_map.n_dofs(), 1, dof_map.n_local_dofs(), 1);

            const std::size_t n = is_measure_complete.size();

            for (std::size_t i = 0; i < n; ++i) {
                if (!touched[i]) continue;
                if (!is_measure_complete[i]) continue;

                const auto &dofs = dof_map.dofs(i);

                for (auto d : dofs) {
                    is_complete.set(d, 0, 1.0);
                }
            }
        }

        template <class SpaceT>
        void mark_complete(const SpaceT &space, MatrixInserter<T> &is_complete) {
            const auto &dof_map = space.dof_map();

            is_complete.clear();

            const Integer n = static_cast<Integer>(is_measure_complete.size());

            for (Integer i = 0; i < n; ++i) {
                if (!touched[i]) continue;
                if (!is_measure_complete[i]) continue;

                const auto &dofs = dof_map.dofs(i);

                for (auto d : dofs) {
                    is_complete.insert(d, 1.0);
                }
            }

            is_complete.finalize(static_cast<Integer>(dof_map.n_local_dofs()), 1);
        }

        // used to remove incomplete intersections
        MatrixInserter<T> measure;
        T tol;

        Storage<bool> is_measure_complete;
        Storage<bool> touched;
        Storage<T> true_measure;

        bool has_incomplete;
        std::list<Integer> to_remove;

    private:
        template <class SpaceT>
        void build_list(const SpaceT &space) {
            if (!has_incomplete) return;

            const auto &dof_map = space.dof_map();

            to_remove.clear();
            const Integer n = static_cast<Integer>(is_measure_complete.size());

            for (Integer i = 0; i < n; ++i) {
                if (!touched[i]) continue;
                if (is_measure_complete[i]) continue;

                const auto &dofs = dof_map.dofs(i);

                for (auto d : dofs) {
                    to_remove.push_back(d);
                }
            }
        }
    };
}  // namespace moonolith

#endif  // UTOPIA_INCOMPLETE_INTERSECTION_REMOVER_HPP
