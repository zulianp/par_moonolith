#include "moonolith_fe.hpp"
#include "moonolith_elem_edge2.hpp"

namespace moonolith {
    template class FE<Edge2<Real>>;
}