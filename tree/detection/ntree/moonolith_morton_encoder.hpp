#ifndef MOONOLITH_MORTON_ENCODER_HPP
#define MOONOLITH_MORTON_ENCODER_HPP

#include "moonolith_static_math.hpp"
#include <limits.h>

namespace moonolith {

	template<typename Int, Int Dimension>
	class MortonEncoder {};

	template<>
	class MortonEncoder<Integer, 1> {
	public:
		typedef std::make_unsigned<Integer>::type Unsigned;

		inline static Unsigned encode(const Unsigned * coords)		
		{
			return encode(coords[0]);
		}

		inline static Unsigned encode(const Integer * coords)		
		{
			return encode(coords[0]);
		}

		inline static Unsigned encode(Unsigned x)
		{
			x &= 0x0000ffff;                  // x = ---- ---- ---- ---- fedc ba98 7654 3210
		    x = (x ^ (x <<  8)) & 0x00ff00ff; // x = ---- ---- fedc ba98 ---- ---- 7654 3210
		    x = (x ^ (x <<  4)) & 0x0f0f0f0f; // x = ---- fedc ---- ba98 ---- 7654 ---- 3210
		    x = (x ^ (x <<  2)) & 0x33333333; // x = --fe --dc --ba --98 --76 --54 --32 --10
		    x = (x ^ (x <<  1)) & 0x55555555; // x = -f-e -d-c -b-a -9-8 -7-6 -5-4 -3-2 -1-0
		    return x;
		}

		inline static Unsigned encode_child(Unsigned parent, Unsigned childNumber) 
		{
			return (parent << 1) + childNumber;
		}

		inline static void decode(Unsigned morton, Integer * coords) 
		{
			Integer x = morton;
			x &= 0x55555555;                  // x = -f-e -d-c -b-a -9-8 -7-6 -5-4 -3-2 -1-0
	    	x = (x ^ (x >>  1)) & 0x33333333; // x = --fe --dc --ba --98 --76 --54 --32 --10
	    	x = (x ^ (x >>  2)) & 0x0f0f0f0f; // x = ---- fedc ---- ba98 ---- 7654 ---- 3210
	    	x = (x ^ (x >>  4)) & 0x00ff00ff; // x = ---- ---- fedc ba98 ---- ---- 7654 3210
	    	x = (x ^ (x >>  8)) & 0x0000ffff; // x = ---- ---- ---- ---- fedc ba98 7654 3210
	    	coords[0] = x;
	    }
	};

	template<>
	class MortonEncoder<Integer, 2> {
	public:
		typedef std::make_unsigned<Integer>::type Unsigned;

		inline static Unsigned encode(const Unsigned * coords)		
		{
			return encode(coords[0], coords[1]);
		}

		inline static Unsigned encode(const Integer * coords)		
		{
			return encode(coords[0], coords[1]);
		}

		inline static Unsigned encode(Unsigned x, Unsigned y)
		{
			return MortonEncoder<Integer, 1>::encode(x) | ( MortonEncoder<Integer, 1>::encode(y) << 1);
		}

		inline static Unsigned encode_child(Unsigned parent, Unsigned childNumber) 
		{
			return (parent << 2) + childNumber;
		}

		inline static void decode(Unsigned morton, Integer * coords) 
		{
			Integer &x = coords[0];
			Integer &y = coords[1];
			MortonEncoder<Integer, 1>::decode(morton, &x);
			MortonEncoder<Integer, 1>::decode(morton >> 1, &y);
		}
	};

	template<>
	class MortonEncoder<Integer, 3> {
	public:

		typedef std::make_unsigned<Integer>::type Unsigned;

		// method to seperate bits from a given integer 3 positions apart
		inline static Unsigned split_by_3(Unsigned a)
		{
		    Unsigned x = a    & 0x1fffff; // we only look at the first 21 bits
		    x = (x | x << 32) & 0x1f00000000ffff;  // shift left 32 bits, OR with self, and  00011111000000000000000000000000000000001111111111111111
		    x = (x | x << 16) & 0x1f0000ff0000ff;  // shift left 32 bits, OR with self, and  00011111000000000000000011111111000000000000000011111111
		    x = (x | x << 8 ) & 0x100f00f00f00f00f; // shift left 32 bits, OR with self, and 0001000000001111000000001111000000001111000000001111000000000000
		    x = (x | x << 4 ) & 0x10c30c30c30c30c3; // shift left 32 bits, OR with self, and 0001000011000011000011000011000011000011000011000011000100000000
		    x = (x | x << 2 ) & 0x1249249249249249;
		    return x;
		}

		inline static Unsigned encode(const Integer * coords)		
		{
			return encode(coords[0], coords[1], coords[2]);
		}

		inline static Unsigned encode(const Unsigned * coords)		
		{
			return encode(coords[0], coords[1], coords[2]);
		}

		inline static Unsigned encode(Unsigned x, Unsigned y, Unsigned z)
		{
			Unsigned answer = 0;
			answer |= split_by_3(x) | split_by_3(y) << 1 | split_by_3(z) << 2;
			return answer;
		}

		inline static Unsigned encode_child(Unsigned parent, Unsigned childNumber)
		{
			return (parent << 3) + childNumber;
		}

		inline static void decode(Unsigned morton, Integer * coords) {
			Integer &x = coords[0];
			Integer &y = coords[1];
			Integer &z = coords[2];

			x = 0;
			y = 0;
			z = 0;
			for (Unsigned i = 0; i < (sizeof(Unsigned) * CHAR_BIT)/3; ++i) {
				x |= ((morton & (Unsigned( 1ull ) << Unsigned((3ull * i) + 0ull))) >> Unsigned(((3ull * i) + 0ull)-i));
				y |= ((morton & (Unsigned( 1ull ) << Unsigned((3ull * i) + 1ull))) >> Unsigned(((3ull * i) + 1ull)-i));
				z |= ((morton & (Unsigned( 1ull ) << Unsigned((3ull * i) + 2ull))) >> Unsigned(((3ull * i) + 2ull)-i));
			}
		}
	};


	//todo make this as the others
	template<>
	class MortonEncoder<Integer, 4> {
	public:
		typedef std::make_unsigned<Integer>::type Unsigned;

		inline static Unsigned encode(const Unsigned * coords)		
		{
			return encode(coords[0], coords[1], coords[2], coords[3]);
		}

		inline static Unsigned encode(const Integer * coords)		
		{
			return encode(coords[0], coords[1], coords[2], coords[3]);
		}

		inline static Unsigned encode(Unsigned x, Unsigned y, Unsigned z, Unsigned w)
		{
			Unsigned answer = 0;
			answer += (x == 1) * 8 + (y == 1) * 4 + (z == 1) * 2 + (w == 1);
			return answer;
		}

		inline static Unsigned encode_child(Unsigned parent, Unsigned childNumber) 
		{
			return (parent << 4) + childNumber;
		}

		inline static void decode(Unsigned morton, Integer * coords) 
		{
			Integer &x = coords[0];
			Integer &y = coords[1];
			Integer &z = coords[2];
			Integer &w = coords[3];

			w = (1 & morton) > 0;
			z = (2 & morton) > 0;
			y = (4 & morton) > 0;
			x = (8 & morton) > 0;
		}
	};
}

#endif //MOONOLITH_MORTON_ENCODER_HPP
