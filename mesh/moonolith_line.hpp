#ifndef MOONOLITH_LINE_HPP
#define MOONOLITH_LINE_HPP

#include "moonolith_empirical_tol.hpp"
#include "moonolith_geo_algebra.hpp"
#include "moonolith_plane.hpp"
#include "moonolith_ray.hpp"
#include "moonolith_shape.hpp"
#include "moonolith_vector.hpp"

namespace moonolith {

    template <typename T, int Dim>
    class Line : public Shape<T, 1, Dim> {
    public:
        using Point = moonolith::Vector<T, Dim>;
        using Point1 = moonolith::Vector<T, 1>;

        inline bool contains(const Point &p, const T tol) const {
            auto u = p - p0;
            auto v = p1 - p0;

            auto len_u = length(u);
            auto len_v = length(v);

            if (approxeq(len_u, static_cast<T>(0.), tol)) {
                return true;
            }

            u /= len_u;
            v /= len_v;

            if (!approxeq(dot(u, v), static_cast<T>(1.), tol)) {
                return false;
            }

            return len_u < len_v + tol;
        }

        bool intersect(const Ray<T, Dim> &ray, T &t) override {
            Point1 x_param;
            return intersect(ray, t, x_param);
        }

        bool intersect(const Ray<T, Dim> &ray, T &t) const {
            Point1 x_param;
            return intersect(ray, t, x_param);
        }

        bool intersect(const Ray<T, Dim> &ray, T &t, Point1 &x_param) override {
            return const_cast<const Line &>(*this).intersect(ray, t, x_param);
        }

        bool intersect(const Ray<T, Dim> &ray, T &t, Point1 &x_param) const {
            // TODO fix for higher/lower dimensions than 2

            Plane<T, Dim> plane;
            auto u = p1 - p0;
            auto len_line = length(u);
            u /= len_line;

            plane.p = p0;
            perp(u, plane.n);

            const auto cos_angle = -dot(plane.n, ray.dir);

            if (std::abs(cos_angle) < GeometricTol<T>::value()) {
                t = 0.0;
                return false;
            }

            // distance from plane
            const auto dist = plane.signed_dist(ray.o);

            // point in ray trajectory
            t = dist / cos_angle;

            auto isect = ray.dir;
            isect *= t;
            isect += ray.o;
            isect -= p0;

            x_param.x = dot(isect, u);
            if (x_param.x > len_line) {
                return false;
            }

            if (x_param.x < 0) {
                return false;
            }

            return true;
        }

        Point p0;
        Point p1;
    };

    template <typename T, int Dim>
    class PolyLine : public Shape<T, Dim> {
    public:
        using Point = moonolith::Vector<T, Dim>;

        inline std::size_t size() const { return points.size(); }
        Point &operator[](const std::size_t idx) { return points[idx]; }
        const Point &operator[](const std::size_t idx) const { return points[idx]; }
        Storage<Point> points;

        bool intersect(const Ray<T, Dim> &ray, T &t) override {
            std::size_t n_segments = size() - 1;
            Line<T, Dim> line;
            for (std::size_t i = 0; i < n_segments; ++i) {
                line.p0 = points[i];
                line.p1 = points[i + 1];

                if (line.intersect(ray, t)) {
                    return true;
                }
            }

            return false;
        }

        virtual bool intersect(const Ray<T, Dim> &ray, T &t, Point &x_param) override {
            bool ok = intersect(ray, t);
            x_param = ray.o + t * ray.dir;
            return ok;
        }
    };

    template <typename T, int Dim>
    T measure(const Line<T, Dim> &line) {
        return length(line.p1 - line.p0);
    }

    template <typename T, int Dim>
    T measure(const Storage<Vector<T, Dim>> &poly_line) {
        T ret = 0.;

        std::size_t n = poly_line.size();

        for (std::size_t i = 1; i < n; ++i) {
            ret += length(poly_line[i - 1] - poly_line[i]);
        }

        return ret;
    }

    template <typename T, int Dim>
    T measure(const PolyLine<T, Dim> &poly_line) {
        return measure(poly_line.points);
    }

    template <typename T, int Dim>
    bool approxeq(const Line<T, Dim> &l, const Line<T, Dim> &r, const T tol) {
        if (length(l.p0 - r.p0) < tol) {
            return length(l.p1 - r.p1) < tol;
        } else {
            if (length(l.p0 - r.p1) < tol) {
                return length(l.p1 - r.p0) < tol;
            }
        }

        return false;
    }

}  // namespace moonolith

#endif  // MOONOLITH_LINE_HPP
