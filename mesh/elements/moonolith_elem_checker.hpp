#ifndef MOONOLITH_ELEM_CHECKER_HPP
#define MOONOLITH_ELEM_CHECKER_HPP

#include "moonolith_base.hpp"
#include "moonolith_elem.hpp"

#include <cassert>
#include <limits>

namespace moonolith {

    template <class Elem>
    class ElemChecker {
    public:
        static const int Dim = Elem::Dim;
        static const int PhysicalDim = Elem::PhysicalDim;
        static const std::size_t NNodes = Elem::NNodes;
        static const int Order = Elem::Order;

        static_assert(Dim == PhysicalDim, "check only done for Dim == Codim");

        using T = typename Elem::T;
        using Point = typename Elem::Point;
        using Vector = typename Elem::Vector;
        using CoPoint = typename Elem::CoPoint;

        static void fd_hessian(Elem &e, const int fn, const Point &b, const T h, std::array<T, Dim * Dim> &H) {
            const T h2 = h * h;
            Vector ei, ej;

            for (int i = 0; i < Dim; ++i) {
                if (i > 0) {
                    ei[i - 1] = 0.0;
                }

                ei[i] = h;

                for (int j = 0; j < Dim; ++j) {
                    if (j > 0) {
                        ej[j - 1] = 0.0;
                    }

                    ej[j] = h;

                    const auto fpipj = e.fun(fn, b + ei + ej);
                    const auto fpimj = e.fun(fn, b + ei - ej);
                    const auto fmipj = e.fun(fn, b - ei + ej);
                    const auto fmimj = e.fun(fn, b - ei - ej);

                    H[i * Dim + j] = (fpipj - fpimj - fmipj + fmimj) / (4.0 * h2);
                }
            }
        }

        static bool check(const T tol = std::numeric_limits<T>::epsilon()) {
            std::array<Point, NNodes> ref_nodes;
            Reference<Elem>::points(ref_nodes);

            Point b;
            Elem e;
            for (std::size_t i = 0; i < NNodes; ++i) {
                e.node(i) = ref_nodes[i];
                b += ref_nodes[i];
            }

            b /= NNodes;

            bool failed = false;
            bool ok = false;
            ok = check_at_point(b, tol, false);
            moonolith_test_assert(ok);
            failed = failed || !ok;

            // discover problems if not symmetric
            for (int d = 0; d < Dim; ++d) {
                b[d] += (d + 1) * 0.1;
            }

            ok = check_at_point(b, tol, false);
            moonolith_test_assert(ok);
            failed = failed || !ok;
            return !failed;
        }

        static bool check_at_point(const Point &b, const T tol, const bool check_hessian) {
            bool ok = true;
            bool failed = false;

            std::array<Point, NNodes> ref_nodes;
            Reference<Elem>::points(ref_nodes);

            Elem e;
            for (std::size_t i = 0; i < NNodes; ++i) {
                e.node(i) = ref_nodes[i];
            }

            // check Lagrange property
            for (std::size_t i = 0; i < NNodes; ++i) {
                const T f = e.fun(i, ref_nodes[i]);
                ok = approxeq(f, static_cast<T>(1.0), tol);

                if (!ok) {
                    std::cerr << "[Error] f(" << i << ", " << i << ") != 1" << std::endl;
                    moonolith_test_assert(ok);
                }

                failed = failed || !ok;

                for (std::size_t j = 0; j < NNodes; ++j) {
                    if (j == i) continue;

                    const T f = e.fun(i, ref_nodes[j]);
                    ok = approxeq(f, static_cast<T>(0.0), tol);

                    if (!ok) {
                        std::cerr << "[Error] f(" << i << ", " << j << ") != 0.0" << std::endl;
                        moonolith_test_assert(ok);
                    }

                    failed = failed || !ok;
                }
            }

            const T h = CheckTol<T>::half_value();

            // check gradients
            Vector fd_g, g;
            for (std::size_t i = 0; i < NNodes; ++i) {
                auto f = e.fun(i, b);
                for (int d = 0; d < Dim; ++d) {
                    Point p = b;
                    p[d] += h;

                    auto fph = e.fun(i, p);

                    fd_g[d] = (fph - f) / h;
                }

                e.grad(i, b, g);

                ok = approxeq(fd_g, g, CheckTol<T>::quarter_value());

                if (!ok) {
                    std::cerr << "[Error] grad(" << i << ") is wrong!" << std::endl;
                    moonolith_test_assert(ok);
                }

                failed = failed || !ok;
            }

            std::array<T, PhysicalDim * Dim> J;
            e.jacobian(b, J);

            for (int i = 0; i < PhysicalDim; ++i) {
                ok = approxeq(J[i * Dim + i], static_cast<T>(1.0), CheckTol<T>::quarter_value());
                moonolith_test_assert(ok);

                for (int j = 0; j < Dim; ++j) {
                    if (i == j) continue;

                    ok = approxeq(J[i * Dim + j], static_cast<T>(0.0), CheckTol<T>::quarter_value());
                    moonolith_test_assert(ok);
                }
            }

            // check hessian
            // for some reason this test fails but the derivatives are correct
            if (check_hessian) {
                std::array<T, Dim * Dim> fd_H, H;

                for (std::size_t i = 0; i < NNodes; ++i) {
                    e.hessian(i, b, H);

                    if (Order < 1) {
                        for (int k = 0; k < Dim * Dim; ++k) {
                            ok = approxeq(H[k], static_cast<T>(0.0), tol);
                            moonolith_test_assert(ok);
                        }
                    }

                    fd_hessian(e, i, b, h, fd_H);

                    for (int k = 0; k < Dim * Dim; ++k) {
                        ok = approxeq(H[k], fd_H[k], CheckTol<T>::quarter_value());

                        if (!ok) {
                            std::cerr << "---------------------------------\n";
                            std::cerr << "[Error] H(" << i << ") is wrong at " << k << "! " << std::endl;
                            std::cerr << "expected: [ ";

                            for (int k = 0; k < Dim * Dim; ++k) {
                                std::cerr << fd_H[k] << " ";
                            }

                            std::cerr << "]\nactual:   [ ";

                            for (int k = 0; k < Dim * Dim; ++k) {
                                std::cerr << H[k] << " ";
                            }

                            std::cerr << "]" << std::endl;
                            std::cerr << "---------------------------------\n";
                            moonolith_test_assert(ok);
                        }
                    }
                }
            }

            return !failed;
        }
    };
}  // namespace moonolith

#endif  // MOONOLITH_ELEM_CHECKER_HPP
