#ifndef MOONOLITH_INTERSECT_POLYGON_WITH_H_POLYHEDRON_IMPL_HPP
#define MOONOLITH_INTERSECT_POLYGON_WITH_H_POLYHEDRON_IMPL_HPP

#include "moonolith_intersect_polygon_with_h_polyhedron.hpp"
#include "moonolith_intersect_polygons_impl.hpp"

namespace moonolith {

    template <typename T>
    short IntersectPolygonWithHPolyhedron<T>::apply(const Polygon3 &polygon,
                                                    const HPolyhedron &h_poly,
                                                    Polygon3 &result,
                                                    T tol) {
        const Integer n_half_spaces = static_cast<Integer>(h_poly.half_spaces.size());
        in = polygon;

        side_num_ = -1;

        for (Integer k = 0; k < n_half_spaces; ++k) {
            const Integer n_points = static_cast<Integer>(in.points.size());
            result.clear();

            Integer n_aligned = 0;

            for (Integer i = 0; i < n_points; ++i) {
                const Integer ip1 = (i + 1 == n_points) ? 0 : (i + 1);

                line.p0 = in.points[i];
                line.p1 = in.points[ip1];

                int n_intersected = 0;
                bool line_outside_h_poly = false;

                bool orginal_vertex[2] = {true, true};
                short ret = isect_line_with_half_space.apply(line, h_poly.half_spaces[k], isect_line, tol);

                switch (ret) {
                    case IsectLineWithHS::NO_INTERSECTION: {
                        // outside half-space
                        line_outside_h_poly = true;
                        break;
                    }
                    case IsectLineWithHS::SEGMENT_FROM_POINT_0: {
                        orginal_vertex[1] = false;
                        line = isect_line;
                        ++n_intersected;
                        break;
                    }
                    case IsectLineWithHS::SEGMENT_FROM_POINT_1: {
                        // intersection with separating plane
                        orginal_vertex[0] = false;
                        line = isect_line;
                        ++n_intersected;
                        break;
                    }

                    case IsectLineWithHS::INSIDE: {
                        // completely inside
                        break;
                    }

                    case IsectLineWithHS::ON_BOUNDARY: {
                        ++n_aligned;
                        break;
                    }

                    default: {
                        assert(false);
                        break;
                    }
                }

                if (line_outside_h_poly) {
                    // the segment is to be skipped and not to be used
                    continue;
                }

                result.points.push_back(isect_line.p0);

                if (n_intersected && !orginal_vertex[1]) {
                    result.points.push_back(isect_line.p1);
                }
            }

            intersect_polygons.remove_duplicate_points(result, tol);

            if (result.points.size() < 3) {
                assert(h_poly.n_contained(std::begin(polygon.points), std::end(polygon.points), tol) < 3);
                return 0;
            }

            in = result;

            if (n_aligned == n_points) {
                assert(side_num_ == -1);
                side_num_ = k;
            }
        }

        return 1;
    }
}  // namespace moonolith

#endif  // MOONOLITH_INTERSECT_POLYGON_WITH_H_POLYHEDRON_IMPL_HPP
