#ifndef MOONOLITH_INTERSECT_RAY_WITH_BOX_HPP
#define MOONOLITH_INTERSECT_RAY_WITH_BOX_HPP

#include "moonolith_aabb.hpp"
#include "moonolith_ray.hpp"
#include "moonolith_vector.hpp"

#include <cmath>

namespace moonolith {

    template <typename T, int PhysicalDim>
    class IntersectRayWithAABB {};

    template <typename T>
    class IntersectRayWithAABB<T, 3> {
    public:
        // https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-box-intersection
        bool apply(const Ray<T, 3> &ray, const AABB<3, T> &box, T &tmin, T &tmax) {
            enlarged_ = box;
            enlarged_.enlarge(tol_);

            const auto &o = ray.o;

            Vector<T, 3> inv_dir{1. / ray.dir[0], 1. / ray.dir[1], 1. / ray.dir[2]};
            bool sign[3] = {inv_dir[0] < 0, inv_dir[1] < 0, inv_dir[2] < 0};

            T xmin = sign[0] ? enlarged_.max(0) : enlarged_.min(0);
            T xmax = sign[0] ? enlarged_.min(0) : enlarged_.max(0);

            T ymin = sign[1] ? enlarged_.max(1) : enlarged_.min(1);
            T ymax = sign[1] ? enlarged_.min(1) : enlarged_.max(1);

            T txmin, txmax, tymin, tymax, tzmin, tzmax;
            tmin = (xmin - o[0]) * inv_dir[0];
            tmax = (xmax - o[0]) * inv_dir[0];

            if (std::isnan(tmin) || std::isnan(tmax)) {
                return false;
            }

            tymin = (ymin - o[1]) * inv_dir[1];
            tymax = (ymax - o[1]) * inv_dir[1];

            if ((tmin > tymax) || (tymin > tmax)) return false;
            if (tymin > tmin) tmin = tymin;
            if (tymax < tmax) tmax = tymax;

            T zmin = sign[2] ? enlarged_.max(2) : enlarged_.min(2);
            T zmax = sign[2] ? enlarged_.min(2) : enlarged_.max(2);

            tzmin = (zmin - o[2]) * inv_dir[2];
            tzmax = (zmax - o[2]) * inv_dir[2];

            if ((tmin > tzmax) || (tzmin > tmax)) return false;
            if (tzmin > tmin) tmin = tzmin;
            if (tzmax < tmax) tmax = tzmax;
            return true;
        }

        T tol_{GeometricTol<T>::value()};
        AABB<3, T> enlarged_;
    };

    template <typename T>
    class IntersectRayWithAABB<T, 2> {
    public:
        bool apply(const Ray<T, 2> &ray, const AABB<2, T> &box, T &tmin, T &tmax) {
            // return true;  // FIXME

            enlarged_ = box;
            enlarged_.enlarge(tol_);

            const auto &o = ray.o;

            Vector<T, 2> inv_dir{1. / ray.dir[0], 1. / ray.dir[1]};
            bool sign[2] = {inv_dir[0] < 0, inv_dir[1] < 0};

            T xmin = sign[0] ? enlarged_.max(0) : enlarged_.min(0);
            T xmax = sign[0] ? enlarged_.min(0) : enlarged_.max(0);

            T ymin = sign[1] ? enlarged_.max(1) : enlarged_.min(1);
            T ymax = sign[1] ? enlarged_.min(1) : enlarged_.max(1);

            T txmin, txmax, tymin, tymax, tzmin, tzmax;
            tmin = (xmin - o[0]) * inv_dir[0];
            tmax = (xmax - o[0]) * inv_dir[0];

            if (std::isnan(tmin) || std::isnan(tmax)) {
                return false;
            }

            tymin = (ymin - o[1]) * inv_dir[1];
            tymax = (ymax - o[1]) * inv_dir[1];

            if ((tmin > tymax) || (tymin > tmax)) return false;

            if (tymin > tmin) tmin = tymin;
            if (tymax < tmax) tmax = tymax;

            return true;
        }

        T tol_{GeometricTol<T>::value()};
        AABB<2, T> enlarged_;
    };

}  // namespace moonolith

#endif  // MOONOLITH_INTERSECT_RAY_WITH_BOX_HPP
