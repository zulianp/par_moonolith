#ifndef MOONOLITH_ASSEMBLY_HPP
#define MOONOLITH_ASSEMBLY_HPP

#include <cassert>
#include "moonolith_expanding_array.hpp"

namespace moonolith {

    template <class TrialFE, class TestFE, class Matrix>
    static void assemble_mass_matrix_add(TrialFE &trial, TestFE &test, Matrix &mat) {
        assert(static_cast<Integer>(trial.n_shape_functions()) == static_cast<Integer>(mat.cols()));
        assert(static_cast<Integer>(test.n_shape_functions()) == static_cast<Integer>(mat.rows()));

        const int n_qps = test.n_quad_points();

        assert(n_qps > 0);

        for (int i = 0; i < mat.rows(); ++i) {
            for (int j = 0; j < mat.cols(); ++j) {
                for (int k = 0; k < n_qps; ++k) {
                    mat(i, j) += test.fun(i, k) * trial.fun(j, k) * test.dx(k);
                }
            }
        }
    }

    template <class TrialFE, class TestFE, class Matrix>
    static void assemble_mass_matrix(TrialFE &trial, TestFE &test, Matrix &mat) {
        Resize<Matrix>::apply(mat, test.n_shape_functions(), trial.n_shape_functions());
        mat.zero();

        assemble_mass_matrix_add(trial, test, mat);
    }

}  // namespace moonolith

#endif  // MOONOLITH_ASSEMBLY_HPP
