
#ifndef NEOHOOKEAN_INTERFACES_CL
#define NEOHOOKEAN_INTERFACES_CL

#include "deformation_interface.cl"

typedef struct {
	Scalar mu;
	Scalar lambda;
} NeohookeanMaterial;


#ifndef CLIPP_HOST_CL

void aux_compute_element_grad_inv_x_grad(const FEObject *element, const Matrixdxd *element_grad, const Deformation *def, const bool must_transpose, Matrixdxd * grad_inv_x_grad);
void aux_compute_element_dots(const FEObject *element, const Matrixdxd *element_grad, const Deformation *def, Scalar * dots);

Scalar compressible_neohookean_energy_at_quad_index(const SizeType quad_index,
	const NeohookeanMaterial *material, 
	const FEObject *fe, const Deformation *def
	);

void compressible_neohookean_first_pk_stress(const SizeType quad_index,
	const NeohookeanMaterial *material, 
	const FEObject *fe, const Deformation *def, 
	Scalar *result);

void compressible_neohookean_gradient_at_quad_index(const SizeType quad_index,
	const NeohookeanMaterial *material, 
	const FEObject *trial, const FEObject *test, 
	const Deformation *def, 
	const Matrixdxd *test_grad,
	Scalar *cummulative_result);


void compressible_neohookean_hessian_symmetric_at_quad_index(const SizeType quad_index,
	const NeohookeanMaterial *material, 
	const FEObject *element,
	const Deformation *def, 
	const Matrixdxd *element_grad,
	Scalar *cummulative_result
	);

void compressible_neohookean_hessian_at_quad_index(const SizeType quad_index,
	const NeohookeanMaterial *material, 
	const FEObject *trial, const FEObject *test, 
	const Deformation *def, 
	const Matrixdxd *trial_grad,
	const Matrixdxd *test_grad,
	Scalar * cummulative_result
	);

void compressible_neohookean_symmetric_assemble_local(
	const NeohookeanMaterial * material,
	const FEObject * element, 
	const Scalar * local_displacement,
	Scalar * local_hessian,
	Scalar * local_gradient
	);

void compressible_neohookean_assemble_local(
	const NeohookeanMaterial * material,
	const FEObject * trial, 
	const FEObject * test, 
	const Scalar * local_displacement,
	Scalar * local_hessian,
	Scalar * local_gradient
	);

void compressible_neohookean_assemble_gradient(
	const NeohookeanMaterial * material,
	const FEObject * trial, 
	const FEObject * test, 
	const Scalar * local_displacement,
	Scalar * local_gradient);

Scalar compressible_neohookean_assemble_energy(
	const NeohookeanMaterial * material,
	const FEObject * trial, 
	const FEObject * test, 
	const Scalar * local_displacement);

void neohookean_von_mises_stress_local(const NeohookeanMaterial *material, 
	const FEObject * trial, 
	const FEObject * test, 
	const Scalar * local_displacement,
	Scalar *result);

#endif //CLIPP_HOST_CL
#endif //NEOHOOKEAN_INTERFACES_CL


