#ifndef MOONOLITH_IS_GLUE_HPP
#define MOONOLITH_IS_GLUE_HPP

#include "par_moonolith_config.hpp"
#include "moonolith_describable.hpp"

#include <set>
#include <tuple>

namespace moonolith {

    class IsGlue final : public Describable {
    public:

        inline IsGlue() {}

        inline bool operator()(const Integer tag1, const Integer tag2) const
        {
            auto it = is_glue_.find(std::pair<Integer, Integer>(tag1, tag2));
            if(it == is_glue_.end()) {
                return false;
            } else {
                return true;
            }
        }

        inline void insert(const Integer tag1, const Integer tag2)
        {
            is_glue_.insert(std::pair<Integer, Integer>(tag1, tag2));
        }

        inline void describe(std::ostream &os) const override
        {
            for(auto p : is_glue_) {
                os << p.first << ", " << p.second << "\n";
            }
        }

        inline bool empty() const
        {
            return is_glue_.empty();
        }

    public:
        std::set<std::pair<Integer, Integer>>  is_glue_;
    };

}

#endif //MOONOLITH_IS_GLUE_HPP
