#include "moonolith_lapack.hpp"

#ifdef MOONOLITH_HAVE_LAPACK

#include <algorithm>

extern "C" {
void dgesv_(const int* n,
            const int* nrhs,
            const double* a,
            const int* lda,
            const int* ipiv,
            const double* b,
            const int* ldb,
            const int* info);

void sgesv_(const int* n,
            const int* nrhs,
            const float* a,
            const int* lda,
            const int* ipiv,
            const float* b,
            const int* ldb,
            const int* info);
}

namespace moonolith {

    template <typename T>
    struct LapackAlgorithms {};

    template <>
    struct LapackAlgorithms<double> {
        using T = double;

        static inline void gesv(int n, int nrhs, T* mat, int lda, int* ipiv, T* out, int ldb, int* info) {
            dgesv_(&n, &nrhs, mat, &lda, ipiv, out, &ldb, info);
        }
    };

    template <>
    struct LapackAlgorithms<float> {
        using T = float;

        static inline void gesv(int n, int nrhs, T* mat, int lda, int* ipiv, T* out, int ldb, int* info) {
            sgesv_(&n, &nrhs, mat, &lda, ipiv, out, &ldb, info);
        }
    };

    template <typename T>
    bool LapackInverse<T>::init(const int rows, const T* mat) {
        rows_ = rows;
        this->mat_.resize(rows * rows);
        std::copy(mat, mat + this->mat_.size(), &this->mat_[0]);
        return true;
    }

    template <typename T>
    bool LapackInverse<T>::apply(const T* in, T* out, const int nrhs) {
        const int n = rows_;
        const int lda = rows_;
        const int ldb = rows_;
        std::vector<int> ipiv(rows_);
        int info;

        if (in != out) {
            std::copy(in, in + (n * nrhs), out);
        }

        LapackAlgorithms<T>::gesv(n, nrhs, &mat_[0], lda, &ipiv[0], out, ldb, &info);
        return true;
    }

    template class LapackInverse<double>;
    template class LapackInverse<float>;

}  // namespace moonolith

#endif  // MOONOLITH_HAVE_LAPACK
