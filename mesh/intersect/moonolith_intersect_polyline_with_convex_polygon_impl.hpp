#ifndef MOONOLITH_INTERSECT_POLYLINE_WITH_CONVEX_POLYGON_IMPL_HPP
#define MOONOLITH_INTERSECT_POLYLINE_WITH_CONVEX_POLYGON_IMPL_HPP

#include "moonolith_intersect_polyline_with_convex_polygon.hpp"

namespace moonolith {

    template <typename T>
    bool IntersectPolylineWithConvexPolygon2<T>::apply(const Line2 &line,
                                                       const Polygon2 &polygon,
                                                       Line2 &result,
                                                       const T &tol) {
        const Integer n_poly_nodes = static_cast<Integer>(polygon.size());
        result = line;
        side_num_ = -1;

        is_inside.resize(2);

        h_poly.make(polygon);

        for (Integer i = 0; i < n_poly_nodes; ++i) {
            buff = result;
            const short ret = isect_halfspace.apply(buff, h_poly.half_spaces[i], result, tol);

            // NO_INTERSECTION = 0;
            // SEGMENT_FROM_POINT_0 = 1;
            // SEGMENT_FROM_POINT_1 = 2;
            // INSIDE          = 3;
            // ON_BOUNDARY     = 4;

            switch (ret) {
                case IsectLineHS::NO_INTERSECTION:
                    return false;
                case IsectLineHS::ON_BOUNDARY: {
                    side_num_ = i;
                    continue;
                }
                default: {
                    continue;
                }
            }
        }

        assert(line.contains(result.p0, CheckTol<T>::half_value()));
        assert(line.contains(result.p1, CheckTol<T>::half_value()));

        return moonolith::measure(result) > tol;
    }

}  // namespace moonolith

#endif  // MOONOLITH_INTERSECT_POLYLINE_WITH_CONVEX_POLYGON_HPP