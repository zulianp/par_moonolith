#ifndef MOONOLITH_CONVEX_DECOMPOSITION_HPP
#define MOONOLITH_CONVEX_DECOMPOSITION_HPP

#include "moonolith_expanding_array.hpp"
#include "moonolith_polyhedron.hpp"
#include "moonolith_vector.hpp"

namespace moonolith {

    template <typename T, int Dim>
    class ConvexDecomposition {};

    template <typename T>
    class ConvexDecomposition<T, 3> {
    public:
        using Point = moonolith::Vector<T, 3>;

        static void decompose_tet10(const Storage<Point> &points, Storage<Polyhedron<T>> &poly) {
            // 4 tets
            // 1 octahedron
            poly.resize(5);
            make_tet(points, 0, 4, 6, 7, poly[0]);
            make_tet(points, 1, 5, 4, 8, poly[1]);
            make_tet(points, 2, 6, 5, 9, poly[2]);
            make_tet(points, 3, 7, 9, 8, poly[3]);
            make_octahedron(points, 4, 5, 6, 7, 8, 9, poly[4]);
        }

        static void decompose_tet10(const std::array<Point, 10> &points, Storage<Polyhedron<T>> &poly) {
            // 4 tets
            // 1 octahedron
            poly.resize(5);
            make_tet(points, 0, 4, 6, 7, poly[0]);
            make_tet(points, 1, 5, 4, 8, poly[1]);
            make_tet(points, 2, 6, 5, 9, poly[2]);
            make_tet(points, 3, 7, 9, 8, poly[3]);
            make_octahedron(points, 4, 5, 6, 7, 8, 9, poly[4]);
        }

        template <class ArrayType>
        static void make_octahedron(const ArrayType &points,
                                    const Integer i0,
                                    const Integer i1,
                                    const Integer i2,
                                    const Integer i3,
                                    const Integer i4,
                                    const Integer i5,
                                    Polyhedron<T> &poly) {
            poly.el_ptr.resize(8 + 1);
            poly.el_index.resize(8 * 3);
            poly.points.resize(6);

            poly.el_ptr[0] = 0;
            poly.el_ptr[1] = 3;
            poly.el_ptr[2] = 6;
            poly.el_ptr[3] = 9;
            poly.el_ptr[4] = 12;
            poly.el_ptr[5] = 15;
            poly.el_ptr[6] = 18;
            poly.el_ptr[7] = 21;
            poly.el_ptr[8] = 24;

            poly.points[0] = points[i0];
            poly.points[1] = points[i1];
            poly.points[2] = points[i2];
            poly.points[3] = points[i3];
            poly.points[4] = points[i4];
            poly.points[5] = points[i5];

            // external facets of the tetrahedron
            poly.el_index[0] = 0;
            poly.el_index[1] = 4;
            poly.el_index[2] = 3;

            poly.el_index[3] = 1;
            poly.el_index[4] = 5;
            poly.el_index[5] = 4;

            poly.el_index[6] = 2;
            poly.el_index[7] = 3;
            poly.el_index[8] = 5;

            poly.el_index[9] = 0;
            poly.el_index[10] = 2;
            poly.el_index[11] = 1;

            // internal facets
            poly.el_index[12] = 0;
            poly.el_index[13] = 1;
            poly.el_index[14] = 4;

            poly.el_index[15] = 0;
            poly.el_index[16] = 3;
            poly.el_index[17] = 2;

            poly.el_index[18] = 1;
            poly.el_index[19] = 2;
            poly.el_index[20] = 5;

            poly.el_index[21] = 4;
            poly.el_index[22] = 5;
            poly.el_index[23] = 3;

            poly.type = moonolith::Polyhedron<T>::OCTAHEDRON;
        }

        template <class ArrayType>
        static void make_tet(const ArrayType &points,
                             const Integer i0,
                             const Integer i1,
                             const Integer i2,
                             const Integer i3,
                             Polyhedron<T> &poly) {
            poly.el_ptr.resize(4 + 1);
            poly.el_index.resize(12);
            poly.points.resize(4);

            poly.el_ptr[0] = 0;
            poly.el_ptr[1] = 3;
            poly.el_ptr[2] = 6;
            poly.el_ptr[3] = 9;
            poly.el_ptr[4] = 12;

            poly.points[0] = points[i0];
            poly.points[1] = points[i1];
            poly.points[2] = points[i2];
            poly.points[3] = points[i3];

            poly.el_index[0] = 0;
            poly.el_index[1] = 2;
            poly.el_index[2] = 1;

            poly.el_index[3] = 0;
            poly.el_index[4] = 3;
            poly.el_index[5] = 2;

            poly.el_index[6] = 0;
            poly.el_index[7] = 1;
            poly.el_index[8] = 3;

            poly.el_index[9] = 1;
            poly.el_index[10] = 2;
            poly.el_index[11] = 3;

            poly.type = moonolith::Polyhedron<T>::TET;
        }

        template <class ArrayType>
        static void make_prism(const ArrayType &points,
                               const Integer i0,
                               const Integer i1,
                               const Integer i2,
                               const Integer i3,
                               const Integer i4,
                               const Integer i5,
                               Polyhedron<T> &poly) {
            poly.el_ptr.resize(5 + 1);
            poly.el_index.resize(18);
            poly.points.resize(6);

            poly.el_ptr[0] = 0;
            poly.el_ptr[1] = 3;
            poly.el_ptr[2] = 6;
            poly.el_ptr[3] = 10;
            poly.el_ptr[4] = 14;
            poly.el_ptr[5] = 18;

            poly.points[0] = points[i0];
            poly.points[1] = points[i1];
            poly.points[2] = points[i2];
            poly.points[3] = points[i3];
            poly.points[4] = points[i4];
            poly.points[5] = points[i5];

            poly.el_index[0] = 0;
            poly.el_index[1] = 2;
            poly.el_index[2] = 1;

            poly.el_index[3] = 3;
            poly.el_index[4] = 4;
            poly.el_index[5] = 5;

            poly.el_index[6] = 0;
            poly.el_index[7] = 3;
            poly.el_index[8] = 5;
            poly.el_index[9] = 2;

            poly.el_index[10] = 1;
            poly.el_index[11] = 2;
            poly.el_index[12] = 5;
            poly.el_index[13] = 4;

            poly.el_index[14] = 0;
            poly.el_index[15] = 1;
            poly.el_index[16] = 4;
            poly.el_index[17] = 3;

            poly.type = moonolith::Polyhedron<T>::PRISM;
        }

        template <class ArrayType>
        static void make_prism(const ArrayType &points, Polyhedron<T> &poly) {
            make_prism(points, 0, 1, 2, 3, 4, 5, poly);
        }

        // static void decompose_hex27(const std::array<Point, 27> &points, Storage<Polyhedron<T>> &poly) {

        template <class ArrayType>
        static void decompose_hex27(const ArrayType &points, Storage<Polyhedron<T>> &poly) {
            // 8 hexes
            poly.resize(8);
            // 0,0,0
            make_hex(points, 0, 8, 20, 11, 12, 21, 26, 24, poly[0]);

            // 1, 0, 0
            make_hex(points, 8, 1, 9, 20, 21, 13, 22, 26, poly[1]);

            // 0, 1, 0
            make_hex(points, 11, 20, 10, 3, 24, 26, 23, 15, poly[2]);

            // 1, 1, 0
            make_hex(points, 20, 9, 2, 10, 26, 22, 14, 23, poly[3]);

            // 0, 0, 1
            make_hex(points, 12, 21, 26, 24, 4, 16, 25, 19, poly[4]);

            // 1, 0, 1
            make_hex(points, 21, 13, 22, 26, 16, 5, 17, 25, poly[5]);

            // 0, 1, 1
            make_hex(points, 24, 26, 23, 15, 19, 25, 18, 7, poly[6]);

            // 1, 1, 1
            make_hex(points, 26, 22, 14, 23, 25, 17, 6, 18, poly[7]);
        }

        // static void decompose_non_affine_hex8(const std::array<Point, 8> &points, Storage<Polyhedron<T>> &poly) {
        template <class ArrayType8>
        static void decompose_non_affine_hex8(const ArrayType8 &points, Storage<Polyhedron<T>> &poly) {
            // https://cecas.clemson.edu/cvel/modeling/EMAG/EMAP/emap4/Meshing.html#:~:text=While%20dividing%20a%20hexahedron%20into,between%20local%20and%20global%20numbering.
            // https://arxiv.org/pdf/1801.01288.pdf
            poly.resize(5);
            make_tet(points, 0, 1, 2, 5, poly[0]);
            make_tet(points, 0, 5, 7, 4, poly[1]);
            make_tet(points, 2, 5, 6, 7, poly[3]);
            // make_tet(points, 0, 2, 5, 7, poly[2]);
            make_tet(points, 0, 2, 7, 5, poly[2]);
            make_tet(points, 0, 2, 3, 7, poly[4]);
        }

        // static void make_hex27_from_hex8(const std::array<Point, 8> &points_8, std::array<Point, 27> &points_27) {
        template <class ArrayType8, class ArrayType27>
        static void make_hex27_from_hex8(const ArrayType8 &points_8, ArrayType27 &points_27) {
            // copy corners
            for (int i = 0; i < 8; ++i) {
                points_27[i] = points_8[i];
            }

            //////////////////////////////////////////////////////
            // lower horizonatal edges
            points_27[8] = 0.5 * (points_8[0] + points_8[1]);
            points_27[9] = 0.5 * (points_8[1] + points_8[2]);
            points_27[10] = 0.5 * (points_8[2] + points_8[3]);
            points_27[11] = 0.5 * (points_8[3] + points_8[0]);

            //////////////////////////////////////////////////////
            // vertical edges
            points_27[12] = 0.5 * (points_8[0] + points_8[4]);
            points_27[13] = 0.5 * (points_8[1] + points_8[5]);
            points_27[14] = 0.5 * (points_8[2] + points_8[6]);
            points_27[15] = 0.5 * (points_8[3] + points_8[7]);
            //////////////////////////////////////////////////////

            // upper horizonal edges
            points_27[16] = 0.5 * (points_8[4] + points_8[5]);
            points_27[17] = 0.5 * (points_8[5] + points_8[6]);
            points_27[18] = 0.5 * (points_8[6] + points_8[7]);
            points_27[19] = 0.5 * (points_8[7] + points_8[4]);

            //////////////////////////////////////////////////////
            // Use bilinear mapping
            // 0.25 * (p0 + p1 + p2 + p3)
            // face nodes
            // points_27[20] = 0.25 * (points_8[0] + points_8[1] + points_8[2] + points_8[3]);
            // points_27[21] = 0.25 * (points_8[0] + points_8[1] + points_8[4] + points_8[5]);
            // points_27[22] = 0.25 * (points_8[1] + points_8[2] + points_8[5] + points_8[6]);
            // points_27[23] = 0.25 * (points_8[2] + points_8[3] + points_8[6] + points_8[7]);
            // points_27[24] = 0.25 * (points_8[0] + points_8[3] + points_8[4] + points_8[7]);
            // points_27[25] = 0.25 * (points_8[4] + points_8[5] + points_8[6] + points_8[7]);

            // equivalent version using edges
            points_27[20] = 0.5 * (points_27[8] + points_27[10]);
            points_27[21] = 0.5 * (points_27[8] + points_27[16]);
            points_27[22] = 0.5 * (points_27[9] + points_27[17]);
            points_27[23] = 0.5 * (points_27[10] + points_27[18]);
            points_27[24] = 0.5 * (points_27[11] + points_27[19]);
            points_27[25] = 0.5 * (points_27[16] + points_27[18]);

            //////////////////////////////////////////////////////
            // Use trilinear mapping
            // Volume node
            // points_27[26] = points_8[0];

            // for (int i = 1; i < 8; ++i) {
            //     points_27[26] += points_8[i];
            // }

            // points_27[26] *= 0.125;

            // equivalent version using faces
            points_27[26] = 0.5 * (points_27[20] + points_27[25]);
            //////////////////////////////////////////////////////
        }

        template <class ArrayType>
        static void make_hex(const ArrayType &points,
                             const Integer i0,
                             const Integer i1,
                             const Integer i2,
                             const Integer i3,
                             const Integer i4,
                             const Integer i5,
                             const Integer i6,
                             const Integer i7,
                             Polyhedron<T> &poly) {
            poly.el_ptr.resize(6 + 1);
            poly.el_index.resize(24);
            poly.points.resize(8);

            poly.el_ptr[0] = 0;
            poly.el_ptr[1] = 4;
            poly.el_ptr[2] = 8;
            poly.el_ptr[3] = 12;
            poly.el_ptr[4] = 16;
            poly.el_ptr[5] = 20;
            poly.el_ptr[6] = 24;

            poly.points[0] = points[i0];
            poly.points[1] = points[i1];
            poly.points[2] = points[i2];
            poly.points[3] = points[i3];
            poly.points[4] = points[i4];
            poly.points[5] = points[i5];
            poly.points[6] = points[i6];
            poly.points[7] = points[i7];

            poly.el_index[0] = 0;
            poly.el_index[1] = 1;
            poly.el_index[2] = 5;
            poly.el_index[3] = 4;

            poly.el_index[4] = 1;
            poly.el_index[5] = 2;
            poly.el_index[6] = 6;
            poly.el_index[7] = 5;

            poly.el_index[8] = 3;
            poly.el_index[9] = 7;
            poly.el_index[10] = 6;
            poly.el_index[11] = 2;

            poly.el_index[12] = 0;
            poly.el_index[13] = 4;
            poly.el_index[14] = 7;
            poly.el_index[15] = 3;

            poly.el_index[16] = 2;
            poly.el_index[17] = 1;
            poly.el_index[18] = 0;
            poly.el_index[19] = 3;

            poly.el_index[20] = 6;
            poly.el_index[21] = 7;
            poly.el_index[22] = 4;
            poly.el_index[23] = 5;

            poly.type = moonolith::Polyhedron<T>::HEX;
        }
    };
}  // namespace moonolith

#endif  // MOONOLITH_CONVEX_DECOMPOSITION_HPP
