#ifndef MOONOLITH_MESH_HPP
#define MOONOLITH_MESH_HPP

#include "moonolith_check_stream.hpp"
#include "moonolith_communicator.hpp"
#include "moonolith_elem.hpp"
#include "moonolith_elem_hexahedron.hpp"
#include "moonolith_elem_quad.hpp"
#include "moonolith_elem_segment.hpp"
#include "moonolith_elem_tetrahedron.hpp"
#include "moonolith_elem_triangle.hpp"
#include "moonolith_elem_type.hpp"
#include "moonolith_expanding_array.hpp"
#include "moonolith_function_space.hpp"
#include "moonolith_input_stream.hpp"
#include "moonolith_matlab_scripter.hpp"
#include "moonolith_output_stream.hpp"
#include "moonolith_serializable.hpp"
#include "moonolith_vector.hpp"
#include "par_moonolith_config.hpp"

#include "moonolith_elem_edge.hpp"
#include "moonolith_elem_edge1.hpp"
#include "moonolith_elem_edge2.hpp"
#include "moonolith_elem_edge3.hpp"

#include "moonolith_elem_tri.hpp"
#include "moonolith_elem_tri1.hpp"
#include "moonolith_elem_tri3.hpp"
#include "moonolith_elem_tri6.hpp"

#include "moonolith_elem_quad.hpp"
#include "moonolith_elem_quad1.hpp"
#include "moonolith_elem_quad4.hpp"
#include "moonolith_elem_quad8.hpp"
#include "moonolith_elem_quad9.hpp"

#include "moonolith_elem_tet.hpp"
#include "moonolith_elem_tet1.hpp"
#include "moonolith_elem_tet10.hpp"
#include "moonolith_elem_tet4.hpp"

#include "moonolith_elem_hex.hpp"
#include "moonolith_elem_hex1.hpp"
#include "moonolith_elem_hex27.hpp"
#include "moonolith_elem_hex8.hpp"

#include "moonolith_elem_prism.hpp"
#include "moonolith_elem_prism6.hpp"

#include "moonolith_synched_describable.hpp"

#include <algorithm>  // std::reverse
#include <numeric>
#include <vector>

namespace moonolith {

    template <typename T>
    class MakeNormal {
    public:
        static void apply(const ElemType &, const Storage<Integer> &, const Storage<Vector<T, 1>> &, Vector<T, 1> &n) {
            n.x = 1.0;
        }

        static void apply(const ElemType &type,
                          const Storage<Integer> &nodes,
                          const Storage<Vector<T, 2>> &points,
                          Vector<T, 2> &n) {
            using std::swap;

            switch (type) {
                case EDGE2:
                case EDGE3: {
                    const auto &p1 = points[nodes[0]];
                    const auto &p2 = points[nodes[1]];
                    n = p2 - p1;
                    swap(n.x, n.y);
                    n.x = -n.x;
                    n /= length(n);
                }

                default:
                    break;
            }
        }

        static void apply(const ElemType &type,
                          const Storage<Integer> &nodes,
                          const Storage<Vector<T, 3>> &points,
                          Vector<T, 3> &n) {
            switch (type) {
                case TRI3:
                case TRI6: {
                    const auto &p1 = points[nodes[0]];
                    const auto &p2 = points[nodes[1]];
                    const auto &p3 = points[nodes[2]];

                    n = cross(p2 - p1, p3 - p1);
                    n /= length(n);
                    break;
                }

                case QUAD4:
                case QUAD8:
                case QUAD9: {
                    const auto &p1 = points[nodes[0]];
                    const auto &p2 = points[nodes[1]];
                    const auto &p3 = points[nodes[3]];

                    n = cross(p2 - p1, p3 - p1);
                    n /= length(n);
                    break;
                }

                default:
                    break;
            }
        }
    };

    template <class MeshElem, class GeoElem>
    struct MakeElement {};

    template <class MeshElem, typename T, int PhysicalDim>
    struct MakeElement<MeshElem, Elem<T, 1, PhysicalDim>> {
        static_assert(PhysicalDim >= 1, "embedding must be equal or greater than 1");

        using Elem1 = moonolith::Elem<T, 1, PhysicalDim>;

        static void apply(const int e_type,
                          const MeshElem &e,
                          const Storage<Vector<T, PhysicalDim>> &points,
                          std::shared_ptr<Elem1> &elem) {
            switch (e_type) {
                case EDGE1: {
                    using ElemT = moonolith::Edge1<T, PhysicalDim>;

                    auto edge = std::make_shared<ElemT>();

                    for (int i = 0; i < 2; ++i) {
                        edge->point(i) = points[e.nodes[i]];
                    }

                    edge->node(0) = edge->point(0) + edge->point(1);
                    edge->node(0) *= 0.5;

                    elem = edge;
                    break;
                }
                case EDGE2: {
                    using ElemT = moonolith::Edge2<T, PhysicalDim>;

                    auto edge = std::make_shared<ElemT>();

                    for (int i = 0; i < ElemT::NNodes; ++i) {
                        edge->node(i) = points[e.nodes[i]];
                    }

                    elem = edge;
                    break;
                }
                case EDGE3: {
                    using ElemT = moonolith::Edge3<T, PhysicalDim>;

                    auto edge = std::make_shared<ElemT>();

                    for (int i = 0; i < ElemT::NNodes; ++i) {
                        edge->node(i) = points[e.nodes[i]];
                    }

                    edge->set_affine(e.is_affine);
                    elem = edge;
                    break;
                }

                default: {
                    assert(false && "implement me");
                }
            }
        }
    };

    template <class MeshElem, typename T, int PhysicalDim>
    struct MakeElement<MeshElem, Elem<T, 2, PhysicalDim>> {
        static_assert(PhysicalDim >= 2, "embedding must be equal or greater than 2");

        using Elem2 = moonolith::Elem<T, 2, PhysicalDim>;

        static void apply(const int e_type,
                          const MeshElem &e,
                          const Storage<Vector<T, PhysicalDim>> &points,
                          std::shared_ptr<Elem2> &elem) {
            switch (e_type) {
                case TRI1: {
                    using ElemT = moonolith::Tri1<T, PhysicalDim>;

                    auto triangle = std::make_shared<ElemT>();

                    for (int i = 0; i < 3; ++i) {
                        triangle->point(i) = points[e.nodes[i]];
                    }

                    triangle->node(0) = triangle->point(0) + triangle->point(1) + triangle->point(2);
                    triangle->node(0) *= 1. / 3.;

                    elem = triangle;
                    break;
                }

                case TRI3: {
                    using ElemT = moonolith::Tri3<T, PhysicalDim>;

                    auto triangle = std::make_shared<ElemT>();

                    for (int i = 0; i < ElemT::NNodes; ++i) {
                        triangle->node(i) = points[e.nodes[i]];
                    }

                    elem = triangle;
                    break;
                }

                case TRI6: {
                    using ElemT = moonolith::Tri6<T, PhysicalDim>;

                    auto triangle = std::make_shared<ElemT>();

                    for (int i = 0; i < ElemT::NNodes; ++i) {
                        triangle->node(i) = points[e.nodes[i]];
                    }

                    triangle->set_affine(e.is_affine);
                    elem = triangle;
                    break;
                }

                    /////////////////////////////////////////////////////////

                case QUAD1: {
                    using ElemT = moonolith::Quad1<T, PhysicalDim>;

                    auto quad = std::make_shared<ElemT>();

                    for (int i = 0; i < 4; ++i) {
                        quad->point(i) = points[e.nodes[i]];
                    }

                    quad->node(0) = quad->point(0);
                    quad->node(0) += quad->point(1);
                    quad->node(0) += quad->point(2);
                    quad->node(0) += quad->point(3);
                    quad->node(0) *= 1. / 4.;

                    elem = quad;
                    break;
                }

                case QUAD4: {
                    using ElemT = moonolith::Quad4<T, PhysicalDim>;

                    auto quad = std::make_shared<ElemT>();

                    for (int i = 0; i < ElemT::NNodes; ++i) {
                        quad->node(i) = points[e.nodes[i]];
                    }

                    quad->set_affine(e.is_affine);
                    elem = quad;
                    break;
                }

                case QUAD8: {
                    using ElemT = moonolith::Quad8<T, PhysicalDim>;

                    auto quad = std::make_shared<ElemT>();

                    for (int i = 0; i < ElemT::NNodes; ++i) {
                        quad->node(i) = points[e.nodes[i]];
                    }

                    quad->set_affine(e.is_affine);
                    elem = quad;
                    break;
                }

                case QUAD9: {
                    using ElemT = moonolith::Quad9<T, PhysicalDim>;

                    auto quad = std::make_shared<ElemT>();

                    for (int i = 0; i < ElemT::NNodes; ++i) {
                        quad->node(i) = points[e.nodes[i]];
                    }

                    quad->set_affine(e.is_affine);
                    elem = quad;
                    break;
                }

                default: {
                    assert(false && "should never come here");
                }
            }
        }
    };

    template <class MeshElem, typename T, int PhysicalDim>
    struct MakeElement<MeshElem, Elem<T, 3, PhysicalDim>> {
        static_assert(PhysicalDim >= 3, "embedding must be equal or greater than 3");

        using Elem3 = moonolith::Elem<T, 3, PhysicalDim>;

        static void apply(const int e_type,
                          const MeshElem &e,
                          const Storage<Vector<T, PhysicalDim>> &points,
                          std::shared_ptr<Elem3> &elem) {
            switch (e_type) {
                case TET1: {
                    using ElemT = moonolith::Tet1<T, PhysicalDim>;

                    auto tet = std::make_shared<ElemT>();

                    for (int i = 0; i < 4; ++i) {
                        tet->point(i) = points[e.nodes[i]];
                    }

                    tet->node(0) = tet->point(0);
                    tet->node(0) += tet->point(1);
                    tet->node(0) += tet->point(2);
                    tet->node(0) += tet->point(3);
                    tet->node(0) *= 1. / 4.;

                    elem = tet;
                    break;
                }

                case TET4: {
                    using ElemT = moonolith::Tet4<T, PhysicalDim>;

                    auto tet = std::make_shared<ElemT>();

                    for (int i = 0; i < ElemT::NNodes; ++i) {
                        tet->node(i) = points[e.nodes[i]];
                    }

                    elem = tet;
                    break;
                }

                case TET10: {
                    using ElemT = moonolith::Tet10<T, PhysicalDim>;

                    auto tet = std::make_shared<ElemT>();

                    for (int i = 0; i < ElemT::NNodes; ++i) {
                        tet->node(i) = points[e.nodes[i]];
                    }

                    tet->set_affine(e.is_affine);
                    elem = tet;
                    break;
                }

                    /////////////////////////////////////////////////////////

                case HEX1: {
                    using ElemT = moonolith::Hex1<T, PhysicalDim>;
                    auto hex = std::make_shared<ElemT>();

                    for (int i = 0; i < 8; ++i) {
                        hex->point(i) = points[e.nodes[i]];
                    }

                    hex->node(0) = hex->point(0);
                    for (int i = 1; i < 8; ++i) {
                        hex->node(0) += hex->point(i);
                    }

                    hex->node(0) *= 1. / 8.;

                    elem = hex;
                    break;
                }

                case HEX8: {
                    using ElemT = moonolith::Hex8<T, PhysicalDim>;

                    auto hex = std::make_shared<ElemT>();

                    for (int i = 0; i < ElemT::NNodes; ++i) {
                        hex->node(i) = points[e.nodes[i]];
                    }

                    hex->set_affine(e.is_affine);
                    elem = hex;
                    break;
                }

                case HEX27: {
                    using ElemT = moonolith::Hex27<T, PhysicalDim>;

                    auto hex = std::make_shared<ElemT>();

                    for (int i = 0; i < ElemT::NNodes; ++i) {
                        hex->node(i) = points[e.nodes[i]];
                    }

                    hex->set_affine(e.is_affine);
                    elem = hex;
                    break;
                }

                case PRISM6: {
                    using ElemT = moonolith::Prism6<T, PhysicalDim>;

                    auto prism = std::make_shared<ElemT>();

                    for (int i = 0; i < ElemT::NNodes; ++i) {
                        prism->node(i) = points[e.nodes[i]];
                    }

                    prism->set_affine(e.is_affine);
                    elem = prism;
                    break;
                }
            }
        }
    };

    template <class MeshElem, typename T, int PhysicalDim>
    struct MakeElement<MeshElem, PhysicalElem<T, PhysicalDim>> {
        using PhysicalElem = moonolith::PhysicalElem<T, PhysicalDim>;

    public:
        using Point1 = moonolith::Vector<T, 1>;
        using Point2 = moonolith::Vector<T, 2>;
        using Point3 = moonolith::Vector<T, 3>;

        // 1-manifold
        using Elem11 = moonolith::Elem<T, 1, 1>;
        using Elem12 = moonolith::Elem<T, 1, 2>;
        using Elem13 = moonolith::Elem<T, 1, 3>;

        // 2-manifold
        using Elem22 = moonolith::Elem<T, 2, 2>;
        using Elem23 = moonolith::Elem<T, 2, 3>;

        // 3-manifold
        using Elem33 = moonolith::Elem<T, 3, 3>;

        // physical elements
        using PhysicalElem1 = moonolith::PhysicalElem<T, 1>;
        using PhysicalElem2 = moonolith::PhysicalElem<T, 2>;
        using PhysicalElem3 = moonolith::PhysicalElem<T, 3>;

        static void apply(const MeshElem &e,
                          const Storage<Vector<T, PhysicalDim>> &points,
                          std::shared_ptr<PhysicalElem> &elem) {
            apply(e.type, e, points, elem);
        }

        static void apply(const int e_type,
                          const MeshElem &e,
                          const Storage<Point3> &points,
                          std::shared_ptr<PhysicalElem3> &elem) {
            switch (e.dim()) {
                case 1: {
                    std::shared_ptr<Elem13> temp_elem;
                    MakeElement<MeshElem, Elem13>::apply(e_type, e, points, temp_elem);
                    elem = temp_elem;
                    return;
                }
                case 2: {
                    std::shared_ptr<Elem23> temp_elem;
                    MakeElement<MeshElem, Elem23>::apply(e_type, e, points, temp_elem);
                    elem = temp_elem;
                    return;
                }
                case 3: {
                    std::shared_ptr<Elem33> temp_elem;
                    MakeElement<MeshElem, Elem33>::apply(e_type, e, points, temp_elem);
                    elem = temp_elem;
                    return;
                }
                default: {
                    assert(false);
                }
            }
        }

        static void apply(const int e_type,
                          const MeshElem &e,
                          const Storage<Point2> &points,
                          std::shared_ptr<PhysicalElem2> &elem) {
            switch (e.dim()) {
                case 1: {
                    std::shared_ptr<Elem12> temp_elem;
                    MakeElement<MeshElem, Elem12>::apply(e_type, e, points, temp_elem);
                    elem = temp_elem;
                    return;
                }
                case 2: {
                    std::shared_ptr<Elem22> temp_elem;
                    MakeElement<MeshElem, Elem22>::apply(e_type, e, points, temp_elem);
                    elem = temp_elem;
                    return;
                }
                default: {
                    assert(false);
                }
            }
        }

        static void apply(const int e_type,
                          const MeshElem &e,
                          const Storage<Point1> &points,
                          std::shared_ptr<PhysicalElem1> &elem) {
            std::shared_ptr<Elem11> temp_elem;
            MakeElement<MeshElem, Elem11>::apply(e_type, e, points, temp_elem);
            elem = temp_elem;
        }
    };

    template <typename T>
    class IMesh : public SynchedDescribable {
    public:
        virtual ~IMesh() = default;
    };

    template <typename T_, int Dim_>
    class Mesh : public IMesh<T_> {
    public:
        static const int Dim = Dim_;
        using T = T_;
        using Point = moonolith::Vector<T, Dim>;
        using PhysicalElem = moonolith::PhysicalElem<T, Dim>;

        class Elem {
        public:
            int type;
            int block;
            Integer local_idx;
            Integer global_idx;
            Storage<Integer> nodes;
            bool is_affine;

            Elem() : type(-1), block(-1), local_idx(-1), global_idx(-1), nodes(), is_affine(false) {}

            inline Integer node(const Integer idx) const { return nodes[idx]; }

            inline Integer n_nodes() const { return static_cast<Integer>(nodes.size()); }

            void invert_orientation() { std::reverse(nodes.begin(), nodes.end()); }

            inline void normal(const Storage<Point> &points, Vector<T, Dim> &n) const {
                MakeNormal<T>::apply(ElemType(type), nodes, points, n);
            }

            inline T measure(const Storage<Point> &points) const {
                std::shared_ptr<PhysicalElem> elem;
                MakeElement<Elem, PhysicalElem>::apply(*this, points, elem);
                return elem->measure();
            }

            inline T approx_measure(const Storage<Point> &points) const {
                std::shared_ptr<PhysicalElem> elem;
                MakeElement<Elem, PhysicalElem>::apply(*this, points, elem);
                return elem->approx_measure();
            }

            template <int PreDim>
            inline void make_element(const Storage<Point> &points,
                                     std::shared_ptr<moonolith::Elem<T, PreDim, Dim>> &e_ptr) const {
                MakeElement<Elem, moonolith::Elem<T, PreDim, Dim>>::apply(type, *this, points, e_ptr);
            }

            template <int PreDim>
            inline void make_element(const FEType fe_type,
                                     const Storage<Point> &points,
                                     std::shared_ptr<moonolith::Elem<T, PreDim, Dim>> &e_ptr) const {
                MakeElement<Elem, moonolith::Elem<T, PreDim, Dim>>::apply(fe_type, *this, points, e_ptr);
            }

            inline int dim() const {
                switch (type) {
                    case EDGE1:
                    case EDGE2:
                    case EDGE3: {
                        return 1;
                    }

                    case TRI1:
                    case TRI3:
                    case TRI6:
                    case QUAD1:
                    case QUAD4:
                    case QUAD8:
                    case QUAD9: {
                        return 2;
                    }

                    default: {
                        return 3;
                    }
                }
            }
        };

        template <class Bound>
        void fill_bound(const Integer handle, Bound &bound, const T blow_up) const {
            const auto &e = this->elem(handle);
            const auto nne = e.n_nodes();

            if (blow_up != 0.0) {
                Point p, q;

                Vector<T, Dim> nn;
                fill(nn, static_cast<T>(1.0));

                e.normal(this->nodes_, nn);

                for (Integer i = 0; i < nne; ++i) {
                    auto n = e.node(i);
                    q = this->point(n);

                    for (Integer d = 0; d < Dim; ++d) {
                        p[d] = q[d] + blow_up * nn[d];
                    }

                    bound += p;

                    for (Integer d = 0; d < Dim; ++d) {
                        p[d] = q[d] - blow_up * nn[d];
                    }

                    bound += p;
                }

            } else {
                for (Integer i = 0; i < nne; ++i) {
                    auto n = e.node(i);
                    bound += this->point(n);
                }
            }
        }

        template <class Iterator>
        void serialize(const Iterator &begin, const Iterator &end, OutputStream &os) const {
            CHECK_STREAM_WRITE_BEGIN("serialize", os);

            Integer n_elements = static_cast<Integer>(std::distance(begin, end));

            os << n_elements;

            if (n_elements == 0) {
                CHECK_STREAM_WRITE_END("serialize", os);
                return;
            }

            std::vector<Integer> node_index(this->n_nodes(), 0);

            for (auto it = begin; it != end; ++it) {
                const auto &e = this->elem(*it);

                const auto enn = e.n_nodes();

                for (Integer i = 0; i < enn; ++i) {
                    node_index[e.node(i)] = 1;
                }
            }

            Integer n_nodes = std::accumulate(std::begin(node_index), std::end(node_index), 0);

            os << n_nodes;

            Integer cum_sum = 0;
            for (auto &n : node_index) {
                if (n == 1) {
                    cum_sum += n;
                    n = cum_sum;
                }
            }

            std::for_each(std::begin(node_index), std::end(node_index), [](Integer &v) { v -= 1; });

            // write points
            for (Integer i = 0; i < this->n_nodes(); ++i) {
                if (node_index[i] >= 0) {
                    const auto &p = this->point(i);

                    for (Integer d = 0; d < Dim; ++d) {
                        os << p[d];
                    }
                }
            }

            // write element connectivity
            for (auto it = begin; it != end; ++it) {
                const auto &e = this->elem(*it);
                Integer nne = e.n_nodes();

                os << nne;
                os << e.block;
                os << e.global_idx;
                os << e.type;
                os << e.is_affine;

                const auto enn = e.n_nodes();
                for (Integer i = 0; i < enn; ++i) {
                    auto n = e.node(i);
                    assert(node_index[n] >= 0);
                    os << node_index[n];
                }
            }

            CHECK_STREAM_WRITE_END("serialize", os);
        }

        void build(InputStream &is) {
            CHECK_STREAM_READ_BEGIN("serialize", is);

            Integer n_elements, n_nodes;
            is >> n_elements;

            if (n_elements == 0) {
                CHECK_STREAM_READ_END("serialize", is);
                return;
            }

            is >> n_nodes;

            this->resize(n_elements, n_nodes);

            for (Integer i = 0; i < n_nodes; ++i) {
                auto &p = nodes_[i];

                for (Integer d = 0; d < Dim; ++d) {
                    is >> p[d];
                }
            }

            elements_.resize(n_elements);

            for (Integer i = 0; i < n_elements; ++i) {
                auto &e = this->elem(i);

                Integer nne = -1;

                is >> nne;
                is >> e.block;
                is >> e.global_idx;
                is >> e.type;
                is >> e.is_affine;

                e.nodes.resize(nne);

                for (auto &n : e.nodes) {
                    is >> n;
                }
            }

            CHECK_STREAM_READ_END("serialize", is);
        }

        Mesh(const Communicator &comm) : comm_(comm.get()), local_element_offset_(-1), manifold_dim_(-1) {}

        inline void reserve(const Integer n_elems, const Integer n_points) {
            elements_.reserve(n_elems);
            nodes_.reserve(n_points);
        }

        inline void resize(const Integer n_elems, const Integer n_points) {
            elements_.resize(n_elems);
            for (Integer i = 0; i < n_elems; ++i) {
                elements_[i].local_idx = i;
            }

            nodes_.resize(n_points);
        }

        inline void clear() {
            elements_.clear();
            nodes_.clear();
            local_element_offset_ = -1;
        }

        inline Integer n_nodes() const { return static_cast<Integer>(nodes_.size()); }

        inline Integer n_elements() const { return static_cast<Integer>(elements_.size()); }

        inline Elem &elem(const Integer idx) {
            assert(idx < n_elements());
            return elements_[idx];
        }

        inline const Elem &elem(const Integer idx) const {
            assert(idx < n_elements());
            return elements_[idx];
        }

        inline Point &point(const Integer idx) {
            assert(idx < n_nodes());
            return nodes_[idx];
        }

        inline const Point &point(const Integer idx) const {
            assert(idx < n_nodes());
            return nodes_[idx];
        }

        inline Storage<Point> &nodes() { return nodes_; }

        inline const Storage<Point> &nodes() const { return nodes_; }


        inline const Storage<Elem> &elements() const {
            return elements_;
        }

        inline void generate_global_element_ids() {
            Integer ne = n_elements();
            local_element_offset_ = 0;

            comm_.exscan(&ne, &local_element_offset_, 1, MPISum());

            for (Integer i = 0; i < ne; ++i) {
                elem(i).global_idx = i + local_element_offset_;
            }
        }

        inline void finalize() { generate_global_element_ids(); }

        void draw(MatlabScripter &script) const {
            script.hold_on();

            Storage<Point> pts;
            Polygon<T, Dim> poly;
            for (const auto &e : elements_) {
                switch (e.type) {
                    case EDGE2: {
                        pts.resize(2);
                        pts[0] = nodes()[e.nodes[0]];
                        pts[1] = nodes()[e.nodes[1]];
                        script.plot(pts, "\'b.-\'");
                        break;
                    }

                    case EDGE3: {
                        pts.resize(3);
                        pts[0] = nodes()[e.nodes[0]];
                        pts[1] = nodes()[e.nodes[2]];
                        pts[2] = nodes()[e.nodes[1]];
                        script.plot(pts, "\'b.-\'");
                        break;
                    }

                    case TRI3: {
                        poly.resize(3);
                        poly[0] = nodes()[e.nodes[0]];
                        poly[1] = nodes()[e.nodes[1]];
                        poly[2] = nodes()[e.nodes[2]];
                        script.plot(poly, "\'b.-\'");
                        break;
                    }

                    case TRI6: {
                        poly.resize(6);
                        poly[0] = nodes()[e.nodes[0]];
                        poly[1] = nodes()[e.nodes[3]];
                        poly[2] = nodes()[e.nodes[1]];
                        poly[3] = nodes()[e.nodes[4]];
                        poly[4] = nodes()[e.nodes[2]];
                        poly[5] = nodes()[e.nodes[5]];
                        script.plot(poly, "\'b.-\'");
                        break;
                    }

                    case QUAD4: {
                        poly.resize(4);
                        poly[0] = nodes()[e.nodes[0]];
                        poly[1] = nodes()[e.nodes[1]];
                        poly[2] = nodes()[e.nodes[2]];
                        poly[4] = nodes()[e.nodes[4]];
                        script.plot(poly, "\'b.-\'");
                        break;
                    }
                    case QUAD9: {
                        script.plot(nodes()[e.nodes[8]], "\'b*\'");
                        // then runs the case for QUAD8
                    }
                    // fall through
                    case QUAD8: {
                        poly.resize(8);
                        poly[0] = nodes()[e.nodes[0]];
                        poly[1] = nodes()[e.nodes[4]];
                        poly[2] = nodes()[e.nodes[1]];
                        poly[3] = nodes()[e.nodes[5]];
                        poly[4] = nodes()[e.nodes[2]];
                        poly[5] = nodes()[e.nodes[6]];
                        poly[6] = nodes()[e.nodes[3]];
                        poly[7] = nodes()[e.nodes[7]];
                        script.plot(poly, "\'b.-\'");
                        break;
                    }

                    default: {
                        std::cerr << "Mesh::draw: unimplemented element" << std::endl;
                        break;
                    }
                }
            }
        }

        void measure(Storage<T> &meas) const {
            const auto ne = elements_.size();
            meas.resize(ne);
            for (std::size_t i = 0; i < ne; ++i) {
                meas[i] = elements_[i].measure(nodes_);
            }
        }

        void approx_measure(Storage<T> &meas) const {
            const auto ne = elements_.size();
            meas.resize(ne);
            for (std::size_t i = 0; i < ne; ++i) {
                meas[i] = elements_[i].approx_measure(nodes_);
            }
        }

        inline Integer local_element_offset() const {
            assert(local_element_offset_ != -1 && "make sure to call Mesh::finalize before using the mesh");
            return local_element_offset_;
        }

        inline Integer add_node(const Point &p) {
            Integer idx = static_cast<Integer>(nodes_.size());
            nodes_.push_back(p);
            return idx;
        }

        inline Point &add_node() {
            nodes_.emplace_back();
            return nodes_.back();
        }

        inline Point &node(const Integer idx) { return nodes_[idx]; }

        inline const Point &node(const Integer idx) const { return nodes_[idx]; }

        Elem &add_elem() {
            Integer local_idx = static_cast<Integer>(elements_.size());
            elements_.emplace_back();
            elements_.back().local_idx = local_idx;
            return elements_.back();
        }

        inline void set_manifold_dim(const int dim) { manifold_dim_ = dim; }

        inline int manifold_dim() const { return manifold_dim_; }

        Communicator &comm() { return comm_; }

        const Communicator &comm() const { return comm_; }

        inline void scale(const T &factor) {
            for (auto &p : nodes_) {
                p *= factor;
            }
        }

        void describe(std::ostream &os) const override {
            if (comm_.rank() == 0) {
                os << "manifold_dim:\t" << manifold_dim_ << '\n';
            }

            os << "num_local_elements:\t" << elements_.size() << '\n';
            os << "num_local_nodes:\t" << nodes_.size() << '\n';
            os << "local_element_offset:\t" << local_element_offset_ << '\n';
        }

        void invert_element_orientation() {
            for (auto &e : elements_) {
                e.invert_orientation();
            }
        }

    private:
        Communicator comm_;
        Storage<Elem> elements_;
        Storage<Point> nodes_;

        Integer local_element_offset_;
        int manifold_dim_;
    };

    template <typename T, int Dim, int PhysicalDim>
    inline void make(const Mesh<T, PhysicalDim> &mesh,
                     const typename Mesh<T, PhysicalDim>::Elem &e,
                     std::shared_ptr<Elem<T, Dim, PhysicalDim>> &elem) {
        e.make_element(mesh.nodes(), elem);
    }

    template <typename T, int Dim, int PhysicalDim>
    inline void make(const FunctionSpace<Mesh<T, PhysicalDim>> &space,
                     const typename Mesh<T, PhysicalDim>::Elem &e,
                     std::shared_ptr<Elem<T, Dim, PhysicalDim>> &elem) {
        auto local_e_idx = e.local_idx;
        e.make_element(space.dof_map().type(local_e_idx), space.mesh().nodes(), elem);
    }

}  // namespace moonolith

#endif  // MOONOLITH_MESH_HPP