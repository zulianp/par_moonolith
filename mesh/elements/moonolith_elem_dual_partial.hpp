#ifndef MOONOLITH_ELEM_DUAL_PARTIAL_HPP
#define MOONOLITH_ELEM_DUAL_PARTIAL_HPP

#include "moonolith_assembly.hpp"
#include "moonolith_elem.hpp"
#include "moonolith_expanding_array.hpp"
#include "moonolith_fe.hpp"
#include "moonolith_matrix.hpp"
#include "moonolith_transformed_fe.hpp"
#include "moonolith_vector.hpp"

namespace moonolith {

    template <class Elem>
    class DualWeightsPartial {
    public:
        static const int NNodes = Elem::NNodes;
        static const int Dim = Elem::Dim;
        using T = typename Elem::T;
        using Quadrature = moonolith::Quadrature<T, Dim>;

        void weights(Elem &e, const Storage<Quadrature> &qs, Matrix<T, NNodes, NNodes> &w) {
            const int n = e.n_nodes();

            resize(w, n, n);
            w.zero();

            resize(mass_matrix_, n, n);
            mass_matrix_.zero();

            if (e.order() > 1) {
                for (const auto &q : qs) {
                    if (q.empty()) continue;

                    fe_.init(e, q, true, false, true);
                    trafo_fe_.init(fe_);
                    assemble_mass_matrix_add(trafo_fe_, trafo_fe_, mass_matrix_);
                }

            } else {
                for (const auto &q : qs) {
                    if (q.empty()) continue;

                    fe_.init(e, q, true, false, true);
                    assemble_mass_matrix_add(fe_, fe_, mass_matrix_);
                }
            }

            mass_matrix_.sum_cols(diag_);
            resize(inv_mass_matrix_, mass_matrix_.rows(), mass_matrix_.cols());
            invert(mass_matrix_.values, inv_mass_matrix_.values);

            w = inv_mass_matrix_;

            for (int i = 0; i < n; ++i) {
                for (int j = 0; j < n; ++j) {
                    w(i, j) *= diag_[i];
                }
            }
        }

    private:
        FE<Elem> fe_;
        TransformedFE<Elem> trafo_fe_;
        Matrix<T, NNodes, NNodes> mass_matrix_;
        Matrix<T, NNodes, NNodes> inv_mass_matrix_;
        Vector<T, NNodes> diag_;
        // Vector<T, NNodes> col_;
    };

}  // namespace moonolith

#endif  // MOONOLITH_ELEM_DUAL_PARTIAL_HPP
