#ifndef MATRIX_CL
#define MATRIX_CL 

#include "kernel_base.cl"
#include "algebra_interfaces.cl"

bool approx_equal(const Scalar left, const Scalar right, const Scalar tol)
{
	return fabs(left - right) < tol;
}


#define generic_add(x, y) (x) + (y)
#define vector_reduce_aux(n, vec, result, fun) 				\
{													 		\
	if(n > 0) { 											\
		result = vec[0]; 									\
		for(SizeType i = 1; i < n; ++i) {					\
			result = fun(result, vec[i]);					\
		}													\
	} 														\
	else { 													\
		result = 0; 										\
	} 														\
}

void identity_3(Scalar * matrix)
{
	matrix[0] = 1;
	matrix[1] = 0;
	matrix[2] = 0;

	matrix[3] = 0;
	matrix[4] = 1;
	matrix[5] = 0;

	matrix[6] = 0;
	matrix[7] = 0;
	matrix[8] = 1;
}

static Scalar add_n(const SizeType n_entries, const Scalar *entries)
{
	Scalar result = 0;
	vector_reduce_aux(n_entries, entries, result, generic_add);
	return result;
}


static bool has_nan_entries(const SizeType n_entries, const Scalar *entries)
{
	for(SizeType i = 0; i < n_entries; ++i) {
		if(entries[i] == entries[i]) { continue; }
		else { return true; }
	}
	
	return false;
}

static Scalar min_n(const SizeType n, const Scalar *values)
{
	Scalar result;
	vector_reduce_aux(n, values, result, min);
	return result;
}

static Scalar max_n(const SizeType n, const Scalar *values)
{
	Scalar result;
	vector_reduce_aux(n, values, result, max);
	return result;
}

Scalar dot_2(const Scalar *left, const Scalar *right)
{
	return left[0]*right[0] + left[1]*right[1];
}

static Scalar dot_3(const Scalar *left, const Scalar *right)
{
	return left[0]*right[0] + left[1]*right[1] + left[2]*right[2];
}

static Scalar dot_n(const SizeType n, const Scalar *left, const Scalar *right)
{
	Scalar result = 0;
	for(SizeType i = 0; i < n; ++i) {
		result += left[i] * right[i];
	}

	return result;
}

Scalar matrix_dot_n_t(const SizeType n_rows, const Scalar *left, const Scalar *right)
{
	Scalar result = 0;

	SizeType index = 0;
	for(SizeType i = 0; i < n_rows; ++i) {
		for(SizeType j = 0; j < n_rows; ++j) {

			const SizeType offset_j = j * n_rows;
			result += left[index++] * right[offset_j + i];
		}
	}

	return result;
}

Scalar trace(const SizeType rows, const SizeType cols, const Scalar *matrix)
{
	Scalar result = matrix[0];
	for(SizeType i = 1; i < rows; ++i) {
		const SizeType index = i*(cols+1);
		result += matrix[index];
	}

	return result;
}

void set(const SizeType n_entries, const Scalar value, Scalar *values)
{
	for(SizeType i = 0; i < n_entries; ++i) { values[i] = value; }
}


Scalar det_n(const SizeType n_dims, const Scalar *mat) 
{
	switch(n_dims) {
		case 2: { return det_2(mat); }
		case 3: { return det_3(mat); }
		default: { REPORT_OUT_OF_RANGE_ERROR("det_n: undefined for specific dim", n_dims, 2, 3); return 0; }
	}

	return 0;
}



static Scalar det_3(const Scalar *mat)
{
	return mat[0] * mat[4] * mat[8] + 
	mat[1] * mat[5] * mat[6] + 
	mat[2] * mat[3] * mat[7] - 
	mat[0] * mat[5] * mat[7] - 
	mat[1] * mat[3] * mat[8] - 
	mat[2] * mat[4] * mat[6]; 
}

//layout [a_00, a_01, a_10, a_11]
static Scalar det_2(const Scalar *mat)
{
	return mat[0] * mat[3] - mat[2] * mat[1];
}

void inverse_n(const SizeType n_dims, const Scalar *in, const Scalar det, Scalar *out)
{
	switch(n_dims) {
		case 2: { inverse_2(in, det, out); break; }
		case 3: { inverse_3(in, det, out); break; }
		default: { REPORT_OUT_OF_RANGE_ERROR("inverse_n: undefined for specific dim", n_dims, 2, 3); }
	}
}

static void inverse_2(const Scalar *in, const Scalar det, Scalar *out) {
	out[0] =  in[3]/det;
	out[1] = -in[1]/det;
	out[2] = -in[2]/det;
	out[3] =  in[0]/det;
}

static void inverse_3(const Scalar *in, const Scalar det, Scalar *out) {
	out[0] = (in[4]*in[8] - in[5]*in[7])/det;
	out[1] = (in[2]*in[7] - in[1]*in[8])/det;
	out[2] = (in[1]*in[5] - in[2]*in[4])/det;
	out[3] = (in[5]*in[6] - in[3]*in[8])/det;
	out[4] = (in[0]*in[8] - in[2]*in[6])/det;
	out[5] = (in[2]*in[3] - in[0]*in[5])/det;
	out[6] = (in[3]*in[7] - in[4]*in[6])/det;
	out[7] = (in[1]*in[6] - in[0]*in[7])/det;
	out[8] = (in[0]*in[4] - in[1]*in[3])/det;
}

static Scalar norm_n(const SizeType n_dims, const Scalar *vector)
{
	Scalar result = vector[0] * vector[0];
	for(SizeType i = 1; i < n_dims; ++i) {
		result += vector[i] * vector[i];
	}

	return sqrt(result);
}

Scalar distance_n(const SizeType n_dims, const Scalar *left, const Scalar *right) {
	Scalar temp = left[0] - right[0];
	Scalar result = temp * temp;
	for(SizeType i = 1; i < n_dims; ++i) {
		temp = left[i] - right[i];
		result += temp * temp;
	}
	return sqrt(result);
}

static void mat_vec_mul(const SizeType rows, const SizeType cols, const Scalar *matrix, const Scalar *vector, Scalar *result)
{	
	for(SizeType i = 0; i < rows; ++i) {
		const SizeType offset = i * cols;
		result[i] = matrix[offset] * vector[0];

		for(SizeType j = 1; j < cols; ++j) {
			result[i] += matrix[offset+j] * vector[j];
		}
	}
}

static void vec_scale(const SizeType n_entries, const Scalar scale_factor, Scalar *vector)
{
	for(SizeType i = 0; i < n_entries; ++i) {
		vector[i] *= scale_factor;
	}
}

static void vec_minus(const SizeType n_entries, const Scalar *left, const Scalar *right, Scalar *result)
{	
	for(SizeType i = 0; i < n_entries; ++i) {
		result[i] = left[i] - right[i];
	}
}

void mat_plus_mat_transposed(const SizeType rows_and_cols, const Scalar scale_left, const Scalar *left, const Scalar scale_right, const Scalar *right, Scalar *result)
{
	for(SizeType i = 0; i < rows_and_cols; ++i) {
		const SizeType offsetI = i*rows_and_cols;

		for(SizeType j = 0; j < rows_and_cols; ++j) {
			const SizeType offsetJ = j*rows_and_cols;
			result[offsetI+j] = scale_left * left[offsetI+j] + scale_right * right[offsetJ+i];
		}
	}
}

void mat_symmetrized(const SizeType rows_and_cols, const Scalar *matrix, Scalar *result)
{
	mat_plus_mat_transposed(rows_and_cols, 1, matrix, 1, matrix, result);
	vec_scale(rows_and_cols*rows_and_cols, 0.5, result);
}

void mat_symmetrize(const SizeType rows_and_cols, Scalar *matrix)
{
	for(SizeType i = 0; i < rows_and_cols; ++i) {

		const SizeType offset_i = i * rows_and_cols;

		for(SizeType j = i+1; j < rows_and_cols; ++j) {

			const SizeType offset_j = j * rows_and_cols;
			const Scalar entry = 0.5 * (matrix[offset_i + j]  + matrix[offset_j + i]);

			matrix[offset_i + j] = entry;
			matrix[offset_j + i] = entry;
		}
	}
}

void set_block_to_identity(const SizeType block_i, 	const SizeType block_j, 
	const SizeType block_rows, const SizeType block_columns, 
	const SizeType n_block_rows, const SizeType n_block_columns,
	const Scalar diag_scaling, Scalar * matrix)
{
	const SizeType n_block_entries = block_rows * block_columns;
	const SizeType offset = (block_i * n_block_columns + block_j) * n_block_entries;

	for(SizeType i = 0; i < block_rows; ++i) {
		const SizeType block_offset_i = offset + i * block_columns;
		for(SizeType j = 0; j < block_columns; ++j) {
			matrix[block_offset_i + j] = ((Scalar)(i == j)) * diag_scaling;
		}
	}
}

void transpose(const SizeType rows_and_cols, const Scalar *mat, Scalar *result)
{
	if(mat == result) {
		for(SizeType i = 0; i < rows_and_cols; ++i) {
			const SizeType offset_i = i*rows_and_cols;
			for(SizeType j = i+1; j < rows_and_cols; ++j) {
				const SizeType offset_j = j*rows_and_cols;
				
				const Scalar temp = result[offset_i+j];
				result[offset_i+j] = result[offset_j+i];
				result[offset_j+i] = temp;
			}
		} 	
	} else {
		for(SizeType i = 0; i < rows_and_cols; ++i) {
			const SizeType offset_i = i*rows_and_cols;
			for(SizeType j = 0; j < rows_and_cols; ++j) {
				const SizeType offset_j = j*rows_and_cols;
				result[offset_i+j] = mat[offset_j+i];
			}
		} 
	}
}

static void axpy(const SizeType n_entries, const Scalar alpha, const Scalar *x, Scalar *y)
{
	for(SizeType i = 0; i < n_entries; ++i) {
		y[i] += alpha*x[i];
	}
}

void mat_times_self_transposed(const SizeType rows_and_cols, const Scalar *mat, Scalar * result) 
{
	for(SizeType i = 0; i < rows_and_cols; ++i) {
		const SizeType offset_i = i*rows_and_cols;
		for(SizeType j = 0; j < rows_and_cols; ++j) {
			const SizeType offset_ij = offset_i + j;

			result[offset_ij] = 0;
			for(SizeType k = 0; k < rows_and_cols; ++k) {
				result[offset_ij] += mat[offset_i + k] * mat[k * rows_and_cols + j];
			}
		}
	}
}

void mat_mat_mul(const SizeType left_rows, const SizeType left_columns, const SizeType right_columns,  const Scalar *left, const Scalar *right, Scalar *result)
{
	for(SizeType i = 0; i < left_rows; ++i) {
		const SizeType offset_i = i*right_columns;

		for(SizeType j = 0; j < right_columns; ++j) {
			const SizeType offset_ij = offset_i + j;

			result[offset_ij] = 0;
			for(SizeType k = 0; k < left_columns; ++k) {
				result[offset_ij] += left[offset_i + k] * right[k * right_columns + j];
			}
		}
	}
}

void outer_product(const SizeType size_left, const SizeType size_right, const Scalar *left, const Scalar *right, Scalar * result)
{
	for(SizeType i = 0; i < size_left; ++i) {
		const SizeType offset_i = i * size_right;
		for(SizeType j = 0; j < size_right; ++j) {
			result[offset_i+j] = left[i] * right[j];
		}
	}
}	


static void matrix_print(const SizeType rows, const SizeType cols, const Scalar *matrix) {
	printf("[\n"); //]
	for(SizeType i = 0; i < rows; ++i) {
		for(SizeType j = 0; j < cols; ++j) {
			printf("\t%g", matrix[i*cols+j]);
		}
		printf("\n");
	}

	//[
	printf("]\n");
}

void g_matrix_print(const SizeType rows, const SizeType cols, m_global__ const Scalar *matrix) {
	printf("[\n"); //]
	for(SizeType i = 0; i < rows; ++i) {
		for(SizeType j = 0; j < cols; ++j) {
			printf("\t%g", matrix[i*cols+j]);
		}
		printf("\n");
	}

	//[
	printf("]\n");
}


void g_int_matrix_print(const SizeType rows, const SizeType cols, m_global__ const SizeType *matrix) {

	printf("[\n"); //]
	for(SizeType i = 0; i < rows; ++i) {
		for(SizeType j = 0; j < cols; ++j) {
			printf("\t%d", matrix[i*cols+j]);
		}
		printf("\n");
	}

	//[
	printf("]\n");
}

void block_matrix_print(const SizeType n_block_rows, 
	const SizeType n_block_columns, 
	const SizeType block_rows, 
	const SizeType block_columns, const Scalar *matrix) 
{
	const SizeType n_block_entries = block_rows * block_columns;
	
	printf("-----------------------------------\n");
	for(SizeType block_i = 0; block_i < n_block_rows; ++block_i) {
		for(SizeType block_j = 0; block_j < n_block_columns; ++block_j) {

			printf("(%d, %d)[\n", block_i, block_j); //]
			const SizeType offset = (block_i * n_block_columns + block_j) * n_block_entries;
			for(SizeType i = 0; i < block_rows; ++i) {
				const SizeType block_offset_i = offset + i * block_columns;
				printf("\t");
				for(SizeType j = 0; j < block_columns; ++j) {
					printf("%g\t", matrix[block_offset_i + j]);
				}
				printf("\n");
			}	
			//[
			printf("]\n");
		}
	}
	printf("-----------------------------------\n");
}


void matrixdxd_print(const SizeType n_dims, const SizeType n_matrices, Matrixdxd *matrices)
{
	for(SizeType i = 0; i < n_matrices; ++i) {
		printf("%d)\n",i);
		matrix_print(n_dims, n_dims, matrices[i].entries); 
	}
}

static SizeType symmetric_matrix_n_entries(const SizeType n_rows)
{
	return ( n_rows * (n_rows + 1) )/2;
}

static SizeType symmetric_matrix_index(const SizeType i, const SizeType j)
{
	if(j > i) {
		return ( j * (j + 1) )/2 + i;
	} else {
		return ( i * (i + 1) )/2 + j;
	}
}

static void householder_reflection_2(const Scalar *vec, Scalar *result_matrix)
{
	//row 0
	result_matrix[0] = - 2.0 * vec[0] * vec[0] + 1.0;
	result_matrix[1] = - 2.0 * vec[0] * vec[1];


	//row 1
	result_matrix[2] = result_matrix[1];
	result_matrix[3] = - 2.0 * vec[1] * vec[1] + 1.0;
}


static void householder_reflection_3(const Scalar *vec, Scalar *result_matrix)
{
	//row 0
	result_matrix[0] = - 2.0 * vec[0] * vec[0] + 1.0;
	result_matrix[1] = - 2.0 * vec[0] * vec[1];
	result_matrix[2] = - 2.0 * vec[0] * vec[2];

	//row 1
	result_matrix[3] = result_matrix[1];
	result_matrix[4] = - 2.0 * vec[1] * vec[1] + 1.0;
	result_matrix[5] = - 2.0 * vec[1] * vec[2];
	
	//row 2
	result_matrix[6] = result_matrix[2];
	result_matrix[7] = result_matrix[5];
	result_matrix[8] = - 2.0 * vec[2] * vec[2] + 1.0;

	KERNEL_ASSERT(result_matrix[3] + 2.0 * vec[1] * vec[0] < DEFAULT_TOLLERANCE, "householder_reflection_3");
	KERNEL_ASSERT(result_matrix[6] + 2.0 * vec[2] * vec[0] < DEFAULT_TOLLERANCE, "householder_reflection_3");
	KERNEL_ASSERT(result_matrix[7] + 2.0 * vec[2] * vec[1] < DEFAULT_TOLLERANCE, "householder_reflection_3");
}


void col_average(const SizeType n_rows, const SizeType n_cols, const Scalar *matrix, Scalar *result)
{
	for(SizeType i = 0; i < n_rows; ++i) {
		const SizeType offset_i = i * n_cols;

		result[i] = matrix[offset_i];

		for(SizeType j = 1; j < n_cols; ++j) {
			result[i] += matrix[offset_i + j];
		}

		result[i] /= n_cols;
	}
}

void col_subtract(const SizeType n_rows, const SizeType n_cols, const Scalar *col_vector, Scalar *matrix)
{
	for(SizeType i = 0; i < n_rows; ++i) {
		const SizeType offset_i = i * n_cols;

		for(SizeType j = 0; j < n_cols; ++j) {
			matrix[offset_i + j] -= col_vector[i];
		}
	}
}

void col_arg_min(const SizeType n_rows, const SizeType n_cols, const Scalar *matrix, SizeType *i_min)
{
	for(SizeType i = 0; i < n_rows; ++i) {
		const SizeType offset_i = i * n_cols;

		Scalar min_value = matrix[offset_i];	
		i_min[i] = 0;
		
		for(SizeType j = 0; j < n_cols; ++j) {
			if(min_value > matrix[offset_i + j]) {
				min_value = matrix[offset_i + j];
				i_min[i]  = j;
			}
		}
	}
}

void col_arg_max(const SizeType n_rows, const SizeType n_cols, const Scalar *matrix, SizeType *i_max)
{
	for(SizeType i = 0; i < n_rows; ++i) {
		const SizeType offset_i = i * n_cols;

		Scalar max_value = matrix[offset_i];	
		i_max[i] = 0;
		
		for(SizeType j = 0; j < n_cols; ++j) {
			if(max_value < matrix[offset_i + j]) {
				max_value = matrix[offset_i + j];
				i_max[i]  = j;
			}
		}
	}
}


//////////////////
static void row_average(const SizeType n_rows, const SizeType n_cols, const Scalar *matrix, Scalar *result)
{
	for(SizeType j = 0; j < n_cols; ++j) {
		result[j] = matrix[j];
	}

	for(SizeType i = 1; i < n_rows; ++i) {
		const SizeType offset_i = i * n_cols;

		for(SizeType j = 0; j < n_cols; ++j) {
			result[j] += matrix[offset_i + j];
		}
	}

	for(SizeType j = 0; j < n_cols; ++j) {
		result[j] /= n_rows;
	}
}

static void row_subtract(const SizeType n_rows, const SizeType n_cols, const Scalar *row_vector, Scalar *matrix)
{
	for(SizeType i = 0; i < n_rows; ++i) {
		const SizeType offset_i = i * n_cols;

		for(SizeType j = 0; j < n_cols; ++j) {
			matrix[offset_i + j] -= row_vector[j];
		}
	}
}

static void row_arg_min(const SizeType n_rows, const SizeType n_cols, const Scalar *matrix, SizeType *i_min)
{
	for(SizeType j = 0; j < n_cols; ++j) {
		i_min[j] = 0;
	}

	for(SizeType i = 0; i < n_rows; ++i) {
		const SizeType offset_i = i * n_cols;

		for(SizeType j = 0; j < n_cols; ++j) {
			const Scalar min_value = matrix[ i_min[j] * n_cols + j];	
			const SizeType offset_j = offset_i + j;

			if(min_value > matrix[offset_j]) {
				i_min[j]  = i;
			}
		}
	}
}

static void row_arg_max(const SizeType n_rows, const SizeType n_cols, const Scalar *matrix, SizeType *i_max)
{
	for(SizeType j = 0; j < n_cols; ++j) {
		i_max[j] = 0;
	}

	for(SizeType i = 0; i < n_rows; ++i) {
		const SizeType offset_i = i * n_cols;

		for(SizeType j = 0; j < n_cols; ++j) {
			const Scalar max_value = matrix[ i_max[j] * n_cols + j];	
			const SizeType offset_j = offset_i + j;

			if(max_value < matrix[offset_j]) {
				i_max[j] = i;
			}
		}
	}
}


void matrix_copy_upper_to_lower(const SizeType n_rows_and_columns, Scalar * matrix)
{
	for(SizeType i = 0; i < n_rows_and_columns; ++i) {
		const SizeType offset_i = i*n_rows_and_columns;
		for(SizeType j = 0; j < i; ++j) {
			const SizeType offset_j = j*n_rows_and_columns;
			matrix[offset_j+i] = matrix[offset_i+j];
		}	
	}
}

void block_matrix_copy_upper_to_lower(const SizeType n_rows_and_columns, const SizeType block_rows_and_columns, Scalar * matrix)
{
	const SizeType block_stride = block_rows_and_columns * block_rows_and_columns;

	for(SizeType i = 0; i < n_rows_and_columns; ++i) {
		const SizeType offset_i = i * n_rows_and_columns * block_stride;

		for(SizeType j = 0; j < i; ++j) {
			const SizeType offset 			 = offset_i + j * block_stride;
			const SizeType offset_transposed = (j * n_rows_and_columns  + i) * block_stride;
			
			for(SizeType ik = 0; ik < block_rows_and_columns; ++ik) {
				const SizeType offset_block = offset + ik * block_rows_and_columns;

				for(SizeType jl = 0; jl < block_rows_and_columns; ++jl) {
					const SizeType offset_block_transposed = offset_transposed + jl * block_rows_and_columns;
					matrix[offset_block + jl] = matrix[offset_block_transposed + ik];

				}
			}
		}	
	}
}

void solve_2x2(const Scalar*mat, const Scalar *rhs, Scalar *result)
{
	Scalar inv_mat[2*2];
	inverse_2(mat, det_2(mat), inv_mat);
	mat_vec_mul(2, 2, inv_mat, rhs, result);
}

void solve_3x3(const Scalar*mat, const Scalar *rhs, Scalar *result)
{
	Scalar inv_mat[3 * 3];
	inverse_3(mat, det_3(mat), inv_mat);
	mat_vec_mul(3, 3, inv_mat, rhs, result);
}


void vec_repeat(const SizeType n_times_to_repeat, const SizeType n_elements_to_repeat, const Scalar *in, Scalar *out)
{
	for(SizeType i = 0; i < n_times_to_repeat; ++i) {
		const SizeType offset = i * n_elements_to_repeat;

		for(SizeType j = 0; j < n_elements_to_repeat; ++j) {
			out[offset + j] = in[j];
		}
	}
}

#endif //MATRIX_CL

