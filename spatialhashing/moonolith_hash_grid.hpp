#ifndef MOONOLITH_GRID_HPP
#define MOONOLITH_GRID_HPP

#include "moonolith_aabb.hpp"
#include "moonolith_cartesian_topology.hpp"

#include <array>
#include <cassert>
#include <cmath>
#include <vector>

namespace moonolith {

    template <Integer Dimension>
    class DistributionTensor {
    public:
        void init(Communicator &comm, const std::array<Integer, Dimension> &global_sizes) {
            std::array<int, Dimension> dims, dims_i;
            MPI_Dims_create(comm.size(), Dimension, &dims[0]);

            CartesianTopology<Dimension> ct;
            ct.create(comm, dims);

            std::copy(dims.begin(), dims.end(), dims_i.begin());
            resize(dims_i);

            std::array<Integer, Dimension> coordinates;
            for (std::size_t i = 0; i < tensor_.size(); ++i) {
                coord(i, coordinates);
                tensor_[i] = ct.rank(coordinates);
            }

            global_sizes = global_sizes_;
        }

        void init_global_coordinates() {
            // _pcoords.resize(coords.size(), comm.size());
            // _pcoords.allSet(0);

            // int ok = 1;
            // for(Integer d = 0; d < sizes.size(); ++d) {
            // 	const Integer size = _globalSizes[d]/_dims[d];
            // 	const Integer begin = size*coords[d];

            // 	_pcoords(d, r) = coords[d];

            // 	_sizes[d] = size + 1;
            // 	_begin(d, r) = begin;
            // 	_end(d, r) = size * (coords[d]+1) + 1;

            // 	if(coords[d] == _dims[d] - 1) {
            // 		const Integer end = _globalSizes[d];
            // 		_end(d, r) = end;
            // 		_sizes[d] = end-begin;
            // 	}

            // 	ok &= _sizes[d] > 1;
            // }

            // comm.allReduce(&ok, 1, MPI_MIN);
            // if(!ok) {
            // 	if(comm.isRoot())
            // 		std::cerr << "[Error] DGrid invalid grid decomposition.\n" << std::endl;
            // 	//let the message be readable
            // 	comm.barrier();
            // 	CUTK_ASSERT(false);
            // 	return false;
            // }

            // comm.allReduce(_begin.rawPtr(), _begin.size(), MPI_MAX);
            // comm.allReduce(_end.rawPtr(), _end.size(), MPI_MAX);
            // comm.allReduce(_pcoords.rawPtr(), _pcoords.size(), MPI_MAX);
        }

        inline void resize(const std::array<Integer, Dimension> &dims) {
            Integer size = 1;
            for (auto i : dims) {
                size *= i;
            }

            tensor_.resize(size);
        }

        inline Integer index(const std::array<Integer, Dimension> &coord) {
            Integer result = coord[0];
            for (Integer i = 1; i < Dimension; ++i) {
                result = coord[i] + dims_[i] * result;
            }

            return result;
        }

        inline void coord(const Integer &index, std::array<Integer, Dimension> &tensor_index) {
            Integer current = index;
            const Integer last = Dimension - 1;

            for (Integer i = last; i >= 0; --i) {
                const Integer next = current / dims_[i];
                tensor_index[i] = current - next * dims_[i];
                current = next;
            }

            assert(index == index(tensor_index));
        }

        inline void set(const std::array<Integer, Dimension> &coord, const Integer process) {
            tensor_[index(coord)] = process;
        }

        inline Integer get(const std::array<Integer, Dimension> &coord) { return tensor_[index(coord)]; }

        // inline void cell_to_coord(
        // 	const std::array<Integer, Dimension> &index,
        // 	std::array<Integer, Dimension> &coords)
        // {
        // 	// std::fill(coords.begin(), coords.end(), 0);

        // for(Integer d = 0; d < index.size(); ++d) {

        // 	if(dims_[d] == 1) {
        // 		continue;
        // 	}

        // 	const Integer ind = index[d];
        // 	const Integer offset = _begin.columns()*float(ind)/_globalSizes[d];
        // 	Integer c = _pcoords(d, offset);

        // 	//at most 1 loop (to be tested)
        // 	if(_begin(d, offset) > ind) {
        // 		for(Integer i = offset-1; i >= 0; --i) {
        // 			if(_begin(d, i) < ind) {
        // 				c = _pcoords(d, i);
        // 				break;
        // 			}
        // 		}
        // 	} else if(_end(d, offset)-1 <= ind) {
        // 		for(Integer i = offset; i < _end.columns(); ++i) {
        // 			if(_end(d, i)-1 > ind) {
        // 				c = _pcoords(d, i);
        // 				break;
        // 			}
        // 		}
        // 	} else {
        // 		c =_pcoords(d, offset);
        // 	}

        // 	if(_topology.rank() == 0) {
        // 		std::cout << "d        " << d << std::endl;
        // 		std::cout << "index(d) " << index[d] << std::endl;
        // 		std::cout << "coord(d) " << c << std::endl;
        // 	}

        // 	coords[d] = c;
        // 	CUTK_ASSERT(c >= 0);
        // 	CUTK_ASSERT(c < dims_[d]);

        // }
        // }

    public:
        std::array<Integer, Dimension> dims_;
        std::array<Integer, Dimension> global_sizes_;
        std::array<std::vector<Integer>, Dimension> offsets_;
        std::vector<Integer> tensor_;
    };

    template <Integer Dimension, typename T>
    class UniformHashGrid {
    public:
        typedef moonolith::AABB<Dimension, T> Box;

        inline Integer n_cells() const { return n_cells_; }

        inline Integer n_dims() const { return Dimension; }

        void describe(std::ostream &os) const {
            os << "box:\n";
            box_.describe(os);

            os << "range:\n";
            for (auto r : range_) os << r << " ";
            os << "\n";

            os << "dims: ";
            for (auto d : dims_) {
                os << d << " ";
            }

            os << "\n";
            os << "n_cells: " << n_cells_ << "\n";
        }

        Integer hash(const std::array<Real, Dimension> &point) const {
            Real x = (point[0] - box_.min(0)) / range_[0];
            Integer result = floor(x * dims_[0]);

            Integer total_dim = dims_[0];

            for (std::size_t i = 1; i < range_.size(); ++i) {
                result *= dims_[i];

                x = (point[i] - box_.min(i)) / range_[i];
                result += floor(x * dims_[i]);
                total_dim *= dims_[i];
            }

            if (result >= total_dim || result < 0) {
                printf("error -> %d\n", result);
            }

            return result;
        }

        Integer hash(const std::array<Integer, Dimension> &coord) const {
            Integer result = coord[0];
            Integer total_dim = dims_[0];

            for (std::size_t i = 1; i < range_.size(); ++i) {
                result *= dims_[i];
                result += coord[i];
                total_dim *= dims_[i];
            }

            assert(result >= 0 && "Integer overflow: change type of Integer to larger representation");
            return result;
        }

        void hash_range(const std::array<Real, Dimension> &min,
                        const std::array<Real, Dimension> &max,
                        std::vector<Integer> &hashes) {
            const Integer dim = min.size();
            std::array<Integer, Dimension> imin, imax;

            // generate tensor indices
            for (Integer i = 0; i < dim; ++i) {
                Real x = (min[i] - box_.min(i)) / range_[i];
                imin[i] = floor(x * dims_[i]);
            }

            for (Integer i = 0; i < dim; ++i) {
                Real x = (max[i] - box_.min(i)) / range_[i];
                imax[i] = floor(x * dims_[i]);
            }

            std::array<Integer, Dimension> offsets;
            for (Integer i = 0; i < dim; ++i) {
                offsets[i] = imax[i] - imin[i];
            }

            // FIXME make more general for greater dim
            if (dim == 1) {
                std::array<Integer, Dimension> coord;
                for (Integer i = imin[0]; i <= imax[0]; ++i) {
                    coord[0] = i;
                    hashes.push_back(hash(coord));
                }

            } else if (dim == 2) {
                std::array<Integer, Dimension> coord;
                for (Integer i = imin[0]; i <= imax[0]; ++i) {
                    // if(i < 0) continue;
                    if (i < 0 || i >= dims_[0]) continue;

                    for (Integer j = imin[1]; j <= imax[1]; ++j) {
                        // if(j < 0) continue;
                        if (j < 0 || j >= dims_[1]) continue;

                        coord[0] = i;
                        coord[1] = j;
                        hashes.push_back(hash(coord));
                    }
                }
            } else if (dim == 3) {
                std::array<Integer, Dimension> coord;
                for (Integer i = imin[0]; i <= imax[0]; ++i) {
                    for (Integer j = imin[1]; j <= imax[1]; ++j) {
                        for (Integer k = imin[2]; k <= imax[2]; ++k) {
                            coord[0] = i;
                            coord[1] = j;
                            coord[2] = k;
                            hashes.push_back(hash(coord));
                        }
                    }
                }
            } else {
                assert(false && "dim > 3 not supported yet!");
            }

            // assert(!hashes.empty());
        }

        UniformHashGrid(const Box &box, const std::array<Integer, Dimension> &dims)
            : box_(box), dims_(dims), n_cells_(1) {
            box_.enlarge(1e-8);
            range_ = box_.get_max();

            for (std::size_t i = 0; i < dims_.size(); ++i) {
                n_cells_ *= dims_[i];
                range_[i] -= box_.min(i);
            }
        }

        void reset(const Box &box, const std::array<Integer, Dimension> &dims) {
            box_ = box;
            dims_ = dims;
            n_cells_ = 1;

            box_.enlarge(1e-8);
            range_ = box_.get_max();

            for (std::size_t i = 0; i < dims_.size(); ++i) {
                n_cells_ *= dims_[i];
                range_[i] -= box_.min(i);
            }
        }

        UniformHashGrid() : n_cells_(0) {}

        void hash_to_tensor_index(const Integer &hash, std::array<Integer, Dimension> &index) const {
            index.resize(dims_.size());
            std::fill(index.begin(), index.end(), 0);

            const Integer dim = range_.size();

            Integer current = hash;
            for (Integer i = dim - 1; i >= 0; --i) {
                const Integer next = current / dims_[i];
                index[i] = current - next * dims_[i];
                current = next;
            }
        }

        void coord(const std::array<Integer, Dimension> &tensor_index, std::array<Real, Dimension> &coord) const {
            coord.resize(tensor_index.size());

            for (std::size_t i = 0; i < tensor_index.size(); ++i) {
                coord[i] = box_.min(i) + (tensor_index[i] * range_[i] / dims_[i]);
            }
        }

        void coord(const Integer &hash, std::array<Real, Dimension> &coord) const {
            std::array<Integer, Dimension> tensor_index;
            hash_to_tensor_index(hash, tensor_index);
            this->coord(tensor_index, coord);
        }

        bool find_intersecting_cells(const std::array<Box, Dimension> &boxes,
                                     std::vector<std::vector<Integer> > &box_to_cell_hashes) {
            box_to_cell_hashes.clear();
            box_to_cell_hashes.resize(boxes.size());

            bool found_intersections = false;
            std::array<Integer, Dimension> hashes;

            for (std::size_t i = 0; i < boxes.size(); ++i) {
                const Box &b = boxes[i];
                hashes.clear();
                hash_range(b.min(), b.get_max(), box_to_cell_hashes[i]);

                found_intersections |= !box_to_cell_hashes[i].empty();
            }

            return found_intersections;
        }

        inline Integer proc(const Integer &hash) {
            std::array<Integer, Dimension> index;
            return hash_to_tensor_index(hash, index);
        }

        inline DistributionTensor<Dimension> &distro() { return distro_; }

        inline void init_distro(Communicator &comm) { distro().init(comm); }

    private:
        Box box_;
        std::array<Real, Dimension> range_;
        std::array<Integer, Dimension> dims_;
        Integer n_cells_;

        DistributionTensor<Dimension> distro_;
    };

}  // namespace moonolith

#endif  // MOONOLITH_GRID_HPP
