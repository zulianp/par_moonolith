#include "moonolith_communicator.hpp"
#include "moonolith_profiler.hpp"
#include "moonolith_stream_utils.hpp"

#include "par_moonolith_instance.hpp"

#include "gtest/gtest.h"

int main(int argc, char **argv) {
    using namespace moonolith;

    Moonolith::Init(argc, argv);
    MOONOLITH_PROFILING_BEGIN();

    ::testing::InitGoogleTest(&argc, argv);
    int ok = RUN_ALL_TESTS();

    MOONOLITH_PROFILING_END();
    return Moonolith::Finalize() && ok;
}
