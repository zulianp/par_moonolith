#ifndef MOONOLITH_INTERSECT_POLYGONS_IMPL_HPP
#define MOONOLITH_INTERSECT_POLYGONS_IMPL_HPP

#include "moonolith_geo_algebra.hpp"
#include "moonolith_householder.hpp"
#include "moonolith_intersect_polygons.hpp"
#include "moonolith_intersect_polyline_with_convex_polygon.hpp"
#include "moonolith_make_unique.hpp"

#include "clipper/clipper.hpp"

#include <algorithm>
#include <iostream>

namespace moonolith {

    template <typename T>
    static void sort_scalar_and_index_array(const Integer n_elements, T *values, Integer *indices) {
        for (Integer i = 1; i < n_elements; ++i) {
            const T value = values[i];
            const Integer index = indices[i];

            Integer j = i - 1;

            while (j >= 0 && values[j] > value) {
                const Integer jp1 = j + 1;
                values[jp1] = values[j];
                indices[jp1] = indices[j];
                --j;
            }

            const Integer jp1 = j + 1;
            values[jp1] = value;
            indices[jp1] = index;
        }
    }

    template <typename T, int Dim>
    void IntersectConvexPolygons<T, Dim>::remove_duplicate_points(Polygon &polygon, const T tol) {
        remove_duplicate_points(polygon.points, tol);
    }

    template <typename T, int Dim>
    void RemoveDuplicatePoints<T, Dim>::apply(Storage<Point> &polygon, const T tol) {
        if (polygon.empty()) return;

        Integer current_n_points = polygon.size();
        keep.resize(current_n_points);
        keep[0] = true;

        Integer result_n_points = current_n_points;
        for (Integer i = 1; i < current_n_points; ++i) {
            const Integer im1 = i - 1;

            if (distance(polygon[i], polygon[im1]) <= tol) {
                result_n_points--;
                keep[i] = false;
            } else {
                keep[i] = true;
            }
        }

        const Integer last = (current_n_points - 1);
        ;

        if (keep[last]) {
            if (distance(polygon[0], polygon[last]) <= tol) {
                result_n_points--;

                keep[last] = false;

            } else {
                keep[last] = true;
            }
        }

        Integer range_to_fill_begin = -1;
        bool must_shift = false;

        for (Integer i = 1; i < current_n_points; ++i) {
            if (keep[i] && must_shift) {
                assert(range_to_fill_begin > 0);

                const Integer to = range_to_fill_begin;
                const Integer from = i;

                polygon[to] = polygon[from];

                keep[i] = false;

                if (keep[++range_to_fill_begin]) {
                    range_to_fill_begin = -1;
                    must_shift = false;
                }
            }

            if (!keep[i] && !must_shift) {
                must_shift = true;
                range_to_fill_begin = i;
            }
        }

        polygon.resize(result_n_points);
    }

    template <typename T, class IndexT>
    bool SortPolygon<T, IndexT>::apply(const Storage<Vector<T, 3>> &points,
                                       const Vector<T, 3> &normal,
                                       IndexT *index,
                                       const std::size_t n_points) {
        // static const int SubDim = 2;

        // using Vector3 = moonolith::Vector<T, 3>;
        using Vector2 = moonolith::Vector<T, 2>;

        local_point.resize(n_points);
        local_value.resize(n_points);

        if (n_points == 3) {
            // trivial case
            const auto &p0 = points[index[0]];
            const auto &p1 = points[index[1]];
            const auto &p2 = points[index[2]];

            if (dot(cross(p1 - p0, p2 - p0), normal) <= 0) {
                std::swap(index[1], index[2]);
            }

            return true;
        }

        // default is xy-plane
        Integer coord_1 = 0;
        Integer coord_2 = 1;

        bool transformed = false;

        if (fabs(normal[1]) > 0.9999) {
            // xz-plane
            coord_2 = 2;
        } else if (fabs(normal[0]) > 0.9999) {
            // yz-plane
            coord_1 = 1;
            coord_2 = 2;
        } else if (!(fabs(normal[2]) > 0.9999)) {
            // also not xy-plane
            transformed = true;
        }

        if (transformed) {
            // The bug is inside this branch
            // printf("householder_reflection_3: ROTATING POLY\n");
            // Rotation to the xy-plane is necessary

            v[0] = normal[0];
            v[1] = normal[1];
            v[2] = normal[2] + 1.0;

            const auto norm_v = length(v);
            v /= norm_v;

            householder_reflection(v, H);

            for (std::size_t i = 0; i < n_points; ++i) {
                const auto idx = index[i];
                mat_vec_mul(H, points[idx], local_point[i]);
            }

        } else {
            for (std::size_t i = 0; i < n_points; ++i) {
                const auto idx = index[i];
                local_point[i].x = points[idx][coord_1];
                local_point[i].y = points[idx][coord_2];
            }
        }

        avg[0] = 0.;
        avg[1] = 0.;

        std::for_each(std::begin(local_point), std::end(local_point), [this](const Vector2 &v) { avg += v; });

        avg /= static_cast<T>(n_points);

        std::for_each(std::begin(local_point), std::end(local_point), [this](Vector2 &v) { v -= avg; });

        // Project local points onto reference circle norm_2(p) = 1
        for (std::size_t i = 0; i < n_points; ++i) {
            auto norm_p = length(local_point[i]);
            local_point[i] /= norm_p;
        }

        graham_scan.apply(local_point, index);

        // arg_min(local_point, i_min);
        // arg_max(local_point, i_max);

        // if(i_min[0] == i_max[0] || i_min[1] == i_max[1]) {
        //     std::cerr << i_min[0] << ", " << i_max[0] << ", " << i_min[1] << ", " << i_max[1] << std::endl;;
        //     std::cerr << ("fix_polygon_ordering: degenerate input\n");
        //     assert(false);
        //     return false;
        // }

        // const auto &left_bound  = local_point[i_min[0]];
        // const auto &right_bound = local_point[i_max[0]];

        // proj_vec = (right_bound - left_bound);
        // proj_vec /= length(proj_vec);

        // perp_proj_vec.x = -proj_vec.y;
        // perp_proj_vec.y = proj_vec.x;

        // avg[0] = left_bound.x;
        // avg[1] = left_bound.y;

        // std::for_each(std::begin(local_point), std::end(local_point), [this](Vector2 &v) {
        //     v -= avg;
        // });

        // //[-1, 0.0 + x ...., 3.0, 4.0 + x .......]
        // for(Integer i = 0; i < n_points; ++i) {
        //     if(i == i_min[0]) {
        //         local_value[i] = -1.0; //put it far left
        //         continue;
        //     }

        //     if(i == i_max[0]) {
        //         local_value[i] = 3.0; //put it in the middle
        //         continue;
        //     }

        //     const auto &dir = local_point[i];
        //     const auto proj_location = dot(proj_vec, dir);
        //     const auto dist = dot(dir, perp_proj_vec);

        //     if(dist > 0) {
        //         //It is in the upper hemisphere
        //         local_value[i] = 50.0 - proj_location; //put it far right but the other way around
        //     } else if(dist < 0) {
        //         //It is in the lower hemisphere
        //         local_value[i] = proj_location;

        //     } else {
        //         std::cerr << ("fix_polygon_ordering: special case of collinear object not handled") << std::endl;
        //     }
        // }

        // sort_scalar_and_index_array(n_points, &local_value[0], index);

        // make sure that the face is oriented according to the normal
        // u = points[index[1]] - points[index[0]];
        // v = points[index[2]] - points[index[0]];
        // w = cross(u, v);

        // if(dot(w, normal) < 0.) {
        //     std::reverse(index, index + n_points);
        // }

        return true;
    }

    template <typename T, class IndexT>
    bool sorted(const Storage<Vector<T, 3>> &points,
                const Vector<T, 3> &normal,
                IndexT *index,
                const std::size_t n_points) {
        return SortPolygon<T, IndexT>().apply(points, normal, index, n_points);
    }

    template <typename T, int Dim>
    bool IntersectConvexPolygons<T, Dim>::apply(const Polygon &poly1,
                                                const Polygon &poly2,
                                                Polygon &result,
                                                const T &tol) {
        int n_vertices_1 = poly1.size();
        int n_input_points = poly2.size();

        result.clear();

        input_buffer = poly2;
        Polygon *input = &input_buffer;
        Polygon *output = &result;

        for (int i = 0; i < n_vertices_1; ++i) {
            const int ip1 = ((i + 1) == n_vertices_1) ? 0 : (i + 1);

            if (i > 0) {
                std::swap(input, output);

                n_input_points = input->size();
                output->clear();
            }

            const auto &e1 = poly1[i];
            const auto &e2 = poly1[ip1];

            is_inside.resize(n_input_points);

            int n_outside = 0;
            for (int j = 0; j < n_input_points; ++j) {
                const auto &p = (*input)[j];

                is_inside[j] = inside_half_plane(e1, e2, p, tol);
                n_outside += is_inside[j] != INSIDE;
            }

            if (n_input_points - n_outside == 0) return false;

            for (int j = 0; j < n_input_points; ++j) {
                const int jp1 = (j + 1 == n_input_points) ? 0 : (j + 1);

                const auto &s = (*input)[j];
                const auto &e = (*input)[jp1];

                if (is_inside[j]) {
                    output->points.push_back(s);

                    if ((is_inside[j] != ON_EDGE) && (!is_inside[jp1])) {
                        output->points.push_back(intersect_lines(e1, e2, s, e));
                    }

                } else if (is_inside[jp1]) {
                    output->points.push_back(intersect_lines(e1, e2, s, e));
                }
            }

            if (static_cast<int>(output->size()) < 3) {
                return false;
            }

            remove_duplicate_points(*output, tol);

            if (static_cast<int>(output->size()) < 3) {
                return false;
            }
        }

        if (output != &result) {
            result = *output;
        }

        return static_cast<int>(result.size()) >= 3;
    }

    template <typename T, int Dim>
    class IntersectPolygons<T, Dim>::Impl {
    public:
        using Paths = ClipperLib::Paths;
        using Clipper = ClipperLib::Clipper;

        using Polygon = moonolith::Polygon<T, Dim>;
        using Point = typename Polygon::Point;

        Paths subj, clip, solution;
        // Clipper c;

        RemoveDuplicatePoints<T, Dim> remove_duplicate_points;

        inline bool apply(const Polygon &poly1, const Polygon &poly2, Storage<Polygon> &result, const T &tol) {
            using namespace ClipperLib;

            int n_vertices_1 = static_cast<int>(poly1.size());
            int n_vertices_2 = static_cast<int>(poly2.size());

            T min_x = poly1[0].x;
            T min_y = poly1[0].y;
            T max_x = poly1[0].x;
            T max_y = poly1[0].y;

            for (int i = 1; i < n_vertices_1; ++i) {
                min_x = std::min(min_x, poly1[i].x);
                max_x = std::max(max_x, poly1[i].x);

                min_y = std::min(min_y, poly1[i].y);
                max_y = std::max(max_y, poly1[i].y);
            }

            for (int i = 1; i < n_vertices_2; ++i) {
                min_x = std::min(min_x, poly2.points[i].x);
                max_x = std::max(max_x, poly2.points[i].x);

                min_y = std::min(min_y, poly2.points[i].y);
                max_y = std::max(max_y, poly2.points[i].y);
            }

            const T denom = std::max(max_x - min_x, max_y - min_y);
            const T cut_off = 1e10 / denom;  // 1e18 is the max represented value

            // Paths subj, clip, solution;
            subj.resize(1), clip.resize(1);
            subj[0].clear();
            clip[0].clear();

            subj[0].reserve(n_vertices_1);
            clip[0].reserve(n_vertices_2);

            for (int i = 0; i < n_vertices_1; ++i) {
                ClipperLib::cInt x_int = static_cast<ClipperLib::cInt>((poly1[i].x - min_x) * cut_off);
                ClipperLib::cInt y_int = static_cast<ClipperLib::cInt>((poly1[i].y - min_y) * cut_off);
                subj[0].push_back(IntPoint(x_int, y_int));
            }

            for (int i = 0; i < n_vertices_2; ++i) {
                ClipperLib::cInt x_int = static_cast<ClipperLib::cInt>((poly2[i].x - min_x) * cut_off);
                ClipperLib::cInt y_int = static_cast<ClipperLib::cInt>((poly2[i].y - min_y) * cut_off);
                clip[0].push_back(IntPoint(x_int, y_int));
            }

            // perform intersection ...
            Clipper c;

            c.AddPaths(subj, ptSubject, true);
            c.AddPaths(clip, ptClip, true);
            c.Execute(ctIntersection, solution, pftNonZero, pftNonZero);

            if (solution.empty() || solution[0].size() < 3) return false;

            // assert(solution.size() == 1);

            Integer n_islands = static_cast<Integer>(solution.size());
            result.resize(n_islands);

            Integer n_actual_intersection = 0;

            for (Integer k = 0; k < n_islands; ++k) {
                Integer n_nodes = static_cast<Integer>(solution[k].size());

                if (n_nodes < 3) {
                    continue;
                }

                result[n_actual_intersection].points.resize(n_nodes);

                for (Integer i = 0; i < n_nodes; ++i) {
                    result[n_actual_intersection][i].x = solution[k][i].X / cut_off + min_x;
                    result[n_actual_intersection][i].y = solution[k][i].Y / cut_off + min_y;
                }

                // it bugged here
                result[n_actual_intersection].remove_collinear_points(10.0 * tol);

                if (result[n_actual_intersection].size() >= 3) {
                    remove_duplicate_points.apply(result[n_actual_intersection].points, 10. * tol);
                }

                if (result[n_actual_intersection].size() >= 3) {
                    ++n_actual_intersection;
                }
            }

            // clean-up
            subj.clear();
            clip.clear();
            solution.clear();
            // c.Clear();

            if (n_actual_intersection == 0) {
                result.clear();
                return false;
            }

            result.resize(n_actual_intersection);
            return true;
        }
    };

    template <typename T, int Dim>
    IntersectPolygons<T, Dim>::IntersectPolygons() : impl_(moonolith::make_unique<Impl>()) {}

    template <typename T, int Dim>
    IntersectPolygons<T, Dim>::~IntersectPolygons() {}

    template <typename T, int Dim>
    bool IntersectPolygons<T, Dim>::apply(const Polygon &poly1,
                                          const Polygon &poly2,
                                          Storage<Polygon> &result,
                                          const T &tol) {
        return impl_->apply(poly1, poly2, result, tol);
    }

    template <typename T>
    bool IntersectConvexPolygons<T, 3>::apply(const Polygon &poly1,
                                              const Polygon &poly2,
                                              Polygon &result,
                                              const T &tol) {
        const auto &p0 = poly1[0];
        const auto &p1 = poly1[1];
        const auto &p2 = poly1[2];

        u = p1 - p0;
        v = p2 - p0;
        n = cross(u, v);
        n /= length(n);
        n += p0;

        if (!make(p0, p1, p2, n, trafo)) {
            assert(false);
            std::cerr << "[Error] bad element" << std::endl;
            return false;
        }

        const Integer n_points_1 = poly1.size();
        const Integer n_points_2 = poly2.size();

        ref_poly_1.resize(n_points_1);
        ref_poly_2.resize(n_points_2);

        for (Integer i = 0; i < n_points_1; ++i) {
            trafo.apply_inverse(poly1[i], v);
            ref_poly_1[i].x = v.x;
            ref_poly_1[i].y = v.y;
        }

        for (Integer i = 0; i < n_points_2; ++i) {
            trafo.apply_inverse(poly2[i], v);
            ref_poly_2[i].x = v.x;
            ref_poly_2[i].y = v.y;
        }

        if (!isect.apply(ref_poly_1, ref_poly_2, ref_isect, tol)) {
            return false;
        }

        const Integer n_points_isect = ref_isect.size();
        result.resize(n_points_isect);

        for (Integer i = 0; i < n_points_isect; ++i) {
            v.x = ref_isect[i].x;
            v.y = ref_isect[i].y;
            v.z = 0.;
            trafo.apply(v, result[i]);
        }

        return true;
    }

    template <typename T>
    bool IntersectConvexPolygons<T, 3>::apply(const Polygon &poly1,
                                              const Polygon &poly2,
                                              Line<T, 3> &result,
                                              const T &tol) {
        const auto &p0 = poly1[0];
        const auto &p1 = poly1[1];
        const auto &p2 = poly1[2];

        u = p1 - p0;
        v = p2 - p0;
        n = cross(u, v);
        n /= length(n);

        const auto &q0 = poly2[0];
        const auto &q1 = poly2[1];
        const auto &q2 = poly2[2];

        Vector qu = q1 - q0;
        Vector qv = q2 - q0;
        Vector qn = cross(qu, qv);
        qn /= length(qn);

        T cos_angle = dot(n, qn);

        if (moonolith::approxeq(std::abs(cos_angle), static_cast<T>(1.0), static_cast<T>(1e-14))) {
            // coplanar skip
            return false;
        }

        n += p0;

        if (!make(p0, p1, p2, n, trafo)) {
            assert(false);
            std::cerr << "[Error] bad element" << std::endl;
            return false;
        }

        // Vector qn_ref;
        // trafo.apply_inverse_to_direction(qn, qn_ref);

        Polygon trafo_poly_2;

        // trafo.apply_inverse(poly1.points, trafo_poly_1.points);
        trafo.apply_inverse(poly2.points, trafo_poly_2.points);

        Plane<T, 3> plane;
        plane.p.zero();
        plane.n = {0.0, 0.0, 1.0};

        Ray<T, 3> ray;

        const Integer n2 = poly2.size();

        Storage<Point> isects;
        for (Integer i = 0; i < n2; ++i) {
            const auto ip1 = (i + 1 == n2) ? 0 : i + 1;

            ray.o = trafo_poly_2[i];
            ray.dir = trafo_poly_2[ip1] - ray.o;

            auto len_dir = length(ray.dir);
            ray.dir /= len_dir;

            T t = 0.0;
            if (!plane.intersect(ray, t)) {
                continue;
            }

            if (t > len_dir || t < 0) {
                continue;
            }

            qu = ray.o + t * ray.dir;

            if (!approxeq(qu.z, static_cast<T>(0.0), tol)) {
                assert(false);
            }

            isects.push_back(qu);

            if (approxeq(t, len_dir, 100 * tol)) {
                // skip next ray
                i += 1;
            }
        }

        rdp.apply(isects, 100 * tol);

        const auto n_isect = isects.size();

        if (n_isect < 2) {
            return false;
        }

        if (n_isect > 2) {
            assert(false && "handle me");
        }

        Line<T, 2> line2, result2;
        line2.p0.x = isects[0].x;
        line2.p0.y = isects[0].y;

        line2.p1.x = isects[1].x;
        line2.p1.y = isects[1].y;

        // if intersection with x-y plane
        const Integer n_points_1 = poly1.size();

        ref_poly_1.resize(n_points_1);

        for (Integer i = 0; i < n_points_1; ++i) {
            trafo.apply_inverse(poly1[i], v);
            ref_poly_1[i].x = v.x;
            ref_poly_1[i].y = v.y;
        }

        IntersectPolylineWithConvexPolygon2<T> isect_line_with_polygon;
        if (!isect_line_with_polygon.apply(line2, ref_poly_1, result2, tol)) {
            return false;
        }

        qu.x = result2.p0.x;
        qu.y = result2.p0.y;
        qu.z = 0.0;

        qv.x = result2.p1.x;
        qv.y = result2.p1.y;
        qv.z = 0.0;

        trafo.apply(qu, result.p0);
        trafo.apply(qv, result.p1);
        return true;
    }

}  // namespace moonolith

#endif  // MOONOLITH_INTERSECT_POLYGONS_IMPL_HPP]
