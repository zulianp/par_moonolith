#include "moonolith_elem.hpp"
#include "moonolith_lower_dim_l2_assembler_impl.hpp"

#include "moonolith_elem_edge2.hpp"
#include "moonolith_elem_tri3.hpp"

namespace moonolith {

    template class LowerDimL2Transfer<Edge2<Real, 2>, Edge2<Real, 2>>;
    template class LowerDimL2Transfer<Edge3<Real, 2>, Edge3<Real, 2>>;

    template class LowerDimL2Transfer<Tri3<Real, 3>, Tri3<Real, 3>>;
    template class LowerDimL2Transfer<Tri6<Real, 3>, Tri6<Real, 3>>;

    template class LowerDimL2Transfer<Quad4<Real, 3>, Quad4<Real, 3>>;
    template class LowerDimL2Transfer<Quad8<Real, 3>, Quad8<Real, 3>>;
    template class LowerDimL2Transfer<Quad9<Real, 3>, Quad9<Real, 3>>;

    template class LowerDimL2Transfer<Elem<Real, 1, 2>, Elem<Real, 1, 2>>;
    template class LowerDimL2Transfer<Elem<Real, 2, 3>, Elem<Real, 2, 3>>;
    // template class LowerDimL2Transfer<Elem<Real, 3, 2>, Elem<Real, 3, 2>>;
}  // namespace moonolith