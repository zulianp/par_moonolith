#ifndef MOONOLITH_INTERSECT_POLYGON_WITH_MESH_HPP
#define MOONOLITH_INTERSECT_POLYGON_WITH_MESH_HPP

#include "moonolith_h_polytope.hpp"
#include "moonolith_mesh.hpp"
#include "moonolith_polygon.hpp"
#include "moonolith_polyhedron.hpp"


#include "moonolith_intersect_polygon_with_h_polyhedron_impl.hpp"
#include "moonolith_intersect_polyline_with_convex_polygon_impl.hpp"

#include <utility>

namespace moonolith {

    template <typename T, Integer PhysicalDim>
    class IntersectPolyWithMesh {};

    template <typename T>
    class IntersectPolyWithMesh<T, 3> {
    public:
        using MeshT = moonolith::Mesh<T, 3>;
        using PolygonT = moonolith::Polygon<T, 3>;
        using PolyhedronT = moonolith::Polyhedron<T>;
        using HPolyhedronT = moonolith::HPolytope<T, 3>;

        bool apply(const PolygonT &polygon, const MeshT &mesh, Storage<std::pair<Integer, PolygonT>> &intersection) {
            //
            auto ne = mesh.n_elements();
            intersection.clear();
            intersection.reserve(ne);

            PolyhedronT polyhedron;
            HPolyhedronT h_polyhedron;
            PolygonT intersection_result;

            IntersectPolygonWithHPolyhedron<T> isect;

            std::shared_ptr<Elem<T, 3, 3>> elem;

            Integer k = 0;
            for(auto &e : mesh.elements()) {
                make(mesh, e, elem);

                assert(elem);
                make(*elem, polyhedron);
                h_polyhedron.make(polyhedron);

                if(isect.apply(polygon, h_polyhedron, intersection_result)) {
                    intersection.push_back(std::make_pair(k, intersection_result));
                }

                ++k;
            }

            return !intersection.empty();

        }
    };

    template <typename T>
    class IntersectPolyWithMesh<T, 2> {
    public:
        using MeshT = moonolith::Mesh<T, 2>;
        using PolygonT = moonolith::Polygon<T, 2>;
        using LineT = moonolith::Line<T, 2>;
        using HPolygonT = moonolith::HPolytope<T, 2>;

        bool apply(const LineT &line, const MeshT &mesh, Storage<std::pair<Integer, LineT>> &intersection) {
            //
            auto ne = mesh.n_elements();
            intersection.clear();
            intersection.reserve(ne);

            PolygonT polygon;
            LineT intersection_result;

            IntersectPolylineWithConvexPolygon2<T> isect;
            std::shared_ptr<Elem<T, 2, 2>> elem;

            Integer k = 0;
            for(auto &e : mesh.elements()) {
                make(mesh, e, elem);

                assert(elem);
                make(*elem, polygon);

                if(isect.apply(line, polygon, intersection_result)) {
                    intersection.push_back(std::make_pair(k, intersection_result));
                }

                ++k;
            }

            return !intersection.empty();

        }
    };

}  // namespace moonolith

#endif  // MOONOLITH_INTERSECT_POLYGON_WITH_MESH_HPP