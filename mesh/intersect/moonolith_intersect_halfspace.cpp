#include "moonolith_intersect_halfspace_impl.hpp"

namespace moonolith {

    template class IntersectLineWithHalfSpace<Real, 2>;
    template class IntersectLineWithHalfSpace<Real, 3>;
}  // namespace moonolith