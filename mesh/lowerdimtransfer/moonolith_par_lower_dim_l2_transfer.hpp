#ifndef MOONOLITH_PAR_LOWER_DIM_L2_TRANSFER_HPP
#define MOONOLITH_PAR_LOWER_DIM_L2_TRANSFER_HPP

#include "moonolith_duplicate_intersection_avoidance.hpp"
#include "moonolith_many_masters_one_slave_algorithm.hpp"
#include "moonolith_one_master_one_slave_algorithm.hpp"
#include "moonolith_single_collection_one_master_one_slave_algorithm.hpp"
#include "moonolith_sparse_matrix.hpp"
#include "par_moonolith_config.hpp"

#include "moonolith_collection_manager.hpp"
#include "moonolith_function_space.hpp"
#include "moonolith_gauss_quadrature_rule.hpp"
#include "moonolith_incomplete_intersection_remover.hpp"
#include "moonolith_keast_quadrature_rule.hpp"
#include "moonolith_l2_assembler.hpp"
#include "moonolith_lower_dim_l2_assembler.hpp"
#include "moonolith_matrix_inserter.hpp"
#include "moonolith_par_l2_transfer.hpp"

#include <tuple>
#include <vector>

#include <mpi.h>

namespace moonolith {

    // template<typename T>
    // class MatrixBuffers {
    // public:
    //     MatrixBuffers(Communicator &comm)
    //     : B(comm), D(comm)/*, Q(comm, false), incomplete_remover(comm) */
    //     {}

    //     // inline MatrixInserter<T> &measure()
    //     // {
    //     //     return incomplete_remover.measure;
    //     // }

    //     MatrixInserter<T> B, D;// Q;

    //     //used to remove incomplete intersections
    //     // IncompleteIntersectionRemover<T> incomplete_remover;
    // };

    template <class MasterElem, class SlaveElem>
    class LocalLowerDimL2Assembler {
    public:
        using T = typename SlaveElem::T;
        static const int Dim = SlaveElem::Dim;

        using QuadratureT = moonolith::Quadrature<T, Dim - 1>;
        using L2TransferT = moonolith::LowerDimL2Transfer<MasterElem, SlaveElem>;

        template <class Elem>
        static int order_of_quadrature(const Elem &e) {
            if (e.is_simplex()) {
                return e.order();
            } else {
                return e.order() * Dim;
            }
        }

        void init_quadrature() {
            int master_order = order_of_quadrature(*master_elem);
            int slave_order = order_of_quadrature(*slave_elem);

            int order = 0;
            if (slave_elem->is_affine()) {
                order = master_order + slave_order;
            } else {
                // assumes tensor product
                order = master_order + slave_order + (slave_order - 1) * 2;
            }

            if (order == current_quadrature_order_) return;

            Gauss::get(order, q_rule);
            algo.set_quadrature(q_rule);
        }

        template <class ElemAdapter>
        inline bool operator()(const ElemAdapter &master, const ElemAdapter &slave) {
            auto &e_m = master.elem();
            auto &e_s = slave.elem();

            auto &m_m = master.collection();
            auto &m_s = slave.collection();

            auto &dof_obj_s = m_s.dof_map().dof_object(slave.element_handle());

            auto &dof_m = m_m.dof_map().dofs(master.element_handle());
            auto &dof_s = dof_obj_s.dofs;

            make(m_m, e_m, master_elem);
            make(m_s, e_s, slave_elem);

            init_quadrature();

            if (algo.assemble(*master_elem, *slave_elem)) {
                const auto &B_e = algo.coupling_matrix();
                const auto &D_e = algo.mass_matrix();
                // const auto &Q_e = algo.transformation();

                buffers.B.insert(dof_s, dof_m, B_e);
                buffers.D.insert(dof_s, dof_s, D_e);
                // buffers.Q.insert(dof_s, dof_s, Q_e);
                // buffers.measure().insert(dof_obj_s.element_dof, D_e.sum());
                return true;
            } else {
                return false;
            }
        }

        void clear() { algo.clear(); }

        LocalLowerDimL2Assembler(MatrixBuffers<T> &buffers) : buffers(buffers), current_quadrature_order_(-1) {}

        L2TransferT algo;

    private:
        QuadratureT q_rule;
        std::shared_ptr<MasterElem> master_elem;
        std::shared_ptr<SlaveElem> slave_elem;
        MatrixBuffers<T> &buffers;
        int current_quadrature_order_;
    };

    template <typename T, Integer PhysicalDim, Integer MasterDim = PhysicalDim, Integer SlaveDim = PhysicalDim>
    class ParLowerDimL2Transfer {
    public:
        using MeshT = moonolith::Mesh<T, PhysicalDim>;
        using SpaceT = moonolith::FunctionSpace<MeshT>;

        using SpaceAlgorithmT = moonolith::OneMasterOneSlaveAlgorithm<PhysicalDim, SpaceT>;
        using SingleSpaceAlgorithmT = moonolith::SingleCollectionOneMasterOneSlaveAlgorithm<PhysicalDim, SpaceT>;

        using ElemAdapter = typename SpaceAlgorithmT::Adapter;
        using SingleElemAdapter = typename SingleSpaceAlgorithmT::Adapter;

        using MasterElem = moonolith::Elem<T, MasterDim, PhysicalDim>;
        using SlaveElem = moonolith::Elem<T, SlaveDim, PhysicalDim>;

        ParLowerDimL2Transfer(Communicator &comm) : comm(comm), buffers(comm), local_assembler(buffers) {}

        void post_process(const SpaceT &master, const SpaceT &slave) {
            Integer n_master_dofs = static_cast<Integer>(master.dof_map().n_local_dofs());
            Integer n_slave_dofs = static_cast<Integer>(slave.dof_map().n_local_dofs());

            buffers.B.finalize(n_slave_dofs, n_master_dofs);
            buffers.D.finalize(n_slave_dofs, n_slave_dofs);
            // buffers.Q.finalize(n_slave_dofs, n_slave_dofs);

            // if(remove_incomplete_intersections_) {
            //     auto &remover = buffers.incomplete_remover;
            //     remover.determine_complete(slave);

            //     remover.remove_incomplete(buffers.B.get());
            //     remover.remove_incomplete(buffers.D.get());
            //     remover.remove_incomplete(buffers.Q.get());
            // }
        }

        bool assemble(const SpaceT &master, const SpaceT &slave, const T box_tol = 0.0) {
            Real elapsed = MPI_Wtime();

            SpaceAlgorithmT algo(comm, moonolith::make_unique<CollectionManager<SpaceT>>(comm, true));

            algo.init_simple(master, slave, box_tol);

            elapsed = MPI_Wtime() - elapsed;
            logger() << "init: " << elapsed << std::endl;

            ////////////////////////////////////////////////////
            /////////////////// pair-wise method ///////////////////////
            elapsed = MPI_Wtime();

            local_assembler.clear();

            algo.compute([&](const ElemAdapter &master, const ElemAdapter &slave) -> bool {
                if (local_assembler(master, slave)) {
                    return true;
                }

                return false;
            });

            Real vol = local_assembler.algo.intersection_measure();

            comm.all_reduce(&vol, 1, MPISum());

            Real sum_mat_B = buffers.B.m_matrix.sum();
            Real sum_mat_D = buffers.D.m_matrix.sum();

            post_process(master, slave);

            elapsed = MPI_Wtime() - elapsed;
            if (Moonolith::instance().verbose()) {
                logger() << "time ParLowerDimL2Transfer::assemble: " << elapsed << std::endl;
                logger() << "vol: " << vol << " sum(B): " << sum_mat_B << " sum(D): " << sum_mat_D << std::endl;
            }
            return vol > 0.0;
        }

        bool assemble(const SpaceT &master,
                      const SpaceT &slave,
                      const std::vector<std::pair<int, int>> &tags,
                      const T box_tol = 0.0) {
            Real elapsed = MPI_Wtime();

            SpaceAlgorithmT algo(comm, moonolith::make_unique<CollectionManager<SpaceT>>(comm, false));

            algo.init(master, slave, tags, box_tol);

            elapsed = MPI_Wtime() - elapsed;

            if (Moonolith::instance().verbose()) {
                logger() << "init: " << elapsed << std::endl;
            }

            ////////////////////////////////////////////////////
            /////////////////// pair-wise method ///////////////////////
            elapsed = MPI_Wtime();

            local_assembler.clear();

            algo.compute([&](const ElemAdapter &master, const ElemAdapter &slave) -> bool {
                if (local_assembler(master, slave)) {
                    return true;
                }

                return false;
            });

            Real vol = local_assembler.algo.intersection_measure();

            comm.all_reduce(&vol, 1, MPISum());

            Real sum_mat_B = buffers.B.m_matrix.sum();
            Real sum_mat_D = buffers.D.m_matrix.sum();

            post_process(master, slave);

            elapsed = MPI_Wtime() - elapsed;

            if (Moonolith::instance().verbose()) {
                logger() << "time ParLowerDimL2Transfer::assemble: " << elapsed << std::endl;
                logger() << "vol: " << vol << " sum(B): " << sum_mat_B << " sum(D): " << sum_mat_D << std::endl;
            }

            return vol > 0.0;
        }

        bool assemble(const SpaceT &space, const std::vector<std::pair<int, int>> &tags, const T box_tol = 0.0) {
            Real elapsed = MPI_Wtime();

            SingleSpaceAlgorithmT algo(comm, moonolith::make_unique<CollectionManager<SpaceT>>(comm));

            algo.init(space, tags, box_tol);

            elapsed = MPI_Wtime() - elapsed;
            logger() << "init: " << elapsed << std::endl;

            ////////////////////////////////////////////////////
            /////////////////// pair-wise method ///////////////////////
            elapsed = MPI_Wtime();

            local_assembler.clear();

            algo.compute([&](const ElemAdapter &master, const ElemAdapter &slave) -> bool {
                if (local_assembler(master, slave)) {
                    return true;
                }

                return false;
            });

            Real vol = local_assembler.algo.intersection_measure();

            comm.all_reduce(&vol, 1, MPISum());

            Real sum_mat_B = buffers.B.m_matrix.sum();
            Real sum_mat_D = buffers.D.m_matrix.sum();

            post_process(space, space);

            elapsed = MPI_Wtime() - elapsed;
            if (Moonolith::instance().verbose()) {
                logger() << "time ParLowerDimL2Transfer::assemble: " << elapsed << std::endl;
                logger() << "vol: " << vol << " sum(B): " << sum_mat_B << " sum(D): " << sum_mat_D << std::endl;
            }

            return vol > 0.0;
        }

        // inline void remove_incomplete_intersections(const bool val)
        // {
        //     remove_incomplete_intersections_ = val;
        // }

        Communicator &comm;
        MatrixBuffers<T> buffers;
        LocalLowerDimL2Assembler<MasterElem, SlaveElem> local_assembler;

        // private:
        //     bool remove_incomplete_intersections_;
    };

}  // namespace moonolith

#endif  // MOONOLITH_PAR_LOWER_DIM_L2_TRANSFER_HPP
