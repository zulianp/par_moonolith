#ifndef MOONOLITH_COLLECTION_MANAGER_HPP
#define MOONOLITH_COLLECTION_MANAGER_HPP

#include "moonolith_collection_traits.hpp"
#include "moonolith_function_space.hpp"
#include "moonolith_make_unique.hpp"
#include "moonolith_mesh.hpp"

namespace moonolith {

    template <typename T, int Dim>
    class CollectionManager<Mesh<T, Dim>> {
    public:
        using Mesh = moonolith::Mesh<T, Dim>;
        using Elem = typename Mesh::Elem;
        using Point = typename Mesh::Point;

        CollectionManager(const Communicator &comm, const bool is_auto_mode = false)
            : comm_(comm), is_auto_mode_(is_auto_mode) {}

        template <class Adapter>
        static void data_added(const Integer local_idx, const Adapter &a) {
            (void)local_idx;
            (void)a;
        }

        static const Elem &elem(const Mesh &mesh, const Integer handle) { return mesh.elem(handle); }

        Integer tag(const Mesh &mesh, const Integer handle) {
            if (is_auto_mode_) return -1;

            return mesh.elem(handle).block;
        }

        static Integer n_elements(const Mesh &mesh) { return mesh.n_elements(); }

        static Integer elements_begin(const Mesh &) { return 0; }

        static Integer elements_end(const Mesh &mesh) { return mesh.n_elements(); }

        static Integer handle(const Mesh &, const Integer element_index) { return element_index; }

        static bool skip(const Mesh &, const Integer &) { return false; }

        template <class Bound>
        static void fill_bound(const Mesh &mesh, const Integer handle, Bound &bound, const Real blow_up) {
            mesh.fill_bound(handle, bound, blow_up);
        }

        template <class Iterator>
        static void serialize(const Mesh &mesh,
                              const Iterator &begin,
                              const Iterator &end,
                              moonolith::OutputStream &os) {
            mesh.serialize(begin, end, os);
        }

        std::unique_ptr<Mesh> build(moonolith::InputStream &is) const {
            std::unique_ptr<Mesh> m = moonolith::make_unique<Mesh>(comm_);
            m->build(is);
            return m;
        }

        inline bool is_auto_mode() const { return is_auto_mode_; }

    private:
        Communicator comm_;
        bool is_auto_mode_;
    };

    template <typename T, int Dim>
    class CollectionManager<FunctionSpace<Mesh<T, Dim>>> {
    public:
        using Mesh = moonolith::Mesh<T, Dim>;
        using FunctionSpace = moonolith::FunctionSpace<Mesh>;
        using MeshManager = moonolith::CollectionManager<Mesh>;
        using Elem = typename Mesh::Elem;

        CollectionManager(Communicator &comm, const bool is_auto_mode = false) : mesh_manager_(comm, is_auto_mode) {}

        template <class Adapter>
        static void data_added(const Integer local_idx, const Adapter &a) {
            MeshManager::data_added(local_idx, a);
        }

        static const Elem &elem(const FunctionSpace &space, const Integer handle) {
            return MeshManager::elem(space.mesh(), handle);
        }

        Integer tag(const FunctionSpace &space, const Integer handle) {
            if (mesh_manager_.is_auto_mode()) {
                return -1;
            } else {
                return space.dof_map().block(handle);
            }
        }

        static Integer n_elements(const FunctionSpace &space) { return MeshManager::n_elements(space.mesh()); }

        static Integer elements_begin(const FunctionSpace &space) { return MeshManager::elements_begin(space.mesh()); }

        static Integer elements_end(const FunctionSpace &space) { return MeshManager::elements_end(space.mesh()); }

        static Integer handle(const FunctionSpace &space, const Integer element_index) {
            return MeshManager::handle(space.mesh(), element_index);
        }

        static bool skip(const FunctionSpace &space, const Integer &element_index) {
            return MeshManager::skip(space.mesh(), element_index);
        }

        template <class Bound>
        static void fill_bound(const FunctionSpace &space, const Integer handle, Bound &bound, const Real blow_up) {
            MeshManager::fill_bound(space.mesh(), handle, bound, blow_up);
        }

        template <class Iterator>
        static void serialize(const FunctionSpace &space,
                              const Iterator &begin,
                              const Iterator &end,
                              moonolith::OutputStream &os) {
            CHECK_STREAM_WRITE_BEGIN("fs_serialize", os);
            MeshManager::serialize(space.mesh(), begin, end, os);
            space.dof_map().serialize(begin, end, os);
            CHECK_STREAM_WRITE_END("fs_serialize", os);
        }

        std::unique_ptr<FunctionSpace> build(moonolith::InputStream &is) const {
            CHECK_STREAM_READ_BEGIN("fs_serialize", is);

            auto space = moonolith::make_unique<FunctionSpace>(mesh_manager_.build(is));
            space->dof_map().build(is);

            CHECK_STREAM_READ_END("fs_serialize", is);
            return space;
        }

    private:
        MeshManager mesh_manager_;
    };

}  // namespace moonolith

#endif  // MOONOLITH_COLLECTION_MANAGER_HPP
