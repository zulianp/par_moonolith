#ifndef MOONOLITH_PREDICATE_HPP
#define MOONOLITH_PREDICATE_HPP

#include "moonolith_serializable.hpp"
#include "moonolith_input_stream.hpp"
#include "moonolith_output_stream.hpp"

#include <set>
#include <assert.h>

namespace moonolith {

    class Predicate {
    public:
        virtual ~Predicate() { }

        ///@brief allows for process wise discrimination, by default it is always true
        // virtual bool areRelated(const Integer localRank, const Integer remote_rank)  const
        // {
        // 	(void)(localRank);
        // 	(void)(remote_rank);
        // 	return true;
        // }

        virtual bool select(const Integer tag) const {
            (void)(tag);
            return true;
        }


        virtual bool are_master_and_slave(const Integer tag1, const Integer tag2) const {
            (void)(tag1);
            (void)(tag2);
            return true;
        }

        ///@brief  simmetry must be true tags_are_related(tag1, tag2) == tags_are_related(tag2, tag1)
        virtual bool tags_are_related(const Integer tag1, const Integer tag2) const {
            (void)(tag1);
            (void)(tag2);
            return true;
        }

        template<class Iterator1, class Iterator2>
        bool tags_are_related(const Iterator1 &begin1, const Iterator1 &end1, const Iterator2 &begin2,
                            const Iterator2 &end2) const {
            for (Iterator1 it = begin1; it != end1; ++it) {
                if (tags_are_related(*it, begin2, end2)) return true;
            }
            return false;
        }

        template<class Iterator>
        bool tags_are_related(const Integer tag, const Iterator &begin, const Iterator &end) const {
            for (Iterator it = begin; it != end; ++it) {
                if (tags_are_related(tag, *it)) {
                    assert(tags_are_related(*it, tag));
                    return true;
                }

                assert(!tags_are_related(*it, tag));
            }

            return false;
        }

        ///@brief override to tell if the serial size changes on the different processes (true) or it is the same (false)
        ///@return true by default
        virtual bool has_static_serial_size() const {
            return true;
        }

        virtual void write_local(OutputStream &os) const {
            (void) os;
        }

        virtual void read_merge_for(const Integer remote_rank, InputStream &is) {
            (void) remote_rank;
            (void) is;
        }

        virtual bool match_local_data() const {
            return true;
        }
    };

    class MultigridPredicate : public Predicate {
    public:
        virtual bool tags_are_related(const Integer tag1, const Integer tag2) const override {
            return tag1 + 1 == tag2 || tag1 - 1 == tag2 || tag2 + 1 == tag1;
        }

        //level l is slave of level l-1
        virtual bool are_master_and_slave(const Integer tag1, const Integer tag2) const override {
            return tag1 == tag2 + 1;
        }
    };

    class SkipSamePredicate : public Predicate {
    public:
        virtual bool tags_are_related(const Integer tag1, const Integer tag2) const override {
            return tag1 != tag2;
        }

        virtual bool are_master_and_slave(const Integer tag1, const Integer tag2) const override {
            return tag1 < tag2;
        }
    };

    class IPairPredicate : public Predicate {
    public:
        virtual void add(const Integer tag1, const Integer tag2) = 0;

        virtual ~IPairPredicate() { }
    };

    class SkipPairs : public IPairPredicate {
    public:


        void add(const Integer tag1, const Integer tag2) override {
            tags_.insert(std::make_pair(tag1, tag2));

            if (tag1 != tag2)
                tags_.insert(std::make_pair(tag2, tag1));
        }

        virtual bool tags_are_related(const Integer tag1, const Integer tag2) const override {
            return tags_.find(std::make_pair(tag1, tag2)) == tags_.end();
        }

        void clear() {
            tags_.clear();
        }

    private:
        std::set<std::pair<Integer, Integer> > tags_;
    };


    class ChoosePairs : public IPairPredicate {
    public:

        void add(const Integer tag1, const Integer tag2) override {
            tags_.insert(std::make_pair(tag1, tag2));

            if (tag1 != tag2)
                tags_.insert(std::make_pair(tag2, tag1));
        }

        virtual bool tags_are_related(const Integer tag1, const Integer tag2) const override {
            return tags_.find(std::make_pair(tag1, tag2)) != tags_.end();
        }

        void clear() {
            tags_.clear();
        }

        virtual ~ChoosePairs() { }

    private:
        std::set<std::pair<Integer, Integer> > tags_;
    };

    class MasterAndSlave : public IPairPredicate {
    public:
        using IPairPredicate::tags_are_related;

        void add(const Integer tag1, const Integer tag2) override {
            tags_.insert(std::make_pair(tag1, tag2));
            to_select_.insert(tag1);
            to_select_.insert(tag2);
        }

        virtual bool tags_are_related(const Integer tag1, const Integer tag2) const override {
            return tags_.find(std::make_pair(tag1, tag2)) != tags_.end() ||
                   tags_.find(std::make_pair(tag2, tag1)) != tags_.end();
        }

        virtual bool are_master_and_slave(const Integer tag1, const Integer tag2) const override {
            return tags_.find(std::make_pair(tag1, tag2)) != tags_.end();
        }

        inline bool is_master(const Integer tag) const {
            for(const auto &t : tags_) {
                if(t.first == tag) {
                    return true;
                }
            }

            return false;
        }

        inline bool is_slave(const Integer tag) const {
            for(const auto &t : tags_) {
                if(t.second == tag) {
                    return true;
                }
            }
            
            return false;
        }


        void clear() {
            tags_.clear();
            to_select_.clear();
        }

        virtual bool select(const Integer tag) const override {
            return to_select_.find(tag) != to_select_.end();
        }

    private:
        std::set<std::pair<Integer, Integer> > tags_;
        std::set<Integer> to_select_;
    };

}

#endif //MOONOLITH_PREDICATE_HPP
