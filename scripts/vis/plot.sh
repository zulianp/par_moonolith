#!/bin/bash
make -j4 example_illustrate_intersections
./examples/example_illustrate_intersections

# declare -a arr=("tet4")
# declare -a arr=("hex8" "warped_hex8" "warped_hex8_decomposed")
# declare -a arr=("hex27" "warped_hex27" "warped_hex27_decomposed")
# declare -a arr=("tet10" "warped_tet10" "warped_tet10_decomposed" "prism6")
# declare -a arr=("tri3" "tri6" "warped_tri6")

declare -a arr=("quad4" "quad8" "quad9" "warped_quad8" "warped_quad9" )

for i in "${arr[@]}"
do
   echo "$i"
   matlab -nodisplay -nodesktop -r "run('./$i.m'); quit;"
   potrace --tight --longcurve  $i.bmp
   open $i.eps
done

mkdir eps
mv *.eps eps/