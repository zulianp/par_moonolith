#ifndef MOONOLITH_ELEM_HEX_8_HPP
#define MOONOLITH_ELEM_HEX_8_HPP

#include "moonolith_convex_decomposition.hpp"
#include "moonolith_elem_hex.hpp"

namespace moonolith {

    template <typename T_, int PhysicalDim_ = 3>
    class Hex8 final : public Hex<T_, 1, PhysicalDim_> {
    public:
        static const int Order = 1;
        static const int Dim = 3;
        static const int PhysicalDim = PhysicalDim_;
        static const int NNodes = 8;

        using T = T_;

        using Vector = moonolith::Vector<T, Dim>;
        using CoVector = moonolith::Vector<T, PhysicalDim>;

        using Point = moonolith::Vector<T, Dim>;
        using CoPoint = moonolith::Vector<T, PhysicalDim>;

        using Super = moonolith::Hex<T_, 1, PhysicalDim_>;
        using Super::make;

    private:
        class Grad final {
        public:
            Grad() {
                // f = (1.0 - p.x) * (1.0 - p.y) * (1.0 - p.z);
                f[0] = [](const Point &p, Vector &g) {
                    g.x = -(1.0 - p.y) * (1.0 - p.z);
                    g.y = -(1.0 - p.x) * (1.0 - p.z);
                    g.z = -(1.0 - p.x) * (1.0 - p.y);
                };

                // f = p.x * (1.0 - p.y) * (1.0 - p.z);
                f[1] = [](const Point &p, Vector &g) {
                    g.x = (1.0 - p.y) * (1.0 - p.z);
                    g.y = -p.x * (1.0 - p.z);
                    g.z = -p.x * (1.0 - p.y);
                };

                // f = p.x * p.y * (1.0 - p.z);
                f[2] = [](const Point &p, Vector &g) {
                    g.x = p.y * (1.0 - p.z);
                    g.y = p.x * (1.0 - p.z);
                    g.z = -p.x * p.y;
                };

                // f = (1.0 - p.x) * p.y * (1.0 - p.z);
                f[3] = [](const Point &p, Vector &g) {
                    g.x = -p.y * (1.0 - p.z);
                    g.y = (1.0 - p.x) * (1.0 - p.z);
                    g.z = -(1.0 - p.x) * p.y;
                };

                // f = (1.0 - p.x) * (1.0 - p.y) * p.z;
                f[4] = [](const Point &p, Vector &g) {
                    g.x = -(1.0 - p.y) * p.z;
                    g.y = -(1.0 - p.x) * p.z;
                    g.z = (1.0 - p.x) * (1.0 - p.y);
                };

                // f = p.x * (1.0 - p.y) * p.z;
                f[5] = [](const Point &p, Vector &g) {
                    g.x = (1.0 - p.y) * p.z;
                    g.y = -p.x * p.z;
                    g.z = p.x * (1.0 - p.y);
                };

                // f = p.x * p.y * p.z;
                f[6] = [](const Point &p, Vector &g) {
                    g.x = p.y * p.z;
                    g.y = p.x * p.z;
                    g.z = p.x * p.y;
                };

                // f = (1.0 - p.x) * p.y * p.z;
                f[7] = [](const Point &p, Vector &g) {
                    g.x = -p.y * p.z;
                    g.y = (1.0 - p.x) * p.z;
                    g.z = (1.0 - p.x) * p.y;
                };
            }

            std::array<std::function<void(const Point &, Vector &)>, NNodes> f;
        };

        class Fun final {
        public:
            Fun() {
                f[0] = [](const Point &p) -> T { return (1.0 - p.x) * (1.0 - p.y) * (1.0 - p.z); };

                f[1] = [](const Point &p) -> T { return p.x * (1.0 - p.y) * (1.0 - p.z); };

                f[2] = [](const Point &p) -> T { return p.x * p.y * (1.0 - p.z); };

                f[3] = [](const Point &p) -> T { return (1.0 - p.x) * p.y * (1.0 - p.z); };

                f[4] = [](const Point &p) -> T { return (1.0 - p.x) * (1.0 - p.y) * p.z; };

                f[5] = [](const Point &p) -> T { return p.x * (1.0 - p.y) * p.z; };

                f[6] = [](const Point &p) -> T { return p.x * p.y * p.z; };

                f[7] = [](const Point &p) -> T { return (1.0 - p.x) * p.y * p.z; };
            }

            std::array<std::function<T(const Point &)>, NNodes> f;
        };

    public:
        T fun(const Integer i, const Point &p) const override { return fun_.f[i](p); }

        void grad(const Integer i, const Point &p, Vector &g) const override { grad_.f[i](p, g); }

        void hessian(const Integer, const Point &, std::array<T, Dim * Dim> &H) {
            std::fill(std::begin(H), std::end(H), 0.0);
        }

        void jacobian(const Point &p, std::array<T, PhysicalDim * Dim> &J) const override {
            if (is_affine()) {
                affine_approx_jacobian(J);
            } else {
                IsoParametric<Hex8>::jacobian(*this, p, J);
            }
        }

        void affine_approx_jacobian(std::array<T, PhysicalDim * Dim> &J) const override {
            Affine<Hex8>::jacobian(*this, {0, 1, 3, 4}, J);
        }

        CoPoint &node(const Integer i) override { return nodes_[i]; }

        const CoPoint &node(const Integer i) const override { return nodes_[i]; }

        void point(const Point &p, CoPoint &q) const override { IsoParametric<Hex8>::point(*this, p, q); }

        Hex8() : is_affine_(false) {}

        bool is_affine() const override { return is_affine_; }

        inline int n_nodes() const override { return NNodes; }
        inline int order() const override { return Order; }

        const std::array<CoPoint, NNodes> &nodes() const { return nodes_; }

        inline void set_affine(const bool val) { is_affine_ = val; }

        inline void make_reference() {
            Reference<Hex8>::points(nodes_);
            set_affine(true);
        }

        inline ElemType type() const override { return HEX8; }

        inline bool is_simplex() const override { return false; }

        inline T approx_measure() const override {
            if (is_affine()) {
                return approx_hexahedron_volume(nodes_);
            } else {
                assert(false);
                return approx_hexahedron_volume(nodes_);
            }
        }

        inline T measure() const override {
            if (is_affine()) {
                return approx_hexahedron_volume(nodes_);
            } else {
                Quadrature<T, Dim> q;
                Gauss::Hex::get(1, q);
                return IsoParametric<Hex8>::measure(*this, q);
            }
        }

        inline T reference_measure() const override { return 1.0; }

        inline void make(Polyhedron<T> &poly) const override {
            assert(is_affine());
            ConvexDecomposition<T, PhysicalDim>::make_hex(nodes(), 0, 1, 2, 3, 4, 5, 6, 7, poly);
        }

        inline void make(Storage<Polyhedron<T>> &poly) const override {
            if (is_affine()) {
                poly.resize(1);
                make(poly[0]);
            } else {
                ConvexDecomposition<T, PhysicalDim>::decompose_non_affine_hex8(nodes(), poly);

                // std::array<CoPoint, 27> nodes_27;
                // ConvexDecomposition<T, PhysicalDim>::make_hex27_from_hex8(nodes(), nodes_27);
                // ConvexDecomposition<T, PhysicalDim>::decompose_hex27(nodes_27, poly);
            }
        }

    private:
        bool is_affine_;
        std::array<CoPoint, NNodes> nodes_;
        const Fun fun_;
        const Grad grad_;
    };

    template <typename T, int PhysicalDim>
    class Reference<Hex8<T, PhysicalDim>> {
    public:
        using Point = typename Hex8<T, PhysicalDim>::Point;
        static const int NNodes = 8;

        template <std::size_t Size>
        static void points(std::array<Point, Size> &nodes) {
            static_assert(Size >= NNodes, "size must be at least NNodes");
            ///////////////////////
            // Lower nodes

            // p0
            nodes[0].x = 0.0;
            nodes[0].y = 0.0;
            nodes[0].z = 0.0;

            // p1
            nodes[1].x = 1.0;
            nodes[1].y = 0.0;
            nodes[1].z = 0.0;

            // p2
            nodes[2].x = 1.0;
            nodes[2].y = 1.0;
            nodes[2].z = 0.0;

            // p3
            nodes[3].x = 0.0;
            nodes[3].y = 1.0;
            nodes[3].z = 0.0;

            ///////////////////////

            // Upper nodes

            // p4
            nodes[4].x = 0.0;
            nodes[4].y = 0.0;
            nodes[4].z = 1.0;

            // p5
            nodes[5].x = 1.0;
            nodes[5].y = 0.0;
            nodes[5].z = 1.0;

            // p6
            nodes[6].x = 1.0;
            nodes[6].y = 1.0;
            nodes[6].z = 1.0;

            // p7
            nodes[7].x = 0.0;
            nodes[7].y = 1.0;
            nodes[7].z = 1.0;
        }

        static T measure() { return 1.0; }
    };

    template <typename T, int PhysicalDim>
    class GaussQRule<Hex8<T, PhysicalDim>> {
    public:
        static bool get(const Integer order, Quadrature3<T> &q) { return Gauss::Hex::get(order, q); }
    };

}  // namespace moonolith

#endif  // MOONOLITH_ELEM_HEX_8_HPP
