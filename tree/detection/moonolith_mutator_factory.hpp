#ifndef MOONOLITH_MUTATOR_FACTORY_HPP
#define MOONOLITH_MUTATOR_FACTORY_HPP 

#include "moonolith_forward_declarations.hpp"
#include "moonolith_remote_node.hpp"
#include "moonolith_bound_synchronize.hpp"
#include "moonolith_local_gather.hpp"
#include "moonolith_refine_all.hpp"
#include "moonolith_data_handle.hpp"

#include <memory>

namespace moonolith {

	template<class Traits>
	class MutatorFactory {
	public:
		typedef typename Traits::DataType DataType;
		typedef typename Traits::Bound Bound;
		typedef typename moonolith::Node<Traits> Node;

		virtual ~MutatorFactory() {}

		virtual std::shared_ptr< Mutator<Traits> > new_can_refine() const = 0;
		virtual std::shared_ptr< Mutator<Traits> > new_refine() const = 0;

		virtual std::shared_ptr< Mutator<Traits> > new_insert(const DataHandle &handle, const DataType &data) const = 0;
		virtual std::shared_ptr< Mutator<Traits> > new_bound_expand(const DataType &data) const = 0;
		virtual std::shared_ptr< Mutator<Traits> > new_bound_expand(const Bound &bound) const = 0;
		virtual std::shared_ptr< Mutator<Traits> > new_match(const DataType &data) const = 0;
		
		virtual std::shared_ptr< Mutator<Traits> > new_local_gather() const
		{
			return std::make_shared< LocalGather<Traits> >();
		}

        virtual std::shared_ptr< Mutator<Traits> > new_refine_all() const
        {
            return std::make_shared< RefineAll<Traits> >();
        }

		virtual std::shared_ptr< Hash<Traits> > new_hash() const = 0;

		///@brief override it to extend with new mutators (does not return default mutators)
		virtual std::shared_ptr< Mutator<Traits> > new_mutator(const std::string &)
		{
			return nullptr;
		}

		virtual std::shared_ptr< NodeNavigator<Traits> > new_node_navigator() const
		{
			assert(false && "Implement in subclass");
			return nullptr;
		}

		//FIXME find the right place for this or change the name of this class
		virtual std::shared_ptr< RemoteNode<Node> > new_remote_node() const
		{
			return std::make_shared< RemoteNode<Node> >();
		}

		virtual std::shared_ptr< BoundSynchronize<Traits> > new_bound_synchronize() const = 0; 
		
	};
}

#endif //MOONOLITH_MUTATOR_FACTORY_HPP


