#ifndef MOONOLITH_INTERSECT_LINES_IMPL_HPP
#define MOONOLITH_INTERSECT_LINES_IMPL_HPP

#include "moonolith_intersect_lines.hpp"
#include <cmath>
#include <algorithm>

namespace moonolith {

	template<typename T, int Dim>
	bool IntersectLines<T, Dim>::apply(const Line &line1, const Line &line2, Storage<Point> &line_or_point)
	{
		line_or_point.clear();

		const auto &p1 = line1.p0;
		const auto &p2 = line1.p1;

		const auto &q1 = line2.p0;
		const auto &q2 = line2.p1;

		u = p2 - p1;
		v = q2 - q1;

		auto len_u = length(u);
		auto len_v = length(v);

		u /= len_u;
		v /= len_v;

		const T cos_angle = dot(u, v);

		if(std::abs(std::abs(cos_angle) - 1.) > 1e-14) {
			//LOWER DIMENSIONAL

			//not collinear
			Ray<T, Dim> ray;
			ray.o = q1;
			ray.dir = v;

			T t = 0.0;
			if(!line1.intersect(ray, t)) {
				return false;
			}

			if(t < 1e-14 || t > 1 + 1e-14) {
				return false;
			}
			
			v *= t;
			v += q1;

			line_or_point.resize(1);
			line_or_point[0] = v;
			return true;

		} else {
			//EQUI DIMENSIONAL
			
			w = q2 - p1;
			auto len_w = length(w);

			if(len_w > 1e-14) {
				w /= len_w;
				const T cos_angle_2 = dot(w, u);
				if(std::abs(std::abs(cos_angle_2) - 1.) > 1e-14) {
					//not on the same plane
					return false;
				}
			}

			const T up1 = 0.;
			const T up2 = dot(u, (p2 - p1));

			const T min_p = std::min(up1, up2);
			const T max_p = std::max(up1, up2);

			const T uq1 = dot(u, (q1 - p1));
			const T uq2 = dot(u, (q2 - p1));

			const T min_q = std::min(uq1, uq2);
			const T max_q = std::max(uq1, uq2);

			if(max_q <= min_p) {
				return false;
			}

			if(max_p <= min_q) {
				return false;
			}

			T isect_min = std::max(min_p, min_q);
			T isect_max = std::min(max_p, max_q);
			T isect_len = isect_max - isect_min;

			line_or_point.resize(2);

			line_or_point[0] = p1 + isect_min * u;
			line_or_point[1] = p1 + isect_max * u;

			if(isect_len < 1e-16) {
				return false;
			}

			return true;
		}
	}

	template<typename T, int Dim>
	bool IntersectLines<T, Dim>::apply(const Line &line1, const Line &line2, Line &result)
	{
		const auto &p1 = line1.p0;
		const auto &p2 = line1.p1;

		const auto &q1 = line2.p0;
		const auto &q2 = line2.p1;

		u = p2 - p1;
		v = q2 - q1;

		auto len_u = length(u);
		auto len_v = length(v);

		u /= len_u;
		v /= len_v;

		const T cos_angle = dot(u, v);

		if(std::abs(std::abs(cos_angle) - 1.) > 1e-14) {
			//not collinear
			return false;
		}

		w = q2 - p1;
		auto len_w = length(w);

		if(len_w > 1e-14) {
			w /= len_w;
			const T cos_angle_2 = dot(w, u);
			if(std::abs(std::abs(cos_angle_2) - 1.) > 1e-14) {
				//not on the same plane
				return false;
			}
		}

		const T up1 = 0.;
		const T up2 = dot(u, (p2 - p1));

		const T min_p = std::min(up1, up2);
		const T max_p = std::max(up1, up2);

		const T uq1 = dot(u, (q1 - p1));
		const T uq2 = dot(u, (q2 - p1));

		const T min_q = std::min(uq1, uq2);
		const T max_q = std::max(uq1, uq2);

		if(max_q <= min_p) {
			return false;
		}

		if(max_p <= min_q) {
			return false;
		}

		T isect_min = std::max(min_p, min_q);
		T isect_max = std::min(max_p, max_q);
		T isect_len = isect_max - isect_min;

		result.p0 = p1 + isect_min * u;
		result.p1 = p1 + isect_max * u;

		if(isect_len < 1e-16) {
			return false;
		}

		return true;
	}
	
}

#endif //MOONOLITH_INTERSECT_LINES_IMPL_HPP