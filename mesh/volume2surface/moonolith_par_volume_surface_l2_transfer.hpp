#ifndef MOONOLITH_PAR_L2_TRANSFER_SPECIALIZATION_HPP
#define MOONOLITH_PAR_L2_TRANSFER_SPECIALIZATION_HPP

#include "moonolith_par_l2_transfer.hpp"
#include "moonolith_volume_surface_l2_assembler.hpp"

namespace moonolith {

    template <typename T, Integer PhysicalDim>
    class ParVolumeSurfaceL2Transfer {
    public:
        using MeshT = moonolith::Mesh<T, PhysicalDim>;
        using SpaceT = moonolith::FunctionSpace<MeshT>;

        using SpaceAlgorithmT = moonolith::ManyMastersOneSlaveAlgorithm<PhysicalDim, SpaceT>;

        using ElemAdapter = typename SpaceAlgorithmT::Adapter;

        using MasterElem = moonolith::Elem<T, PhysicalDim, PhysicalDim>;
        using SlaveElem = moonolith::Elem<T, PhysicalDim - 1, PhysicalDim>;

        ParVolumeSurfaceL2Transfer(Communicator &comm)
            : comm(comm), buffers(comm), local_assembler(buffers), remove_incomplete_intersections_(false) {}

        void post_process(const SpaceT &master, const SpaceT &slave) {
            auto n_master_dofs = master.dof_map().n_local_dofs();
            auto n_slave_dofs = slave.dof_map().n_local_dofs();

            buffers.B.finalize(n_slave_dofs, n_master_dofs);
            buffers.D.finalize(n_slave_dofs, n_slave_dofs);
            buffers.Q.finalize(n_slave_dofs, n_slave_dofs);
        }

        bool assemble(const SpaceT &master, const SpaceT &slave, const T box_tol = 0.0) {
            Real elapsed = MPI_Wtime();

            SpaceAlgorithmT algo(comm, moonolith::make_unique<CollectionManager<SpaceT>>(comm, true));

            algo.init_simple(master, slave, box_tol);

            elapsed = MPI_Wtime() - elapsed;

            if (Moonolith::instance().verbose()) {
                logger() << "init: " << elapsed << std::endl;
            }

            ////////////////////////////////////////////////////
            /////////////////// pair-wise method ///////////////////////
            elapsed = MPI_Wtime();

            local_assembler.clear();

            algo.compute([&](const Storage<ElemAdapter> &master, const ElemAdapter &slave) -> bool {
                if (local_assembler(master, slave)) {
                    return true;
                }

                return false;
            });

            Real vol = local_assembler.assembler.intersection_measure();

            comm.all_reduce(&vol, 1, MPISum());

            Real sum_mat_B = buffers.B.m_matrix.sum();
            Real sum_mat_D = buffers.D.m_matrix.sum();

            post_process(master, slave);

            elapsed = MPI_Wtime() - elapsed;
            if (Moonolith::instance().verbose()) {
                logger() << "time ParVolumeSurfaceL2Transfer::assemble: " << elapsed << std::endl;
                logger() << "vol: " << vol << " sum(B): " << sum_mat_B << " sum(D): " << sum_mat_D << std::endl;
            }

            return vol > 0.0;
        }

        bool assemble(const SpaceT &master,
                      const SpaceT &slave,
                      const std::vector<std::pair<int, int>> &tags,
                      const T box_tol = 0.0) {
            Real elapsed = MPI_Wtime();

            SpaceAlgorithmT algo(comm, moonolith::make_unique<CollectionManager<SpaceT>>(comm, false));

            algo.init_simple(master, slave, tags, box_tol);

            elapsed = MPI_Wtime() - elapsed;

            if (Moonolith::instance().verbose()) {
                logger() << "init: " << elapsed << std::endl;
            }

            ////////////////////////////////////////////////////
            /////////////////// pair-wise method ///////////////////////
            elapsed = MPI_Wtime();

            local_assembler.clear();

            algo.compute([&](const Storage<ElemAdapter> &master, const ElemAdapter &slave) -> bool {
                if (local_assembler(master, slave)) {
                    return true;
                }

                return false;
            });

            Real vol = local_assembler.assembler.intersection_measure();

            comm.all_reduce(&vol, 1, MPISum());

            Real sum_mat_B = buffers.B.m_matrix.sum();
            Real sum_mat_D = buffers.D.m_matrix.sum();

            post_process(master, slave);

            elapsed = MPI_Wtime() - elapsed;

            if (Moonolith::instance().verbose()) {
                logger() << "time ParVolumeSurfaceL2Transfer::assemble: " << elapsed << std::endl;
                logger() << "vol: " << vol << " sum(B): " << sum_mat_B << " sum(D): " << sum_mat_D << std::endl;
            }
            return vol > 0.0;
        }

        inline void remove_incomplete_intersections(const bool val) { remove_incomplete_intersections_ = val; }

        Communicator &comm;
        MatrixBuffers<T> buffers;
        VolumeSurfaceL2Assembler<MasterElem, SlaveElem> local_assembler;

    private:
        bool remove_incomplete_intersections_;
    };

}  // namespace moonolith

#endif  // MOONOLITH_PAR_L2_TRANSFER_SPECIALIZATION_HPP
