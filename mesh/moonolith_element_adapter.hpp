#ifndef MOONOLITH_ELEMENT_ADAPTER_HPP
#define MOONOLITH_ELEMENT_ADAPTER_HPP

#include "moonolith_check_stream.hpp"
#include "moonolith_collection_traits.hpp"
#include "moonolith_describable.hpp"
#include "moonolith_serializable.hpp"

namespace moonolith {

    template <class Bound, class Collection>
    class ElementAdapterBase : public Serializable, public Describable {
    public:
        using CM = moonolith::CollectionManager<Collection>;
        using Elem = typename CM::Elem;

        const Collection *collection_;
        Integer element_handle_, handle_;
        Integer tag_;
        Bound bound_;

        virtual ~ElementAdapterBase() {}

        ElementAdapterBase(const Collection *collection,
                           const Integer element_handle,
                           const Integer handle,
                           const Integer tag,
                           const Real blow_up)
            : collection_(collection), element_handle_(element_handle), handle_(handle), tag_(tag) {
            assert(collection);
            CM::fill_bound(*collection, element_handle, bound_, blow_up);
        }

        ElementAdapterBase() : collection_(nullptr), element_handle_(-1), tag_(-1) {}

        const Bound &bound() const { return bound_; }

        Bound &bound() { return bound_; }

        // Collection &collection()
        // {
        //     assert(collection_);
        //     return *collection_;
        // }

        const Collection &collection() const {
            assert(collection_);
            return *collection_;
        }

        void apply_read_write(Stream &stream) override {
            stream &element_handle_;
            stream &handle_;
            stream &bound_;
        }

        void describe(std::ostream &os) const override { bound_.describe(os); }

        inline Integer element_handle() const { return element_handle_; }

        inline Integer handle() const { return handle_; }

        inline void set_element_handle(const Integer element_handle) { element_handle_ = element_handle; }

        inline Integer tag() const { return tag_; }

        const Elem &elem() const {
            assert(collection_);
            return CM::elem(*collection_, element_handle_);
        }
    };

    template <class Bound, class Collection>
    class ElementAdapter final : public ElementAdapterBase<Bound, Collection> {
    public:
        using super = moonolith::ElementAdapterBase<Bound, Collection>;
        using super::super;
    };
}  // namespace moonolith

#endif  // MOONOLITH_ELEMENT_ADAPTER_HPP
