#ifndef MOONOLITH_DATA_HANDLE_HPP
#define MOONOLITH_DATA_HANDLE_HPP


#include "moonolith_serializable.hpp"
#include "moonolith_forward_declarations.hpp"
#include "moonolith_stream.hpp"

#include <set>

namespace moonolith {
	class DataHandle : public Serializable {
	public:
		DataHandle(const Integer id = null_id())
		: id_(id)
		{}

		inline operator Integer() const
		{
			return id_;
		}

		inline static Integer null_id() 
		{
			return -1;
		}

		inline static bool is_null(const DataHandle &d)
		{
			return d.id_ == -1;
		}

		inline void apply_read_write(Stream &stream) override
		{
			stream & id_;
		}
		
		inline Integer id() const
		{
			return id_;
		}

	private:
		Integer id_;
	};

	typedef std::set<DataHandle>::const_iterator SelectionIter;
}

#endif //MOONOLITH_DATA_HANDLE_HPP

