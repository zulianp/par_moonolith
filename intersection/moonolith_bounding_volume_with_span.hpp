#ifndef MOONOLITH_BOUNDING_VOLUME_WITH_SPAN_HPP
#define MOONOLITH_BOUNDING_VOLUME_WITH_SPAN_HPP

#include "moonolith_aabb.hpp"
#include "moonolith_kdop.hpp"
#include "moonolith_vector.hpp"

namespace moonolith {
	
	template<Integer Dimension, typename T, class StaticBound_, class DynamicBound_>
	class BoundingVolumeWithSpan : public Describable, public Serializable {
	public:
		typedef StaticBound_ StaticBound;
		typedef DynamicBound_ DynamicBound;

		void read(InputStream &is) override
		{
			static_bound_.read(is);
			dynamic_bound_.read(is);
		}

		void write(OutputStream &os) const override
		{
			static_bound_.write(os);
			dynamic_bound_.write(os);
		}

		virtual bool intersects(const BoundingVolumeWithSpan &bound) const
		{
			return static_bound_.intersects(bound.static_bound_) && 
				   dynamic_bound_.intersects(bound.dynamic_bound_);
		}

		inline T min(const Integer coord) const
		{
			return static_bound_.min(coord);
		}

		inline T max(const Integer coord) const
		{
			return static_bound_.max(coord);
		}

		inline void set_min(const Integer coord, const T value) 
		{
			static_bound_.set_min(coord, value);
		}

		inline void set_max(const Integer coord, const T value) 
		{
			static_bound_.set_max(coord, value);
		}

		//expands to contain the union of this and BoundingVolumeWithSpan
		virtual BoundingVolumeWithSpan &operator +=(const BoundingVolumeWithSpan &bound)
		{
			static_bound_  += bound.static_bound_;
			dynamic_bound_ += bound.dynamic_bound_;
			return *this;
		}

		virtual BoundingVolumeWithSpan &operator +=(const std::array<T, Dimension> &point)
		{
			static_bound_ += point;
			dynamic_bound_ += point;
			return *this;
		}

		virtual BoundingVolumeWithSpan &operator +=(const Vector<T, Dimension> &point)
		{
			static_bound_ += point;
			dynamic_bound_ += point;
			return *this;
		}


		virtual BoundingVolumeWithSpan &operator +=(const std::vector<T> &point)
		{
			assert(point.size() == std::size_t(Dimension));

			std::array<T, Dimension> a_point;
			std::copy(std::begin(point), std::end(point), std::begin(a_point));

			static_bound_ += a_point;
			dynamic_bound_ += a_point;
			return *this;
		}


		virtual bool empty() const
		{
			return static_bound_.empty();
		}

			//puts this in the intial state
		virtual void clear()
		{
			static_bound_.clear();
			dynamic_bound_.clear();
		}

		static inline Integer n_dims() 
		{
			return Dimension;
		}

		BoundingVolumeWithSpan()
		{
			clear();
		}

		void describe(std::ostream &os) const override
		{
			os << "Static bound:\n"  << static_bound_  << "\n";
			os << "Dynamic bound:\n" << dynamic_bound_ << "\n";
		}

		inline StaticBound &static_bound() { return static_bound_; }
		inline const StaticBound &static_bound() const { return static_bound_; }

		inline DynamicBound &dynamic_bound() { return dynamic_bound_; }
		inline const DynamicBound &dynamic_bound() const { return dynamic_bound_; }

	private:
		StaticBound static_bound_;
		DynamicBound dynamic_bound_;
	};

	template<Integer Dimension, typename T>
	class AABBWithSpan : public BoundingVolumeWithSpan<Dimension, T, AABB<Dimension, T>, AABB<Dimension, T> > {};

	template<Integer Dimension, typename T>
	class AABBWithKDOPSpan {};

	template<typename T>
	class AABBWithKDOPSpan<1, T> : public BoundingVolumeWithSpan<1, T, AABB<1, T>, AABB<1, T> > {};

	template<typename T>
	class AABBWithKDOPSpan<2, T> : public BoundingVolumeWithSpan<2, T, AABB<2, T>, KDOP<2, 4, T> > {};

	template<typename T>
	class AABBWithKDOPSpan<3, T> : public BoundingVolumeWithSpan<3, T, AABB<3, T>, KDOP<3, 9, T> > {};

	//FIXME
	template<typename T>
	class AABBWithKDOPSpan<4, T> : public BoundingVolumeWithSpan<4, T, AABB<4, T>, AABB<4, T> > {};
}

#endif //MOONOLITH_BOUNDING_VOLUME_WITH_SPAN_HPP
