#ifndef MOONOLITH_BASE_HPP
#define MOONOLITH_BASE_HPP

#include "par_moonolith_config.hpp"

#include <cassert>
#include <string>

#define MOONOLITH_UNUSED(macro_expr_) (void)(macro_expr_)

namespace moonolith {
    void test_check_assertion(const bool expr,
                              const std::string &filename,
                              const int line,
                              const std::string &expr_string);

}

#ifndef NDEBUG
#define moonolith_test_assert(macro_expr_) assert(macro_expr_)
#else
#define moonolith_test_assert(macro_expr_) \
    moonolith::test_check_assertion(macro_expr_, __FILE__, __LINE__, #macro_expr_)
#endif

#if defined __has_cpp_attribute
#if __has_cpp_attribute(fallthrough)
#define MOONOLITH_FALLTHROUGH [[fallthrough]]
#else
#define MOONOLITH_FALLTHROUGH
#endif
#else
#define MOONOLITH_FALLTHROUGH
#endif

#endif  // MOONOLITH_BASE_HPP
