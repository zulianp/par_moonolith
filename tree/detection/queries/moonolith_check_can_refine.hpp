#ifndef MOONOLITH_CHECK_CAN_REFINE_HPP
#define MOONOLITH_CHECK_CAN_REFINE_HPP

#include "moonolith_can_refine.hpp"
#include "moonolith_stream_utils.hpp"

#include <memory>

namespace moonolith {

    template <class Tree>
    class CheckCanRefine : public Mutator<typename Tree::Traits> {
    public:
        typedef typename Tree::Traits Traits;
        typedef typename Traits::DataType DataType;
        typedef typename Traits::Bound Bound;

        typedef moonolith::Node<Traits> Node;
        typedef moonolith::Branch<Traits> Branch;
        typedef moonolith::Root<Traits> Root;
        typedef moonolith::Leaf<Traits> Leaf;
        typedef moonolith::TreeMemory<Traits> TreeMemory;

        CheckCanRefine() : _success(false) {}

        virtual NavigatorOption visit(Root &, TreeMemory &memory) {
            can_refine_ = memory.mutator_factory()->new_can_refine();
            return CONTINUE;
        }

        inline NavigatorOption visit(Leaf &leaf, TreeMemory &memory) {
            can_refine_->clear();
            leaf.accept(*can_refine_, memory);
            _success |= can_refine_->success();

            if (_success) {
                logger() << "iiiiiiiiiiiiiiiiiiiiiiiiii" << std::endl;
                leaf.describe(logger());
                logger() << "iiiiiiiiiiiiiiiiiiiiiiiiii" << std::endl;
            }

            if (_success) return STOP;

            return CONTINUE;
        }

        inline NavigatorOption visit(Branch &, TreeMemory &) { return CONTINUE; }

        virtual ~CheckCanRefine() {}

        inline bool success() const { return _success; }

    private:
        bool _success;
        std::shared_ptr<Mutator<Traits> > can_refine_;
    };
}  // namespace moonolith

#endif  // MOONOLITH_CHECK_CAN_REFINE_HPP
