
// general
#include "par_moonolith.hpp"

// specific
#include "moonolith_collection_manager.hpp"
#include "moonolith_intersect_polygons.hpp"
#include "moonolith_matlab_scripter.hpp"
#include "moonolith_mesh.hpp"
#include "moonolith_mesh_io.hpp"
#include "moonolith_one_master_one_slave_algorithm.hpp"

/// Compile examples with `make moonolith_examples`
/// Run for instance with  `mpiexec -np 2 ./examples/example_mesh_intersection`
int main(int argc, char *argv[]) {
    using namespace moonolith;
    static const int Dim = 2;

    // Type of the mesh (Custom meshes classes can be used but it requires some work)
    using MeshT = moonolith::Mesh<Real, Dim>;

    // Basic element type of the mesh
    using ElemT = MeshT::Elem;

    // Type of the algorithm
    using AlgoT = moonolith::OneMasterOneSlaveAlgorithm<Dim, MeshT>;

    // Element adapter which point to both mesh and element
    using Adapter = AlgoT::Adapter;

    Moonolith::Init(argc, argv);

    // World by default
    Communicator comm;
    MeshT mesh(comm);

    std::string path = "../examples/data/square_2.tri";
    if (argc >= 2) {
        path = argv[1];
    }

    // Mesh is read only on one process
    if (comm.is_root()) {
        TriMeshReader<MeshT> reader;
        if (!reader.read(path, mesh)) {
            error_logger() << "Could not read mesh at path: " << path << std::endl;
            Moonolith::Abort();
        }
    }

    mesh.finalize();

    // Create a smaller mesh
    Real scale_factor = 0.25;
    MeshT mesh_smaller(comm);
    mesh_smaller = mesh;
    mesh_smaller.scale(scale_factor);

    // Create and run ntersection-detection and load-balancing algorithm
    AlgoT algo(comm);
    algo.init_simple(mesh, mesh_smaller, 0.0);

    // Data-structures for computing intersections (this can be replaced with user application code)
    Polygon<Real, 2> m_poly, s_poly, isect;
    IntersectConvexPolygons<Real, 2> isect_algo;

    // Allocate space for copying points
    m_poly.resize(3);
    s_poly.resize(3);

    Real isect_area = 0.0;

    // Compute user function (in parallel execution data is migrated automatically from process to process, the task of
    // rearranging the result is left to the user)
    algo.compute([&](const Adapter &master, const Adapter &slave) -> bool {
        const ElemT &m = master.elem();
        const ElemT &s = slave.elem();

        const MeshT &m_mesh = master.collection();
        const MeshT &s_mesh = slave.collection();

        for (int i = 0; i < 3; ++i) {
            m_poly[i] = m_mesh.point(m.nodes[i]);
            s_poly[i] = s_mesh.point(s.nodes[i]);
        }

        // Compute intersection
        if (isect_algo.apply(m_poly, s_poly, isect)) {
            // Compute area of intersection
            isect_area += measure(isect);

            // Let Moonolith know that the operation was ok
            return true;
        } else {
            // Let Moonolith know that the pair was a false positive
            return false;
        }
    });

    comm.all_reduce(&isect_area, 1, MPI_SUM);

    if (comm.is_root()) {
        logger() << "computed_area: " << isect_area << " == expected_area: " << (scale_factor * scale_factor)
                 << std::endl;
    }

    return Moonolith::Finalize();
}
