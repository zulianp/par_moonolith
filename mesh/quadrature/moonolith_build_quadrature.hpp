#ifndef MOONOLITH_BUILD_QUADRATURE_HPP
#define MOONOLITH_BUILD_QUADRATURE_HPP

#include "moonolith_intersect_lines.hpp"
#include "moonolith_intersect_polygon_with_h_polyhedron.hpp"
#include "moonolith_intersect_polygons.hpp"
#include "moonolith_intersect_polyhedra.hpp"
#include "moonolith_intersect_polyline_with_convex_polygon.hpp"
#include "moonolith_line.hpp"
#include "moonolith_map_quadrature.hpp"

namespace moonolith {

    /**
     * @brief intersects the two shapes and builds a composite quadrature rule in global coordinates
     */
    template <class Shape1, class Shape2 = Shape1>
    class BuildQuadrature {};

    template <typename T, int Dim>
    class BuildQuadrature<Line<T, Dim> > {
    public:
        using Quadrature1 = moonolith::Quadrature<T, 1>;
        using Quadrature = moonolith::Quadrature<T, Dim>;
        using Line = moonolith::Line<T, Dim>;

        /// unscaled because test and trial roles are not defined yet
        bool apply(const Quadrature1 &ref, const Line &line1, const Line &line2, Quadrature &out) {
            if (!intersect_lines.apply(line1, line2, isect)) {
                return false;
            }

            map(ref, static_cast<T>(1.), isect, out);
            return true;
        }

    private:
        Line isect;
        IntersectLines<T, Dim> intersect_lines;
    };

    template <typename T, int Dim>
    class BuildQuadrature<Polygon<T, Dim>, Polygon<T, Dim> > {
    public:
        using Polygon = moonolith::Polygon<T, Dim>;
        using Quadrature2 = moonolith::Quadrature<T, 2>;
        using Quadrature = moonolith::Quadrature<T, Dim>;

        bool apply(const Quadrature2 &ref, const Polygon &poly1, const Polygon &poly2, Quadrature &out) {
            const bool is_convex = poly1.convex && poly2.convex;

            if (is_convex) {
                if (!intersect_convex_polygons.apply(poly1, poly2, isect_convex)) {
                    return false;
                }

                map(ref, static_cast<T>(1.), isect_convex, out);

            } else {
                if (!intersect_polygons.apply(poly1, poly2, isect)) {
                    return false;
                }

                map(ref, static_cast<T>(1.), isect, out);
            }

            return !out.empty();
        }

    private:
        // convex
        Polygon isect_convex;
        IntersectConvexPolygons<T, Dim> intersect_convex_polygons;

        // non-convex
        IntersectPolygons<T, Dim> intersect_polygons;
        Storage<Polygon> isect;
    };

    template <typename T>
    class BuildQuadrature<Line<T, 2>, Polygon<T, 2> > {
    public:
        using Line2 = moonolith::Line<T, 2>;
        using Polygon2 = moonolith::Polygon<T, 2>;
        using Quadrature1 = moonolith::Quadrature<T, 1>;
        using Quadrature2 = moonolith::Quadrature<T, 2>;

        bool apply(const Quadrature1 &ref, const Line2 &line, const Polygon2 &poly, Quadrature2 &out) {
            if (!intersect.apply(line, poly, isect)) {
                return false;
            }

            map(ref, static_cast<T>(1.), isect, out);
            return true;
        }

    private:
        Line2 isect;
        IntersectPolylineWithConvexPolygon2<T> intersect;
    };

    template <typename T>
    class BuildQuadrature<Polygon<T, 2>, Line<T, 2> > {
    public:
        using Line2 = moonolith::Line<T, 2>;
        using Polygon2 = moonolith::Polygon<T, 2>;
        using Quadrature1 = moonolith::Quadrature<T, 1>;
        using Quadrature2 = moonolith::Quadrature<T, 2>;

        bool apply(const Quadrature1 &ref, const Polygon2 &poly, const Line2 &line, Quadrature2 &out) {
            if (!intersect.apply(line, poly, isect)) {
                return false;
            }

            map(ref, static_cast<T>(1.), isect, out);
            return true;
        }

    private:
        Line2 isect;
        IntersectPolylineWithConvexPolygon2<T> intersect;
    };

    template <typename T>
    class BuildQuadrature<Polyhedron<T> > {
    public:
        using Polyhedron = moonolith::Polyhedron<T>;
        using Quadrature3 = moonolith::Quadrature<T, 3>;

        bool apply(const Quadrature3 &ref, const Polyhedron &poly1, const Polyhedron &poly2, Quadrature3 &out) {
            assert(poly1.valid());
            assert(poly2.valid());

            if (!intersect_polyhedra.apply(poly1, poly2, isect)) {
                return false;
            }

            map(ref, static_cast<T>(1.), isect, out);
            return true;
        }

    private:
        Polyhedron isect;
        IntersectConvexPolyhedra<T> intersect_polyhedra;
    };

    template <typename T>
    class BuildQuadrature<Polygon<T, 3>, Polyhedron<T> > {
    public:
        using Polyhedron = moonolith::Polyhedron<T>;
        using Quadrature2 = moonolith::Quadrature<T, 2>;
        using Quadrature3 = moonolith::Quadrature<T, 3>;
        using Polygon3 = moonolith::Polygon<T, 3>;
        using HPolyhedron = moonolith::HPolytope<T, 3>;

        bool apply(const Quadrature2 &ref, const Polygon3 &poly1, const Polyhedron &poly2, Quadrature3 &out) {
            hpoly.make(poly2);
            if (!intersect.apply(poly1, hpoly, isect)) {
                return false;
            }

            map(ref, static_cast<T>(1.), isect, out);
            return true;
        }

    private:
        Polygon3 isect;
        HPolyhedron hpoly;
        IntersectPolygonWithHPolyhedron<T> intersect;
    };

    template <typename T>
    class BuildQuadrature<Polyhedron<T>, Polygon<T, 3> > {
    public:
        using Polyhedron = moonolith::Polyhedron<T>;
        using Quadrature2 = moonolith::Quadrature<T, 2>;
        using Quadrature3 = moonolith::Quadrature<T, 3>;
        using Polygon3 = moonolith::Polygon<T, 3>;
        using HPolyhedron = moonolith::HPolytope<T, 3>;

        bool apply(const Quadrature2 &ref, const Polyhedron &poly2, const Polygon3 &poly1, Quadrature3 &out) {
            hpoly.make(poly2);
            if (!intersect.apply(poly1, hpoly, isect)) {
                return false;
            }

            map(ref, static_cast<T>(1.), isect, out);
            return true;
        }

    private:
        Polygon3 isect;
        HPolyhedron hpoly;
        IntersectPolygonWithHPolyhedron<T> intersect;
    };

    template <typename T>
    class BuildQuadrature<Polyhedron4<T>, Polyhedron4<T> > {
    public:
        using Polyhedron4 = moonolith::Polyhedron4<T>;
        using Quadrature3 = moonolith::Quadrature<T, 3>;
        using Quadrature4 = moonolith::Quadrature<T, 4>;

        bool apply(const Quadrature3 &ref, const Polyhedron4 &poly1, const Polyhedron4 &poly2, Quadrature4 &out) {
            if (!intersect.apply(poly1, poly2, isect)) {
                return false;
            }

            map(ref, static_cast<T>(1.), isect, out);
            return false;
        }

    private:
        Polyhedron4 poly_m, poly_s, isect;
        IntersectConvexSurfPolyhedra4<T> intersect;
    };

}  // namespace moonolith

#endif  // MOONOLITH_BUILD_QUADRATURE_HPP
