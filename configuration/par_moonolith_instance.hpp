#ifndef PAR_MOONOLITH_INSTANCE_HPP
#define PAR_MOONOLITH_INSTANCE_HPP

#include <mpi.h>
#include <memory>

#include "par_moonolith_config.hpp"

namespace moonolith {
    class Communicator;

    class Moonolith {
    public:
        static void Init(int argc, char *argv[]);
        static void Init(int argc, char *argv[], MPI_Comm comm);
        static int Finalize();
        static void Abort();
        static const char *version();

        static std::shared_ptr<Communicator> world_communicator();
        static Moonolith &instance();
        bool verbose() const;
        void verbose(const bool val);
        Moonolith();
        ~Moonolith();

    private:
        std::shared_ptr<Communicator> world_communicator_;
        bool verbose_;
        bool owned_comm_{false};
    };
}  // namespace moonolith

#endif  // PAR_MOONOLITH_INSTANCE_HPP
