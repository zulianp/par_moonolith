#ifndef MOONOLITH_NTREE_INSERT_HPP
#define MOONOLITH_NTREE_INSERT_HPP

#include "moonolith_insert.hpp"

namespace moonolith {

	template<class Tree>
	class NTreeInsert : public Insert<Tree> {
	public:
		typedef typename Tree::Traits Traits;
		typedef typename Traits::DataType DataType;

		typedef moonolith::Node<Traits> Node;
		typedef moonolith::Branch<Traits> Branch;
		typedef moonolith::Root<Traits> Root;
		typedef moonolith::Leaf<Traits> Leaf;
		typedef moonolith::Visitor<Traits> Visitor;

		typedef moonolith::TreeMemory<Traits> TreeMemory;

		virtual bool match(const Node &node) const override
		{
			return node.bound().intersects(this->object().bound());
		}

		virtual bool must_insert(Leaf &, TreeMemory &) override {
			return true;
		}

		explicit NTreeInsert(const DataHandle &handle, const DataType &obj)
		: Insert<Tree>(handle, obj)
		{}
	};
	
}

#endif //MOONOLITH_NTREE_INSERT_HPP
