#ifndef MOONOLITH_N_TREE_WITH_TAGS_BOUND_SYNCHRONIZE_HPP
#define MOONOLITH_N_TREE_WITH_TAGS_BOUND_SYNCHRONIZE_HPP

#include "moonolith_n_tree_bound_synchronize.hpp"

namespace moonolith {

    template <class Tree>
    class NTreeWithTagsBoundSynchronize : public NTreeBoundSynchronize<Tree> {
    public:
        typedef typename Tree::Traits Traits;
        typedef typename Traits::Bound Bound;

        virtual bool has_static_serial_size() const override { return false; }

        virtual bool read_merge_for(const Integer rank, InputStream &is) override {
            CHECK_STREAM_READ_BEGIN("NTreeWithTagsBoundSynchronize::{write_local|read_merge_for}", is);

            if (!NTreeBoundSynchronize<Tree>::read_merge_for(rank, is)) {
                assert(false);
                std::cerr << "[Error] failed to read " << std::endl;
                return false;
            }

            std::vector<Integer> remote_tags;
            Integer n_tags;
            is >> n_tags;

            if (n_tags) {
                assert(n_tags > 0);
                remote_tags.resize(n_tags);
                is.read(remote_tags.begin(), remote_tags.end());
            }

            CHECK_STREAM_READ_END("NTreeWithTagsBoundSynchronize::{write_local|read_merge_for}", is);

            if (!this->intersecting(rank)) return true;

            if (predicate_) {
                this->set_intersecting(
                    rank,
                    predicate_->tags_are_related(_tags.begin(), _tags.end(), remote_tags.begin(), remote_tags.end()));
            } else {
                assert(predicate_);
                std::cerr << "[Error] No predicate provided... (not handled)" << std::endl;
            }
            return true;
        }

        virtual bool write_local(OutputStream &os) const override {
            CHECK_STREAM_WRITE_BEGIN("NTreeWithTagsBoundSynchronize::{write_local|read_merge_for}", os);

            if (!NTreeBoundSynchronize<Tree>::write_local(os)) {
                assert(false);
                std::cerr << "[Error] NTreeWithTagsBoundSynchronize::write_local Should never happen" << std::endl;
                return false;
            }

            Integer n_tags = static_cast<Integer>(_tags.size());
            os << n_tags;
            os.write(_tags.begin(), _tags.end());

            CHECK_STREAM_WRITE_END("NTreeWithTagsBoundSynchronize::{write_local|read_merge_for}", os);
            return true;
        }

        virtual void initialize(const Node<Traits> &root, const Integer nProcs) override {
            NTreeBoundSynchronize<Tree>::initialize(root, nProcs);
            _tags = root.tags();
        }

        NTreeWithTagsBoundSynchronize(const std::shared_ptr<Predicate> &predicate) : predicate_(predicate) {}

    private:
        std::vector<Integer> _tags;
        std::shared_ptr<Predicate> predicate_;
    };
}  // namespace moonolith

#endif  // MOONOLITH_N_TREE_WITH_TAGS_BOUND_SYNCHRONIZE_HPP
