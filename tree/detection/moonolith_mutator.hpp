#ifndef MOONOLITH_MUTATOR_HPP
#define MOONOLITH_MUTATOR_HPP

#include "moonolith_forward_declarations.hpp"
#include "moonolith_visitor.hpp"

#include <string>
#include <assert.h>

namespace moonolith {

	template<class Traits>
	class Mutator : public Visitor<Traits>, public Serializable {
	public:
		virtual ~Mutator() {}
		virtual bool success() const = 0;

		virtual void read(InputStream &)
		{
			assert(false && "implement me");
		}

		virtual void write(OutputStream &) const
		{
			assert(false && "implement me");
		}

		virtual void clear() {}

		virtual std::string name() const
		{
			return "Mutator";
		}
	};
	
}

#endif //MOONOLITH_MUTATOR_HPP
