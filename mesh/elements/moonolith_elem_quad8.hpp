#ifndef MOONOLITH_ELEM_QUAD_8_HPP
#define MOONOLITH_ELEM_QUAD_8_HPP

#include "moonolith_elem_quad.hpp"
#include "moonolith_elem_quad4.hpp"

namespace moonolith {

    template <typename T_, int PhysicalDim_ = 2>
    class Quad8 final : public Quad<T_, 2, PhysicalDim_> {
    public:
        static const int Order = 2;
        static const int Dim = 2;
        static const int PhysicalDim = PhysicalDim_;
        static const int NNodes = 8;

        using T = T_;

        using Vector = moonolith::Vector<T, Dim>;
        using CoVector = moonolith::Vector<T, PhysicalDim>;

        using Point = moonolith::Vector<T, Dim>;
        using CoPoint = moonolith::Vector<T, PhysicalDim>;

        using Super = moonolith::Quad<T_, 2, PhysicalDim_>;
        using Polygon = typename Super::Polygon;

    private:
        class Fun final {
        public:
            Fun() {
                // corners
                f[0] = [](const Point &p) -> T { return 2.0 * (1.0 - p.x) * (p.y - 1.0) * (p.x + p.y - 0.5); };

                f[1] = [](const Point &p) -> T { return 2.0 * p.x * (p.y - 1.0) * (p.y - p.x + 0.5); };

                f[2] = [](const Point &p) -> T { return 2.0 * p.x * p.y * (p.x + p.y - 3.0 / 2.0); };

                f[3] = [](const Point &p) -> T { return 2.0 * p.y * (p.x - 1.0) * (p.x - p.y + 0.5); };

                // edges
                f[4] = [](const Point &p) -> T { return 4.0 * (1.0 - p.y) * (p.x - p.x * p.x); };

                f[5] = [](const Point &p) -> T { return 4.0 * p.x * (p.y - p.y * p.y); };

                f[6] = [](const Point &p) -> T { return 4.0 * p.y * (p.x - p.x * p.x); };

                f[7] = [](const Point &p) -> T { return 4.0 * (1.0 - p.x) * (p.y - p.y * p.y); };
            }

            std::array<std::function<T(const Point &)>, 8> f;
        };

        class Grad final {
        public:
            Grad() {
                // corners

                // f:= 2.0 * (1.0 - x) * (y - 1.0) * (x + y - 0.5);
                f[0] = [](const Point &p, Vector &g) {
                    g.x = -3.0 + p.x * (4.0 - 4.0 * p.y) + 5 * p.y - 2.0 * p.y * p.y;
                    g.y = -3.0 - 2.0 * p.x * p.x + p.x * (5.0 - 4.0 * p.y) + 4.0 * p.y;
                };

                // f := 2.0 * x * (y - 1.0) * (y - x + 0.5);
                f[1] = [](const Point &p, Vector &g) {
                    g.x = -1.0 + p.x * (4.0 - 4.0 * p.y) - p.y + 2 * p.y * p.y;
                    g.y = p.x * (-1.0 - 2.0 * p.x + 4.0 * p.y);
                };

                // f := 2.0 * x * y * (x + y - 3.0/2.0);
                f[2] = [](const Point &p, Vector &g) {
                    g.x = 2.0 * p.x * p.y + 2.0 * p.y * (-1.5 + p.x + p.y);
                    g.y = 2.0 * p.x * (-1.5 + p.x + 2.0 * p.y);
                };

                // f:= 2.0 * y * (x - 1.0) * (x - y + 0.5);
                f[3] = [](const Point &p, Vector &g) {
                    g.x = (-1.0 + 4.0 * p.x - 2.0 * p.y) * p.y;
                    g.y = -1.0 + 2.0 * p.x * p.x + p.x * (-1.0 - 4.0 * p.y) + 4.0 * p.y;
                };

                // edges

                // f:= 4.0 * (1.0 - y) * (x - x*x)
                f[4] = [](const Point &p, Vector &g) {
                    g.x = 8.0 * (-0.5 + p.x) * (-1. + p.y);
                    g.y = 4.0 * (-1.0 + p.x) * p.x;
                };

                // f:= 4.0 * x * (y - y*y)
                f[5] = [](const Point &p, Vector &g) {
                    g.x = 4.0 * (p.y - p.y * p.y);
                    g.y = 4.0 * p.x * (1.0 - 2.0 * p.y);
                };

                // f:= 4.0 * y * (x - x*x);
                f[6] = [](const Point &p, Vector &g) {
                    g.x = 4.0 * p.y * (1.0 - 2.0 * p.x);
                    g.y = 4.0 * (p.x - p.x * p.x);
                };

                // f:= 4.0 * (1.0 - x) * (y - y*y)
                f[7] = [](const Point &p, Vector &g) {
                    g.x = 4.0 * (-1.0 + p.y) * p.y;
                    g.y = 8.0 * (-1.0 + p.x) * (-0.5 + p.y);
                };
            }

            std::array<std::function<void(const Point &, Vector &)>, 8> f;
        };

        class Hessian final {
        public:
            Hessian() {
                // corners
                // g.x = -3.0 + p.x * (4.0 - 4.0 * p.y) + 5.0 * p.y - 2.0 * p.y * p.y;
                // g.y = -3.0 + p.x * (5.0 - 4.0 * p.y) + 4.0 * p.y - 2.0 * p.x * p.x;
                f[0] = [](const Point &p, std::array<T, Dim * Dim> &H) {
                    const auto xy = -4.0 * p.x + 5.0 - 4.0 * p.y;

                    H[0] = (4.0 - 4.0 * p.y);
                    H[1] = xy;
                    H[2] = xy;
                    H[3] = p.x + 4.0;
                };

                // g.x = -1.0 + p.x * (4.0 - 4.0 * p.y) - p.y + 2 * p.y*p.y;
                // g.y = p.x * (-1.0 - 2.0 * p.x + 4.0 * p.y);
                f[1] = [](const Point &p, std::array<T, Dim * Dim> &H) {
                    const auto xy = 4.0 * (p.y - p.x) - 1;

                    H[0] = (4.0 - 4.0 * p.y);
                    H[1] = xy;
                    H[2] = xy;
                    H[3] = p.x + 4.0;
                };

                // g.x = 2.0 * x * y + 2.0 * y * (-1.5 + x + y);
                // g.y = 2.0 * x * (-1.5 + x + 2.0 * y);
                f[2] = [](const Point &p, std::array<T, Dim * Dim> &H) {
                    const auto xy = 4.0 * (p.x + p.y) - 3;

                    H[0] = 4.0 * p.y;
                    H[1] = xy;
                    H[2] = xy;
                    H[3] = 4.0 * p.x;
                };

                // g.x = (-1.0 + 4.0 * x - 2.0 * y) * y;
                // g.y = -1.0 + 2.0 * x * x + x * (-1.0 - 4.0 * y) + 4.0 * y;
                f[3] = [](const Point &p, std::array<T, Dim * Dim> &H) {
                    const auto xy = 4.0 * (p.x - p.y) - 1;

                    H[0] = 4.0 * p.y;
                    H[1] = xy;
                    H[2] = xy;
                    H[3] = 4.0 - 4.0 * p.x;
                };

                // edges
                // g.x = 8.0 * (-0.5 + x) * (-1. + y);
                // g.y = 4.0 * (-1.0 + x) * x;
                f[4] = [](const Point &p, std::array<T, Dim * Dim> &H) {
                    const auto xy = 8.0 * p.x - 4.0;

                    H[0] = 8.0 * p.y - 8.0;
                    H[1] = xy;
                    H[2] = xy;
                    H[3] = 0.0;
                };

                // g.x = 4.0 * (y - y*y);
                // g.y = 4.0 * x * (1.0 - 2.0 * y);
                f[5] = [](const Point &p, std::array<T, Dim * Dim> &H) {
                    const auto xy = 4.0 - 8 * p.y;

                    H[0] = 0.0;
                    H[1] = xy;
                    H[2] = xy;
                    H[3] = -4.0 * p.x;
                };

                // g.x = 4.0 * y * (1.0 - 2.0 * x);
                // g.y = 4.0 * (x - x*x);
                f[6] = [](const Point &p, std::array<T, Dim * Dim> &H) {
                    const auto xy = 4.0 - 8 * p.x;

                    H[0] = -4.0 * p.y;
                    H[1] = xy;
                    H[2] = xy;
                    H[3] = 0.0;
                };

                // g.x = 4.0 * (-1.0 + p.y) * p.y;
                // g.y = 8.0 * (-1.0 + p.x) * (-0.5 + p.y);
                f[7] = [](const Point &p, std::array<T, Dim * Dim> &H) {
                    const auto xy = 8.0 * p.y - 4.0;

                    H[0] = 0.0;
                    H[1] = xy;
                    H[2] = xy;
                    H[3] = 8.0 * p.x - 8.0;
                };
            }

            std::array<std::function<void(const Point &, std::array<T, Dim * Dim> &)>, 8> f;
        };

    public:
        T fun(const Integer i, const Point &p) const override { return fun_.f[i](p); }

        void grad(const Integer i, const Point &p, Vector &g) const override { grad_.f[i](p, g); }

        void hessian(const Integer i, const Point &p, std::array<T, Dim * Dim> &H) { hessian_.f[i](p, H); }

        void jacobian(const Point &p, std::array<T, PhysicalDim * Dim> &J) const override {
            if (is_affine()) {
                affine_approx_jacobian(J);
                return;
            }

            IsoParametric<Quad8>::jacobian(*this, p, J);
        }

        void affine_approx_jacobian(std::array<T, PhysicalDim * Dim> &J) const override {
            const auto &p0 = node(0);

            for (int i = 0; i < PhysicalDim; ++i) {
                const int i_offset = i * Dim;
                J[i_offset] = node(1)[i] - p0[i];
                J[i_offset + 1] = node(3)[i] - p0[i];
            }
        }

        CoPoint &node(const Integer i) override { return nodes_[i]; }

        const CoPoint &node(const Integer i) const override { return nodes_[i]; }

        void point(const Point &p, CoPoint &q) const override { IsoParametric<Quad8>::point(*this, p, q); }

        Quad8() : is_affine_(false) {}

        bool is_affine() const override { return is_affine_; }
        inline int order() const override { return Order; }

        inline void set_affine(const bool val) { is_affine_ = val; }

        inline int n_nodes() const override { return NNodes; }

        inline void make_reference() {
            Reference<Quad8>::points(nodes_);
            set_affine(true);
        }

        inline ElemType type() const override { return QUAD8; }

        inline bool is_simplex() const override { return false; }

        inline T measure() const override {
            if (is_affine()) {
                return trapezoid_area(nodes_[0], nodes_[1], nodes_[3]);
            } else {
                Quadrature<T, Dim> q;
                Gauss::Quad::get(2, q);
                return IsoParametric<Quad8>::measure(*this, q);
            }
        }

        inline T approx_measure() const override {
            if (is_affine()) {
                return trapezoid_area(nodes_[0], nodes_[1], nodes_[3]);
            } else {
                Polygon poly(false);
                make(poly);
                return poly.area();
            }
        }

        inline T reference_measure() const override { return 1.0; }

        inline void make(Polygon &poly) const override {
            if (is_affine()) {
                poly.resize(4);

                for (int i = 0; i < 4; ++i) {
                    poly[i] = node(i);
                }
            } else {
                poly.resize(8);

                for (int i = 0; i < 4; ++i) {
                    const auto i2 = i * 2;
                    poly[i2] = node(i);
                    poly[i2 + 1] = node(4 + i);
                }
            }
        }

        inline std::array<CoPoint, 8> &nodes() { return nodes_; }

        inline const std::array<CoPoint, 8> &nodes() const { return nodes_; }

    private:
        bool is_affine_;
        std::array<CoPoint, 8> nodes_;
        const Fun fun_;
        const Grad grad_;
        const Hessian hessian_;
    };

    template <typename T, int PhysicalDim>
    class Reference<Quad8<T, PhysicalDim>> {
    public:
        // using Point = typename Quad8<T, 2, PhysicalDim>::Point;
        static const int NNodes = 8;

        template <class Point, std::size_t Size>
        static void points(std::array<Point, Size> &nodes) {
            static_assert(Size >= NNodes, "size must be at least 8");

            Reference<Quad4<T, PhysicalDim>>::points(nodes);

            // p4
            nodes[4].x = 0.5;
            nodes[4].y = 0.0;

            // p5
            nodes[5].x = 1.0;
            nodes[5].y = 0.5;

            // p6
            nodes[6].x = 0.5;
            nodes[6].y = 1.0;

            // p7
            nodes[7].x = 0.0;
            nodes[7].y = 0.5;
        }

        inline constexpr static T measure() { return 1.0; }
    };

    template <typename T, int PhysicalDim>
    class GaussQRule<Quad8<T, PhysicalDim>> {
    public:
        static bool get(const Integer order, Quadrature2<T> &q) { return Gauss::Quad::get(order, q); }
    };

}  // namespace moonolith

#endif  // MOONOLITH_ELEM_QUAD_8_HPP
