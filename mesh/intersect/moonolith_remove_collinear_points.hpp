#ifndef MOONOLITH_REMOVE_COLLINEAR_POINTS_HPP
#define MOONOLITH_REMOVE_COLLINEAR_POINTS_HPP

#include "moonolith_expanding_array.hpp"
#include "moonolith_polygon.hpp"
#include "moonolith_vector.hpp"

namespace moonolith {
    template <typename T, int Dim>
    class RemoveCollinearPoints {
    public:
        using Vector = moonolith::Vector<T, Dim>;

        Integer find(const Polygon<T, Dim> &polygon, Storage<bool> &is_collinear, const T tol) {
            Integer n_vertices = polygon.size();
            Integer n_collinear = 0;

            is_collinear.resize(n_vertices);
            std::fill(is_collinear.begin(), is_collinear.end(), false);

            for (Integer i = 0; i < n_vertices; ++i) {
                Integer im1 = (i == 0 ? n_vertices : i) - 1;
                Integer ip1 = (i + 1 == n_vertices) ? 0 : i + 1;

                u = polygon[i] - polygon[im1];
                v = polygon[ip1] - polygon[i];

                u /= length(u);
                v /= length(v);

                if (approxeq(dot(u, v), static_cast<T>(1.), tol)) {
                    is_collinear[i] = true;
                    n_collinear++;
                }
            }

            return n_collinear;
        }

        void apply(Polygon<T, Dim> &polygon, const T tol) {
            Integer n_vertices = polygon.size();
            // std::size_t result_n_points = n_vertices;

            // remove.resize(n_vertices);
            // std::fill(remove.begin(), remove.end(), false);

            // for(auto i = 0; i < n_vertices; ++i) {
            //     auto im1 = (i == 0? n_vertices : i) - 1;
            //     auto ip1 = (i + 1 == n_vertices) ? 0 : i + 1;

            //     u = polygon[i] - polygon[im1];
            //     v = polygon[ip1] - polygon[i];

            //     u /= length(u);
            //     v /= length(v);

            //     if(approxeq(dot(u, v), 1., tol)) {
            //         remove[i] = true;
            //         --result_n_points;
            //     }
            // }

            Integer result_n_points = n_vertices - find(polygon, remove, tol);

            auto range_to_fill_begin = -1;
            bool must_shift = false;

            for (Integer i = 0; i < n_vertices; ++i) {
                if (!remove[i] && must_shift) {
                    assert(range_to_fill_begin >= 0);

                    const auto to = range_to_fill_begin;
                    const auto from = i;

                    polygon[to] = polygon[from];

                    remove[i] = true;

                    if (!remove[++range_to_fill_begin]) {
                        range_to_fill_begin = -1;
                        must_shift = false;
                    }
                }

                if (remove[i] && !must_shift) {
                    must_shift = true;
                    range_to_fill_begin = i;
                }
            }

            polygon.resize(result_n_points);
        }

    private:
        Vector u, v;
        Storage<bool> remove;
    };

}  // namespace moonolith

#endif  // MOONOLITH_REMOVE_COLLINEAR_POINTS_HPP
