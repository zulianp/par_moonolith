#ifndef MOONOLITH_HALF_SPACE_HPP
#define MOONOLITH_HALF_SPACE_HPP

#include "moonolith_vector.hpp"
#include "moonolith_line.hpp"
#include "moonolith_plane.hpp"
#include "moonolith_polyhedron.hpp"
#include "moonolith_polygon.hpp"
#include "moonolith_geo_algebra.hpp"

#include <vector>
#include <cassert>
#include <iostream>

namespace moonolith {

	template<typename T, int Dim>
	class HalfSpace {
	public:
		using Point  = moonolith::Vector<T, Dim>;
		using Vector = moonolith::Vector<T, Dim>;

		static const short INSIDE = 1;
		static const short ON_PLANE = 2;
		static const short OUTSIDE = 0;

		//negative is inside the half space
		inline T signed_dist(const Point &q) const
		{
			return dot(n, q) - d;
		}

		inline bool on_plane(const Point &p, const T tol) const
		{
			return std::abs(signed_dist(p)) < tol;
		}

		inline bool inside(const Point &p, const T tol) const
		{
			return signed_dist(p) < tol;
		}

		inline bool intersect_plane(
			const Point &ray_origin,
			const Vector &ray_dir,
			Vector &intersection,
			const T tol) const
		{
			return intersect_plane(ray_origin, ray_dir, signed_dist(ray_origin), intersection, tol);
		}

		inline bool intersect_plane(
			const Point &ray_origin,
			const Vector &ray_dir,
			const T &distance,
			Vector &intersection,
			const T tol) const
		{
			const T cos_angle = -(dot(ray_dir, n));

			if(fabs(cos_angle) < tol) {
				//coplanar
				return false;
			} 

			const T delta = distance/cos_angle;
			
			intersection = ray_dir;
			intersection *= delta;
			intersection += ray_origin;
			return true;
		}

		void describe(std::ostream &os) const
		{
			for(int i = 0; i < Dim; ++i) {
				os << n[i] << " ";
			}

			os << " | " << d << "\n";
		}


		Vector n;
		T d;
	};

}

#endif //MOONOLITH_HALF_SPACE_HPP
