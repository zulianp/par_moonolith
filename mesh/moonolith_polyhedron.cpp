#include "moonolith_polyhedron_impl.hpp"

namespace moonolith {
    template Real tetrahedron_volume<Real>(const Vector<Real, 3> p1,
                                           const Vector<Real, 3> p2,
                                           const Vector<Real, 3> p3,
                                           const Vector<Real, 3> p4);
}