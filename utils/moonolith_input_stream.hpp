
#ifndef MOONOLITH_INPUT_STREAM_HPP
#define MOONOLITH_INPUT_STREAM_HPP

#include "moonolith_assert.hpp"
#include "moonolith_serializable.hpp"
#include "moonolith_stream.hpp"
#include "par_moonolith_config.hpp"

#include <assert.h>
#include <map>
#include <vector>

namespace moonolith {

    class InputStream : public Stream {
    public:
        virtual ~InputStream() {}

        template <typename T>
        friend InputStream &operator<<(T &obj, InputStream &is) {
            is >> obj;
            return is;
        }

        template <typename T>
        InputStream &operator>>(T &obj) {
            (*this) & obj;
            return *this;
        }

        template <typename T>
        InputStream &read(T *array, const Integer size) {
            this->apply(array, size);
            return *this;
        }

        InputStream &read(Serializable *array, const Integer size) {
            assert(array);
            for (Integer i = 0; i < size; ++i) {
                assert(good());
                array[i].read(*this);
            }

            return *this;
        }

        template <typename T>
        inline const T next() {
            using std::memcpy;
            T obj;
            (*this) & obj;
            return obj;
        }

        virtual bool has_next() const = 0;
        virtual void clear() = 0;
        virtual bool good() = 0;
        virtual Integer offset() const = 0;
        virtual Integer allocated_size() const = 0;
        // virtual byte * pointer() = 0;
        virtual void reserve(const Integer nBytes) = 0;

        template <typename Iterator>
        InputStream &read(const Iterator &begin, const Iterator &end) {
            if (begin == end) return *this;

            for (Iterator it = begin; it != end; ++it) {
                assert(good());
                (*this) >> *it;
            }

            return *this;
        }

        template <typename Key, typename Value>
        InputStream &read(std::map<Key, Value> &map) {
            Integer size;
            (*this) >> size;

            typename std::map<Key, Value>::iterator it = map.begin();
            for (Integer i = 0; i < size; ++i) {
                std::pair<Key, Value> temp;
                assert(good());
                (*this) >> temp;
                it = map.insert(it, temp);
            }

            return *this;
        }
    };

    class ByteInputStream : public InputStream {
    public:
        typedef std::vector<byte> StorageType;

    private:
        Integer _offset;
        StorageType *storage_;

    public:
        explicit ByteInputStream(StorageType *storage = NULL) : _offset(0), storage_(storage) {}

        inline void set_storage(StorageType *storage) { storage_ = storage; }

        inline StorageType &storage() {
            assert(storage_);
            return *storage_;
        }
        inline const StorageType &storage() const {
            assert(storage_);
            return *storage_;
        }

        inline ByteInputStream &operator&(Serializable &obj) {
            obj.read(*this);
            return *this;
        }
        inline ByteInputStream &operator&(int &obj) {
            apply_aux(obj);
            return *this;
        }
        inline ByteInputStream &operator&(unsigned int &obj) {
            apply_aux(obj);
            return *this;
        }
        inline ByteInputStream &operator&(short &obj) {
            apply_aux(obj);
            return *this;
        }
        inline ByteInputStream &operator&(unsigned short &obj) {
            apply_aux(obj);
            return *this;
        }
        inline ByteInputStream &operator&(long &obj) {
            apply_aux(obj);
            return *this;
        }
        inline ByteInputStream &operator&(unsigned long &obj) {
            apply_aux(obj);
            return *this;
        }

        inline ByteInputStream &operator&(unsigned long long &obj) {
            apply_aux(obj);
            return *this;
        }

        inline ByteInputStream &operator&(float &obj) {
            apply_aux(obj);
            return *this;
        }
        inline ByteInputStream &operator&(double &obj) {
            apply_aux(obj);
            return *this;
        }
        inline ByteInputStream &operator&(char &obj) {
            apply_aux(obj);
            return *this;
        }
        inline ByteInputStream &operator&(unsigned char &obj) {
            apply_aux(obj);
            return *this;
        }
        inline ByteInputStream &operator&(bool &obj) {
            apply_aux(obj);
            return *this;
        }

        inline ByteInputStream &apply(int *array, const Integer nElements) {
            apply_aux(array, nElements);
            return *this;
        }
        inline ByteInputStream &apply(unsigned int *array, const Integer nElements) {
            apply_aux(array, nElements);
            return *this;
        }
        inline ByteInputStream &apply(short *array, const Integer nElements) {
            apply_aux(array, nElements);
            return *this;
        }
        inline ByteInputStream &apply(unsigned short *array, const Integer nElements) {
            apply_aux(array, nElements);
            return *this;
        }
        inline ByteInputStream &apply(long *array, const Integer nElements) {
            apply_aux(array, nElements);
            return *this;
        }
        inline ByteInputStream &apply(unsigned long *array, const Integer nElements) {
            apply_aux(array, nElements);
            return *this;
        }

        inline ByteInputStream &apply(unsigned long long *array, const Integer nElements) {
            apply_aux(array, nElements);
            return *this;
        }

        inline ByteInputStream &apply(float *array, const Integer nElements) {
            apply_aux(array, nElements);
            return *this;
        }
        inline ByteInputStream &apply(double *array, const Integer nElements) {
            apply_aux(array, nElements);
            return *this;
        }
        inline ByteInputStream &apply(char *array, const Integer nElements) {
            apply_aux(array, nElements);
            return *this;
        }
        inline ByteInputStream &apply(unsigned char *array, const Integer nElements) {
            apply_aux(array, nElements);
            return *this;
        }
        inline ByteInputStream &apply(bool *array, const Integer nElements) {
            apply_aux(array, nElements);
            return *this;
        }

#ifdef MOONOLITH_HAVE_QUAD_PRECISION
        inline ByteInputStream &operator&(long double &obj) {
            apply_aux(obj);
            return *this;
        }
        inline ByteInputStream &apply(long double *array, const Integer nElements) {
            apply_aux(array, nElements);
            return *this;
        }
#endif  // MOONOLITH_HAVE_QUAD_PRECISION

        virtual ByteInputStream &apply(Serializable *array, const Integer nElements) {
            for (Integer i = 0; i < nElements; ++i) {
                assert(good());
                array[i].read(*this);
            }
            return *this;
        }

        template <typename T>
        ByteInputStream &peek(T &obj) {
            using std::memcpy;
            assert(good());
            assert(size_t(_offset) + sizeof(T) <= storage().size());
            memcpy(&obj, &storage()[_offset], sizeof(T));
            return *this;
        }

        template <typename T>
        ByteInputStream &peek(T *array, const Integer size) {
            using std::memcpy;

            assert(good());
            MOONOLITH_DEBUG(Integer newOffset = _offset + sizeof(T) * size);
            assert(newOffset <= static_cast<Integer>(storage().size()));

            memcpy(array, &storage()[_offset], sizeof(T) * size);
            return *this;
        }

        inline bool has_next() const { return _offset < static_cast<Integer>(storage().size()); }

        template <typename T>
        inline void dump_next() {
            assert(good());
            assert(_offset + sizeof(T) <= static_cast<Integer>(storage().size()));
            _offset += sizeof(T);
        }

        virtual void clear() { _offset = 0; }

        virtual bool good() { return _offset < static_cast<Integer>(storage().size()); }

        inline Integer offset() const { return _offset; }

        inline Integer allocated_size() const { return static_cast<Integer>(storage().size()); }

        inline byte *pointer() { return &storage()[0]; }

        inline byte *raw_ptr() { return &storage()[0]; }

        void reserve(const Integer nBytes) { storage().resize(nBytes); }

        template <typename T>
        inline const T peek() {
            using std::memcpy;
            T obj;
            peek(obj);
            return obj;
        }

        inline void reset_offset() { _offset = 0; }

    private:
        template <typename T>
        ByteInputStream apply_aux(T &obj) {
            peek(obj);
            _offset += sizeof(T);
            return *this;
        }

        template <typename T>
        ByteInputStream &apply_aux(T *array, const Integer size) {
            peek(array, size);
            _offset += sizeof(T) * size;
            return *this;
        }
    };

    class ByteInputBuffer : public ByteInputStream {
    private:
        typedef moonolith::ByteInputStream super;
        typedef super::StorageType StorageType;
        StorageType storage_;

    public:
        ByteInputBuffer();
        ByteInputBuffer(const Integer size);

        ByteInputBuffer(const ByteInputBuffer &other);
        ByteInputBuffer &operator=(const ByteInputBuffer &other);

        void resize(const Integer newSize);
        Integer size() const;
        bool empty() const { return storage_.empty(); }

        byte &operator[](const Integer index);
        const byte &operator[](const Integer index) const;
    };

}  // namespace moonolith

#endif  // MOONOLITH_INPUT_STREAM_HPP
