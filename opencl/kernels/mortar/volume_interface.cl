#ifndef VOLUME_INTERFACE_CL
#define VOLUME_INTERFACE_CL

#include "kernel_base.cl"

#ifndef CLIPP_HOST_CL


static Vector2 barycenter_2(const SizeType n_vertices, const Scalar *poly);
static Vector3 barycenter_3(const SizeType n_vertices, const Scalar *poly);

static Scalar polygon_area_2(const SizeType n_vertices, const Scalar *poly);
static Scalar trapezoid_area_2(const Vector2 p1, const Vector2 p2, const Vector2 p3);
static Scalar trapezoid_area_3(const Vector3 p1, const Vector3 p2, const Vector3 p3);
static Scalar polygon_area_3(const SizeType n_vertices, const Scalar *poly);

#endif //CLIPP_HOST_CL

#endif //VOLUME_INTERFACE_CL
