#ifndef VON_MISES_STRESS_CL
#define VON_MISES_STRESS_CL 

#include "kernel_base.cl"
#include "fe/interpolation.cl"
#include "von_mises_stress_interface.cl"

Scalar von_mises_stress(const SizeType n_dims, const Scalar * stress)
{
	switch(n_dims) {
		case 2: { return von_mises_stress_2(stress); }
		case 3: { return von_mises_stress_3(stress); }
		default : { KERNEL_ASSERT(false, "von_mises_stress: not supported for dim."); return 0.0; }
	}
	return 0.0;
}

Scalar von_mises_stress_2(const Scalar *stress)
{
	Scalar result =  0.5 * ( stress[0] - stress[3] ) * 
	( stress[0] - stress[3] ) + 
	3.0  *  stress[1] * stress[1];

	result = sqrt( fabs(result) );
	KERNEL_ASSERT(result == result, "von_mises_stress_2: result is nan");
	return result;
}

Scalar von_mises_stress_3(const Scalar *stress)
{
	Scalar result =  0.5 * ( stress[0] - stress[4] ) * 
	( stress[0] - stress[4] ) + 
	3.0  *  stress[1] * stress[1];

	result += 0.5 * (stress[8] - stress[4]) * (stress[8] - stress[4]) + 3.0  * stress[7] * stress[7];
	result += 0.5 * (stress[8] - stress[0]) * (stress[8] - stress[0]) + 3.0  * stress[6] * stress[6];

	result = sqrt( fabs(result) );

	KERNEL_ASSERT(result == result, "von_mises_stress_3: result is nan");
	return result;
}

void von_mises_stress_at_quad_index(const SizeType quad_index, const FEObject *fe, const Scalar *stress, Scalar *cumulative_result)
{
	const Scalar value = von_mises_stress(fe->n_dims, stress);
	integrate_value_at_quad_index_and_store_weight(quad_index, fe, value, cumulative_result);
}

#endif //VON_MISES_STRESS_CL

