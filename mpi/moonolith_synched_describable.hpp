#ifndef MOONOLITH_SYNCHED_DESCRIBABLE_HPP
#define MOONOLITH_SYNCHED_DESCRIBABLE_HPP

#include "moonolith_communicator.hpp"
#include "moonolith_describable.hpp"

#include <iostream>

namespace moonolith {

    class SynchedDescribable : public Describable {
    public:
        ~SynchedDescribable() {}

        virtual void synch_describe(Communicator &comm, std::ostream &os) const {
            for (Integer i = 0; i < comm.size(); ++i) {
                comm.barrier();
                if (i == comm.rank()) {
                    os << "Comm: " << comm << "\n";
                    os << "-------------------------------------------------------\n";
                    this->describe(os);
                    os << "-------------------------------------------------------\n";
                }
            }

            comm.barrier();
        }
    };

    template <class Object>
    void synch_describe(const Object &obj, Communicator &comm, std::ostream &os) {
        for (Integer i = 0; i < comm.size(); ++i) {
            comm.barrier();
            if (i == comm.rank()) {
                os << "Comm: " << comm << "\n";
                os << "-------------------------------------------------------\n";
                os << obj << "\n";
                os << "-------------------------------------------------------\n";
            }
        }

        comm.barrier();
    }

    template <class Object>
    void root_describe(const Object &obj, Communicator &comm, std::ostream &os) {
        comm.barrier();
        if (comm.is_root()) {
            os << obj << std::endl;
        }
        comm.barrier();
    }
}  // namespace moonolith

#endif  // MOONOLITH_SYNCHED_DESCRIBABLE_HPP
