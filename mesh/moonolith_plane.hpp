#ifndef MOONOLITH_PLANE_HPP
#define MOONOLITH_PLANE_HPP

#include "moonolith_vector.hpp"
#include "moonolith_shape.hpp"

namespace moonolith {
	
	template<typename T, int Dim>
	class Plane : public Shape<T, Dim, Dim> {
	public:
		using Point  = moonolith::Vector<T, Dim>;
		using Vector = moonolith::Vector<T, Dim>;

		inline T signed_dist(const Point &q) const
		{
			return dot(n, q - p);
		}

		bool intersect(
			const Ray<T, Dim> &ray,
			T &t) override
        {

        	auto denom = dot(ray.dir, n);
        	
        	if(denom == 0.) {
        		return false;
        	}

			auto u = p - ray.o;
			auto num = dot(u, n);

			t = num/denom;
			return true;
        } 

        bool intersect(
            const Ray<T, Dim> &ray,
            T &t,
            Vector &x_param) override
        {
        	if(!intersect(ray, t)) return false;

        	x_param = ray.dir;
        	x_param *= t;
        	x_param += ray.o;
			return true;
        }

		Vector n;
		Point p;
	};
}

#endif //MOONOLITH_PLANE_HPP
