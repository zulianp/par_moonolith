#ifndef MOONOLITH_QUADRATUE_AFFINE_LOCATOR_HPP
#define MOONOLITH_QUADRATUE_AFFINE_LOCATOR_HPP

#include "moonolith_build_quadrature.hpp"
#include "moonolith_h_polytope.hpp"
#include "moonolith_map_quadrature.hpp"

namespace moonolith {

    template <typename T, int Dim>
    class LocatePoints {
    public:
        using Quadrature = Quadrature<T, Dim>;
        using Point = moonolith::Vector<T, Dim>;
        using HPolytope = moonolith::HPolytope<T, Dim>;

        static bool apply(const Storage<Point> &p,
                          const Storage<HPolytope> &h_poly,
                          Storage<Integer> &which,
                          const T tol = GeometricTol<T>::value()) {
            auto n_p = p.size();
            auto n_poly = h_poly.size();

            which.resize(n_p);

            bool located = false;
            for (std::size_t k = 0; k < n_p; ++k) {
                const auto &pk = p[k];
                which[k] = -1;

                for (std::size_t i = 0; i < n_poly; ++i) {
                    if (h_poly[i].contains(pk, tol)) {
                        which[k] = i;
                        located = true;
                        break;
                    }
                }
            }

            return located;
        }
    };

    template <typename T, int Dim>
    class LocatePointsAndBuildQuadrature {
    public:
        using Quadrature = Quadrature<T, Dim>;
        using Point = moonolith::Vector<T, Dim>;
        using HPolytope = moonolith::HPolytope<T, Dim>;

        bool apply(const Quadrature &p,
                   const Storage<HPolytope> &h_poly,
                   Storage<Quadrature> &quadratures,
                   const T tol = GeometricTol<T>::value()) {
            if (!LocatePoints<T, Dim>::apply(p.points, h_poly, which, tol)) {
                return false;
            }
        }

    private:
        Storage<Integer> which;
        Storage<Integer> n_qp;
    };

}  // namespace moonolith

#endif  // MOONOLITH_QUADRATUE_AFFINE_LOCATOR_HPP
