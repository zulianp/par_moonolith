#ifndef MOONOLITH_ASSIGN_FUNCTIONS_HPP
#define MOONOLITH_ASSIGN_FUNCTIONS_HPP

namespace moonolith {

	template<typename Left, typename Right = Left>
	class Assign {
	public:
		inline void operator()(Left &left, const Right &right) const
		{
			left = right;
		}
	};

	template<typename Left, typename Right = Left>
	class AddAssign {
	public:
		inline void operator()(Left &left, const Right &right) const
		{
			left += right;
		}
	};

	template<typename Left, typename Right = Left>
	class SubtractAssign {
	public:
		inline void operator()(Left &left, const Right &right) const
		{
			left -= right;
		}
	};

	template<typename Left, typename Right = Left>
	class MulAssign {
	public:
		inline void operator()(Left &left, const Right &right) const
		{
			left *= right;
		}
	};


	template<typename Left, typename Right = Left>
	class DivAssign {
	public:
		inline void operator()(Left &left, const Right &right) const
		{
			left /= right;
		}
	};
}

#endif //MOONOLITH_ASSIGN_FUNCTIONS_HPP
