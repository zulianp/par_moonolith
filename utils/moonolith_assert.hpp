#ifndef MOONOLITH_ASSERT_HPP
#define MOONOLITH_ASSERT_HPP

#include <string>
#include <stdlib.h> 
#include <stdio.h>
#include <iostream>

#ifdef NDEBUG

#define MOONOLITH_ASSERT(_Expr) ((void)0) /*_Expr*/
#define MOONOLITH_ASSERT_MSG(_Expr, _Msg) ((void)0)

#define MOONOLITH_ASSERT_EQUAL(_Left, _Right) ((void)0) /*_Expr*/
#define MOONOLITH_ASSERT_EQUAL_MSG(_Left, _Right, _Msg) ((void)0)

#define MOONOLITH_PRECONDITION(_Expr) ((void)0)/*_Expr*/
#define MOONOLITH_POSTCONDITION(_Expr) ((void)0)/*_Expr*/
#define MOONOLITH_DEBUG(_Expr)

#else //NDEBUG


#ifdef USE_EXCEPTIONS

#define MOONOLITH_ASSERT(expr)														\
	if(!(expr)) {																\
	throw std::exception(std::string("Failed Assertion: ") + #expr);			\
	}			
#define MOONOLITH_ASSERT_EQUAL(_Left, _Right) MOONOLITH_ASSERT(_Left != _Right) //FIXME
#define MOONOLITH_ASSERT_EQUAL_MSG(_Left, _Right, _Msg) MOONOLITH_ASSERT(_Left != _Right) //FIXME
#define MOONOLITH_ASSERT_MSG(_Expr, _Msg) MOONOLITH_ASSERT(_Expr) //FIXME
#else //USE_EXCEPTIONS


#define MOONOLITH_ASSERT(_Expr) assert(_Expr)
#define MOONOLITH_ASSERT_MSG(_Expr, _Msg) moonolith::Assert::WithMessage(_Expr, __FILE__, __LINE__, MOONOLITH_STRINGIFY_MACRO(_Expr), _Msg);
#define MOONOLITH_STRINGIFY_MACRO(_Expr) #_Expr
#define MOONOLITH_ASSERT_EQUAL(_Left, _Right) moonolith::Assert::Equal(_Left, _Right, __FILE__, __LINE__, MOONOLITH_STRINGIFY_MACRO(_Left == _Right))
#define MOONOLITH_ASSERT_EQUAL_MSG(_Left, _Right, _Msg) moonolith::Assert::EqualWithMessage(_Left, _Right, __FILE__, __LINE__, MOONOLITH_STRINGIFY_MACRO(_Left == _Right), _Msg)
#endif //USE_EXCEPTIONS


#define MOONOLITH_DEBUG(_Expr) _Expr
//TODO better 
#define MOONOLITH_PRECONDITION(_Expr) assert(_Expr)
#define MOONOLITH_POSTCONDITION(_Expr) assert(_Expr)

#endif //NDEBUG

namespace moonolith {
	class Assert {
	public:

		inline static void WithMessage(const bool value, const char * file, const int line, const char * expr, const std::string &message)
		{
			if(!value) {
				std::cerr << "\nAssertion failed\nAt " << file << ":" << line << ".\nExpression " << expr << std::endl;
				std::cerr << "Message: " << message << std::endl;
				std::cerr << std::flush;
				abort(); 
			}
		}

		template<typename Left, typename Right>
		inline static void Equal(Left left, Right right, const char * file, const int line, const char * expr)
		{
			if(left != right) {
				std::cerr << "\nEquality assertion failed: " << left << " != " << right << ".\nAt " << file << ":" << line << ".\nExpression " << expr << std::endl;
				std::cerr << std::flush;
				abort(); 
			}
		}

		template<typename Left, typename Right>
		inline static void EqualWithMessage(Left left, Right right, const char * file, const int line, const char * expr, const std::string &message)
		{
			if(left != right) {
				std::cerr << "\nEquality assertion failed: " << left << " != " << right << ".\nAt " << file << ":" << line << ".\nExpression " << expr << std::endl;
				std::cerr << "Message: " << message << std::endl;
				std::cerr << std::flush;
				abort(); 
			}
		}
	};
}

#endif //MOONOLITH_ASSERT_HPP
