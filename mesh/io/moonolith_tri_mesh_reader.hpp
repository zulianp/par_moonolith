#ifndef MOONOLITH_TRI_MESH_READER_HPP
#define MOONOLITH_TRI_MESH_READER_HPP

#include <array>
#include <cassert>
#include <cstdio>
#include <fstream>
#include <list>
#include <string>

#include "par_moonolith_config.hpp"

#include "moonolith_elem_type.hpp"

namespace moonolith {

    template <class Mesh>
    class TriMeshReader {
    public:
        using Point = typename Mesh::Point;
        using T = typename Mesh::T;
        using Triangle = std::array<int, 3>;

        bool read(const std::string &path, Mesh &mesh) {
            size_t index = path.find_last_of(".");
            if (index >= path.size()) {
                assert(false);
                return false;
            }

            std::string vert_path = path.substr(0, index) + ".vert";

            std::list<Point> points;
            std::list<Triangle> triangles;
            std::string line;
            Point p;
            Triangle t;

            // read points
            {
                std::ifstream is(vert_path.c_str());
                if (!is.good()) {
                    is.close();
                    return false;
                }

                while (is.good()) {
                    std::getline(is, line);

                    if (line.empty()) continue;

                    // int n = 0;
                    // FIXME use same as compute precision
                    double p_buff[3] = {T(0), T(0), T(0)};
                    std::sscanf(line.c_str(), "%lg %lg %lg", &p_buff[0], &p_buff[1], &p_buff[2]);

                    for (int d = 0; d < p.size(); ++d) {
                        p[d] = p_buff[d];
                    }

                    points.push_back(p);
                }

                is.close();
            }

            // read triangles
            {
                std::ifstream is(path.c_str());
                if (!is.good()) {
                    is.close();
                    return false;
                }

                while (is.good()) {
                    std::getline(is, line);

                    int n = std::sscanf(line.c_str(), "%d %d %d", &t[0], &t[1], &t[2]);
                    if (n != 3) continue;

                    triangles.push_back(t);
                }

                is.close();
            }

            const Integer n_elements = triangles.size();
            const Integer n_points = points.size();

            mesh.resize(n_elements, n_points);

            {
                Integer i = 0;

                for (const auto &p : points) {
                    mesh.point(i++) = p;
                }
            }

            {
                Integer i = 0;
                for (const auto &t : triangles) {
                    auto &e = mesh.elem(i);

                    e.type = TRI3;
                    e.block = 1;
                    e.global_idx = i;
                    e.nodes.resize(3);

                    // from index base 1 to 0
                    e.nodes[0] = t[0] - 1;
                    e.nodes[1] = t[1] - 1;
                    e.nodes[2] = t[2] - 1;

                    e.is_affine = true;
                    ++i;
                }
            }

            return true;
        }

        virtual ~TriMeshReader() = default;
    };

}  // namespace moonolith

#endif  // MOONOLITH_TRI_MESH_READER_HPP
