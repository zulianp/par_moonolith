#ifndef OPENCL_ADAPTER_HPP
#define OPENCL_ADAPTER_HPP

#include "par_moonolith_config.hpp"

#include "moonolith_vector.hpp"

#include <assert.h>
#include <cmath>

#include <algorithm>
#include <string>

#ifndef m_kernel__
#define m_kernel__
#define m_global__
#define m_local__
#define m_constant__
#endif

#define CLIPP_HOST_CL

namespace moonolith {

    class OpenCLAdapter {
    public:
        typedef Vector<double, 2> double2;
        typedef Vector<double, 3> double3;

        typedef Vector<float, 2> float2;
        typedef Vector<float, 3> float3;

        typedef Real Scalar;
        typedef Vector<Scalar, 2> Vector2;
        typedef Vector<Scalar, 3> Vector3;

        typedef int SizeType;

        int global_size[3];

        OpenCLAdapter();

        int get_global_id(const int i) const;
        int get_global_size(const int i) const;
        int get_group_id(const int i) const;
        int get_local_size(const int i) const;
        int get_local_id(const int i) const;

        void set_global_size(const int i, const int size);

        static const int CLK_LOCAL_MEM_FENCE = 0;
        static const int CLK_GLOBAL_MEM_FENCE = 1;

        inline static Scalar min(const Scalar x, const Scalar y) { return std::min(x, y); }

        inline static Scalar max(const Scalar x, const Scalar y) { return std::max(x, y); }

        inline static Vector3 vec_3(const Scalar x, const Scalar y, const Scalar z) { return Vector3(x, y, z); }

        inline static Vector2 vec_2(const Scalar x, const Scalar y) { return Vector2(x, y); }

        inline static int sign(const Scalar x) { return (x < 0) ? -1 : (x > 0 ? 1 : 0); }

        inline static void barrier(const int barrier_type) { (void)barrier_type; }

        static void assertion_failure(const char *assertion, const char *file, unsigned line, const char *function) {
            fprintf(stderr,
                    "%s:%u: %s%sAssertion `%s' failed.\n",
                    file,
                    line,
                    function ? function : "",
                    function ? ": " : "",
                    assertion);

            abort();
        }
    };
}  // namespace moonolith

#endif  // OPENCL_ADAPTER_HPP
