#ifndef MOONOLITH_K_DOP_HPP
#define MOONOLITH_K_DOP_HPP

#include "moonolith_describable.hpp"
#include "moonolith_serializable.hpp"
#include "moonolith_stream.hpp"
#include "moonolith_vector.hpp"

#include <functional>
#include <limits>
#include <vector>

namespace moonolith {

    template <int Dimensions, int Directions, typename T, class Projector>
    class KDOPBase : public Serializable, public Describable {
    public:
        // void apply_read_write(Stream &stream) override
        // {
        // 	for(int i = 0; i < Directions; ++i) {
        // 		stream & (min_[i]);
        // 		stream & (max_[i]);
        // 	}
        // }

        void read(InputStream &is) override {
            for (int i = 0; i < Directions; ++i) {
                is >> min_[i];
                is >> max_[i];
            }
        }

        void write(OutputStream &os) const override {
            for (int i = 0; i < Directions; ++i) {
                os << min_[i];
                os << max_[i];
            }
        }

        inline constexpr static int n_dims() { return Dimensions; }

        inline KDOPBase &operator+=(const std::vector<T> &point) {
            std::array<T, Dimensions> a;
            for (std::size_t i = 0; i < Dimensions; ++i) {
                a[i] = point[i];
            }

            return (*this) += a;
        }

        inline KDOPBase &operator+=(const Vector<T, Dimensions> &point) {
            std::array<T, Dimensions> a;
            for (int i = 0; i < Dimensions; ++i) {
                a[i] = point[i];
            }

            return (*this) += a;
        }

        inline KDOPBase &operator+=(const std::array<T, Dimensions> &point) {
            std::array<T, Directions> projections;
            Projector::project(point, projections);
            this->expand(projections);
            return *this;
        }

        inline KDOPBase &operator+=(const KDOPBase &other) {
            for (int i = 0; i < Directions; ++i) {
                min_[i] = std::min(other.min_[i], min_[i]);
                max_[i] = std::max(other.max_[i], max_[i]);
            }

            return *this;
        }

        void expand(const std::array<T, Directions> &projections) {
            for (int i = 0; i < Directions; ++i) {
                min_[i] = std::min(projections[i], min_[i]);
                max_[i] = std::max(projections[i], max_[i]);
            }
        }

        inline bool intersects(const KDOPBase &other, const T tol = 0) const {
            for (int i = 0; i < Directions; ++i) {
                if (min_[i] > (other.max_[i] + tol) || other.min_[i] > (max_[i] + tol)) {
                    return false;
                }
            }

            return true;
        }

        void describe(std::ostream &os) const override {
            for (int i = 0; i < Directions; ++i) {
                os << min_[i] << ", " << max_[i] << "\n";
            }
        }

        virtual ~KDOPBase() {}

        inline void clear() {
            for (int i = 0; i < Directions; ++i) {
                min_[i] = std::numeric_limits<T>::max();
                max_[i] = -std::numeric_limits<T>::max();
            }
        }

        inline bool empty() const { return min_[0] > max_[0]; }

        KDOPBase() { clear(); }

    private:
        std::array<T, Directions> min_, max_;
    };

    template <typename T>
    struct kdop_projector_2_2 {
        inline static void project(const std::array<T, 2> &point, std::array<T, 2> &projections) {
            projections[0] = point[0] + point[1];
            projections[1] = -point[0] + point[1];
        }
    };

    template <typename T>
    struct kdop_projector_2_4 {
        inline static void project(const std::array<T, 2> &point, std::array<T, 4> &projections) {
            projections[0] = point[0];
            projections[1] = point[1];
            projections[2] = point[0] + point[1];
            projections[3] = -point[0] + point[1];
        }
    };

    template <typename T>
    struct kdop_projector_3_4 {
        inline static void project(const std::array<T, 3> &point, std::array<T, 4> &projections) {
            projections[0] = point[0] + point[1] + point[2];
            projections[1] = point[0] + point[1] - point[2];
            projections[2] = point[0] - point[1] + point[2];
            projections[4] = -point[0] + point[1] + point[2];
        }
    };

    template <typename T>
    struct kdop_projector_3_9 {
        inline static void project(const std::array<T, 3> &point, std::array<T, 9> &projections) {
            projections[0] = point[0];
            projections[1] = point[1];
            projections[2] = point[2];
            projections[3] = point[0] + point[1];
            projections[4] = point[0] + point[2];
            projections[5] = point[1] + point[2];
            projections[6] = point[0] - point[1];
            projections[7] = point[0] - point[2];
            projections[8] = point[1] - point[2];
        }
    };

    template <typename T>
    struct kdop_projector_3_13 {
        inline static void project(const std::array<T, 3> &point, std::array<T, 13> &projections) {
            projections[0] = point[0];
            projections[1] = point[1];
            projections[2] = point[2];
            projections[3] = point[0] + point[1] + point[2];
            projections[4] = point[0] - point[1] + point[2];
            projections[5] = -point[0] + point[1] + point[2];
            projections[6] = point[0] - point[1] - point[2];
            projections[7] = point[0] + point[1];
            projections[8] = point[0] + point[2];
            projections[9] = point[1] + point[2];
            projections[10] = point[0] - point[2];
            projections[11] = point[0] + point[1] + point[2];
            projections[12] = point[1] - point[2];
        }
    };

    template <int Dimensions, int Directions, typename T>
    class KDOP {};

    template <typename T>
    class KDOP<2, 2, T> : public KDOPBase<2, 2, T, kdop_projector_2_2<T> > {};

    template <typename T>
    class KDOP<2, 4, T> : public KDOPBase<2, 4, T, kdop_projector_2_4<T> > {};

    template <typename T>
    class KDOP<3, 4, T> : public KDOPBase<3, 4, T, kdop_projector_3_4<T> > {};

    template <typename T>
    class KDOP<3, 9, T> : public KDOPBase<3, 9, T, kdop_projector_3_9<T> > {};

    template <typename T>
    class KDOP<3, 13, T> : public KDOPBase<3, 13, T, kdop_projector_3_13<T> > {};

    typedef moonolith::KDOP<2, 2, double> KDOP2x2d;
    typedef moonolith::KDOP<2, 4, double> KDOP2x4d;
    typedef moonolith::KDOP<3, 4, double> KDOP3x4d;
    typedef moonolith::KDOP<3, 9, double> KDOP3x9d;
    typedef moonolith::KDOP<3, 13, double> KDOP3x13d;
}  // namespace moonolith

#endif  // MOONOLITH_K_DOP_HPP
