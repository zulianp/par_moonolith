#ifndef FE_FUNCTION_INTERFACE_CL
#define FE_FUNCTION_INTERFACE_CL 

#include "kernel_base.cl"
#include "algebra/algebra_interfaces.cl"
#include "fe_object.cl"

typedef struct {
	Scalar fun [MAX_N_DIMS];
	Scalar grad[MAX_N_DIMS * MAX_N_DIMS];
} FEFunction;

#ifndef CLIPP_HOST_CL

void interpolate_dxd_at_quad_point(const SizeType quad_index, 
								   const Matrixdxd *grads_at_quad_index,
								   const FEObject *fe, 
								   const Scalar * coefficients, 
								   FEFunction *fe_fun
								   );

void interpolate_2x2_at_quad_point(const SizeType quad_index, 
								   const Matrixdxd *grads_at_quad_index,
								   const FEObject *fe, 
								   const Scalar * coefficients, 
								   FEFunction *fe_fun
								   );

void interpolate_3x3_at_quad_point(const SizeType quad_index, 
								   const Matrixdxd *grads_at_quad_index,
								   const FEObject *fe, 
								   const Scalar * coefficients, 
								   FEFunction *fe_fun
								   );

void fe_function_print(const SizeType n_dims, const SizeType n_codims, FEFunction *fe_fun);

#endif //CLIPP_HOST_CL
#endif //FE_FUNCTION_INTERFACE_CL

