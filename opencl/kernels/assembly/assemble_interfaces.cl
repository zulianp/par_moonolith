

#ifndef ASSEMBLE_INTERFACES_CL
#define ASSEMBLE_INTERFACES_CL 

#include "kernel_base.cl"
#include "fe/fe_interfaces.cl"
#include "quadrature_interface.cl"

#ifndef CLIPP_HOST_CL

SizeType block_get_i(const SizeType block_i, const SizeType test_n_dims, const SizeType block_ik);
SizeType block_get_j(const SizeType block_j, const SizeType trial_n_dims, const SizeType block_jl);


void block_global_to_local(const SizeType i, const SizeType n_codims, const FEObject * fe, 
						   const m_global__ SizeType * el_ptr, 
						   const m_global__ SizeType * el_index, 
						   const m_global__ Scalar * displacement, 
						   Scalar * local_displacement);

void mass_matrix_assemble_local(FEObject *trial, FEObject *test, Scalar * matrix);
void laplacian_assemble_local(FEObject *trial, FEObject*test, Scalar * matrix);
void linear_elasticity_assemble_local(const Scalar mu, const Scalar lambda,  FEObject *trial, FEObject*test, Scalar * matrix);
void block_mass_matrix_assemble_local(FEObject *trial, FEObject*test, Scalar * matrix);

#endif //CLIPP_HOST_CL

#endif //ASSEMBLE_INTERFACES_CL


