#ifndef TRANSFORM_CL
#define TRANSFORM_CL 

#include "kernel_base.cl"
#include "fe_object.cl"

#include "fe_interfaces_2.cl"
#include "fe_interfaces_3.cl"


void line_transform_2(const Scalar *line, const SizeType n_quad_points, const Scalar *quad_points, Scalar * global_points)
{
	const Vector2 p1 = vec_2(line[0], line[1]);
	const Vector2 p2 = vec_2(line[2], line[3]);
	
	for(SizeType i = 0; i < n_quad_points; ++i) {
		const SizeType i2 = i * 2;
		const Scalar w = quad_points[i];
		const Vector2 gp = p1 * (1 - w) + p2 * w;

		global_points[i2] 	  = gp.x;
		global_points[i2 + 1] = gp.y;
	}
}

void iso_parametric_transform_2(const FEObject *fe, const Scalar * points, Scalar * global_quad_points)
{
	const Vector2 p0 = vec_2(points[0], points[1]);

	for(SizeType q = 0; q < fe->n_quad_points; ++q) {	
		const SizeType q2 = q*2;
		const SizeType q2p1 = q2+1;

		Vector2 global_point = fe->fun[0][q] * p0;

		for(SizeType i = 1; i < fe->n_shape_functions; ++i) {
			const SizeType i2 = i*2;
			const Vector2 p = vec_2(points[i2], points[i2+1]);
			global_point += fe->fun[i][q] * p;
		}
		
		global_quad_points[q2] 	 = global_point[0];
		global_quad_points[q2p1] = global_point[1];
	}
}

void iso_parametric_jacobian_2(const FEObject *fe, const Scalar * points, Scalar * jacobians)
{
	Scalar outer_product_buff[2*2];

	const SizeType n_quad_points_x_d = fe->n_quad_points*4;
	set(n_quad_points_x_d, 0, jacobians);
	
	for(SizeType q = 0; q < fe->n_quad_points; ++q) {
		const SizeType q_grad = q * 2;
		const SizeType q_jac = q * 4;

		for(SizeType i = 0; i < fe->n_shape_functions; ++i) {
			outer_product(2, 2, &points[i*2], &fe->grad[i][q_grad], outer_product_buff);	
			axpy(4, 1.0, outer_product_buff, &jacobians[q_jac]);
		}
	}
}

static void make_inverse_affine_transform_2(const Scalar *A, const Scalar *b, Scalar *Ainv, Scalar *binv)
{
	const Scalar det_A = det_2(A);
	inverse_2(A, det_A, Ainv);
	mat_vec_mul(2, 2, Ainv, b, binv);
	vec_scale(2, -1, binv);
}

static void apply_affine_transform_2(const Scalar *A, const Scalar *b, const SizeType n_points, const Scalar *in, Scalar *out)
{
	for(SizeType i = 0; i < n_points; ++i) {
		const SizeType i2 = i * 2;
		mat_vec_mul(2, 2, A, &in[i2], &out[i2]);
		axpy(2, 1.0, b, &out[i2]);
	}
}

void iso_parametric_transform_3(const FEObject *fe, const Scalar * points, Scalar * global_quad_points)
{
	const Vector3 p0 = vec_3(points[0], points[1], points[2]);

	for(SizeType q = 0; q < fe->n_quad_points; ++q) {	
		const SizeType q3   = q*3;
		const SizeType q3p1 = q3 + 1;
		const SizeType q3p2 = q3 + 2;

		Vector3 global_point = fe->fun[0][q] * p0;

		for(SizeType i = 1; i < fe->n_shape_functions; ++i) {
			const SizeType i3 = i*3;
			const Vector3 p = vec_3(points[i3], points[i3+1], points[i3+2]);
			global_point += fe->fun[i][q] * p;
		}
		
		global_quad_points[q3] 	 = global_point[0];
		global_quad_points[q3p1] = global_point[1];
		global_quad_points[q3p2] = global_point[2];
	}
}

void iso_parametric_jacobian_3(const FEObject *fe, const Scalar * points, Scalar * jacobians)
{
	Scalar outer_product_buff[3*3];

	const SizeType n_quad_points_x_d = fe->n_quad_points*9;
	set(n_quad_points_x_d, 0, jacobians);
	
	for(SizeType q = 0; q < fe->n_quad_points; ++q) {
		const SizeType q_grad = q * 3;
		const SizeType q_jac  = q * 9;

		for(SizeType i = 0; i < fe->n_shape_functions; ++i) {
			outer_product(3, 3, &points[i*3], &fe->grad[i][q_grad], outer_product_buff);
			axpy(9, 1.0, outer_product_buff, &jacobians[q_jac]);
		}
	}
}

static void make_inverse_affine_transform_3(const Scalar *A, const Scalar *b, Scalar *Ainv, Scalar *binv)
{
	const Scalar det_A = det_3(A);
	inverse_3(A, det_A, Ainv);
	mat_vec_mul(3, 3, Ainv, b, binv);
	vec_scale(3, -1, binv);
}

static void apply_affine_transform_3(const Scalar *A, const Scalar *b, const SizeType n_points, const Scalar *in, Scalar *out)
{
	for(SizeType i = 0; i < n_points; ++i) {
		const SizeType i3 = i * 3;
		mat_vec_mul(3, 3, A, &in[i3], &out[i3]);
		axpy(3, 1.0, b, &out[i3]);
	}
}

#endif //TRANSFORM_CL
