#ifndef MOONOLITH_POLYHEDRON_HPP
#define MOONOLITH_POLYHEDRON_HPP

#include "moonolith_expanding_array.hpp"
#include "moonolith_matlab_scripter.hpp"
#include "moonolith_vector.hpp"
#include "par_moonolith_config.hpp"

#include <fstream>
#include <ostream>
#include <vector>

namespace moonolith {

    template <typename T>
    T tetrahedron_volume(const Vector<T, 3> p1, const Vector<T, 3> p2, const Vector<T, 3> p3, const Vector<T, 3> p4);

    template <typename T, int Dim>
    class PMesh {
    public:
        using Vector = moonolith::Vector<T, Dim>;
        using Point = moonolith::Vector<T, Dim>;

        // types
        const static int UNSTRUCTURED = 0;
        const static int AABB = 1;
        const static int TET = 2;
        const static int HEX = 3;
        const static int SURF = 4;
        const static int TRIANGLE = 5;
        const static int QUAD = 6;
        const static int OCTAHEDRON = 7;
        const static int PRISM = 8;

        Storage<Integer> el_ptr;
        Storage<Integer> el_index;
        Storage<Point> points;
        short type;

        inline void clear() {
            el_ptr.resize(1);
            el_ptr[0] = 0;
            el_index.clear();
            points.clear();
            type = UNSTRUCTURED;
        }

        inline void scale(const T &factor) {
            for (auto &p : points) {
                p *= factor;
            }
        }

        inline bool empty() const { return points.empty(); }

        inline std::size_t n_nodes() const { return points.size(); }

        inline std::size_t n_elements() const { return el_ptr.size() - 1; }

        inline Point barycenter() const {
            Point ret;

            for (const auto &p : points) {
                ret += p;
            }

            ret /= static_cast<T>(points.size());
            return ret;
        }

        bool valid() const {
            auto n = el_ptr.size();
            for (std::size_t i = 1; i < n; ++i) {
                bool ok = el_ptr[i] > el_ptr[i - 1];
                assert(el_ptr[i] - el_ptr[i - 1] >= 3);
                assert(ok);
                if (!ok) {
                    return false;
                }
            }

            Storage<Vector> ns;
            normals(ns);

            for (const auto &n : ns) {
                auto n_norm = length(n);
                bool ok = std::abs(n_norm - 1.) < 1e-6;
                assert(ok);
                if (!ok) {
                    return false;
                }
            }

            auto b = barycenter();
            auto n_elements = this->n_elements();
            for (std::size_t i = 0; i < n_elements; ++i) {
                auto v1 = el_index[el_ptr[i]];
                auto v2 = el_index[el_ptr[i] + 1];
                auto v3 = el_index[el_ptr[i] + 2];

                const auto &p1 = points[v1];
                const auto &p2 = points[v2];
                const auto &p3 = points[v3];

                Vector n;
                normal(p1, p2, p3, n);

                auto dir = p1 - b;
                auto cos_angle = dot(dir, n);

                if (cos_angle <= 0) {
                    MatlabScripter script;
                    script.plot(*this, true);
                    script.save("bug_polyhedron.m");

                    assert(cos_angle > 0);
                    return false;
                }
            }

            auto n_nodes = this->n_nodes();
            if (n_nodes == 4) {
                assert(n_elements == 4);
                if (n_elements != 4) {
                    return false;
                }
            }

            return true;
        }

        void describe(std::ostream &os) const {
            for (auto e : el_ptr) {
                os << e << " ";
            }

            os << "\n";

            for (auto i : el_index) {
                os << i << " ";
            }

            os << "\n";
        }

        bool save_obj(const std::string &path) const {
            std::ofstream os(path.c_str());
            if (!os.good()) {
                os.close();
                return false;
            }

            write_obj(os);
            os.close();
            return true;
        }

        void write_obj(std::ostream &os) const {
            const Integer n_elems = n_elements();
            const Integer nn = n_nodes();

            os << "n_elements " << n_elems << " n_nodes " << nn << "\n";

            for (Integer i = 0; i < nn; ++i) {
                os << ("v");
                for (Integer d = 0; d < Dim; ++d) {
                    os << " " << points[i][d];
                }

                os << ("\n");
            }

            for (Integer e = 0; e < n_elems; ++e) {
                const Integer begin = el_ptr[e];
                const Integer end = el_ptr[e + 1];

                os << ("f");
                for (Integer k = begin; k < end; ++k) {
                    os << " " << (el_index[k] + 1);
                }

                os << "\n";
            }

            os << "\n";
        }

        void normals(Storage<Vector> &normals) const {
            auto bary = barycenter();  // FIXME TO REMOVE?

            auto n_elements = this->n_elements();

            normals.resize(n_elements);

            Vector u, v;
            for (std::size_t i = 0; i < n_elements; ++i) {
                const std::size_t begin = el_ptr[i];

                assert((el_ptr[i + 1] - begin) >= Dim);  //, "compute_normals: (end - begin) >= 3");

                const auto v_o = el_index[begin];
                const auto v_u = el_index[begin + 1];
                const auto v_v = el_index[begin + 2];

                const auto &o = points[v_o];
                u = points[v_u];
                v = points[v_v];

                u -= o;
                v -= o;

                auto &n = normals[i];
                n = cross(u, v);
                n = normalize(n);

                if (dot(n, u + o - bary) < 0) {
                    n = -n;
                }

                assert(fabs(length(n) - 1.0) < 1e-6);
                assert(dot(n, u + o - bary) > 0);
            }
        }

        void normal(const Point &p1, const Point &p2, const Point &p3, Vector &n) const {
            n = cross(p2 - p1, p3 - p1);
            n /= length(n);
        }

        void fix_ordering() {
            Storage<Vector> normals;
            fix_ordering_and_get_normals(normals);
        }

        void fix_ordering_and_get_normals(Storage<Vector> &normals) {
            auto bary = barycenter();  // FIXME TO REMOVE?

            auto n_elements = this->n_elements();

            normals.resize(n_elements);

            Vector u, v;
            for (std::size_t i = 0; i < n_elements; ++i) {
                const std::size_t begin = el_ptr[i];

                assert((el_ptr[i + 1] - begin) >= Dim);  //, "compute_normals: (end - begin) >= 3");

                const auto v_o = el_index[begin];
                const auto v_u = el_index[begin + 1];
                const auto v_v = el_index[begin + 2];

                const auto &o = points[v_o];
                u = points[v_u];
                v = points[v_v];

                u -= o;
                v -= o;

                auto &n = normals[i];
                n = cross(u, v);
                n = normalize(n);

                if (dot(n, u + o - bary) < 0) {
                    n = -n;

                    if ((el_ptr[i + 1] - begin) == 4) {
                        // NEW (If something brakes is because of this 2 lines!)
                        std::swap(el_index[begin], el_index[begin + 3]);
                        std::swap(el_index[begin+1], el_index[begin + 2]);
                    } else {
                        std::swap(el_index[begin + 1], el_index[begin + 2]);
                    }
                }

                assert(fabs(length(n) - 1.0) < 1e-6);
                assert(dot(n, u + o - bary) > 0);
            }
        }
    };

    template <typename T>
    using Polyhedron = PMesh<T, 3>;

    template <typename T>
    using Polyhedron4 = PMesh<T, 4>;

    template <typename T>
    void make_cube(const Vector<T, 3> &min, const Vector<T, 3> &max, Polyhedron<T> &poly) {
        poly.type = Polyhedron<T>::HEX;
        poly.el_ptr.resize(6 + 1);
        poly.el_index.resize(6 * 4);

        poly.el_ptr[0] = 0;
        poly.el_ptr[1] = 4;
        poly.el_ptr[2] = 8;
        poly.el_ptr[3] = 12;
        poly.el_ptr[4] = 16;
        poly.el_ptr[5] = 20;
        poly.el_ptr[6] = 24;

        // //n = [0, 0, -1]
        // poly.el_index[0] = 0;
        // poly.el_index[1] = 1;
        // poly.el_index[2] = 2;
        // poly.el_index[3] = 3;

        // //n = [0, -1, 0]
        // poly.el_index[4] = 0;
        // poly.el_index[5] = 1;
        // poly.el_index[6] = 5;
        // poly.el_index[7] = 4;

        // //n = [-1, 0, 0]
        // poly.el_index[8] =  0;
        // poly.el_index[9] =  4;
        // poly.el_index[10] = 7;
        // poly.el_index[11] = 3;

        // //n = [1, 0, 0]
        // poly.el_index[12] = 1;
        // poly.el_index[13] = 2;
        // poly.el_index[14] = 6;
        // poly.el_index[15] = 5;

        // //n = [0, 1, 0]
        // poly.el_index[16] = 2;
        // poly.el_index[17] = 3;
        // poly.el_index[18] = 7;
        // poly.el_index[19] = 6;

        // //n = [0, 0, 1]
        // poly.el_index[20] = 4;
        // poly.el_index[21] = 5;
        // poly.el_index[22] = 6;
        // poly.el_index[23] = 7;

        // face 0
        poly.el_index[0] = 0;
        poly.el_index[1] = 1;
        poly.el_index[2] = 5;
        poly.el_index[3] = 4;

        // face 1
        poly.el_index[4] = 1;
        poly.el_index[5] = 2;
        poly.el_index[6] = 6;
        poly.el_index[7] = 5;

        // face 2
        poly.el_index[8] = 3;
        poly.el_index[9] = 7;
        poly.el_index[10] = 6;
        poly.el_index[11] = 2;

        // face 3
        poly.el_index[12] = 0;
        poly.el_index[13] = 4;
        poly.el_index[14] = 7;
        poly.el_index[15] = 3;

        // face 4
        poly.el_index[16] = 2;
        poly.el_index[17] = 1;
        poly.el_index[18] = 0;
        poly.el_index[19] = 3;

        // face 5
        poly.el_index[20] = 6;
        poly.el_index[21] = 7;
        poly.el_index[22] = 4;
        poly.el_index[23] = 5;

        poly.points.resize(8);
        poly.points[0] = min;
        poly.points[1] = {max.x, min.y, min.z};
        poly.points[2] = {max.x, max.y, min.z};
        poly.points[3] = {min.x, max.y, min.z};
        poly.points[4] = {min.x, min.y, max.z};
        poly.points[5] = {max.x, min.y, max.z};
        poly.points[6] = max;
        poly.points[7] = {min.x, max.y, max.z};
    }

    template <typename T>
    T measure(const Polyhedron<T> &polyhedron) {
        using std::abs;
        auto n_nodes = polyhedron.n_nodes();

        if (n_nodes == 4) {
            const auto &p = polyhedron.points;
            return abs(tetrahedron_volume(p[0], p[1], p[2], p[3]));
        }

        auto b = polyhedron.barycenter();
        auto n_elements = polyhedron.n_elements();

        T result = 0.;
        for (std::size_t i = 0; i < n_elements; ++i) {
            const auto begin = polyhedron.el_ptr[i];
            const auto end = polyhedron.el_ptr[i + 1];

            const auto n_triangles = end - begin - 2;

            assert((end - begin) >= 3);

            const auto v1 = polyhedron.el_index[begin];
            const auto &p1 = polyhedron.points[v1];

            for (auto k = 1; k <= n_triangles; ++k) {
                const auto v2 = polyhedron.el_index[begin + k];
                const auto v3 = polyhedron.el_index[begin + k + 1];

                const auto &p2 = polyhedron.points[v2];
                const auto &p3 = polyhedron.points[v3];

                result += abs(tetrahedron_volume(b, p1, p2, p3));
            }
        }

        return result;
    }

    template <typename T>
    T measure(const Polyhedron4<T> &polyhedron) {
        assert(!polyhedron.empty());

        if (polyhedron.empty()) {
            return 0.0;
        }

        // AffineTransform<T, 3, 4> t;
        // make

        assert(false);
        return 1.0;
    }

    template <typename T>
    T measure(const Storage<Polyhedron<T>> &polyhedra) {
        T ret = 0.0;
        for (const auto &p : polyhedra) {
            ret += measure(p);
        }

        return ret;
    }

}  // namespace moonolith

#endif  // MOONOLITH_POLYHEDRON_HPP
