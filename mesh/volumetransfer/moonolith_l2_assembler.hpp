#ifndef MOONOLITH_L2_ASSEMBLER_HPP
#define MOONOLITH_L2_ASSEMBLER_HPP

#include "moonolith_assembly.hpp"
#include "moonolith_build_quadrature.hpp"
#include "moonolith_build_quadrature_non_convex.hpp"
#include "moonolith_elem_dual.hpp"
#include "moonolith_elem_shape.hpp"
#include "moonolith_fe.hpp"
#include "moonolith_intersect_polyhedra.hpp"
#include "moonolith_intersect_polyhedra_impl.hpp"
#include "moonolith_iso_parametric_transform.hpp"
#include "moonolith_map_quadrature.hpp"
#include "moonolith_polygon.hpp"
#include "moonolith_project_convex_polygons.hpp"
#include "moonolith_project_line_segments.hpp"
#include "moonolith_project_polygons.hpp"
#include "moonolith_project_polylines.hpp"
#include "moonolith_shape.hpp"
#include "moonolith_transform.hpp"

#include "moonolith_elem_hexahedron.hpp"
#include "moonolith_elem_tetrahedron.hpp"
#include "moonolith_transformed_fe.hpp"

#include <memory>
#include <string>

namespace moonolith {

    template <typename T, int PhysicalDim>
    class Transfer {
    public:
        virtual ~Transfer() {}
    };

    template <typename T, int PhysicalDim, int MasterDim = PhysicalDim, int SlaveDim = PhysicalDim>
    class BuildQuadratureAlgo {};

    ///@brief template method for building quadrature on physical element and mapping it to the reference element
    template <class MasterGeom,
              class SlaveGeom,
              class QReferenceMaster,
              class QReferenceSlave,
              class QPhysical,
              class MasterTransform_,
              class SlaveTransform_ = MasterTransform_>
    class BuildQuadratureBase {
    public:
        using MasterTransform = MasterTransform_;
        using SlaveTransform = SlaveTransform_;

        // INPUT
        MasterGeom master;
        SlaveGeom slave;
        QReferenceSlave q_rule;
        std::shared_ptr<MasterTransform> trafo_master;
        std::shared_ptr<SlaveTransform> trafo_slave;

        // OUTPUT
        QPhysical q_physical;
        QReferenceMaster q_master;
        QReferenceSlave q_slave;

        virtual ~BuildQuadratureBase() {}
        bool compute();

        inline void use_reference_frame(const bool use_reference_frame) { use_reference_frame_ = use_reference_frame; }

    private:
        BuildQuadrature<MasterGeom, SlaveGeom> build_q;

        MasterGeom aux_master;
        SlaveGeom aux_slave;
        bool use_reference_frame_{false};
    };

    // line domain of integration
    template <typename T, int PhysicalDim>
    class BuildQuadratureAlgo<T, PhysicalDim, 1, 1> final : public BuildQuadratureBase<Line<T, PhysicalDim>,
                                                                                       Line<T, PhysicalDim>,
                                                                                       Quadrature<T, 1>,
                                                                                       Quadrature<T, 1>,
                                                                                       Quadrature<T, PhysicalDim>,
                                                                                       Transform<T, 1, PhysicalDim>> {};

    // polygonal domain of integration
    template <typename T, int PhysicalDim>
    class BuildQuadratureAlgo<T, PhysicalDim, 2, 2> final : public BuildQuadratureBase<Polygon<T, PhysicalDim>,
                                                                                       Polygon<T, PhysicalDim>,
                                                                                       Quadrature<T, 2>,
                                                                                       Quadrature<T, 2>,
                                                                                       Quadrature<T, PhysicalDim>,
                                                                                       Transform<T, 2, PhysicalDim>> {};

    // polyhedral domain of integration
    template <typename T>
    class BuildQuadratureAlgo<T, 3, 3, 3> final : public BuildQuadratureNonAffine<T, 3, 3> {};

    // mixed manifold dimensions
    template <typename T>
    class BuildQuadratureAlgo<T, 3, 3, 2> final : public BuildQuadratureBase<Polyhedron<T>,
                                                                             Polygon<T, 3>,
                                                                             Quadrature<T, 3>,
                                                                             Quadrature<T, 2>,
                                                                             Quadrature<T, 3>,
                                                                             Transform<T, 3, 3>,
                                                                             Transform<T, 2, 3>> {};

    template <typename T>
    class BuildQuadratureAlgo<T, 2, 2, 1> final : public BuildQuadratureBase<Polygon<T, 2>,
                                                                             Line<T, 2>,
                                                                             Quadrature<T, 2>,
                                                                             Quadrature<T, 1>,
                                                                             Quadrature<T, 2>,
                                                                             Transform<T, 2, 2>,
                                                                             Transform<T, 1, 2>> {};

    template <class MasterElem, class SlaveElem>
    class L2Transfer : public Transfer<typename SlaveElem::T, SlaveElem::PhysicalDim>, public Describable {
    public:
        using T = typename SlaveElem::T;

        static const int MasterDim = MasterElem::Dim;
        static const int SlaveDim = SlaveElem::Dim;
        static const int PhysicalDim = SlaveElem::PhysicalDim;
        static const int NMasterNodes = MasterElem::NNodes;
        static const int NSlaveNodes = SlaveElem::NNodes;

        using CoVector = moonolith::Vector<T, PhysicalDim>;
        using Algo = moonolith::BuildQuadratureAlgo<T, PhysicalDim, MasterDim, SlaveDim>;
        using MasterAffineTrafo = moonolith::AffineTransform<T, MasterDim, PhysicalDim>;
        using SlaveAffineTrafo = moonolith::AffineTransform<T, SlaveDim, PhysicalDim>;

        void set_quadrature(const Quadrature<T, SlaveDim> &q_rule) { algo_.q_rule = q_rule; }

        void set_quadrature(Quadrature<T, SlaveDim> &&q_rule) { algo_.q_rule = std::move(q_rule); }

        bool assemble(MasterElem &master, SlaveElem &slave);

        bool assemble_affine(MasterElem &master, SlaveElem &slave);

        bool assemble_warped(MasterElem &master, SlaveElem &slave);

        void assemble(MasterElem &master,
                      SlaveElem &slave,
                      const Quadrature<T, MasterDim> &q_master,
                      const Quadrature<T, SlaveDim> &q_slave);

        L2Transfer();

        inline void clear() { intersection_measure_ = 0.0; }

        inline const Matrix<T, NSlaveNodes, NMasterNodes> &coupling_matrix() const { return coupling_mat_; }

        inline const Matrix<T, NSlaveNodes, NSlaveNodes> &mass_matrix() const { return mass_mat_; }

        inline const Matrix<T, NSlaveNodes, NSlaveNodes> &transformation() const { return trafo_; }

        inline T intersection_measure() const { return intersection_measure_; }

        void describe(std::ostream &os) const override;

        inline bool dual_lagrange_multiplier() const { return dual_lagrange_multiplier_; }

        inline void dual_lagrange_multiplier(const bool val) { dual_lagrange_multiplier_ = val; }

        inline void use_reference_frame(const bool use_reference_frame) {
            algo_.use_reference_frame(use_reference_frame);
        }

    private:
        Algo algo_;
        T intersection_measure_;

        FE<MasterElem> master_fe_;
        FE<SlaveElem> slave_fe_;
        Dual<SlaveElem> dual_fe_;
        TransformedFE<SlaveElem> trafo_fe_;

        Matrix<T, NSlaveNodes, NMasterNodes> coupling_mat_;
        Matrix<T, NSlaveNodes, NSlaveNodes> mass_mat_;
        Matrix<T, NSlaveNodes, NSlaveNodes> trafo_;

        std::shared_ptr<MasterAffineTrafo> trafo_master;
        std::shared_ptr<SlaveAffineTrafo> trafo_slave;

        std::shared_ptr<Transform<T, MasterDim, PhysicalDim>> iso_trafo_master;
        std::shared_ptr<Transform<T, SlaveDim, PhysicalDim>> iso_trafo_slave;

        bool dual_lagrange_multiplier_;
    };

}  // namespace moonolith

#endif  // MOONOLITH_L2_ASSEMBLER_HPP
