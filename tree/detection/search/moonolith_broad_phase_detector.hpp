#ifndef MOONOLITH_BROAD_PHASE_DETECTOR_HPP
#define MOONOLITH_BROAD_PHASE_DETECTOR_HPP

#include "moonolith_forward_declarations.hpp"

#include "moonolith_bound_synchronize.hpp"
#include "moonolith_predicate.hpp"
#include "moonolith_stream_utils.hpp"
#include "moonolith_verbose_object.hpp"

#include <iostream>

namespace moonolith {

    template <class Tree>
    class BroadPhaseDetector : public VerboseObject {
    public:
        bool detect(Communicator &comm, Tree &tree, BoundSynchronize<typename Tree::Traits> &bound_synchronize) const {
            assert(!comm.has_pending_requests());

            if (comm.is_alone()) {
                if (this->verbose() && comm.is_root()) {
                    logger() << "[Status] algorithm not run for single process" << std::endl;
                }

                return false;
            }

            if (this->verbose() && comm.is_root()) {
                logger() << comm << "[Status] Broad-phase detection BEGIN" << std::endl;
            }

            // Exchange top level information
            ByteOutputBuffer os;
            bound_synchronize.initialize(*tree.root(), comm.size());
            bound_synchronize.write_local(os);

            if (bound_synchronize.has_static_serial_size()) {
                if (this->verbose() && comm.is_root()) {
                    logger() << comm << "[Status] Using static size bound synch" << std::endl;
                }

                ByteInputBuffer is;
                is.reserve(os.size() * comm.size());
                comm.all_gather(os.pointer(), is.pointer(), os.size());

                for (Integer rank = 0; rank < comm.size(); ++rank) {
                    bound_synchronize.read_merge_for(rank, is);
                }

            } else {
#if MPI_VERSION >= 3

                if (this->verbose() && comm.is_root()) {
                    logger() << comm << "[Status] Using dynamic size bound synch (new)" << std::endl;
                }

                ByteInputBuffer is;
                is.reserve(os.size() * comm.size());
                comm.all_gatherv(os, is);

                for (Integer rank = 0; rank < comm.size(); ++rank) {
                    bound_synchronize.read_merge_for(rank, is);
                }

#else   // MPI_VERSION >= 3
                if (this->verbose() && comm.is_root()) {
                    logger() << comm << "[Status] Using dynamic size bound synch" << std::endl;
                }

                std::vector<ByteInputBuffer> recv_buffers;
                comm.unstructured_all_gather(os, recv_buffers, false);

                const Integer n_incoming = comm.size() - 1;
                for (Integer i = 0; i < n_incoming; ++i) {
                    Integer rank, index;
                    while (!comm.test_recv_any(&rank, &index)) {
                    }

                    ByteInputBuffer &is(recv_buffers[rank]);
                    bound_synchronize.read_merge_for(rank, is);
                }

                comm.wait_all();
                comm.clear();
#endif  // MPI_VERSION >= 3
            }

            if (this->verbose()) {
                bound_synchronize.describe(logger());
            }

            int ok = tree.expand_bound(bound_synchronize.global_bound());
            tree.finalize();

            comm.all_reduce(&ok, 1, MPIMax());

            if (!ok) {
                // should never happen
                std::cerr << "[Error] Bound expansion unsuccessfull" << std::endl;
                assert(false);
                return false;
            }

            if (this->verbose() && comm.is_root()) {
                logger() << comm << "[Status] Broad-phase detection END" << std::endl;
            }

            return true;
        }
    };

}  // namespace moonolith

#endif  // MOONOLITH_BROAD_PHASE_DETECTOR_HPP
