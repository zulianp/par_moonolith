#include "moonolith_serial_hash_grid.hpp"

#include "moonolith_stream_utils.hpp"

#include <cassert>
#include <cmath>
#include <algorithm>    // std::sort

namespace moonolith {

    template <int Dim, typename T>
    Integer SerialHashGrid<Dim, T>::hash(const Point &point) const {
        T x = (point(0) - box_.min(0)) / range_(0);
        Integer result = std::floor(x * dims_[0]);

        Integer totalDim = dims_[0];

        for (int i = 1; i < range_.size(); ++i) {
            result *= dims_[i];

            x = (point(i) - box_.min(i)) / range_(i);
            result += std::floor(x * dims_[i]);
            totalDim *= dims_[i];
        }

        if (result >= totalDim || result < 0) {
            printf("error -> %ld\n", result);
        }

        return result;
    }

    template <int Dim, typename T>
    Integer SerialHashGrid<Dim, T>::hash(const IntPoint &coord) const {
        Integer result = coord[0];
        Integer totalDim = dims_[0];

        for (int i = 1; i < range_.size(); ++i) {
            result *= dims_[i];
            result += coord[i];
            totalDim *= dims_[i];
        }

        return result;
    }

    template <int Dim, typename T>
    void SerialHashGrid<Dim, T>::hash_range(const Point &min, const Point &max, std::vector<Integer> &hashes) {
        const int dim = min.size();
        IntPoint imin(dim), imax(dim);

        // generate tensor indices
        for (int i = 0; i < dim; ++i) {
            T x = (min(i) - box_.min(i)) / range_(i);
            imin[i] = std::floor(x * dims_[i]);
        }

        for (int i = 0; i < dim; ++i) {
            T x = (max(i) - box_.min(i)) / range_(i);
            imax[i] = std::floor(x * dims_[i]);
        }

        IntPoint offsets;
        for (int i = 0; i < dim; ++i) {
            offsets[i] = imax[i] - imin[i];
        }

        // FIXME make more general for greater dim
        if (dim == 1) {
            IntPoint coord;
            for (int i = imin[0]; i <= imax[0]; ++i) {
                coord[0] = i;
                hashes.push_back(this->hash(coord));
            }

        } else if (dim == 2) {
            IntPoint coord;
            for (int i = imin[0]; i <= imax[0]; ++i) {
                for (int j = imin[1]; j <= imax[1]; ++j) {
                    coord[0] = i;
                    coord[1] = j;
                    hashes.push_back(this->hash(coord));
                }
            }
        } else if (dim == 3) {
            IntPoint coord;
            for (int i = imin[0]; i <= imax[0]; ++i) {
                for (int j = imin[1]; j <= imax[1]; ++j) {
                    for (int k = imin[2]; k <= imax[2]; ++k) {
                        coord[0] = i;
                        coord[1] = j;
                        coord[2] = k;
                        hashes.push_back(this->hash(coord));
                    }
                }
            }
        } else {
            assert(false && "dim > 3 not supported yet!");
        }
    }

    template <int Dim, typename T>
    SerialHashGrid<Dim, T>::SerialHashGrid(const Box &box, const IntPoint &dims) : box_(box), dims_(dims), n_cells_(1) {
        init();
    }

    template <int Dim, typename T>
    void SerialHashGrid<Dim, T>::init() {
        box_.enlarge(1e-8);

        n_cells_ = 1;
        for (int i = 0; i < dims_.size(); ++i) {
            range_[i] = box_.max(i);
            range_[i] -= box_.min(i);
            n_cells_ *= dims_[i];
        }
    }

    template <int Dim, typename T>
    SerialHashGrid<Dim, T>::SerialHashGrid() {
        clear();
    }

    template <int Dim, typename T>
    bool SerialHashGrid<Dim, T>::detect(const std::vector<Box> &boxes_1,
                                        const std::vector<Box> &boxes_2,
                                        std::vector<Integer> &pairs) {
        const Integer n1 = boxes_1.size();
        const Integer n2 = boxes_2.size();

        pairs.clear();
        pairs.reserve(2 * std::max(n1, n2));

        if (empty()) {
            const Integer n_x_dim = std::max(1, int(std::pow(n1, 1. / Dim)));
            dims_.set(n_x_dim);

            for (Integer i = 0; i < n1; ++i) {
                box_ += boxes_1[i];
            }

            init();
        }

        std::vector<std::vector<Integer>> box_table_1(n_cells_);

        Point pmin, pmax;
        std::vector<Integer> hashes;
        for (Integer i = 0; i < n1; ++i) {
            const Box &b = boxes_1[i];

            b.get_min(pmin);
            b.get_max(pmax);

            hashes.clear();
            this->hash_range(pmin, pmax, hashes);

            for (auto h : hashes) {
                box_table_1[h].push_back(i);
            }
        }

        std::vector<Integer> candidates;
        for (Integer i = 0; i < n2; ++i) {
            const Box &b = boxes_2[i];

            b.get_min(pmin);
            b.get_max(pmax);

            hashes.clear();
            this->hash_range(pmin, pmax, hashes);
            candidates.clear();

            for (auto h : hashes) {
                if (h < 0) {
                    continue;
                }

                for (int j : box_table_1[h]) {
                    const Box &bj = boxes_1[j];

                    if (b.intersects(bj)) {
                        candidates.push_back(j);
                    }
                }
            }

            std::sort(candidates.begin(), candidates.end());
            auto last = std::unique(candidates.begin(), candidates.end());
            candidates.erase(last, candidates.end());

            for (auto c : candidates) {
                pairs.push_back(c);
                pairs.push_back(i);
            }
        }

        return !pairs.empty();
    }
}  // namespace moonolith
