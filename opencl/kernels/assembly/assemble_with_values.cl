
#include "make_assembly_values.cl"
#include "local_assembly.cl"

#include "fe/fe_interfaces.cl"

//use it for code genereation
m_kernel__ void assemble_with_values(
							//input
							//A) basic data
							const SizeType n_dims,						//0)
							const SizeType n_codims,						//1)
							const SizeType n_elements,					//2)

						    //B) quadrature meta-data
						    m_global__ SizeType * quad_meta, 			//3)  (n_elements) x 4 [order, n_shape_fun_offset, n_quad_points_offset, element_matrix_offset]
							
							//C) Geometry
							m_global__ Scalar   * global_points,      //4)
							m_global__ Scalar   * jacobians,			//5)
							m_global__ Scalar   * dxs,				//6)
							
							//D) Basis functions
						    m_global__ Scalar   *	functions,			//7) 
						    m_global__ Scalar   * gradients,			//8)
							
    						//E) output
							m_global__ Scalar	  * element_matrices	//9) (sum((el_ptr[1+1]-el_ptr[i])^2)*n_block_entries=1)
)
{

	KERNEL_ASSERT(n_dims == n_codims || n_codims == 1, "n_codims is supported only for n_codims = n_dims, or n_codims = 1");

	Scalar local_matrix[MAX_SHAPE_FUNCS * MAX_SHAPE_FUNCS * MAX_CODIM * MAX_CODIM];
	const SizeType n_dims2 = n_dims * n_dims;

	FEObject fe;
	for(SizeType i = get_global_id(0); i < n_elements; i+= get_global_size(0)) {
		const SizeType element_matrix_offset = get_element_matrix_offset(i, quad_meta);


		make_fe_object_from_global_values(i, n_dims, quad_meta, global_points, jacobians, dxs,	functions, gradients, &fe);


		//or other assembly calls (for instance generated code)
		if(n_codims == 1) {
			const SizeType n_entries = fe.n_shape_functions*fe.n_shape_functions;
			
			REPORT_ERROR("NOTHING IMPLEMENTED HERE");

			//local to global memory
			generic_copy(n_entries, local_matrix, &element_matrices[element_matrix_offset]);

			// matrix_print(fe.n_shape_functions, fe.n_shape_functions, local_matrix);		
		} else {
			const SizeType n_entries = fe.n_shape_functions * fe.n_shape_functions * n_dims2;
	
			REPORT_ERROR("NOTHING IMPLEMENTED HERE");

			//local to global memory
			generic_copy(n_entries, local_matrix, &element_matrices[element_matrix_offset]);

			// block_matrix_print(fe.n_shape_functions, fe.n_shape_functions, n_codims, n_codims, local_matrix);		
		}
	}
}

m_kernel__ void assemble_block_mass_matrix_with_values(
							//input
							//A) basic data
							const SizeType n_dims,						//0)
							const SizeType n_codims,						//1)
							const SizeType n_elements,					//2)

						    //B) quadrature meta-data
						    m_global__ SizeType * quad_meta, 			//3)  (n_elements) x 4 [order, n_shape_fun_offset, n_quad_points_offset, element_matrix_offset]
							
							//C) Geometry
							m_global__ Scalar   * global_points,      //4)
							m_global__ Scalar   * jacobians,			//5)
							m_global__ Scalar   * dxs,				//6)
							
							//D) Basis functions
						    m_global__ Scalar   *	functions,			//7) 
						    m_global__ Scalar   * gradients,			//8)
							
    						//E) output
							m_global__ Scalar	  * element_matrices	//9) (sum((el_ptr[1+1]-el_ptr[i])^2)*n_block_entries=1)
)
{
	KERNEL_ASSERT(n_dims == n_codims, "n_codims is supported only for n_codims = n_dims");

	Scalar local_matrix[MAX_SHAPE_FUNCS * MAX_SHAPE_FUNCS * MAX_CODIM * MAX_CODIM];
	const SizeType n_dims2 = n_dims * n_dims;

	FEObject fe;
	for(SizeType i = get_global_id(0); i < n_elements; i+= get_global_size(0)) {
		const SizeType element_matrix_offset = get_element_matrix_offset(i, quad_meta);
		

		make_fe_object_from_global_values(i, n_dims, quad_meta, global_points, jacobians, dxs,	functions, gradients, &fe);
		block_mass_matrix_assemble_local(&fe, &fe, local_matrix);


		const SizeType n_entries = fe.n_shape_functions * fe.n_shape_functions * n_dims2;
		//local to global memory
		generic_copy(n_entries, local_matrix, &element_matrices[element_matrix_offset]);
		// block_matrix_print(fe.n_shape_functions, fe.n_shape_functions, n_codims, n_codims, local_matrix);		
	}
}

m_kernel__ void assemble_linear_elasticity_with_values(
							//input
							//A) basic data
							const SizeType n_dims,						//0)
							const SizeType n_codims,						//1)
							const SizeType n_elements,					//2)

						    //B) quadrature meta-data
						    m_global__ SizeType * quad_meta, 			//3)  (n_elements) x 4 [order, n_shape_fun_offset, n_quad_points_offset, element_matrix_offset]
							
							//C) Geometry
							m_global__ Scalar   * global_points,      //4)
							m_global__ Scalar   * jacobians,			//5)
							m_global__ Scalar   * dxs,				//6)
							
							//D) Basis functions
						    m_global__ Scalar   *	functions,			//7) 
						    m_global__ Scalar   * gradients,			//8)
							
    						//E) output
							m_global__ Scalar	  * element_matrices,	//9) (sum((el_ptr[1+1]-el_ptr[i])^2)*n_block_entries=1)
							
							//F) Model paramters (EXTRAS)				
							const Scalar mu,						//10)
							const Scalar lambda					//11)
)
{
	KERNEL_ASSERT(n_dims == n_codims, "n_codims is supported only for n_codims = n_dims");

	Scalar local_matrix[MAX_SHAPE_FUNCS * MAX_SHAPE_FUNCS * MAX_CODIM * MAX_CODIM];
	const SizeType n_dims2 = n_dims * n_dims;

	FEObject fe;
	for(SizeType i = get_global_id(0); i < n_elements; i+= get_global_size(0)) {
		const SizeType element_matrix_offset = get_element_matrix_offset(i, quad_meta);
		

		make_fe_object_from_global_values(i, n_dims, quad_meta, global_points, jacobians, dxs,	functions, gradients, &fe);
		
		linear_elasticity_assemble_local(mu, lambda, &fe, &fe, local_matrix);


		const SizeType n_entries = fe.n_shape_functions * fe.n_shape_functions * n_dims2;
		//local to global memory
		generic_copy(n_entries, local_matrix, &element_matrices[element_matrix_offset]);
		// block_matrix_print(fe.n_shape_functions, fe.n_shape_functions, n_codims, n_codims, local_matrix);		
	}
}

m_kernel__ void assemble_mass_matrix_with_values(
							//input
							//A) basic data
							const SizeType n_dims,						//0)
							const SizeType n_codims,						//1)
							const SizeType n_elements,					//2)

						    //B) quadrature meta-data
						    m_global__ SizeType * quad_meta, 			//3)  (n_elements) x 4 [order, n_shape_fun_offset, n_quad_points_offset, element_matrix_offset]
							
							//C) Geometry
							m_global__ Scalar   * global_points,      //4)
							m_global__ Scalar   * jacobians,			//5)
							m_global__ Scalar   * dxs,				//6)
							
							//D) Basis functions
						    m_global__ Scalar   *	functions,			//7) 
						    m_global__ Scalar   * gradients,			//8)
							
    						//E) output
							m_global__ Scalar	  * element_matrices	//9) (sum((el_ptr[1+1]-el_ptr[i])^2)*n_block_entries=1)
)
{
	KERNEL_ASSERT(1 == n_codims, "n_codims is supported only for n_codims = 1");

	Scalar local_matrix[MAX_SHAPE_FUNCS * MAX_SHAPE_FUNCS];
	

	FEObject fe;
	for(SizeType i = get_global_id(0); i < n_elements; i+= get_global_size(0)) {
		const SizeType element_matrix_offset = get_element_matrix_offset(i, quad_meta);
		

		make_fe_object_from_global_values(i, n_dims, quad_meta, global_points, jacobians, dxs,	functions, gradients, &fe);
		// fe_object_print(&fe);

		mass_matrix_assemble_local(&fe, &fe, local_matrix);

		const SizeType n_entries = fe.n_shape_functions * fe.n_shape_functions;
		//local to global memory
		generic_copy(n_entries, local_matrix, &element_matrices[element_matrix_offset]);
		// block_matrix_print(fe.n_shape_functions, fe.n_shape_functions, n_codims, n_codims, local_matrix);		
	}
}

m_kernel__ void assemble_laplacian_with_values(
							//input
							//A) basic data
							const SizeType n_dims,						//0)
							const SizeType n_codims,						//1)
							const SizeType n_elements,					//2)

						    //B) quadrature meta-data
						    m_global__ SizeType * quad_meta, 			//3)  (n_elements) x 4 [order, n_shape_fun_offset, n_quad_points_offset, element_matrix_offset]
							
							//C) Geometry
							m_global__ Scalar   * global_points,      //4)
							m_global__ Scalar   * jacobians,			//5)
							m_global__ Scalar   * dxs,				//6)
							
							//D) Basis functions
						    m_global__ Scalar   *	functions,			//7) 
						    m_global__ Scalar   * gradients,			//8)
							
    						//E) output
							m_global__ Scalar	  * element_matrices	//9) (sum((el_ptr[1+1]-el_ptr[i])^2)*n_block_entries=1)
)
{
	KERNEL_ASSERT(1 == n_codims, "n_codims is supported only for n_codims = 1");

	Scalar local_matrix[MAX_SHAPE_FUNCS * MAX_SHAPE_FUNCS];

	FEObject fe;
	for(SizeType i = get_global_id(0); i < n_elements; i+= get_global_size(0)) {
		const SizeType element_matrix_offset = get_element_matrix_offset(i, quad_meta);
		

		make_fe_object_from_global_values(i, n_dims, quad_meta, global_points, jacobians, dxs,	functions, gradients, &fe);

		// fe_object_print(&fe);

		laplacian_assemble_local(&fe, &fe, local_matrix);

		const SizeType n_entries = fe.n_shape_functions * fe.n_shape_functions;
		//local to global memory
		generic_copy(n_entries, local_matrix, &element_matrices[element_matrix_offset]);
		// block_matrix_print(fe.n_shape_functions, fe.n_shape_functions, n_codims, n_codims, local_matrix);		
	}
}
