#ifndef MESH_INTERFACE_CL
#define MESH_INTERFACE_CL 

#include "kernel_base.cl"
#include "fe_object.cl"

typedef struct {
	SizeType n_elements;
	SizeType n_nodes;
	SizeType n_dims;

	m_global__ SizeType * el_ptr;
	m_global__ SizeType * el_index;
	m_global__ SizeType * el_type;
	m_global__ SizeType * meta;
	m_global__ Scalar   * points;
} Mesh;


#ifndef CLIPP_HOST_CL

SizeType mesh_n_points(const Mesh mesh, const SizeType element);
void mesh_points(const Mesh mesh, const SizeType element, Scalar * points);
bool make_fe_object(Mesh mesh, const SizeType element, const SizeType order, FEObject * fe);
bool make_fe_object_2(Mesh mesh, const SizeType element, const SizeType order, FEObject * fe);
bool make_fe_object_3(Mesh mesh, const SizeType element, const SizeType order, FEObject * fe);
void mesh_print(Mesh *mesh);

bool make_fe_object_from_global_quad(const Mesh mesh, const SizeType element, const SizeType n_quad_points, const Scalar *quad_points, const Scalar *quad_weights, FEObject *fe);
bool make_fe_object_from_global_quad_2(const Mesh mesh, const SizeType element, const SizeType n_quad_points, 
	const Scalar *quad_points, const Scalar *quad_weights, FEObject *fe);

bool make_fe_object_from_global_quad_3(const Mesh mesh, const SizeType element, const SizeType n_quad_points, 
	const Scalar *quad_points, const Scalar *quad_weights, FEObject *fe);


bool make_fe_object_from_quad_3(Mesh mesh, const SizeType element, const SizeType n_quad_points, 
							    const Scalar *quad_points, const Scalar * quad_weights, 
							    FEObject * fe);

bool make_fe_object_from_quad_2(Mesh mesh, const SizeType element, const SizeType n_quad_points, 
							    const Scalar *quad_points, const Scalar * quad_weights, 
							    FEObject * fe);

bool inverse_transform_3(const Mesh mesh, const SizeType element, const SizeType n_quad_points, const Scalar *global_quad_points, Scalar *quad_points);
bool transform_3(const Mesh mesh, const SizeType element, const SizeType n_quad_points, const Scalar *quad_points, Scalar *global_quad_points);

bool transform_2(const Mesh mesh, const SizeType element, const SizeType n_quad_points, const Scalar *quad_points, Scalar *global_quad_points);
bool inverse_transform_2(const Mesh mesh, const SizeType element, const SizeType n_quad_points, const Scalar *global_quad_points, Scalar *quad_points);
#endif
#endif //MESH_INTERFACE_CL

