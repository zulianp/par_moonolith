#ifndef MOONOLITH_MATRIX_HPP
#define MOONOLITH_MATRIX_HPP

#include "moonolith_base.hpp"

#include "moonolith_empirical_tol.hpp"
#include "moonolith_expanding_array.hpp"
#include "moonolith_geo_algebra.hpp"
#include "moonolith_invert.hpp"
#include "moonolith_static_math.hpp"
#include "moonolith_vector.hpp"

#include <algorithm>
#include <array>
#include <cassert>
#include <initializer_list>

namespace moonolith {

    template <typename T, int Rows, int Cols>
    class Matrix {
    public:
        static_assert(Rows > 0, "matrix row size must be positive");
        static_assert(Cols > 0, "matrix col size must be positive");

        Matrix() {}

        Matrix(std::initializer_list<T> values) {
            assert(values.size() == Rows * Cols);
            std::copy(std::begin(values), std::end(values), std::begin(this->values));
        }

        inline constexpr static int rows() { return Rows; }
        inline constexpr static int cols() { return Cols; }

        inline T sum() const {
            T ret = 0.0;
            for (const auto &v : values) {
                ret += v;
            }

            return ret;
        }

        inline void sum_cols(Vector<T, Rows> &v) const {
            get_col(0, v);

            for (int i = 0; i < Rows; ++i) {
                for (int j = 1; j < Cols; ++j) {
                    v[i] += (*this)(i, j);
                }
            }
        }

        inline T &operator()(const int i, const int j) {
            assert(i < Rows);
            assert(j < Cols);
            return values[i * cols() + j];
        }

        inline const T &operator()(const int i, const int j) const {
            assert(i < Rows);
            assert(j < Cols);
            return values[i * cols() + j];
        }

        inline void col(const int c, const Vector<T, Rows> &v) {
            assert(c < Cols);

            for (int d = 0; d < Rows; ++d) {
                (*this)(d, c) = v(d);
            }
        }

        inline void get_col(const int c, Vector<T, Rows> &v) const {
            assert(c < Cols);

            for (int d = 0; d < Rows; ++d) {
                v[d] = (*this)(d, c);
            }
        }

        inline void zero() { std::fill(begin(values), end(values), 0.); }

        inline void identity() {
            zero();
            auto N = StaticMin<Rows, Cols>::value;
            for (int i = 0; i < N; ++i) {
                (*this)(i, i) = 1.0;
            }
        }

        void describe(std::ostream &os) const {
            for (int i = 0; i < Rows; ++i) {
                for (int j = 0; j < Cols; ++j) {
                    os << (*this)(i, j) << " ";
                }
                os << "\n";
            }

            os << "\n";
        }

        friend std::ostream &operator<<(std::ostream &os, const Matrix &m) {
            m.describe(os);
            return os;
        }

        template <int OtherCols>
        inline Matrix<T, Rows, OtherCols> operator*(const Matrix<T, Cols, OtherCols> &other) const {
            Matrix<T, Rows, OtherCols> ret;
            ret.zero();

            for (int i = 0; i < Rows; ++i) {
                for (int j = 0; j < Cols; ++j) {
                    for (int k = 0; k < OtherCols; ++k) {
                        ret(i, k) += (*this)(i, j) * other(j, k);
                    }
                }
            }

            return ret;
        }

        inline bool inverse(Matrix<T, Cols, Rows> &result) const {
            PseudoInvert<T, Rows, Cols> p_inv;
            return p_inv.apply(values, result.values);
        }

        inline void mult(const Vector<T, Cols> &in, Vector<T, Rows> &out) {
            mat_vec_mul<T, Rows, Cols>(values, in, out);
        }

        inline void transpose_mult(const Vector<T, Rows> &in, Vector<T, Cols> &out) {
            mat_T_vec_mul<T, Rows, Cols>(values, in, out);
        }

        std::array<T, Rows * Cols> values;
    };

    template <typename T, int N>
    T measure(const Matrix<T, N, N> &mat) {
        static_assert(N > 0, "matrix size must be positive");
        return det(mat.values);
    }

    template <typename T, int Rows, int Cols>
    T measure(const Matrix<T, Rows, Cols> &mat) {
        static_assert(Rows > 0, "matrix row size must be positive");
        static_assert(Cols > 0, "matrix col size must be positive");

        return pseudo_det<T, Rows, Cols>(mat.values);
    }

    template <typename T>
    class Matrix<T, -1, -1> {
    public:
        Matrix() : rows_(0), cols_(0) {}

        Matrix(std::initializer_list<T> values) : rows_(0), cols_(0), values(values.size()) {
            std::copy(std::begin(values), std::end(values), std::begin(this->values));
        }

        inline void set_rows(const int rows) { rows_ = rows; }

        inline void set_cols(const int cols) { cols_ = cols; }

        inline int rows() const { return rows_; }
        inline int cols() const { return cols_; }
        inline void resize(const int rows, const int cols) {
            set_rows(rows);
            set_cols(cols);
            values.resize(rows * cols);
        }

        inline T sum() const {
            T ret = 0.0;
            for (const auto &v : values) {
                ret += v;
            }

            return ret;
        }

        template <int OtherRows>
        inline void get_col(const int c, Vector<T, OtherRows> &v) const {
            moonolith::resize(v, rows());
            for (int d = 0; d < rows(); ++d) {
                v[d] = (*this)(d, c);
            }
        }

        template <int OtherRows>
        inline void sum_cols(Vector<T, OtherRows> &v) const {
            get_col(0, v);

            for (int i = 0; i < rows(); ++i) {
                for (int j = 1; j < cols(); ++j) {
                    v[i] += (*this)(i, j);
                }
            }
        }

        inline T &operator()(const int i, const int j) {
            assert(i < rows());
            assert(j < cols());
            return values[i * cols() + j];
        }

        inline const T &operator()(const int i, const int j) const {
            assert(i < rows());
            assert(j < cols());
            return values[i * cols() + j];
        }

        template <int Rows>
        inline void col(const int c, const Vector<T, Rows> &v) {
            assert(Rows == rows());
            assert(c < cols());

            for (int d = 0; d < rows(); ++d) {
                (*this)(d, c) = v(d);
            }
        }

        inline void zero() { std::fill(values.begin(), values.end(), 0.); }

        inline void identity() {
            assert(rows_ > 0);
            assert(cols_ > 0);

            zero();
            auto N = std::min(rows(), cols());
            for (int i = 0; i < N; ++i) {
                (*this)(i, i) = 1.0;
            }
        }

        void describe(std::ostream &os) const {
            for (int i = 0; i < rows(); ++i) {
                for (int j = 0; j < cols(); ++j) {
                    os << (*this)(i, j) << " ";
                }
                os << "\n";
            }

            os << "\n";
        }

        friend std::ostream &operator<<(std::ostream &os, const Matrix &m) {
            m.describe(os);
            return os;
        }

        template <class OtherMatrix>
        inline Matrix operator*(const OtherMatrix &other) const {
            assert(cols() == other.rows());

            Matrix ret;
            ret.set_size(rows(), other.cols());

            ret.zero();

            for (int i = 0; i < rows(); ++i) {
                for (int j = 0; j < cols(); ++j) {
                    for (int k = 0; k < other.cols(); ++k) {
                        ret(i, k) += (*this)(i, j) * other(j, k);
                    }
                }
            }

            return ret;
        }

        template <class OtherMatrix>
        inline Matrix &operator=(const OtherMatrix &other) {
            resize(other.rows(), other.cols());
            std::copy(other.values.begin(), other.values.end(), this->values.begin());
            return *this;
        }

        // inline bool inverse(Matrix & /*result*/) const {
        //     // PseudoInvert<T, -1, -1> p_inv;
        //     // return p_inv.apply(*this, result);

        //     assert(false && "implement me");
        //     return false;
        // }

        // template <int Rows, int Cols>
        // inline void mult(const Vector<T, Cols> & /*in*/, Vector<T, Rows> & /*out*/) {
        //     // assert(out.size() == rows());
        //     // assert(in.size() == cols());

        //     assert(false && "implement me");
        //     // mat_vec_mul<T, Rows, Cols>(values, in, out);
        // }

        // template <int Rows, int Cols>
        // inline void transpose_mult(const Vector<T, Rows> & /*in*/, Vector<T, Cols> & /*out*/) {
        //     assert(false && "implement me");
        //     // mat_T_vec_mul<T, Rows, Cols>(values, in, out);
        // }

        int rows_;
        int cols_;
        Storage<T> values;
    };

    template <typename T, int Rows, int Cols>
    class Resize<Matrix<T, Rows, Cols> > {
    public:
        void static apply(const Matrix<T, Rows, Cols> &, const int rows, const int cols) {
            assert(rows == Rows);
            assert(cols == Cols);

            MOONOLITH_UNUSED(rows);
            MOONOLITH_UNUSED(cols);
        }
    };

    template <class M>
    inline void resize(M &m, const int rows, const int cols) {
        Resize<M>::apply(m, rows, cols);
    }

    template <typename T>
    class Resize<Matrix<T, -1, -1> > {
    public:
        void static apply(Matrix<T, -1, -1> &mat, const int rows, const int cols) { mat.resize(rows, cols); }
    };

    template <typename T, int N>
    void make_transpose(Matrix<T, N, N> &mat) {
        using std::swap;
        for (int i = 0; i < N; ++i) {
            for (int j = i + 1; j < N; ++j) {
                swap(mat(i, j), mat(j, i));
            }
        }
    }

    template <typename T>
    void make_transpose(Matrix<T, -1, -1> &mat) {
        assert(mat.rows() == mat.cols());
        using std::swap;
        for (int i = 0; i < mat.rows(); ++i) {
            for (int j = i + 1; j < mat.cols(); ++j) {
                swap(mat(i, j), mat(j, i));
            }
        }
    }

    template <typename T, int N>
    bool approxeq(const Matrix<T, N, N> &l, const Matrix<T, N, N> &r, const T tol = StandardTol<T>::value()) {
        using std::swap;
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < N; ++j) {
                if (!approxeq(l(i, j), r(i, j), tol)) {
                    return false;
                }
            }
        }

        return true;
    }

    template <typename T, int N>
    Matrix<T, N, N> diag(const Vector<T, N> &v) {
        const int n = v.size();

        Matrix<T, N, N> ret;
        resize(ret, n, n);

        ret.zero();

        for (int i = 0; i < n; ++i) {
            ret(i, i) = v[i];
        }

        return ret;
    }

    // template<typename T>
    // class PseudoInvert<T, -1, -1> {
    // public:
    //     template<class MatIn, class MatOut>
    //     bool apply(const MatIn &in, MatOut &out)
    //     {
    //         // mat_T_mat_mul<T, Rows, Cols>(in, A);

    //         // if(!invert(A, A_inv)) {
    //         //     return false;
    //         // }

    //         // mat_mat_T_mul<T, Cols, Cols, Rows>(A_inv, in, out);
    //         // return true;

    //         assert(false && "implement me");
    //         return false;
    //     }

    // private:
    //     std::array<T, Cols * Cols> A, A_inv;
    // };

}  // namespace moonolith

#endif  // MOONOLITH_MATRIX_HPP
