#ifndef PAR_MOONOLITH_SERIAL_HASH_GRID_HPP
#define PAR_MOONOLITH_SERIAL_HASH_GRID_HPP

#include "par_moonolith_config.hpp"

#include "moonolith_aabb.hpp"
#include "moonolith_vector.hpp"

namespace moonolith {

    template <int Dim, typename T = Real>
    class SerialHashGrid {
    public:
        using Point = moonolith::Vector<T, Dim>;
        using IntPoint = moonolith::Vector<int, Dim>;
        using Box = moonolith::AABB<Dim, T>;

        Integer hash(const Point &point) const;
        Integer hash(const IntPoint &coord) const;
        void hash_range(const Point &min, const Point &max, std::vector<Integer> &hashes);
        SerialHashGrid(const Box &box, const IntPoint &dims);
        SerialHashGrid();

        inline void set_box(const Box &box) { box_ = box; }
        inline void set_dims(const IntPoint &dims) { dims_ = dims; }

        void init();
        bool empty() const { return n_cells_ == 0; }

        bool detect(const std::vector<Box> &boxes_1, const std::vector<Box> &boxes_2, std::vector<Integer> &pairs);

        inline Integer n_cells() const { return n_cells_; }
        inline void clear() {
            n_cells_ = 0;
            box_.clear();
            dims_.set(0);
        }

    private:
        Box box_;
        Point range_;
        IntPoint dims_;
        Integer n_cells_{0};
    };

}  // namespace moonolith

#endif  // PAR_MOONOLITH_SERIAL_HASH_GRID_HPP