#ifndef MOONOLITH_ELEM_TRI_1_HPP
#define MOONOLITH_ELEM_TRI_1_HPP

#include "moonolith_elem_tri.hpp"
#include "moonolith_elem_tri3.hpp"

namespace moonolith {

    template <typename T_, int PhysicalDim_ = 2>
    class Tri1 final : public Tri<T_, 0, PhysicalDim_> {
    public:
        static const int Order = 0;
        static const int Dim = 2;
        static const int PhysicalDim = PhysicalDim_;
        static const int NNodes = 1;

        using T = T_;

        using Vector = moonolith::Vector<T, Dim>;
        using CoVector = moonolith::Vector<T, PhysicalDim>;

        using Point = moonolith::Vector<T, Dim>;
        using CoPoint = moonolith::Vector<T, PhysicalDim>;

        using Super = moonolith::Tri<T_, 0, PhysicalDim_>;
        using Polygon = typename Super::Polygon;

    private:
        class Grad final {
        public:
            Grad() {
                f[0] = [](const Point &, Vector &g) { g.x = 0.0; };
            }

            std::array<std::function<void(const Point &, Vector &)>, NNodes> f;
        };

        class Fun final {
        public:
            Fun() {
                f[0] = [](const Point &) -> T { return 1.0; };
            }

            std::array<std::function<T(const Point &)>, NNodes> f;
        };

    public:
        T fun(const Integer i, const Point &p) const override { return fun_.f[i](p); }

        void grad(const Integer i, const Point &p, Vector &g) const override { grad_.f[i](p, g); }

        void hessian(const Integer, const Point &, std::array<T, Dim * Dim> &H) {
            std::fill(std::begin(H), std::end(H), 0.0);
        }

        void jacobian(const Point &p, std::array<T, PhysicalDim * Dim> &J) const override {
            (void)p;
            affine_approx_jacobian(J);
        }

        void affine_approx_jacobian(std::array<T, PhysicalDim * Dim> &J) const override {
            Affine<Tri1>::jacobian(points_, J);
        }

        CoPoint &node(const Integer i) override { return nodes_[i]; }

        const CoPoint &node(const Integer i) const override { return nodes_[i]; }

        void point(const Point &p, CoPoint &q) const override { Affine<Tri1>::point(points_, p, q); }

        inline CoPoint &point(const Integer idx) { return points_[idx]; }

        inline const CoPoint &point(const Integer idx) const { return points_[idx]; }

        Tri1() {}

        bool is_affine() const override { return true; }

        inline int n_nodes() const override { return NNodes; }
        inline int order() const override { return Order; }

        inline const std::array<CoPoint, 3> &points() const { return points_; }

        inline void make_reference() {
            Reference<Tri1>::points(nodes_);
            Reference<Tri3<T, PhysicalDim>>::points(points_);
        }

        inline ElemType type() const override { return TRI1; }

        inline bool is_simplex() const override { return true; }

        inline T measure() const override { return 0.5 * trapezoid_area(points_[0], points_[1], points_[2]); }

        inline T reference_measure() const override { return 0.5; }

        inline void make(Polygon &poly) const override {
            poly.resize(3);

            for (int i = 0; i < 3; ++i) {
                poly[i] = point(i);
            }
        }

    private:
        std::array<CoPoint, NNodes> nodes_;
        std::array<CoPoint, 3> points_;
        const Fun fun_;
        const Grad grad_;
    };

    template <typename T, int PhysicalDim>
    class Reference<Tri1<T, PhysicalDim>> {
    public:
        static const int NNodes = 1;

        template <class Point, std::size_t Size>
        static void points(std::array<Point, Size> &nodes) {
            static_assert(Size >= NNodes, "size must be at least 1");
            // p0
            auto x = sqrt(2.) / 4.0;
            nodes[0].x = x;
            nodes[0].y = x;
        }

        inline static T measure() { return 0.5; }
    };

    template <typename T, int PhysicalDim>
    class GaussQRule<Tri1<T, PhysicalDim>> {
    public:
        static bool get(const Integer order, Quadrature2<T> &q) { return Gauss::get(order, q); }
    };

}  // namespace moonolith

#endif  // MOONOLITH_ELEM_TRI_1_HPP
