#ifndef MOONOLITH_ISO_PARAMETRIC_TRANSFORM_FOR_SURFACE_HPP
#define MOONOLITH_ISO_PARAMETRIC_TRANSFORM_FOR_SURFACE_HPP

#include "moonolith_affine_transform.hpp"
#include "moonolith_transform.hpp"
#include "par_moonolith_config.hpp"

#include <iostream>

namespace moonolith {

    template <class Elem>
    class IsoParametricTransformForSurface : public Transform<typename Elem::T, Elem::Dim, Elem::PhysicalDim> {
    public:
        using T = typename Elem::T;
        static const int Dim = Elem::Dim;
        static const int PhysicalDim = Elem::PhysicalDim;

        using super = moonolith::Transform<T, Dim, PhysicalDim>;
        using Vector = typename super::Vector;
        using CoVector = typename super::CoVector;
        using CoPoint = typename super::CoPoint;
        using Point = typename super::Point;

        virtual ~IsoParametricTransformForSurface() {}

        static_assert(Dim < PhysicalDim, "for surface elements only");

        inline bool apply(const Point &in, CoPoint &out) override {
            elem_->point(in, out);
            return true;
        }

        inline void affine_apply_inverse(const CoPoint &in, Point &out) {
            mat_vec_mul<T, PhysicalDim, PhysicalDim>(augmented_J_inv, in, temp_out);
            temp_out += b_inv;

            // normal projection
            temp_out[Dim] = 0.0;
            for (int i = 0; i < Dim; ++i) {
                out[i] = temp_out[i];
            }
        }

        inline bool apply_inverse(const CoPoint &in, Point &out) override {
            // temp_out initialized here
            affine_apply_inverse(in, out);

            if (elem_->is_affine()) {
                return true;
            }

            elem_->point(out, f_in);
            g = in - f_in;
            auto len_g = length(g);

            if (len_g < tol_) {
                return true;
            }

            auto diff = 1e6;
            for (Integer i = 0; i < max_it_; ++i) {
                elem_->point(out, f_in);
                g = in - f_in;

                this->jacobian(out, augmented_J);
                p_inv.apply(augmented_J, augmented_J_inv);
                mat_vec_mul<T, PhysicalDim, PhysicalDim>(augmented_J_inv, g, h);

                // normal projection
                h[Dim] = 0.0;

                temp_out += h;

                diff = length(h);

                if (verbose_) {
                    std::cout << i << "] " << diff << "\n";
                    std::cout << "\tg = ";
                    print(g);

                    std::cout << "\tx = ";
                    print(out);

                    std::cout << "\th = ";
                    print(h);

                    std::cout << "\tf_in = ";
                    print(f_in);

                    std::cout << "\ttemp_x = ";
                    print(temp_out);
                }

                // normal projection
                for (int i = 0; i < Dim; ++i) {
                    out[i] = temp_out[i];
                }

                if (diff < tol_ || i >= max_it_) {
                    break;
                }
            }

            return diff < accept_tol_;
        }

        IsoParametricTransformForSurface(Elem &elem,
                                         const CoVector &surface_normal,
                                         const T tol = 1e-10,
                                         const Integer max_it = 10000,
                                         const T accept_tol = 1e-6,
                                         const bool verbose = false)
            : elem_(&elem),
              surface_normal_(surface_normal),
              tol_(tol),
              accept_tol_(accept_tol),
              max_it_(max_it),
              verbose_(verbose) {
            init_aux();
        }

        void init(Elem &elem, const CoVector &surface_normal) {
            elem_ = &elem;
            surface_normal_ = surface_normal;
            init_aux();
        }

        inline T estimate_condition_number() const override {
            // FIXME
            return 1.0;
        }

    private:
        Elem *elem_;
        CoVector surface_normal_;
        T tol_, accept_tol_;
        Integer max_it_;
        bool verbose_;

        // buffers
        std::array<T, PhysicalDim * Dim> J;
        std::array<T, PhysicalDim * PhysicalDim> augmented_J, augmented_J_inv;

        CoVector g;
        CoVector f_in;
        CoVector h;
        CoVector b;
        CoVector b_inv;
        CoVector temp_out;

        PseudoInvert<T, PhysicalDim, PhysicalDim> p_inv;

        void jacobian(const Point &p, std::array<T, PhysicalDim * PhysicalDim> &out) {
            elem_->jacobian(p, J);
            augment_jacobian(J, surface_normal_, out);
        }

        static void augment_jacobian(const std::array<T, PhysicalDim * Dim> &J,
                                     const CoVector &surface_normal,
                                     std::array<T, PhysicalDim * PhysicalDim> &out) {
            for (int i = 0; i < PhysicalDim; ++i) {
                for (int j = 0; j < Dim; ++j) {
                    out[i * PhysicalDim + j] = J[i * Dim + j];
                }

                out[i * PhysicalDim + Dim] = surface_normal[i];
            }

            assert(det(out) != 0.);
        }

        void init_aux() {
            elem_->affine_approx_jacobian(J);
            b = elem_->node(0);
            augment_jacobian(J, surface_normal_, augmented_J);
            p_inv.apply(augmented_J, augmented_J_inv);
            mat_vec_mul<T, PhysicalDim, PhysicalDim>(augmented_J_inv, b, b_inv);
            b_inv *= -1.0;
        }
    };

}  // namespace moonolith

#endif  // MOONOLITH_ISO_PARAMETRIC_TRANSFORM_FOR_SURFACE_HPP
