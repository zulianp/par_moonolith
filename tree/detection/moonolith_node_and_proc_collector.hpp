#ifndef MOONOLITH_NODE_AND_PROC_COLLECTOR_HPP
#define MOONOLITH_NODE_AND_PROC_COLLECTOR_HPP

#include "moonolith_synched_describable.hpp"
#include "moonolith_traversal_listener.hpp"
#include "moonolith_work_table.hpp"

#include <assert.h>
#include <memory>

namespace moonolith {

    template <class Tree>
    class NodeAndProcCollector : public TraversalListener<Tree>,
                                 public Visitor<typename Tree::Traits>,
                                 public Describable {
    public:
        typedef typename Tree::Traits Traits;
        typedef typename Tree::Node Node;
        typedef typename Tree::Leaf Leaf;
        typedef typename Tree::Branch Branch;
        typedef typename Tree::DataType DataType;

        typedef moonolith::TreeMemory<Traits> TreeMemory;
        typedef moonolith::RemoteNode<Node> RemoteNode;

        virtual ~NodeAndProcCollector() {}
        virtual void entry_added(Tree &tree,
                                 Node &node,
                                 const RemoteNode &remote_node,
                                 const Integer remote_rank) override {
            assert(remote_rank >= 0);

            remote_node_ = &remote_node;
            remote_rank_ = remote_rank;

            current_work_batch_ = std::make_shared<WorkBatch>();

            BreadthFirstNavigator<Tree> nav;
            nav.walk(node, *this, tree.memory());

            _work->reserve(tree.memory().n_nodes());
            current_work_batch_->set_global_handle(node.global_handle());
            current_work_batch_->set_owner_rank(owner_rank_);
            _work->add_batch(node.handle(), remote_rank, current_work_batch_);

            remote_node_ = NULL;
            remote_rank_ = -1;
        }

        virtual void add_local_entry(Tree &, Node &node) {
            if (node.empty()) return;

            auto batch = std::make_shared<WorkBatch>();
            batch->set_global_handle(node.global_handle());
            batch->set_owner_rank(owner_rank_);
            batch->insert(node.data_begin(), node.data_end());
            _work->add_batch(node.handle(), owner_rank_, batch);
        }

        virtual void traversal_terminated() override {}

        inline Integer remote_rank() const {
            assert(remote_rank_ >= 0);
            return remote_rank_;
        }

        void collect_data(Node &node, TreeMemory &memory) {
            memory.each(node.handle(), [this](const DataHandle &handle, DataType &data) {
                if (remote_node_->match(data)) {
                    this->current_work_batch_->insert(handle);
                }
            });
        }

        NavigatorOption visit(Leaf &leaf, TreeMemory &memory) override {
            if (leaf.empty()) {
                return CONTINUE;
            }

            if (!remote_node_->match(leaf)) {
                return CONTINUE;
            }
            collect_data(leaf, memory);

            return CONTINUE;
        }

        NavigatorOption visit(Branch &branch, TreeMemory &memory) override {
            if (!remote_node_->match(branch)) {
                return SKIP_SUBTREE;
            }

            collect_data(branch, memory);
            return CONTINUE;
        }

        NodeAndProcCollector(const std::shared_ptr<WorkTable> &work)
            : remote_node_(NULL), remote_rank_(-1), _work(work) {}

        void set_owner_rank(const Integer ownerRank) { owner_rank_ = ownerRank; }

        void set_remote_rank(const Integer rank) { remote_rank_ = rank; }

        void describe(std::ostream &os) const override { os << "Collecting for rank " << remote_rank_ << "\n"; }

    private:
        const RemoteNode *remote_node_;
        Integer remote_rank_;
        Integer owner_rank_;
        std::shared_ptr<WorkBatch> current_work_batch_;
        std::shared_ptr<WorkTable> _work;
    };
}  // namespace moonolith

#endif  // MOONOLITH_NODE_AND_PROC_COLLECTOR_HPP
