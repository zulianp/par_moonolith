

#ifndef FE_VALUES_INTERFACE_CL
#define FE_VALUES_INTERFACE_CL 

#include "algebra/algebra_interfaces.cl"

#ifndef CLIPP_HOST_CL

bool make_grad_dxd_at_quad_index(const SizeType quad_index, const FEObject *fe, Matrixdxd *result);
void make_grad_2x2_at_quad_index(const SizeType quad_index, const FEObject *fe, Matrixdxd *result);
void make_grad_3x3_at_quad_index(const SizeType quad_index, const FEObject *fe, Matrixdxd *result);

bool make_grad_dxd(FEObject *fe, Matrixdxd *grad);
void make_grad_2x2(FEObject *fe, Matrixdxd *result);
void make_grad_3x3(FEObject *fe, Matrixdxd *result);
void compute_traces(const SizeType n_dims, const SizeType n_matrices, const Matrixdxd *matrices, Scalar *traces);
void symmetrize(const SizeType n_dims, const SizeType n_matrices, Matrixdxd *matrices);

void make_fe_object_from_global_values(	const SizeType i, 
	const SizeType n_dims, 
	const m_global__ SizeType * quad_meta,
	const m_global__ Scalar   * global_points,     
	const m_global__ Scalar   * jacobians,		
	const m_global__ Scalar   * dxs,				
	const m_global__ Scalar   * functions,			
	const m_global__ Scalar   * gradients,		
	FEObject * fe	
	);


#endif //CLIPP_HOST_CL


#endif //FE_VALUES_INTERFACE_CL