#ifndef MOONOLITH_NTREE_WITH_TAGS_MATCH_HPP
#define MOONOLITH_NTREE_WITH_TAGS_MATCH_HPP

#include "moonolith_branch.hpp"
#include "moonolith_leaf.hpp"
#include "moonolith_n_tree_with_span_match.hpp"
#include "moonolith_node.hpp"
#include "moonolith_predicate.hpp"

namespace moonolith {

    template <class Tree>
    class NTreeWithTagsMatch : public NTreeWithSpanMatch<Tree> {
    public:
        typedef typename Tree::Traits Traits;
        typedef typename Traits::DataType DataType;
        typedef moonolith::Node<Traits> Node;
        typedef moonolith::Branch<Traits> Branch;
        typedef moonolith::Leaf<Traits> Leaf;
        typedef moonolith::Root<Traits> Root;

        virtual bool match(Node &node) const {
            if (predicate_) {
                Integer objTag = tag(this->obj());

                bool related = false;
                for (auto t : node.tags()) {
                    if (predicate_->tags_are_related(objTag, t)) {
                        related = true;
                        break;
                    }
                }

                if (!related) return false;
            }

            return this->obj().bound().intersects(node.bound());
        }

        template <class Object>
        Integer tag(const Object &) const {
            // FIXME figure out why obj is ignored and add a comment here
            return this->obj().tag();
        }

        template <class Object>
        Integer tag(const std::shared_ptr<Object> &) const {
            // FIXME figure out why obj is ignored and add a comment here
            return this->obj()->tag();
        }

        virtual bool match(DataType &object) const {
            return NTreeWithSpanMatch<Tree>::match(object) &&
                   predicate_->tags_are_related(this->obj().tag(), object.tag());
        }

        NTreeWithTagsMatch(const DataType &obj, const std::shared_ptr<Predicate> &predicate)
            : NTreeWithSpanMatch<Tree>(obj), predicate_(predicate) {}

    private:
        std::shared_ptr<Predicate> predicate_;
    };
}  // namespace moonolith

#endif  // MOONOLITH_NTREE_WITH_TAGS_MATCH_HPP
