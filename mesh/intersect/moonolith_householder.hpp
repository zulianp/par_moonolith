#ifndef MOONOLITH_HOUSEHOLDER_HPP
#define MOONOLITH_HOUSEHOLDER_HPP

#include "par_moonolith_config.hpp"
#include "moonolith_vector.hpp"
#include "moonolith_expanding_array.hpp"
#include "moonolith_matrix.hpp"

#include <vector>
#include <cassert>

namespace moonolith {

	template<typename T, int Dim>
	void householder_reflection(const Vector<T, Dim> &vec, Storage<T> &result_matrix)
	{
		if(Dim == 2) {
			result_matrix.resize(2*2);

			//row 0
			result_matrix[0] = - 2.0 * vec[0] * vec[0] + 1.0;
			result_matrix[1] = - 2.0 * vec[0] * vec[1];


			//row 1
			result_matrix[2] = result_matrix[1];
			result_matrix[3] = - 2.0 * vec[1] * vec[1] + 1.0;
		} else {
			result_matrix.resize(3*3);

			//row 0
			result_matrix[0] = - 2.0 * vec[0] * vec[0] + 1.0;
			result_matrix[1] = - 2.0 * vec[0] * vec[1];
			result_matrix[2] = - 2.0 * vec[0] * vec[2];

			//row 1
			result_matrix[3] = result_matrix[1];
			result_matrix[4] = - 2.0 * vec[1] * vec[1] + 1.0;
			result_matrix[5] = - 2.0 * vec[1] * vec[2];
			
			//row 2
			result_matrix[6] = result_matrix[2];
			result_matrix[7] = result_matrix[5];
			result_matrix[8] = - 2.0 * vec[2] * vec[2] + 1.0;

			assert(result_matrix[3] + 2.0 * vec[1] * vec[0] < static_cast<T>(1e-6));
			assert(result_matrix[6] + 2.0 * vec[2] * vec[0] < static_cast<T>(1e-6));
			assert(result_matrix[7] + 2.0 * vec[2] * vec[1] < static_cast<T>(1e-6));

		}
	}


	template<typename T>
	void householder_reflection(const Vector<T, 2> &vec, std::array<T, 4> &result_matrix)
	{
		//row 0
		result_matrix[0] = - 2.0 * vec[0] * vec[0] + 1.0;
		result_matrix[1] = - 2.0 * vec[0] * vec[1];

		//row 1
		result_matrix[2] = result_matrix[1];
		result_matrix[3] = - 2.0 * vec[1] * vec[1] + 1.0;
	}

	template<typename T>
	void householder_reflection(const Vector<T, 3> &vec, std::array<T, 9> &result_matrix)
	{
		//row 0
		result_matrix[0] = - 2.0 * vec[0] * vec[0] + 1.0;
		result_matrix[1] = - 2.0 * vec[0] * vec[1];
		result_matrix[2] = - 2.0 * vec[0] * vec[2];

		//row 1
		result_matrix[3] = result_matrix[1];
		result_matrix[4] = - 2.0 * vec[1] * vec[1] + 1.0;
		result_matrix[5] = - 2.0 * vec[1] * vec[2];
		
		//row 2
		result_matrix[6] = result_matrix[2];
		result_matrix[7] = result_matrix[5];
		result_matrix[8] = - 2.0 * vec[2] * vec[2] + 1.0;

		assert(result_matrix[3] + 2.0 * vec[1] * vec[0] < static_cast<T>(1e-6));
		assert(result_matrix[6] + 2.0 * vec[2] * vec[0] < static_cast<T>(1e-6));
		assert(result_matrix[7] + 2.0 * vec[2] * vec[1] < static_cast<T>(1e-6));
	}


	template<typename T, int DimIn, int DimOut>
	static void mat_vec_mul(const Storage<T> &matrix, const Vector<T, DimIn> &vector, Vector<T, DimOut> &result)
	{	
		for(int i = 0; i < DimOut; ++i) {
			const auto offset = i * DimIn;
			result[i] = matrix[offset] * vector[0];

			for(int j = 1; j < DimIn; ++j) {
				result[i] += matrix[offset + j] * vector[j];
			}
		}
	}

	template<typename T, int Dim>
	class HouseholderTransformation {
	public:

		HouseholderTransformation() {}

		HouseholderTransformation(const Vector<T, Dim> &n)
		{
			householder_reflection(n, trafo_.values);
		}

		void identity()
		{
			std::fill(trafo_.values.begin(), trafo_.values.end(), 0.);
			
			for(int i = 0; i < Dim; ++i) {
				(*this)(i, i) = 1.0;
			}
		}

		void init(const Vector<T, Dim> &n)
		{
			householder_reflection(n, trafo_.values);
		}

		void flip(int coord)
		{
			std::fill(trafo_.values.begin(), trafo_.values.end(), 0.);

			for(int i = 0; i < Dim; ++i) {
				trafo_.values[i * Dim + i] = 1;
			}

			trafo_.values[coord * Dim + coord] = -1;
		}

		template<int DimIn, int DimOut>
		inline void apply(const Vector<T, DimIn> &in, Vector<T, DimOut> &out) const
		{
			for(int i = 0; i < DimOut; ++i) {
				const auto offset = i * Dim;
				out[i] = trafo_.values[offset] * in[0];

				for(int j = 1; j < DimIn; ++j) {
					out[i] += trafo_.values[offset + j] * in[j];
				}
			}
		}

        inline const T &operator()(const int i, const int j) const
        {
            return trafo_(i, j);
        }

        inline T &operator()(const int i, const int j)
        {
            return trafo_(i, j);
        }

        const Matrix<T, Dim, Dim> &trafo() const
        {
        	return trafo_;
        }


	private:
		Matrix<T, Dim, Dim> trafo_;
	};

	template<typename T, int Dim>
	inline T measure(const HouseholderTransformation<T, Dim> &H)
	{
		return measure(H.trafo());
	}
}

#endif //MOONOLITH_HOUSEHOLDER_HPP
