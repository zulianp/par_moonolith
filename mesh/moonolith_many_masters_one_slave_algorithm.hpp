#ifndef MOONOLITH_MANY_MASTERS_ONE_SLAVE_ALGORITHM_HPP
#define MOONOLITH_MANY_MASTERS_ONE_SLAVE_ALGORITHM_HPP

#include "moonolith_base.hpp"
#include "par_moonolith_instance.hpp"

#include "moonolith_aabb.hpp"
#include "moonolith_asynch_lookup_table_builder.hpp"
#include "moonolith_bounding_volume_with_span.hpp"
#include "moonolith_check_can_refine.hpp"
#include "moonolith_collection_traits.hpp"
#include "moonolith_describe.hpp"
#include "moonolith_exchange.hpp"
#include "moonolith_lookup_table_builder.hpp"
#include "moonolith_n_tree_bound_synchronize.hpp"
#include "moonolith_n_tree_mutator_factory.hpp"
#include "moonolith_n_tree_with_span_mutator_factory.hpp"
#include "moonolith_n_tree_with_tags_mutator_factory.hpp"
#include "moonolith_tree.hpp"
// #include "moonolith_test_trees.hpp"
#include "moonolith_element_adapter.hpp"
#include "moonolith_make_unique.hpp"
#include "moonolith_polygon.hpp"
#include "moonolith_search_radius.hpp"

#include <algorithm>
#include <cassert>
#include <map>
#include <numeric>

namespace moonolith {

    // FIXME add concept of global handle and local handle
    template <Integer Dimension_, class Collection>
    class ManyMastersOneSlaveAlgorithm {
    public:
        typedef moonolith::AABBWithKDOPSpan<Dimension_, Real> Bound;
        using CM = moonolith::CollectionManager<Collection>;
        using Elem = typename CM::Elem;
        using Adapter = moonolith::ElementAdapter<Bound, Collection>;

        struct Traits {
            enum { Dimension = Dimension_ };

            typedef moonolith::AABBWithKDOPSpan<Dimension, Real> Bound;
            typedef Adapter DataType;
        };

        class SlaveBasedTree : public Tree<Traits> {
        public:
            static std::shared_ptr<SlaveBasedTree> New(const std::shared_ptr<Predicate> &pred) {
                std::shared_ptr<SlaveBasedTree> ret = std::make_shared<SlaveBasedTree>();
                // std::shared_ptr< NTreeWithSpanMutatorFactory<SlaveBasedTree> > factory = std::make_shared<
                // NTreeWithSpanMutatorFactory<SlaveBasedTree> >();
                std::shared_ptr<NTreeWithTagsMutatorFactory<SlaveBasedTree>> factory =
                    std::make_shared<NTreeWithTagsMutatorFactory<SlaveBasedTree>>(pred);

                factory->set_refine_params(20, 6);
                ret->set_mutator_factory(factory);
                return ret;
            }

            SlaveBasedTree() {}
        };

        void add_master_and_slave_pairing(const Integer master, const Integer slave) { predicate->add(master, slave); }

        void add_elem(const Adapter &a) { (*tree) += a; }

        void add_elem(const Collection *collection,
                      const Integer element_handle,
                      const Integer handle,
                      const Integer tag,
                      const Real blow_up) {
            (*tree) += Adapter(collection, element_handle, handle, tag, blow_up);
        }

        bool compute(const std::function<bool(const Storage<Adapter> &, const Adapter &)> &f) {
            Storage<Adapter> masters;

            Integer found_matches = 0, false_positives = 0;
            for (Integer i = 0; i < tree->memory().n_data(); ++i) {
                if (candidates[i].empty()) continue;

                auto &slave = tree->data(i);

                for (auto c : candidates[i]) {
                    masters.push_back(tree->data(c));
                }

                if (f(masters, slave)) {
                    ++found_matches;
                } else {
                    ++false_positives;
                }

                masters.clear();
            }

            if (Moonolith::instance().verbose()) {
                logger() << "found_matches: " << found_matches << " false positives: " << false_positives << std::endl;
            }

            return found_matches > 0;
        }

        bool compute(const std::function<bool(const Adapter &, const Adapter &)> &f) {
            Integer found_matches = 0, false_positives = 0;
            for (Integer i = 0; i < tree->memory().n_data(); ++i) {
                if (candidates[i].empty()) continue;

                auto &slave = tree->data(i);

                for (auto c : candidates[i]) {
                    auto &master = tree->data(c);

                    if (f(master, slave)) {
                        ++found_matches;
                    } else {
                        ++false_positives;
                    }
                }
            }

            if (Moonolith::instance().verbose()) {
                logger() << "found_matches: " << found_matches << " false positives: " << false_positives << std::endl;
            }

            return found_matches > 0;
        }

        bool init_simple(const Collection &master, const Collection &slave, const Real blow_up) {
            return init_simple(master, slave, std::make_shared<SearchRadius<Real>>(blow_up));
        }

        bool init_simple(const Collection &master,
                         const Collection &slave,
                         const std::vector<std::pair<int, int>> &master_slave_tags,
                         const Real blow_up) {
            return init_simple(master, slave, master_slave_tags, std::make_shared<SearchRadius<Real>>(blow_up));
        }

        bool init_simple(const Collection &master,
                         const Collection &slave,
                         const std::shared_ptr<SearchRadius<Real>> &search_radius) {
            // search_radius_ = search_radius;

            auto master_end = cm->elements_end(master);
            auto master_begin = cm->elements_begin(master);

            add_master_and_slave_pairing(0, 1);

            tree->reserve(cm->n_elements(master) + cm->n_elements(slave));

            Integer idx = 0;
            for (auto it = master_begin; it != master_end; ++it, ++idx) {
                if (cm->skip(master, it)) continue;

                add_elem(&master, cm->handle(master, it), idx, 0, search_radius->get(0));
            }

            auto slave_begin = cm->elements_begin(slave);
            auto slave_end = cm->elements_end(slave);

            idx = 0;
            for (auto it = slave_begin; it != slave_end; ++it, ++idx) {
                if (cm->skip(slave, it)) continue;

                add_elem(&slave, cm->handle(slave, it), idx, 1, search_radius->get(1));
            }

            // return aux_init(master, slave, search_radius);

            //////////////////////////////////////////////////////
            //////////////////////////////////////////////////////

            tree->root()->bound().static_bound().enlarge(1e-8);

            if (comm.size() > 1) {
                build_look_up_table();

                std::vector<std::vector<Integer>> outgoing(comm.size());

                const Integer n_data2proc = static_cast<Integer>(table->data_2_proc().size());
                for (Integer d = 0; d < n_data2proc; ++d) {
                    const auto &data = tree->data(DataHandle(d));

                    for (auto r : table->data_2_proc()[d]) {
                        if (r == comm.rank()) continue;

                        if (predicate->is_master(data.tag())) {
                            auto master_id = data.handle();
                            outgoing[r].push_back(master_id);
                        }
                    }
                }

                std::vector<ByteInputBuffer> recv_buffers(comm.size());
                std::vector<ByteOutputBuffer> send_buffers(comm.size());

                for (Integer i = 0; i < comm.size(); ++i) {
                    const auto &out_i = outgoing[i];
                    if (out_i.empty()) continue;
                    cm->serialize(master, std::begin(out_i), std::end(out_i), send_buffers[i]);
                }

                const Integer n_connections = comm.i_send_recv_all(send_buffers, recv_buffers);

                remote_collections.clear();

                for (Integer i = 0; i < n_connections; ++i) {
                    Integer rank, index;
                    while (!comm.test_recv_any(&rank, &index)) {
                    }
                    ByteInputStream &is = recv_buffers[rank];
                    auto &remote_master = *(remote_collections[rank] = cm->build(is));

                    // add remote elements to local tree
                    auto remote_master_end = cm->elements_end(remote_master);
                    auto remote_master_begin = cm->elements_begin(remote_master);

                    int idx = 0;
                    for (auto it = remote_master_begin; it != remote_master_end; ++it, ++idx) {
                        add_elem(&remote_master, cm->handle(remote_master, it), idx, 0, search_radius->get(0));
                    }
                }

                comm.wait_all();
            }

            // refine tree where necessary
            tree->refine();

            ////////////////////////////////////////////////////////////////////////

            candidates.clear();
            candidates.resize(tree->memory().n_data());

            Integer n_candidates = 0;
            Integer n_isect_candidates = 0;

            for (auto node_ptr : tree->memory().nodes()) {
                // must compute intersection for related data
                if (node_ptr->has_data() && predicate->tags_are_related(node_ptr->tags().begin(),
                                                                        node_ptr->tags().end(),
                                                                        node_ptr->tags().begin(),
                                                                        node_ptr->tags().end())) {
                    // find intersections
                    for (auto it = node_ptr->data_begin(); it != node_ptr->data_end(); ++it) {
                        auto &d = tree->data(*it);

                        for (auto it_other = it + 1; it_other != node_ptr->data_end(); ++it_other) {
                            auto &d_other = tree->data(*it_other);

                            if (d.bound().intersects(d_other.bound())) {
                                ++n_isect_candidates;

                                if (predicate->are_master_and_slave(d.tag(), d_other.tag())) {
                                    candidates[*it_other].insert(*it);
                                    ++n_candidates;
                                } else if (predicate->are_master_and_slave(d_other.tag(), d.tag())) {
                                    candidates[*it].insert(*it_other);
                                    ++n_candidates;
                                }
                            }
                        }
                    }
                }
            }

            return n_candidates > 0;
        }

        bool init_simple(const Collection &master,
                         const Collection &slave,
                         const std::vector<std::pair<int, int>> &master_slave_tags,
                         const std::shared_ptr<SearchRadius<Real>> &search_radius) {
            auto master_end = cm->elements_end(master);
            auto master_begin = cm->elements_begin(master);

            for (const auto &mst : master_slave_tags) {
                add_master_and_slave_pairing(mst.first, mst.second);
            }

            tree->reserve(cm->n_elements(master) + cm->n_elements(slave));

            Integer idx = 0;
            for (auto it = master_begin; it != master_end; ++it, ++idx) {
                if (cm->skip(master, it)) continue;

                const auto tag = cm->tag(master, it);
                if (predicate->select(tag)) {
                    add_elem(&master, cm->handle(master, it), idx, tag, search_radius->get(0));
                }
            }

            auto slave_begin = cm->elements_begin(slave);
            auto slave_end = cm->elements_end(slave);

            idx = 0;
            for (auto it = slave_begin; it != slave_end; ++it, ++idx) {
                if (cm->skip(slave, it)) continue;

                const auto tag = cm->tag(slave, it);
                if (predicate->select(tag)) {
                    add_elem(&slave, cm->handle(slave, it), idx, tag, search_radius->get(1));
                }
            }

            return aux_init(master, slave, search_radius);
        }

        bool aux_init(const Collection &master,
                      const Collection &slave,
                      const std::shared_ptr<SearchRadius<Real>> &search_radius) {
            //////////////////////////////////////////////////////
            //////////////////////////////////////////////////////

            tree->root()->bound().static_bound().enlarge(1e-8);

            if (comm.size() > 1) {
                build_look_up_table();

                std::vector<std::vector<Integer>> outgoing(comm.size());

                const Integer n_data2proc = static_cast<Integer>(table->data_2_proc().size());
                for (Integer d = 0; d < n_data2proc; ++d) {
                    const auto &data = tree->data(DataHandle(d));

                    for (auto r : table->data_2_proc()[d]) {
                        if (r == comm.rank()) continue;

                        if (predicate->is_master(data.tag())) {
                            auto master_id = data.handle();
                            outgoing[r].push_back(master_id);
                        }
                    }
                }

                std::vector<ByteInputBuffer> recv_buffers(comm.size());
                std::vector<ByteOutputBuffer> send_buffers(comm.size());

                for (Integer i = 0; i < comm.size(); ++i) {
                    const auto &out_i = outgoing[i];
                    if (out_i.empty()) continue;
                    cm->serialize(master, std::begin(out_i), std::end(out_i), send_buffers[i]);
                }

                const Integer n_connections = comm.i_send_recv_all(send_buffers, recv_buffers);

                remote_collections.clear();

                for (Integer i = 0; i < n_connections; ++i) {
                    Integer rank, index;
                    while (!comm.test_recv_any(&rank, &index)) {
                    }
                    ByteInputStream &is = recv_buffers[rank];
                    auto &remote_master = *(remote_collections[rank] = cm->build(is));

                    // add remote elements to local tree
                    auto remote_master_end = cm->elements_end(remote_master);
                    auto remote_master_begin = cm->elements_begin(remote_master);

                    int idx = 0;
                    for (auto it = remote_master_begin; it != remote_master_end; ++it, ++idx) {
                        const auto tag = cm->tag(remote_master, it);
                        add_elem(&remote_master, cm->handle(remote_master, it), idx, tag, search_radius->get(0));
                    }
                }

                comm.wait_all();
            }

            // refine tree where necessary
            tree->refine();

            ////////////////////////////////////////////////////////////////////////

            candidates.clear();
            candidates.resize(tree->memory().n_data());

            Integer n_candidates = 0;
            Integer n_isect_candidates = 0;

            for (auto node_ptr : tree->memory().nodes()) {
                // must compute intersection for related data
                if (node_ptr->has_data() && predicate->tags_are_related(node_ptr->tags().begin(),
                                                                        node_ptr->tags().end(),
                                                                        node_ptr->tags().begin(),
                                                                        node_ptr->tags().end())) {
                    // find intersections
                    for (auto it = node_ptr->data_begin(); it != node_ptr->data_end(); ++it) {
                        auto &d = tree->data(*it);

                        for (auto it_other = it + 1; it_other != node_ptr->data_end(); ++it_other) {
                            auto &d_other = tree->data(*it_other);

                            if (d.bound().intersects(d_other.bound())) {
                                ++n_isect_candidates;

                                if (predicate->are_master_and_slave(d.tag(), d_other.tag())) {
                                    candidates[*it_other].insert(*it);
                                    ++n_candidates;
                                } else if (predicate->are_master_and_slave(d_other.tag(), d.tag())) {
                                    candidates[*it].insert(*it_other);
                                    ++n_candidates;
                                }
                            }
                        }
                    }
                }
            }

            return n_candidates > 0;
        }

        ManyMastersOneSlaveAlgorithm(Communicator &comm, const std::shared_ptr<CollectionManager<Collection>> &cm)
            : comm(comm), predicate(std::make_shared<moonolith::MasterAndSlave>()), cm(cm) {
            tree = SlaveBasedTree::New(predicate);
        }

    private:
        Communicator comm;
        std::shared_ptr<SlaveBasedTree> tree;
        std::shared_ptr<moonolith::MasterAndSlave> predicate;
        std::shared_ptr<LookUpTable> table;
        std::map<Integer, std::unique_ptr<Collection>> remote_collections;
        std::vector<std::set<Integer>> candidates;
        std::shared_ptr<CollectionManager<Collection>> cm;

        bool build_look_up_table() {
            table = std::make_shared<LookUpTable>();
            AsynchLookUpTableBuilder<SlaveBasedTree> builder;

            if (!builder.build_for(comm, *tree, table)) {
                return false;
            }

            assert(table->check_consistency(comm));
            return true;
        }
    };
}  // namespace moonolith

#endif  // MOONOLITH_MANY_MASTERS_ONE_SLAVE_ALGORITHM_HPP
