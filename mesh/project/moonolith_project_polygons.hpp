#ifndef MOONOLITH_PROJECT_POLYGONS_HPP
#define MOONOLITH_PROJECT_POLYGONS_HPP

#include "moonolith_empirical_tol.hpp"
#include "moonolith_householder.hpp"
#include "moonolith_intersect_polygons.hpp"
#include "moonolith_map_quadrature.hpp"
#include "moonolith_plane.hpp"
#include "moonolith_polygon.hpp"
#include "moonolith_triangulate.hpp"

namespace moonolith {

    template <typename T>
    class ProjectPolygons {
    public:
        using Polygon2 = moonolith::Polygon<T, 2>;
        using Vector2 = typename Polygon2::Vector;

        using Polygon3 = moonolith::Polygon<T, 3>;
        using Vector3 = typename Polygon3::Vector;
        using Plane3 = moonolith::Plane<T, 3>;

        ProjectPolygons(const T tol = GeometricTol<T>::value()) : tol_(tol), triangulate_poly(tol) {}

        // onto general plane
        bool project_intersect_and_map_quadrature(const Polygon3 &poly1,
                                                  const Polygon3 &poly2,
                                                  const Plane3 &plane,
                                                  // ref-quad-rule
                                                  const Quadrature2<T> &ref_q,
                                                  Quadrature3<T> &plane_q);

        T intersection_ratio_1;
        T intersection_ratio_2;

    private:
        void map_quadrature_rule(const Storage<Vector2> &q_points,
                                 const Storage<T> &q_weights,
                                 const T weight,
                                 const Storage<Polygon2> &domain_of_integration,
                                 const Storage<Storage<int>> &triangulations,
                                 Storage<Vector2> &composite_q_points,
                                 Storage<T> &composite_q_weights);

        // private:
        T tol_;
        HouseholderTransformation<T, 3> trafo;
        Vector3 np1;
        Vector2 u, v;
        Polygon2 triangle;
        Polygon2 projected_1, projected_2;
        Storage<Polygon2> result_2d;
        Storage<Vector2> composite_q_points_2d;
        Storage<Storage<int>> triangulations;

        IntersectPolygons<T, 2> isect_polygons;
        TriangulatePolygon<T> triangulate_poly;
    };

}  // namespace moonolith

#endif  // MOONOLITH_PROJECT_POLYGONS_HPP
