#include "moonolith_elem.hpp"

#include "moonolith_elem_segment.hpp"
#include "moonolith_elem_tetrahedron.hpp"
#include "moonolith_elem_triangle.hpp"

#include "moonolith_elem_hexahedron.hpp"
#include "moonolith_elem_quad.hpp"

#include "moonolith_elem_edge.hpp"
#include "moonolith_elem_edge1.hpp"
#include "moonolith_elem_edge2.hpp"
#include "moonolith_elem_edge3.hpp"

#include "moonolith_elem_tri.hpp"
#include "moonolith_elem_tri1.hpp"
#include "moonolith_elem_tri3.hpp"
#include "moonolith_elem_tri6.hpp"

#include "moonolith_elem_quad.hpp"
#include "moonolith_elem_quad1.hpp"
#include "moonolith_elem_quad4.hpp"
#include "moonolith_elem_quad8.hpp"
#include "moonolith_elem_quad9.hpp"

#include "moonolith_elem_tet.hpp"
#include "moonolith_elem_tet1.hpp"
#include "moonolith_elem_tet10.hpp"
#include "moonolith_elem_tet4.hpp"

#include "moonolith_elem_hex.hpp"
#include "moonolith_elem_hex1.hpp"
#include "moonolith_elem_hex27.hpp"
#include "moonolith_elem_hex8.hpp"

namespace moonolith {
    ////////////////////////////////////
    // P1
    // template class Segment<Real, 1, 1>;
    // template class Segment<Real, 1, 2>;
    // template class Segment<Real, 1, 3>;

    // template class Triangle<Real, 1, 2>;
    // template class Triangle<Real, 1, 3>;

    // template class Tetrahedron<Real, 1, 3>;
    // ////////////////////////////////////
    // //P2
    // template class Triangle<Real, 2, 2>;
    // template class Triangle<Real, 2, 3>;
    // ////////////////////////////////////

    // ////////////////////////////////////
    // //Q1
    // template class Quad<Real, 1, 2>;
    // template class Quad<Real, 1, 3>;
    // template class Hexahedron<Real, 1, 3>;

    template class Edge1<Real, 1>;
    template class Edge1<Real, 2>;
    template class Edge1<Real, 3>;

    template class Edge2<Real, 1>;
    template class Edge2<Real, 2>;
    template class Edge2<Real, 3>;

    template class Edge3<Real, 1>;
    template class Edge3<Real, 2>;
    template class Edge3<Real, 3>;

    template class Tri1<Real, 2>;
    template class Tri1<Real, 3>;

    template class Tri3<Real, 2>;
    template class Tri3<Real, 3>;

    template class Tri6<Real, 2>;
    template class Tri6<Real, 3>;

    template class Tet4<Real, 3>;
    template class Tet10<Real, 3>;

    template class Quad4<Real, 2>;
    template class Quad4<Real, 3>;

    template class Quad8<Real, 2>;
    template class Quad8<Real, 3>;

    template class Hex8<Real, 3>;
    template class Hex27<Real, 3>;
}  // namespace moonolith
