#ifndef MOONOLITH_PAR_L2_ASSEMBLER_NO_COVERING_HPP
#define MOONOLITH_PAR_L2_ASSEMBLER_NO_COVERING_HPP

#include "moonolith_l2_assembler_no_covering.hpp"
#include "moonolith_par_l2_transfer.hpp"

#include "moonolith_collection_manager.hpp"
#include "moonolith_expanding_array.hpp"
#include "moonolith_function_space.hpp"
#include "moonolith_gauss_quadrature_rule.hpp"
#include "moonolith_incomplete_intersection_remover.hpp"
#include "moonolith_keast_quadrature_rule.hpp"
#include "moonolith_map_quadrature.hpp"
#include "moonolith_map_quadrature_impl.hpp"
#include "moonolith_matrix_buffers.hpp"
#include "moonolith_matrix_inserter.hpp"
#include "moonolith_sparse_matrix.hpp"

#include "moonolith_many_masters_one_slave_algorithm.hpp"

namespace moonolith {

    template <typename T, Integer PhysicalDim, Integer MasterDim = PhysicalDim, Integer SlaveDim = PhysicalDim>
    class ParL2TransferNoCovering {
    public:
        using MeshT = moonolith::Mesh<T, PhysicalDim>;
        using SpaceT = moonolith::FunctionSpace<MeshT>;

        using SpaceAlgorithmT = moonolith::ManyMastersOneSlaveAlgorithm<PhysicalDim, SpaceT>;

        using ElemAdapter = typename SpaceAlgorithmT::Adapter;

        using MasterElem = moonolith::Elem<T, MasterDim, PhysicalDim>;
        using SlaveElem = moonolith::Elem<T, SlaveDim, PhysicalDim>;

        ParL2TransferNoCovering(Communicator &comm)
            : comm(comm), buffers(comm), local_assembler(), remove_incomplete_intersections_(false) {}

        void post_process(const SpaceT &master, const SpaceT &slave) {
            const Integer n_master_dofs = static_cast<Integer>(master.dof_map().n_local_dofs());
            const Integer n_slave_dofs = static_cast<Integer>(slave.dof_map().n_local_dofs());

            buffers.B.finalize(n_slave_dofs, n_master_dofs);
            buffers.D.finalize(n_slave_dofs, n_slave_dofs);
            buffers.Q.finalize(n_slave_dofs, n_slave_dofs);
        }

        bool assemble(const SpaceT &master, const SpaceT &slave, const T box_tol = 0.0) {
            Real elapsed = MPI_Wtime();

            SpaceAlgorithmT algo(comm, moonolith::make_unique<CollectionManager<SpaceT>>(comm, true));

            algo.init_simple(master, slave, box_tol);

            elapsed = MPI_Wtime() - elapsed;
            if (Moonolith::instance().verbose() && !comm.rank()) {
                logger() << "init: " << elapsed << std::endl;
            }

            ////////////////////////////////////////////////////
            /////////////////// pair-wise method ///////////////////////
            elapsed = MPI_Wtime();

            local_assembler.clear();

            algo.compute([&](const Storage<ElemAdapter> &master, const ElemAdapter &slave) -> bool {
                if (local_assembler.apply(master, slave, buffers)) {
                    return true;
                }

                return false;
            });

            Real vol = local_assembler.intersection_measure();

            comm.all_reduce(&vol, 1, MPISum());

            Real sum_mat_B = buffers.B.m_matrix.sum();
            Real sum_mat_D = buffers.D.m_matrix.sum();

            post_process(master, slave);

            elapsed = MPI_Wtime() - elapsed;
            if (Moonolith::instance().verbose() && !comm.rank()) {
                logger() << "time ParL2TransferNoCovering::assemble: " << elapsed << std::endl;
                logger() << "vol: " << vol << " sum(B): " << sum_mat_B << " sum(D): " << sum_mat_D << std::endl;
            }

            return vol > 0.0;
        }

        inline void remove_incomplete_intersections(const bool val) { remove_incomplete_intersections_ = val; }

        inline const SparseMatrix<T> &coupling_matrix() const { return buffers.B.get(); }

        inline const SparseMatrix<T> &mass_matrix() const { return buffers.D.get(); }

        inline const SparseMatrix<T> &transformation_matrix() const { return buffers.Q.get(); }

        Communicator &comm;
        MatrixBuffers<T> buffers;
        L2AssemblerNoCovering<MasterElem, SlaveElem> local_assembler;

    private:
        bool remove_incomplete_intersections_;
    };

}  // namespace moonolith

#endif  // MOONOLITH_PAR_L2_ASSEMBLER_NO_COVERING_HPP
