#ifndef MOONOLITH_LOWER_DIM_L2_ASSEMBLER_IMPL_HPP
#define MOONOLITH_LOWER_DIM_L2_ASSEMBLER_IMPL_HPP

#include "moonolith_lower_dim_l2_assembler.hpp"

#include "moonolith_assembly.hpp"
#include "moonolith_build_quadrature.hpp"
#include "moonolith_build_quadrature_non_convex.hpp"
#include "moonolith_elem_dual.hpp"
#include "moonolith_elem_shape.hpp"
#include "moonolith_fe.hpp"
#include "moonolith_iso_parametric_transform.hpp"
#include "moonolith_map_quadrature.hpp"
#include "moonolith_polygon.hpp"
#include "moonolith_project_convex_polygons.hpp"
#include "moonolith_project_line_segments.hpp"
#include "moonolith_project_polygons.hpp"
#include "moonolith_project_polylines.hpp"
#include "moonolith_shape.hpp"
#include "moonolith_transform.hpp"

#include "moonolith_elem_hexahedron.hpp"
#include "moonolith_elem_tetrahedron.hpp"
#include "moonolith_transformed_fe.hpp"

#include "moonolith_map_quadrature_impl.hpp"

#include <iostream>
#include <memory>
#include <string>

namespace moonolith {

    template <class MasterGeom,
              class SlaveGeom,
              class QReferenceMaster,
              class QReferenceSlave,
              class QPhysical,
              class QDomainOfIntegration,
              class MasterTransform,
              class SlaveTransform>
    bool BuildLowerDimQuadratureBase<MasterGeom,
                                     SlaveGeom,
                                     QReferenceMaster,
                                     QReferenceSlave,
                                     QPhysical,
                                     QDomainOfIntegration,
                                     MasterTransform,
                                     SlaveTransform>::compute() {
        assert(trafo_master);
        assert(trafo_slave);

        if (!build_q.apply(q_rule, master, slave, q_physical)) {
            return false;
        }

        const auto n_qp = q_physical.n_points();

        q_master.resize(n_qp);
        q_slave.resize(n_qp);

        // const auto vol_master = measure(master);
        // const auto vol_slave  = measure(slave);

        bool ok_master = true, ok_slave = true, ok = true;
        for (Integer k = 0; k < n_qp; ++k) {
            ok_master = trafo_master->apply_inverse(q_physical.point(k), q_master.point(k));
            assert(ok_master);
            ok_slave = trafo_slave->apply_inverse(q_physical.point(k), q_slave.point(k));
            assert(ok_slave);

            q_master.weights[k] = q_physical.weights[k];  /// vol_master;
            q_slave.weights[k] = q_physical.weights[k];   /// vol_slave;

            if (!ok_master || !ok_slave) {
                ok = false;
                break;
            }
        }

        return ok;
    }

    template <class MasterElem, class SlaveElem>
    bool LowerDimL2Transfer<MasterElem, SlaveElem>::assemble(MasterElem &master, SlaveElem &slave) {
        const bool is_affine = master.is_affine() && slave.is_affine();

        if (is_affine) {
            return assemble_affine(master, slave);
        } else {
            assert(false && "not implemented yet");
            return false;
            // return assemble_warped(master, slave);
        }

        return false;
    }

    template <class MasterElem, class SlaveElem>
    bool LowerDimL2Transfer<MasterElem, SlaveElem>::assemble_affine(MasterElem &master, SlaveElem &slave) {
        assert(!algo_.q_rule.empty() || (SlaveDim == 1 && MasterDim == 1));

        make(master, algo_.master);
        make(slave, algo_.slave);

        make_transform(master, *trafo_master);
        make_transform(slave, *trafo_slave);

        algo_.trafo_master = trafo_master;
        algo_.trafo_slave = trafo_slave;

        if (algo_.compute()) {
            assemble(master, slave, algo_.q_master, algo_.q_slave);

            return true;
        } else {
            return false;
        }
    }

    // template<class MasterElem, class SlaveElem>
    // bool LowerDimL2Transfer<MasterElem, SlaveElem>::assemble_warped(
    //                                  MasterElem &master,
    //                                  SlaveElem  &slave
    //                                  )
    // {
    //     assert(!algo_.q_rule.empty());

    //     make(master, algo_.master);
    //     make(slave,  algo_.slave);

    //     //polymorphic transformation
    //     make_transform(master, iso_trafo_master);
    //     make_transform(slave,  iso_trafo_slave);

    //     algo_.trafo_master = iso_trafo_master;
    //     algo_.trafo_slave  = iso_trafo_slave;

    //     if(algo_.compute()) {
    //         assemble(
    //                  master,
    //                  slave,
    //                  algo_.q_master,
    //                  algo_.q_slave
    //                  );

    //         return true;
    //     } else {
    //         return false;
    //     }
    // }

    template <class TrialFE, class TestFE, class Weights, class Matrix>
    static void assemble_mass_matrix_with_weights(TrialFE &trial, TestFE &test, Weights &w, Matrix &mat) {
        Resize<Matrix>::apply(mat, test.n_shape_functions(), trial.n_shape_functions());
        mat.zero();

        assert(trial.n_shape_functions() == mat.cols());
        assert(test.n_shape_functions() == mat.rows());

        const int n_qps = test.n_quad_points();

        assert(n_qps > 0);

        for (int i = 0; i < mat.rows(); ++i) {
            for (int j = 0; j < mat.cols(); ++j) {
                for (int k = 0; k < n_qps; ++k) {
                    mat(i, j) += test.fun(i, k) * trial.fun(j, k) * w[k];
                }
            }
        }
    }

    template <class MasterElem, class SlaveElem>
    void LowerDimL2Transfer<MasterElem, SlaveElem>::assemble(MasterElem &master,
                                                             SlaveElem &slave,
                                                             const Quadrature<T, MasterDim> &q_master,
                                                             const Quadrature<T, SlaveDim> &q_slave) {
        master_fe_.init(master, q_master, true, false, false);
        slave_fe_.init(slave, q_slave, true, false, false);
        // dual_fe_.init(slave_fe_);
        // trafo_fe_.init(slave_fe_);

        assemble_mass_matrix_with_weights(master_fe_, slave_fe_, q_slave.weights, coupling_mat_);
        assemble_mass_matrix_with_weights(slave_fe_, slave_fe_, q_slave.weights, mass_mat_);

        // trafo_ = trafo_fe_.transformation();
        //!!! The global transformation needs the transpose of the basis transform
        // make_transpose(trafo_);

        intersection_measure_ += mass_mat_.sum();
        assert(intersection_measure_ > 0.0);
    }

    template <class MasterElem, class SlaveElem>
    LowerDimL2Transfer<MasterElem, SlaveElem>::LowerDimL2Transfer() : intersection_measure_(0) {
        trafo_master = std::make_shared<MasterAffineTrafo>();
        trafo_slave = std::make_shared<SlaveAffineTrafo>();
    }

    template <class MasterElem, class SlaveElem>
    void LowerDimL2Transfer<MasterElem, SlaveElem>::describe(std::ostream &os) const {
        os << "master:            " << measure(algo_.master) << std::endl;
        os << "slave:             " << measure(algo_.slave) << std::endl;
        os << "q_master:          " << measure(algo_.q_master) << std::endl;
        os << "q_slave:           " << measure(algo_.q_slave) << std::endl;
        os << "intersection_measure: " << intersection_measure_ << std::endl;
    }

}  // namespace moonolith

#endif  // MOONOLITH_LOWER_DIM_L2_ASSEMBLER_IMPL_HPP
