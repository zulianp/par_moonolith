#ifndef MOONOLITH_CHECK_STREAM_HPP
#define MOONOLITH_CHECK_STREAM_HPP 

#include "par_moonolith_config.hpp"


#ifndef MOONOLITH_ENABLE_CHECK_STREAM
#define CHECK_STREAM_READ(arg, is)
#define CHECK_STREAM_WRITE(arg, os)

#define CHECK_STREAM_READ_BEGIN(arg, is)
#define CHECK_STREAM_WRITE_BEGIN(arg, os)
#define CHECK_STREAM_READ_END(arg, is)
#define CHECK_STREAM_WRITE_END(arg, os)

#else
#define CHECK_STREAM_READ(arg, is) moonolith::CheckStream::read(arg, is, __FILE__, __LINE__)
#define CHECK_STREAM_WRITE(arg, os) moonolith::CheckStream::write(arg, os)

#define CHECK_STREAM_READ_BEGIN(arg, is) CHECK_STREAM_READ(arg "_begin", is)
#define CHECK_STREAM_WRITE_BEGIN(arg, os) CHECK_STREAM_WRITE(arg "_begin", os) 
#define CHECK_STREAM_READ_END(arg, is) CHECK_STREAM_READ(arg "_end", is)
#define CHECK_STREAM_WRITE_END(arg, os) CHECK_STREAM_WRITE(arg "_end", os) 

#endif

#include "moonolith_forward_declarations.hpp"
#include <string>

namespace moonolith {
	
	class CheckStream {
	public:
		static void read(const std::string &expected, InputStream &is, const std::string &file, const int line);
		static void write(const std::string &expected, OutputStream &os);
	};
}

#endif //MOONOLITH_CHECK_STREAM_HPP


