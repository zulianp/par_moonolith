#ifndef MOONOLITH_KEAST_QUADRATURE_RULE_HPP
#define MOONOLITH_KEAST_QUADRATURE_RULE_HPP

#include "moonolith_map_quadrature.hpp"

namespace moonolith {

    class Keast {
    public:

        template<typename T>
        static bool get(const Integer order, Quadrature<T, 3> &q)
        {
            switch(order) {
                case 6: {
                    
                    q.points = {
                        { 0.3561913862225449, 0.2146028712591517, 0.2146028712591517 },
                        { 0.2146028712591517, 0.2146028712591517, 0.2146028712591517 },
                        { 0.2146028712591517, 0.2146028712591517, 0.3561913862225449 },
                        { 0.2146028712591517, 0.3561913862225449, 0.2146028712591517 },
                        { 0.8779781243961660, 0.0406739585346113, 0.0406739585346113 },
                        { 0.0406739585346113, 0.0406739585346113, 0.0406739585346113 },
                        { 0.0406739585346113, 0.0406739585346113, 0.8779781243961660 },
                        { 0.0406739585346113, 0.8779781243961660, 0.0406739585346113 },
                        { 0.0329863295731731, 0.3223378901422757, 0.3223378901422757 },
                        { 0.3223378901422757, 0.3223378901422757, 0.3223378901422757 },
                        { 0.3223378901422757, 0.3223378901422757, 0.0329863295731731 },
                        { 0.3223378901422757, 0.0329863295731731, 0.3223378901422757 },
                        { 0.2696723314583159, 0.0636610018750175, 0.0636610018750175 },
                        { 0.0636610018750175, 0.2696723314583159, 0.0636610018750175 },
                        { 0.0636610018750175, 0.0636610018750175, 0.2696723314583159 },
                        { 0.6030056647916491, 0.0636610018750175, 0.0636610018750175 },
                        { 0.0636610018750175, 0.6030056647916491, 0.0636610018750175 },
                        { 0.0636610018750175, 0.0636610018750175, 0.6030056647916491 },
                        { 0.0636610018750175, 0.2696723314583159, 0.6030056647916491 },
                        { 0.2696723314583159, 0.6030056647916491, 0.0636610018750175 },
                        { 0.6030056647916491, 0.0636610018750175, 0.2696723314583159 },
                        { 0.0636610018750175, 0.6030056647916491, 0.2696723314583159 },
                        { 0.2696723314583159, 0.0636610018750175, 0.6030056647916491 },
                        { 0.6030056647916491, 0.2696723314583159, 0.0636610018750175 }
                    };

                    q.weights = {
                        0.0399227502581679,
                        0.0399227502581679,
                        0.0399227502581679,
                        0.0399227502581679,
                        0.0100772110553207,
                        0.0100772110553207,
                        0.0100772110553207,
                        0.0100772110553207,
                        0.0553571815436544,
                        0.0553571815436544,
                        0.0553571815436544,
                        0.0553571815436544,
                        0.0482142857142857,
                        0.0482142857142857,
                        0.0482142857142857,
                        0.0482142857142857,
                        0.0482142857142857,
                        0.0482142857142857,
                        0.0482142857142857,
                        0.0482142857142857,
                        0.0482142857142857,
                        0.0482142857142857,
                        0.0482142857142857,
                        0.0482142857142857 
                    };
                }

                default: break;
            }


            return false;
        }

    };
}

#endif //MOONOLITH_KEAST_QUADRATURE_RULE_HPP
