#ifndef ASSEMBLY_MACROS_CL
#define ASSEMBLY_MACROS_CL 

///Available indices i (test), j (trial), q (quadrature), grad_q (quadrature offseted for gradients)
#define assemble_ijq(trial, test, matrix, operator_code)						\
{																				\
	if(trial == test) {															\
		const SizeType n_shape_functions = test->n_shape_functions;				\
		const SizeType n_quad_points = test->n_quad_points;						\
		for(SizeType i = 0; i < n_shape_functions; ++i) {						\
			const SizeType offset = i * n_shape_functions;						\
			const SizeType diag   = offset + i;									\
			matrix[diag] = 0;													\
			for(SizeType q = 0; q < n_quad_points; ++q) {						\
				const SizeType j = i;											\
				const SizeType grad_q = q * test->n_dims; (void) grad_q; 		\
				matrix[diag] += (operator_code);								\
			}																	\
			for(SizeType j = i+1; j < n_shape_functions; ++j) {					\
				const SizeType offset_j = offset + j;							\
				matrix[offset_j] = 0;											\
				for(SizeType q = 0; q < n_quad_points; ++q) {					\
					const SizeType grad_q = q * test->n_dims; (void) grad_q; 	\
					matrix[offset_j] += (operator_code);						\
				}																\
				matrix[j * n_shape_functions + i] = matrix[offset_j];			\
			}																	\
		}																		\
	} else {																	\
		for(SizeType i = 0; i < test->n_shape_functions; ++i) {					\
			const SizeType offset = i * trial->n_shape_functions;				\
			for(SizeType j = 0; j < trial->n_shape_functions; ++j) {			\
				const SizeType offset_j = offset + j;							\
				matrix[offset_j] = 0;											\
				for(SizeType q = 0; q < test->n_quad_points; ++q) {				\
					const SizeType grad_q = q * test->n_dims; (void) grad_q; 	\
					matrix[offset_j] += (operator_code);						\
				}																\
			}																	\
		}																		\
	}																			\
}	
		
#define assemble_block_ij(trial, test, matrix, operator_code)									\
{																								\
	SizeType index = 0;																			\
	for(SizeType block_i = 0; block_i < test->n_shape_functions; ++block_i) {					\
		for(SizeType block_j = 0; block_j < trial->n_shape_functions; ++block_j) {				\
			for(SizeType block_ik = 0; block_ik < test->n_dims; ++block_ik) {					\
				const SizeType i = block_get_i(block_i, test->n_dims, block_ik); 				\
				for(SizeType block_jl = 0; block_jl < trial->n_dims; ++block_jl) {				\
					const SizeType j = block_get_j(block_j, trial->n_dims, block_jl);			\
					matrix[index++] += (operator_code);											\
				}																				\
			}																					\
		}																						\
	}																							\
}	

#define symmetric_assemble_block_ij(elem, matrix, operator_code)                                \
{                                                                                               \
    const SizeType n_dims = elem->n_dims;                                                       \
    const SizeType n_dims2 = n_dims * n_dims;                                                   \
    const SizeType stride_i = elem->n_shape_functions * n_dims2;                                \
    for(SizeType block_i = 0; block_i < elem->n_shape_functions; ++block_i) {                   \
        const SizeType offset_block_i  = block_i * stride_i;                                    \
        const SizeType offset_block_j  = offset_block_i + block_i * n_dims2;                    \
        for(SizeType block_ik = 0; block_ik < elem->n_dims; ++block_ik) {                       \
            const SizeType offset_entry_ij_k = offset_block_j + block_ik * n_dims;              \
            const SizeType i = block_get_i(block_i, elem->n_dims, block_ik);                    \
            {                                                                                   \
                const SizeType j = block_get_j(block_i, elem->n_dims, block_ik);                \
                const SizeType diag_kk = offset_entry_ij_k + block_ik;                          \
                matrix[diag_kk] += (operator_code);                                             \
            }                                                                                   \
            for(SizeType block_jl = block_ik+1; block_jl < elem->n_dims; ++block_jl) {          \
                const SizeType offset_entry_ij_l = offset_block_j + block_jl * n_dims;          \
                                                                                                \
                const SizeType diag_kl = offset_entry_ij_k + block_jl;                          \
                const SizeType diag_lk = offset_entry_ij_l + block_ik;                          \
                const SizeType j = block_get_j(block_i, elem->n_dims, block_jl);                \
                matrix[diag_kl] += (operator_code);                                             \
                matrix[diag_lk] = matrix[diag_kl];                                              \
            }                                                                                   \
        }                                                                                       \
        for(SizeType block_j = block_i+1; block_j < elem->n_shape_functions; ++block_j) {       \
            const SizeType offset_block_j = offset_block_i + block_j * n_dims2;                 \
            for(SizeType block_ik = 0; block_ik < elem->n_dims; ++block_ik) {                   \
                const SizeType offset_entry_ij_k = offset_block_j + block_ik * n_dims;          \
                const SizeType i = block_get_i(block_i, elem->n_dims, block_ik);                \
                                                                                                \
                for(SizeType block_jl = 0; block_jl < elem->n_dims; ++block_jl) {               \
                    const SizeType off_diag_kl = offset_entry_ij_k + block_jl;                  \
                    const SizeType j = block_get_j(block_j, elem->n_dims, block_jl);            \
                    matrix[off_diag_kl] += (operator_code);                                     \
                }                                                                               \
            }                                                                                   \
        }                                                                                       \
    }                                                                                           \
}

#endif  //ASSEMBLY_MACROS_CL

