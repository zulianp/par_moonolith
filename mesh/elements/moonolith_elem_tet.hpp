#ifndef MOONOLITH_ELEM_TET_HPP
#define MOONOLITH_ELEM_TET_HPP

#include "moonolith_elem.hpp"
#include "moonolith_convex_decomposition.hpp"
#include "moonolith_gauss_quadrature_rule.hpp"
#include "moonolith_polyhedral_element.hpp"

namespace moonolith {

    template<typename T, int Order_, int PhysicalDim_ = 3>
    class Tet : public PolyhedralElement<T, PhysicalDim_> {
    public:
        static const int Order = Order_;
        virtual ~Tet() {}
    };

    template<typename T, int Order, int PhysicalDim>
    class GaussQRule< Tet<T, Order, PhysicalDim> > {
    public:
        static bool get(const Integer order, Quadrature3<T> &q) {
            return Gauss::get(order, q);
        }
    };

}

#endif //MOONOLITH_ELEM_TET_HPP
