#include "moonolith_build_quadrature.hpp"
#include "moonolith_convex_decomposition.hpp"
#include "moonolith_elem_hexahedron.hpp"
#include "moonolith_elem_polymorphic.hpp"
#include "moonolith_elem_tetrahedron.hpp"
#include "moonolith_graham_scan.hpp"
#include "moonolith_intersect_polyhedra.hpp"
#include "moonolith_intersect_polyhedra_impl.hpp"
#include "moonolith_matlab_scripter.hpp"

// Remove warnings of deprecated functions since we are using them right now
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wextra"
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wpedantic"
#pragma GCC diagnostic ignored "-Wunused-parameter"

#include <cmath>
#include "opencl_adapter.hpp"

#include <fstream>
#include <iostream>
#include <vector>

#include "gtest/gtest.h"

#define MOONOLITH_HAVE_DOUBLE_PRECISION
#define DEFAULT_TOLLERANCE 1e-12

namespace test_ {
    class Intersector : public moonolith::OpenCLAdapter {
    public:
        // I do not know why the compiler wants this...
        template <typename T>
        inline static T sqrt(const T v) {
            return std::sqrt(v);
        }

#include "all_kernels.cl"
    };

    typedef test_::Intersector::PMesh CPolyhedron;
}  // namespace test_

static const int repeat_test = 1;

using namespace moonolith;

TEST(DeprecatedTest, intersect_convex_polyhedra_test) {
    using namespace test_;

    CPolyhedron poly1, poly2;

    poly1.n_elements = 6;
    poly1.n_nodes = 8;
    poly1.n_dims = 3;

    Storage<int> el_ptr = {
        0,   //[0]
        4,   //[1]
        8,   //[2]
        12,  //[3]
        16,  //[4]
        20,  //[5]
        24   //[6]
    };

    Storage<int> el_index = {
        0,  //[0]
        1,  //[1]
        5,  //[2]
        4,  //[3]
        1,  //[4]
        2,  //[5]
        6,  //[6]
        5,  //[7]
        3,  //[8]
        7,  //[9]
        6,  //[10]
        2,  //[11]
        0,  //[12]
        4,  //[13]
        7,  //[14]
        3,  //[15]
        2,  //[16]
        1,  //[17]
        0,  //[18]
        3,  //[19]
        6,  //[20]
        7,  //[21]
        4,  //[22]
        5   //[23]
    };

    Storage<Real> points = {
        -0.30000000000000004,  //[0]
        -0.30000000000000004,  //[1]
        0.30000000000000004,   //[2]
        -0.19999999999999996,  //[3]
        -0.30000000000000004,  //[4]
        0.30000000000000004,   //[5]
        -0.19999999999999996,  //[6]
        -0.19999999999999996,  //[7]
        0.30000000000000004,   //[8]
        -0.30000000000000004,  //[9]
        -0.19999999999999996,  //[10]
        0.30000000000000004,   //[11]
        -0.30000000000000004,  //[12]
        -0.30000000000000004,  //[13]
        0.39999999999999991,   //[14]
        -0.19999999999999996,  //[15]
        -0.30000000000000004,  //[16]
        0.39999999999999991,   //[17]
        -0.19999999999999996,  //[18]
        -0.19999999999999996,  //[19]
        0.39999999999999991,   //[20]
        -0.30000000000000004,  //[21]
        -0.19999999999999996,  //[22]
        0.39999999999999991    //[23]
    };

    poly1.type = 3;

    std::copy(std::begin(el_ptr), std::end(el_ptr), std::begin(poly1.el_ptr));
    std::copy(std::begin(el_index), std::end(el_index), std::begin(poly1.el_index));
    std::copy(std::begin(points), std::end(points), std::begin(poly1.points));

    /////////////////////////////////////////

    poly2.n_elements = 6;
    poly2.n_nodes = 8;
    poly2.n_dims = 3;

    el_ptr = {
        0,   //[0]
        4,   //[1]
        8,   //[2]
        12,  //[3]
        16,  //[4]
        20,  //[5]
        24   //[6]
    };

    el_index = {
        0,  //[0]
        1,  //[1]
        5,  //[2]
        4,  //[3]
        1,  //[4]
        2,  //[5]
        6,  //[6]
        5,  //[7]
        3,  //[8]
        7,  //[9]
        6,  //[10]
        2,  //[11]
        0,  //[12]
        4,  //[13]
        7,  //[14]
        3,  //[15]
        2,  //[16]
        1,  //[17]
        0,  //[18]
        3,  //[19]
        6,  //[20]
        7,  //[21]
        4,  //[22]
        5   //[23]
    };

    points = {
        -0.22651513583561636,  //[0]
        -0.22651513583561636,  //[1]
        0.29887968534141401,   //[2]
        -0.15241447521870757,  //[3]
        -0.22651513583561642,  //[4]
        0.300986262333859,     //[5]
        -0.15241447521870757,  //[6]
        -0.15241447521870757,  //[7]
        0.30309283932630399,   //[8]
        -0.22651513583561636,  //[9]
        -0.15241447521870757,  //[10]
        0.300986262333859,     //[11]
        -0.26504358749434054,  //[12]
        -0.26504358749434054,  //[13]
        0.35174008807660145,   //[14]
        -0.17834708691207959,  //[15]
        -0.26504358749434054,  //[16]
        0.35421713095038032,   //[17]
        -0.17834708691207959,  //[18]
        -0.17834708691207959,  //[19]
        0.35669417382415919,   //[20]
        -0.26504358749434054,  //[21]
        -0.17834708691207959,  //[22]
        0.35421713095038032    //[23]
    };

    poly2.type = 3;

    std::copy(std::begin(el_ptr), std::end(el_ptr), std::begin(poly2.el_ptr));
    std::copy(std::begin(el_index), std::end(el_index), std::begin(poly2.el_index));
    std::copy(std::begin(points), std::end(points), std::begin(poly2.points));

    // std::cout << "-----------------------------\n";
    // Intersector::p_mesh_print(&poly1);
    // std::cout << "-----------------------------\n";
    // Intersector::p_mesh_print(&poly2);
    // std::cout << "-----------------------------\n";

    CPolyhedron isect;

    for (int i = 0; i < repeat_test; ++i) {
        bool ok = Intersector::intersect_convex_polyhedra(poly1, poly2, &isect);
        ASSERT_TRUE(ok);
    }

    // Intersector::p_mesh_print(&isect);
    // std::cout << "-----------------------------\n";
}

#pragma GCC diagnostic pop
