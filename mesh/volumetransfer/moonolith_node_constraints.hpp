#ifndef MOONOLITH_NODE_CONSTRAINTS_HPP
#define MOONOLITH_NODE_CONSTRAINTS_HPP

namespace moonolith {

    /**
     * @brief Handling of possibly over-constrained nodes
     * Idea for 2D (in 3D we need to take care of edges too)
     * Overconstrained nodes are n-tuples (n>=3) of nodes that:
     * - have the same geometric location
     * - do not have the same id
     * - have at least one different role (roles are master and slave)
     * This nodes are tipically corners (X) of matching multiple sub-domains (here 1-4), e.g.,
     * ---------------------
     * |         |          |
     * |         |          |
     * |    1    |    2     |
     * |         |          |
     * |---------X----------|
     * |         |          |
     * |         |          |
     * |    3    |    4     |
     * |         |          |
     * ---------------------
     * There are two options for handling such cases
     * 1) ignore them and have a discontinous solution at that point
     * 2) use interpolation, one node is choosen to be master and the others to be slaves
     *
     * Both computations can be performed before the mortar assembly.
     * the elements connected to such nodes shall be removed from the mortar assembly routines.
     *
     * Algorithm overview
     * 1) detect nodes that are tagged for mortar and they represent a
     *    corner (node is connected to faces that are all on the boundary)
     *    and the dihedral angles can be used to check if it is an asperity (none of the faces should be aligned)
     * 2) similarly ƒor edges
     * 3) Elements connected to constrained edges or nodes shall be removed from the mortar assembly
     */
}

#endif //MOONOLITH_NODE_CONSTRAINTS_HPP
