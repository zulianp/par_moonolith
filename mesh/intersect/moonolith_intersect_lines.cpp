#include "moonolith_intersect_lines_impl.hpp"

namespace moonolith {
    template class IntersectLines<Real, 1>;
    template class IntersectLines<Real, 2>;
    template class IntersectLines<Real, 3>;
}  // namespace moonolith
