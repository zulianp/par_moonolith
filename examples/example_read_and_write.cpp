
// general
#include "par_moonolith.hpp"

// specific
#include "moonolith_matlab_scripter.hpp"
#include "moonolith_mesh.hpp"
#include "moonolith_mesh_io.hpp"
#include "moonolith_vtu_mesh_writer.hpp"

/// Moonolith is not a mesh manager so it does not provide IO or mesh generation
/// Typically Meshes are converted from the client code to the our format
/// Nevertheless, we have few functionalities for reading and writing for the purpose of debugging
/// Compile examples with `make moonolith_examples`
/// Run for instance with  `mpiexec -np 2 ./examples/example_read_and_write`
int main(int argc, char *argv[]) {
    using namespace moonolith;
    static const int Dim = 2;
    using MeshT = moonolith::Mesh<Real, Dim>;

    Moonolith::Init(argc, argv);

    // World by default
    Communicator comm;
    MeshT mesh(comm);

    std::string mesh_path = "../examples/data/triangle_4.tri";

    if (argc == 2) {
        mesh_path = argv[1];
        logger() << "using:\nmesh:\n" << mesh_path << std::endl;
    }

    //////

    TriMeshReader<MeshT> reader;
    if (!reader.read(mesh_path, mesh)) {
        error_logger() << "Could not read mesh" << std::endl;
        Moonolith::Abort();
    }

    // Visualize mesh with MatLab script
    MatlabScripter script;
    mesh.draw(script);
    script.save("mesh_" + std::to_string(comm.rank()) + ".m");

    // Save mesh in vtu format
    VTUMeshWriter<MeshT> vtu;
    vtu.write("mesh.vtu", mesh);

    const Integer n_nodes = mesh.n_nodes();
    std::vector<Real> x(n_nodes);

    for (Integer i = 0; i < n_nodes; ++i) {
        x[i] = mesh.point(i)(0);
    }

    vtu.write("mesh_x.vtu", mesh, x);

    return Moonolith::Finalize();
}
