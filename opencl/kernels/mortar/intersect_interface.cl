#ifndef INTERSECT_INTERFACES_CL
#define INTERSECT_INTERFACES_CL 

#include "kernel_base.cl"
#include "fe/fe_interfaces.cl"

#define MAX_N_HALF_SPACES  30
#define MAX_LOCAL_ELEMENTS 30
#define MAX_LOCAL_MESH_POINTS 120
#define MAX_LOCAL_EL_POINTERS (MAX_LOCAL_ELEMENTS+1)
#define MAX_LOCAL_EL_INDEX	  (MAX_LOCAL_ELEMENTS*4)
#define USE_OBJ_OUTPUT
#define MAX_N_ISECT_POINTS 40

// #define USE_TRIANGULATED_OBJ_OUTPUT

typedef struct {
	SizeType n_elements;
	SizeType n_nodes;
	SizeType n_dims;

	SizeType el_ptr   [MAX_LOCAL_EL_POINTERS];
	SizeType el_index [MAX_LOCAL_EL_INDEX];
	Scalar points 	  [MAX_LOCAL_MESH_POINTS * MAX_N_DIMS];

	KERNEL_DEBUG(bool is_new_element[MAX_LOCAL_ELEMENTS]);

	int type;
} PMesh;

#define P_MESH_TYPE_UNSTRUCTURED 0
#define P_MESH_TYPE_AABB 1
#define P_MESH_TYPE_TET 2
#define P_MESH_TYPE_HEX 3
#define P_MESH_TYPE_SURF 4
#define P_MESH_TYPE_TRIANGLE 5
#define P_MESH_TYPE_QUAD 6

#ifdef DEBUG_KERNEL
#define PMESH_DEBUG(pmesh) 												\
{ 																		\
	pmesh->n_elements = INVALID_INDEX; 									\
	pmesh->n_nodes = INVALID_INDEX; 									\
	pmesh->n_dims = INVALID_INDEX;										\
	generic_set(MAX_LOCAL_EL_POINTERS, INVALID_INDEX, pmesh->el_ptr);	\
	generic_set(MAX_LOCAL_EL_INDEX, INVALID_INDEX, pmesh->el_index);	\
	generic_set(MAX_LOCAL_MESH_POINTS * MAX_N_DIMS, -6, pmesh->points);	\
	generic_set(MAX_LOCAL_ELEMENTS, false, pmesh->is_new_element); 		\
}
#else
#define PMESH_DEBUG(pmesh)
#endif




#ifndef CLIPP_HOST_CL

//sutherland hodgman polygon clipping algorithm. polygon_1 is the clipping polygon
static bool intersect_convex_polygons(const SizeType n_vertices_1, const Scalar *polygon_1,
							  const SizeType n_vertices_2, const Scalar *polygon_2, 
							  SizeType *n_vertices_result, Scalar *result, const Scalar tol);

static short inside_half_plane(const Vector2 e1, const Vector2 e2, const Vector2 p, const Scalar tol);
static SizeType append_point(const SizeType current_n_points, const Vector2 point, Scalar *output);
static Vector2 intersect_lines(const Vector2 line_1a, const Vector2 line_1b, const Vector2 line_2a, const Vector2 line_2b);
static SizeType collapse_quasi_equal_points(const SizeType current_n_points, Scalar *points, Scalar tol);

static void p_mesh_print(const PMesh *mesh);
void p_mesh_crs_print(const PMesh *mesh);
static void p_mesh_copy(const PMesh *in, PMesh *out);
bool make_polyhedron_from_element(const Mesh mesh, const SizeType element, PMesh *result);
void make_faceted_tetrahedron_from_element(const Mesh mesh, const SizeType element, PMesh *result);
void make_faceted_hexahedron_from_element(const Mesh mesh, const SizeType element, PMesh *result);
static void compute_normals(const PMesh mesh, Scalar *normals);
static void compute_normals_and_fix_ordering(PMesh *surf_mesh, Scalar *normals);
Scalar p_mesh_volume_3(const PMesh surf_mesh);
Scalar tetrahedron_volume(const Vector3 p1, const Vector3 p2, const Vector3 p3, const Vector3 p4);
static Scalar m_tetrahedron_volume(const Scalar *tet);

SizeType make_h_polyhedron_from_element(const Mesh mesh, const SizeType element, Scalar *plane_normals, Scalar *plane_dists_from_origin);
static SizeType make_h_polyhedron_from_polyhedron(const PMesh polyhedron, Scalar *plane_normals, Scalar *plane_dists_from_origin);

static bool hs_cut_polyhedron(const SizeType n_half_spaces,
	const Scalar *planes_normal,
	const Scalar *planes_dist_from_origin,
	PMesh *polyhedron, Scalar *normals);

static bool intersect_convex_polyhedra(const PMesh poly1, const PMesh poly_2, PMesh *result);


static Scalar point_plane_distance(const SizeType n_dims, const Scalar *normal, const Scalar d, const Scalar * point);
bool plane_contains_point(const SizeType n_dims, const Scalar *normal, const Scalar d, const Scalar * point, const Scalar tol);

static short half_space_cut(const Scalar * normal,
	const Scalar d, 
	const PMesh *in, 
	const Scalar *in_surface_normals,
	Scalar *out_surface_normals, 
	PMesh *out, const Scalar tol);


SizeType edge_index(const SizeType n_vertices, const SizeType v1, const SizeType v2);
SizeType get_edge_id(const SizeType n_vertices, const SizeType v1, const SizeType v2, SizeType * edge_map);
SizeType make_edge_id(const SizeType n_vertices, const SizeType v1, const SizeType v2, const SizeType n_ids, SizeType * edge_map);
void bind_edge_to_id(const SizeType n_vertices, const SizeType v1, const SizeType v2, const SizeType id, SizeType * edge_map);


static bool intersect_ray_with_plane_2(const Scalar *ray_origin, const Scalar *ray_dir, const Scalar *plane_normal, const Scalar plane_dist_from_origin, Scalar *delta, const Scalar tol);
static bool intersect_ray_with_plane_3(const Scalar *ray_origin, const Scalar *ray_dir, const Scalar *plane_normal, const Scalar plane_dist_from_origin, Scalar *delta, const Scalar tol);
bool intersect_ray_with_plane_point_3(const Scalar *ray_origin, const Scalar *ray_dir, const Scalar *plane_normal, const Scalar plane_dist_from_origin, Scalar *intersection, const Scalar tol);
static bool intersect_ray_with_plane_using_ray_o_dist_3(const Scalar *ray_origin, const Scalar *ray_dir, const Scalar *plane_normal, const Scalar distance, Scalar *intersection, const Scalar tol);
static bool fix_polygon_ordering(const SizeType n_points, const Scalar *normal, const Scalar * points, SizeType *index);
static void sort_scalar_and_index_array(const SizeType n_elements, Scalar *values, SizeType * indices);
bool intersect_ray_with_line_2(const Scalar *ray_origin, const Scalar *ray_dir, const Scalar *line, Scalar *delta);
#endif //CLIPP_HOST_CL


#endif //INTERSECT_INTERFACES_CL
