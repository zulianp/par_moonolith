#ifndef MOONOLITH_FORWARD_DECLARATIONS_HPP
#define MOONOLITH_FORWARD_DECLARATIONS_HPP

// #define MOONOLITH_DEBUG(_Expr) _Expr
#include "par_moonolith_config.hpp"

// #ifndef MOONOLITH_DEBUG
// // #warning "MOONOLITH_DEBUG define with fallback macro"
// #define MOONOLITH_DEBUG(_Expr)
// #endif

namespace moonolith {
    // base types
    typedef unsigned char byte;
    static const int DEFAULT_REFINE_DEPTH = 5;
    static const int DEFAULT_REFINE_MAX_ELEMENTS = 40;

    class Serializable;
    class Stream;
    class InputStream;
    class OutputStream;
    class ByteInputStream;
    class ByteOutputStream;
    class DebugInputStream;
    class DebuOutputStream;

    // tree types
    template <class Traits>
    class Node;

    template <class Traits>
    class Root;

    template <class Traits>
    class Branch;

    template <class Traits>
    class Leaf;

    template <class Traits>
    class Visitor;

    template <class Traits>
    class TreeMemory;

    template <class Traits>
    class Tree;

    template <class Traits>
    class Mutator;

    template <class Traits>
    class MutatorFactory;

    template <class Traits>
    class Match;

    template <class Traits>
    class Hash;

    template <class Traits>
    class LocalGather;

    template <class Node>
    class NodeNavigator;
    template <class Tree>
    class BreadthFirstNavigator;

    template <class T>
    class Iterable;

    template <typename T, int Dim>
    class PMesh;

    class NodeHandle;

    template <int Dimension, typename T>
    class AABB;

}  // namespace moonolith

#endif  // MOONOLITH_FORWARD_DECLARATIONS_HPP
