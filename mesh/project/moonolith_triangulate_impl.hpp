#ifndef MOONOLITH_TRIANGULATE_IMPL_HPP
#define MOONOLITH_TRIANGULATE_IMPL_HPP

#include "moonolith_triangulate.hpp"

namespace moonolith {

    template <typename T>
    T TriangulatePolygon<T>::signed_triangle_area(const Point2 &a, const Point2 &b, const Point2 &c) {
        return ((a.x * b.y - a.y * b.x + a.y * c.x - a.x * c.y + b.x * c.y - c.x * b.y) / 2.0);
    }

    template <typename T>
    T TriangulatePolygon<T>::triangle_area(const Point2 &a, const Point2 &b, const Point2 &c) {
        using std::abs;
        return (abs(signed_triangle_area(a, b, c)));
    }

    template <typename T>
    bool TriangulatePolygon<T>::ccw(const Point2 &a, const Point2 &b, const Point2 &c) {
        // return (signed_triangle_area(a, b, c) > tol);
        return (signed_triangle_area(a, b, c) > 0.);
    }

    template <typename T>
    bool TriangulatePolygon<T>::cw(const Point2 &a, const Point2 &b, const Point2 &c) {
        // return (signed_triangle_area(a,b,c) < - tol);
        return (signed_triangle_area(a, b, c) < 0.);
    }

    template <typename T>
    bool TriangulatePolygon<T>::point_in_triangle(const Point2 &p, const Triangle2 &t) {
        for (int i = 0; i < 3; i++) {
            if (cw(t[i], t[(i + 1) % 3], p)) return false;
        }

        return true;
    }

    template <typename T>
    bool TriangulatePolygon<T>::ear_q(int i, int j, int k, const Polygon2 &p) {
        if (cw(p.points[i], p.points[j], p.points[k])) return false;

        t[0] = p.points[i];
        t[1] = p.points[j];
        t[2] = p.points[k];

        const int n_points = p.size();

        for (int m = 0; m < n_points; m++) {
            if ((m != i) && (m != j) && (m != k)) {
                if (point_in_triangle(p.points[m], t)) return false;
            }
        }

        return true;
    }

    template <typename T>
    void TriangulatePolygon<T>::add_triangle(int i, int j, int k, Storage<int> &t) {
        t.push_back(i);
        t.push_back(j);
        t.push_back(k);
    }

    template <typename T>
    void TriangulatePolygon<T>::apply(const Polygon<T, 2> &p, Storage<int> &t) {
        t.clear();

        int n_points = p.size();

        l.resize(n_points);
        r.resize(n_points);

        for (int i = 0; i < n_points; i++) {
            l[i] = ((i - 1) + n_points) % n_points;
            r[i] = ((i + 1) + n_points) % n_points;
        }

        int i = n_points - 1;
        int n_triangles_x_3 = (n_points - 2) * 3;
        while (static_cast<int>(t.size()) < n_triangles_x_3) {
            i = r[i];

            if (ear_q(l[i], i, r[i], p)) {
                add_triangle(l[i], i, r[i], t);
                l[r[i]] = l[i];
                r[l[i]] = r[i];
            }
        }
    }
}  // namespace moonolith

#endif  // MOONOLITH_TRIANGULATE_IMPL_HPP
