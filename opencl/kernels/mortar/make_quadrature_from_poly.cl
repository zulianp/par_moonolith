#ifndef MAKE_QUADRATURE_FROM_POLY_CL
#define MAKE_QUADRATURE_FROM_POLY_CL

#include "make_quadrature_from_poly_interface.cl"


static SizeType n_volume_elements(const PMesh polyhedron)
{	
	if(polyhedron.n_elements == 4) return 1;

	SizeType result = 0;
	for(SizeType e = 0; e < polyhedron.n_elements; ++e) {
		const SizeType begin = polyhedron.el_ptr[e];
		const SizeType end 	 = polyhedron.el_ptr[e + 1];
		result += (end - begin) - 2; 
	}

	return result;
}

static SizeType max_n_elements_from_facets(const PMesh polyhedron)
{
	if(polyhedron.n_elements == 4) return 1;

	SizeType result = 0;
	for(SizeType e = 0; e < polyhedron.n_elements; ++e) {
		const SizeType begin = polyhedron.el_ptr[e];
		const SizeType end 	 = polyhedron.el_ptr[e + 1];
		result = max( (end - begin) - 2, result ); 
	}

	return result;
}

SizeType make_quadrature_points_from_polygon_2(const SizeType n_points, const Scalar * polygon, const Scalar rescaling, 
	const SizeType order, Scalar *composite_quad_points, Scalar *composite_quad_weights)
{

	Scalar quad_points    [MAX_QUAD_POINTS*2];
	Scalar quad_weights   [MAX_QUAD_POINTS];
	Scalar triangle_buffer[3*2];


	//THIS DOES NOT WORK
	// if(n_points == 4) {
	// 	const SizeType rule_index = find_compatible_quad_rule_2(max_order_for_quad_rules, quad_rules, order);
	// 	const SizeType n_quad_points = quad_rules[rule_index].size;

	// 	KERNEL_ASSERT(n_quad_points > 0, "make_quadrature_points_from_polygon: n_quad_points = triangle_rules[rule_index].size");

	// 	generic_copy(n_quad_points * 2, quad_rules[rule_index].points,  quad_points);
	// 	generic_copy(n_quad_points,     quad_rules[rule_index].weights, composite_quad_weights);
	// 	quad_transform(polygon, n_quad_points, quad_points, composite_quad_points);

	// 	vec_scale(n_quad_points, polygon_area(4, polygon) * rescaling, composite_quad_weights);
	// 	return n_quad_points;
	// }

	const SizeType rule_index = find_compatible_quad_rule_2(max_order_for_triangle_rules, triangle_rules, order);
	const SizeType n_quad_points = triangle_rules[rule_index].size;

	KERNEL_ASSERT(n_quad_points > 0, "make_quadrature_points_from_polygon: n_quad_points = triangle_rules[rule_index].size");

	generic_copy(n_quad_points * 2, triangle_rules[rule_index].points,  quad_points);
	generic_copy(n_quad_points,     triangle_rules[rule_index].weights, quad_weights);

	const SizeType n_triangles = n_points - 2;
	const SizeType total_n_quad_points = n_triangles * n_quad_points;
	KERNEL_ASSERT(total_n_quad_points <= MAX_QUAD_POINTS, "make_quadrature_points_from_polygon: total_n_quad_points <= MAX_QUAD_POINTS");

	if(total_n_quad_points >= MAX_QUAD_POINTS) { printf("total_n_quad_points: %d\n", total_n_quad_points);}

	triangle_buffer[0] = polygon[0];
	triangle_buffer[1] = polygon[1];

	for(SizeType i = 0; i < n_triangles; ++i) {
		const SizeType tri_offset = 2 + i * 2;
		triangle_buffer[2] = polygon[tri_offset];
		triangle_buffer[3] = polygon[tri_offset + 1];

		triangle_buffer[4] = polygon[tri_offset + 2];
		triangle_buffer[5] = polygon[tri_offset + 3];

		const Scalar scale = polygon_area_2(3, triangle_buffer);
		KERNEL_ASSERT(scale > 0, "make_quadrature_points_from_polygon: negative area");

		const SizeType offset = i * n_quad_points;
		const SizeType offset_2 = offset * 2;

		triangle_transform(triangle_buffer, n_quad_points, quad_points, &composite_quad_points[offset_2]);
		generic_copy(n_quad_points, quad_weights, &composite_quad_weights[offset]);
		vec_scale(n_quad_points, scale, &composite_quad_weights[offset]);
	}

	vec_scale(total_n_quad_points, rescaling, composite_quad_weights);
	return total_n_quad_points;
}

SizeType make_quadrature_points_from_polygon_3(const SizeType n_points, const Scalar * polygon, const Scalar rescaling, 
	const SizeType order, Scalar *composite_quad_points, Scalar *composite_quad_weights)
{

	Scalar quad_points    [MAX_QUAD_POINTS * 3];
	Scalar quad_weights   [MAX_QUAD_POINTS];
	Scalar triangle_buffer[3 * 3];

	const SizeType rule_index = find_compatible_quad_rule_2(max_order_for_triangle_rules, triangle_rules, order);
	const SizeType n_quad_points = triangle_rules[rule_index].size;

	KERNEL_ASSERT(n_quad_points > 0, "make_quadrature_points_from_polygon: n_quad_points = triangle_rules[rule_index].size");

	generic_copy(n_quad_points * 2, triangle_rules[rule_index].points,  quad_points);
	generic_copy(n_quad_points,     triangle_rules[rule_index].weights, quad_weights);

	const SizeType n_triangles = n_points - 2;
	const SizeType total_n_quad_points = n_triangles * n_quad_points;
	KERNEL_ASSERT(total_n_quad_points <= MAX_QUAD_POINTS, "make_quadrature_points_from_polygon: total_n_quad_points <= MAX_QUAD_POINTS");

	if(total_n_quad_points >= MAX_QUAD_POINTS) { printf("total_n_quad_points: %d\n", total_n_quad_points);}

	//copy point 1
	triangle_buffer[0] = polygon[0];
	triangle_buffer[1] = polygon[1];
	triangle_buffer[2] = polygon[2];

	for(SizeType i = 0; i < n_triangles; ++i) {
		//skip first and i*stride
		const SizeType tri_offset = 3 + i * 3;
		//point 2
		triangle_buffer[3] = polygon[tri_offset];
		triangle_buffer[4] = polygon[tri_offset + 1];
		triangle_buffer[5] = polygon[tri_offset + 2];
		//point 3
		triangle_buffer[6] = polygon[tri_offset + 3];
		triangle_buffer[7] = polygon[tri_offset + 4];
		triangle_buffer[8] = polygon[tri_offset + 5];

		const Scalar scale = polygon_area_3(3, triangle_buffer);
		KERNEL_ASSERT(scale > 0, "make_quadrature_points_from_polygon: negative area");

		const SizeType offset = i * n_quad_points;
		const SizeType offset_3 = offset * 3;

		triangle_transform_3(triangle_buffer, n_quad_points, quad_points, &composite_quad_points[offset_3]);
		generic_copy(n_quad_points, quad_weights, &composite_quad_weights[offset]);
		vec_scale(n_quad_points, scale, &composite_quad_weights[offset]);
	}

	vec_scale(total_n_quad_points, rescaling, composite_quad_weights);
	return total_n_quad_points;
}

SizeType make_quadrature_points_from_polyhedron(const PMesh polyhedron, const Scalar rescaling, const SizeType order, 
	Scalar * composite_quad_points, Scalar * composite_quad_weights)
{
	Scalar quad_points [MAX_QUAD_POINTS * 3];
	Scalar quad_weights[MAX_QUAD_POINTS];
	Scalar barycenter  [3];
	
	KERNEL_ASSERT(polyhedron.n_elements >= 4, "make_quadrature_points_from_polyhedron: malformed polyhedron");

	KERNEL_ASSERT(order > 0, "order > 0");

	if(polyhedron.n_elements == 4) {
		return make_quadrature_from_tetrahedron(polyhedron.points, rescaling, order, composite_quad_points, composite_quad_weights);
	}

	const SizeType rule_index = find_compatible_quad_rule_3(max_order_for_tetrahedron_rules, tetrahedron_rules, order);
	const SizeType n_quad_points =  tetrahedron_rules[rule_index].size;

	KERNEL_ASSERT(n_quad_points > 0, "make_quadrature_points_from_polyhedron: n_quad_points > 0");

	generic_copy(n_quad_points * 3, tetrahedron_rules[rule_index].points,  quad_points);
	generic_copy(n_quad_points,     tetrahedron_rules[rule_index].weights, quad_weights);
	
	//store barycenter as first point of the tetrahedron
	row_average(polyhedron.n_nodes, polyhedron.n_dims, polyhedron.points, barycenter);
	return make_quadrature_points_from_polyhedron_in_range_around_point(polyhedron, 0, polyhedron.n_elements, 
		rescaling, n_quad_points, quad_points,
		quad_weights, barycenter, composite_quad_points,  
		composite_quad_weights);
}

SizeType make_quadrature_from_tetrahedron(const Scalar *points, const Scalar rescaling, 
	const SizeType order, 
	Scalar * composite_quad_points, 
	Scalar * composite_quad_weights)
{
	Scalar quad_points [MAX_QUAD_POINTS * 3];
	Scalar quad_weights[MAX_QUAD_POINTS];

	KERNEL_ASSERT(order > 0, "order > 0");

	const SizeType rule_index 	 = find_compatible_quad_rule_3(max_order_for_tetrahedron_rules, tetrahedron_rules, order);
	const SizeType n_quad_points = tetrahedron_rules[rule_index].size;

	KERNEL_ASSERT(n_quad_points > 0, "make_quadrature_points_from_polyhedron: n_quad_points > 0");

	generic_copy(n_quad_points * 3, tetrahedron_rules[rule_index].points,  quad_points);
	generic_copy(n_quad_points,     tetrahedron_rules[rule_index].weights, quad_weights);

	tetrahedron_transform(points, n_quad_points, quad_points, composite_quad_points);
	generic_copy(n_quad_points, quad_weights, composite_quad_weights);
	vec_scale(n_quad_points, fabs(m_tetrahedron_volume(points) * rescaling), composite_quad_weights);
	return n_quad_points;
}

static SizeType make_quadrature_points_from_polyhedron_in_range_around_point(
	const PMesh polyhedron, 
	const SizeType elements_begin,
	const SizeType elements_end,
	const Scalar rescaling, 
	const SizeType n_quad_points,
	const Scalar * quad_points,
	const Scalar * quad_weights,
	const Scalar * barycenter,
	Scalar * composite_quad_points, 
	Scalar * composite_quad_weights)
{
	Scalar tetra_buffer[4 * 3];
	
	KERNEL_ASSERT(polyhedron.n_elements >= 4, "make_quadrature_points_from_polyhedron: malformed polyhedron");

	//store barycenter as first point of the tetrahedron
	generic_copy(polyhedron.n_dims, barycenter, tetra_buffer);

	SizeType n_tetra = 0;
	for(SizeType e = elements_begin; e < elements_end; ++e) {
		const SizeType begin = polyhedron.el_ptr[e];
		const SizeType end 	 = polyhedron.el_ptr[e + 1];
		n_tetra += (end - begin) - 2; //treat also non-triangular surface elements
	}

	KERNEL_ASSERT(n_tetra > 0, "make_quadrature_points_from_polyhedron: n_tetra > 0");

	const SizeType total_n_quad_points = n_tetra * n_quad_points;
	KERNEL_ASSERT(total_n_quad_points <= MAX_QUAD_POINTS, "make_quadrature_points_from_polyhedron: total_n_quad_points <= MAX_QUAD_POINTS");
	KERNEL_ASSERT(total_n_quad_points > 0, "make_quadrature_points_from_polyhedron: total_n_quad_points > 0");

	if(total_n_quad_points > MAX_QUAD_POINTS) { 
		printf("n_tetra: %d, total_n_quad_points: %d\n", n_tetra, total_n_quad_points);
		p_mesh_print(&polyhedron);
		return 0;
	}

	SizeType offset = 0;
	for(SizeType e = elements_begin; e < elements_end; ++e) {
		const SizeType begin 		 = polyhedron.el_ptr[e];
		const SizeType end 	 		 = polyhedron.el_ptr[e + 1];
		const SizeType n_local_tetra = (end - begin) - 2; //treat also non-triangular surface elements

		const SizeType v1 = polyhedron.el_index[begin] * 3;
		generic_copy(3, &polyhedron.points[v1], &tetra_buffer[3]);

		for(SizeType k = 1; k <= n_local_tetra; ++k) {
			const SizeType node_offset = begin + k;
			const SizeType v2 = polyhedron.el_index[node_offset] 	 * 3;
			const SizeType v3 = polyhedron.el_index[node_offset + 1] * 3;
			const SizeType offset_3 = offset * 3;

			generic_copy(3, &polyhedron.points[v2], &tetra_buffer[6]);
			generic_copy(3, &polyhedron.points[v3], &tetra_buffer[9]);

			tetrahedron_transform(tetra_buffer, n_quad_points, quad_points, &composite_quad_points[offset_3]);

			generic_copy(n_quad_points, quad_weights, &composite_quad_weights[offset]);

			const Scalar scale = fabs(m_tetrahedron_volume(tetra_buffer));

			KERNEL_ASSERT(scale == scale, "nan volume");

			vec_scale(n_quad_points, scale, &composite_quad_weights[offset]);

			offset += n_quad_points;
		}
	}

	KERNEL_ASSERT(total_n_quad_points == offset, "make_quadrature_points_from_polyhedron: total_n_quad_points == offset");
	KERNEL_ASSERT(total_n_quad_points > 0, "make_quadrature_points_from_polyhedron: total_n_quad_points > 0");

	vec_scale(total_n_quad_points, rescaling, composite_quad_weights);
	return total_n_quad_points;
}


SizeType make_quadrature_points_from_polyline_2(
		const SizeType n_points, 
		const Scalar *poly_line, 
		const Scalar rescaling, 
		const SizeType order, 
		Scalar *composite_quad_points, 
		Scalar *composite_quad_weights)
{
	Scalar quad_points  [MAX_QUAD_POINTS];
	Scalar quad_weights [MAX_QUAD_POINTS];
	Scalar line_buffer 	[2 * 2];

	const SizeType rule_index    = find_compatible_quad_rule_1(max_order_for_line_rules, line_rules, order);
	const SizeType n_quad_points = line_rules[rule_index].size;

	KERNEL_ASSERT(n_quad_points > 0, "make_quadrature_points_from_polyline_2: n_quad_points = line_rules[rule_index].size");

	generic_copy(n_quad_points, line_rules[rule_index].points,  quad_points);
	generic_copy(n_quad_points, line_rules[rule_index].weights, quad_weights);

	const SizeType n_lines = n_points - 1;
	const SizeType total_n_quad_points = n_lines * n_quad_points;
	KERNEL_ASSERT(total_n_quad_points <= MAX_QUAD_POINTS, "make_quadrature_points_from_polyline_2: total_n_quad_points <= MAX_QUAD_POINTS");

	if(total_n_quad_points >= MAX_QUAD_POINTS) { printf("total_n_quad_points: %d\n", total_n_quad_points);}

	//copy point 1
	line_buffer[0] = poly_line[0];
	line_buffer[1] = poly_line[1];

	for(SizeType i = 0; i < n_lines; ++i) {
		//skip first and i*stride
		const SizeType line_offset = 2 + i * 2;
		
		//point 2
		line_buffer[2] = poly_line[line_offset];
		line_buffer[3] = poly_line[line_offset + 1];
		
		const Scalar scale = distance_n(2, line_buffer, &line_buffer[2]);
		KERNEL_ASSERT(scale > 0, "make_quadrature_points_from_polygon: negative area");

		const SizeType offset = i * n_quad_points;
		const SizeType offset_2 = offset * 2;

		line_transform_2(line_buffer, n_quad_points, quad_points, &composite_quad_points[offset_2]);
		generic_copy(n_quad_points, quad_weights, &composite_quad_weights[offset]);
		vec_scale(n_quad_points, scale, &composite_quad_weights[offset]);
	}

	vec_scale(total_n_quad_points, rescaling, composite_quad_weights);
	return total_n_quad_points;
}

#endif //MAKE_QUADRATURE_FROM_POLY_CL
