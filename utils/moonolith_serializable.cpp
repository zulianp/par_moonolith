#include "moonolith_serializable.hpp"
#include <assert.h>
#include "moonolith_input_stream.hpp"
#include "moonolith_output_stream.hpp"

namespace moonolith {

	Serializable::~Serializable() {}

	void Serializable::apply_read_write(Stream &) 
	{ 
		assert(false && "implement me or implement both read and write"); 
	}

	void Serializable::write(OutputStream &stream) const 
	{ 
		const_cast<Serializable &>(*this).apply_read_write(stream); 
	}

	void Serializable::read(InputStream &stream) 
	{ 
		apply_read_write(stream); 
	}    
}
