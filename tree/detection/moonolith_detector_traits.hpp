#ifndef MOONOLITH_DETECTOR_TRAITS_HPP
#define MOONOLITH_DETECTOR_TRAITS_HPP 

namespace cutlibpp {

	template<class _DataType, class _Bound>
	struct DefaultDetectorTraits {
		typedef _DataType DataType;
		typedef _Bound Bound;
	};

	//example traits
	// struct DetectorTraits {
	// 	typedef MyDataType DataType;
	// 	typedef MyBox Bound;
	// 	typedef MyPredicate Predicate;
	// 	typedef MyNodeAllocator NodeAllocator;
	//	typedef MyDataAllocator Allocator;
	// };
}

#endif //MOONOLITH_DETECTOR_TRAITS_HPP


