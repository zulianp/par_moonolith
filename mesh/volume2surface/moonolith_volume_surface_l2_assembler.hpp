#ifndef MOONOLITH_VOLUME_SURFACE_L2_TRANSFER_HPP
#define MOONOLITH_VOLUME_SURFACE_L2_TRANSFER_HPP

#include "moonolith_duplicate_intersection_avoidance.hpp"
#include "moonolith_expanding_array.hpp"
#include "moonolith_map_quadrature.hpp"
#include "moonolith_map_quadrature_impl.hpp"
#include "moonolith_matrix_buffers.hpp"
#include "moonolith_sparse_matrix.hpp"

#include "moonolith_collection_manager.hpp"
#include "moonolith_function_space.hpp"
#include "moonolith_gauss_quadrature_rule.hpp"
#include "moonolith_incomplete_intersection_remover.hpp"
#include "moonolith_keast_quadrature_rule.hpp"
#include "moonolith_l2_assembler.hpp"
#include "moonolith_matrix_inserter.hpp"

#include <memory>

namespace moonolith {

    template <typename T, int PhysicalDim>
    class BuildQuadratureWithDuplicateAvoidance {
    public:
        using MasterTransform = moonolith::AffineTransform<T, PhysicalDim, PhysicalDim>;
        using SlaveTransform = moonolith::AffineTransform<T, PhysicalDim - 1, PhysicalDim>;
        using QMaster = moonolith::Quadrature<T, PhysicalDim>;
        using QSlave = moonolith::Quadrature<T, PhysicalDim - 1>;

        template <class MasterElem, class SlaveElem>
        bool apply(const QSlave &q_ref,
                   const Storage<std::shared_ptr<MasterElem>> &masters,
                   const std::shared_ptr<SlaveElem> &slave) {
            const std::size_t n_masters = masters.size();
            dia_.master_poly.resize(n_masters);

            for (std::size_t i = 0; i < n_masters; ++i) {
                make(*masters[i], dia_.master_poly[i]);
            }

            make(*slave, dia_.slave_poly);

            if (!dia_.apply()) {
                return false;
            }

            assert(dia_.is_slave_covered());

            resize_q(n_masters);

            make_transform(*slave, trafo_slave_);

            const T measure_slave = measure(dia_.slave_poly);
            T isect_measure = 0.0;

            for (std::size_t i = 0; i < n_masters; ++i) {
                if (dia_.intersected[i]) {
                    map(q_ref, static_cast<T>(1.), dia_.isect[i], q_physical_[i]);
                    make_transform(*masters[i], trafo_master_);
                    map_quadrature(measure_slave, i);

                    isect_measure += dia_.measures[i];

                } else {
                    q_master_[i].clear();
                    q_slave_[i].clear();
                    q_physical_[i].clear();
                }
            }

            if (!approxeq(measure_slave, isect_measure, static_cast<T>(1e-6))) {
                logger() << "[Warning] " << measure_slave << " != " << isect_measure << std::endl;
            }

            return true;
        }

        inline bool intersected(const std::size_t i) const { return dia_.intersected[i]; }

        inline const QMaster &q_master(const std::size_t i) const { return q_master_[i]; }

        inline const QSlave &q_slave(const std::size_t i) const { return q_slave_[i]; }

    private:
        DuplicateIntersectionAvoidanceFromDim<T, PhysicalDim> dia_;

        MasterTransform trafo_master_;
        SlaveTransform trafo_slave_;

        Storage<QMaster> q_physical_;
        Storage<QMaster> q_master_;
        Storage<QSlave> q_slave_;

        void resize_q(const std::size_t n_masters) {
            q_master_.resize(n_masters);
            q_slave_.resize(n_masters);
            q_physical_.resize(n_masters);
        }

        bool map_quadrature(const T measure_slave, const std::size_t i_master) {
            const auto measure_master = measure(dia_.master_poly[i_master]);

            auto &q_master = q_master_[i_master];
            auto &q_slave = q_slave_[i_master];

            const auto &q_physical = q_physical_[i_master];
            const std::size_t n_qp = q_physical.n_points();

            q_master.resize(n_qp);
            q_slave.resize(n_qp);

            bool ok_master = true, ok_slave = true, ok = true;

            for (std::size_t k = 0; k < n_qp; ++k) {
                ok_master = trafo_master_.apply_inverse(q_physical.point(k), q_master.point(k));
                assert(ok_master);
                ok_slave = trafo_slave_.apply_inverse(q_physical.point(k), q_slave.point(k));
                assert(ok_slave);

                q_master.weights[k] = q_physical.weights[k] / measure_master;
                q_slave.weights[k] = q_physical.weights[k] / measure_slave;

                if (!ok_master || !ok_slave) {
                    ok = false;
                    break;
                }
            }

            return ok;
        }
    };

    template <class MasterElem, class SlaveElem>
    class VolumeSurfaceL2Assembler {
    public:
        using T = typename SlaveElem::T;
        static const int Dim = SlaveElem::Dim;
        static const int PhysicalDim = SlaveElem::PhysicalDim;

        using QuadratureT = moonolith::Quadrature<T, Dim>;
        using L2TransferT = moonolith::L2Transfer<MasterElem, SlaveElem>;

        template <class Elem>
        static int order_of_quadrature(const Elem &e) {
            if (e.is_simplex()) {
                return e.order();
            } else {
                return e.order() * Dim;
            }
        }

        void init_quadrature() {
            if (master_elem.empty()) return;

            int master_order = order_of_quadrature(*master_elem[0]);

            const auto n = master_elem.size();
            for (std::size_t i = 1; i < n; ++i) {
                master_order = std::max(master_order, order_of_quadrature(*master_elem[i]));
            }

            int slave_order = order_of_quadrature(*slave_elem);

            int order = 0;
            if (slave_elem->is_affine()) {
                order = master_order + slave_order;
            } else {
                // assumes tensor product
                order = master_order + slave_order + (slave_order - 1) * 2;
            }

            if (order == current_quadrature_order_) return;

            Gauss::get(order, q_rule);
        }

        template <class ElemAdapter>
        inline bool operator()(const Storage<ElemAdapter> &masters, const ElemAdapter &slave) {
            const std::size_t n_masters = masters.size();

            master_elem.resize(n_masters);

            for (std::size_t i = 0; i < n_masters; ++i) {
                auto &e_m = masters[i].elem();
                auto &m_m = masters[i].collection();
                make(m_m, e_m, master_elem[i]);
            }

            auto &e_s = slave.elem();
            auto &m_s = slave.collection();
            auto &dof_obj_s = m_s.dof_map().dof_object(slave.element_handle());
            auto &dof_s = dof_obj_s.dofs;
            make(m_s, e_s, slave_elem);

            init_quadrature();

            if (!build_q_.apply(q_rule, master_elem, slave_elem)) {
                return false;
            }

            bool had_intersection = false;

            for (std::size_t i = 0; i < n_masters; ++i) {
                if (!build_q_.intersected(i)) continue;

                assembler.assemble(*master_elem[i], *slave_elem, build_q_.q_master(i), build_q_.q_slave(i));

                auto &m_m = masters[i].collection();
                auto &dof_m = m_m.dof_map().dofs(masters[i].element_handle());

                const auto &B_e = assembler.coupling_matrix();
                const auto &D_e = assembler.mass_matrix();
                const auto &Q_e = assembler.transformation();

                buffers.B.insert(dof_s, dof_m, B_e);
                buffers.D.insert(dof_s, dof_s, D_e);
                buffers.Q.insert(dof_s, dof_s, Q_e);
                buffers.measure().insert(dof_obj_s.element_dof, D_e.sum());

                had_intersection = true;
            }

            return had_intersection;
        }

        void clear() { assembler.clear(); }

        VolumeSurfaceL2Assembler(MatrixBuffers<T> &buffers) : buffers(buffers), current_quadrature_order_(-1) {}

        L2TransferT assembler;

    private:
        QuadratureT q_rule;
        Storage<std::shared_ptr<MasterElem>> master_elem;
        std::shared_ptr<SlaveElem> slave_elem;
        MatrixBuffers<T> &buffers;
        int current_quadrature_order_;

        BuildQuadratureWithDuplicateAvoidance<T, PhysicalDim> build_q_;
    };
}  // namespace moonolith

#endif  // MOONOLITH_VOLUME_SURFACE_L2_TRANSFER_HPP
