#ifndef HEXAHEDRON_CL
#define HEXAHEDRON_CL 


#include "kernel_base.cl"
#include "fe_object.cl"
#include "transform.cl"
#include "fe_interfaces_3.cl"

#define F_MINUS(x) (1.0-x)
#define F_PLUS(x)  (x)
#define D_MINUS(x) (-1.0)
#define D_PLUS(x) (1.0)

void hex_make_fe_object(const Scalar *points, const SizeType n_quad_points, const Scalar * quad_points, FEObject *object)
{
	object->n_dims = 3;
	object->n_quad_points = n_quad_points;
	object->reference_volume = 1;
	object->n_shape_functions = 8;

	static_assert(MAX_SHAPE_FUNCS >= 8, "check kernel_base for MAX_SHAPE_FUNCS");
	
	hex_trilinear_fun_0(n_quad_points, quad_points, object->fun[0]);
	hex_trilinear_fun_1(n_quad_points, quad_points, object->fun[1]);
	hex_trilinear_fun_2(n_quad_points, quad_points, object->fun[2]);
	hex_trilinear_fun_3(n_quad_points, quad_points, object->fun[3]);
	hex_trilinear_fun_4(n_quad_points, quad_points, object->fun[4]);
	hex_trilinear_fun_5(n_quad_points, quad_points, object->fun[5]);
	hex_trilinear_fun_6(n_quad_points, quad_points, object->fun[6]);
	hex_trilinear_fun_7(n_quad_points, quad_points, object->fun[7]);


	hex_trilinear_grad_0(n_quad_points, quad_points, object->grad[0]);
	hex_trilinear_grad_1(n_quad_points, quad_points, object->grad[1]);
	hex_trilinear_grad_2(n_quad_points, quad_points, object->grad[2]);
	hex_trilinear_grad_3(n_quad_points, quad_points, object->grad[3]);
	hex_trilinear_grad_4(n_quad_points, quad_points, object->grad[4]);
	hex_trilinear_grad_5(n_quad_points, quad_points, object->grad[5]);
	hex_trilinear_grad_6(n_quad_points, quad_points, object->grad[6]);
	hex_trilinear_grad_7(n_quad_points, quad_points, object->grad[7]);

	hex_iso_parametric_transform(object, points, object->point);	
	hex_iso_parametric_jacobian(object, points,  object->jacobian);	
}

void hex_iso_parametric_transform(const FEObject *fe, const Scalar * points, Scalar * global_quad_points)
{
	iso_parametric_transform_3(fe, points, global_quad_points);
}

void hex_iso_parametric_jacobian(const FEObject *fe, const Scalar * points, Scalar * jacobians)
{
	iso_parametric_jacobian_3(fe, points, jacobians);
}

void hex_transform(const Scalar * points, const SizeType n_quad_points, const Scalar * quad_points, Scalar * global_quad_points)
{	
	Scalar f0, f1, f2, f3, f4, f5, f6, f7;
    const Vector3 p0 = vec_3(points[0],  points[1],  points[2]);
    const Vector3 p1 = vec_3(points[3],  points[4],  points[5]);
    const Vector3 p2 = vec_3(points[6],  points[7],  points[8]);
    const Vector3 p3 = vec_3(points[9],  points[10], points[11]);
    const Vector3 p4 = vec_3(points[12], points[13], points[14]);
    const Vector3 p5 = vec_3(points[15], points[16], points[17]);
    const Vector3 p6 = vec_3(points[18], points[19], points[20]);
    const Vector3 p7 = vec_3(points[21], points[22], points[23]);


    for(SizeType q = 0; q < n_quad_points; ++q) {   
        const SizeType q3 	= q  * 3;
        const SizeType q3p1 = q3 + 1;
        const SizeType q3p2 = q3 + 2;

        hex_trilinear_fun_0(1, &quad_points[q3], &f0);
        hex_trilinear_fun_1(1, &quad_points[q3], &f1);
        hex_trilinear_fun_2(1, &quad_points[q3], &f2);
        hex_trilinear_fun_3(1, &quad_points[q3], &f3);
        hex_trilinear_fun_4(1, &quad_points[q3], &f4);
        hex_trilinear_fun_5(1, &quad_points[q3], &f5);
        hex_trilinear_fun_6(1, &quad_points[q3], &f6);
        hex_trilinear_fun_7(1, &quad_points[q3], &f7);

        Vector3 global_point = f0 * p0;
        global_point        += f1 * p1;
        global_point        += f2 * p2;
        global_point        += f3 * p3;
        global_point        += f4 * p4;
        global_point        += f5 * p5;
        global_point        += f6 * p6;
        global_point        += f7 * p7;

        global_quad_points[q3]   = global_point[0];
        global_quad_points[q3p1] = global_point[1];
        global_quad_points[q3p2] = global_point[2];
    }
}


void hex_transform_jacobian(const Scalar * points, const SizeType n_quad_points, const Scalar * quad_points, Scalar * jacobians)
{
    Scalar outer_product_buff[3 * 3];
    Scalar grad 			 [3];

    const SizeType n_quad_points_x_d = n_quad_points * 9;
  
    set(n_quad_points_x_d, 0, jacobians);
    
    for(SizeType q = 0; q < n_quad_points; ++q) {
    	const SizeType q3 	 = q * 3;
        const SizeType q_jac = q * 9;

        const Scalar *quad_point = &quad_points[q3];

        hex_trilinear_grad_0(1, quad_point, grad);
        outer_product(3, 3, &points[0], grad, outer_product_buff);    
        axpy(9, 1.0, outer_product_buff, &jacobians[q_jac]);

        hex_trilinear_grad_1(1, quad_point, grad);
        outer_product(3, 3, &points[3], grad, outer_product_buff);    
        axpy(9, 1.0, outer_product_buff, &jacobians[q_jac]);

        hex_trilinear_grad_2(1, quad_point, grad);
        outer_product(3, 3, &points[6], grad, outer_product_buff);    
        axpy(9, 1.0, outer_product_buff, &jacobians[q_jac]);

        hex_trilinear_grad_3(1, quad_point, grad);
        outer_product(3, 3, &points[9], grad, outer_product_buff);    
        axpy(9, 1.0, outer_product_buff, &jacobians[q_jac]);

        hex_trilinear_grad_4(1, quad_point, grad);
        outer_product(3, 3, &points[12], grad, outer_product_buff);    
        axpy(9, 1.0, outer_product_buff, &jacobians[q_jac]);

        hex_trilinear_grad_5(1, quad_point, grad);
        outer_product(3, 3, &points[15], grad, outer_product_buff);    
        axpy(9, 1.0, outer_product_buff, &jacobians[q_jac]);

        hex_trilinear_grad_6(1, quad_point, grad);
        outer_product(3, 3, &points[18], grad, outer_product_buff);    
        axpy(9, 1.0, outer_product_buff, &jacobians[q_jac]);

        hex_trilinear_grad_7(1, quad_point, grad);
        outer_product(3, 3, &points[21], grad, outer_product_buff);    
        axpy(9, 1.0, outer_product_buff, &jacobians[q_jac]);
    }
}

void hex_inverse_transform(const Scalar * points, const SizeType n_quad_points, const Scalar * global_quad_points, Scalar * quad_points)
{
    const SizeType max_newton_steps = 10;
    
    Scalar increment[3];
    Scalar gradient [3];
    Scalar hessian  [3 * 3];

    set(n_quad_points * 3, 0, quad_points);
    
    for(SizeType q = 0; q < n_quad_points; ++q) {
        const SizeType q3 = q * 3;
       
        Scalar *quad_point = &quad_points[q3]; 
        const Scalar *global_point = &global_quad_points[q3];
        
        for(SizeType i = 0; i < max_newton_steps; ++i) {
            hex_transform(points, 1, quad_point, gradient);

            gradient[0] -= global_point[0];
            gradient[1] -= global_point[1];
            gradient[2] -= global_point[2];

            hex_transform_jacobian(points, 1, quad_point, hessian);
            solve_3x3(hessian, gradient, increment);

            quad_point[0] -= increment[0];
            quad_point[1] -= increment[1];
            quad_point[2] -= increment[2];

            // printf("%d %g\n", i, norm_n(3, gradient) );

            if(norm_n(3, increment) < DEFAULT_TOLLERANCE) {
                break;
            }
        }
    }

    // #ifdef CLIPP_HOST_CL
    // 	express::ConstMatrixView<Scalar> view(quad_points, n_quad_points, 3);
    // 	geom::plotp(view.transposed(), "100, 100, 100", "hex_ref");
    // #endif //CLIPP_HOST_CL
}

///0, 0, 0
void hex_trilinear_fun_0(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		const SizeType q3 = q*3;
		funs[q] = F_MINUS(quad_points[q3]) * F_MINUS(quad_points[q3+1]) * F_MINUS(quad_points[q3+2]);
	}
}

void hex_trilinear_grad_0(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads)
{
	const SizeType n_quad_points_x_d = n_quad_points*3;
	for(SizeType q = 0; q < n_quad_points_x_d; q+=3) {	
		const Scalar x = quad_points[q];
		const Scalar y = quad_points[q+1];
		const Scalar z = quad_points[q+2];


		grads[q]   = D_MINUS(x) * F_MINUS(y) * F_MINUS(z);
		grads[q+1] = F_MINUS(x) * D_MINUS(y) * F_MINUS(z);
		grads[q+2] = F_MINUS(x) * F_MINUS(y) * D_MINUS(z);
	}
}

///1, 0, 0
void hex_trilinear_fun_1(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		const SizeType q3 = q*3;
		funs[q] = F_PLUS(quad_points[q3]) * F_MINUS(quad_points[q3+1]) * F_MINUS(quad_points[q3+2]);
	}
}

void hex_trilinear_grad_1(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads)
{
	const SizeType n_quad_points_x_d = n_quad_points*3;
	for(SizeType q = 0; q < n_quad_points_x_d; q+=3) {	
		const Scalar x = quad_points[q];
		const Scalar y = quad_points[q+1];
		const Scalar z = quad_points[q+2];

		grads[q]   = D_PLUS(x) * F_MINUS(y) * F_MINUS(z);
		grads[q+1] = F_PLUS(x) * D_MINUS(y) * F_MINUS(z);
		grads[q+2] = F_PLUS(x) * F_MINUS(y) * D_MINUS(z);
	}
}

///1, 0, 1
void hex_trilinear_fun_2(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		const SizeType q3 = q*3;
		funs[q] = F_PLUS(quad_points[q3]) * F_MINUS(quad_points[q3+1]) * F_PLUS(quad_points[q3+2]);
	}
}

void hex_trilinear_grad_2(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads)
{
	const SizeType n_quad_points_x_d = n_quad_points*3;
	for(SizeType q = 0; q < n_quad_points_x_d; q+=3) {	
		const Scalar x = quad_points[q];
		const Scalar y = quad_points[q+1];
		const Scalar z = quad_points[q+2];

		grads[q]   = D_PLUS(x) * F_MINUS(y) * F_PLUS(z);
		grads[q+1] = F_PLUS(x) * D_MINUS(y) * F_PLUS(z);
		grads[q+2] = F_PLUS(x) * F_MINUS(y) * D_PLUS(z);
	}
}

///0, 0, 1
void hex_trilinear_fun_3(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		const SizeType q3 = q*3;
		funs[q] = F_MINUS(quad_points[q3]) * F_MINUS(quad_points[q3+1]) * F_PLUS(quad_points[q3+2]);
	}
}

void hex_trilinear_grad_3(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads)
{
	const SizeType n_quad_points_x_d = n_quad_points*3;
	for(SizeType q = 0; q < n_quad_points_x_d; q+=3) {	
		const Scalar x = quad_points[q];
		const Scalar y = quad_points[q+1];
		const Scalar z = quad_points[q+2];

		grads[q]   = D_MINUS(x) * F_MINUS(y) * F_PLUS(z);
		grads[q+1] = F_MINUS(x) * D_MINUS(y) * F_PLUS(z);
		grads[q+2] = F_MINUS(x) * F_MINUS(y) * D_PLUS(z);
	}
}

///0, 1, 0
void hex_trilinear_fun_4(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		const SizeType q3 = q*3;
		funs[q] = F_MINUS(quad_points[q3]) * F_PLUS(quad_points[q3+1]) * F_MINUS(quad_points[q3+2]);
	}
}

void hex_trilinear_grad_4(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads)
{
	const SizeType n_quad_points_x_d = n_quad_points*3;
	for(SizeType q = 0; q < n_quad_points_x_d; q+=3) {	
		const Scalar x = quad_points[q];
		const Scalar y = quad_points[q+1];
		const Scalar z = quad_points[q+2];

		grads[q]   = D_MINUS(x) * F_PLUS(y) * F_MINUS(z);
		grads[q+1] = F_MINUS(x) * D_PLUS(y) * F_MINUS(z);
		grads[q+2] = F_MINUS(x) * F_PLUS(y) * D_MINUS(z);
	}
}

///1, 1, 0
void hex_trilinear_fun_5(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		const SizeType q3 = q*3;
		funs[q] = F_PLUS(quad_points[q3]) * F_PLUS(quad_points[q3+1]) * F_MINUS(quad_points[q3+2]);
	}
}

void hex_trilinear_grad_5(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads)
{
	const SizeType n_quad_points_x_d = n_quad_points*3;
	for(SizeType q = 0; q < n_quad_points_x_d; q+=3) {	
		const Scalar x = quad_points[q];
		const Scalar y = quad_points[q+1];
		const Scalar z = quad_points[q+2];

		grads[q]   = D_PLUS(x) * F_PLUS(y) * F_MINUS(z);
		grads[q+1] = F_PLUS(x) * D_PLUS(y) * F_MINUS(z);
		grads[q+2] = F_PLUS(x) * F_PLUS(y) * D_MINUS(z);
	}
}

///1, 1, 1
void hex_trilinear_fun_6(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		const SizeType q3 = q*3;
		funs[q] = F_PLUS(quad_points[q3]) * F_PLUS(quad_points[q3+1]) * F_PLUS(quad_points[q3+2]);
	}
}

void hex_trilinear_grad_6(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads)
{
	const SizeType n_quad_points_x_d = n_quad_points*3;
	for(SizeType q = 0; q < n_quad_points_x_d; q+=3) {	
		const Scalar x = quad_points[q];
		const Scalar y = quad_points[q+1];
		const Scalar z = quad_points[q+2];

		grads[q]   = D_PLUS(x) * F_PLUS(y) * F_PLUS(z);
		grads[q+1] = F_PLUS(x) * D_PLUS(y) * F_PLUS(z);
		grads[q+2] = F_PLUS(x) * F_PLUS(y) * D_PLUS(z);
	}
}

///0, 1, 1
void hex_trilinear_fun_7(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		const SizeType q3 = q*3;
		funs[q] = F_MINUS(quad_points[q3]) * F_PLUS(quad_points[q3+1]) * F_PLUS(quad_points[q3+2]);
	}
}

void hex_trilinear_grad_7(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads)
{
	const SizeType n_quad_points_x_d = n_quad_points*3;
	for(SizeType q = 0; q < n_quad_points_x_d; q+=3) {	
		const Scalar x = quad_points[q];
		const Scalar y = quad_points[q+1];
		const Scalar z = quad_points[q+2];

		grads[q]   = D_MINUS(x) * F_PLUS(y) * F_PLUS(z);
		grads[q+1] = F_MINUS(x) * D_PLUS(y) * F_PLUS(z);
		grads[q+2] = F_MINUS(x) * F_PLUS(y) * D_PLUS(z);
	}
}

//clean macros
#undef F_MINUS
#undef F_PLUS
#undef D_MINUS
#undef D_PLUS


#endif //HEXAHEDRON_CL

