#ifndef MOONOLITH_ELEM_NODE1_HPP
#define MOONOLITH_ELEM_NODE1_HPP

#include "moonolith_elem.hpp"

namespace moonolith {

    template <typename T, int Order_, int PhysicalDim = 1>
    class NodeElement : public Elem<T, 0, PhysicalDim> {
    public:
        using Node = moonolith::Vector<T, PhysicalDim>;
        static const int Order = Order_;
        virtual ~NodeElement() {}

        virtual void make(Node &) const = 0;
    };

    template <typename T_, int PhysicalDim_ = 1>
    class Node1 final : public NodeElement<T_, 0, PhysicalDim_> {
    public:
        static const int Order = 1;
        static const int Dim = 0;
        static const int PhysicalDim = PhysicalDim_;
        static const int NNodes = 1;

        using T = T_;

        using Vector = moonolith::Vector<T, Dim>;
        using CoVector = moonolith::Vector<T, PhysicalDim>;

        using Point = moonolith::Vector<T, Dim>;
        using CoPoint = moonolith::Vector<T, PhysicalDim>;

        using Super = NodeElement<T_, 0, PhysicalDim_>;
        using Node = typename Super::Node;


    public:
        T fun(const Integer, const Point &) const override { return 1.0; }

        void hessian(const Integer, const Point &, std::array<T, Dim * Dim> &H) {
            std::fill(std::begin(H), std::end(H), 0.0);
        }

        void grad(const Integer, const Point &, Vector &) const override {}

        void jacobian(const Point &, std::array<T, PhysicalDim * Dim> &) const override {}

        void affine_approx_jacobian(std::array<T, PhysicalDim * Dim> &) const override {}

        CoPoint &node(const Integer) override { return nodes_[0]; }

        const CoPoint &node(const Integer) const override { return nodes_[0]; }

        void point(const Point &, CoPoint &q) const override { q = nodes_[0]; }

        Node1() {}

        bool is_affine() const override { return true; }
        inline bool is_simplex() const override { return true; }

        inline int n_nodes() const override { return NNodes; }
        const std::array<CoPoint, 1> &nodes() const { return nodes_; }
        inline int order() const override { return Order; }

        // const Fun &fun() const { return fun_; }

        void make_reference() { nodes_[0].set(0.0); }

        inline ElemType type() const override { return NODE1; }

        inline T measure() const override { return 0.0; }

        inline T reference_measure() const override { return 1.0; }

        inline void make(Node &out) const override { out = nodes_[0]; }

    private:
        std::array<CoPoint, 1> nodes_;
    };

    template <typename T, int PhysicalDim>
    class Reference<Node1<T, PhysicalDim>> {
    public:
        static const int NNodes = 1;

        template <class Point, std::size_t Size>
        static void points(std::array<Point, Size> &nodes) {
            fill(nodes[0], 0.0);
        }

        static T measure() { return 1.0; }
    };

    template <typename T, int PhysicalDim>
    inline void make(const Node1<T, PhysicalDim> &elem, Vector<T, PhysicalDim> &node) {
        elem.make(node);
    }

    // template<typename T, int PhysicalDim>
    // class GaussQRule< Node1<T, PhysicalDim> > {
    // public:
    //     static bool get(const Integer order, Quadrature1<T> &q) {
    //         return Gauss::get(order, q);
    //     }
    // };
}  // namespace moonolith

#endif  // MOONOLITH_ELEM_NODE1_HPP
