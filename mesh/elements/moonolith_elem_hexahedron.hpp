// #ifndef MOONOLITH_ELEM_HEXAHEDRON_HPP
// #define MOONOLITH_ELEM_HEXAHEDRON_HPP

// #include "moonolith_elem.hpp"
// #include "moonolith_elem_segment.hpp"
// #include "moonolith_convex_decomposition.hpp"

// namespace moonolith {


//     template<typename T, int Dim, std::size_t Size>
//     inline T approx_hexahedron_volume(const std::array<Vector<T, Dim>, Size> &points)
//     {
//         static_assert(Size >= 8, "must be a hexahedron");
//         Vector<T, Dim> u = points[1] - points[0];
//         T l1 = length(u);

//         u = points[3] - points[0];
//         T l2 = length(u);

//         u = points[4] - points[0];
//         T l3 = length(u);
//         return l1*l2*l3;
//     }

//     template<typename T, int Order_, int PhysicalDim_ = 3>
//     class Hexahedron : public Elem<T, 3, PhysicalDim_> {};

//     //Only affine case
//     template<typename T_, int PhysicalDim_>
//     class Hexahedron<T_, 0, PhysicalDim_> final : public Elem<T_, 3, PhysicalDim_> {
//     public:
//         static const int Order = 0;
//         static const int Dim   = 3;
//         static const int PhysicalDim = PhysicalDim_;
//         static const int NNodes = 1;

//         using T = T_;

//         using Vector   = moonolith::Vector<T, Dim>;
//         using CoVector = moonolith::Vector<T, PhysicalDim>;

//         using Point   = moonolith::Vector<T, Dim>;
//         using CoPoint = moonolith::Vector<T, PhysicalDim>;

//     private:

//         class Grad final {
//         public:
//             Grad()
//             {   
//                 f[0] = [](const Point &, Vector &g) {
//                     g.x = 0.0;
//                     g.y = 0.0;
//                     g.z = 0.0;
//                 };
//             }

//             std::array<std::function<void (const Point &, Vector &)>, NNodes> f;
//         };

//         class Fun final {
//         public:

//             Fun()
//             {
//                 f[0] = [](const Point &) -> T {
//                     return 1.0;
//                 };
//             }

//             std::array<std::function<T (const Point &)>, NNodes> f;
//         };

//     public:

//         T fun(const Integer i, const Point &p) const override
//         {
//             return fun_.f[i](p);
//         }

//         void grad(const Integer i, const Point &p, Vector &g) const override
//         {
//             grad_.f[i](p, g);
//         }

//        void hessian(const Integer i, const Point &p, std::array<T, Dim*Dim> &H)
//        {
//            std::fill(std::begin(H), std::end(H), 0.0);
//        }

//         void jacobian(const Point &p, std::array<T, PhysicalDim*Dim> &J) const override
//         {
//             (void)p;
//             affine_approx_jacobian(J);
//         }

//         void affine_approx_jacobian(std::array<T, PhysicalDim*Dim> &J) const override
//         {
//             Affine<Hexahedron<T, 1, PhysicalDim>>::jacobian(points_, {0, 1, 3, 4}, J);
//         }

//         CoPoint &node(const Integer i) override
//         {
//             return nodes_[i];
//         }

//         const CoPoint &node(const Integer i) const override
//         {
//             return nodes_[i];
//         }

//         void point(const Point &p, CoPoint &q) const override
//         {
//             Affine<Hexahedron>::point(points_, {0, 1, 3, 4}, p, q);
//         }

//         inline CoPoint &point(const Integer idx)
//         {
//             return points_[idx];
//         }

//         inline const CoPoint &point(const Integer idx) const
//         {
//             return points_[idx];
//         }


//         Hexahedron() {}

//         // bool is_affine() const override { return is_affine_; }
//         // void set_affine(const bool val) { is_affine_ = val; }

//         bool is_affine() const override { return true; }

//         inline int n_nodes() const override { return NNodes; }
//         inline int order() const override { return Order; }

//         const std::array<CoPoint, NNodes> &nodes() const { return nodes_; }
//         const std::array<CoPoint, 8> &points() const { return points_; }

//         inline void make_reference()
//         {
//             Reference<Hexahedron>::points(nodes_);
//             Reference<Hexahedron<T, 1, PhysicalDim>>::points(points_);
//         }

//         inline ElemType type() const override
//         {
//             return HEX1;
//         }

//         inline bool is_simplex() const override { return false; }

//         inline T approx_measure() const override
//         {
//             return approx_hexahedron_volume(points_);
//         }

//         inline T measure() const override
//         {
//             return approx_hexahedron_volume(points_);
//         }

//         inline T reference_measure() const override { return 1.0; }

//     private:
//         // bool is_affine_;
//         std::array<CoPoint, NNodes> nodes_;
//         std::array<CoPoint, 8> points_;
//         const Fun  fun_;
//         const Grad grad_;
//     };

//     template<typename T, int PhysicalDim>
//     class Reference<Hexahedron<T, 0, PhysicalDim>> {
//     public:
//         static const int NNodes = 1;
        
//         template<class Point, std::size_t Size>
//         static void points(std::array<Point, Size> &nodes)
//         {
//             static_assert(Size >= NNodes, "size must be at least NNodes");
//             ///////////////////////
//             //p0
//             nodes[0].x = 0.5;
//             nodes[0].y = 0.5;
//             nodes[0].z = 0.5;
//         }

//         static T measure() { return 1.0; }
//     };

//     template<typename T, int PhysicalDim>
//     class MakePolyhedron< Hexahedron<T, 0, PhysicalDim> > {
//     public:
//         static void apply(const Hexahedron<T, 0, PhysicalDim> &elem, Polyhedron<T> &poly)
//         {
//             assert(elem.is_affine());
//             ConvexDecomposition<T, PhysicalDim>::make_hex(elem.points(), 0, 1, 2, 3, 4, 5, 6, 7, poly);
//         }

//         static void apply(const Hexahedron<T, 0, PhysicalDim> &elem, Storage<Polyhedron<T>> &poly)
//         {
//             assert(elem.is_affine());
//             poly.resize(1);
//             apply(elem, poly[0]);
//         }

//         template<class Any>
//         static void apply(const Elem<T, 3, PhysicalDim> &e, Any &poly)
//         {
//             auto e_ptr = dynamic_cast<const Hexahedron<T, 0, PhysicalDim> *>(&e); assert(e_ptr);
//             if(e_ptr) {
//                 apply(*e_ptr, poly);
//             }
//         }
//     };



//     template<typename T_, int PhysicalDim_>
//     class Hexahedron<T_, 1, PhysicalDim_> final : public Elem<T_, 3, PhysicalDim_> {
//     public:
//         static const int Order = 1;
//         static const int Dim   = 3;
//         static const int PhysicalDim = PhysicalDim_;
//         static const int NNodes = 8;

//         using T = T_;

//         using Vector   = moonolith::Vector<T, Dim>;
//         using CoVector = moonolith::Vector<T, PhysicalDim>;

//         using Point   = moonolith::Vector<T, Dim>;
//         using CoPoint = moonolith::Vector<T, PhysicalDim>;

//     private:

//         class Grad final {
//         public:
//             Grad()
//             {   
//                 // f = (1.0 - p.x) * (1.0 - p.y) * (1.0 - p.z);
//                 f[0] = [](const Point &p, Vector &g) {
//                     g.x = -(1.0 - p.y) * (1.0 - p.z);
//                     g.y = -(1.0 - p.x) * (1.0 - p.z);
//                     g.z = -(1.0 - p.x) * (1.0 - p.y);
//                 };
    
//                 // f = p.x * (1.0 - p.y) * (1.0 - p.z);
//                 f[1] = [](const Point &p, Vector &g) {
//                     g.x = (1.0 - p.y) * (1.0 - p.z);
//                     g.y = -p.x * (1.0 - p.z);
//                     g.z = -p.x * (1.0 - p.y);
//                 };
    
//                 // f = p.x * p.y * (1.0 - p.z);
//                 f[2] = [](const Point &p, Vector &g) {
//                     g.x = p.y * (1.0 - p.z);
//                     g.y = p.x * (1.0 - p.z);
//                     g.z = -p.x * p.y;
//                 };
    
//                 // f = (1.0 - p.x) * p.y * (1.0 - p.z);
//                 f[3] = [](const Point &p, Vector &g) {
//                     g.x = - p.y * (1.0 - p.z);
//                     g.y = (1.0 - p.x) * (1.0 - p.z);
//                     g.z = -(1.0 - p.x) * p.y;
//                 };
    
//                 // f = (1.0 - p.x) * (1.0 - p.y) * p.z;
//                 f[4] = [](const Point &p, Vector &g) {
//                     g.x = -(1.0 - p.y) * p.z;
//                     g.y = -(1.0 - p.x) * p.z;
//                     g.z = (1.0 - p.x) * (1.0 - p.y);
//                 };
    
//                 // f = p.x * (1.0 - p.y) * p.z;
//                 f[5] = [](const Point &p, Vector &g) {
//                     g.x = (1.0 - p.y) * p.z;
//                     g.y = -p.x * p.z;
//                     g.z = p.x * (1.0 - p.y);
//                 };
    
//                 // f = p.x * p.y * p.z;
//                 f[6] = [](const Point &p, Vector &g) {
//                     g.x = p.y * p.z;
//                     g.y = p.x * p.z;
//                     g.z = p.x * p.y;
//                 };
    
//                 // f = (1.0 - p.x) * p.y * p.z;
//                 f[7] = [](const Point &p, Vector &g) {
//                     g.x = -p.y * p.z;
//                     g.y = (1.0 - p.x) * p.z;
//                     g.z = (1.0 - p.x) * p.y;
//                 };
//             }

//             std::array<std::function<void (const Point &, Vector &)>, NNodes> f;
//         };

//         class Fun final {
//         public:

//             Fun()
//             {
//                 f[0] = [](const Point &p) -> T {
//                     return (1.0 - p.x) * (1.0 - p.y) * (1.0 - p.z);
//                 };

//                 f[1] = [](const Point &p) -> T {
//                     return p.x * (1.0 - p.y) * (1.0 - p.z);
//                 };

//                 f[2] = [](const Point &p) -> T {
//                     return p.x * p.y * (1.0 - p.z);
//                 };

//                 f[3] = [](const Point &p) -> T {
//                     return (1.0 - p.x) * p.y * (1.0 - p.z);
//                 };

//                 f[4] = [](const Point &p) -> T {
//                     return (1.0 - p.x) * (1.0 - p.y) * p.z;
//                 };

//                 f[5] = [](const Point &p) -> T {
//                     return p.x * (1.0 - p.y) * p.z;
//                 };

//                 f[6] = [](const Point &p) -> T {
//                     return p.x * p.y * p.z;
//                 };

//                 f[7] = [](const Point &p) -> T {
//                     return (1.0 - p.x) * p.y * p.z;
//                 };

//             }

//             std::array<std::function<T (const Point &)>, NNodes> f;
//         };

//     public:

//         T fun(const Integer i, const Point &p) const override
//         {
//             return fun_.f[i](p);
//         }

//         void grad(const Integer i, const Point &p, Vector &g) const override
//         {
//             grad_.f[i](p, g);
//         }

//        void hessian(const Integer i, const Point &p, std::array<T, Dim*Dim> &H)
//        {
//            std::fill(std::begin(H), std::end(H), 0.0);
//        }

//         void jacobian(const Point &p, std::array<T, PhysicalDim*Dim> &J) const override
//         {
//             if(is_affine()) {
//                 affine_approx_jacobian(J);
//             } else {
//                 IsoParametric<Hexahedron>::jacobian(*this, p, J);
//             }
//         }

//         void affine_approx_jacobian(std::array<T, PhysicalDim*Dim> &J) const override
//         {
//             Affine<Hexahedron>::jacobian(*this, {0, 1, 3, 4}, J);
//         }

//         CoPoint &node(const Integer i) override
//         {
//             return nodes_[i];
//         }

//         const CoPoint &node(const Integer i) const override
//         {
//             return nodes_[i];
//         }

//         void point(const Point &p, CoPoint &q) const override
//         {
//             IsoParametric<Hexahedron>::point(*this, p, q);
//         }

//         Hexahedron() : is_affine_(false) {}

//         bool is_affine() const override { return is_affine_; }

//         inline int n_nodes() const override { return NNodes; }
//         inline int order() const override { return Order; }

//         const std::array<CoPoint, NNodes> &nodes() const { return nodes_; }

//         inline void set_affine(const bool val) {
//             is_affine_ = val;
//         }

//         inline void make_reference()
//         {
//             Reference<Hexahedron>::points(nodes_);
//             set_affine(true);
//         }

//         inline ElemType type() const override
//         {
//             return HEX8;
//         }

//         inline bool is_simplex() const override { return false; }

//         inline T approx_measure() const override
//         {
//             if(is_affine()) {
//                 return approx_hexahedron_volume(nodes_);
//             } else {
//                 assert(false);
//                 return approx_hexahedron_volume(nodes_);
//             }
//         }

//         inline T measure() const override
//         {
//            if(is_affine()) {
//                return approx_hexahedron_volume(nodes_);
//            } else {
//                Quadrature<T, Dim> q;
//                Gauss::Hex::get(1, q);
//                return IsoParametric<Hexahedron>::measure(*this, q);
//            }
//         }

//         inline T reference_measure() const override { return 1.0; }

//     private:
//         bool is_affine_;
//         std::array<CoPoint, NNodes> nodes_;
//         const Fun  fun_;
//         const Grad grad_;
//     };

//     template<typename T, int PhysicalDim>
//     class MakePolyhedron< Hexahedron<T, 1, PhysicalDim> > {
//     public:
//         static void apply(const Hexahedron<T, 1, PhysicalDim> &elem, Polyhedron<T> &poly)
//         {
//             assert(elem.is_affine());
//             ConvexDecomposition<T, PhysicalDim>::make_hex(elem.nodes(), 0, 1, 2, 3, 4, 5, 6, 7, poly);
//         }

//         static void apply(const Hexahedron<T, 1, PhysicalDim> &elem, Storage<Polyhedron<T>> &poly)
//         {
//             if(elem.is_affine()) {
//                 poly.resize(1);
//                 apply(elem, poly[0]);
//             } else {
//                 // ConvexDecomposition<T, PhysicalDim>::split_hex8(elem.nodes(), poly);
//                 assert(false && "implement me");
//             }
//         }

//         template<class Any>
//         static void apply(const Elem<T, 3, PhysicalDim> &e, Any &poly)
//         {
//             auto e_ptr = dynamic_cast<const Hexahedron<T, 1, PhysicalDim> *>(&e); assert(e_ptr);
//             if(e_ptr) {
//                 apply(*e_ptr, poly);
//             }
//         }
//     };

//     template<typename T, int PhysicalDim>
//     class Reference<Hexahedron<T, 1, PhysicalDim>> {
//     public:
//         using Point = typename Hexahedron<T, 1, PhysicalDim>::Point;
//         static const int NNodes = 8;
        
//         template<std::size_t Size>
//         static void points(std::array<Point, Size> &nodes)
//         {
//             static_assert(Size >= NNodes, "size must be at least NNodes");
//             ///////////////////////
//             // Lower nodes

//             //p0
//             nodes[0].x = 0.0;
//             nodes[0].y = 0.0;
//             nodes[0].z = 0.0;

//             //p1
//             nodes[1].x = 1.0;
//             nodes[1].y = 0.0;
//             nodes[1].z = 0.0;

//             //p2
//             nodes[2].x = 1.0;
//             nodes[2].y = 1.0;
//             nodes[2].z = 0.0;

//             //p3
//             nodes[3].x = 0.0;
//             nodes[3].y = 1.0;
//             nodes[3].z = 0.0;

//             ///////////////////////

//             //Upper nodes

//             //p4
//             nodes[4].x = 0.0;
//             nodes[4].y = 0.0;
//             nodes[4].z = 1.0;

//             //p5
//             nodes[5].x = 1.0;
//             nodes[5].y = 0.0;
//             nodes[5].z = 1.0;

//             //p6
//             nodes[6].x = 1.0;
//             nodes[6].y = 1.0;
//             nodes[6].z = 1.0;

//             //p7
//             nodes[7].x = 0.0;
//             nodes[7].y = 1.0;
//             nodes[7].z = 1.0;
//         }

//         static T measure() { return 1.0; }
//     };



//     /////////////////////////////////////////////////////////////////////////////////////

//     template<typename T_, int PhysicalDim_>
//     class Hexahedron<T_, 2, PhysicalDim_> final : public Elem<T_, 3, PhysicalDim_> {
//     public:
//         static const int Order = 2;
//         static const int Dim   = 3;
//         static const int PhysicalDim = PhysicalDim_;
//         static const int NNodes = 27;

//         using T = T_;

//         using Vector   = moonolith::Vector<T, Dim>;
//         using CoVector = moonolith::Vector<T, PhysicalDim>;

//         using Point   = moonolith::Vector<T, Dim>;
//         using CoPoint = moonolith::Vector<T, PhysicalDim>;

//     private:

//         inline static T f0(const T &x) {
//             return 1.0 - 3.0 * x + 2.0 * (x * x);
//         }

//         inline static T f1(const T &x) {
//             return -x + 2.0 * (x * x);
//         }

//         inline static T f2(const T &x) {
//             return 4.0 * x - 4.0 * (x * x);
//         }

//         inline static T d0(const T &x) {
//             return -3.0 + 4.0 * x;
//         }

//         inline static T d1(const T &x) {
//             return -1.0 + 4.0 * x;
//         }

//         inline static T d2(const T &x) {
//             return 4.0 - 8.0 * x;
//         }

//         class Fun final {
//         public:
         
//             Fun()
//             {
//                 //corner nodes
//                 f[0] = [](const Point &p) -> T {
//                     return f0(p.x) * f0(p.y) * f0(p.z);
//                 };

//                 f[1] = [](const Point &p) -> T {
//                     return f1(p.x) * f0(p.y) * f0(p.z);
//                 };

//                 f[2] = [](const Point &p) -> T {
//                     return f1(p.x) * f1(p.y) * f0(p.z);
//                 };

//                 f[3] = [](const Point &p) -> T {
//                     return f0(p.x) * f1(p.y) * f0(p.z);
//                 };

//                 f[4] = [](const Point &p) -> T {
//                     return f0(p.x) * f0(p.y) * f1(p.z);
//                 };

//                 f[5] = [](const Point &p) -> T {
//                     return f1(p.x) * f0(p.y) * f1(p.z);
//                 };

//                 f[6] = [](const Point &p) -> T {
//                     return f1(p.x) * f1(p.y) * f1(p.z);
//                 };

//                 f[7] = [](const Point &p) -> T {
//                     return f0(p.x) * f1(p.y) * f1(p.z);
//                 };

//                 //edge nodes
//                 f[8] = [](const Point &p) -> T {
//                     return f2(p.x) * f0(p.y) * f0(p.z);
//                 };

//                 f[9] = [](const Point &p) -> T {
//                     return f1(p.x) * f2(p.y) * f0(p.z);
//                 };

//                 f[10] = [](const Point &p) -> T {
//                     return f2(p.x) * f1(p.y) * f0(p.z);
//                 };

//                 f[11] = [](const Point &p) -> T {
//                     return f0(p.x) * f2(p.y) * f0(p.z);
//                 };

//                 f[12] = [](const Point &p) -> T {
//                     return f0(p.x) * f0(p.y) * f2(p.z);
//                 };

//                 f[13] = [](const Point &p) -> T {
//                     return f1(p.x) * f0(p.y) * f2(p.z);
//                 };

//                 f[14] = [](const Point &p) -> T {
//                     return f1(p.x) * f1(p.y) * f2(p.z);
//                 };

//                 f[15] = [](const Point &p) -> T {
//                     return f0(p.x) * f1(p.y) * f2(p.z);
//                 };

//                 f[16] = [](const Point &p) -> T {
//                     return f2(p.x) * f0(p.y) * f1(p.z);
//                 };

//                 f[17] = [](const Point &p) -> T {
//                     return f1(p.x) * f2(p.y) * f1(p.z);
//                 };

//                 f[18] = [](const Point &p) -> T {
//                     return f2(p.x) * f1(p.y) * f1(p.z);
//                 };

//                 f[19] = [](const Point &p) -> T {
//                     return f0(p.x) * f2(p.y) * f1(p.z);
//                 };

//                 f[20] = [](const Point &p) -> T {
//                     return f2(p.x) * f2(p.y) * f0(p.z);
//                 };

//                 f[21] = [](const Point &p) -> T {
//                     return f2(p.x) * f0(p.y) * f2(p.z);
//                 };

//                 f[22] = [](const Point &p) -> T {
//                     return f1(p.x) * f2(p.y) * f2(p.z);
//                 };

//                 f[23] = [](const Point &p) -> T {
//                     return f2(p.x) * f1(p.y) * f2(p.z);
//                 };

//                 f[24] = [](const Point &p) -> T {
//                     return f0(p.x) * f2(p.y) * f2(p.z);
//                 };

//                 f[25] = [](const Point &p) -> T {
//                     return f2(p.x) * f2(p.y) * f1(p.z);
//                 };

//                 //internal
//                 f[26] = [](const Point &p) -> T {
//                     return f2(p.x) * f2(p.y) * f2(p.z);
//                 };
//             }

//             std::array<std::function<T (const Point &)>, NNodes> f;
//         };

//         class Grad final {
//         public:
//             Grad()
//             {   
//                //corner nodes
//                f[0] = [](const Point &p, Vector &g) {
//                     //f0(p.x) * f0(p.y) * f0(p.z)
//                     g.x = d0(p.x) * f0(p.y) * f0(p.z);
//                     g.y = f0(p.x) * d0(p.y) * f0(p.z);
//                     g.z = f0(p.x) * f0(p.y) * d0(p.z);
//                };

//                f[1] = [](const Point &p, Vector &g) {
//                     //f1(p.x) * f0(p.y) * f0(p.z)
//                     g.x = d1(p.x) * f0(p.y) * f0(p.z);
//                     g.y = f1(p.x) * d0(p.y) * f0(p.z);
//                     g.z = f1(p.x) * f0(p.y) * d0(p.z);
//                };

//                f[2] = [](const Point &p, Vector &g) {
//                     //f1(p.x) * f1(p.y) * f0(p.z)
//                     g.x = d1(p.x) * f1(p.y) * f0(p.z);
//                     g.y = f1(p.x) * d1(p.y) * f0(p.z);
//                     g.z = f1(p.x) * f1(p.y) * d0(p.z);
//                };

//                f[3] = [](const Point &p, Vector &g) {
//                     //f0(p.x) * f1(p.y) * f0(p.z)
//                     g.x = d0(p.x) * f1(p.y) * f0(p.z);
//                     g.y = f0(p.x) * d1(p.y) * f0(p.z);
//                     g.z = f0(p.x) * f1(p.y) * d0(p.z);
//                };

//                f[4] = [](const Point &p, Vector &g) {
//                     //f0(p.x) * f0(p.y) * f1(p.z)
//                     g.x = d0(p.x) * f0(p.y) * f1(p.z);
//                     g.y = f0(p.x) * d0(p.y) * f1(p.z);
//                     g.z = f0(p.x) * f0(p.y) * d1(p.z);
//                };

//                f[5] = [](const Point &p, Vector &g) {
//                     //f1(p.x) * f0(p.y) * f1(p.z)
//                     g.x = d1(p.x) * f0(p.y) * f1(p.z);
//                     g.y = f1(p.x) * d0(p.y) * f1(p.z);
//                     g.z = f1(p.x) * f0(p.y) * d1(p.z);
//                };

//                f[6] = [](const Point &p, Vector &g) {
//                     //f1(p.x) * f1(p.y) * f1(p.z)
//                     g.x = d1(p.x) * f1(p.y) * f1(p.z);
//                     g.y = f1(p.x) * d1(p.y) * f1(p.z);
//                     g.z = f1(p.x) * f1(p.y) * d1(p.z);
//                };

//                f[7] = [](const Point &p, Vector &g) {
//                     //f0(p.x) * f1(p.y) * f1(p.z)
//                     g.x = d0(p.x) * f1(p.y) * f1(p.z);
//                     g.y = f0(p.x) * d1(p.y) * f1(p.z);
//                     g.z = f0(p.x) * f1(p.y) * d1(p.z);
//                };

//                // //edge nodes
//                f[8] = [](const Point &p, Vector &g) {
//                     //f2(p.x) * f0(p.y) * f0(p.z)
//                     g.x = d2(p.x) * f0(p.y) * f0(p.z);
//                     g.y = f2(p.x) * d0(p.y) * f0(p.z);
//                     g.z = f2(p.x) * f0(p.y) * d0(p.z);
//                };

//                f[9] = [](const Point &p, Vector &g) {
//                     //f1(p.x) * f2(p.y) * f0(p.z)
//                     g.x = d1(p.x) * f2(p.y) * f0(p.z);
//                     g.y = f1(p.x) * d2(p.y) * f0(p.z);
//                     g.z = f1(p.x) * f2(p.y) * d0(p.z);
//                };

//                f[10] = [](const Point &p, Vector &g) {
//                     //f2(p.x) * f1(p.y) * f0(p.z);
//                     g.x = d2(p.x) * f1(p.y) * f0(p.z);
//                     g.y = f2(p.x) * d1(p.y) * f0(p.z);
//                     g.z = f2(p.x) * f1(p.y) * d0(p.z);
//                };

//                f[11] = [](const Point &p, Vector &g) {
//                     //f0(p.x) * f2(p.y) * f0(p.z);
//                     g.x = d0(p.x) * f2(p.y) * f0(p.z);
//                     g.y = f0(p.x) * d2(p.y) * f0(p.z);
//                     g.z = f0(p.x) * f2(p.y) * d0(p.z);
//                };

//                f[12] = [](const Point &p, Vector &g) {
//                     //f0(p.x) * f0(p.y) * f2(p.z);
//                     g.x = d0(p.x) * f0(p.y) * f2(p.z);
//                     g.y = f0(p.x) * d0(p.y) * f2(p.z);
//                     g.z = f0(p.x) * f0(p.y) * d2(p.z);
//                };

//                f[13] = [](const Point &p, Vector &g) {
//                     //f1(p.x) * f0(p.y) * f2(p.z);
//                     g.x = d1(p.x) * f0(p.y) * f2(p.z);
//                     g.y = f1(p.x) * d0(p.y) * f2(p.z);
//                     g.z = f1(p.x) * f0(p.y) * d2(p.z);
//                };

//                f[14] = [](const Point &p, Vector &g) {
//                     //f1(p.x) * f1(p.y) * f2(p.z);
//                     g.x = d1(p.x) * f1(p.y) * f2(p.z);
//                     g.y = f1(p.x) * d1(p.y) * f2(p.z);
//                     g.z = f1(p.x) * f1(p.y) * d2(p.z);
//                };

//                f[15] = [](const Point &p, Vector &g) {
//                     //f0(p.x) * f1(p.y) * f2(p.z);
//                     g.x = d0(p.x) * f1(p.y) * f2(p.z);
//                     g.y = f0(p.x) * d1(p.y) * f2(p.z);
//                     g.z = f0(p.x) * f1(p.y) * d2(p.z);
//                };

//                f[16] = [](const Point &p, Vector &g) {
//                     //f2(p.x) * f0(p.y) * f1(p.z);
//                     g.x = d2(p.x) * f0(p.y) * f1(p.z);
//                     g.y = f2(p.x) * d0(p.y) * f1(p.z);
//                     g.z = f2(p.x) * f0(p.y) * d1(p.z);
//                };

//                f[17] = [](const Point &p, Vector &g) {
//                     //f1(p.x) * f2(p.y) * f1(p.z);
//                     g.x = d1(p.x) * f2(p.y) * f1(p.z);
//                     g.y = f1(p.x) * d2(p.y) * f1(p.z);
//                     g.z = f1(p.x) * f2(p.y) * d1(p.z);
//                };

//                f[18] = [](const Point &p, Vector &g) {
//                     //f2(p.x) * f1(p.y) * f1(p.z);
//                     g.x = d2(p.x) * f1(p.y) * f1(p.z);
//                     g.y = f2(p.x) * d1(p.y) * f1(p.z);
//                     g.z = f2(p.x) * f1(p.y) * d1(p.z);
//                };

//                f[19] = [](const Point &p, Vector &g) {
//                     //f0(p.x) * f2(p.y) * f1(p.z);
//                     g.x = d0(p.x) * f2(p.y) * f1(p.z);
//                     g.y = f0(p.x) * d2(p.y) * f1(p.z);
//                     g.z = f0(p.x) * f2(p.y) * d1(p.z);
//                };

//                f[20] = [](const Point &p, Vector &g) {
//                     //f2(p.x) * f2(p.y) * f0(p.z);
//                     g.x = d2(p.x) * f2(p.y) * f0(p.z);
//                     g.y = f2(p.x) * d2(p.y) * f0(p.z);
//                     g.z = f2(p.x) * f2(p.y) * d0(p.z);
//                };

//                f[21] = [](const Point &p, Vector &g) {
//                     //f2(p.x) * f0(p.y) * f2(p.z);
//                     g.x = d2(p.x) * f0(p.y) * f2(p.z);
//                     g.y = f2(p.x) * d0(p.y) * f2(p.z);
//                     g.z = f2(p.x) * f0(p.y) * d2(p.z);
//                };

//                f[22] = [](const Point &p, Vector &g) {
//                     //f1(p.x) * f2(p.y) * f2(p.z);
//                     g.x = d1(p.x) * f2(p.y) * f2(p.z);
//                     g.y = f1(p.x) * d2(p.y) * f2(p.z);
//                     g.z = f1(p.x) * f2(p.y) * d2(p.z);
//                };

//                f[23] = [](const Point &p, Vector &g) {
//                     //f2(p.x) * f1(p.y) * f2(p.z);
//                     g.x = d2(p.x) * f1(p.y) * f2(p.z);
//                     g.y = f2(p.x) * d1(p.y) * f2(p.z);
//                     g.z = f2(p.x) * f1(p.y) * d2(p.z);
//                };

//                f[24] = [](const Point &p, Vector &g) {
//                     //f0(p.x) * f2(p.y) * f2(p.z);
//                     g.x = d0(p.x) * f2(p.y) * f2(p.z);
//                     g.y = f0(p.x) * d2(p.y) * f2(p.z);
//                     g.z = f0(p.x) * f2(p.y) * d2(p.z);
//                };

//                f[25] = [](const Point &p, Vector &g) {
//                     //f2(p.x) * f2(p.y) * f1(p.z);
//                     g.x = d2(p.x) * f2(p.y) * f1(p.z);
//                     g.y = f2(p.x) * d2(p.y) * f1(p.z);
//                     g.z = f2(p.x) * f2(p.y) * d1(p.z);
//                };

//                //internal
//                f[26] = [](const Point &p, Vector &g) {
//                     //f2(p.x) * f2(p.y) * f2(p.z);
//                     g.x = d2(p.x) * f2(p.y) * f2(p.z);
//                     g.y = f2(p.x) * d2(p.y) * f2(p.z);
//                     g.z = f2(p.x) * f2(p.y) * d2(p.z);
//                };
//             }

//             std::array<std::function<void (const Point &, Vector &)>, NNodes> f;
//         };

//     public:

//         T fun(const Integer i, const Point &p) const override
//         {
//             return fun_.f[i](p);
//         }

//         void grad(const Integer i, const Point &p, Vector &g) const override
//         {
//             grad_.f[i](p, g);
//         }

//        void hessian(const Integer i, const Point &p, std::array<T, Dim*Dim> &H)
//        {
//            std::fill(std::begin(H), std::end(H), 0.0);
//        }

//         void jacobian(const Point &p, std::array<T, PhysicalDim*Dim> &J) const override
//         {
//             if(is_affine()) {
//                 affine_approx_jacobian(J);
//             } else {
//                 IsoParametric<Hexahedron>::jacobian(*this, p, J);
//             }
//         }

//         void affine_approx_jacobian(std::array<T, PhysicalDim*Dim> &J) const override
//         {
//             Affine<Hexahedron>::jacobian(*this, {0, 1, 3, 4}, J);
//         }

//         CoPoint &node(const Integer i) override
//         {
//             return nodes_[i];
//         }

//         const CoPoint &node(const Integer i) const override
//         {
//             return nodes_[i];
//         }

//         void point(const Point &p, CoPoint &q) const override
//         {
//             IsoParametric<Hexahedron>::point(*this, p, q);
//         }

//         Hexahedron() : is_affine_(false) {}

//         bool is_affine() const override { return is_affine_; }

//         inline int n_nodes() const override { return NNodes; }
//         inline int order() const override { return Order; }

//         const std::array<CoPoint, NNodes> &nodes() const { return nodes_; }

//         inline void set_affine(const bool val) {
//             is_affine_ = val;
//         }

//         inline void make_reference()
//         {
//             Reference<Hexahedron>::points(nodes_);
//             set_affine(true);
//         }

//         inline ElemType type() const override
//         {
//             return HEX27;
//         }

//         inline bool is_simplex() const override { return false; }

//         inline T approx_measure() const override
//         {
//             if(is_affine()) {
//                 return approx_hexahedron_volume(nodes_);
//             } else {
//                 assert(false);
//                 return approx_hexahedron_volume(nodes_);
//             }
//         }

//         inline T measure() const override
//         {
//             if(is_affine()) {
//                 return approx_hexahedron_volume(nodes_);
//             } else {
//                 Quadrature<T, Dim> q;
//                 Gauss::Hex::get(2, q);
//                 return IsoParametric<Hexahedron>::measure(*this, q);
//             }
//         }

//         inline T reference_measure() const override { return 1.0; }

//     private:
//         bool is_affine_;
//         std::array<CoPoint, NNodes> nodes_;
//         const Fun  fun_;
//         const Grad grad_;
//     };

//     template<typename T, int PhysicalDim>
//     class Reference<Hexahedron<T, 2, PhysicalDim>> {
//     public:
//         static const int NNodes = 27;
        
//         template<class Point, std::size_t Size>
//         static void points(std::array<Point, Size> &nodes)
//         {
//             static_assert(Size >= NNodes, "size must be at least NNodes");
//             ///////////////////////

//             Reference<Hexahedron<T, 1, PhysicalDim>>::points(nodes);
            
//             //p8
//             nodes[8].x = 0.5;
//             nodes[8].y = 0.0;
//             nodes[8].z = 0.0;

//             //p9
//             nodes[9].x = 1.0;
//             nodes[9].y = 0.5;
//             nodes[9].z = 0.0;

//             //p10
//             nodes[10].x = 0.5;
//             nodes[10].y = 1.0;
//             nodes[10].z = 0.0;

//             //p11
//             nodes[11].x = 0.0;
//             nodes[11].y = 0.5;
//             nodes[11].z = 0.0;

//             ///////////////////////


//             //p12
//             nodes[12].x = 0.0;
//             nodes[12].y = 0.0;
//             nodes[12].z = 0.5;

//             //p13
//             nodes[13].x = 1.0;
//             nodes[13].y = 0.0;
//             nodes[13].z = 0.5;

//             //p14
//             nodes[14].x = 1.0;
//             nodes[14].y = 1.0;
//             nodes[14].z = 0.5;

//             //p15
//             nodes[15].x = 0.0;
//             nodes[15].y = 1.0;
//             nodes[15].z = 0.5;


//             ///////////////////////


//             //p16
//             nodes[16].x = 0.5;
//             nodes[16].y = 0.0;
//             nodes[16].z = 1.0;

//             //p17
//             nodes[17].x = 1.0;
//             nodes[17].y = 0.5;
//             nodes[17].z = 1.0;

//             //p18
//             nodes[18].x = 0.5;
//             nodes[18].y = 1.0;
//             nodes[18].z = 1.0;

//             //p19
//             nodes[19].x = 0.0;
//             nodes[19].y = 0.5;
//             nodes[19].z = 1.0;


//             ///////////////////////

//             //p20
//             nodes[20].x = 0.5;
//             nodes[20].y = 0.5;
//             nodes[20].z = 0.0;

//             //p21
//             nodes[21].x = 0.5;
//             nodes[21].y = 0.0;
//             nodes[21].z = 0.5;

//             //p22
//             nodes[22].x = 1.0;
//             nodes[22].y = 0.5;
//             nodes[22].z = 0.5;

//             //p23
//             nodes[23].x = 0.5;
//             nodes[23].y = 1.0;
//             nodes[23].z = 0.5;

//             ///////////////////////


//             //p24
//             nodes[24].x = 0.0;
//             nodes[24].y = 0.5;
//             nodes[24].z = 0.5;

//             //p25
//             nodes[25].x = 0.5;
//             nodes[25].y = 0.5;
//             nodes[25].z = 1.0;

//             //p26
//             nodes[26].x = 0.5;
//             nodes[26].y = 0.5;
//             nodes[26].z = 0.5;
//         }

//         static T measure() { return 1.0; }
//     };

//     template<typename T, int PhysicalDim>
//     class MakePolyhedron< Hexahedron<T, 2, PhysicalDim> > {
//     public:
//         static void apply(const Hexahedron<T, 2, PhysicalDim> &elem, Polyhedron<T> &poly)
//         {
//             assert(elem.is_affine());
//             ConvexDecomposition<T, PhysicalDim>::make_hex(elem.nodes(), 0, 1, 2, 3, 4, 5, 6, 7, poly);
//         }

//         static void apply(const Hexahedron<T, 2, PhysicalDim> &elem, Storage<Polyhedron<T>> &poly)
//         {
//             if(elem.is_affine()) {
//                 poly.resize(1);
//                 apply(elem, poly[0]);
//             } else {
//                 ConvexDecomposition<T, PhysicalDim>::decompose_hex27(elem.nodes(), poly);
//             }
//         }

//         template<class Any>
//         static void apply(const Elem<T, 3, PhysicalDim> &e, Any &poly)
//         {
//             auto e_ptr = dynamic_cast<const Hexahedron<T, 2, PhysicalDim> *>(&e); assert(e_ptr);
//             if(e_ptr) {
//                 apply(*e_ptr, poly);
//             }
//         }
//     };

//     template<typename T, int Order, int PhysicalDim>
//     class GaussQRule< Hexahedron<T, Order, PhysicalDim> > {
//     public:
//         static bool get(const Integer order, Quadrature3<T> &q) {
//             return Gauss::Hex::get(order, q);
//         }
//     };
// }

// #endif //MOONOLITH_ELEM_HEXAHEDRON_HPP
