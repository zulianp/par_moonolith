#ifndef MOONOLITH_ELEM_TYPE_HPP
#define MOONOLITH_ELEM_TYPE_HPP

#include <iostream>

namespace moonolith {

    enum ElemType {
        EDGE2 = 0,
        EDGE3 = 1,
        TRI3 = 2,
        TRI6 = 3,
        QUAD4 = 4,
        QUAD8 = 5,
        TET4 = 6,
        TET10 = 10,
        HEX8 = 11,
        HEX27 = 12,
        HEX1 = 13,
        TET1 = 14,
        TRI1 = 15,
        QUAD1 = 16,
        EDGE1 = 17,
        QUAD9 = 18,
        PRISM6 = 19,
        NODE1 = 20,
        INVALID
    };

    using FEType = moonolith::ElemType;

    inline std::ostream &operator<<(std::ostream &os, ElemType t) {
        switch (t) {
            case EDGE2: {
                os << "EDGE2";
                break;
            }
            case EDGE3: {
                os << "EDGE3";
                break;
            }
            case TRI3: {
                os << "TRI3";
                break;
            }
            case TRI6: {
                os << "TRI6";
                break;
            }
            case QUAD4: {
                os << "QUAD4";
                break;
            }
            case QUAD8: {
                os << "QUAD8";
                break;
            }
            case TET4: {
                os << "TET4";
                break;
            }
            case TET10: {
                os << "TET10";
                break;
            }
            case HEX8: {
                os << "HEX8";
                break;
            }
            case HEX27: {
                os << "HEX27";
                break;
            }
            case HEX1: {
                os << "HEX1";
                break;
            }
            case TET1: {
                os << "TET1";
                break;
            }
            case TRI1: {
                os << "TRI1";
                break;
            }
            case QUAD1: {
                os << "QUAD1";
                break;
            }
            case EDGE1: {
                os << "EDGE1";
                break;
            }
            case QUAD9: {
                os << "QUAD9";
                break;
            }
            case PRISM6: {
                os << "PRISM6";
                break;
            }
            case NODE1: {
                os << "NODE1";
                break;
            }
            case INVALID: {
                os << "INVALID";
                break;
            }
            default: {
                os << "UNDEFINED";
                break;
            }
        }

        return os;
    }
}  // namespace moonolith

#endif  // MOONOLITH_ELEM_TYPE_HPP
