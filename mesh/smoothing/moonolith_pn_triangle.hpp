#ifndef MOONOLITH_PN_TRIANGLE_HPP
#define MOONOLITH_PN_TRIANGLE_HPP

#include <array>
#include "moonolith_matlab_scripter.hpp"
#include "moonolith_vector.hpp"

namespace moonolith {

    ///@brief from: Curved PN Triangles, Vlachos et al.
    template <typename T, int Dim>
    class PNTriangle {
    public:
        using Point2 = moonolith::Vector<T, 2>;
        using Point = moonolith::Vector<T, Dim>;
        using Vector = moonolith::Vector<T, Dim>;
        static const std::size_t N = 10;

        void init(const std::array<Point, 3> &points, const std::array<Vector, 3> &normals) {
            static const std::size_t I[6] = {0, 1, 0, 1, 2, 2};
            static const std::size_t J[6] = {1, 0, 2, 2, 0, 1};

            B[0] = points[0];
            B[1] = points[1];
            B[2] = points[2];

            V = B[0] + B[1] + B[2];
            V /= 3.0;

            fill(E, static_cast<T>(0.0));

            for (std::size_t q = 0; q < 6; ++q) {
                const auto i = I[q];
                const auto j = J[q];
                const auto k = q + 3;

                auto w = dot(points[j] - points[i], normals[i]);

                B[k] = 2. * points[j] + points[i] - w * normals[j];
                B[k] /= 3.0;
                E += B[k];
            }

            E /= 6.0;
            B[9] = E + (E - V) / 2.0;
        }

        void apply(const Point2 &x, Point &p) {
            compute_shape(x);

            fill(p, static_cast<T>(0.0));

            for (std::size_t i = 0; i < N; ++i) {
                p += U[i] * B[i];
            }
        }

        const std::array<Point, N> &points() const { return B; }

    private:
        std::array<T, N> U;
        std::array<Point, N> B;
        Point E, V;

        void compute_shape(const Point2 &x) {
            const auto u = x.x;
            const auto v = x.y;
            const auto w = 1.0 - u - v;

            auto u2 = u * u;
            auto u3 = u2 * u;

            auto v2 = v * v;
            auto v3 = v2 * v;

            auto w2 = w * w;
            auto w3 = w2 * w;

            U[0] = w3;
            U[1] = u3;
            U[2] = v3;
            U[3] = 3.0 * w2 * u;
            U[4] = 3.0 * w * u2;
            U[5] = 3.0 * w2 * v;
            U[6] = 3.0 * u2 * v;
            U[7] = 3.0 * w * v2;
            U[8] = 3.0 * u * v2;
            U[9] = 6.0 * u * v * w;
        }
    };

    template <typename T, int Dim>
    void write(MatlabScripter &script,
               const std::array<Vector<T, Dim>, 3> &points,
               const std::array<Vector<T, Dim>, 3> &normals,
               const PNTriangle<T, Dim> &tri) {
        script.hold_on();
        script.plot_polygon(points, "g*-");
        script.quiver(points, normals);
        script.plot(tri.points(), "r*");
    }

    template <typename T, int Dim>
    bool write(const std::string &path,
               const std::array<Vector<T, Dim>, 3> &points,
               const std::array<Vector<T, Dim>, 3> &normals,
               const PNTriangle<T, Dim> &tri) {
        MatlabScripter script;
        script.close_all();
        write(script, points, normals, tri);
        script.axis_equal();
        return script.save(path);
    }
}  // namespace moonolith

#endif  // MOONOLITH_PN_TRIANGLE_HPP
