#ifndef MOONOLITH_ELEM_PRISM_HPP
#define MOONOLITH_ELEM_PRISM_HPP

#include "moonolith_elem.hpp"
#include "moonolith_polyhedral_element.hpp"
#include "moonolith_gauss_quadrature_rule.hpp"

namespace moonolith {
    
    template<typename T, int Order_, int PhysicalDim_ = 3>
    class Prism : public PolyhedralElement<T, PhysicalDim_> {
    public:
        virtual ~Prism() {}
    };

    template<typename T, int Order, int PhysicalDim>
    class GaussQRule< Prism<T, Order, PhysicalDim> > {
    public:
        static bool get(const Integer order, Quadrature3<T> &q) {
            return Gauss::Prism::get(order, q);
        }
    };

}

#endif //MOONOLITH_ELEM_PRISM_HPP
