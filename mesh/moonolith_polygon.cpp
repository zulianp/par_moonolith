#include "moonolith_polygon_impl.hpp"

namespace moonolith {
    template class Polygon<Real, 2>;
    template class Polygon<Real, 3>;
}  // namespace moonolith
