
#ifndef MOONOLITH_OUTPUT_STREAM_HPP
#define MOONOLITH_OUTPUT_STREAM_HPP

#include "moonolith_serializable.hpp"
#include "moonolith_stream.hpp"

#include <assert.h>
#include <map>
#include <vector>

namespace moonolith {

    class OutputStream : public moonolith::Stream {
    public:
        virtual ~OutputStream() {}
        using moonolith::Stream::operator&;

        template <typename T>
        OutputStream &operator<<(const T &obj) {
            this->operator&(const_cast<T &>(obj));
            return *this;
        }

        template <typename T>
        friend OutputStream &operator>>(const T &obj, OutputStream &os) {
            os << obj;
            return os;
        }

        template <typename Iterator>
        OutputStream &write(const Iterator &begin, const Iterator &end) {
            if (begin == end) return *this;

            request_space(static_cast<Integer>(std::distance(begin, end)), *begin);
            for (Iterator it = begin; it != end; ++it) {
                (*this) << *it;
            }
            return *this;
        }

        template <typename Key, typename Value>
        OutputStream &write(const std::map<Key, Value> &map) {
            const Integer size = map.size();
            (*this) << size;
            write(map.begin(), map.end());
            return *this;
        }

        template <typename T>
        OutputStream &write(const T *array, const Integer size) {
            apply(const_cast<T *>(array), size);
            return *this;
        }

        virtual OutputStream &write(const Serializable *array, const Integer size) {
            for (Integer i = 0; i < size; ++i) {
                (*this) << array[i];
            }

            return *this;
        }

        inline OutputStream &write(const Serializable &obj) {
            obj.write(*this);
            return *this;
        }

        template <typename T>
        inline void request_space(const Integer nBytes, const T &) {
            request_space(nBytes * sizeof(T));
        }

        virtual void request_space(const Integer nBytes) = 0;
        virtual void reserve(const Integer nBytes) = 0;
        virtual void resize(const Integer nBytes) = 0;
        virtual void clear() = 0;
        virtual bool good() = 0;
        virtual Integer size() const = 0;
        virtual Integer allocated_size() const = 0;
    };

    class ByteOutputStream : public OutputStream {
    public:
        typedef std::vector<moonolith::byte> StorageType;

    private:
        StorageType *_storage;

    public:
        ByteOutputStream(StorageType *storage = NULL, Integer capacity = 0) : OutputStream(), _storage(storage) {
            if (_storage && capacity) {
                _storage->reserve(capacity);
            }
        }

        ByteOutputStream(StorageType &storage) : OutputStream(), _storage(&storage) {}

        virtual ~ByteOutputStream() {}

        inline void set_storage(StorageType *storage) {
            assert(storage);
            _storage = storage;
        }

        inline StorageType &storage() {
            assert(_storage);
            return *_storage;
        }

        inline const StorageType &storage() const {
            assert(_storage);
            return *_storage;
        }

        inline ByteOutputStream &operator&(Serializable &obj) {
            obj.write(*this);
            return *this;
        }
        inline ByteOutputStream &operator&(int &obj) {
            apply_aux(obj);
            return *this;
        }
        inline ByteOutputStream &operator&(unsigned int &obj) {
            apply_aux(obj);
            return *this;
        }
        inline ByteOutputStream &operator&(short &obj) {
            apply_aux(obj);
            return *this;
        }
        inline ByteOutputStream &operator&(unsigned short &obj) {
            apply_aux(obj);
            return *this;
        }
        inline ByteOutputStream &operator&(long &obj) {
            apply_aux(obj);
            return *this;
        }
        inline ByteOutputStream &operator&(unsigned long &obj) {
            apply_aux(obj);
            return *this;
        }
        inline ByteOutputStream &operator&(unsigned long long &obj) {
            apply_aux(obj);
            return *this;
        }
        inline ByteOutputStream &operator&(float &obj) {
            apply_aux(obj);
            return *this;
        }
        inline ByteOutputStream &operator&(double &obj) {
            apply_aux(obj);
            return *this;
        }
        inline ByteOutputStream &operator&(char &obj) {
            apply_aux(obj);
            return *this;
        }
        inline ByteOutputStream &operator&(unsigned char &obj) {
            apply_aux(obj);
            return *this;
        }
        inline ByteOutputStream &operator&(bool &obj) {
            apply_aux(obj);
            return *this;
        }

        inline ByteOutputStream &apply(int *array, const Integer nElements) {
            apply_aux(array, nElements);
            return *this;
        }
        inline ByteOutputStream &apply(unsigned int *array, const Integer nElements) {
            apply_aux(array, nElements);
            return *this;
        }
        inline ByteOutputStream &apply(short *array, const Integer nElements) {
            apply_aux(array, nElements);
            return *this;
        }
        inline ByteOutputStream &apply(unsigned short *array, const Integer nElements) {
            apply_aux(array, nElements);
            return *this;
        }
        inline ByteOutputStream &apply(long *array, const Integer nElements) {
            apply_aux(array, nElements);
            return *this;
        }
        inline ByteOutputStream &apply(unsigned long *array, const Integer nElements) {
            apply_aux(array, nElements);
            return *this;
        }
        inline ByteOutputStream &apply(unsigned long long *array, const Integer nElements) {
            apply_aux(array, nElements);
            return *this;
        }
        inline ByteOutputStream &apply(float *array, const Integer nElements) {
            apply_aux(array, nElements);
            return *this;
        }
        inline ByteOutputStream &apply(double *array, const Integer nElements) {
            apply_aux(array, nElements);
            return *this;
        }
        inline ByteOutputStream &apply(char *array, const Integer nElements) {
            apply_aux(array, nElements);
            return *this;
        }
        inline ByteOutputStream &apply(unsigned char *array, const Integer nElements) {
            apply_aux(array, nElements);
            return *this;
        }
        inline ByteOutputStream &apply(bool *array, const Integer nElements) {
            apply_aux(array, nElements);
            return *this;
        }

#ifdef MOONOLITH_HAVE_QUAD_PRECISION
        inline ByteOutputStream &operator&(long double &obj) {
            apply_aux(obj);
            return *this;
        }
        inline ByteOutputStream &apply(long double *array, const Integer nElements) {
            apply_aux(array, nElements);
            return *this;
        }
#endif  // MOONOLITH_HAVE_QUAD_PRECISION

        // his method appears to have problems
        virtual ByteOutputStream &apply(Serializable *array, const Integer nElements) {
            for (Integer i = 0; i < nElements; ++i) {
                array[i].write(*this);
            }
            return *this;
        }

        inline void request_space(const Integer nBytes) {
            Integer newSize = nBytes + size();
            if (newSize > Integer(storage().capacity())) {
                storage().reserve(newSize);
            }
        }

        inline void reserve(const Integer nBytes) { storage().reserve(nBytes); }

        inline void resize(const Integer nBytes) {
            std::cout << __FILE__ << "resize called" << std::endl;
            storage().resize(nBytes);
        }

        virtual void clear() { storage().clear(); }

        virtual bool good() { return true; }

        virtual Integer size() const { return static_cast<Integer>(storage().size()); }

        inline Integer allocated_size() const { return static_cast<Integer>(storage().capacity()); }

        inline moonolith::byte *pointer() {
            assert(!storage().empty());
            return &storage()[0];
        }

        inline moonolith::byte *raw_ptr() {
            assert(!storage().empty());
            return &storage()[0];
        }

        inline void reset_offset() { storage().resize(0); }

    private:
        template <typename T>
        ByteOutputStream &apply_aux(const T *array, const Integer size) {
            using std::memcpy;

            if (!size) return *this;

            const std::size_t offset(storage().size());
            storage().resize(storage().size() + sizeof(T) * size);

            assert(storage().size() == offset + sizeof(T) * size);

            memcpy(&storage()[offset], array, sizeof(T) * size);

            return *this;
        }

        template <typename T>
        inline ByteOutputStream &apply_aux(const T &obj) {
            using std::memcpy;

            const std::size_t offset = storage().size();
            storage().resize(storage().size() + sizeof(T));

            assert(storage().size() == std::size_t(offset) + sizeof(T));
            memcpy(&storage()[offset], &obj, sizeof(T));
            return *this;
        }
    };

    class ByteOutputBuffer : public ByteOutputStream {
    private:
        typedef ByteOutputStream super;
        ByteOutputStream::StorageType _storage;

    public:
        ByteOutputBuffer() : super(), _storage() {
            set_storage(&_storage);
            assert(_storage.empty());
        }

        inline ByteOutputBuffer(const ByteOutputBuffer &other) : super(other), _storage(other._storage) {
            set_storage(&_storage);
        }

        inline ByteOutputBuffer &operator=(const ByteOutputBuffer &other) {
            super::operator=(other);
            _storage = other._storage;
            set_storage(&_storage);
            return *this;
        }

        inline bool empty() const { return _storage.empty(); }

        inline moonolith::byte &operator[](const int index) { return _storage[index]; }

        inline const moonolith::byte &operator[](const int index) const { return _storage[index]; }
    };

}  // namespace moonolith

#endif  // MOONOLITH_OUTPUT_STREAM_HPP
