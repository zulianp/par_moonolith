# Copyright 2013-2020 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class ParMoonolith(CMakePackage):
    """ParMoonolith: Library for parallel intersection detection, automatic load-balancing, and variational transfer."""

    homepage = "https://bitbucket.org/zulianp/par_moonolith"
    url      = "https://bitbucket.org/zulianp/par_moonolith/get/v1.tar.gz"
    git      = "https://bitbucket.org/zulianp/par_moonolith/"

    maintainers = ['patrick zulian']
    install_targets = ['install_all']

    #####################################
    # VERSIONS
    #####################################

    version('1', sha256='6854b4001c8db95e1088f9bd3db85014c8a011d6fcfd3f81741ba4f8813037c3')
    version('development', branch='development')
    version('master', branch='master')

    #####################################
    # DEPENDENCIES
    #####################################

    depends_on('cmake', type='build')
    depends_on('mpi')
    depends_on('lapack')

    #####################################
    # VARIANTS
    #####################################
    variant("shared", default=True, description="build as shared library")

    variant('precision', default='double', values=('single', 'double', 'quad'), multi=False, description='Switches between single and double precision')
    variant('int64',  default=True,  description='Index size')
    variant('build_type', default='release',  values=('debug', 'release', 'relwithdebinfo', 'minsizerel'), multi=False, description='Build type')
    variant('pic',  default=True,  description='Position independent code')

    #####################################

    def cmake_args(self):
        spec = self.spec
        variants = spec.variants;

        options = [
            self.define_from_variant('BUILD_SHARED_LIBS', 'shared'),
            self.define_from_variant('CMAKE_POSITION_INDEPENDENT_CODE', 'pic'),
            self.define_from_variant('CMAKE_BUILD_TYPE', 'build_type'),
            self.define("MOONOLITH_ENABLE_TESTING", False),
            self.define("MOONOLITH_ENABLE_BENCHMARK", False)
        ]

        options.append('-DMOONOLITH_HAVE_%s_PRECISION=ON' % (variants['precision'].value).upper())

        if "+int64" in spec:
            options.append("-DMOONOLITH_INDEX_SIZE=64")
        else:
            options.append("-DMOONOLITH_INDEX_SIZE=32")

        return options
