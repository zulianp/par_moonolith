#ifndef QUADRATURE_INTERFACE_CL
#define QUADRATURE_INTERFACE_CL 

#define QUAD_META_SIZE 5

typedef struct {
	Scalar points [MAX_QUAD_POINTS];
	Scalar weights[MAX_QUAD_POINTS];
	SizeType size;
} Quadrature_1;

typedef struct {
	Scalar points [MAX_QUAD_POINTS*2];
	Scalar weights[MAX_QUAD_POINTS];
	SizeType size;
} Quadrature_2;

typedef struct {
	Scalar points [MAX_QUAD_POINTS*3];
	Scalar weights[MAX_QUAD_POINTS];
	SizeType size;
} Quadrature_3;

//max order available
#define max_order_for_line_rules 2
#define max_order_for_triangle_rules 6
#define max_order_for_quad_rules 4
#define max_order_for_tetrahedron_rules 8
#define max_order_for_hex_rules 9

#ifndef CLIPP_HOST_CL

SizeType get_order(const SizeType element, const m_global__ SizeType *quad_meta);
SizeType get_n_shape_fun_offset(const SizeType element, const m_global__ SizeType *quad_meta);
SizeType get_n_quad_points_offset(const SizeType element, const m_global__ SizeType *quad_meta);
SizeType get_element_matrix_offset(const SizeType element, const m_global__ SizeType *quad_meta);
SizeType get_element_vector_offset(const SizeType element, const m_global__ SizeType *quad_meta);

SizeType find_compatible_quad_rule_1(const SizeType max_order, m_constant__ Quadrature_1 *rules, const SizeType order);
SizeType find_compatible_quad_rule_2(const SizeType max_order, m_constant__ Quadrature_2 *rules, const SizeType order);
SizeType find_compatible_quad_rule_3(const SizeType max_order, m_constant__ Quadrature_3 *rules, const SizeType order);

#endif //CLIPP_HOST_CL
#endif //QUADRATURE_INTERFACE_CL
