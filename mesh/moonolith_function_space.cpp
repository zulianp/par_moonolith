#include "moonolith_function_space.hpp"

namespace moonolith {

	bool DofMap::is_valid() const
	{
	    if(n_dofs_ == 0) {
	        error_logger()  << "[Error] n_dofs not defined " << std::endl;
	        return false;
	    }

	    bool valid = true;
	    for(const auto &dofs : element_dofs_) {

	        bool element_valid = true;

	        for(const auto &d : dofs.dofs)
	        {
	            if(d >= n_dofs_) {
	                element_valid = false;
	                break;
	            }
	        }

	        if(!element_valid) {
	            error_logger() << "[Error] Badly formed element for dofmap with ndofs = " << n_dofs_ << std::endl;
	            dofs.describe(error_logger());
	            valid = false;
	        }
	    }

	    return valid;
	}
}
