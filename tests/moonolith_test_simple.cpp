#include "moonolith_intersect_polygon_with_mesh.hpp"

#include <cmath>

#include <fstream>
#include <iostream>
#include <vector>

#include "gtest/gtest.h"

template class moonolith::IntersectPolyWithMesh<double, 3>;
template class moonolith::IntersectPolyWithMesh<double, 2>;

TEST(SimpleTest, test_intersection) {
    //
    moonolith::IntersectPolyWithMesh<double, 3> isect3;
    moonolith::IntersectPolyWithMesh<double, 2> isect2;
}
