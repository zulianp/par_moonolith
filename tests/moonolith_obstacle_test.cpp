#include <cmath>

#include "moonolith_intersect_ray_with_aabb.hpp"
#include "moonolith_stream_utils.hpp"

#include "gtest/gtest.h"

#include <iostream>

#include "par_moonolith.hpp"

#include "moonolith_ray_casting_tree.hpp"

#include "moonolith_mesh_io.hpp"

#include "moonolith_obstacle.hpp"

TEST(ObstacleTest, ray_aabb_3) {
    using namespace moonolith;

    Ray<Real, 3> ray{{0.000001, 0.000001, 0.2}, {0.0, 0.0, 1.0}};
    AABB<3, Real> aabb;

    aabb.set_min(0, 0.0);
    aabb.set_min(1, 0.0);
    aabb.set_min(2, 0.0);

    aabb.set_max(0, 1.0);
    aabb.set_max(1, 1.0);
    aabb.set_max(2, 1.0);

    IntersectRayWithAABB<Real, 3> isect;

    Real tmin = 0, tmax = 0;
    isect.apply(ray, aabb, tmin, tmax);

    ASSERT_NEAR(-0.2, tmin, CheckTol<Real>::half_value());
    ASSERT_NEAR(0.8, tmax, CheckTol<Real>::half_value());
}

TEST(ObstacleTest, ray_aabb_2) {
    using namespace moonolith;

    Ray<Real, 2> ray{{0.000001, 0.2}, {0.0, 1.0}};
    AABB<2, Real> aabb;

    aabb.set_min(0, 0.0);
    aabb.set_min(1, 0.0);

    aabb.set_max(0, 1.0);
    aabb.set_max(1, 1.0);

    IntersectRayWithAABB<Real, 2> isect;

    Real tmin = 0, tmax = 0;
    isect.apply(ray, aabb, tmin, tmax);

    ASSERT_NEAR(-0.2, tmin, CheckTol<Real>::half_value());
    ASSERT_NEAR(0.8, tmax, CheckTol<Real>::half_value());
}

TEST(ObstacleTest, ray_mesh) {
    using namespace moonolith;
    static const int Dimension = 3;

    using MeshT = moonolith::Mesh<Real, Dimension>;
    using Point = moonolith::Vector<Real, Dimension>;

    Communicator comm;

    if(!comm.is_alone()) {
        return;
    }

    // std::string path = "../examples/data/square_2.tri";
    std::string path = "../examples/data/square_200.tri";

    MeshT mesh(comm);

    // Mesh is read only on one process
    if (comm.is_root()) {
        TriMeshReader<MeshT> reader;
        if (!reader.read(path, mesh)) {
            error_logger() << "Could not read mesh at path: " << path << std::endl;
            return;
        }
    }

    // auto f = [](const Point &p) -> Real { return 2 * (p[0] * p[0] + p[1] * p[1]); };
    auto f = [](const Point &p) -> Real { return 2; };

    for (Integer i = 0; i < mesh.n_nodes(); ++i) {
        mesh.point(i)[2] = f(mesh.point(i));
    }

    auto tree = RayCastingTree<Real, Dimension>::New();
    tree->init(mesh);

    Ray<Real, 3> ray{{0.52, 0.52, 10}, {0.0, 0.0, -1.0}};

    MatlabScripter script;
    script.close_all();
    script.hold_on();
    mesh.draw(script);

    Line<Real, Dimension> line;
    line.p0 = ray.o;

    Real t;
    Vector<Real, Dimension> p;
    if (tree->intersect(ray, t, p)) {
        script.plot(p, "\'r*\'");
        line.p1 = p;
        script.plot(line, "\'g-o\'");
    } else {
        ASSERT_TRUE(false);
    }

    script.save("ray_casting.m");
}

TEST(ObstacleTest, assemble_obstacle) {
    using namespace moonolith;

    using MeshT = moonolith::Mesh<Real, 3>;
    using Point = moonolith::Vector<Real, 3>;

    Communicator comm;

    if(!comm.is_alone()) return;
    
    auto mesh = std::make_shared<MeshT>(comm);

    // Mesh is read only on one process
    // std::string path = "../examples/data/square_2.tri";
    std::string path = "../examples/data/square_200.tri";
    if (comm.is_root()) {
        TriMeshReader<MeshT> reader;
        if (!reader.read(path, *mesh)) {
            error_logger() << "Could not read mesh at path: " << path << std::endl;
            return;
        }
    }

    auto f_obs = [](const Point &p) -> Real { return 2 + (p[0] * p[0] + p[1] * p[1]); };
    auto f_m = [](const Point &p) -> Real {
        auto dx = (p[0] - 0.5);
        auto dy = (p[1] - 0.5);
        auto dz = (p[2] - 0.5);
        return -1 + -0.2 * (dx * dx + dy * dy + dz * dz);
    };

    MeshT obstacle_mesh = *mesh;

    for (Integer i = 0; i < obstacle_mesh.n_nodes(); ++i) {
        obstacle_mesh.point(i)[2] = f_obs(obstacle_mesh.point(i));
        mesh->point(i)[2] = f_m(mesh->point(i));
    }

    for (Integer i = 0; i < obstacle_mesh.n_elements(); ++i) {
        auto &e = obstacle_mesh.elem(i);
        std::swap(e.nodes[1], e.nodes[2]);
    }

    auto tree = RayCastingTree<Real, 3>::New();
    tree->init(obstacle_mesh);

    FunctionSpace<MeshT> space(mesh);
    space.dof_map().set_n_local_dofs(mesh->n_nodes());

    space.make_iso_parametric();

    Obstacle<Real, 3> obstacle(tree);

    obstacle.assemble({}, space);

    if (false)
    //
    {
        obstacle.describe(logger());

        MatlabScripter script;
        script.close_all();
        script.hold_on();
        mesh->draw(script);
        obstacle_mesh.draw(script);

        script.axis_equal();
        script.axis_tight();
        script.save("obstacle.m");
    }
}
