#ifndef MOONOLITH_NTREE_BOUND_EXPAND_HPP
#define MOONOLITH_NTREE_BOUND_EXPAND_HPP

#include "moonolith_bound_expand.hpp"

namespace moonolith {

	template<class Tree>
	class NTreeBoundExpand : public BoundExpand<Tree> {
	public:
		typedef typename Tree::Traits Traits;
		typedef typename Traits::Bound Bound;
		typedef typename Traits::DataType DataType;
		
		virtual bool expand(Bound &bound) override
		{
			bound += _bound;
			return true;
		}
		
		NTreeBoundExpand(const DataType &obj)
		: _bound(obj.bound())
		{}

		NTreeBoundExpand(const Bound &bound)
		: _bound(bound)
		{}
		
	private:
		Bound _bound;
	};

}

#endif //MOONOLITH_NTREE_BOUND_EXPAND_HPP
