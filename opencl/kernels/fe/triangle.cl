#ifndef TRIANGLE_CL
#define TRIANGLE_CL 

#include "kernel_base.cl"
#include "fe_object.cl"

#include "fe_interfaces.cl"


void triangle_make_fe_object(const Scalar *points, const SizeType n_quad_points, const Scalar * quad_points, FEObject *object)
{
	object->n_dims = 2;
	object->n_quad_points = n_quad_points;
	object->reference_volume = 0.5;
	object->n_shape_functions = 3;

	triangle_transform(points, n_quad_points, quad_points, object->point);
	triangle_jacobian(points, n_quad_points, quad_points, object->jacobian);

	static_assert(MAX_SHAPE_FUNCS >= 3, "check kernel_base for MAX_SHAPE_FUNCS");
	
	triangle_linear_fun_0(n_quad_points, quad_points, object->fun[0]);
	triangle_linear_fun_1(n_quad_points, quad_points, object->fun[1]);
	triangle_linear_fun_2(n_quad_points, quad_points, object->fun[2]);

	triangle_linear_grad_0(n_quad_points, quad_points, object->grad[0]);
	triangle_linear_grad_1(n_quad_points, quad_points, object->grad[1]);
	triangle_linear_grad_2(n_quad_points, quad_points, object->grad[2]);
}

void triangle_transform(const Scalar * points, const SizeType n_quad_points, const Scalar * quad_points, Scalar * global_quad_points)
{
	const Vector2 p1 = vec_2(points[0], points[1]);
	const Vector2 p2 = vec_2(points[2], points[3]);
	const Vector2 p3 = vec_2(points[4], points[5]);

	const Vector2 u1 = p2 - p1;
	const Vector2 u2 = p3 - p1;
	const SizeType n_quad_points_x_d = n_quad_points*2;
	
	for(SizeType q = 0; q < n_quad_points_x_d; q+=2) {	
		const SizeType qp1 = q+1;
		const Vector2 global_point = p1 + u1 * quad_points[q] + u2 * quad_points[qp1];

		global_quad_points[q] 	= global_point[0];
		global_quad_points[qp1] = global_point[1];
	}
}

void triangle_transform_3(const Scalar * points, const SizeType n_quad_points, const Scalar * quad_points, Scalar * global_quad_points)
{
	const Vector3 p1 = vec_3(points[0], points[1], points[2]);
	const Vector3 p2 = vec_3(points[3], points[4], points[5]);
	const Vector3 p3 = vec_3(points[6], points[7], points[8]);

	const Vector3 u1 = p2 - p1;
	const Vector3 u2 = p3 - p1;
	
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		const SizeType q3   = q  * 3;
		const SizeType q3p1 = q3 + 1;
		const SizeType q3p2 = q3 + 2;

		const SizeType q2   = q  * 2;
		const SizeType q2p1 = q2 + 1; 

		const Vector3 global_point = p1 + u1 * quad_points[q2] + u2 * quad_points[q2p1];

		global_quad_points[q3] 	 = global_point[0];
		global_quad_points[q3p1] = global_point[1];
		global_quad_points[q3p2] = global_point[2];
	}
}

static void triangle_make_affine_transform_3(const Scalar * points, Scalar *A, Scalar *b)
{
	const Vector3 p1 = vec_3(points[0], points[1], points[2]);
	const Vector3 p2 = vec_3(points[3], points[4], points[5]);
	const Vector3 p3 = vec_3(points[6], points[7], points[8]);

	const Vector3 u1 = p2 - p1;
	const Vector3 u2 = p3 - p1;
	const Vector3 n  = normalize(cross(u1, u2));

	//row 0
	A[0] = u1.x;
	A[1] = u2.x;
	A[2] = n.x;

	//row 1
	A[3] = u1.y;
	A[4] = u2.y;
	A[5] = n.y;

	//row 2
	A[6] = u1.z;
	A[7] = u2.z;
	A[8] = n.z;

	b[0] = p1.x;
	b[1] = p1.y; 
	b[2] = p1.z;
}

void triangle_inverse_transform_3(const Scalar * points, const SizeType n_quad_points, const Scalar * global_quad_points, Scalar * quad_points, const bool preserve_z)
{
	Scalar A[3*3];
	Scalar b[3];

	triangle_make_affine_transform_3(points, A, b);

	Scalar temp[3] = { 0, 0, 0 };

	Scalar Ainv[3 * 3];
	inverse_3(A, det_3(A), Ainv);
	
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		const SizeType q3 = q * 3;
		

		generic_copy(3, &global_quad_points[q3], temp);
		axpy(3, -1, b, temp);

		if(preserve_z) {
			mat_vec_mul(3, 3, Ainv, temp, &quad_points[q3]);
		} else {
			const SizeType q2 = q * 2;
			mat_vec_mul(2, 3, Ainv, temp, &quad_points[q2]);
		}
	}
}

void triangle_inverse_transform(const Scalar * points, const SizeType n_quad_points, const Scalar * global_quad_points, Scalar * quad_points)
{
	Scalar affine_trafo_A    [2 * 2];  //linear map
	Scalar inv_affine_trafo_A[2 * 2];  //linear map
	Scalar temp              [2];

	
	triangle_jacobian(points, 1, &global_quad_points[0], affine_trafo_A); //input does not matter
	const Scalar detJ = det_2(affine_trafo_A);
	inverse_2(affine_trafo_A, detJ, inv_affine_trafo_A);

	KERNEL_ASSERT(detJ > 0, "triangle_inverse_transform: detJ > 0");

	for(SizeType i = 0; i < n_quad_points; ++i) {
		const SizeType offset_2 = i * 2;
		temp[0] = global_quad_points[offset_2] 	   - points[0];
		temp[1] = global_quad_points[offset_2 + 1] - points[1];

		mat_vec_mul(2, 2, inv_affine_trafo_A, temp, &quad_points[offset_2]);

		KERNEL_ASSERT(quad_points[offset_2]     >= -DEFAULT_TOLLERANCE, "triangle_inverse_transform: quad_points[offset_2] > 0");
		KERNEL_ASSERT(quad_points[offset_2 + 1] >= -DEFAULT_TOLLERANCE, "triangle_inverse_transform: quad_points[offset_2 + 1] > 0");
	}
}

//layout of point data [x1, y1, x2, y2, z1, z2, ...]
//layout of the jacobian data [ f1_x1, f2_x1, f1_x2, f2_x2, ... ]
//@param n_quad_points  should be 1
void triangle_jacobian(const Scalar * points, const SizeType n_quad_points, const Scalar * quad_points, Scalar * jacobians)
{
	(void) quad_points;

	const Vector2 p1 = vec_2(points[0], points[1]);
	const Vector2 p2 = vec_2(points[2], points[3]);
	const Vector2 p3 = vec_2(points[4], points[5]);

	const Vector2 u1 = p2 - p1;
	const Vector2 u2 = p3 - p1;

	const SizeType n_quad_points_x_d = n_quad_points*4;
	
	for(SizeType q = 0; q < n_quad_points_x_d; q+=4) {
		//row 0	
		jacobians[q] 	 = u1[0];
		jacobians[q + 1] = u2[0];
		//row 1
		jacobians[q + 2] = u1[1];
		jacobians[q + 3] = u2[1];
	}
}

void triangle_linear_shape_fun(const SizeType number, const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	switch(number)
	{
		case 0: 
		{
			triangle_linear_fun_0(n_quad_points, quad_points, funs);
			break;
		}

		case 1: 
		{
			triangle_linear_fun_1(n_quad_points, quad_points, funs);
			break;
		}

		case 2: 
		{
			triangle_linear_fun_2(n_quad_points, quad_points, funs);
			break;
		}

		default:
		{
			//TODO report error
			break;
		}
	}
}

void triangle_linear_fun_0(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		const SizeType q2 = q*2;
		funs[q] = 1.0 - quad_points[q2]- quad_points[q2+1];
	}
}

void triangle_linear_fun_1(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		funs[q] = quad_points[q*2];
	}
}

void triangle_linear_fun_2(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		funs[q] = quad_points[q*2+1];
	}
}

void triangle_linear_grad_0(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads)
{
	const SizeType n_quad_points_x_d = n_quad_points*2;
	for(SizeType q = 0; q < n_quad_points_x_d; q+=2) {	
		grads[q]   = -1;
		grads[q+1] = -1;
	}
}

void triangle_linear_grad_1(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads)
{
	const SizeType n_quad_points_x_d = n_quad_points*2;
	for(SizeType q = 0; q < n_quad_points_x_d; q+=2) {	
		grads[q]   = 1;
		grads[q+1] = 0;
	}

}

void triangle_linear_grad_2(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads)
{
	const SizeType n_quad_points_x_d = n_quad_points*2;
	for(SizeType q = 0; q < n_quad_points_x_d; q+=2) {	
		grads[q]   = 0;
		grads[q+1] = 1;
	}
}

void triangle_normal(const Scalar *points, Scalar *normal)
{
	const Vector3 o = vec_3(
		points[0],
		points[1],
		points[2]);

	const Vector3 u = vec_3(
		points[3],
		points[4],
		points[5]) - o;

	const Vector3 v = vec_3(
		points[6],
		points[7],
		points[8]) - o;

	const Vector3 n = normalize(cross(u, v));

	normal[0] = n.x;
	normal[1] = n.y;
	normal[2] = n.z;
}

#endif //TRIANGLE_CL

