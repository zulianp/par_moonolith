#ifndef MOONOLITH_MESSAGE_TAG_HPP
#define MOONOLITH_MESSAGE_TAG_HPP

namespace moonolith {

	class MessageTag {
	public:
		static const int MIN_TAG = 10000;
		static const int MAX_TAG = 100000;

		inline static int next_unique_id()
		{
			static int tag_ = MIN_TAG;
			++tag_;

			if(tag_ == MAX_TAG) {
				tag_ = MIN_TAG;
			}

			return tag_;
		}
	};

}

#endif //MOONOLITH_MESSAGE_TAG_HPP
