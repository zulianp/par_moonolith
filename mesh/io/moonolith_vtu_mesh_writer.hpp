#ifndef MOONOLITH_VTU_MESH_WRITER_HPP
#define MOONOLITH_VTU_MESH_WRITER_HPP

#include "par_moonolith_config.hpp"

#include "moonolith_stream_utils.hpp"

#include <cassert>
#include <fstream>
#include <iostream>

namespace moonolith {

    template <class Mesh>
    class VTUMeshWriter {
    private:
        static const int Dim = Mesh::Dim;

        static const int VTU_TETRA = 10;
        static const int VTU_TRIANGLE = 5;
        static const int VTU_QUAD = 9;
        static const int VTU_HEXAHEDRON = 12;
        static const int VTU_POLYGON = 7;
        static const int VTU_LINE = 3;

    public:
        inline static int vtk_tag_volume(const Integer n_nodes) {
            switch (n_nodes) {
                case 4:
                    return VTU_TETRA;
                case 8:
                    return VTU_HEXAHEDRON;
                default:
                    assert(
                        false);  // element type not supported. To add it (http://www.vtk.org/VTK/img/file-formats.pdf)
                    return -1;
            }
        }

        inline static int vtk_tag(const Integer n_nodes) {
            switch (n_nodes) {
                case 2:
                    return VTU_LINE;
                default:
                    error_logger() << "[Error] " << n_nodes << " not supported" << std::endl;
                    assert(
                        false);  // element type not supported. To add it (http://www.vtk.org/VTK/img/file-formats.pdf)
                    return -1;
            }
        }

        inline static int vtk_tag_planar(const Integer n_nodes) {
            if (n_nodes > 4) {
                return VTU_POLYGON;
            }

            switch (n_nodes) {
                case 3:
                    return VTU_TRIANGLE;
                case 4:
                    return VTU_QUAD;
                default:
                    error_logger() << "[Error] " << n_nodes << " not supported" << std::endl;
                    assert(
                        false);  // element type not supported. To add it (http://www.vtk.org/VTK/img/file-formats.pdf)
                    return -1;
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////

        bool write(const std::string &path, const Mesh &mesh) {
            std::ofstream os;
            os.open(path.c_str());
            if (!os.good()) {
                os.close();
                return false;
            }

            write_header(mesh, os);
            write_points(mesh, os);
            write_cells(mesh, os);

            write_footer(mesh, os);
            os.close();
            return true;
        }

        template <class Vector>
        bool write(const std::string &path, const Mesh &mesh, Vector &sol) {
            std::ofstream os;
            os.open(path.c_str());
            if (!os.good()) {
                os.close();
                return false;
            }

            write_header(mesh, os);
            write_point_data(mesh, sol, 1, os);
            // write_cell_data(mesh, sol, 1, os);
            write_points(mesh, os);
            write_cells(mesh, os);

            write_footer(mesh, os);
            os.close();
            // clear();
            return true;
        }

        void write_header(const Mesh &mesh, std::ostream &os) {
            const Integer n_cells = mesh.n_elements();
            os << "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n";
            os << "<UnstructuredGrid>\n";
            os << "<Piece NumberOfPoints=\"" << mesh.n_nodes() << "\" NumberOfCells=\"" << n_cells << "\">\n";
        }

        template <class Vector>
        void write_point_data(const Mesh &mesh, const Vector &sol, const Integer &nc, std::ostream &os) {
            const Integer n_nodes = mesh.n_nodes();
            os << "<PointData Scalars=\"scalars\">\n";

            if (nc == 1) {
                os << "<DataArray type=\"Float32\"  Name=\"solution\" Format=\"ascii\">\n";
                for (int i = 0; i < n_nodes; ++i) {
                    os << sol[i] << "\n";
                }
            } else {
                os << "<DataArray type=\"Float32\"  Name=\"solution\" NumberOfComponents=\"" << nc << "\">\n";
                for (int i = 0; i < n_nodes; ++i) {
                    for (int j = 0; j < nc; ++j) {
                        os << sol[i] << " ";
                    }
                    os << "\n";
                }
            }
            os << "</DataArray>\n";
            os << "</PointData>\n";
        }

        template <class Vector>
        void write_cell_data(const Mesh &mesh, const Vector &sol, const Integer &nc, std::ostream &os) {
            const Integer n_cells = mesh.n_elements();
            os << "<CellData Scalars=\"scalars\">\n";

            if (nc == 1) {
                os << "<DataArray type=\"Float32\"  Name=\"solution_cell\" Format=\"ascii\">\n";
                for (int i = 0; i < n_cells; ++i) {
                    os << sol[i] << "\n";
                }
            } else {
                os << "<DataArray type=\"Float32\" NumberOfComponents=\"" << nc << "\" Format=\"ascii\">\n";
                for (int i = 0; i < n_cells; ++i) {
                    for (int j = 0; j < nc; ++j) {
                        os << sol[i] << " ";
                    }
                    os << "\n";
                }
            }
            os << "</DataArray>\n";
            os << "</CellData>\n";
        }

        void write_points(const Mesh &mesh, std::ostream &os) {
            os << "<Points>\n";
            os << "<DataArray type=\"Float32\" NumberOfComponents=\"" << ((Dim < 3) ? 3 : Dim)
               << "\" format=\"ascii\">\n";

            const Integer n_nodes = mesh.n_nodes();
            for (Integer i = 0; i < n_nodes; ++i) {
                const auto &p = mesh.point(i);

                for (Integer d = 0; d < Dim; ++d) {
                    os << p(d);
                    if (d < Dim - 1) {
                        os << " ";
                    } else if (Dim == 2) {  // padding for paraview vtu format visualisation.
                        os << " ";
                        os << 0;

                    } else if (Dim == 1) {
                        os << " ";
                        os << 0;
                        os << " ";
                        os << 0;
                    }
                }
                os << "\n";
            }

            os << "</DataArray>\n";
            os << "</Points>\n";
        }

        void write_cells(const Mesh &mesh, std::ostream &os) {
            const auto n_elements = mesh.n_elements();

            os << "<Cells>\n";
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            os << "<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n";

            Integer manifold_dim = mesh.manifold_dim();

            if (manifold_dim <= 0) {
                manifold_dim = Dim;
            }

            Integer min_n_nodes = mesh.elem(0).nodes.size();
            Integer max_n_nodes = max_n_nodes;

            for (Integer k = 0; k < mesh.n_elements(); ++k) {
                Integer n_nodes = mesh.elem(k).nodes.size();
                min_n_nodes = std::min(min_n_nodes, n_nodes);
                max_n_nodes = std::max(max_n_nodes, n_nodes);

                for (Integer i = 0; i < n_nodes; ++i) {
                    const Integer v = mesh.elem(k).nodes[i];
                    os << v;
                    if (i < n_nodes) {
                        os << " ";
                    }
                }
                os << "\n";
            }

            os << "</DataArray>\n";
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            int minTag, maxTag;
            if (manifold_dim == 1) {
                minTag = vtk_tag(min_n_nodes);
                maxTag = vtk_tag(max_n_nodes);
            } else if (manifold_dim == 2) {
                minTag = vtk_tag_planar(min_n_nodes);
                maxTag = vtk_tag_planar(max_n_nodes);
            } else {
                minTag = vtk_tag_volume(min_n_nodes);
                maxTag = vtk_tag_volume(max_n_nodes);
            }

            os << "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\" RangeMin=\"" << minTag << "\" RangeMax=\""
               << maxTag << "\">\n";
            for (Integer i = 0; i < n_elements; ++i) {
                Integer n_nodes = mesh.elem(i).nodes.size();

                if (manifold_dim == 3) {
                    os << vtk_tag_volume(n_nodes) << "\n";
                } else if (manifold_dim == 2) {
                    os << vtk_tag_planar(n_nodes) << "\n";
                } else
                    os << vtk_tag(n_nodes) << "\n";
            }

            os << "</DataArray>\n";

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            os << "<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\" RangeMin=\"" << (manifold_dim + 1)
               << "\" RangeMax=\"" << (n_elements * (manifold_dim + 1)) << "\">\n";

            // for (Integer i = 0, offset = (manifold_dim + 1); i < n_elements; ++i, offset += (manifold_dim + 1)) {
            //     os << offset << "\n";
            // }

            for (Integer k = 0, offset = 0; k < mesh.n_elements(); ++k) {
                Integer n_nodes = mesh.elem(k).nodes.size();
                offset += n_nodes;
                os << offset << "\n";
            }

            os << "</DataArray>\n";
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            os << "</Cells>\n";
        }

        void write_footer(const Mesh &, std::ostream &os) {
            os << "</Piece>\n";
            os << "</UnstructuredGrid>\n";
            os << "</VTKFile>\n";
        }

        virtual ~VTUMeshWriter() {
            std::ofstream os;
            os.close();
        }
    };
}  // namespace moonolith

#endif  // MOONOLITH_VTU_MESH_WRITER_HPP
