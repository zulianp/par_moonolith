#ifndef MOONOLITH_STATIC_MATH_HPP
#define MOONOLITH_STATIC_MATH_HPP

namespace moonolith {
		/** 
		* @tparam T is the numeric type
		* @tparam MULTIPLE is the upper-bound multiple 
		* @param number is the number for which we want the upper-bound
		* @return the upper bound of number
		*/
	template<int Value, int Multiple, int UpperBound = 1, int Bigger = 0>
	struct StaticUpperBoundHelper {
		enum { value = StaticUpperBoundHelper<Value, Multiple, UpperBound*Multiple, (UpperBound*Multiple > Value)>::value };
	};

	template<int Value, int Multiple, int UpperBound>
	struct StaticUpperBoundHelper<Value, Multiple, UpperBound, 1> {
		enum { value = UpperBound };
	};

	template<int Value, int Multiple>
	struct StaticUpperBound {
		enum { value = StaticUpperBoundHelper<Value, Multiple>::value };
	};

	template< int V1, int V2 >
	struct StaticMax {
		enum { value = (V1 > V2)? V1 : V2 };
	};

	template< int V1, int V2 >
	struct StaticMin {
		enum { value = (V2 > V1)? V1 : V2 };
	};

	template< int X, int Exponent >
	struct StaticPow {
		enum{ value = X*StaticPow<X, Exponent-1>::value };
	};

	template< int X >
	struct StaticPow< X, 1 > {
		enum{ value = X };
	};

}

#endif //MOONOLITH_STATIC_MATH_HPP
