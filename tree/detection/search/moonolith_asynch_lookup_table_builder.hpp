#ifndef MOONOLITH_ASYNCH_LOOK_UP_TABLE_BUILDER_HPP
#define MOONOLITH_ASYNCH_LOOK_UP_TABLE_BUILDER_HPP

#include "moonolith_bound_synchronize.hpp"
#include "moonolith_broad_phase_detector.hpp"
#include "moonolith_communicator.hpp"
#include "moonolith_lookup_table.hpp"
#include "moonolith_lookup_table_builder.hpp"
#include "moonolith_predicate.hpp"
#include "moonolith_traversal_algorithm.hpp"

#include <cmath>

namespace moonolith {

    template <class Tree>
    class AsynchLookUpTableBuilder : public LookUpTableBuilder<Tree> {
    public:
        void setBoundSynchronize(const std::shared_ptr<BoundSynchronize<typename Tree::Traits> > &boundSynchronize) {
            bound_synchronize_ = boundSynchronize;
        }

        std::shared_ptr<BoundSynchronize<typename Tree::Traits> > getBoundSynchronize() { return bound_synchronize_; }

        enum CommStatus { UNDEFINED = 0, POSTED = 1, RESOLVED = 2 };

        struct TraversalData {
            ByteInputBuffer is;
            ByteOutputBuffer os;
            std::shared_ptr<TraversalAlgorithm<Tree> > algorithm;
            Integer current_level;
            CommStatus posted_send;
            CommStatus posted_recv;

            inline bool level_complete() const { return posted_send == RESOLVED && posted_recv == RESOLVED; }

            void if_level_complete_increment_level() {
                if (level_complete()) {
                    ++current_level;
                }
            }

            explicit TraversalData(const std::shared_ptr<TraversalAlgorithm<Tree> > &algorithm)
                : algorithm(algorithm), current_level(0), posted_send(UNDEFINED), posted_recv(UNDEFINED) {}
        };

        bool build_for(Communicator &comm, Tree &tree, const std::shared_ptr<LookUpTable> &lookUpTable) {
            if (comm.is_alone()) return true;

            assert(!comm.has_pending_requests());
            clear();

            if (this->verbose()) {
                if (comm.is_root()) logger() << "Using AsynchLookUpTableBuilder" << std::endl;
            }

            if (!bound_synchronize_) {
                bound_synchronize_ = tree.mutator_factory()->new_bound_synchronize();
            }

            BroadPhaseDetector<Tree> broad_phase_detector;
            broad_phase_detector.set_verbose(this->verbose());

            if (!broad_phase_detector.detect(comm, tree, *bound_synchronize_)) {
                return false;
            }

            // tree.refine();

            traversals_.resize(comm.size());
            for (Integer rank = 0; rank < comm.size(); ++rank) {
                // Skip self
                if (rank == comm.rank()) continue;

                if (bound_synchronize_->intersecting(rank)) {
                    stats_.n_related_procs++;
                    std::shared_ptr<TraversalAlgorithm<Tree> > t =
                        this->new_traversal_algorithm(tree, rank, lookUpTable);
                    assert(t);
                    traversals_[rank] = std::make_shared<TraversalData>(t);
                }
            }

            if (this->verbose()) root_describe("[Status] Broad-phase detection END", comm, logger());

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            std::vector<Integer> ready_to_send;
            for (auto t : traversals_) {
                if (t) ready_to_send.push_back(t->algorithm->rank());
            }

            bool finished = stats_.n_related_procs == 0;

            Integer sends_to_complete = 0;
            Integer recvs_to_post = 0;
            Integer recvs_to_complete = 0;
            Integer n_to_terminate = static_cast<Integer>(ready_to_send.size());

            while (!finished) {
                finished = true;
                // Send all avaiable infor
                for (auto ready : ready_to_send) {
                    assert(traversals_[ready]);

                    TraversalData &d = *traversals_[ready];
                    assert(d.posted_send == UNDEFINED);

                    const Integer rank = d.algorithm->rank();

                    d.os.clear();
                    d.algorithm->write_next_level(d.os);
                    if (this->verbose()) {
                        logger() << comm << " sending at " << rank << " in level " << d.current_level << std::endl;
                    }
                    comm.i_send(d.os, rank, d.current_level);
                    d.posted_send = POSTED;
                    ++sends_to_complete;
                    // for each send one recv incoming
                    ++recvs_to_post;
                    ++recvs_to_complete;
                }

                ready_to_send.clear();

                typedef Communicator::Activity Activity;
                Activity a;
                Integer completed = -1;
                while (a.type == Activity::NONE) {
                    a = comm.test_activity<byte>();

                    if (this->verbose() && a.type != Activity::NONE) a.describe(logger());

                    switch (a.type) {
                        case Activity::PROBE: {
                            assert(traversals_[a.rank]);

                            if (!traversals_[a.rank]) {
                                logger() << comm << " !traversals_[a.rank] is null for: " << a.rank << std::endl;
                                continue;
                            }

                            TraversalData &d = *traversals_[a.rank];

                            if (!d.posted_recv) {
                                assert(d.posted_recv == UNDEFINED);
                                // assert_EQUAL(a.tag, d.current_level);

                                // ignore tags that are not related to the level traversal
                                if (a.tag != d.current_level) continue;

                                d.is.clear();
                                d.is.reserve(a.size);
                                comm.i_recv(d.is.raw_ptr(), a.size, a.rank, d.current_level);
                                d.posted_recv = POSTED;
                                --recvs_to_post;
                            }

                            break;
                        }

                        case Activity::RECV_COMPLETE: {
                            assert(traversals_[a.rank]);
                            TraversalData &d = *traversals_[a.rank];
                            d.algorithm->read_next_level(d.is);

                            assert(d.posted_recv == POSTED);

                            d.posted_recv = RESOLVED;
                            --recvs_to_complete;

                            if (d.level_complete()) {
                                completed = a.rank;
                            }

                            break;
                        }

                        case Activity::SEND_COMPLETE: {
                            assert(traversals_[a.rank]);
                            // finalizing send
                            TraversalData &d = *traversals_[a.rank];

                            assert(d.posted_send == POSTED);

                            d.posted_send = RESOLVED;
                            --sends_to_complete;
                            if (d.level_complete()) {
                                completed = a.rank;
                            }

                            break;
                        }

                        default: {
                            break;
                        }
                    }
                }

                if (completed != -1) {
                    if (this->verbose()) logger() << comm << " completed " << completed << std::endl;
                    assert(!traversals_[completed]->algorithm->terminated());

                    traversals_[completed]->algorithm->process_level();
                    traversals_[completed]->posted_recv = UNDEFINED;
                    traversals_[completed]->posted_send = UNDEFINED;

                    if (!traversals_[completed]->algorithm->terminated()) {
                        traversals_[completed]->current_level += 1;
                        ready_to_send.push_back(completed);

                    } else {
                        stats_.n_levels = std::max(stats_.n_levels, traversals_[completed]->current_level + 1);
                        --n_to_terminate;
                        if (this->verbose()) logger() << comm << " terminated traversal " << std::endl;
                    }
                }

                if (this->verbose()) {
                    logger() << "Levels: ";
                    for (auto t : traversals_) {
                        if (t) logger() << t->current_level << ", ";
                    }

                    logger() << std::endl;
                }

                finished = n_to_terminate == 0 && sends_to_complete == 0 && recvs_to_complete == 0;
            }

            if (this->verbose() && comm.is_root()) {
                logger() << "[Status] Middle-phase detection END" << std::endl;
            }

            if (this->verbose()) {
                stats_.describe(logger());
            }
            comm.clear();
            return true;
        }

        void clear() {
            traversals_.clear();
            stats_.clear();
        }

        AsynchLookUpTableBuilder() {}

    private:
        typedef std::vector<std::shared_ptr<TraversalData> > TraversalList;
        typedef typename TraversalList::iterator TraversalIter;

        TraversalList traversals_;
        std::shared_ptr<BoundSynchronize<typename Tree::Traits> > bound_synchronize_;

        class BuilderStats : public Describable {
        public:
            Integer n_related_procs;
            Integer n_levels;

            BuilderStats() : n_related_procs(0), n_levels(0) {}

            void clear() {
                n_related_procs = 0;
                n_levels = 0;
            }

            void describe(std::ostream &os) const {
                os << "n_releated_procs: " << n_related_procs << "\n";
                os << "n_levels: " << n_levels << "\n";
            }
        };

        BuilderStats stats_;
    };

}  // namespace moonolith

#endif  // MOONOLITH_ASYNCH_LOOK_UP_TABLE_BUILDER_HPP
