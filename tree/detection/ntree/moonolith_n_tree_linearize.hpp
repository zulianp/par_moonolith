#ifndef MOONOLITH_NTREE_LINEARIZE_HPP
#define MOONOLITH_NTREE_LINEARIZE_HPP

#include "moonolith_can_refine.hpp"

namespace moonolith {

	template<class Tree>
	class NTreeLinearize : public Mutator<typename Tree::Traits> {
	public:
		typedef typename Tree::Traits Traits;
		typedef moonolith::Leaf<Traits> Leaf;
		typedef moonolith::TreeMemory<Traits> TreeMemory;

		virtual NavigatorOption visit(Node &, TreeMemory &) override
		{
			assert(false && "implement me");
			return CONTINUE;
		}	
	};
}

#endif //MOONOLITH_NTREE_LINEARIZE_HPP
