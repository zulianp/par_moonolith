#ifndef MOONOLITH_PROFILER_HPP
#define MOONOLITH_PROFILER_HPP

#include "par_moonolith_config.hpp"
#include "moonolith_describable.hpp"
#include "moonolith_communicator.hpp"
#include "moonolith_synched_describable.hpp"

#include <string>
#include <fstream>
#include <map>

#ifdef MOONOLITH_PROFILING_ENABLED
#define MOONOLITH_PROFILING_BEGIN()
#define MOONOLITH_EVENT_BEGIN(event_name) moonolith::Profiler::instance().event_begin(event_name)
#define MOONOLITH_EVENT_END(event_name) moonolith::Profiler::instance().event_end()
#define MOONOLITH_PROFILING_END() moonolith::Profiler::instance().save("moonolith_prof_world_size_" + std::to_string(moonolith::Communicator().size()) + "_rank_" + std::to_string(moonolith::Communicator().rank()) + ".txt")
#define MOONOLITH_PROFILING_END_WITH_PATH(path) moonolith::Profiler::instance().save(std::string(path) + ("/moonolith_prof_world_size_" + std::to_string(moonolith::Communicator().size()) + "_rank_" + std::to_string(moonolith::Communicator().rank()) + ".txt"))
#define MOONOLITH_PROFILING_DESCRIBE(comm, stream) moonolith::Profiler::instance().synch_describe(comm, stream)
#else
#define MOONOLITH_PROFILING_BEGIN()
#define MOONOLITH_EVENT_BEGIN(event_name) 
#define MOONOLITH_EVENT_END(event_name) 
#define MOONOLITH_PROFILING_END()
#define MOONOLITH_PROFILING_END_WITH_PATH(path)
#define MOONOLITH_PROFILING_DESCRIBE(comm, stream)
#endif

namespace moonolith {
	class Profiler : public SynchedDescribable {
	public:
		class Event : public Describable {
		public:
			double total_time_;
			double global_total_time_;
			Integer occurrence_;

			Event()
			: total_time_(0), global_total_time_(0), occurrence_(0)
			{}

			void add_to_global_total_time(const double elapsed)
			{
				global_total_time_ += elapsed;
			}

			void occurence(const double elapsed)
			{
				total_time_ += elapsed;
				occurrence_ += 1;
			}

			static void describe_header_entries(std::ostream &os)
			{
				os << "Occurrencies, Time";
			}

			void describe(std::ostream &os) const 
			{
				os << occurrence_ << ", " << total_time_;
			}

			void describe_global(std::ostream &os) const
			{
				os << occurrence_ << ", " << global_total_time_;
			}

		};

		void event_begin(const std::string &event_name);
		void event_end();
		void describe(std::ostream &os) const ;
		bool save(const std::string &path);


		inline static Profiler &instance() 
		{
			static Profiler _instance;
			return _instance;
		}


		inline ~Profiler() {}

	private:
		inline Profiler() 
		: event_active_(false), elapsed_(0)
		{}

		bool event_active_;
		double elapsed_; 
		std::string active_event_;
		std::map<std::string, Event> events_;
	};
}

#endif //MOONOLITH_PROFILER_HPP
