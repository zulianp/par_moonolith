#ifndef MOONOLITH_NTREE_BOUND_PARTITION_HPP
#define MOONOLITH_NTREE_BOUND_PARTITION_HPP

#include "moonolith_morton_encoder.hpp"

#include <array>
#include <assert.h>

namespace moonolith {

	template<class Bound, Integer Dimension>
	class NTreeBoundPartition {
	public:
		typedef std::array<Integer, Dimension> IntArray;

		inline static Bound new_sub_cell(const Bound &bound, const int sub_cell_number) 
		{
			return new_sub_cell_aux(bound, sub_cell_number, bound.min(0));
		}

		inline static IntArray hyper_cube_index(const int i)
		{
			IntArray index;
			MortonEncoder<Integer, Dimension>::decode(i, &index[0]);
			assert( Integer(MortonEncoder<Integer, Dimension>::encode(&index[0])) == i );
			return index;
		}

	private:

		template<typename Scalar>
		static Bound new_sub_cell_aux(const Bound &bound, const int sub_cell_number, const Scalar &) 
		{
			assert (sub_cell_number < (StaticPow<2, Dimension>::value));

			Bound ret;
			const IntArray subcellIndex = hyper_cube_index(sub_cell_number);

			for(int d = 0; d < Dimension; d++) {
				assert ( bound.min(d) <= bound.max(d) );
				const Scalar parSmallestD = bound.min(d);
				const Scalar parLargestD = bound.max(d);
				const Scalar sizeD = (parLargestD - parSmallestD)/2.0;
				ret.set_min(d,  parSmallestD + ((Scalar) subcellIndex[d]) * sizeD);
				ret.set_max(d, parSmallestD + ((Scalar)subcellIndex[d] + 1) * sizeD);
			}

			return ret;
		}
	};

}

#endif //MOONOLITH_NTREE_BOUND_PARTITION_HPP
