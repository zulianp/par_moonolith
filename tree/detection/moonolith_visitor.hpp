#ifndef MOONOLITH_VISITOR_HPP
#define MOONOLITH_VISITOR_HPP


#include "moonolith_forward_declarations.hpp"
#include "moonolith_navigator_option.hpp"
#include "moonolith_node_handle.hpp"

namespace moonolith {

	template<class Traits>
	class Visitor {
	public:
		typedef moonolith::Node<Traits> Node;
		typedef moonolith::Branch<Traits> Branch;
		typedef moonolith::Root<Traits> Root;
		typedef moonolith::Leaf<Traits> Leaf;
		typedef moonolith::TreeMemory<Traits> TreeMemory;

		virtual NavigatorOption visit(Branch &branch, TreeMemory &memory)
		{
			return visit(static_cast<Node &>(branch), memory);
		}	

		virtual NavigatorOption visit(Root &root, TreeMemory &memory)
		{
			return visit(static_cast<Branch &>(root), memory);
		}

		virtual NavigatorOption visit(Leaf &leaf, TreeMemory &memory)
		{
			return visit(static_cast<Node &>(leaf), memory);
		}

		virtual NavigatorOption visit(Node &node, TreeMemory &memory)
		{
			//do nothing
			(void)(node);
			(void)(memory);
			return CONTINUE;
		}	

		virtual NodeHandle selection() const
		{
			return NodeHandle();
		}

		virtual ~Visitor() {}
	};
	
}



#endif //MOONOLITH_VISITOR_HPP