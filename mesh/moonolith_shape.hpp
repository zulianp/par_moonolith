#ifndef MOONOLITH_SHAPE_HPP
#define MOONOLITH_SHAPE_HPP

#include "moonolith_ray.hpp"

namespace moonolith {

	template<typename T, int Dim, int PhysicalDim = Dim>
	class Shape {
	public:
        using CoPoint = moonolith::Vector<T, PhysicalDim>;
        using Point   = moonolith::Vector<T, Dim>;

        using CoVector = moonolith::Vector<T, PhysicalDim>;
        using Vector   = moonolith::Vector<T, Dim>;

		virtual ~Shape() {}

		virtual bool intersect(
			const Ray<T, PhysicalDim> &ray,
			T &t) 
        {
            Point x_param;
            return intersect(ray, t, x_param);
        }

        virtual bool intersect(
            const Ray<T, PhysicalDim> &ray,
            T &t,
            Point &x_param) = 0;
	};

}


#endif //MOONOLITH_SHAPE_HPP
