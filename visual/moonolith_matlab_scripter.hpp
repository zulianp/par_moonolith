#ifndef MOONOLITH_MATLAB_SCRIPTER_HPP
#define MOONOLITH_MATLAB_SCRIPTER_HPP

#include <fstream>
#include <sstream>

#include "moonolith_line.hpp"
#include "moonolith_polygon.hpp"
#include "moonolith_vector.hpp"
// #include "moonolith_polyhedron.hpp"
#include "moonolith_forward_declarations.hpp"

namespace moonolith {

    class MatlabScripter {
    public:
        template <typename T>
        using Polyhedron = moonolith::PMesh<T, 3>;

        template <typename T>
        void plot_any_polyhedron(const Polyhedron<T> &poly,
                                 const bool show_index = false,
                                 const std::string &modifier = "\'k-\'") {
            ss_ << "\n";

            hold_on();

            auto n_elems = poly.n_elements();
            auto n_nodes = poly.n_nodes();

            Polygon<T, 3> polygon;
            for (std::size_t i = 0; i < n_elems; ++i) {
                auto e_begin = poly.el_ptr[i];
                auto e_end = poly.el_ptr[i + 1];
                auto n = e_end - e_begin;

                polygon.resize(n);

                int idx = 0;
                for (auto e = e_begin; e != e_end; ++e, ++idx) {
                    polygon[idx] = poly.points[poly.el_index[e]];
                }

                plot(polygon, modifier);
            }

            if (show_index) {
                for (std::size_t k = 0; k < n_nodes; ++k) {
                    text(poly.points[k], std::to_string(k));
                }
            }
        }

        template <typename T>
        void enumerate(const Storage<T> &points) {
            auto n_nodes = points.size();
            for (std::size_t k = 0; k < n_nodes; ++k) {
                text(points[k], std::to_string(k));
            }
        }

        ///@brief plots polyhedra using trimesh(TRI,X,Y,Z,C)
        template <typename T>
        void plot(const Polyhedron<T> &poly,
                  const bool show_index = false,
                  const std::string &modifier = "\'FaceAlpha\', 0.4") {
            if (poly.type != Polyhedron<T>::HEX && poly.type != Polyhedron<T>::TET) {
                plot_any_polyhedron(poly, show_index, "\'k-\'");
                return;
            }

            ss_ << "\n";

            auto n_elems = poly.n_elements();
            auto n_nodes = poly.n_nodes();

            ss_ << "trimesh(";
            ss_ << "[";
            for (std::size_t i = 0; i < n_elems; ++i) {
                auto e_begin = poly.el_ptr[i];
                auto e_end = poly.el_ptr[i + 1];

                for (auto e = e_begin; e != e_end; ++e) {
                    ss_ << " " << poly.el_index[e] + 1;
                }

                if (i + 1 < n_elems) {
                    ss_ << ";";
                }
            }

            ss_ << "], ";

            for (int i = 0; i < 3; ++i) {
                ss_ << "[ ";
                for (std::size_t k = 0; k < n_nodes; ++k) {
                    ss_ << poly.points[k][i] << " ";
                }

                ss_ << "]";

                if (i + 1 < 3) {
                    ss_ << ",";
                }
            }

            if (!modifier.empty()) {
                ss_ << ", " << modifier << " ";
            }

            ss_ << ");\n";

            if (show_index) {
                for (std::size_t k = 0; k < n_nodes; ++k) {
                    text(poly.points[k], std::to_string(k));
                }
            }
        }

        template <typename T, int Dim>
        void plot(const Line<T, Dim> &line, const std::string &modifier = "\'b-\'") {
            ss_ << "\n";

            ss_ << "plot";

            if (Dim == 3) {
                ss_ << "3";
            }

            ss_ << "(";

            for (int i = 0; i < Dim; ++i) {
                ss_ << "[ ";

                ss_ << line.p0[i] << " ";
                ss_ << line.p1[i] << " ";

                ss_ << "] ";

                if (i + 1 < Dim) {
                    ss_ << ",";
                }
            }

            if (!modifier.empty()) {
                ss_ << ", " << modifier << " ";
            }

            ss_ << ");\n";
        }

        template <typename T, int Dim>
        void plot(const Polygon<T, Dim> &poly, const std::string &modifier = "") {
            const auto &p = poly.points;
            const Integer N = poly.size();

            ss_ << "\n";

            ss_ << "plot";

            if (Dim == 3) {
                ss_ << "3";
            }

            ss_ << "(";

            for (int i = 0; i < Dim; ++i) {
                ss_ << "[ ";
                for (Integer k = 0; k < N; ++k) {
                    ss_ << p[k][i] << " ";
                }

                ss_ << p[0][i] << "] ";
                if (i + 1 < Dim) {
                    ss_ << ",";
                }
            }

            if (!modifier.empty()) {
                ss_ << ", " << modifier << " ";
            }

            ss_ << ");\n";
        }

        template <typename T, int Dim>
        void plot(const Vector<T, Dim> &p, const std::string &modifier = "") {
            ss_ << "plot";

            if (Dim == 3) {
                ss_ << "3";
            }

            ss_ << "(";

            for (int i = 0; i < Dim - 1; ++i) {
                ss_ << p[i] << ", ";
            }

            ss_ << p[Dim - 1];

            if (!modifier.empty()) {
                ss_ << ", " << modifier << " ";
            }

            ss_ << ");\n";
        }

        template <typename T, int Dim>
        void text(const Vector<T, Dim> &p, const std::string &text) {
            ss_ << "text";

            ss_ << "(";

            for (int i = 0; i < Dim; ++i) {
                ss_ << p[i] << ", ";
            }

            ss_ << " \'" << text << "\'"
                << ");\n";
        }

        template <typename T, int Dim, std::size_t N>
        void plot(const std::array<Vector<T, Dim>, N> &p, const std::string &modifier = "") {
            ss_ << "\n";

            ss_ << "plot";

            if (Dim == 3) {
                ss_ << "3";
            }

            ss_ << "(";

            for (int i = 0; i < Dim; ++i) {
                ss_ << "[ ";
                for (std::size_t k = 0; k < N; ++k) {
                    ss_ << p[k][i] << " ";
                }

                ss_ << "]";

                if (i + 1 < Dim) {
                    ss_ << ",";
                }
            }

            if (!modifier.empty()) {
                ss_ << ", " << modifier << " ";
            }

            ss_ << ");\n";
        }

        template <typename T, int Dim>
        void plot(const Storage<Vector<T, Dim>> &p, const std::string &modifier = "") {
            ss_ << "\n";

            ss_ << "plot";

            if (Dim == 3) {
                ss_ << "3";
            }

            ss_ << "(";

            std::size_t N = p.size();
            for (int i = 0; i < Dim; ++i) {
                ss_ << "[ ";
                for (std::size_t k = 0; k < N; ++k) {
                    ss_ << p[k][i] << " ";
                }

                ss_ << "]";

                if (i + 1 < Dim) {
                    ss_ << ",";
                }
            }

            if (!modifier.empty()) {
                ss_ << ", " << modifier << " ";
            }

            ss_ << ");\n";
        }

        template <typename T, int Dim, std::size_t N>
        void plot_polygon(const std::array<Vector<T, Dim>, N> &p, const std::string &modifier = "\'b-\'") {
            ss_ << "\n";

            ss_ << "plot";

            if (Dim == 3) {
                ss_ << "3";
            }

            ss_ << "(";

            for (int i = 0; i < Dim; ++i) {
                ss_ << "[ ";
                for (std::size_t k = 0; k < N; ++k) {
                    ss_ << p[k][i] << " ";
                }

                ss_ << p[0][i] << "] ";
                if (i + 1 < Dim) {
                    ss_ << ",";
                }
            }

            if (!modifier.empty()) {
                ss_ << ", " << modifier << " ";
            }

            ss_ << ");\n";
        }

        void view(const float azimuth, const float elevation) {
            ss_ << "view(gca,[" << azimuth << ", " << elevation << "]);\n";
        }

        void save_figure_as(const std::string &path) { ss_ << "saveas(gca,\'" << path << "\');\n"; }

        void close_all() { ss_ << "close all;\n"; }

        void hold_on() { ss_ << "hold on;\n"; }

        void axis_equal() { ss_ << "axis equal;\n"; }

        void axis_tight() { ss_ << "axis tight;\n"; }

        void hide_axis() { ss_ << "set(gca,\'XColor\', \'none\',\'YColor\',\'none\', \'ZColor\', \'none\');\n"; }

        void wait_for_button_press() { ss_ << "waitforbuttonpress;\n"; }

        void figure(const int num) { ss_ << "figure(" << num << ");"; }

        void figure() { ss_ << "figure;"; }

        void comment(const std::string &line) { ss_ << "%" << line << "\n"; }

        template <typename T, int Dim, std::size_t N>
        void quiver(const std::array<Vector<T, Dim>, N> &p,
                    const std::array<Vector<T, Dim>, N> &v,
                    const T scale = 1.) {
            ss_ << "\n";
            ss_ << "quiver";

            if (Dim == 3) {
                ss_ << "3";
            }

            ss_ << "(";

            for (int i = 0; i < Dim; ++i) {
                ss_ << "[ ";
                for (std::size_t k = 0; k < N; ++k) {
                    ss_ << p[k][i] << " ";
                }

                ss_ << "],";
            }

            for (int i = 0; i < Dim; ++i) {
                ss_ << "[ ";
                for (std::size_t k = 0; k < N; ++k) {
                    ss_ << v[k][i] << " ";
                }

                ss_ << "],";
            }

            ss_ << scale << ");\n";
        }

        bool save(const std::string &path) const {
            std::ofstream os(path.c_str());
            if (!os.good()) {
                assert(false);
                return false;
            }

            os << ss_.str();
            return true;
        }

    private:
        std::stringstream ss_;
    };
}  // namespace moonolith

#endif  // MOONOLITH_MATLAB_SCRIPTER_HPP
