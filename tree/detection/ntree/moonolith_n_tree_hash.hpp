#ifndef MOONOLITH_NTREE_HASH_HPP
#define MOONOLITH_NTREE_HASH_HPP


#include "moonolith_forward_declarations.hpp"
#include "moonolith_hash.hpp"

namespace moonolith {
	template<class Tree>
	class NTreeHash : public Hash<typename Tree::Traits> {
	public:
		typedef typename Tree::Traits Traits;
		typedef moonolith::Leaf<Traits> Leaf;
		typedef moonolith::TreeMemory<Traits> TreeMemory;
		typedef moonolith::Node<Traits> Node;

		enum {
			Dimension = Traits::Dimension
		};

		virtual ~NTreeHash() {}
		
		virtual std::string name() const override
		{
			return "Hash";
		}

		inline Integer flattened_hash(const Node &node, const TreeMemory &memory) const override
		{
			const Integer depth = memory.stats().n_levels();
			const Integer shift = (depth - node.level()) * Dimension;
			Integer id = node.global_handle();
			return id << shift;
		}
	};	
}

#endif //MOONOLITH_NTREE_HASH_HPP
