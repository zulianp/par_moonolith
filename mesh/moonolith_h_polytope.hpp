#ifndef MOONOLITH_H_POLYTOPE_HPP
#define MOONOLITH_H_POLYTOPE_HPP

#include "moonolith_empirical_tol.hpp"
#include "moonolith_expanding_array.hpp"
#include "moonolith_halfspace.hpp"
#include "moonolith_line.hpp"
#include "moonolith_polygon.hpp"
#include "moonolith_polyhedron.hpp"

#include <cassert>
#include <vector>

namespace moonolith {

    template <typename T, int Dim>
    class HPolytope {
    public:
        using Vector = moonolith::Vector<T, Dim>;
        using Point = Vector;
        using HalfSpace = moonolith::HalfSpace<T, Dim>;
        Storage<HalfSpace> half_spaces;

        void make(const Polyhedron<T> &in) {
            Storage<Vector> normals;
            make(in, normals);
        }

        void make(const Polyhedron<T> &in, Storage<Vector> &normals) {
            auto n_sides = in.n_elements();

            half_spaces.resize(n_sides);
            in.normals(normals);

            for (std::size_t i = 0; i < n_sides; ++i) {
                const auto begin = in.el_ptr[i];
                assert(in.el_ptr[i + 1] - begin >= 3);

                half_spaces[i].n = normals[i];
                const auto v0 = in.el_index[begin];
                const auto &n = half_spaces[i].n;
                const auto &p = in.points[v0];

                half_spaces[i].d = dot(n, p);

                assert(std::abs(length(n) - 1.) < 1e-6);
            }
        }

        void make(const Polygon<T, Dim> &in) {
            assert(Dim == 2);

            Vector dir;

            Integer n_half_spaces = in.size();
            half_spaces.resize(n_half_spaces);
            for (Integer i = 0; i < n_half_spaces; ++i) {
                Integer ip1 = (i + 1 == n_half_spaces) ? 0 : i + 1;
                auto &n = half_spaces[i].n;
                auto &d = half_spaces[i].d;

                dir = in[ip1] - in[i];
                n.x = dir.y;
                n.y = -dir.x;
                n /= length(n);
                d = dot(n, in[i]);
            }
        }

        bool contains(const Point &p, const T tol = GeometricTol<T>::value()) const {
            for (const auto &h : half_spaces) {
                if (!h.inside(p, tol)) {
                    return false;
                }
            }

            return true;
        }

        inline bool all_outside(const Storage<Point> points, const T tol = GeometricTol<T>::value()) const {
            // find out if there is a separating plane between the convex set and the points
            for (const auto &h : half_spaces) {
                bool inside = false;

                for (const auto &p : points) {
                    if (h.inside(p, tol)) {
                        inside = true;
                        break;
                    }
                }

                if (!inside) {
                    return true;
                }
            }

            return false;
        }

        inline bool contains_any(const Storage<Point> points, const T tol = GeometricTol<T>::value()) const {
            for (const auto &p : points) {
                if (contains(p, tol)) {
                    return true;
                }
            }

            return false;
        }

        template <class Iterator>
        Integer n_contained(const Iterator &begin, const Iterator &end, const T tol = GeometricTol<T>::value()) const {
            Integer ret = 0;
            for (Iterator it = begin; it != end; ++it) {
                ret += contains(*it, tol);
            }

            return ret;
        }

        void describe(std::ostream &os) const {
            for (const auto &h : half_spaces) {
                h.describe(os);
            }
        }
    };

}  // namespace moonolith

#endif  // MOONOLITH_H_POLYTOPE_HPP
