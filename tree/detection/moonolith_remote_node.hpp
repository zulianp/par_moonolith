#ifndef MOONOLITH_REMOTE_NODE_HPP
#define MOONOLITH_REMOTE_NODE_HPP

#include "moonolith_forward_declarations.hpp"
#include "moonolith_node_handle.hpp"
#include "moonolith_predicate.hpp"

#include <memory>

namespace moonolith {
    template <class Node>
    class RemoteNode : public Serializable {
    public:
        typedef typename Node::Traits::DataType DataType;
        virtual ~RemoteNode() {}

        ///@brief override if more discrimination is needed
        virtual bool match(const Node &) const { return true; }

        virtual bool match(const DataType &) const { return true; }

        inline bool empty() const { return _empty; }

        inline bool can_refine_search() const { return _can_refine_search; }

        inline NodeHandle handle() const { return _handle; }

        virtual void write(OutputStream &os) const override {
            CHECK_STREAM_WRITE_BEGIN("RemoteNode", os);

            os << _handle;
            os << _can_refine_search;
            os << _empty;
            os << _size;

            CHECK_STREAM_WRITE_END("RemoteNode", os);
        }

        virtual void read(InputStream &is) override {
            CHECK_STREAM_READ_BEGIN("RemoteNode", is);

            is >> _handle;
            is >> _can_refine_search;
            is >> _empty;
            is >> _size;

            CHECK_STREAM_READ_END("RemoteNode", is);
        }

        virtual void initialize(const Node &node, const bool can_refine_search) {
            _handle = node.handle();
            _can_refine_search = can_refine_search;
            _empty = node.empty();
            _size = node.size();
        }

        virtual void describe(std::ostream &os) const {
            os << "Handle: " << _handle.id() << "\n";
            os << "CanRefineSearch: " << _can_refine_search << "\n";
            os << "IsEmpty: " << _empty << "\n";
            os << "Size: " << _size << "\n";
        }

        RemoteNode() : _handle(), _can_refine_search(false), _empty(true), _size(0) {}

    private:
        NodeHandle _handle;
        bool _can_refine_search, _empty;
        Integer _size;
    };

    template <class Node>
    class RemoteNodeWithBounds : public RemoteNode<Node> {
    public:
        typedef typename Node::Traits::DataType DataType;
        typedef typename Node::Bound Bound;

        virtual bool match(const Node &node) const override {
            const bool res = _bound.intersects(node.bound());
            return res;
        }

        virtual void write(OutputStream &os) const override {
            CHECK_STREAM_WRITE_BEGIN("RemoteNodeWithBounds", os);

            RemoteNode<Node>::write(os);
            if (!this->empty()) os << _bound;

            CHECK_STREAM_WRITE_END("RemoteNodeWithBounds", os);
        }

        virtual void read(InputStream &is) override {
            CHECK_STREAM_READ_BEGIN("RemoteNodeWithBounds", is);

            RemoteNode<Node>::read(is);
            if (!this->empty()) is >> _bound;

            CHECK_STREAM_READ_END("RemoteNodeWithBounds", is);
        }

        virtual void initialize(const Node &node, const bool can_refine_search) override {
            RemoteNode<Node>::initialize(node, can_refine_search);
            _bound = node.bound();
        }

        virtual void describe(std::ostream &os) const override {
            RemoteNode<Node>::describe(os);
            // Other usefull descriptions?
        }

        virtual bool match(const DataType &data) const override { return data.bound().intersects(_bound); }

        inline const Bound &bound() const { return _bound; }

    private:
        Bound _bound;
    };

    template <class Node>
    class RemoteNodeWithTags : public RemoteNodeWithBounds<Node> {
    public:
        typedef typename Node::Traits::DataType DataType;

        virtual bool match(const Node &node) const override {
            return RemoteNodeWithBounds<Node>::match(node) &&
                   _predicate->tags_are_related(node.tags().begin(), node.tags().end(), _tags.begin(), _tags.end());
        }

        virtual void write(OutputStream &os) const override {
            CHECK_STREAM_WRITE_BEGIN("RemoteNodeWithBounds", os);

            RemoteNodeWithBounds<Node>::write(os);

            Integer nTags = static_cast<Integer>(_tags.size());
            os << nTags;
            os.write(_tags.begin(), _tags.end());

            CHECK_STREAM_WRITE_END("RemoteNodeWithBounds", os);
        }

        virtual void read(InputStream &is) override {
            CHECK_STREAM_READ_BEGIN("RemoteNodeWithBounds", is);

            RemoteNodeWithBounds<Node>::read(is);

            Integer nTags;
            is >> nTags;
            if (nTags) {
                _tags.resize(nTags);
                is.read(_tags.begin(), _tags.end());
            }

            CHECK_STREAM_READ_END("RemoteNodeWithBounds", is);
        }

        virtual void initialize(const Node &node, const bool can_refine_search) override {
            RemoteNodeWithBounds<Node>::initialize(node, can_refine_search);
            _tags = node.tags();
        }

        virtual void describe(std::ostream &os) const override {
            RemoteNodeWithBounds<Node>::describe(os);
            // Other usefull descriptions?
        }

        virtual bool match(const DataType &data) const override {
            return RemoteNodeWithBounds<Node>::match(data) &&
                   _predicate->tags_are_related(data.tag(), _tags.begin(), _tags.end());
        }

        RemoteNodeWithTags(const std::shared_ptr<Predicate> &predicate) : _predicate(predicate) {}

    public:
        std::vector<Integer> _tags;
        std::shared_ptr<Predicate> _predicate;
    };
}  // namespace moonolith

#endif  // MOONOLITH_REMOTE_NODE_HPP