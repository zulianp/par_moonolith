#ifndef MOONOLITH_INVERT_HPP
#define MOONOLITH_INVERT_HPP

#include <array>
#include "moonolith_expanding_array.hpp"
#include "moonolith_lapack.hpp"

namespace moonolith {
    // determinant

    template <typename T, std::size_t Rows, std::size_t Cols>
    bool pseudo_invert(const std::array<T, Rows * Cols> &in, std::array<T, Cols * Rows> &out);

    //////////////////////////////////////////////////////////////////////////////////////////////////

    template <typename T>
    inline auto det1(const T &mat) -> decltype(0 * mat[0]) {
        return mat[0];
    }

    template <typename T>
    inline auto det2(const T &mat) -> decltype(0 * mat[0]) {
        return mat[0] * mat[3] - mat[2] * mat[1];
    }

    template <typename T>
    inline auto det3(const T &mat) -> decltype(0 * mat[0]) {
        return mat[0] * mat[4] * mat[8] + mat[1] * mat[5] * mat[6] + mat[2] * mat[3] * mat[7] -
               mat[0] * mat[5] * mat[7] - mat[1] * mat[3] * mat[8] - mat[2] * mat[4] * mat[6];
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////

    template <typename T>
    inline T det(const std::array<T, 1> &mat) {
        return det1(mat);
    }

    template <typename T>
    inline T det(const std::array<T, 4> &mat) {
        return det2(mat);
    }

    template <typename T>
    inline T det(const std::array<T, 9> &mat) {
        return det3(mat);
    }

    template <typename T>
    inline T det(const Storage<T> &mat) {
        switch (mat.size()) {
            case 1:
                return det1(mat);
            case 4:
                return det2(mat);
            case 9:
                return det3(mat);
            default:
                return -1.0;
                // http://www.netlib.org/lapack/explore-html/dd/d9a/group__double_g_ecomputational_ga0019443faea08275ca60a734d0593e60.html
                // DGETRF
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////

    template <typename T>
    bool invert1(const T &mat, T &mat_inv) {
        if (mat[0] == 0.) return false;
        mat_inv[0] = 1. / mat[0];
        return true;
    }

    template <typename T, typename Scalar>
    bool invert2(const T &mat, T &mat_inv, const Scalar &det) {
        mat_inv[0] = mat[3] / det;
        mat_inv[1] = -mat[1] / det;
        mat_inv[2] = -mat[2] / det;
        mat_inv[3] = mat[0] / det;
        return true;
    }

    template <typename T, typename Scalar>
    bool invert3(const T &mat, T &mat_inv, const Scalar &det) {
        assert(det != 0.);

        if (det == 0.) {
            return false;
        }

        mat_inv[0] = (mat[4] * mat[8] - mat[5] * mat[7]) / det;
        mat_inv[1] = (mat[2] * mat[7] - mat[1] * mat[8]) / det;
        mat_inv[2] = (mat[1] * mat[5] - mat[2] * mat[4]) / det;
        mat_inv[3] = (mat[5] * mat[6] - mat[3] * mat[8]) / det;
        mat_inv[4] = (mat[0] * mat[8] - mat[2] * mat[6]) / det;
        mat_inv[5] = (mat[2] * mat[3] - mat[0] * mat[5]) / det;
        mat_inv[6] = (mat[3] * mat[7] - mat[4] * mat[6]) / det;
        mat_inv[7] = (mat[1] * mat[6] - mat[0] * mat[7]) / det;
        mat_inv[8] = (mat[0] * mat[4] - mat[1] * mat[3]) / det;
        return true;
    }

    template <typename T, typename Scalar>
    bool invert4(const T &m, T &inv, Scalar &det) {
        inv[0] = m[5] * m[10] * m[15] - m[5] * m[11] * m[14] - m[9] * m[6] * m[15] + m[9] * m[7] * m[14] +
                 m[13] * m[6] * m[11] - m[13] * m[7] * m[10];

        inv[4] = -m[4] * m[10] * m[15] + m[4] * m[11] * m[14] + m[8] * m[6] * m[15] - m[8] * m[7] * m[14] -
                 m[12] * m[6] * m[11] + m[12] * m[7] * m[10];

        inv[8] = m[4] * m[9] * m[15] - m[4] * m[11] * m[13] - m[8] * m[5] * m[15] + m[8] * m[7] * m[13] +
                 m[12] * m[5] * m[11] - m[12] * m[7] * m[9];

        inv[12] = -m[4] * m[9] * m[14] + m[4] * m[10] * m[13] + m[8] * m[5] * m[14] - m[8] * m[6] * m[13] -
                  m[12] * m[5] * m[10] + m[12] * m[6] * m[9];

        inv[1] = -m[1] * m[10] * m[15] + m[1] * m[11] * m[14] + m[9] * m[2] * m[15] - m[9] * m[3] * m[14] -
                 m[13] * m[2] * m[11] + m[13] * m[3] * m[10];

        inv[5] = m[0] * m[10] * m[15] - m[0] * m[11] * m[14] - m[8] * m[2] * m[15] + m[8] * m[3] * m[14] +
                 m[12] * m[2] * m[11] - m[12] * m[3] * m[10];

        inv[9] = -m[0] * m[9] * m[15] + m[0] * m[11] * m[13] + m[8] * m[1] * m[15] - m[8] * m[3] * m[13] -
                 m[12] * m[1] * m[11] + m[12] * m[3] * m[9];

        inv[13] = m[0] * m[9] * m[14] - m[0] * m[10] * m[13] - m[8] * m[1] * m[14] + m[8] * m[2] * m[13] +
                  m[12] * m[1] * m[10] - m[12] * m[2] * m[9];

        inv[2] = m[1] * m[6] * m[15] - m[1] * m[7] * m[14] - m[5] * m[2] * m[15] + m[5] * m[3] * m[14] +
                 m[13] * m[2] * m[7] - m[13] * m[3] * m[6];

        inv[6] = -m[0] * m[6] * m[15] + m[0] * m[7] * m[14] + m[4] * m[2] * m[15] - m[4] * m[3] * m[14] -
                 m[12] * m[2] * m[7] + m[12] * m[3] * m[6];

        inv[10] = m[0] * m[5] * m[15] - m[0] * m[7] * m[13] - m[4] * m[1] * m[15] + m[4] * m[3] * m[13] +
                  m[12] * m[1] * m[7] - m[12] * m[3] * m[5];

        inv[14] = -m[0] * m[5] * m[14] + m[0] * m[6] * m[13] + m[4] * m[1] * m[14] - m[4] * m[2] * m[13] -
                  m[12] * m[1] * m[6] + m[12] * m[2] * m[5];

        inv[3] = -m[1] * m[6] * m[11] + m[1] * m[7] * m[10] + m[5] * m[2] * m[11] - m[5] * m[3] * m[10] -
                 m[9] * m[2] * m[7] + m[9] * m[3] * m[6];

        inv[7] = m[0] * m[6] * m[11] - m[0] * m[7] * m[10] - m[4] * m[2] * m[11] + m[4] * m[3] * m[10] +
                 m[8] * m[2] * m[7] - m[8] * m[3] * m[6];

        inv[11] = -m[0] * m[5] * m[11] + m[0] * m[7] * m[9] + m[4] * m[1] * m[11] - m[4] * m[3] * m[9] -
                  m[8] * m[1] * m[7] + m[8] * m[3] * m[5];

        inv[15] = m[0] * m[5] * m[10] - m[0] * m[6] * m[9] - m[4] * m[1] * m[10] + m[4] * m[2] * m[9] +
                  m[8] * m[1] * m[6] - m[8] * m[2] * m[5];

        det = m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12];

        if (det == 0) return false;

        const Scalar inv_det = 1.0 / det;

        for (int i = 0; i < 16; i++) {
            inv[i] *= inv_det;
        }

        return true;
    }

    template <typename T>
    inline bool invert(const Storage<T> &mat, Storage<T> &mat_inv) {
        const int n = static_cast<int>(mat.size());
        mat_inv.resize(n);

        if (n > 16) {
#ifdef MOONOLITH_HAVE_LAPACK
            int n = std::sqrt(mat.size());
            assert(n * n == mat.size());

            std::fill(mat_inv.begin(), mat_inv.end(), 0.0);
            for (int i = 0; i < n; ++i) {
                mat_inv[i * n + i] = 1.0;
            }

            LapackInverse<T> lapack;
            lapack.init(n, &mat[0]);
            return lapack.apply(&mat_inv[0], &mat_inv[0], n);
#else
            assert(false);
            return false;
#endif
        }

        if (n == 16) {
            T d;
            return invert4(mat, mat_inv, d);
        }

        if (n == 1) {
            return invert1(mat, mat_inv);
        }

        const T d = det(mat);
        switch (n) {
            case 4:
                return invert2(mat, mat_inv, d);
            case 9:
                return invert3(mat, mat_inv, d);
            default:
                return false;
        }
    }

    template <typename T>
    bool invert(const std::array<T, 1> &mat, std::array<T, 1> &mat_inv) {
        return invert1(mat, mat_inv);
    }

    template <typename T>
    bool invert(const std::array<T, 4> &mat, std::array<T, 4> &mat_inv, const T &det) {
        return invert2(mat, mat_inv, det);
    }

    template <typename T>
    bool invert(const std::array<T, 9> &mat, std::array<T, 9> &mat_inv, const T &det) {
        return invert3(mat, mat_inv, det);
    }

    template <typename T>
    bool invert(const std::array<T, 16> &mat, std::array<T, 16> &mat_inv, T &det) {
        return invert4(mat, mat_inv, det);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    template <typename T>
    bool invert(const std::array<T, 9> &mat, std::array<T, 9> &mat_inv) {
        return invert(mat, mat_inv, moonolith::det(mat));
    }

    template <typename T>
    bool invert(const std::array<T, 4> &mat, std::array<T, 4> &mat_inv) {
        return invert(mat, mat_inv, moonolith::det(mat));
    }

    template <typename T>
    bool invert(const std::array<T, 16> &m, std::array<T, 16> &inv) {
        T det;
        return invert(m, inv, det);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    template <typename T, std::size_t Rows, std::size_t Cols>
    void mat_T_mat_mul(const std::array<T, Rows * Cols> &in, std::array<T, Cols * Cols> &out) {
        std::fill(std::begin(out), std::end(out), 0);

        for (std::size_t i = 0; i < Cols; ++i) {
            auto offset_i = i * Cols;

            for (std::size_t j = 0; j < Cols; ++j) {
                auto offset_ij = offset_i + j;

                for (std::size_t k = 0; k < Rows; ++k) {
                    auto offset_kj = k * Cols + i;
                    auto offset_ik = k * Cols + j;
                    out[offset_ij] += in[offset_kj] * in[offset_ik];
                }
            }
        }
    }

    template <typename T, std::size_t Rows, std::size_t N, std::size_t Cols>
    void mat_mat_mul(const std::array<T, Rows * N> &left,
                     const std::array<T, N * Cols> &right,
                     std::array<T, Rows * Cols> &out) {
        std::fill(std::begin(out), std::end(out), 0);

        for (std::size_t i = 0; i < Rows; ++i) {
            auto offset_i = i * Rows;

            for (std::size_t j = 0; j < Cols; ++j) {
                auto offset_ij = offset_i + j;

                for (std::size_t k = 0; k < N; ++k) {
                    auto offset_kj = k * N + j;
                    auto offset_ik = i * N + k;

                    out[offset_ij] += left[offset_ik] * right[offset_kj];
                }
            }
        }
    }

    template <typename T, std::size_t Rows, std::size_t Cols>
    void mat_vec_mul(const std::array<T, Rows * Cols> &mat, const Vector<T, Cols> &in, Vector<T, Rows> &out) {
        for (int i = 0; i < (int)Rows; ++i) {
            int offset_i = static_cast<int>(i * Cols);

            auto &val = out[i];
            val = 0.0;

            for (int j = 0; j < (int)Cols; ++j) {
                val += mat[offset_i + j] * in[j];
            }
        }
    }

    template <typename T, std::size_t Rows, std::size_t Cols>
    void mat_T_vec_mul(const std::array<T, Rows * Cols> &mat, const Vector<T, Rows> &in, Vector<T, Cols> &out) {
        for (int j = 0; j < (int)Cols; ++j) {
            // auto offset_i = i * Cols;

            auto &val = out[j];
            val = 0.0;

            for (int i = 0; i < (int)Rows; ++i) {
                int offset_i = static_cast<int>(i * Cols);
                val += mat[offset_i + j] * in[i];
            }
        }
    }

    template <typename T, std::size_t Rows, std::size_t N, std::size_t Cols>
    void mat_mat_T_mul(const std::array<T, Rows * N> &left,
                       const std::array<T, Cols * N> &right,
                       std::array<T, Rows * Cols> &out) {
        std::fill(std::begin(out), std::end(out), 0);

        for (int i = 0; i < (int)Rows; ++i) {
            int offset_i = static_cast<int>(i * Cols);

            for (int j = 0; j < (int)Cols; ++j) {
                int offset_ij = offset_i + j;

                for (int k = 0; k < (int)N; ++k) {
                    int offset_ik = static_cast<int>(i * N + k);
                    int offset_kj = static_cast<int>(j * N + k);

                    out[offset_ij] += left[offset_ik] * right[offset_kj];
                }
            }
        }
    }

    template <typename T, std::size_t Rows, std::size_t Cols>
    T pseudo_det(const std::array<T, Rows * Cols> &in) {
        using std::sqrt;
        std::array<T, Cols * Cols> A;
        mat_T_mat_mul<T, Rows, Cols>(in, A);
        return sqrt(det(A));
    }

    template <typename T, std::size_t Rows, std::size_t Cols>
    bool pseudo_invert(const std::array<T, Rows * Cols> &in, std::array<T, Cols * Rows> &out) {
        std::array<T, Cols * Cols> A, A_inv;
        mat_T_mat_mul<T, Rows, Cols>(in, A);

        if (!invert(A, A_inv)) {
            return false;
        }

        mat_mat_T_mul<T, Cols, Cols, Rows>(A_inv, in, out);
        return true;
    }

    template <typename T, std::size_t Rows, std::size_t Cols>
    class PseudoInvert {
    public:
        bool apply(const std::array<T, Rows * Cols> &in, std::array<T, Cols * Rows> &out) {
            mat_T_mat_mul<T, Rows, Cols>(in, A);

            if (!invert(A, A_inv)) {
                return false;
            }

            mat_mat_T_mul<T, Cols, Cols, Rows>(A_inv, in, out);
            return true;
        }

    private:
        std::array<T, Cols * Cols> A, A_inv;
    };

    template <typename T, std::size_t N>
    class PseudoInvert<T, N, N> {
    public:
        static bool apply(const std::array<T, N * N> &in, std::array<T, N * N> &out) { return invert(in, out); }
    };

}  // namespace moonolith

#endif  // MOONOLITH_INVERT_HPP
