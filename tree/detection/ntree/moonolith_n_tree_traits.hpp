#ifndef MOONOLITH_NTREE_TRAITS_HPP
#define MOONOLITH_NTREE_TRAITS_HPP

namespace moonolith {

	// ///@brief this is just a minimal example
	// class ExampleNTreeBound {
	// public:
	// 	//contains of instersect the other bound
	// 	virtual bool intersects(const ExampleNTreeBound &bound) const  = 0;

	// 	//expands to contain the union of this and bound
	// 	virtual ExampleNTreeBound &operator +=(const ExampleNTreeBound &bound) = 0;

	// 	virtual bool isEmpty() const = 0;

	// 	//puts this in the intial state
	// 	virtual void clear() = 0;
	// };

	// class ExampleNTreeObject {
	// public:
	// 	virtual ExampleNTreeBound getBound() = 0;
	// };



	// template<class _DataType, class Bound, SizeType _Dimension>
	// class NTreeTraits {
	// public:
	// 	enum {
	// 		Dimension = _Dimension
	// 	};

	// };

}

#endif //MOONOLITH_NTREE_TRAITS_HPP
