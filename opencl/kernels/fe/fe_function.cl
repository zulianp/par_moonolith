#ifndef FE_FUNCTION_CL
#define FE_FUNCTION_CL 

#include "fe_function_interface.cl"

void interpolate_dxd_at_quad_point(const SizeType quad_index, 
								   const Matrixdxd *grads_at_quad_index,
								   const FEObject *fe, 
								   const Scalar * coefficients, 
								   FEFunction *fe_fun
								   )
{
	switch(fe->n_dims) {
		case 2 :  { interpolate_2x2_at_quad_point(quad_index, grads_at_quad_index, fe, coefficients, fe_fun); return; }
		case 3 :  { interpolate_3x3_at_quad_point(quad_index, grads_at_quad_index, fe, coefficients, fe_fun); return; }
		default : { REPORT_ERROR("interpolate_dxd_at_quad_point: unsupported dim"); return; }
	}
}

void interpolate_2x2_at_quad_point(const SizeType quad_index, 
								   const Matrixdxd *grads_at_quad_index,
								   const FEObject *fe, 
								   const Scalar * coefficients, 
								   FEFunction *fe_fun
								   )
{
	set(2, 0, fe_fun->fun);
	set(4, 0, fe_fun->grad);

	//interpolate function at points
	for(SizeType i = 0; i < fe->n_shape_functions; ++i) {
		const SizeType i2 = i*2;
		const SizeType i2p1 = i2+1;
		fe_fun->fun[0] += fe->fun[i][quad_index] * coefficients[i2];
		fe_fun->fun[1] += fe->fun[i][quad_index] * coefficients[i2p1];
	}

	//interpolate gradient at point
	const SizeType n_shape_functions_2x2 = fe->n_shape_functions*2;
	for(SizeType i = 0; i < n_shape_functions_2x2; ++i) {
		axpy(4, coefficients[i], grads_at_quad_index[i].entries, fe_fun->grad);
	}
}

void interpolate_3x3_at_quad_point(const SizeType quad_index, 
								   const Matrixdxd *grads_at_quad_index,
								   const FEObject *fe, 
								   const Scalar * coefficients, 
								   FEFunction *fe_fun
								   )
{
	set(3, 0, fe_fun->fun);
	set(9, 0, fe_fun->grad);

	//interpolate function at points
	for(SizeType i = 0; i < fe->n_shape_functions; ++i) {
		const SizeType i3 = i * 3;
		const SizeType i3p1 = i3 + 1;
		const SizeType i3p2 = i3 + 2;

		fe_fun->fun[0] += fe->fun[i][quad_index] * coefficients[i3];
		fe_fun->fun[1] += fe->fun[i][quad_index] * coefficients[i3p1];
		fe_fun->fun[2] += fe->fun[i][quad_index] * coefficients[i3p2];
	}

	//interpolate gradient at points
	const SizeType n_shape_functions_3x3 = fe->n_shape_functions*3;
	for(SizeType i = 0; i < n_shape_functions_3x3; ++i) {
		axpy(9, coefficients[i], grads_at_quad_index[i].entries, fe_fun->grad);
	}
}


void fe_function_print(const SizeType n_dims, const SizeType n_codims, FEFunction *fe_fun)
{
	printf("FEFunction\n");
	matrix_print(1, n_codims, fe_fun->fun);
	matrix_print(n_dims, n_codims, fe_fun->grad);
	printf("-----------------------------------\n");
}


#endif //FE_FUNCTION_CL

