#ifndef QUAD_META_CL
#define QUAD_META_CL 

#include "quadrature_interface.cl"

SizeType get_order(const SizeType element, const m_global__ SizeType *quad_meta)
{
	return quad_meta[element*QUAD_META_SIZE];
}

SizeType get_n_shape_fun_offset(const SizeType element, const m_global__ SizeType *quad_meta)
{
	return quad_meta[element*QUAD_META_SIZE+1];
}

SizeType get_n_quad_points_offset(const SizeType element, const m_global__ SizeType *quad_meta)
{
	return quad_meta[element*QUAD_META_SIZE+2];
}

SizeType get_element_matrix_offset(const SizeType element, const m_global__ SizeType *quad_meta)
{
	return quad_meta[element*QUAD_META_SIZE+3];
}

SizeType get_element_vector_offset(const SizeType element, const m_global__ SizeType *quad_meta)
{
	return quad_meta[element*QUAD_META_SIZE+4];
}

#endif //QUAD_META_CL
