
#ifndef MORTAR_SURFACE_INTERFACE_CL
#define MORTAR_SURFACE_INTERFACE_CL 

#define MAX_N_PROJ_POINTS 20

typedef struct {
	SizeType n_elements;
	///CRS type format for accessing sides
	const m_global__ SizeType * el_ptr;  

	///Index of nodes in the parent_mesh
	const m_global__ SizeType * el_index;

	///Index of the parent element in the parent_mesh
	const m_global__ SizeType * parent_element;

	///Parent mesh
	Mesh * parent_mesh;
} SurfaceMesh;

typedef struct {
	FEObject fe_master, fe_slave;

	Scalar surf_el_master		[MAX_SHAPE_FUNCS   * MAX_N_DIMS];
	Scalar surf_el_slave		[MAX_SHAPE_FUNCS   * MAX_N_DIMS];

	Scalar ref_projection_master[MAX_N_PROJ_POINTS * MAX_N_DIMS];
	Scalar ref_projection_slave	[MAX_N_PROJ_POINTS * MAX_N_DIMS];
	Scalar projection_master	[MAX_N_PROJ_POINTS * MAX_N_DIMS];
	Scalar projection_slave		[MAX_N_PROJ_POINTS * MAX_N_DIMS];
	Scalar ref_points_master	[MAX_N_PROJ_POINTS * MAX_N_DIMS];

	Scalar isect_master			[MAX_N_PROJ_POINTS * MAX_N_DIMS];
	Scalar isect_slave			[MAX_N_PROJ_POINTS * MAX_N_DIMS];

	Scalar quad_points 			[MAX_QUAD_POINTS   * MAX_N_DIMS];
	Scalar quad_weights			[MAX_QUAD_POINTS];

	Scalar ray_origins			[MAX_QUAD_POINTS   * MAX_N_DIMS];
	Scalar ray_directions		[MAX_QUAD_POINTS   * MAX_N_DIMS];
	Scalar ray_intersections 	[MAX_QUAD_POINTS];

} SurfaceMortarWorkspace;

#ifndef CLIPP_HOST_CL

bool mortar_surface_line_line(
	const SurfaceMesh surf_master,
	const SizeType element_master,
	const SurfaceMesh surf_slave,
	const SizeType element_slave,
	const SizeType order,
	Scalar *local_matrix,
	Scalar *local_normals,
	Scalar *local_gap
	);

bool mortar_surface_triangle_triangle(
	const SurfaceMesh surf_master,
	const SizeType element_master,
	const SurfaceMesh surf_slave,
	const SizeType element_slave,
	const SizeType order,
	Scalar *local_matrix,
	Scalar *local_normals,
	Scalar *local_gap
	);

static void polygon_make_ccw_2(const SizeType n_points, Scalar *polygon);
void surface_mesh_points(const SurfaceMesh mesh, const SizeType element, Scalar *points);
SizeType surface_mesh_n_points(const SurfaceMesh mesh, const SizeType element);

void local_index_of_side(const SurfaceMesh surf, const SizeType element, SizeType * local_index);
void compute_normals_at_points(const FEObject fe, const Scalar *reference_normal, Scalar *normals);
static void remove_last_dim(const SizeType n_dims, const SizeType n_points, const Scalar *in, Scalar *out);

//returns the number of points of the projection polygon or polyline. Slave poly is the reference poly
static SizeType project_surface_poly_onto_ref_poly(const SizeType n_dims,
							  const SizeType n_points_master, const Scalar * poly_master,
							  const SizeType n_points_slave,  const Scalar * poly_slave,
							  Scalar * projection_master, Scalar * projection_slave);

static void intersect_ray_with_plane(const SizeType n_dims, const SizeType n_rays, const Scalar *origin, const Scalar *dir, const Scalar *plane_normal,  const Scalar plane_distance, Scalar *distance);
static void line_make_affine_transform_2(const Scalar *line, Scalar *A, Scalar *b);
void line_normal(const Scalar *line, Scalar *normal);
#endif //CLIPP_HOST_CL

#endif //MORTAR_SURFACE_INTERFACE_CL
