#ifndef MOONOLITH_BUILD_QUADRATURE_NON_CONVEX_HPP
#define MOONOLITH_BUILD_QUADRATURE_NON_CONVEX_HPP

#include "moonolith_build_quadrature.hpp"
#include "moonolith_convex_decomposition.hpp"
#include "moonolith_map_quadrature.hpp"
#include "moonolith_polyhedron.hpp"
#include "moonolith_transform.hpp"

namespace moonolith {

    template <typename T, int Dim, int PhysicalDim>
    class BuildQuadratureNonAffine {};

    ///@brief template method for building quadrature on physical element and mapping it to the reference element.
    template <typename T>
    class BuildQuadratureNonAffine<T, 3, 3> {
    public:
        using Point3 = typename Quadrature<T, 3>::Point;

        // INPUT
        Quadrature<T, 3> q_rule;
        Storage<Polyhedron<T>> master;
        Storage<Polyhedron<T>> slave;

        std::shared_ptr<Transform<T, 3, 3>> trafo_master;
        std::shared_ptr<Transform<T, 3, 3>> trafo_slave;

        // OUTPUT
        Storage<Quadrature<T, 3>> q_physical;
        Quadrature<T, 3> q_master;
        Quadrature<T, 3> q_slave;

        bool compute() {
            assert(trafo_master);
            assert(trafo_slave);

            assert(!master.empty());
            assert(!slave.empty());

            if (!compute_q_for_non_convex(master, slave)) {
                return false;
            }

            const auto vol_master = measure(master);
            const auto vol_slave = measure(slave);

            const Integer n_q_rules = q_physical.size();

            Integer n_qp = 0;
            for (Integer i = 0; i < n_q_rules; ++i) {
                n_qp += q_physical[i].n_points();
            }

            q_master.resize(n_qp);
            q_slave.resize(n_qp);

            Integer idx = 0;
            for (Integer i = 0; i < n_q_rules; ++i) {
                const auto n = q_physical[i].n_points();

                for (Integer k = 0; k < n; ++k, ++idx) {
                    trafo_master->apply_inverse(q_physical[i].point(k), q_master.point(idx));
                    trafo_slave->apply_inverse(q_physical[i].point(k), q_slave.point(idx));

                    q_master.weights[idx] = q_physical[i].weights[k] / vol_master;
                    q_slave.weights[idx] = q_physical[i].weights[k] / vol_slave;
                }
            }

            return true;
        }

        void clear() {
            master.clear();
            slave.clear();
        }

        BuildQuadratureNonAffine<T, 3, 3>() {}

        inline void use_reference_frame(const bool use_reference_frame) { use_reference_frame_ = use_reference_frame; }

    private:
        BuildQuadrature<Polyhedron<T>> build_q;
        bool use_reference_frame_{false};
        std::shared_ptr<Transform<T, 3, 3>> worst_cond_trafo_;
        bool worse_is_master_{true};

        bool compute_q_for_non_convex(Storage<Polyhedron<T>> &poly_master, Storage<Polyhedron<T>> &poly_slave) {
            // NEW
            choose_worst_conditioned();

            // NEW
            apply_transform_to_poly(poly_master);

            // NEW
            apply_transform_to_poly(poly_slave);

            const auto n1 = poly_master.size();
            const auto n2 = poly_slave.size();

            q_physical.resize(n1 * n2);

            std::size_t idx = 0;
            for (std::size_t i = 0; i < n1; ++i) {
                for (std::size_t j = 0; j < n2; ++j) {
                    if (worse_is_master_) {
                        // First poly is used as a cutter
                        if (build_q.apply(q_rule, poly_master[i], poly_slave[j], q_physical[idx])) {
                            idx++;
                        }
                    } else {
                        // First poly is used as a cutter
                        if (build_q.apply(q_rule, poly_slave[i], poly_master[j], q_physical[idx])) {
                            idx++;
                        }
                    }
                }
            }

            // actual intersections
            q_physical.resize(idx);

            // NEW
            apply_inverse_transform_to_qp();
            return idx > 0;
        }

        void choose_worst_conditioned() {
            if (!use_reference_frame_) return;

            T master_cond = trafo_master->estimate_condition_number();
            T slave_cond = trafo_slave->estimate_condition_number();

            if (master_cond > slave_cond) {
                worst_cond_trafo_ = trafo_master;
                worse_is_master_ = true;
            } else {
                worst_cond_trafo_ = trafo_slave;
                worse_is_master_ = false;
            }
        }

        void apply_transform_to_poly(Storage<Polyhedron<T>> &poly) {
            if (!use_reference_frame_) return;

            assert(worst_cond_trafo_);
            const auto n = poly.size();

            Point3 temp;
            for (std::size_t i = 0; i < n; ++i) {
                for (auto &p : poly[i].points) {
                    temp = p;
                    worst_cond_trafo_->apply_inverse(temp, p);
                }
            }
        }

        void apply_inverse_transform_to_qp() {
            if (!use_reference_frame_) return;

            Point3 temp;
            for (auto &qi : q_physical) {
                for (auto &qp : qi.points) {
                    temp = qp;
                    worst_cond_trafo_->apply(temp, qp);
                }
            }
        }
    };

}  // namespace moonolith

#endif  // MOONOLITH_BUILD_QUADRATURE_NON_CONVEX_HPP
