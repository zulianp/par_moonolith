#include "moonolith_par_contact.hpp"

namespace moonolith {
    template class ParContact<Real, 2>;
    template class ParContact<Real, 3>;
}  // namespace moonolith
