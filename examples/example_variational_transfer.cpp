// general
#include "par_moonolith.hpp"

// specific
#include "moonolith_mesh_io.hpp"
#include "moonolith_par_l2_transfer.hpp"

/// Compile examples with `make moonolith_examples`
/// Run for instance with  `mpiexec -np 2 ./examples/example_variational_transfer`
int main(int argc, char *argv[]) {
    using namespace moonolith;

    static const int Dim = 2;
    using MeshT = moonolith::Mesh<Real, Dim>;

    Moonolith::Init(argc, argv);
    {
        /////////////////////////////////////////////////
        // Set-up (Usually the mesh is created by converting the client code representation)
        /////////////////////////////////////////////////

        Communicator comm;
        auto master_mesh = std::make_shared<MeshT>(comm);
        auto slave_mesh = std::make_shared<MeshT>(comm);

        std::string data_path = "../examples/data/";
        bool verbose = true;

        if (argc >= 2) {
            data_path = argv[1];
        }

        if (argc >= 3) {
            verbose = atoi(argv[2]);
        }

        // Mesh is read only on one process
        if (comm.is_root()) {
            TriMeshReader<MeshT> reader;
            if (!reader.read(data_path + "/square_2.tri", *master_mesh)) {
                error_logger() << "Could not read mesh at path: " << (data_path + "/square_2.tri") << std::endl;
                Moonolith::Abort();
            }

            if (!reader.read(data_path + "/triangle_4.tri", *slave_mesh)) {
                error_logger() << "Could not read mesh at path: " << (data_path + "/triangle_4.tri") << std::endl;
                Moonolith::Abort();
            }
        }

        master_mesh->finalize();
        slave_mesh->finalize();

        FunctionSpace<MeshT> master(master_mesh), slave(slave_mesh);

        //! make sure that this refelects the dof paritioning of your function space
        Integer n_master_local_dofs = master_mesh->n_nodes();
        Integer n_slave_local_dofs = slave_mesh->n_nodes();

        // Same parametrization for the space as for the mesh
        // In this case is linear triangular finite elements
        master.make_iso_parametric();
        slave.make_iso_parametric();

        master.dof_map().set_n_dofs(n_master_local_dofs, n_master_local_dofs);
        slave.dof_map().set_n_dofs(n_slave_local_dofs, n_slave_local_dofs);

        /////////////////////////////////////////////////
        // Compute
        /////////////////////////////////////////////////

        ParL2Transfer<Real, 2> transfer(comm);
        transfer.assemble(master, slave);

        /////////////////////////////////////////////////
        // Post-process (Usually the matrices are converted and used in the client library representation)
        /////////////////////////////////////////////////

        // For P1 elements, transformation_matrix is the identity, for higher order it is a basis transformation
        const auto &mass_matrix = transfer.mass_matrix();
        const auto &coupling_matrix = transfer.coupling_matrix();
        const auto &transformation_matrix = transfer.transformation_matrix();

        if (verbose) {
            mass_matrix.synch_describe(logger());
            coupling_matrix.synch_describe(logger());
            transformation_matrix.synch_describe(logger());
        }

        // The client library will have to convert the three matrices to its format
        // The transfer operator T(.) = transformation_matrix * (mass_matrix \ (coupling_matrix * ( . )))
        // When using the biorthognal basis mass_matrix is diagonal so T can be assembled explicitly
        // T can then be used for transfering fields from the master space to the slave space

        const Real sum_mm = mass_matrix.sum();
        const Real sum_cm = coupling_matrix.sum();
        const Real sum_tm = transformation_matrix.sum();

        if (verbose && comm.is_root()) {
            // These conditions must be true
            logger() << sum_mm << " == " << sum_cm << " == expected_value: 0.5" << std::endl;
            logger() << sum_tm << " == " << n_slave_local_dofs << " == expected_value: 6" << std::endl;
        }
    }

    return Moonolith::Finalize();
}
