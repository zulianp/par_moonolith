#ifndef MOONOLITH_INTERSECT_POLYLINE_WITH_CONVEX_POLYGON_HPP
#define MOONOLITH_INTERSECT_POLYLINE_WITH_CONVEX_POLYGON_HPP

#include "moonolith_polygon.hpp"
#include "moonolith_line.hpp"
#include "moonolith_halfspace.hpp"
#include "moonolith_intersect_polygons.hpp"
#include "moonolith_h_polytope.hpp"
#include "moonolith_intersect_halfspace.hpp"

namespace moonolith {
	template<typename T>
	class IntersectPolylineWithConvexPolygon2 {
	public:
		using Point2     = moonolith::Vector<T, 2>;
		using Vector2    = moonolith::Vector<T, 2>;
		using Line2      = moonolith::Line<T, 2>;
		using HalfPlane2 = moonolith::HalfSpace<T, 2>;
		using Polygon2   = moonolith::Polygon<T, 2>;
		using HPolygon   = moonolith::HPolytope<T, 2>;
		using IsectLineHS = moonolith::IntersectLineWithHalfSpace<T ,2>;

		bool apply(const Line2 &line, const Polygon2 &polygon, Line2 &result, const T &tol = 1e-10);

		inline bool has_side_intersection() const {
			return side_num_ != -1;
		}

		inline Integer side_num() const
		{
			return side_num_;
		}

	private:
		Storage<short> is_inside;
		Storage<Point2> input_buffer;
		Vector2 u, v;
		// IntersectPolygons<T, 2> isect_convex_polygon;
		RemoveDuplicatePoints<T, 2> remove_dups;
		IsectLineHS isect_halfspace;
		Integer side_num_;
		Line2 buff;

		HPolygon h_poly;
	};

	template<typename T, int Dim>
	inline T polyline_length(const Storage<Vector<T, Dim>> &points)
	{
		const std::size_t n_points = points.size();

		T res = 0.;
		for(std::size_t i = 1; i < n_points; ++i) {
			const auto im1 = i - 1;
			const T dx = points[i].x - points[im1].x;
			const T dy = points[i].y - points[im1].y;
			res += std::sqrt(dx*dx + dy*dy);
		}

		return res;
	}
	
}

#endif //MOONOLITH_INTERSECT_POLYLINE_WITH_CONVEX_POLYGON_HPP