#ifndef MOONOLITH_NTREE_WITH_SPAN_MUTATOR_FACTORY_HPP
#define MOONOLITH_NTREE_WITH_SPAN_MUTATOR_FACTORY_HPP

#include "moonolith_mutator_factory.hpp"
#include "moonolith_n_tree_can_refine.hpp"
#include "moonolith_n_tree_refine.hpp"
#include "moonolith_n_tree_bound_expand.hpp"
#include "moonolith_n_tree_with_span_bound_expand.hpp"
#include "moonolith_n_tree_with_span_insert.hpp"
#include "moonolith_n_tree_with_span_match.hpp"
#include "moonolith_n_tree_bound_synchronize.hpp"
#include "moonolith_n_tree_hash.hpp"

#include <memory>

namespace moonolith {

	template<class Tree>
	class NTreeWithSpanMutatorFactory : public MutatorFactory<typename Tree::Traits> {
	public:
		
		typedef typename Tree::Traits Traits;
		typedef typename Traits::DataType DataType;
		typedef typename Traits::Bound Bound;
		typedef moonolith::Node<Traits> Node;
		typedef moonolith::Branch<Traits> Branch;
		typedef moonolith::Leaf<Traits> Leaf;
		typedef moonolith::Root<Traits> Root;
		typedef moonolith::TreeMemory<Traits> TreeMemory;

		void set_refine_params(const Integer max_leaf_size, const Integer max_depth)
		{
			max_leaf_size_ = max_leaf_size;
			max_depth_ = max_depth;
		}

		virtual std::shared_ptr< Mutator<Traits> > new_can_refine() const override
		{
			return  std::make_shared< NTreeCanRefine<Tree> >(max_leaf_size_, max_depth_);
		}

		virtual std::shared_ptr< Mutator<Traits> >  new_insert(const DataHandle &handle, const DataType &data) const override
		{
			return std::make_shared<  NTreeWithSpanInsert<Tree> >(handle, data);
		}

		virtual std::shared_ptr< Mutator<Traits> > new_refine() const override
		{
			return std::make_shared< NTreeRefine<Tree> >();

		}

		virtual std::shared_ptr< Mutator<Traits> > new_bound_expand(const DataType &data) const override
		{
			return std::make_shared< NTreeBoundExpand<Tree> >(data);
		}

		virtual std::shared_ptr< Mutator<Traits> > new_bound_expand(const Bound &bound) const override
		{
			return std::make_shared< NTreeBoundExpand<Tree> >(bound);
		}

		virtual std::shared_ptr< Mutator<Traits> > new_match(const DataType &data) const override
		{
			return std::make_shared< NTreeWithSpanMatch<Tree> >(data);
		}

		virtual std::shared_ptr< NodeNavigator<Traits> > new_node_navigator() const override
		{
			return std::make_shared< BreadthFirstNavigator<Tree> >();
		}

		virtual std::shared_ptr< RemoteNode<Node> > new_remote_node() const override
		{
			return std::make_shared< RemoteNodeWithBounds<Node> >();
		}

		virtual std::shared_ptr< BoundSynchronize<Traits> > new_bound_synchronize() const override
		{
			return std::make_shared< NTreeBoundSynchronize<Tree> >();
		}

		virtual std::shared_ptr< Hash<Traits> > new_hash() const override
		{
			return std::make_shared< NTreeHash<Tree> >();
		}

		NTreeWithSpanMutatorFactory()
		: max_leaf_size_(DEFAULT_REFINE_MAX_ELEMENTS), max_depth_(DEFAULT_REFINE_DEPTH)
		{}

	private:
		Integer max_leaf_size_, max_depth_;
	};

}

#endif //MOONOLITH_MUTATOR_FACTORY_HPP
