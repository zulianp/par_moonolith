#ifndef MOONOLITH_INTERSECT_POLYGON_WITH_H_POLYHEDRON_HPP
#define MOONOLITH_INTERSECT_POLYGON_WITH_H_POLYHEDRON_HPP

#include "moonolith_polygon.hpp"
#include "moonolith_polyhedron.hpp"
#include "moonolith_h_polytope.hpp"
#include "moonolith_line.hpp"
#include "moonolith_intersect_halfspace.hpp"
#include "moonolith_intersect_polygons.hpp"

namespace moonolith {

	template<typename T>
	class IntersectPolygonWithHPolyhedron {
	public:

		using Polygon3 = moonolith::Polygon<T, 3>;
		using Vector   = typename Polygon3::Vector;
		using HPolyhedron = moonolith::HPolytope<T, 3>;
		using Line3    = moonolith::Line<T, 3>;
		using IsectLineWithHS = moonolith::IntersectLineWithHalfSpace<T, 3>;

		short apply(const Polygon3 &polygon, const HPolyhedron &h_poly, Polygon3 &result, T tol = 1e-10);

		short apply(const Polygon3 &polygon, const Polyhedron<T> &poly, Polygon3 &result, T tol = 1e-10)
		{
			HPolyhedron h_poly;
			h_poly.make(poly);
			return apply(polygon, h_poly, result, tol);
		}

		inline bool has_side_intersection() const {
			return side_num_ != -1;
		}

		inline Integer side_num() const
		{
			return side_num_;
		}

	private:
		Line3 line, isect_line;
		Polygon3 in;

		IsectLineWithHS isect_line_with_half_space;
		IntersectConvexPolygons<T, 3> intersect_polygons;

		Integer side_num_;
	};


}

#endif //MOONOLITH_INTERSECT_POLYGON_WITH_H_POLYHEDRON_HPP
