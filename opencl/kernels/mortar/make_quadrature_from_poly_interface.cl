#ifndef MAKE_QUADRATURE_FROM_POLY_INTERFACE_CL
#define MAKE_QUADRATURE_FROM_POLY_INTERFACE_CL

#include "kernel_base.cl"

#ifndef CLIPP_HOST_CL

static SizeType n_volume_elements(const PMesh polyhedron);
static SizeType max_n_elements_from_facets(const PMesh polyhedron);

SizeType make_quadrature_points_from_polygon_2(const SizeType n_points, const Scalar * polygon, const Scalar rescaling, 
	const SizeType order, Scalar *composite_quad_points, Scalar *composite_quad_weights);

SizeType make_quadrature_points_from_polygon_3(const SizeType n_points, const Scalar * polygon, const Scalar rescaling, 
	const SizeType order, Scalar *composite_quad_points, Scalar *composite_quad_weights);

SizeType make_quadrature_points_from_polyhedron(const PMesh polyhedron, const Scalar rescaling, const SizeType quad_order, 
	Scalar * quad_points, Scalar * quad_weights);


SizeType make_quadrature_from_tetrahedron(const Scalar *points, const Scalar rescaling, 
										const SizeType order, 
										Scalar * composite_quad_points, 
										Scalar * composite_quad_weights);

static SizeType make_quadrature_points_from_polyhedron_in_range_around_point(
	const PMesh polyhedron, 
	const SizeType begin,
	const SizeType end,
	const Scalar rescaling, 
	const SizeType n_quad_points,
	const Scalar * quad_points,
	const Scalar * quad_weights,
	const Scalar * barycenter,
	Scalar * composite_quad_points, 
	Scalar * composite_quad_weights);


SizeType make_quadrature_points_from_polyline_2(
	const SizeType n_points, const Scalar *poly_line, const Scalar scale, 
	const SizeType order, Scalar *quad_points, Scalar *quad_weights);

#endif //CLIPP_HOST_CL

#endif //MAKE_QUADRATURE_FROM_POLY_INTERFACE_CL