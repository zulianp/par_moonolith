#ifndef MOONOLITH_TRAFO_ELEMENT_HPP
#define MOONOLITH_TRAFO_ELEMENT_HPP

#include "moonolith_elem_dual.hpp"

namespace moonolith {

    template <class Elem, bool NATIVE = true>
    class TransformedFE final : public FEInterface<typename Elem::T, Elem::Dim, Elem::PhysicalDim> {
    public:
        static const int Dim = Elem::Dim;
        static const int PhysicalDim = Elem::PhysicalDim;
        static const int NNodes = Elem::NNodes;

        using uint = unsigned int;

        using T = typename Elem::T;
        using FE = moonolith::FEInterface<T, Dim, PhysicalDim>;

        using Point = typename Elem::Point;
        using Vector = typename Elem::Vector;

        using CoPoint = typename Elem::CoPoint;
        using CoVector = typename Elem::CoVector;

        template <typename E>
        using Array = typename StorageTraits<E, NNodes>::Type;

        TransformedFE()
            : primal_(nullptr) /*, alpha_(alpha)*/
        {
            if (NNodes > 0) {
                init_weights();
            }
        }

        void init(FE &primal) {
            primal_ = &primal;
            init();
        }

        void reinit() {
            assert(primal_ && "init has to be called at least once before");

            const uint n = n_quad_points();
            const uint n_qp = n_quad_points();
            const uint n_fun = n_shape_functions();

            if (has_fun()) {
                for (uint i = 0; i < n_fun; ++i) {
                    const auto offset_i = i * n_fun;

                    fun_[i].resize(n);

                    for (uint k = 0; k < n_qp; ++k) {
                        fun_[i][k] = 0.;

                        for (uint j = 0; j < n_fun; ++j) {
                            fun_[i][k] += w.values[offset_i + j] * primal_->fun(j, k);
                        }
                    }
                }
            }

            if (has_grad()) {
                Vector g;

                for (uint i = 0; i < n_fun; ++i) {
                    const auto offset_i = i * n_fun;

                    grad_[i].resize(n_qp);

                    for (uint k = 0; k < n_qp; ++k) {
                        moonolith::fill(grad_[i][k], static_cast<T>(0.));

                        for (uint j = 0; j < n_fun; ++j) {
                            primal_->grad(j, k, g);
                            grad_[i][k] += w.values[offset_i + j] * g;
                        }
                    }
                }
            }
        }

        inline T fun(const Integer i, const Integer qp) override { return fun_[i][qp]; }

        inline void grad(const Integer i, const Integer qp, Vector &g) override { g = grad_[i][qp]; }

        inline void jacobian(const Integer qp, Matrix<T, PhysicalDim, Dim> &J) override {
            assert(primal_);
            primal_->jacobian(qp, J);
        }

        inline T dx(const Integer qp) override {
            assert(primal_);
            return primal_->dx(qp);
        }

        inline uint n_shape_functions() const override { return static_cast<uint>(fun_.size()); }

        inline uint n_quad_points() const override {
            assert(primal_);
            return static_cast<uint>(primal_->n_quad_points());
        }

        inline bool has_fun() const override {
            assert(primal_);
            return primal_->has_fun();
        }

        inline bool has_grad() const override {
            assert(primal_);
            return primal_->has_grad();
        }

        inline bool has_jacobian() const override {
            assert(primal_);
            return primal_->has_jacobian();
        }

        inline FEType type() const override {
            assert(primal_);
            return primal_->type();
        }

        inline const Matrix<T, NNodes, NNodes> &transformation() const { return w; }

        inline const Matrix<T, NNodes, NNodes> &inverse_transformation() const { return w_inv; }

    private:
        FE *primal_;
        // T alpha_;
        Matrix<T, NNodes, NNodes> w, w_inv;
        Array<Storage<T>> fun_;
        Array<Storage<Vector>> grad_;

        inline void init_weights() {
            DualWeights<Elem>::basis_transformation(w);
            DualWeights<Elem>::inverse_basis_transformation(w_inv);
        }

        inline void init_weights_with_type() {
            DualWeightsWithType<Elem>::basis_transformation(type(), w);
            DualWeightsWithType<Elem>::inverse_basis_transformation(type(), w_inv);
        }

        inline void set_sizes(const int n_funs) {
            Resize<Array<Storage<T>>>::apply(fun_, n_funs);
            Resize<Array<Storage<Vector>>>::apply(grad_, n_funs);
        }

        void init() {
            assert(primal_);
            set_sizes(primal_->n_shape_functions());

            if (NNodes <= 0) {
                init_weights_with_type();
            }

            reinit();
        }
    };

}  // namespace moonolith

#endif  // MOONOLITH_TRAFO_ELEMENT_HPP
