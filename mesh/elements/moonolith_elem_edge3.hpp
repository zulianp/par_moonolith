#ifndef MOONOLITH_ELEM_EDGE_3_HPP
#define MOONOLITH_ELEM_EDGE_3_HPP

#include "moonolith_elem_edge.hpp"

namespace moonolith {

    template<typename T_, int PhysicalDim_ = 1>
    class Edge3 final : public Edge<T_, 2, PhysicalDim_> {
    public:
        static const int Order = 2;
        static const int Dim   = 1;
        static const int PhysicalDim = PhysicalDim_;
        static const int NNodes = 3;

        using T = T_;

        using Vector   = moonolith::Vector<T, Dim>;
        using CoVector = moonolith::Vector<T, PhysicalDim>;

        using Point   = moonolith::Vector<T, Dim>;
        using CoPoint = moonolith::Vector<T, PhysicalDim>;

    private:

        class Fun final {
        public:

            Fun()
            {
                f[0] = [](const Point &p) -> T {
                    return 1.0 - 3.0 * p.x + 2.0 * (p.x * p.x);
                };

                f[1] = [](const Point &p) -> T {
                    return -p.x + 2.0 * (p.x * p.x);
                };

                f[2] = [](const Point &p) -> T {
                    return 4.0 * p.x - 4.0 * (p.x * p.x);
                };
            }

            std::array<std::function<T (const Point &)>, 3> f;
        };

        class Grad final {
        public:
            Grad()
            {
                f[0] = [](const Point &p, Vector &g) {
                    g.x = -3.0 + 4.0 * p.x;
                };

                f[1] = [](const Point &p, Vector &g) {
                    g.x = -1.0 + 4.0 * p.x;
                };

                f[2] = [](const Point &p, Vector &g) {
                    g.x = 4.0 - 8.0 * p.x;
                };
            }

            std::array<std::function<void (const Point &, Vector &)>, 3> f;
        };

        class Hessian {
        public:

            Hessian()
            {   
                //g.x = -3.0 + 4.0 * p.x;
                f[0] = [](const Point &, std::array<T, Dim*Dim> &H) {
                    H[0] = 4.0;
                };

                //g.x = -1.0 + 4.0 * p.x;
                f[1] = [](const Point &, std::array<T, Dim*Dim> &H) {
                    H[0] = 4.0;
                };

                //g.x = 4.0 - 8.0 * p.x;
                f[2] = [](const Point &, std::array<T, Dim*Dim> &H) {
                    H[0] = -8.0;
                };
            }   

            // H = [ f_xx, f_xy; f_yx, f_yy ];
            std::array<std::function<void (const Point &, std::array<T, Dim*Dim> &)>, 3> f;
        };

    public:

        T fun(const Integer i, const Point &p) const override
        {
            return fun_.f[i](p);
        }

        void grad(const Integer i, const Point &p, Vector &g) const override
        {
            grad_.f[i](p, g);
        }

        void hessian(const Integer i, const Point &p, std::array<T, Dim*Dim> &H)
        {
            hessian_.f[i](p, H);
        }

        void jacobian(const Point &p, std::array<T, PhysicalDim*Dim> &J) const override
        {
            IsoParametric<Edge3>::jacobian(*this, p, J);
        }

        void affine_approx_jacobian(std::array<T, PhysicalDim*Dim> &J) const override
        {
            Affine<Edge3>::jacobian(*this, J);
        }

        CoPoint &node(const Integer i) override
        {
            return nodes_[i];
        }

        const CoPoint &node(const Integer i) const override
        {
            return nodes_[i];
        }

        void point(const Point &p, CoPoint &q) const override
        {
            IsoParametric<Edge3>::point(*this, p, q);
        }

        Edge3() : is_affine_(false) {}

        bool is_affine() const override { return is_affine_; }
        inline bool is_simplex() const override { return true; }
        inline int order() const override { return Order; }

        inline void set_affine(const bool val)
        {
            is_affine_ = val;
        }

        inline int n_nodes() const override { return NNodes; }
        const std::array<CoPoint, NNodes> &nodes() const { return nodes_; }

        void make_reference()
        {
            Reference<Edge3>::points(nodes_);
            set_affine(true);
        }

        inline ElemType type() const override
        {
            return EDGE3;
        }

        inline T measure() const override
        {
            if(is_affine()) {
                return distance(nodes_[0], nodes_[1]);
            } else {
                Quadrature<T, Dim> q;
                Gauss::get(1, q);
                return IsoParametric<Edge3>::measure(*this, q);
            }
        }

        inline T approx_measure() const override { 
            if(is_affine()) {
                return distance(nodes_[0], nodes_[1]);
            } else {
                return distance(nodes_[0], nodes_[2]) + distance(nodes_[1], nodes_[2]);
            }
        }

        inline T reference_measure() const override { return 1.0; }

        inline void make(Line<T, PhysicalDim_> &line) const override
        {
            line.p0 = node(0);
            line.p1 = node(1);
        }

        inline void make(PolyLine<T, PhysicalDim_> &poly_line) const override
        {
            poly_line.points.resize(3);
            poly_line.points[0] = node(0);
            poly_line.points[1] = node(2);
            poly_line.points[2] = node(1);
        }

    private:
        bool is_affine_;
        std::array<CoPoint, NNodes> nodes_;
        const Fun  fun_;
        const Grad grad_;
        const Hessian hessian_;
    };

    template<typename T, int PhysicalDim>
    class Reference<Edge3<T, PhysicalDim>> {
    public:
        static const int NNodes = 3;
        
        template<class Point, std::size_t Size>
        static void points(std::array<Point, Size> &nodes)
        {
            static_assert(Size >= NNodes, "size must be at least 3");
            //p0
            nodes[0].x = 0.0;

            //p1
            nodes[1].x = 1.0;

            //p2
            nodes[2].x = 0.5;
        }

        static T measure() { return 1.0; }
    };

    template<typename T, int PhysicalDim>
    class GaussQRule< Edge3<T, PhysicalDim> > {
    public:
        static bool get(const Integer order, Quadrature1<T> &q) {
            return Gauss::get(order, q);
        }
    };

}

#endif //MOONOLITH_ELEM_EDGE_3_HPP
