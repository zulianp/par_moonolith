#include <benchmark/benchmark.h>
#include "par_moonolith_instance.hpp"

// BENCHMARK_MAIN();

int main(int argc, char** argv) {
    using namespace moonolith;

    Moonolith::Init(argc, argv);

    ::benchmark::Initialize(&argc, argv);
    ::benchmark::RunSpecifiedBenchmarks();

    return Moonolith::Finalize();
}
