#ifndef MORTAR_INTERFACES_CL
#define MORTAR_INTERFACES_CL 

#include "intersect_interface.cl"
#include "make_quadrature_from_poly_interface.cl"

#endif  //MORTAR_INTERFACES_CL
