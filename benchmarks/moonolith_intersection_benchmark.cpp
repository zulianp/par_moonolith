// moonolith_intersection_benchmark.cpp

#include "moonolith_build_quadrature.hpp"
#include "moonolith_convex_decomposition.hpp"
#include "moonolith_elem_hexahedron.hpp"
#include "moonolith_elem_polymorphic.hpp"
#include "moonolith_elem_tetrahedron.hpp"
#include "moonolith_graham_scan.hpp"
#include "moonolith_intersect_polyhedra.hpp"
#include "moonolith_intersect_polyhedra_impl.hpp"
#include "moonolith_l2_assembler_no_covering.hpp"
#include "moonolith_map_quadrature_impl.hpp"
#include "moonolith_par_l2_assembler_no_covering.hpp"

#include <cmath>

#include <benchmark/benchmark.h>

using moonolith::Gauss;
using moonolith::Integer;
using moonolith::L2Transfer;
using moonolith::Real;

using Polyhedron = moonolith::Polyhedron<Real>;
using HPolytope3 = moonolith::HPolytope<Real, 3>;
using Point3 = moonolith::Vector<Real, 3>;
using MasterElem = moonolith::Tet10<Real>;
using SlaveElem = moonolith::Tet10<Real>;
using Quadrature3 = moonolith::Quadrature3<Real>;

static void BM_point_intersection(benchmark::State& state) {
    Polyhedron p1;
    make_cube({0.0, 0.0, 0.0}, {1., 1., 1.}, p1);
    Point3 p2(0.5, 0.5, 0.5);

    HPolytope3 h;
    h.make(p1);

    for (auto _ : state) {
        bool val = h.contains(p2);
        benchmark::DoNotOptimize(val);
    }
}

BENCHMARK(BM_point_intersection);

static void BM_l2_transfer(benchmark::State& state) {
    Quadrature3 q_rule;
    Gauss::get(6, q_rule);

    MasterElem master;
    SlaveElem slave;

    master.make_reference();
    slave.make_reference();

    L2Transfer<MasterElem, SlaveElem> algo;
    algo.set_quadrature(q_rule);

    for (auto _ : state) {
        bool val = algo.assemble(master, slave);
        benchmark::DoNotOptimize(val);
    }
}

BENCHMARK(BM_l2_transfer);

// BENCHMARK_TEMPLATE(TemplateFun, Args...);
