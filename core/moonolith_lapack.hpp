#ifndef MOONOLITH_LAPACK_HPP
#define MOONOLITH_LAPACK_HPP
#include "par_moonolith_config.hpp"
#ifdef MOONOLITH_HAVE_LAPACK

#include "moonolith_expanding_array.hpp"

namespace moonolith {

    template <typename T>
    class LapackInverse {
    public:
        bool init(const int rows, const T* mat);
        bool apply(const T* in, T* out, const int nrhs);

    private:
        int rows_;
        Storage<T> mat_;
    };

}  // namespace moonolith

#endif  // MOONOLITH_HAVE_LAPACK
#endif  // MOONOLITH_LAPACK_HPP