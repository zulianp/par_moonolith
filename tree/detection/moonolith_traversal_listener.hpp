#ifndef MOONOLITH_TRAVERSAL_LISTENER_HPP
#define MOONOLITH_TRAVERSAL_LISTENER_HPP

#include "moonolith_remote_node.hpp"

namespace moonolith {

	template<class Tree>
	class TraversalListener {
	public:
		typedef typename Tree::Node Node;
		typedef moonolith::RemoteNode<Node> RemoteNode;

		virtual ~TraversalListener() {}
		virtual void entry_added(Tree &tree, Node &node, const RemoteNode &remoteNode, const Integer remoteRank) = 0;
		virtual void traversal_terminated() = 0;
	};
}

#endif //MOONOLITH_TRAVERSAL_LISTENER_HPP
