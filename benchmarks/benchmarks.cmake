# benchmarks.cmake

if(MOONOLITH_ENABLE_BENCHMARK)
    # ##########################################################################
    set(BENCHMARK_ENABLE_TESTING OFF)
    # message(STATUS "GOOGLETEST_PATH=${GOOGLETEST_PATH}")

    find_package(benchmark QUIET)

    if(NOT benchmark_FOUND)
        include(FetchContent)

        FetchContent_Declare(
            benchmark
            GIT_REPOSITORY https://github.com/google/benchmark.git
            CMAKE_ARGS -DBENCHMARK_ENABLE_WERROR=OFF
            GIT_TAG v1.9.1)

        FetchContent_GetProperties(benchmark)

        if(NOT benchmark_POPULATED)
            FetchContent_Populate(benchmark)
            add_subdirectory(${benchmark_SOURCE_DIR} ${benchmark_BINARY_DIR})
        endif()

        set_target_properties(benchmark PROPERTIES FOLDER extern)
        set_target_properties(benchmark_main PROPERTIES FOLDER extern)

        set(MOONOLITH_BENCH_LIBRARIES benchmark)
    else()
        set(MOONOLITH_BENCH_LIBRARIES benchmark::benchmark)
    endif()

    # ##########################################################################

    set(MOONOLITH_BENCH_DIR ${CMAKE_CURRENT_SOURCE_DIR}/benchmarks)

    list(APPEND BENCH_MODULES .)

    set(LOCAL_HEADERS "")
    set(LOCAL_SOURCES "")
    find_project_files(MOONOLITH_BENCH_DIR BENCH_MODULES LOCAL_HEADERS
                       LOCAL_SOURCES)

    if(NOT LOCAL_SOURCES)
        # message(WARNING "For some reason benchmark was not found
        # automatically")

        list(APPEND LOCAL_SOURCES
             ${MOONOLITH_BENCH_DIR}/moonolith_intersection_benchmark.cpp)
    endif()

    # message( "LOCAL_SOURCES=${LOCAL_SOURCES},
    # MOONOLITH_BENCH_DIR=${MOONOLITH_BENCH_DIR}" )

    add_executable(
        moonolith_bench ${CMAKE_CURRENT_SOURCE_DIR}/benchmarks/benchmarks.cpp
                        ${LOCAL_SOURCES})

    target_link_libraries(moonolith_bench ${MOONOLITH_BENCH_LIBRARIES})

    target_link_libraries(moonolith_bench par_moonolith)

    target_include_directories(
        moonolith_bench PRIVATE "$<BUILD_INTERFACE:${MOONOLITH_BENCH_DIR}>")
    target_include_directories(moonolith_bench PRIVATE " $<BUILD_INTERFACE:.>")
    target_include_directories(moonolith_bench
                               PRIVATE " $<BUILD_INTERFACE:${BENCH_MODULES}>")

endif(MOONOLITH_ENABLE_BENCHMARK)
