#ifndef MOONOLITH_MAKE_UNIQUE_HPP
#define MOONOLITH_MAKE_UNIQUE_HPP

#include <utility>
#include <memory>

namespace moonolith {

#ifdef WITH_CPP14

	using std::make_unique;

#else	
	// note: this implementation does not disable this overload for array types
	template<typename T, typename... Args>
	std::unique_ptr<T> make_unique(Args&&... args)
	{
	    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
	}

#endif //WITH_CPP14

    template <typename T>
    struct EmptyDeleter
    {
        EmptyDeleter() /* noexcept */
        {
        }

        template <typename U>
        EmptyDeleter(const EmptyDeleter<U>&,
            typename std::enable_if<
                std::is_convertible<U*, T*>::value
            >::type* = nullptr) /* noexcept */
        {
        }

        void operator()(T* const) const /* noexcept */
        {
            // do nothing
        }
    };


    /*!
     * @fn std::shared_ptr<T> make_ref(T &obj)
     * @brief use only when you are sure that the scope of the pointer is limited to the life of obj
     * @return a shared_ptr that does not delete the pointer
     */
    template<typename T>
    std::shared_ptr<T> make_ref(T &obj)
    {
        return std::shared_ptr<T>(&obj, EmptyDeleter<T>());
    }

    /*!
     * @fn std::shared_ptr<const T> make_ref(const T &obj)
     * @brief use only when you are sure that the scope of the pointer is limited to the life of obj
     * @return a shared_ptr that does not delete the pointer
     */
    template<typename T>
    std::shared_ptr<const T> make_ref(const T &obj)
    {
        return std::shared_ptr<const T>(&obj, EmptyDeleter<const T>());
    }


}

#endif //MOONOLITH_MAKE_UNIQUE_HPP
