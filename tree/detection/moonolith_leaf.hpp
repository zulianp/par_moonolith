#ifndef MOONOLITH_LEAF_HPP
#define MOONOLITH_LEAF_HPP

#include "moonolith_forward_declarations.hpp"
#include "moonolith_navigator_option.hpp"

namespace moonolith {
	template<class Traits>
	class Leaf : public Node<Traits> {
	private:
		typedef moonolith::Node<Traits> super;

	public:
		using super::accept;

		virtual NavigatorOption accept(Visitor<Traits> &visitor, TreeMemory<Traits> &memory)
		{
			return visitor.visit(*this, memory);
		}

		virtual bool is_leaf() const { return true; }
		virtual bool is_visitable() const { return true; }
		virtual bool empty() const
		{
			return !this->has_data();
		}

		virtual std::string get_class() const
		{
			return "Leaf";
		}
	};
}

#endif //MOONOLITH_LEAF_HPP
