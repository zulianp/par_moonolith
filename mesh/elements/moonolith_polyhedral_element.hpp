#ifndef MOONOLITH_POLYHEDRAL_ELEMENT_HPP
#define MOONOLITH_POLYHEDRAL_ELEMENT_HPP

#include "moonolith_polyhedron.hpp"
#include "moonolith_elem.hpp"

namespace moonolith {
    
    template<typename T, int Dim>
    class PolyhedralElement : public Elem<T, 3, Dim> {
    public:
        virtual ~PolyhedralElement() {}
        
        virtual void make(Polyhedron<T> &poly) const = 0;
        
        virtual void make(Storage<Polyhedron<T>> &poly) const 
        {
            poly.resize(1);
            make(poly[0]);
        }
    };

    template<class Elem>
    class MakePolyhedron {
    public:
        template<typename T, class Any>
        static void apply(const PolyhedralElement<T, 3> &e, Any &poly)
        {
             e.make(poly);
        }
    };

    template<typename T, int Dim>
    class MakePolyhedron<Elem<T, 3, Dim>> {
    public:
        template<class Any>
        static void apply(const Elem<T, 3, Dim> &e, Any &poly)
        {
             auto e_ptr = dynamic_cast<const PolyhedralElement<T, Dim> *>(&e);
             if(e_ptr) {
                 e_ptr->make(poly);
             }
        }
    };

    template<class Elem>
    inline void make(const Elem &elem, Polyhedron<typename Elem::T> &poly) {
        static_assert(Elem::Dim == 3, "only for volumetric elements");
        MakePolyhedron<Elem>::apply(elem, poly);
    }

    template<class Elem>
    inline void make(const Elem &elem, Storage<Polyhedron<typename Elem::T>> &poly) {
        static_assert(Elem::Dim == 3, "only for volumetric elements");
        MakePolyhedron<Elem>::apply(elem, poly);
    }

}

#endif //MOONOLITH_POLYHEDRAL_ELEMENT_HPP
