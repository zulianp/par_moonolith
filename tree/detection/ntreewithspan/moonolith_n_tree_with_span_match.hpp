#ifndef MOONOLITH_NTREE_WITH_SPAN_MATCH_HPP
#define MOONOLITH_NTREE_WITH_SPAN_MATCH_HPP

#include "moonolith_match.hpp"
#include "moonolith_node.hpp"
#include "moonolith_leaf.hpp"
#include "moonolith_branch.hpp"

namespace moonolith {

	template<class Tree>
	class NTreeWithSpanMatch : public Match<Tree> {
	public:
		typedef typename Tree::Traits Traits;
		typedef typename Traits::DataType DataType;
		typedef moonolith::Node<Traits> Node;
		typedef moonolith::Branch<Traits> Branch;
		typedef moonolith::Leaf<Traits> Leaf;
		typedef moonolith::Root<Traits> Root;
		
		virtual bool match(Node &node) const
		{
			return _obj.bound().intersects(node.bound());
		}

		virtual bool match(DataType &object) const 
		{
			return _obj.bound().intersects(object.bound());
		}

		NTreeWithSpanMatch(const DataType &obj)
		: _obj(obj)
		{}

		virtual ~NTreeWithSpanMatch() {}

		inline const DataType &obj() const { return _obj; }

	private:
		DataType _obj;
	};

}

#endif //MOONOLITH_NTREE_WITH_SPAN_MATCH_HPP
