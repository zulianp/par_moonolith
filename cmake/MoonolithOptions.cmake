# MoonolithOptions

# ##############################################################################
option(BUILD_SHARED_LIBS "build shared libraries" OFF)
option(USE_XSDK_DEFAULTS "XSDK mode" OFF)

# option(MOONOLITH_ENABLE_SANITIZER "check for memory access problems" OFF)
option(MOONOLITH_ENABLE_GLIBCXX_DEBUG
       "uses flags -D_GLIBCXX_DEBUG when compiling in debug mode" OFF)
option(MOONOLITH_ENABLE_PROFILER "Allows producing profiles of the runs" OFF)
option(MOONOLITH_ENABLE_DEV_MODE
       "Add additional flags for more strict compilation" OFF)

option(MOONOLITH_ENABLE_CHECK_STREAM "Check stream messages by adding headers and footers and check consistency" OFF)

get_directory_property(HAS_PARENT PARENT_DIRECTORY)
if(HAS_PARENT)
    option(MOONOLITH_ENABLE_TESTING "Build the tests" OFF)
    option(MOONOLITH_ENABLE_BENCHMARK "enable benchmark suite" OFF)
else()
    option(MOONOLITH_ENABLE_TESTING "Build the tests" OFF)
    option(MOONOLITH_ENABLE_BENCHMARK "enable benchmark suite" OFF)
endif()

# ##############################################################################
# XSDK_PRECISION
# ##############################################################################

if(NOT XSDK_PRECISION OR USE_XSDK_DEFAULTS)
    set(XSDK_PRECISION "DOUBLE")
endif()

string(COMPARE EQUAL ${XSDK_PRECISION} "DOUBLE" MOONOLITH_HAVE_DOUBLE_PRECISION)
string(COMPARE EQUAL ${XSDK_PRECISION} "SINGLE" MOONOLITH_HAVE_SINGLE_PRECISION)
string(COMPARE EQUAL ${XSDK_PRECISION} "QUAD" MOONOLITH_HAVE_QUAD_PRECISION)

# ##############################################################################
# XSDK_INDEX_SIZE
# ##############################################################################

if(USE_XSDK_DEFAULTS)
    set(XSDK_INDEX_SIZE 32)
    set(MOONOLITH_INDEX_SIZE 32)
else()
    if(XSDK_INDEX_SIZE)
        set(MOONOLITH_INDEX_SIZE ${XSDK_INDEX_SIZE})
    elseif(NOT MOONOLITH_INDEX_SIZE)
        set(XSDK_INDEX_SIZE 64)
        set(MOONOLITH_INDEX_SIZE 64)
    endif()
endif()

# ##############################################################################
# Handle xSDK defaults
# ##############################################################################

include(CMakeDependentOption)
cmake_dependent_option(BUILD_SHARED_LIBS "Build shared libraries" OFF
                       "NOT USE_XSDK_DEFAULTS" ON)

# FIXME: LAPACK supporting long double?
cmake_dependent_option(MOONOLITH_ENABLE_LAPACK "Enable
Lapack for dense matrix operations" ON "NOT MOONOLITH_HAVE_QUAD_PRECISION" OFF)

# MPI is required cmake_dependent_option(MOONOLITH_ENABLE_MPI "Enable MPI
# support" ON "NOT TPL_ENABLE_MPI" OFF)

if(NOT CMAKE_BUILD_TYPE)
    if(USE_XSDK_DEFAULTS)
        set(CMAKE_BUILD_TYPE
            "Debug"
            CACHE STRING "Choose the type of build, options are: Debug Release
        RelWithDebInfo MinSizeRel." FORCE)

        message(
            STATUS
                "[Status] Since USE_XSDK_DEFAULTS=ON then CMAKE_BUILD_TYPE=Debug"
        )

    else()
        set(CMAKE_BUILD_TYPE
            "Release"
            CACHE STRING "Choose the type of build, options are: Debug Release
RelWithDebInfo MinSizeRel." FORCE)

        message(STATUS "[Status] CMAKE_BUILD_TYPE=Release")

    endif()
endif(NOT CMAKE_BUILD_TYPE)

# ##############################################################################
# ##############################################################################
# ##############################################################################

if(MOONOLITH_ENABLE_DEV_MODE)
    set(MOONOLITH_DEV_FLAGS
        "-Wall -Wextra -pedantic -Werror -Werror=enum-compare -Werror=delete-non-virtual-dtor -Werror=reorder -Werror=return-type" # -Werror=uninitialized
    )
endif()

if(MOONOLITH_ENABLE_GLIBCXX_DEBUG)
    set(MOONOLITH_SPECIAL_DEBUG_FLAGS
        "${MOONOLITH_SPECIAL_DEBUG_FLAGS} -D_GLIBCXX_DEBUG")
endif()

if(MOONOLITH_ENABLE_PROFILER)
    set(MOONOLITH_PROFILING_ENABLED TRUE)
endif()

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}  ${MOONOLITH_DEV_FLAGS}")
set(CMAKE_CXX_FLAGS_DEBUG
    "${CMAKE_CXX_FLAGS_DEBUG} ${MOONOLITH_SPECIAL_DEBUG_FLAGS}")
