#ifndef MOONOLITH_NTREEWITHTAGSREFINEENTIRETREE_HPP
#define MOONOLITH_NTREEWITHTAGSREFINEENTIRETREE_HPP

#include "moonolith_refine_all.hpp"
#include "moonolith_forward_declarations.hpp"
#include "moonolith_predicate.hpp"

namespace moonolith {

    template<class Tree>
    class NTreeWithTagsRefineAll : public RefineAll<typename Tree::Traits> {
    public:
        typedef typename Tree::Traits Traits;
        typedef moonolith::Node<Traits> Node;
        typedef moonolith::Leaf<Traits> Leaf;
        typedef moonolith::TreeMemory<Traits> TreeMemory;

        virtual NavigatorOption visit(Node &, TreeMemory &) override
        {
            return CONTINUE;
        }

        virtual NavigatorOption visit(Leaf &node, TreeMemory &memory) override
        {
            if(!this->can_refine(node, memory)) return CONTINUE;

            return (!_predicate ||
                     _predicate->tags_are_related(node.tags().begin(), node.tags().end(), node.tags().begin(), node.tags().end())
                    ) ? REFINE : CONTINUE;
        }

        virtual bool success() const override
        {
            return true;
        }

        NTreeWithTagsRefineAll(const std::shared_ptr<Predicate> &predicate)
        : _predicate(predicate)
        {}

    private:
        std::shared_ptr<Predicate> _predicate;
    };

}

#endif //MOONOLITH_NTREEWITHTAGSREFINEENTIRETREE_HPP
