#ifndef MOONOLITH_ELEM_HEX_1_HPP
#define MOONOLITH_ELEM_HEX_1_HPP

#include "moonolith_convex_decomposition.hpp"
#include "moonolith_elem_hex.hpp"
#include "moonolith_elem_hex8.hpp"
#include "moonolith_elem_segment.hpp"

namespace moonolith {

    // Only affine case
    template <typename T_, int PhysicalDim_ = 3>
    class Hex1 final : public Hex<T_, 0, PhysicalDim_> {
    public:
        static const int Order = 0;
        static const int Dim = 3;
        static const int PhysicalDim = PhysicalDim_;
        static const int NNodes = 1;

        using T = T_;

        using Vector = moonolith::Vector<T, Dim>;
        using CoVector = moonolith::Vector<T, PhysicalDim>;

        using Point = moonolith::Vector<T, Dim>;
        using CoPoint = moonolith::Vector<T, PhysicalDim>;

        using Super = moonolith::Hex<T_, 0, PhysicalDim_>;
        using Super::make;

    private:
        class Grad final {
        public:
            Grad() {
                f[0] = [](const Point &, Vector &g) {
                    g.x = 0.0;
                    g.y = 0.0;
                    g.z = 0.0;
                };
            }

            std::array<std::function<void(const Point &, Vector &)>, NNodes> f;
        };

        class Fun final {
        public:
            Fun() {
                f[0] = [](const Point &) -> T { return 1.0; };
            }

            std::array<std::function<T(const Point &)>, NNodes> f;
        };

    public:
        T fun(const Integer i, const Point &p) const override { return fun_.f[i](p); }

        void grad(const Integer i, const Point &p, Vector &g) const override { grad_.f[i](p, g); }

        void hessian(const Integer, const Point &, std::array<T, Dim * Dim> &H) {
            std::fill(std::begin(H), std::end(H), 0.0);
        }

        void jacobian(const Point &p, std::array<T, PhysicalDim * Dim> &J) const override {
            (void)p;
            affine_approx_jacobian(J);
        }

        void affine_approx_jacobian(std::array<T, PhysicalDim * Dim> &J) const override {
            Affine<Hex8<T, PhysicalDim>>::jacobian(points_, {0, 1, 3, 4}, J);
        }

        CoPoint &node(const Integer i) override { return nodes_[i]; }

        const CoPoint &node(const Integer i) const override { return nodes_[i]; }

        void point(const Point &p, CoPoint &q) const override { Affine<Hex1>::point(points_, {0, 1, 3, 4}, p, q); }

        inline CoPoint &point(const Integer idx) { return points_[idx]; }

        inline const CoPoint &point(const Integer idx) const { return points_[idx]; }

        Hex1() {}

        // bool is_affine() const override { return is_affine_; }
        // void set_affine(const bool val) { is_affine_ = val; }

        bool is_affine() const override { return true; }

        inline int n_nodes() const override { return NNodes; }
        inline int order() const override { return Order; }

        const std::array<CoPoint, NNodes> &nodes() const { return nodes_; }
        const std::array<CoPoint, 8> &points() const { return points_; }

        inline void make_reference() {
            Reference<Hex1>::points(nodes_);
            Reference<Hex8<T, PhysicalDim>>::points(points_);
        }

        inline ElemType type() const override { return HEX1; }

        inline bool is_simplex() const override { return false; }

        inline T approx_measure() const override { return approx_hexahedron_volume(points_); }

        inline T measure() const override { return approx_hexahedron_volume(points_); }

        inline T reference_measure() const override { return 1.0; }

        inline void make(Polyhedron<T> &poly) const override {
            assert(is_affine());
            ConvexDecomposition<T, PhysicalDim>::make_hex(points(), 0, 1, 2, 3, 4, 5, 6, 7, poly);
        }

    private:
        // bool is_affine_;
        std::array<CoPoint, NNodes> nodes_;
        std::array<CoPoint, 8> points_;
        const Fun fun_;
        const Grad grad_;
    };

    template <typename T, int PhysicalDim>
    class Reference<Hex1<T, PhysicalDim>> {
    public:
        static const int NNodes = 1;

        template <class Point, std::size_t Size>
        static void points(std::array<Point, Size> &nodes) {
            static_assert(Size >= NNodes, "size must be at least NNodes");
            ///////////////////////
            // p0
            nodes[0].x = 0.5;
            nodes[0].y = 0.5;
            nodes[0].z = 0.5;
        }

        static T measure() { return 1.0; }
    };

    template <typename T, int PhysicalDim>
    class GaussQRule<Hex1<T, PhysicalDim>> {
    public:
        static bool get(const Integer order, Quadrature3<T> &q) { return Gauss::Hex::get(order, q); }
    };

}  // namespace moonolith

#endif  // MOONOLITH_ELEM_HEX_1_HPP
