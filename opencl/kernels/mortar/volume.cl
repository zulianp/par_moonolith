#ifndef VOLUME_CL
#define VOLUME_CL 

#include "kernel_base.cl"
#include "volume_interface.cl"

static Vector2 barycenter_2(const SizeType n_vertices, const Scalar *poly)
{
	Vector2 result = vec_2(0, 0);
	for(SizeType i = 0; i < n_vertices; ++i) {
		const SizeType i2 = i*2;
		result.x += poly[i2];
		result.y += poly[i2+1];
	}

	const Scalar one = 1;
	return result * (one/n_vertices);
}

static Vector3 barycenter_3(const SizeType n_vertices, const Scalar *poly)
{
	Vector3 result = vec_3(0, 0, 0);
	for(SizeType i = 0; i < n_vertices; ++i) {
		const SizeType i3 = i*3;
		result.x += poly[i3];
		result.y += poly[i3 + 1];
		result.z += poly[i3 + 2];
	}

	const Scalar one = 1;
	return result * (one/n_vertices);
}


static Scalar trapezoid_area_2(const Vector2 p1, const Vector2 p2, const Vector2 p3)
{
	const Vector2 d12 = p2 - p1;
	const Vector2 d13 = p3 - p1;
	return (d12.x * d13.y - d13.x * d12.y);
}

//unsigned area
static Scalar trapezoid_area_3(const Vector3 p1, const Vector3 p2, const Vector3 p3)
{
	const Vector3 u = p2 - p1;
	const Vector3 v = p3 - p1;
	return length(cross(u, v));
}

static Scalar polygon_area_2(const SizeType n_vertices, const Scalar *poly)
{
	if(n_vertices == 3) {
		const Vector2 p1 = vec_2(poly[0], poly[1]);
		const Vector2 p2 = vec_2(poly[2], poly[3]);
		const Vector2 p3 = vec_2(poly[4], poly[5]);
		return 0.5*trapezoid_area_2(p1, p2, p3);
	}

	Vector2 b = barycenter_2(n_vertices, poly);
	Vector2 pi;
	Vector2 pip1;

	Scalar result = 0;
	for(SizeType i = 0; i < n_vertices; ++i) {
		const SizeType ip1 = (i + 1 == n_vertices)? 0 : (i + 1);

		const SizeType i_2 	 = i   * 2;
		const SizeType ip1_2 = ip1 * 2;

		pi.x   = poly[i_2];
		pi.y   = poly[i_2 + 1];

		pip1.x = poly[ip1_2];
		pip1.y = poly[ip1_2 + 1];

		result += trapezoid_area_2(b, pi, pip1);
	}

	return 0.5*result;
}

static Scalar polygon_area_3(const SizeType n_vertices, const Scalar *poly)
{
	if(n_vertices == 3) {
		const Vector3 p1 = vec_3(poly[0], poly[1], poly[2]);
		const Vector3 p2 = vec_3(poly[3], poly[4], poly[5]);
		const Vector3 p3 = vec_3(poly[6], poly[7], poly[8]);
		return 0.5 * trapezoid_area_3(p1, p2, p3);
	}

	Vector3 b = barycenter_3(n_vertices, poly);
	Vector3 pi;
	Vector3 pip1;

	Scalar result = 0;
	for(SizeType i = 0; i < n_vertices; ++i) {
		const SizeType ip1 = (i + 1 == n_vertices)? 0 : (i + 1);

		const SizeType i_3 	 = i   * 3;
		const SizeType ip1_3 = ip1 * 3;

		pi.x   = poly[i_3];
		pi.y   = poly[i_3 + 1];
		pi.z   = poly[i_3 + 2];

		pip1.x = poly[ip1_3];
		pip1.y = poly[ip1_3 + 1];
		pip1.z = poly[ip1_3 + 2];

		result += trapezoid_area_3(b, pi, pip1);
	}

	return 0.5*result;
}

#endif //VOLUME_CL

