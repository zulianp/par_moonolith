#ifndef MOONOLITH_NTREE_WITH_TAGS_MUTATOR_FACTORY_H
#define MOONOLITH_NTREE_WITH_TAGS_MUTATOR_FACTORY_H 

#include "moonolith_mutator_factory.hpp"
#include "moonolith_n_tree_can_refine.hpp"
#include "moonolith_n_tree_refine.hpp"
#include "moonolith_n_tree_bound_expand.hpp"
#include "moonolith_n_tree_with_tags_bound_expand.hpp"
#include "moonolith_n_tree_with_tags_insert.hpp"
#include "moonolith_n_tree_with_tags_match.hpp"

#include "moonolith_n_tree_with_tags_bound_synchronize.hpp"
#include "moonolith_n_tree_hash.hpp"
#include "moonolith_n_tree_with_tags_refine_all.hpp"

#include <memory>

namespace moonolith {

	template<class Tree>
	class NTreeWithTagsMutatorFactory : public MutatorFactory<typename Tree::Traits> {
	public:
		
		typedef typename Tree::Traits Traits;
		typedef typename Traits::DataType DataType;
		typedef typename Traits::Bound Bound;
		typedef moonolith::Node<Traits> Node;
		typedef moonolith::Branch<Traits> Branch;
		typedef moonolith::Leaf<Traits> Leaf;
		typedef moonolith::Root<Traits> Root;
		typedef moonolith::TreeMemory<Traits> TreeMemory;

		void set_refine_params(const Integer max_leaf_size, const Integer max_depth)
		{
			max_leaf_size_ = max_leaf_size;
			max_depth_ = max_depth;
		}

		virtual std::shared_ptr< Mutator<Traits> > new_can_refine() const override
		{
			return  std::make_shared< NTreeCanRefine<Tree> >(max_leaf_size_, max_depth_);
		}

		virtual std::shared_ptr< Mutator<Traits> >  new_insert(const DataHandle &handle, const DataType &data) const override
		{
			return std::make_shared<  NTreeWithTagsInsert<Tree> >(handle, data);
		}

		virtual std::shared_ptr< Mutator<Traits> > new_refine() const override
		{
			return std::make_shared< NTreeRefine<Tree> >();
		}

		virtual std::shared_ptr< Mutator<Traits> > new_bound_expand(const DataType &data) const override
		{
			return std::make_shared< NTreeWithTagsBoundExpand<Tree> >(data);
		}

		virtual std::shared_ptr< Mutator<Traits> > new_bound_expand(const Bound &bound) const override
		{
			return std::make_shared< NTreeWithTagsBoundExpand<Tree> >(bound);
		}

		virtual std::shared_ptr< Mutator<Traits> > new_match(const DataType &data) const override
		{
			return std::make_shared< NTreeWithTagsMatch<Tree> >(data, predicate_);
		}

		virtual std::shared_ptr< NodeNavigator<Traits> > new_node_navigator() const override
		{
			return std::make_shared< BreadthFirstNavigator<Tree> >();
		}

		virtual std::shared_ptr< RemoteNode<Node> > new_remote_node() const override
		{
			return std::make_shared< RemoteNodeWithTags<Node> >(predicate_);
		}

		virtual std::shared_ptr< BoundSynchronize<Traits> > new_bound_synchronize() const override
		{
			return std::make_shared< NTreeWithTagsBoundSynchronize<Tree> >(predicate_);
		}

		virtual std::shared_ptr< Hash<Traits> > new_hash() const override
		{
			return std::make_shared< NTreeHash<Tree> >();
		}

		virtual std::shared_ptr< Mutator<Traits> > new_refine_all() const override
		{
			return std::make_shared< NTreeWithTagsRefineAll<Tree> >(predicate_);
		}

		NTreeWithTagsMutatorFactory(const std::shared_ptr<Predicate> &predicate)
		: max_leaf_size_(DEFAULT_REFINE_MAX_ELEMENTS), max_depth_(DEFAULT_REFINE_DEPTH), predicate_(predicate)
		{}

	private:
		Integer max_leaf_size_, max_depth_;
		std::shared_ptr<Predicate> predicate_;
	};

}

#endif //CUTLIBPP_MUTATOR_FACTORY_H
