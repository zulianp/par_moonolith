#ifndef MOONOLITH_EXPANDING_ARRAY_HPP
#define MOONOLITH_EXPANDING_ARRAY_HPP

#include <array>
#include <cassert>
#include <ostream>
#include <vector>

#include "moonolith_base.hpp"

namespace moonolith {

    template <typename T>
    class ExpandingArray {
    public:
        using Ref = typename std::vector<T>::reference;
        using ConstRef = typename std::vector<T>::const_reference;

        ExpandingArray(const std::size_t n) : data_(n), size_(n) {}
        ExpandingArray(std::initializer_list<T> in) : data_(in), size_(data_.size()) {}

        ExpandingArray() : size_(0) {}

        inline Ref operator[](const std::size_t i) {
            assert(i < size_);
            return data_[i];
        }

        inline ConstRef operator[](const std::size_t i) const {
            assert(i < size_);
            return data_[i];
        }

        inline Ref at(const std::size_t i) {
            assert(i < size_);
            return data_[i];
        }

        inline ConstRef at(const std::size_t i) const {
            assert(i < size_);
            return data_[i];
        }

        inline void resize(const std::size_t n) {
            if (n > data_.size()) {
                data_.resize(n);
            }

            size_ = n;
        }

        inline ExpandingArray &operator=(const ExpandingArray &other) {
            if (this == &other) return *this;

            size_ = other.size_;
            if (data_.size() < size_) {
                data_.resize(size_);
            }

            std::copy(std::begin(other), std::end(other), std::begin(*this));
            return *this;
        }

        inline void clear() { size_ = 0; }

        inline std::size_t size() const { return size_; }

        inline void push_back(const T &obj) {
            auto n = size_ + 1;
            if (data_.size() < n) {
                data_.resize(n);
            }

            data_[size_] = obj;
            size_ = n;
        }

        inline Ref back() { return data_[size_ - 1]; }

        inline ConstRef back() const { return data_[size_ - 1]; }

        inline auto begin() -> typename std::vector<T>::iterator { return data_.begin(); }

        inline auto begin() const -> typename std::vector<T>::const_iterator { return data_.begin(); }

        inline auto end() -> typename std::vector<T>::iterator { return data_.begin() + size_; }

        inline auto end() const -> typename std::vector<T>::const_iterator { return data_.begin() + size_; }

        inline auto erase(typename std::vector<T>::iterator position) -> typename std::vector<T>::iterator {
            size_ -= 1;
            return data_.erase(position);
        }

        // template<class... Args>
        inline void emplace_back() {
            auto n = size_ + 1;
            if (data_.size() < n) {
                data_.resize(n);
            }

            size_ = n;
        }

        inline bool empty() const { return size_ == 0; }

        inline void reserve(const std::size_t n) {
            if (data_.capacity() < n) {
                data_.reserve(n);
            }
        }

    private:
        std::vector<T> data_;
        std::size_t size_;
    };

    template <typename T>
    using Storage = ExpandingArray<T>;
    // using Storage = std::vector<T>;

    template <typename T, int N>
    class StorageTraits {
    public:
        using Type = std::array<T, N>;
    };

    template <typename T>
    class StorageTraits<T, -1> {
    public:
        using Type = moonolith::Storage<T>;
    };

    template <class S>
    class Resize {
    public:
        inline static void apply(S &s, const std::size_t n) { s.resize(n); }
    };

    template <typename T, std::size_t N>
    class Resize<std::array<T, N> > {
    public:
        inline static void apply(const std::array<T, N> &, const std::size_t n) {
            assert(N == n);
            MOONOLITH_UNUSED(n);
        }
    };

    template <typename T>
    void print(const Storage<T> &s, std::ostream &os) {
        for (const auto &e : s) {
            os << e << "\n";
        }
    }

    template <class V>
    inline void resize(V &v, const std::size_t n) {
        Resize<V>::apply(v, n);
    }
}  // namespace moonolith

#endif  // MOONOLITH_EXPANDING_ARRAY_HPP
