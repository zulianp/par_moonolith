#ifndef MORTAR_CL
#define MORTAR_CL 

#include "mortar_interfaces.cl"
#include "volume_interface.cl"
#include "make_quadrature_from_poly_interface.cl"
#include "intersect_and_mortar_assemble.cl"

m_kernel__ void assemble_coupling_operator_with_master_and_slave_meshes(		
							//input
							//A) basic data
							const SizeType n_dims,							//0)
							const SizeType n_codims,						//1)

							const SizeType n_elements_master,				//2)
							const m_global__ SizeType * element_order_master, //3) (n_elements)

							//B) Mesh
							m_global__ SizeType * el_ptr_master, 				//4) //n_elements+1
						    m_global__ SizeType * el_index_master, 			//5)
						    m_global__ SizeType * el_type_master,				//6)

						    //C) point data
						    SizeType n_nodes_master,						//7)
						    m_global__ Scalar	* points_master,				//8) N_DIMS x n_nodes

							const SizeType n_elements_slave,				//9)
							const m_global__ SizeType * element_order_slave, 		//10) (n_elements)

							//B) Mesh
							m_global__ SizeType * el_ptr_slave, 				//11) //n_elements+1
						    m_global__ SizeType * el_index_slave, 			//12)
						    m_global__ SizeType * el_type_slave,				//13)

						    //C) point data
						    SizeType n_nodes_slave,							//14)
						    m_global__ Scalar	* points_slave,					//15) N_DIMS x n_nodes

							const SizeType n_pairs,							//16)
							const m_global__ SizeType * pairs,				//17) //n_pairs*2 [layout: (master, slave)]
							const m_global__ SizeType * element_matrix_offsets,
							m_global__ Scalar * element_matrices,				//18) //for each n_pairs k add (n_shape_functions_slave_k *  n_shape_functions_master_k) * n_codims ^ 2
							m_global__ SizeType * pairs_intersect				//19) //n_pairs
)
{
	Mesh mesh_master, mesh_slave;

	mesh_master.n_dims 		= n_dims;
	mesh_master.n_elements 	= n_elements_master;
	mesh_master.el_ptr 		= el_ptr_master;
	mesh_master.el_index 	= el_index_master;
	mesh_master.el_type 	= el_type_master;
	mesh_master.n_nodes 	= n_nodes_master;
	mesh_master.points 		= points_master;

	mesh_slave.n_dims 		= n_dims;
	mesh_slave.n_elements 	= n_elements_slave;
	mesh_slave.el_ptr 		= el_ptr_slave;
	mesh_slave.el_index 	= el_index_slave;
	mesh_slave.el_type 		= el_type_slave;
	mesh_slave.n_nodes 		= n_nodes_slave;
	mesh_slave.points 		= points_slave;
	
	//mortar memory space
	MortarWorkspace w;

	for(SizeType i = get_global_id(0); i < n_pairs; i+= get_global_size(0)) {
		const SizeType i2 = i*2;
		const SizeType element_master = pairs[i2];
		const SizeType element_slave  = pairs[i2+1];

		KERNEL_ASSERT(element_slave  < mesh_slave.n_elements,  "element_slave  < mesh_slave.n_elements");
		KERNEL_ASSERT(element_master < mesh_master.n_elements, "element_master < mesh_master.n_elements");

		const SizeType order_master = element_order_master[element_master];
		const SizeType order_slave  = element_order_slave [element_slave];

		const SizeType element_matrix_offset = element_matrix_offsets[i];
		
		const SizeType n_entries = intersect_and_mortar_assemble(
			mesh_master, element_master, order_master, n_codims,
			mesh_slave,  element_slave,  order_slave,  n_codims,
			&w);

		if(n_entries) {
			pairs_intersect[i] = true;
			generic_copy(n_entries, w.local_matrix, &element_matrices[element_matrix_offset]);
		} else {
			pairs_intersect[i] = false;
		}
	}
}

#endif //MORTAR_CL
