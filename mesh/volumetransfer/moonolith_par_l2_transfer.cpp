#include "moonolith_par_l2_transfer_impl.hpp"

namespace moonolith {
    template class ParL2Transfer<Real, 1, 1, 1>;
    template class ParL2Transfer<Real, 2, 2, 2>;
    template class ParL2Transfer<Real, 3, 3, 3>;

    template class ParL2Transfer<Real, 2, 1, 1>;
    template class ParL2Transfer<Real, 2, 2, 1>;

    template class ParL2Transfer<Real, 3, 2, 2>;
    template class ParL2Transfer<Real, 3, 3, 2>;
}  // namespace moonolith
