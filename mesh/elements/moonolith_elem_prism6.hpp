#ifndef MOONOLITH_ELEM_PRISM_6_HPP
#define MOONOLITH_ELEM_PRISM_6_HPP

#include "moonolith_convex_decomposition.hpp"
#include "moonolith_elem_prism.hpp"

namespace moonolith {

    template <typename T_, int PhysicalDim_ = 3>
    class Prism6 final : public Prism<T_, 1, PhysicalDim_> {
    public:
        static const int Order = 1;
        static const int Dim = 3;
        static const int PhysicalDim = PhysicalDim_;
        static const int NNodes = 6;

        using T = T_;

        using Vector = moonolith::Vector<T, Dim>;
        using CoVector = moonolith::Vector<T, PhysicalDim>;

        using Point = moonolith::Vector<T, Dim>;
        using CoPoint = moonolith::Vector<T, PhysicalDim>;

        using Super = moonolith::Prism<T_, 1, PhysicalDim_>;
        using Super::make;

    private:
        inline static T f0(const T &x) noexcept { return (1.0 - x); }

        inline static T f1(const T &x) noexcept { return x; }

        inline static T d0(const T &) noexcept { return -1.0; }

        inline static T d1(const T &) noexcept { return 1.0; }

        inline static T t0(const Point &p) noexcept { return (1.0 - p.x - p.y); }

        inline static T t1(const Point &p) noexcept { return p.x; }

        inline static T t2(const Point &p) noexcept { return p.y; }

        inline static void dt0(const Point &, Vector &g) noexcept {
            g.x = -1.0;
            g.y = -1.0;
        }

        inline static void dt1(const Point &, Vector &g) noexcept {
            g.x = 1.0;
            g.y = 0.0;
        }

        inline static void dt2(const Point &, Vector &g) noexcept {
            g.x = 0.0;
            g.y = 1.0;
        }

        class Fun final {
        public:
            Fun() {
                f[0] = [](const Point &p) -> T { return f0(p.z) * t0(p); };

                f[1] = [](const Point &p) -> T { return f0(p.z) * t1(p); };

                f[2] = [](const Point &p) -> T { return f0(p.z) * t2(p); };

                f[3] = [](const Point &p) -> T { return f1(p.z) * t0(p); };

                f[4] = [](const Point &p) -> T { return f1(p.z) * t1(p); };

                f[5] = [](const Point &p) -> T { return f1(p.z) * t2(p); };
            }

            std::array<std::function<T(const Point &)>, NNodes> f;
        };

        class Grad final {
        public:
            Grad() {
                // f0(p.z) * t0(p);
                f[0] = [](const Point &p, Vector &g) {
                    auto f_eval = f0(p.z);
                    dt0(p, g);

                    g.x *= f_eval;
                    g.y *= f_eval;
                    g.z = d0(p.z) * t0(p);
                };

                // f0(p.z) * t1(p);
                f[1] = [](const Point &p, Vector &g) {
                    auto f_eval = f0(p.z);
                    dt1(p, g);

                    g.x *= f_eval;
                    g.y *= f_eval;
                    g.z = d0(p.z) * t1(p);
                };

                // f0(p.z) * t2(p);
                f[2] = [](const Point &p, Vector &g) {
                    auto f_eval = f0(p.z);
                    dt2(p, g);

                    g.x *= f_eval;
                    g.y *= f_eval;
                    g.z = d0(p.z) * t2(p);
                };

                // f1(p.z) * t0(p);
                f[3] = [](const Point &p, Vector &g) {
                    auto f_eval = f1(p.z);
                    dt0(p, g);

                    g.x *= f_eval;
                    g.y *= f_eval;
                    g.z = d1(p.z) * t0(p);
                };

                // f1(p.z) * t1(p);
                f[4] = [](const Point &p, Vector &g) {
                    auto f_eval = f1(p.z);
                    dt1(p, g);

                    g.x *= f_eval;
                    g.y *= f_eval;
                    g.z = d1(p.z) * t1(p);
                };

                // f1(p.z) * t2(p);
                f[5] = [](const Point &p, Vector &g) {
                    auto f_eval = f1(p.z);
                    dt2(p, g);

                    g.x *= f_eval;
                    g.y *= f_eval;
                    g.z = d1(p.z) * t2(p);
                };
            }

            std::array<std::function<void(const Point &, Vector &)>, NNodes> f;
        };

    public:
        T fun(const Integer i, const Point &p) const override { return fun_.f[i](p); }

        void grad(const Integer i, const Point &p, Vector &g) const override { grad_.f[i](p, g); }

        void hessian(const Integer, const Point &, std::array<T, Dim * Dim> &H) {
            std::fill(std::begin(H), std::end(H), 0.0);
        }

        void jacobian(const Point &p, std::array<T, PhysicalDim * Dim> &J) const override {
            if (is_affine()) {
                affine_approx_jacobian(J);
            } else {
                IsoParametric<Prism6>::jacobian(*this, p, J);
            }
        }

        void affine_approx_jacobian(std::array<T, PhysicalDim * Dim> &J) const override {
            Affine<Prism6>::jacobian(*this, {0, 1, 2, 3}, J);
        }

        CoPoint &node(const Integer i) override { return nodes_[i]; }

        const CoPoint &node(const Integer i) const override { return nodes_[i]; }

        void point(const Point &p, CoPoint &q) const override { IsoParametric<Prism6>::point(*this, p, q); }

        Prism6() : is_affine_(false) {}

        bool is_affine() const override { return is_affine_; }

        inline int n_nodes() const override { return NNodes; }
        inline int order() const override { return Order; }

        const std::array<CoPoint, NNodes> &nodes() const { return nodes_; }

        inline void set_affine(const bool val) { is_affine_ = val; }

        inline void make_reference() {
            Reference<Prism6>::points(nodes_);
            set_affine(true);
        }

        inline ElemType type() const override { return PRISM6; }

        inline bool is_simplex() const override { return false; }

        inline T approx_measure() const override { return measure(); }

        inline T measure() const override {
            if (is_affine()) {
                return 0.5 * trapezoid_area(nodes_[0], nodes_[1], nodes_[2]) * distance(nodes_[0], nodes_[3]);
                ;
            } else {
                Quadrature<T, Dim> q;
                Gauss::Prism::get(1, q);
                return IsoParametric<Prism6>::measure(*this, q);
            }
        }

        inline T reference_measure() const override { return 0.5; }

        inline void make(Polyhedron<T> &poly) const override {
            assert(is_affine());
            ConvexDecomposition<T, PhysicalDim>::make_prism(nodes(), poly);
        }

    private:
        bool is_affine_;
        std::array<CoPoint, NNodes> nodes_;
        const Fun fun_;
        const Grad grad_;
    };

    template <typename T, int PhysicalDim>
    class Reference<Prism6<T, PhysicalDim>> {
    public:
        using Point = typename Prism6<T, PhysicalDim>::Point;
        static const int NNodes = 6;

        template <std::size_t Size>
        static void points(std::array<Point, Size> &nodes) {
            static_assert(Size >= NNodes, "size must be at least NNodes");
            ///////////////////////
            // Lower nodes

            // p0
            nodes[0].x = 0.0;
            nodes[0].y = 0.0;
            nodes[0].z = 0.0;

            // p1
            nodes[1].x = 1.0;
            nodes[1].y = 0.0;
            nodes[1].z = 0.0;

            // p2
            nodes[2].x = 0.0;
            nodes[2].y = 1.0;
            nodes[2].z = 0.0;

            ///////////////////////

            // Upper nodes

            // p3
            nodes[3].x = 0.0;
            nodes[3].y = 0.0;
            nodes[3].z = 1.0;

            // p4
            nodes[4].x = 1.0;
            nodes[4].y = 0.0;
            nodes[4].z = 1.0;

            // p5
            nodes[5].x = 0.0;
            nodes[5].y = 1.0;
            nodes[5].z = 1.0;
        }

        static T measure() { return 0.5; }
    };

    template <typename T, int PhysicalDim>
    class GaussQRule<Prism6<T, PhysicalDim>> {
    public:
        static bool get(const Integer order, Quadrature3<T> &q) { return Gauss::Prism::get(order, q); }
    };

}  // namespace moonolith

#endif  // MOONOLITH_ELEM_PRISM_6_HPP
