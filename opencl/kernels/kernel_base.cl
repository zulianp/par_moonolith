#ifndef KERNEL_BASE_CL
#define KERNEL_BASE_CL

#ifndef CLIPP_HOST_CL

// Needed for when the kernels are used in host mode without opencl compiler
#ifndef m_kernel__
#define m_kernel__ __kernel
#define m_global__ __global
#define m_local__ __local
#define m_constant__ __constant
#else
#error "should not happen"
#endif

#define static_assert(x, msg) \
    switch (0) {              \
        case 0:               \
        case x:;              \
    }

#ifdef MOONOLITH_HAVE_SINGLE_PRECISION
typedef float Scalar;

typedef float2 Vector2;
typedef float3 Vector3;

typedef float4 Matrix2x2;
typedef float16 Matrix3x3;

#ifndef DEFAULT_TOLLERANCE
#define DEFAULT_TOLLERANCE 1e-6f
#endif  // DEFAULT_TOLLERANCE

#else

#ifdef MOONOLITH_HAVE_DOUBLE_PRECISION
#pragma OPENCL EXTENSION cl_khr_fp64 : enable

typedef double Scalar;

typedef double2 Vector2;
typedef double3 Vector3;

typedef double4 Matrix2x2;
typedef double16 Matrix3x3;

#ifndef DEFAULT_TOLLERANCE
#define DEFAULT_TOLLERANCE 1e-10
#endif  // DEFAULT_TOLLERANCE

#else  // ERROR
#error \
    "Scalar must be defined. (either float or double by -DMOONOLITH_HAVE_SINGLE_PRECISION or -DMOONOLITH_HAVE_DOUBLE_PRECISION)"
#endif  // MOONOLITH_HAVE_DOUBLE_PRECISION
#endif  // MOONOLITH_HAVE_SINGLE_PRECISION

#endif  // CLIPP_HOST_CL

#ifndef MAX_SHAPE_FUNCS
#define MAX_SHAPE_FUNCS 8
#endif

// #ifndef MAX_QUAD_POINTS
// #define MAX_QUAD_POINTS 64
// #define MAX_QUAD_POINTS 600
#define MAX_QUAD_POINTS 1430
// #endif //MAX_QUAD_POINTS

#ifndef MAX_N_DIMS
#define MAX_N_DIMS 3
#endif  // MAX_N_DIMS

#ifndef MAX_CODIM
#define MAX_CODIM 1
#endif

#ifndef CLIPP_HOST_CL

#define vec_2(macro_x, macro_y) (Vector2)(macro_x, macro_y)
#define vec_3(macro_x, macro_y, macro_z) (Vector3)(macro_x, macro_y, macro_z)

#ifndef SizeType
#define SizeType int
#endif

#endif  // CLIPP_HOST_CL

#if CoDim > 1
#define IS_VECTORIAL 1
#endif

// macros for handling different memory address spaces generically
#define generic_copy(n, src, dest)                                         \
    {                                                                      \
        for (SizeType macro_index = 0; macro_index < (n); ++macro_index) { \
            (dest)[macro_index] = (src)[macro_index];                      \
        }                                                                  \
    }
#define generic_set(n, value, values)                                      \
    {                                                                      \
        for (SizeType macro_index = 0; macro_index < (n); ++macro_index) { \
            (values)[macro_index] = (value);                               \
        }                                                                  \
    }
#define generic_ptr_swap(T, left, right) \
    {                                    \
        T* temp = left;                  \
        left = right;                    \
        right = temp;                    \
    }
#define generic_swap(T, left, right) \
    {                                \
        T temp = left;               \
        left = right;                \
        right = temp;                \
    }

#define INVALID_INDEX -1
#define LARGE_INTEGER 1000

// Set DEBUG_KERNEL for debugging
//#define DEBUG_KERNEL
#ifdef DEBUG_KERNEL
#define REPORT_OUT_OF_RANGE_ERROR(msg, value, lower, upper) \
    printf("[Error] %s %d not in range (%d, %d)\n", msg, value, lower, upper)
#define REPORT_ERROR(x) printf("[Error] %s", x)

#ifndef CLIPP_HOST_CL
#define KERNEL_ASSERT(x, msg) \
    if (!(x)) printf("[Error] %s\n", (msg))
#else  // CLIPP_HOST_CL
#define KERNEL_ASSERT(expr, msg)                                                          \
    {                                                                                     \
        if (!(expr)) assertion_failure(__STRING(expr), __FILE__, __LINE__, __FUNCTION__); \
    }
#endif  // CLIPP_HOST_CL

#define FEOBJECT_DEBUG(fe_ptr) init_invalid(fe_ptr)
#define KERNEL_DEBUG(x) x

#else  // DEBUG_KERNEL
#define REPORT_OUT_OF_RANGE_ERROR(msg, value, lower, upper)
#define REPORT_ERROR(x)
#define KERNEL_ASSERT(x, msg)
#define FEOBJECT_DEBUG(fe_ptr)
#define KERNEL_DEBUG(x)
#endif  // DEBUG_KERNEL

#endif  // KERNEL_BASE_CL
