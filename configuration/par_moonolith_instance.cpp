#include "par_moonolith_instance.hpp"

#include "par_moonolith_config.hpp"

#include "moonolith_communicator.hpp"
#include "moonolith_make_unique.hpp"
#include "moonolith_stream_utils.hpp"

#include "moonolith_version.hpp"

#include <mpi.h>
#include <string>

namespace moonolith {

    bool Moonolith::verbose() const { return verbose_; }

    void Moonolith::verbose(const bool val) { verbose_ = val; }

    const char *Moonolith::version() { return MOONOLITH_VERSION; }

    void Moonolith::Init(int argc, char *argv[], MPI_Comm comm) {
        instance().world_communicator_ = nullptr;
        instance().world_communicator_ = std::make_shared<Communicator>(Communicator(comm, false).duplicate());

        const std::string version_opt = "--version";

        // App options
        for (Integer i = 1; i < argc; ++i) {
            if (version_opt == argv[i]) {
                logger() << "Moonolith version: " << version() << std::endl;
            }
        }

        // Library options
        const std::string log_file_opt = "--use_log_file";
        const std::string mute_opt = "--mute";
        const std::string verbose_opt = "--verbose";

        for (Integer i = 1; i < argc; ++i) {
            if (log_file_opt == argv[i]) {
                Logger::instance().set_use_file_stream("log_" + std::to_string(Communicator().rank()) + ".txt");
            } else if (mute_opt == argv[i]) {
                instance().verbose(false);
            } else if (verbose_opt == argv[i]) {
                instance().verbose(true);
            }
        }
    }

    void Moonolith::Init(int argc, char *argv[]) {
        MPI_Init(&argc, &argv);

        Init(argc, argv, MPI_COMM_WORLD);

        instance().owned_comm_ = true;
    }

    std::shared_ptr<Communicator> Moonolith::world_communicator() { return instance().world_communicator_; }

    Moonolith &Moonolith::instance() {
        static Moonolith instance_;
        return instance_;
    }

    Moonolith::~Moonolith() {}

    Moonolith::Moonolith()
        // For retro compatibility if client code is not using Moonolith::Init and Moonolith::Finalize
        : world_communicator_(std::make_shared<Communicator>(MPI_COMM_WORLD, false)), verbose_(false) {}

    int Moonolith::Finalize() {
        instance().world_communicator_ = nullptr;
        if (instance().owned_comm_) {
            return MPI_Finalize();
        } else {
            return 0;
        }
    }

    void Moonolith::Abort() {
        // FIXME
        MPI_Abort(world_communicator()->get(), -1);
    }

}  // namespace moonolith
