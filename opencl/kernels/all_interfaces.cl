
#include "algebra/algebra_interfaces.cl"
#include "assembly/assemble_interfaces.cl"
#include "elasticity/elasticity_interfaces.cl"
#include "fe/fe_interfaces.cl"
#include "mortar/mortar_interfaces.cl"
