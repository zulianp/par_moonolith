#ifndef MOONOLITH_TRAVERSAL_ALGORITHM_HPP
#define MOONOLITH_TRAVERSAL_ALGORITHM_HPP

#include "moonolith_forward_declarations.hpp"
#include "moonolith_navigator_option.hpp"
#include "moonolith_remote_node.hpp"
#include "moonolith_stream_utils.hpp"
#include "moonolith_traversal_listener.hpp"

#include <deque>
#include <list>
#include <memory>

namespace moonolith {

    template <class Tree>
    class TraversalAlgorithm : public Visitor<typename Tree::Traits> {
    public:
        typedef typename Tree::Traits Traits;
        typedef typename Traits::DataType DataType;
        typedef typename Traits::Bound Bound;
        typedef moonolith::Node<Traits> Node;
        typedef moonolith::Branch<Traits> Branch;
        typedef moonolith::Leaf<Traits> Leaf;
        typedef moonolith::Root<Traits> Root;
        typedef moonolith::TreeMemory<Traits> TreeMemory;
        typedef moonolith::RemoteNode<Node> RemoteNode;

    private:
        typedef typename std::deque<NodeHandle>::const_iterator NodeIter;
        typedef typename std::deque<std::shared_ptr<RemoteNode> >::const_iterator RemoteNodeIter;

    public:
        /////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////
        /////////////////////////////////////////////// User control methods
        //////////////////////////////////////////////////

        void add_listener(const std::shared_ptr<TraversalListener<Tree> > &listener) { listeners_.push_back(listener); }

        template <class Iterator>
        void add_listeners(const Iterator &begin, const Iterator &end) {
            listeners_.insert(listeners_.end(), begin, end);
        }

        void clear_listeners() { listeners_.clear(); }

        void fire_entry_added(Node &node, RemoteNode &remote_node) {
            for (auto &l : listeners_) {
                l->entry_added(tree_, node, remote_node, rank());
            }
        }

        void fire_traversal_terminated() {
            for (auto &l : listeners_) {
                l->traversal_terminated();
            }
        }

        inline std::shared_ptr<RemoteNode> new_remote_node() const {
            return tree_.memory().mutator_factory()->new_remote_node();
        }

        void read_meta(InputStream &) {}

        void write_meta(OutputStream &) const {}

        virtual bool terminated() const { return local_queue_.empty(); }

        /////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////// Basic control methods
        ////////////////////////////////////////////

        inline bool can_continue_search(const Node &node, const RemoteNode &remote_node) const {
            return tree_.can_refine_search_from(node.handle()) && remote_node.can_refine_search();
        }

        inline bool must_add_to_lookup_table(const Node &node, const RemoteNode &remote_node) const {
            return !(node.empty() || remote_node.empty());
        }

        inline bool not_related(const Node &node, const RemoteNode &remote_node) const {
            return (node.empty() || remote_node.empty());
        }

        /////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////// Algorithm methods /////////////////////////////////////////

        void read_next_level(InputStream &is) {
            CHECK_STREAM_READ_BEGIN("TraversalAlgorithm", is);

            read_meta(is);
            Integer n_nodes;
            is >> n_nodes;
            remote_queue_.clear();
            for (Integer i = 0; i < n_nodes; ++i) {
                std::shared_ptr<RemoteNode> node = new_remote_node();
                is >> *node;
                remote_queue_.push_back(node);
            }

            CHECK_STREAM_READ_END("TraversalAlgorithm", is);
        }

        NavigatorOption visit(Branch &branch, TreeMemory &memory) override {
            (void)memory;

            Integer nAdded = static_cast<Integer>(local_queue_.size());
            for (typename Branch::NodeIter nIt = branch.begin(); nIt != branch.end(); ++nIt) {
                local_queue_.push_back(*nIt);
                assert(nIt->id() < memory.n_nodes());
            }

            nAdded = static_cast<Integer>(local_queue_.size()) - nAdded;
            return CONTINUE;
        }

        NavigatorOption visit(Leaf &leaf, TreeMemory &memory) override {
            const NodeHandle handle = leaf.handle();
            if (tree_.refine(handle)) {
                assert(tree_.node(handle)->is_branch());
                return tree_.node(handle)->accept(*this, memory);
            }

            return SKIP_SUBTREE;
        }

        NavigatorOption visit(Root &root, TreeMemory &memory) override {
            return visit(static_cast<Branch &>(root), memory);
        }

        void process_level() {
            assert(local_queue_.empty() == remote_queue_.empty());

            if (local_queue_.empty() || remote_queue_.empty()) {
                return;
            }

            const Integer level = tree_.node(local_queue_.front())->level();

            MOONOLITH_DEBUG(Integer nVisited = 0);

            NodeIter localIt = local_queue_.begin();
            RemoteNodeIter remoteIt = remote_queue_.begin();

            std::list<NodeHandle> to_visit;

            for (; localIt != local_queue_.end() && remoteIt != remote_queue_.end(); ++localIt, ++remoteIt) {
                NodeHandle handle = *localIt;
                std::shared_ptr<Node> node = tree_.node(handle);
                std::shared_ptr<RemoteNode> remote_node = *remoteIt;

                assert(handle.id() == node->handle().id());
                assert(node->level() == level);
                MOONOLITH_DEBUG(++nVisited);

                if (not_related(*node, *remote_node)) {
                    // pruning path for node's subtree
                } else if (remote_node->match(*node)) {
                    // If local node and remote match we visit if it is a leaf
                    if (can_continue_search(*node, *remote_node)) {
                        to_visit.push_back(handle);
                        // is a branch or can be refined
                    } else if (must_add_to_lookup_table(*node, *remote_node)) {
                        // adding binding to look up table
                        lookup_table_->add_entry_for(handle, rank(), remote_node->handle());
                        fire_entry_added(*node, *remote_node);
                    }
                }
            }

            assert(nVisited == Integer(remote_queue_.size()));

            for (std::list<NodeHandle>::const_iterator it = to_visit.begin(); it != to_visit.end(); ++it) {
                NodeHandle handle = *it;
                std::shared_ptr<Node> node = tree_.node(handle);
                if (node->accept(*this, tree_.memory()) != CONTINUE) {
                    assert(false);
                }
            }

            // clean-up
            to_visit.clear();
            // remove the current level nodes
            remote_queue_.clear();

            while (!local_queue_.empty()) {
                NodeHandle handle = local_queue_.front();
                std::shared_ptr<Node> node = tree_.node(handle);

                if (node->level() != level) {
                    break;
                }

                local_queue_.pop_front();
            }

            if (terminated()) {
                fire_traversal_terminated();
            }
        }

        Integer count_nodes_in_local_queue_at_current_level() const {
            std::shared_ptr<Node> node = tree_.node(local_queue_.front());
            const Integer level = node->level();

            Integer n_nodes = 0;
            for (NodeIter it = local_queue_.begin(); it != local_queue_.end(); ++it) {
                node = tree_.node(*it);

                if (level != node->level()) {
                    break;
                }

                ++n_nodes;
            }

            return n_nodes;
        }

        void print_queue() {
            for (NodeIter it = local_queue_.begin(); it != local_queue_.end(); ++it) {
                moonolith::logger() << it->id() << " ";
            }
            moonolith::logger() << std::endl;
            moonolith::logger() << std::flush;
        }

        void write_next_level(OutputStream &os) const {
            CHECK_STREAM_WRITE_BEGIN("TraversalAlgorithm", os);

            write_meta(os);

            if (local_queue_.empty()) {
                logger() << "Local queue is empty" << std::endl;
                return;
            }

            const Integer n_nodes = count_nodes_in_local_queue_at_current_level();
            os << n_nodes;

            const Integer level = tree_.node(local_queue_.front())->level();
            for (NodeIter it = local_queue_.begin(); it != local_queue_.end(); ++it) {
                const NodeHandle handle = *it;
                const std::shared_ptr<Node> node = tree_.node(handle);

                if (level != node->level()) {
                    break;
                }

                std::shared_ptr<RemoteNode> remote_node = this->new_remote_node();
                remote_node->initialize(*node, tree_.can_refine_search_from(node->handle()));
                os << *remote_node;
            }

            CHECK_STREAM_WRITE_END("TraversalAlgorithm", os);
        }

        inline Integer rank() const { return rank_; }

        TraversalAlgorithm(Tree &tree, const Integer rank, const std::shared_ptr<LookUpTable> &lookup_table)
            : tree_(tree), rank_(rank), lookup_table_(lookup_table) {
            tree_.root_accept(*this);
        }

    private:
        Tree &tree_;
        Integer rank_;
        std::shared_ptr<LookUpTable> lookup_table_;

        std::deque<NodeHandle> local_queue_;
        std::deque<std::shared_ptr<RemoteNode> > remote_queue_;

        std::vector<std::shared_ptr<TraversalListener<Tree> > > listeners_;
    };

}  // namespace moonolith

#endif  // MOONOLITH_TRAVERSAL_ALGORITHM_HPP
