#ifndef MOONOLITH_ELEM_QUAD_9_HPP
#define MOONOLITH_ELEM_QUAD_9_HPP

#include "moonolith_elem_quad.hpp"
#include "moonolith_elem_quad8.hpp"

namespace moonolith {

    template <typename T_, int PhysicalDim_ = 2>
    class Quad9 final : public Quad<T_, 3, PhysicalDim_> {
    public:
        static const int Order = 2;
        static const int Dim = 2;
        static const int PhysicalDim = PhysicalDim_;
        static const int NNodes = 9;

        using T = T_;

        using Vector = moonolith::Vector<T, Dim>;
        using CoVector = moonolith::Vector<T, PhysicalDim>;

        using Point = moonolith::Vector<T, Dim>;
        using CoPoint = moonolith::Vector<T, PhysicalDim>;

        using Super = moonolith::Quad<T_, 3, PhysicalDim_>;
        using Polygon = typename Super::Polygon;

        // static_assert(Polygon::Point::size() == PhysicalDim_, "Polygon dim must be compatible with physical dim");

    private:
        inline static T f0(const T &x) { return 1.0 - 3.0 * x + 2.0 * (x * x); }

        inline static T f1(const T &x) { return -x + 2.0 * (x * x); }

        inline static T f2(const T &x) { return 4.0 * x - 4.0 * (x * x); }

        inline static T d0(const T &x) { return -3.0 + 4.0 * x; }

        inline static T d1(const T &x) { return -1.0 + 4.0 * x; }

        inline static T d2(const T &x) { return 4.0 - 8.0 * x; }

        class Fun final {
        public:
            Fun() {
                // corners
                f[0] = [](const Point &p) -> T { return f0(p.x) * f0(p.y); };

                f[1] = [](const Point &p) -> T { return f1(p.x) * f0(p.y); };

                f[2] = [](const Point &p) -> T { return f1(p.x) * f1(p.y); };

                f[3] = [](const Point &p) -> T { return f0(p.x) * f1(p.y); };

                // edges
                f[4] = [](const Point &p) -> T { return f2(p.x) * f0(p.y); };

                f[5] = [](const Point &p) -> T { return f1(p.x) * f2(p.y); };

                f[6] = [](const Point &p) -> T { return f2(p.x) * f1(p.y); };

                f[7] = [](const Point &p) -> T { return f0(p.x) * f2(p.y); };

                // center

                f[8] = [](const Point &p) -> T { return f2(p.x) * f2(p.y); };
            }

            std::array<std::function<T(const Point &)>, 9> f;
        };

        class Grad final {
        public:
            Grad() {
                // corners

                // f0(p.x) * f0(p.y);
                f[0] = [](const Point &p, Vector &g) {
                    g.x = d0(p.x) * f0(p.y);
                    g.y = f0(p.x) * d0(p.y);
                };

                // f1(p.x) * f0(p.y);
                f[1] = [](const Point &p, Vector &g) {
                    g.x = d1(p.x) * f0(p.y);
                    g.y = f1(p.x) * d0(p.y);
                };

                // f1(p.x) * f1(p.y);
                f[2] = [](const Point &p, Vector &g) {
                    g.x = d1(p.x) * f1(p.y);
                    g.y = f1(p.x) * d1(p.y);
                };

                // f0(p.x) * f1(p.y);
                f[3] = [](const Point &p, Vector &g) {
                    g.x = d0(p.x) * f1(p.y);
                    g.y = f0(p.x) * d1(p.y);
                };

                // edges
                // f2(p.x) * f0(p.y);
                f[4] = [](const Point &p, Vector &g) {
                    g.x = d2(p.x) * f0(p.y);
                    g.y = f2(p.x) * d0(p.y);
                };

                // f1(p.x) * f2(p.y);
                f[5] = [](const Point &p, Vector &g) {
                    g.x = d1(p.x) * f2(p.y);
                    g.y = f1(p.x) * d2(p.y);
                };

                // f2(p.x) * f1(p.y);
                f[6] = [](const Point &p, Vector &g) {
                    g.x = d2(p.x) * f1(p.y);
                    g.y = f2(p.x) * d1(p.y);
                };

                // f0(p.x) * f2(p.y);
                f[7] = [](const Point &p, Vector &g) {
                    g.x = d0(p.x) * f2(p.y);
                    g.y = f0(p.x) * d2(p.y);
                };

                // center
                // f2(p.x) * f2(p.y);
                f[8] = [](const Point &p, Vector &g) {
                    g.x = d2(p.x) * f2(p.y);
                    g.y = f2(p.x) * d2(p.y);
                };
            }

            std::array<std::function<void(const Point &, Vector &)>, 9> f;
        };

    public:
        T fun(const Integer i, const Point &p) const override { return fun_.f[i](p); }

        void grad(const Integer i, const Point &p, Vector &g) const override { grad_.f[i](p, g); }

        void hessian(const Integer, const Point &, std::array<T, Dim * Dim> &H) {
            // hessian_.f[i](p, H);
            std::fill(H.begin(), H.end(), static_cast<T>(0.0));
            assert(false);
        }

        void jacobian(const Point &p, std::array<T, PhysicalDim * Dim> &J) const override {
            if (is_affine()) {
                affine_approx_jacobian(J);
                return;
            }

            IsoParametric<Quad9>::jacobian(*this, p, J);
        }

        void affine_approx_jacobian(std::array<T, PhysicalDim * Dim> &J) const override {
            const auto &p0 = node(0);

            for (int i = 0; i < PhysicalDim; ++i) {
                const int i_offset = i * Dim;
                J[i_offset] = node(1)[i] - p0[i];
                J[i_offset + 1] = node(3)[i] - p0[i];
            }
        }

        CoPoint &node(const Integer i) override { return nodes_[i]; }

        const CoPoint &node(const Integer i) const override { return nodes_[i]; }

        void point(const Point &p, CoPoint &q) const override { IsoParametric<Quad9>::point(*this, p, q); }

        Quad9() : is_affine_(false) {}

        bool is_affine() const override { return is_affine_; }
        inline int order() const override { return Order; }

        inline void set_affine(const bool val) { is_affine_ = val; }

        inline int n_nodes() const override { return NNodes; }

        inline void make_reference() {
            Reference<Quad9>::points(nodes_);
            set_affine(true);
        }

        inline ElemType type() const override { return QUAD9; }

        inline bool is_simplex() const override { return false; }

        inline T measure() const override {
            if (is_affine()) {
                return trapezoid_area(nodes_[0], nodes_[1], nodes_[3]);
            } else {
                Quadrature<T, Dim> q;
                Gauss::Quad::get(2, q);
                return IsoParametric<Quad9>::measure(*this, q);
            }
        }

        inline T approx_measure() const override {
            if (is_affine()) {
                return trapezoid_area(nodes_[0], nodes_[1], nodes_[3]);
            } else {
                Polygon poly(false);
                make(poly);
                return poly.area();
            }
        }

        inline T reference_measure() const override { return 1.0; }

        inline void make(Polygon &poly) const override {
            if (is_affine()) {
                poly.resize(4);

                for (int i = 0; i < 4; ++i) {
                    poly[i] = node(i);
                }
            } else {
                poly.resize(8);

                for (int i = 0; i < 4; ++i) {
                    const auto i2 = i * 2;
                    poly[i2] = node(i);
                    poly[i2 + 1] = node(4 + i);
                }
            }
        }

        inline std::array<CoPoint, 9> &nodes() { return nodes_; }

        inline const std::array<CoPoint, 9> &nodes() const { return nodes_; }

    private:
        bool is_affine_;
        std::array<CoPoint, 9> nodes_;
        const Fun fun_;
        const Grad grad_;
        // const Hessian hessian_;
    };

    template <typename T, int PhysicalDim>
    class Reference<Quad9<T, PhysicalDim>> {
    public:
        // using Point = typename Quad9<T, 2, PhysicalDim>::Point;
        static const int NNodes = 9;

        template <class Point, std::size_t Size>
        static void points(std::array<Point, Size> &nodes) {
            static_assert(Size >= 9, "size must be at least 8");

            Reference<Quad8<T, PhysicalDim>>::points(nodes);

            // p8
            nodes[8].x = 0.5;
            nodes[8].y = 0.5;
        }

        inline constexpr static T measure() { return 1.0; }
    };

    template <typename T, int PhysicalDim>
    class GaussQRule<Quad9<T, PhysicalDim>> {
    public:
        static bool get(const Integer order, Quadrature2<T> &q) { return Gauss::Quad::get(order, q); }
    };

}  // namespace moonolith

#endif  // MOONOLITH_ELEM_QUAD_9_HPP
