#ifndef MOONOLITH_INTERSECT_POLYHEDRA_HPP
#define MOONOLITH_INTERSECT_POLYHEDRA_HPP

#include "moonolith_empirical_tol.hpp"
#include "moonolith_h_polytope.hpp"
#include "moonolith_halfspace.hpp"
#include "moonolith_intersect_halfspace.hpp"
#include "moonolith_intersect_plane.hpp"
#include "moonolith_intersect_polygons.hpp"
#include "moonolith_polyhedron.hpp"
#include "moonolith_remove_collinear_points.hpp"

#include <cassert>

namespace moonolith {

    template <typename T>
    class IntersectPolyhedronWithHalfSpace {
    public:
        using HalfSpace = moonolith::HalfSpace<T, 3>;
        using Polyhedron = moonolith::Polyhedron<T>;
        using Vector = typename Polyhedron::Vector;
        using Point = typename Polyhedron::Point;

        const static short NOT_INTERSECTED = 0;
        const static short INTERSECTED = 1;
        const static short CONTAINED_USE_ORIGINAL = 2;
        const static short ERROR_IN_ALGORITHM = -3;

        void clear();

        short apply(const HalfSpace &h,
                    const Polyhedron &in,
                    const Storage<Vector> &in_normals,
                    Polyhedron &out,
                    Storage<Vector> &out_normals,
                    const T tol = 1e-10);

        void fix_side_orientation(const int i, const Vector &n, Polyhedron &in_polyhedron);
        void fix_side(const int i,
                      const Vector &n,
                      const bool remove_collinear,
                      Polyhedron &in_polyhedron,
                      const T tol = 1e-10);

    private:
        Storage<T> pp_distance;
        Storage<short> node_query;
        Storage<short> element_query;
        Storage<Integer> map;
        Storage<Integer> cut_face;
        Storage<Integer> mutated_face;
        Storage<Integer> edge_map;
        SortPolygon<T, Integer> sort_polygon;
        Polygon<T, 3> polygon_buffer;

        RemoveCollinearPoints<T, 3> rcp;
        Storage<bool> is_collinear;
    };

    // template<typename T>
    // class IntersectPolygon3WithHPolyhedron {
    // public:
    // 	using Polygon3 = moonolith::Polygon<T, 3>;
    // 	using HPolyhedron = moonolith::HPolytope<T, 3>;
    // 	using Line3 = moonolith::Line<T, 3>;

    // private:
    // 	using ISectLineHS = moonolith::IntersectLineWithHalfSpace<T, 3>;

    // 	bool apply(const Polygon3 &polygon, const HPolyhedron &h_poly, Polygon3 &result, const T &tol = 1e-10);

    // private:
    // 	Line3 line, isect_line;
    // 	Polygon3 in;
    // 	ISectLineHS intersect_line_with_half_space;
    // };

    template <typename T>
    class IntersectConvexPolyhedronWithHPolyhedron {
    public:
        using Polyhedron = moonolith::Polyhedron<T>;
        using HPolyhedron = moonolith::HPolytope<T, 3>;
        using Vector = typename HPolyhedron::Vector;
        using IsectPolyWithHSpace = moonolith::IntersectPolyhedronWithHalfSpace<T>;

        bool apply(const HPolyhedron &h_poly, Polyhedron &in_polyhedron, const T &tol = 1e-10);

    private:
        IsectPolyWithHSpace intersect_poly_with_halfspace;

        // storage
        Polyhedron isect_polyhedron;
        Storage<Vector> isect_normals;
        Storage<Vector> poly_normals;
    };

    template <typename T>
    class IntersectConvexPolyhedra {
    public:
        using Polyhedron = moonolith::Polyhedron<T>;
        using HPolyhedron = moonolith::HPolytope<T, 3>;
        using Vector = typename HPolyhedron::Vector;

        using ISectCPWithHP = IntersectConvexPolyhedronWithHPolyhedron<T>;

        inline bool apply(const Polyhedron &p1,
                          const Polyhedron &p2,
                          Polyhedron &out,
                          const T tol = GeometricTol<T>::value()) {
            assert(!p1.empty() && !p2.empty());

            h_poly_1.make(p1, normal_buff_1);

            if (h_poly_1.all_outside(p2.points, 0.0)) {
                return false;
            }

            h_poly_2.make(p2, normal_buff_2);
            out = p1;
            return isect_cp_with_cp.apply(h_poly_2, out, tol);
        }

    private:
        ISectCPWithHP isect_cp_with_cp;
        HPolyhedron h_poly_1, h_poly_2;
        Storage<Vector> normal_buff_1, normal_buff_2;
    };

    template <typename T>
    inline short intersect_convex_polyhedra_with_halfspace(const Polyhedron<T> &in,
                                                           const HalfSpace<T, 3> &h,
                                                           Polyhedron<T> &out,
                                                           const T tol = GeometricTol<T>::value()) {
        Storage<Vector<T, 3>> in_normals, out_normals;
        in.normals(in_normals);
        return IntersectPolyhedronWithHalfSpace<T>().apply(h, in, in_normals, out, out_normals, tol);
    }

    template <typename T>
    inline bool intersect_convex_polyhedra(const Polyhedron<T> &poly1,
                                           const Polyhedron<T> &poly2,
                                           Polyhedron<T> &result) {
        return IntersectConvexPolyhedra<T>().apply(poly1, poly2, result);
    }

    template <typename T>
    class IntersectConvexSurfPolyhedra4 {
    public:
        using Polyhedron4 = moonolith::Polyhedron4<T>;
        using Polyhedron = moonolith::Polyhedron<T>;
        using HPolyhedron = moonolith::HPolytope<T, 4>;
        using Vector = typename HPolyhedron::Vector;
        using Point = typename HPolyhedron::Point;

        inline bool apply(const Polyhedron4 &poly1,
                          const Polyhedron4 &poly2,
                          Polyhedron4 &result,
                          const T tol = GeometricTol<T>::value()) {
            assert(!poly1.empty() && !poly2.empty());
            // only for tetrahedral faces on poly1
            assert(poly1.n_elements() == 4);
            assert(poly1.n_nodes() == 4);

            MOONOLITH_UNUSED(tol);

            const auto &p0 = poly1.points[0];
            const auto &p1 = poly1.points[1];
            const auto &p2 = poly1.points[2];
            const auto &p3 = poly1.points[3];

            // add code for checking normals

            if (!make(p0, p1, p2, p3, trafo)) {
                assert(false);
                std::cerr << "[Error] bad element" << std::endl;
                return false;
            }

            const auto n_points_1 = poly1.n_nodes();
            const auto n_points_2 = poly2.n_nodes();

            ref_poly_1.points.resize(n_points_1);
            ref_poly_2.points.resize(n_points_2);

            for (std::size_t i = 0; i < n_points_1; ++i) {
                trafo.apply_inverse(poly1.points[i], ref_poly_1.points[i]);
            }

            for (std::size_t i = 0; i < n_points_2; ++i) {
                trafo.apply_inverse(poly2.points[i], ref_poly_2.points[i]);
            }

            ref_poly_1.el_ptr = poly1.el_ptr;
            ref_poly_1.el_index = poly1.el_index;
            ref_poly_1.type = poly1.type;

            ref_poly_2.el_ptr = poly2.el_ptr;
            ref_poly_2.el_index = poly2.el_index;
            ref_poly_2.type = poly2.type;

            // check if poly2 is not on the same hyperplane
            if (moonolith::measure(ref_poly_2) == 0.) {
                return false;
            }

            ref_poly_1.fix_ordering();
            ref_poly_2.fix_ordering();

            if (!isect.apply(ref_poly_1, ref_poly_2, ref_isect)) {
                return false;
            }

            auto ref_measure = moonolith::measure(ref_isect);
            auto trafo_measure = moonolith::pseudo_det<T, 4, 3>(trafo.mat);

            isect_measure_ = std::abs(ref_measure) * std::abs(trafo_measure);

            const auto n_points_isect = ref_isect.n_nodes();
            result.points.resize(n_points_isect);

            for (std::size_t i = 0; i < n_points_isect; ++i) {
                trafo.apply(ref_isect.points[i], result.points[i]);
            }

            result.el_ptr = ref_isect.el_ptr;
            result.el_index = ref_isect.el_index;
            result.type = ref_isect.type;
            return true;
        }

        inline T isect_measure() const { return isect_measure_; }

    private:
        Point v;
        IntersectConvexPolyhedra<T> isect;
        AffineTransform<T, 3, 4> trafo;
        Polyhedron ref_poly_1, ref_poly_2, ref_isect;

        T isect_measure_;
    };

}  // namespace moonolith

#endif  // MOONOLITH_INTERSECT_POLYHEDRA_HPP
