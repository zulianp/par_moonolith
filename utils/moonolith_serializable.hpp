#ifndef MOONOLITH_SERIALIZABLE_HPP
#define MOONOLITH_SERIALIZABLE_HPP

#include "moonolith_forward_declarations.hpp"

#include <string>


namespace moonolith {
	class Serializable {
	public:
		virtual ~Serializable();
		
		virtual void apply_read_write(Stream &stream);
		virtual void write(OutputStream &stream) const;
		virtual void read(InputStream &stream);
		
		inline friend InputStream & operator >>(InputStream &is, Serializable &serializable)
		{
			serializable.read(is);
			return is;
		}
		
		inline friend OutputStream & operator <<(OutputStream &os, const Serializable &serializable)
		{
			serializable.write(os);
			return os;
		}
	};
}

#endif //MOONOLITH_SERIALIZABLE_HPP

