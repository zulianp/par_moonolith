#ifndef MOONOLITH_ELEM_EDGE_HPP
#define MOONOLITH_ELEM_EDGE_HPP

#include "moonolith_elem.hpp"
#include "moonolith_gauss_quadrature_rule.hpp"

namespace moonolith {

    template <typename T, int PhysicalDim>
    class LineElement : public Elem<T, 1, PhysicalDim> {
    public:
        virtual ~LineElement() {}
        virtual void make(Line<T, PhysicalDim> &line) const = 0;
        virtual void make(PolyLine<T, PhysicalDim> &line) const = 0;
    };

    template <typename T, int Order_, int PhysicalDim_ = 1>
    class Edge : public LineElement<T, PhysicalDim_> {
    public:
        static const int Order = Order_;
        virtual ~Edge() {}
    };

    template <class Elem>
    inline void make(const Elem &elem, Storage<typename Elem::CoPoint> &poly) {
        static_assert(Elem::Dim == 1, "only for line elements");
        static_assert(Elem::NNodes <= 3, "TODO missing cases");
        elem.make(poly);
    }

    template <typename T, int Order, int PhysicalDim>
    class GaussQRule<Edge<T, Order, PhysicalDim> > {
    public:
        static bool get(const Integer order, Quadrature1<T> &q) { return Gauss::get(order, q); }
    };

    template <typename T, int PhysicalDim>
    inline void make(const LineElement<T, PhysicalDim> &elem, Line<T, PhysicalDim> &poly) {
        elem.make(poly);
    }

    template <typename T, int Order, int PhysicalDim>
    inline void make(const Edge<T, Order, PhysicalDim> &elem, Line<T, PhysicalDim> &poly) {
        elem.make(poly);
    }

    template <typename T, int Order, int PhysicalDim>
    inline void make(const Edge<T, Order, PhysicalDim> &elem, PolyLine<T, PhysicalDim> &poly_line) {
        elem.make(poly_line);
    }

    template <typename T, int PhysicalDim>
    inline void make(const Elem<T, 1, PhysicalDim> &elem, PolyLine<T, PhysicalDim> &poly_line) {
        auto l = dynamic_cast<const LineElement<T, PhysicalDim> *>(&elem);
        if (!l) {
            assert(false);
            return;
        }

        l->make(poly_line);
    }

    template <typename T, int PhysicalDim>
    inline void make(const Elem<T, 1, PhysicalDim> &elem, Line<T, PhysicalDim> &line) {
        auto l = dynamic_cast<const LineElement<T, PhysicalDim> *>(&elem);
        if (!l) {
            assert(false);
            return;
        }

        l->make(line);
    }

}  // namespace moonolith

#endif  // MOONOLITH_ELEM_EDGE_HPP
