#ifndef MOONOLITH_FE_HPP
#define MOONOLITH_FE_HPP

#include "moonolith_elem.hpp"
#include "moonolith_elem_polymorphic.hpp"
#include "moonolith_matrix.hpp"
#include "moonolith_vector.hpp"
#include "par_moonolith_config.hpp"

#include <array>

namespace moonolith {

    template <typename T, int Dim_, int PhysicalDim_>
    class FEInterface {
    public:
        static const int Dim = Dim_;
        static const int PhysicalDim = PhysicalDim_;
        using uint = unsigned int;

        using Jacobian = moonolith::Matrix<T, PhysicalDim, Dim>;

        virtual ~FEInterface() {}

        // virtual void init(const Quadrature<T, Dim> &q)   = 0;
        virtual T fun(const Integer i, const Integer qp) = 0;
        virtual void grad(const Integer i, const Integer qp, Vector<T, Dim> &g) = 0;
        virtual void jacobian(const Integer qp, Jacobian &J) = 0;
        virtual T dx(const Integer qp) = 0;
        virtual uint n_quad_points() const = 0;
        virtual uint n_shape_functions() const = 0;

        virtual bool has_fun() const = 0;
        virtual bool has_grad() const = 0;
        virtual bool has_jacobian() const = 0;

        virtual FEType type() const = 0;
    };

    template <class Elem>
    class FE final : public FEInterface<typename Elem::T, Elem::Dim, Elem::PhysicalDim> {
    public:
        static const int Dim = Elem::Dim;
        static const int PhysicalDim = Elem::PhysicalDim;
        static const int NNodes = Elem::NNodes;

        using uint = unsigned int;

        using T = typename Elem::T;
        using Point = typename Elem::Point;
        using Vector = typename Elem::Vector;

        using CoPoint = typename Elem::CoPoint;
        using CoVector = typename Elem::CoVector;

        using Jacobian = moonolith::Matrix<T, PhysicalDim, Dim>;

        template <typename E>
        using Array = typename StorageTraits<E, NNodes>::Type;

        void init(Elem &e,
                  const Quadrature<T, Dim> &q,
                  const bool compute_fun,
                  const bool compute_grad,
                  const bool compute_jacobian) {
            clear();
            set_sizes(e.n_nodes());
            type_ = e.type();

            n_qp_ = q.n_points();
            assert(n_qp_ > 0);

            const int n_funs = n_shape_functions();
            assert(n_funs > 0);

            if (compute_fun) {
                has_fun_ = true;

                for (int i = 0; i < n_funs; ++i) {
                    fun_[i].resize(n_qp_);

                    for (uint k = 0; k < n_qp_; ++k) {
                        fun_[i][k] = e.fun(i, q.points[k]);
                    }
                }
            }

            if (compute_jacobian) {
                has_jacobian_ = true;

                jacobian_.resize(n_qp_);
                dx_.resize(n_qp_);

                if (e.is_affine()) {
                    e.affine_approx_jacobian(jacobian_[0].values);
                    auto det_JxM = moonolith::measure(jacobian_[0]) * e.reference_measure();

                    assert(det_JxM > 0.0);

                    for (uint k = 0; k < n_qp_; ++k) {
                        dx_[k] = det_JxM * q.weights[k];
                    }

                    for (uint k = 1; k < n_qp_; ++k) {
                        jacobian_[k] = jacobian_[0];
                    }

                } else {
                    const auto ref_M = e.reference_measure();

                    for (uint k = 0; k < n_qp_; ++k) {
                        e.jacobian(q.points[k], jacobian_[k].values);
                        auto det_JxM = ref_M * moonolith::measure(jacobian_[k]);

                        assert(det_JxM > 0.0);

                        dx_[k] = det_JxM * q.weights[k];
                    }
                }
            }

            if (compute_grad) {
                assert(false && "make global transformation using inv Jacobian");

                has_grad_ = true;

                for (int i = 0; i < NNodes; ++i) {
                    grad_[i].resize(n_qp_);

                    for (uint k = 0; k < n_qp_; ++k) {
                        e.grad(i, q.points[k], grad_[i][k]);
                    }
                }
            }
        }

        inline T fun(const Integer i, const Integer qp) override {
            assert(has_fun());
            return fun_[i][qp];
        }

        inline T dx(const Integer qp) override {
            assert(has_jacobian());
            return dx_[qp];
        }

        inline void grad(const Integer i, const Integer qp, Vector &g) override {
            assert(has_grad());
            g = grad_[i][qp];
        }

        inline void jacobian(const Integer qp, Jacobian &J) override {
            assert(has_jacobian());
            J = jacobian_[qp];
        }

        inline uint n_quad_points() const override { return n_qp_; }

        inline uint n_shape_functions() const override { return static_cast<uint>(fun_.size()); }

        inline bool has_fun() const override { return has_fun_; }

        inline bool has_grad() const override { return has_grad_; }

        inline bool has_jacobian() const override { return has_jacobian_; }

        inline FEType type() const override { return type_; }

        void clear() {
            n_qp_ = 0;
            has_fun_ = false;
            has_grad_ = false;
            has_jacobian_ = false;
        }

        FE() { clear(); }

    private:
        uint n_qp_;
        bool has_fun_, has_grad_, has_jacobian_;
        FEType type_;

        Array<Storage<T>> fun_;
        Array<Storage<Vector>> grad_;

        Storage<Jacobian> jacobian_;
        Storage<T> dx_;

        inline void set_sizes(const int n_funs) {
            Resize<Array<Storage<T>>>::apply(fun_, n_funs);
            Resize<Array<Storage<Vector>>>::apply(grad_, n_funs);
        }
    };

}  // namespace moonolith

#endif  // MOONOLITH_FE_HPP
