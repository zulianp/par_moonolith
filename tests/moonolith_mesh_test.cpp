
#include "moonolith_affine_transform.hpp"
#include "moonolith_contact.hpp"
#include "moonolith_dual_elem_checker.hpp"
#include "moonolith_elem_checker.hpp"
#include "moonolith_elem_hexahedron.hpp"
#include "moonolith_elem_quad.hpp"
#include "moonolith_elem_segment.hpp"
#include "moonolith_elem_shape.hpp"
#include "moonolith_elem_tetrahedron.hpp"
#include "moonolith_elem_triangle.hpp"
#include "moonolith_gauss_quadrature_rule.hpp"
#include "moonolith_keast_quadrature_rule.hpp"
#include "moonolith_l2_assembler.hpp"
#include "moonolith_par_contact.hpp"
#include "moonolith_polygon.hpp"
#include "moonolith_stream_utils.hpp"
#include "moonolith_triangulate.hpp"

#include "moonolith_lower_dim_l2_assembler.hpp"

#include "gtest/gtest.h"

#include <csignal>
#include <iostream>

using namespace moonolith;

TEST(MeshTest, test_affine_trafo_2) {
    using AT2 = moonolith::AffineTransform<Real, 2>;
    using Point2 = moonolith::Vector<Real, 2>;
    using Vector2 = moonolith::Vector<Real, 2>;

    AT2 t;
    make({0., 0.}, {1., 0.}, {0., 1.}, t);

    Point2 p_in(0.1, 0.1);
    Point2 p_out;
    Point2 p_actual;

    t.apply(p_in, p_out);
    t.apply_inverse(p_out, p_actual);

    // test identity
    ASSERT_TRUE(approxeq(p_in, p_out, GeometricTol<Real>::value()));
    ASSERT_TRUE(approxeq(p_in, p_actual, GeometricTol<Real>::value()));

    Point2 p1 = {0.1, 0.};
    Point2 p2 = {1.1, 0.};
    Point2 p3 = {1.1, 1.};

    make(p1, p2, p3, t);

    t.apply(p_in, p_out);
    t.apply_inverse(p_out, p_actual);

    Point2 p_trafo(0.3, 0.1);

    // test actual transformation
    ASSERT_TRUE(approxeq(p_trafo, p_out, GeometricTol<Real>::value()));
    ASSERT_TRUE(approxeq(p_in, p_actual, GeometricTol<Real>::value()));

    // test normal transformation
    Vector2 v = p3 - p2;
    Vector2 n(v.y, -v.x);
    n /= length(n);

    ASSERT_TRUE(approxeq(n, {1., 0.}, GeometricTol<Real>::value()));

    Vector2 n_ref;
    t.apply_inverse_to_direction(n, n_ref);
    n_ref /= length(n_ref);

    Vector2 expected(1., 1.);
    expected /= length(expected);

    ASSERT_TRUE(approxeq(n_ref, expected, GeometricTol<Real>::value()));
}

TEST(MeshTest, test_affine_trafo_3) {
    using AT3 = moonolith::AffineTransform<Real, 3>;
    using Point3 = moonolith::Vector<Real, 3>;
    using Vector3 = moonolith::Vector<Real, 3>;

    AT3 t;
    make({0., 0., 0.}, {1., 0., 0.}, {0., 1., 0.}, {0., 0., 1.}, t);

    Point3 p_in(0.1, 0.1, 0.1);
    Point3 p_out;
    Point3 p_actual;

    t.apply(p_in, p_out);
    t.apply_inverse(p_out, p_actual);

    // test identity
    ASSERT_TRUE(approxeq(p_in, p_out, GeometricTol<Real>::value()));
    ASSERT_TRUE(approxeq(p_in, p_actual, GeometricTol<Real>::value()));

    Point3 p1{0.1, 0., 0.}, p2{1.1, 0., 0.}, p3{0.1, 1., 0.}, p4{0.1, 0., 2.};
    make(p1, p2, p3, p4, t);

    t.apply(p_in, p_out);
    t.apply_inverse(p_out, p_actual);

    // test actual transformation
    ASSERT_TRUE(!approxeq(p_in, p_out, GeometricTol<Real>::value()));
    ASSERT_TRUE(approxeq(p_in, p_actual, GeometricTol<Real>::value()));

    // test normal transformation
    auto u = p3 - p2;
    auto v = p4 - p2;
    auto n = normalize(cross(u, v));

    Vector3 expected(1., 1., 1.);
    expected /= length(expected);

    Vector3 n_ref;
    t.apply_inverse_to_direction(n, n_ref);
    n_ref /= length(n_ref);

    ASSERT_TRUE(approxeq(n_ref, expected, GeometricTol<Real>::value()));
}

TEST(MeshTest, test_affine_trafo_43) {
    using AT43 = moonolith::AffineTransform<Real, 3, 4>;
    using Point3 = moonolith::Vector<Real, 3>;

    using Point4 = moonolith::Vector<Real, 4>;

    AT43 t;
    Point4 p1{0.1, 0., 0., 0.}, p2{1.1, 0., 0., 0.}, p3{0.1, 1., 0., 4.}, p4{0.1, 0., 2., 0.};
    make(p1, p2, p3, p4, t);

    Point3 p_in(0.1, 0.1, 0.1), p_actual;
    Point4 p_out;

    t.apply(p_in, p_out);
    t.apply_inverse(p_out, p_actual);

    // test actual transformation
    ASSERT_TRUE(approxeq(p_in, p_actual, GeometricTol<Real>::value()));
}

TEST(MeshTest, test_triangulate) {
    using Polygon2 = moonolith::Polygon<Real, 2>;

    Polygon2 p;
    // make non-convex quad
    make_quad({0., 0.}, {1., 0.}, {0.4, 0.4}, {0., 1.}, p, false);

    Storage<int> tri;
    triangulate_polygon(p, tri);

    ASSERT_TRUE(tri.size() == 6);

    ASSERT_TRUE(tri[0] == 0);
    ASSERT_TRUE(tri[1] == 1);
    ASSERT_TRUE(tri[2] == 2);

    ASSERT_TRUE(tri[3] == 0);
    ASSERT_TRUE(tri[4] == 2);
    ASSERT_TRUE(tri[5] == 3);
}

// First order
TEST(FETest, edge2) {
    bool ok = ElemChecker<Edge2<Real>>::check();
    ASSERT_TRUE(ok);
}

TEST(FETest, tri3) {
    bool ok = ElemChecker<Tri3<Real>>::check();
    ASSERT_TRUE(ok);
}

TEST(FETest, quad4) {
    bool ok = ElemChecker<Quad4<Real>>::check();
    ASSERT_TRUE(ok);
}

TEST(FETest, tet4) {
    bool ok = ElemChecker<Tet4<Real>>::check();
    ASSERT_TRUE(ok);
}

TEST(FETest, hex8) {
    bool ok = ElemChecker<Hex8<Real>>::check();
    ASSERT_TRUE(ok);
}

TEST(FETest, prism6) {
    bool ok = ElemChecker<Prism6<Real>>::check();
    ASSERT_TRUE(ok);
}

// Second order
TEST(FETest, edge3) {
    bool ok = ElemChecker<Edge3<Real>>::check();
    ASSERT_TRUE(ok);
}

TEST(FETest, tri6) {
    bool ok = ElemChecker<Tri6<Real>>::check();
    ASSERT_TRUE(ok);
}

TEST(FETest, quad8) {
    bool ok = ElemChecker<Quad8<Real>>::check();
    ASSERT_TRUE(ok);
}

TEST(FETest, quad9) {
    bool ok = ElemChecker<Quad9<Real>>::check();
    ASSERT_TRUE(ok);
}

TEST(FETest, tet10) {
    bool ok = ElemChecker<Tet10<Real>>::check();
    ASSERT_TRUE(ok);
}

TEST(FETest, hex27) {
    bool ok = ElemChecker<Hex27<Real>>::check();
    ASSERT_TRUE(ok);
}

// First order
TEST(DualFETest, edge2) {
    bool ok = DualElemChecker<Edge2<Real>>::check();
    ASSERT_TRUE(ok);
}

TEST(DualFETest, tri3) {
    bool ok = DualElemChecker<Tri3<Real>>::check();
    ASSERT_TRUE(ok);
}

TEST(DualFETest, quad4) {
    bool ok = DualElemChecker<Quad4<Real>>::check();
    ASSERT_TRUE(ok);
}

TEST(DualFETest, tet4) {
    bool ok = DualElemChecker<Tet4<Real>>::check();
    ASSERT_TRUE(ok);
}

TEST(DualFETest, hex8) {
    bool ok = DualElemChecker<Hex8<Real>>::check();
    ASSERT_TRUE(ok);
}

TEST(DualFETest, prism6) {
    bool ok = DualElemChecker<Prism6<Real>>::check();
    ASSERT_TRUE(ok);
}

// Second order
TEST(DualFETest, edge3) {
    bool ok = DualElemChecker<Edge3<Real>>::check();
    ASSERT_TRUE(ok);
}

TEST(DualFETest, tri6) {
    bool ok = DualElemChecker<Tri6<Real>>::check();
    ASSERT_TRUE(ok);
}

TEST(DualFETest, quad8) {
    bool ok = DualElemChecker<Quad8<Real>>::check();
    ASSERT_TRUE(ok);
}

TEST(DualFETest, quad9) {
    bool ok = DualElemChecker<Quad9<Real>>::check();
    ASSERT_TRUE(ok);
}

TEST(DualFETest, tet10) {
    bool ok = DualElemChecker<Tet10<Real>>::check();
    ASSERT_TRUE(ok);
}

TEST(DualFETest, hex27) {
    bool ok = DualElemChecker<Hex27<Real>>::check();
    ASSERT_TRUE(ok);
}

TEST(MeshTest, test_hex) {
    Hex8<Real> hex8;
    hex8.make_reference();
    hex8.node(7) *= 0.75;

    Polyhedron<Real> poly;
    Storage<Polyhedron<Real>> decomposed_poly;

    hex8.set_affine(true);
    hex8.make(poly);

    hex8.set_affine(false);
    hex8.make(decomposed_poly);

    MatlabScripter script;
    script.close_all();
    script.plot(poly, true);
    script.hold_on();

    const std::size_t n = decomposed_poly.size();

    for (std::size_t i = 0; i < n; ++i) {
        const auto &p = decomposed_poly[i];
        auto m = measure(p);
        ASSERT_TRUE(m > 0.);
        script.plot(p, false);
    }

    script.save("hex_decomp.m");
}

TEST(MeshTest, test_elem_shape_simple) {
    using Point = moonolith::Vector<Real, 2>;
    using CoPoint = moonolith::Vector<Real, 3>;
    using E = moonolith::Quad4<Real, 3>;

    Ray<Real, 3> ray;
    ray.o.x = 0.0;
    ray.o.y = 0.5;
    ray.dir.z = 1.;

    E e;

    std::array<Point, E::NNodes> ref_nodes;
    Reference<E>::points(ref_nodes);

    for (int i = 0; i < E::NNodes; ++i) {
        e.node(i).x = ref_nodes[i].x;
        e.node(i).y = ref_nodes[i].y;
    }

    e.node(0).z = 1.0;
    e.node(1).z = 3.0;
    e.node(2).z = 1.0;
    e.node(3).z = 3.0;

    // e.set_affine(true);
    ElemShape<E> shape(e, 1e-10, 100, 1e-6, false);

    Real t = 1.8;
    Point x;
    bool ok = shape.intersect(ray, t, x);
    ASSERT_TRUE(ok);
    CoPoint global_x;

    ray.make(t, global_x);
    ASSERT_TRUE(approxeq(t, static_cast<Real>(2.0), static_cast<Real>(1e-7)));

    Point expected(0.0, 0.5);
    ASSERT_TRUE(approxeq(expected, x, static_cast<Real>(1e-7)));
}

TEST(MeshTest, test_elem_shape) {
    using Point = moonolith::Vector<Real, 2>;
    using CoPoint = moonolith::Vector<Real, 3>;
    using E = moonolith::Tri6<Real, 3>;

    Ray<Real, 3> ray;
    ray.o.x = 0.5;
    ray.o.y = 0.5;
    ray.dir.z = 1.;

    E e;

    std::array<Point, E::NNodes> ref_nodes;
    Reference<E>::points(ref_nodes);

    for (int i = 0; i < E::NNodes; ++i) {
        e.node(i).x = ref_nodes[i].x;
        e.node(i).y = ref_nodes[i].y;
    }

    e.node(0).z = 1.0;
    e.node(1).z = 3.0;
    e.node(2).z = 1.0;
    e.node(3).z = 2.0;
    e.node(4).z = 2.0;
    e.node(5).z = 2.0;

    // e.set_affine(false);
    ElemShape<E> shape(e, 1e-10, 100, 1e-6, false);

    Real t = 0.0;
    Point x;
    bool ok = shape.intersect(ray, t, x);
    ASSERT_TRUE(ok);
    CoPoint global_x;

    ray.make(t, global_x);
    ASSERT_TRUE(approxeq(t, static_cast<Real>(2.0), static_cast<Real>(1e-7)));

    Point expected(0.5, 0.5);
    ASSERT_TRUE(approxeq(expected, x, static_cast<Real>(1e-7)));
}

TEST(MeshTest, test_elem_shape_difficult) {
    using Point = moonolith::Vector<Real, 2>;
    using CoPoint = moonolith::Vector<Real, 3>;
    using E = moonolith::Tri6<Real, 3>;

    Ray<Real, 3> ray;
    ray.o.x = 0.1;
    ray.o.y = 0.1;

    ray.dir.x = 0.4;
    ray.dir.y = 0.4;
    ray.dir.z = 1.;
    ray.dir /= length(ray.dir);

    E e;

    std::array<Point, E::NNodes> ref_nodes;
    Reference<E>::points(ref_nodes);

    for (int i = 0; i < E::NNodes; ++i) {
        e.node(i).x = ref_nodes[i].x * 2.0;
        e.node(i).y = ref_nodes[i].y * 2.0;
    }

    e.node(0).z = 1.0;
    e.node(1).z = 3.0;
    e.node(2).z = 1.0;
    e.node(3).z = 2.0;
    e.node(4).z = 2.0;
    e.node(5).z = 2.0;

    // e.set_affine(false);
    ElemShape<E> shape(e, 1e-10, 100, 1e-6, false);

    Real t = 0.0;
    Point x;
    bool ok = shape.intersect(ray, t, x);
    ASSERT_TRUE(ok);
    CoPoint global_x;
    ray.make(t, global_x);
}

TEST(MeshTest, test_l2_transfer_2) {
    // logger() << "test_l2_transfer_2 " << std::endl;

    using MasterElem = moonolith::Quad8<Real>;
    using SlaveElem = moonolith::Tri6<Real>;

    // dummy quad rule
    Quadrature2<Real> q_rule;
    // q_rule.points  = { { 0., 0.}, {1.0, 0.0}, {0.0, 1.0} };
    // q_rule.weights = { 1./3., 1./3., 1./3.};
    Gauss::get(6, q_rule);

    MasterElem master;
    SlaveElem slave;

    master.make_reference();
    slave.make_reference();

    L2Transfer<MasterElem, SlaveElem> algo;

    // always initialize both rules
    algo.set_quadrature(q_rule);
    // algo.set_quadrature_warped(q_rule);

    if (!algo.assemble(master, slave)) {
        ASSERT_TRUE(false);
    }

    // algo.mass_matrix().describe(logger());

    ASSERT_TRUE(approxeq(static_cast<Real>(0.5), algo.intersection_measure(), static_cast<Real>(1e-5)));
}

TEST(MeshTest, test_full_l2_transfer_2) {
    // logger() << "test_full_l2_transfer_2 " << std::endl;

    using MasterElem = moonolith::Quad8<Real>;
    using SlaveElem = moonolith::Tri6<Real>;

    // dummy quad rule
    Quadrature2<Real> q_rule;
    // q_rule.points  = { { 0., 0.}, {1.0, 0.0}, {0.0, 1.0} };
    // q_rule.weights = { 1./3., 1./3., 1./3.};
    Gauss::get(6, q_rule);

    MasterElem master;
    SlaveElem slave;

    master.make_reference();
    slave.make_reference();

    L2Transfer<MasterElem, SlaveElem> algo;
    algo.dual_lagrange_multiplier(false);

    // always initialize both rules
    algo.set_quadrature(q_rule);
    // algo.set_quadrature_warped(q_rule);

    if (!algo.assemble(master, slave)) {
        ASSERT_TRUE(false);
    }

    // algo.mass_matrix().describe(logger());

    ASSERT_TRUE(approxeq(static_cast<Real>(0.5), algo.intersection_measure(), static_cast<Real>(1e-5)));
}

TEST(MeshTest, test_l2_transfer_2_id) {
    // logger() << "test_l2_transfer_2_id " << std::endl;

    using MasterElem = moonolith::Tri6<Real, 3>;
    using SlaveElem = moonolith::Tri6<Real, 3>;

    // dummy quad rule
    Quadrature2<Real> q_rule;
    // q_rule.points  = { { 0., 0.}, {1.0, 0.0}, {0.0, 1.0} };
    // q_rule.weights = { 1./3., 1./3., 1./3.};
    Gauss::get(4, q_rule);

    MasterElem master;
    SlaveElem slave;

    master.make_reference();
    slave.make_reference();

    L2Transfer<MasterElem, SlaveElem> algo;

    // always initialize both rules
    algo.set_quadrature(q_rule);
    // algo.set_quadrature_warped(q_rule);

    if (!algo.assemble(master, slave)) {
        ASSERT_TRUE(false);
    }

    // algo.mass_matrix().describe(logger());

    ASSERT_TRUE(approxeq(static_cast<Real>(0.5), algo.intersection_measure(), static_cast<Real>(1e-5)));
}

TEST(MeshTest, test_l2_transfer_3) {
    // logger() << "test_l2_transfer_3 " << std::endl;

    using MasterElem = moonolith::Tet10<Real>;
    using SlaveElem = moonolith::Tet10<Real>;

    Quadrature3<Real> q_rule;
    Gauss::get(6, q_rule);
    // q_rule.points  = { { 0., 0., 0.}, {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0} };
    // q_rule.weights = { 1./4., 1./4., 1./4., 1./4. };

    MasterElem master;
    SlaveElem slave;

    master.make_reference();
    slave.make_reference();

    L2Transfer<MasterElem, SlaveElem> algo;
    algo.set_quadrature(q_rule);

    if (!algo.assemble(master, slave)) {
        ASSERT_TRUE(false);
    }

    // algo.describe(logger());
    // algo.mass_matrix().describe(logger());

    ASSERT_TRUE(approxeq(static_cast<Real>(1. / 6.), algo.intersection_measure(), static_cast<Real>(1e-5)));
}

TEST(MeshTest, test_l2_transfer_vol2surf2) {
    // logger() << "test_l2_transfer_vol2surf2 " << std::endl;

    using MasterElem = moonolith::Tri6<Real>;
    using SlaveElem = moonolith::Edge3<Real, 2>;

    Quadrature1<Real> q_rule;
    Gauss::get(6, q_rule);
    // q_rule.points  = { { 0., 0., 0.}, {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0} };
    // q_rule.weights = { 1./4., 1./4., 1./4., 1./4. };

    MasterElem master;
    SlaveElem slave;

    master.make_reference();
    slave.make_reference();

    // master.set_affine(false);

    L2Transfer<MasterElem, SlaveElem> algo;
    algo.set_quadrature(q_rule);

    if (!algo.assemble(master, slave)) {
        ASSERT_TRUE(false);
    }

    // algo.describe(logger());
    // algo.mass_matrix().describe(logger());

    ASSERT_TRUE(approxeq(static_cast<Real>(1.), algo.intersection_measure(), static_cast<Real>(1e-5)));
}

TEST(MeshTest, test_l2_transfer_vol2surf3) {
    // logger() << "test_l2_transfer_vol2surf3 " << std::endl;

    using MasterElem = moonolith::Tet10<Real>;
    using SlaveElem = moonolith::Tri6<Real, 3>;

    Quadrature2<Real> q_rule;
    Gauss::get(6, q_rule);
    // q_rule.points  = { { 0., 0., 0.}, {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0} };
    // q_rule.weights = { 1./4., 1./4., 1./4., 1./4. };

    MasterElem master;
    SlaveElem slave;

    master.make_reference();
    slave.make_reference();

    // master.set_affine(false);

    L2Transfer<MasterElem, SlaveElem> algo;
    algo.set_quadrature(q_rule);

    if (!algo.assemble(master, slave)) {
        ASSERT_TRUE(false);
    }

    // algo.describe(logger());
    // algo.mass_matrix().describe(logger());

    ASSERT_TRUE(approxeq(static_cast<Real>(1. / 2.), algo.intersection_measure(), static_cast<Real>(1e-5)));
}

TEST(MeshTest, test_l2_transfer_vol2surf2_polymorphic) {
    // logger() << "test_l2_transfer_vol2surf2_polymorphic " << std::endl;

    using MasterElem = moonolith::Tri6<Real>;
    using SlaveElem = moonolith::Edge3<Real, 2>;
    using PolymorphicElemMaster = moonolith::Elem<Real, 2, 2>;
    using PolymorphicElemSlave = moonolith::Elem<Real, 1, 2>;

    Quadrature1<Real> q_rule;
    Gauss::get(6, q_rule);
    // q_rule.points  = { { 0., 0., 0.}, {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0} };
    // q_rule.weights = { 1./4., 1./4., 1./4., 1./4. };

    MasterElem master;
    SlaveElem slave;

    master.make_reference();
    slave.make_reference();

    // master.set_affine(false);

    L2Transfer<PolymorphicElemMaster, PolymorphicElemSlave> algo;
    algo.set_quadrature(q_rule);

    if (!algo.assemble(master, slave)) {
        ASSERT_TRUE(false);
    }

    // algo.describe(logger());
    // algo.mass_matrix().describe(logger());

    ASSERT_TRUE(approxeq(static_cast<Real>(1.), algo.intersection_measure(), static_cast<Real>(1e-5)));
}

TEST(MeshTest, test_l2_transfer_vol2surf3_polymorphic) {
    // logger() << "test_l2_transfer_vol2surf3_polymorphic " << std::endl;

    using MasterElem = moonolith::Tet10<Real>;
    using SlaveElem = moonolith::Tri6<Real, 3>;
    using PolymorphicElemMaster = moonolith::Elem<Real, 3, 3>;
    using PolymorphicElemSlave = moonolith::Elem<Real, 2, 3>;

    Quadrature2<Real> q_rule;
    Gauss::get(6, q_rule);
    // q_rule.points  = { { 0., 0., 0.}, {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0} };
    // q_rule.weights = { 1./4., 1./4., 1./4., 1./4. };

    MasterElem master;
    SlaveElem slave;

    master.make_reference();
    slave.make_reference();

    // master.set_affine(false);

    L2Transfer<PolymorphicElemMaster, PolymorphicElemSlave> algo;
    algo.set_quadrature(q_rule);

    if (!algo.assemble(master, slave)) {
        ASSERT_TRUE(false);
    }

    // algo.describe(logger());
    // algo.mass_matrix().describe(logger());

    ASSERT_TRUE(approxeq(static_cast<Real>(1. / 2.), algo.intersection_measure(), static_cast<Real>(1e-5)));
}

TEST(MeshTest, test_l2_transfer_hex_to_tet) {
    // logger() << "test_l2_transfer_hex_to_tet " << std::endl;

    using MasterElem = moonolith::Hex27<Real>;
    using SlaveElem = moonolith::Tet10<Real>;

    Quadrature3<Real> q_rule;
    Gauss::get(6, q_rule);
    // q_rule.points  = { { 0., 0., 0.}, {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0} };
    // q_rule.weights = { 1./4., 1./4., 1./4., 1./4. };

    MasterElem master;
    SlaveElem slave;

    master.make_reference();
    slave.make_reference();

    master.set_affine(false);

    L2Transfer<MasterElem, SlaveElem> algo;
    algo.set_quadrature(q_rule);

    if (!algo.assemble(master, slave)) {
        ASSERT_TRUE(false);
    }

    // algo.describe(logger());
    // algo.mass_matrix().describe(logger());

    ASSERT_TRUE(approxeq(static_cast<Real>(1. / 6.), algo.intersection_measure(), static_cast<Real>(1e-5)));
}

TEST(MeshTest, test_l2_transfer_p0_2D) {
    // logger() << "test_l2_transfer_p0_2D " << std::endl;

    using MasterElem = moonolith::Tri1<Real>;
    using SlaveElem = moonolith::Tri1<Real>;

    Quadrature2<Real> q_rule;
    Gauss::get(6, q_rule);
    // q_rule.points  = { { 0., 0., 0.}, {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0} };
    // q_rule.weights = { 1./4., 1./4., 1./4., 1./4. };

    MasterElem master;
    SlaveElem slave;

    master.make_reference();
    slave.make_reference();

    // master.set_affine(false);

    L2Transfer<MasterElem, SlaveElem> algo;
    algo.set_quadrature(q_rule);

    if (!algo.assemble(master, slave)) {
        ASSERT_TRUE(false);
    }

    // algo.describe(logger());
    // algo.mass_matrix().describe(logger());

    ASSERT_TRUE(approxeq(static_cast<Real>(1. / 2.), algo.intersection_measure(), static_cast<Real>(1e-5)));
}

TEST(MeshTest, test_l2_transfer_p0) {
    // logger() << "test_l2_transfer_p0 " << std::endl;

    using MasterElem = moonolith::Hex1<Real>;
    using SlaveElem = moonolith::Tet1<Real>;

    Quadrature3<Real> q_rule;
    Gauss::get(6, q_rule);

    MasterElem master;
    SlaveElem slave;

    master.make_reference();
    slave.make_reference();

    L2Transfer<MasterElem, SlaveElem> algo;
    algo.set_quadrature(q_rule);

    if (!algo.assemble(master, slave)) {
        ASSERT_TRUE(false);
    }

    // algo.describe(logger());
    // algo.mass_matrix().describe(logger());

    ASSERT_TRUE(approxeq(static_cast<Real>(1. / 6.), algo.intersection_measure(), static_cast<Real>(1e-5)));
}

TEST(MeshTest, test_l2_transfer_polymorphic) {
    // logger() << "test_l2_transfer_polymorphic " << std::endl;

    using MasterElem = moonolith::Hex27<Real>;
    using SlaveElem = moonolith::Tet10<Real>;
    using PolymorphicElem = moonolith::Elem<Real, 3, 3>;

    Quadrature3<Real> q_rule;
    Gauss::get(6, q_rule);
    // q_rule.points  = { { 0., 0., 0.}, {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0} };
    // q_rule.weights = { 1./4., 1./4., 1./4., 1./4. };

    MasterElem master;
    SlaveElem slave;

    master.make_reference();
    slave.make_reference();

    master.set_affine(false);

    L2Transfer<PolymorphicElem, PolymorphicElem> algo;
    algo.set_quadrature(q_rule);

    if (!algo.assemble(master, slave)) {
        ASSERT_TRUE(false);
    }

    // algo.describe(logger());
    // algo.mass_matrix().describe(logger());

    ASSERT_TRUE(approxeq(static_cast<Real>(1. / 6.), algo.intersection_measure(), static_cast<Real>(1e-5)));
}

TEST(MeshTest, contact_test) {
    using MasterElem = moonolith::Tri6<Real, 3>;
    using SlaveElem = moonolith::Tri6<Real, 3>;

    Quadrature2<Real> q_rule;
    Gauss::get(4, q_rule);

    MasterElem master;
    SlaveElem slave;

    master.make_reference();
    slave.make_reference();

    ContactMortar<MasterElem, SlaveElem> algo;

    // always initialize both rules
    algo.set_quadrature_affine(q_rule);
    algo.set_quadrature_warped(q_rule);

    if (algo.assemble(master, slave)) {
        ASSERT_TRUE(false);
    }

    slave.set_affine(false);

    if (algo.assemble(master, slave)) {
        ASSERT_TRUE(false);
    }

    invert_orientation(slave);

    slave.set_affine(true);

    if (!algo.assemble(master, slave)) {
        ASSERT_TRUE(false);
    }

    // logger() << "AFFINE" << std::endl;
    // logger() << "------------------" << std::endl;
    // // algo.mass_matrix().describe(logger());
    // logger() << "------------------" << std::endl;
    // // algo.coupling_matrix().describe(logger());
    // logger() << "------------------" << std::endl;

    ASSERT_TRUE(approxeq(static_cast<Real>(0.5), algo.projection_measure(), static_cast<Real>(1e-5)));
    // ASSERT_TRUE(approxeq(algo.mass_matrix(), algo.coupling_matrix(),  static_cast<Real>(1e-5)));

    slave.set_affine(false);

    algo.clear();
    if (!algo.assemble(master, slave)) {
        ASSERT_TRUE(false);
    }

    // logger() << "WARPED" << std::endl;
    // logger() << "------------------" << std::endl;
    // // algo.mass_matrix().describe(logger());
    // logger() << "------------------" << std::endl;
    // // algo.coupling_matrix().describe(logger());
    // logger() << "------------------" << std::endl;

    ASSERT_TRUE(approxeq(static_cast<Real>(0.5), algo.projection_measure(), static_cast<Real>(1e-5)));
    // ASSERT_TRUE(approxeq(algo.mass_matrix(), algo.coupling_matrix(),  static_cast<Real>(1e-5)));
}

TEST(MeshTest, contact_polymorphic_test) {
    using MasterElem = moonolith::Tri3<Real, 3>;
    using SlaveElem = moonolith::Quad4<Real, 3>;

    using PolymorphicElem = moonolith::Elem<Real, 2, 3>;

    // dummy quad rule
    Quadrature2<Real> q_rule;
    q_rule.points = {{0., 0.}, {1.0, 0.0}, {0.0, 1.0}};
    q_rule.weights = {1. / 3., 1. / 3., 1. / 3.};

    MasterElem master;
    SlaveElem slave;

    master.make_reference();
    slave.make_reference();

    ContactMortar<PolymorphicElem, PolymorphicElem> algo;

    // always initialize both rules
    algo.set_quadrature_affine(q_rule);
    algo.set_quadrature_warped(q_rule);

    if (algo.assemble(master, slave)) {
        ASSERT_TRUE(false);
    }

    slave.set_affine(false);

    if (algo.assemble(master, slave)) {
        ASSERT_TRUE(false);
    }

    invert_orientation(slave);

    slave.set_affine(true);

    if (!algo.assemble(master, slave)) {
        ASSERT_TRUE(false);
    }
    ASSERT_TRUE(approxeq(static_cast<Real>(0.5), algo.projection_measure(), static_cast<Real>(1e-5)));

    slave.set_affine(false);

    algo.clear();
    if (!algo.assemble(master, slave)) {
        ASSERT_TRUE(false);
    }

    ASSERT_TRUE(approxeq(static_cast<Real>(0.5), algo.projection_measure(), static_cast<Real>(1e-5)));
    // algo.describe(logger());
}

TEST(MeshTest, par_contact_test) {
    // logger() << "par_contact_test\n";

    Communicator comm;

    auto mesh = std::make_shared<Mesh<Real, 2>>(comm);

    FunctionSpace<Mesh<Real, 2>> space(mesh);

    if (comm.is_root()) {
        ///////////////////////////////////////////////////

        mesh->resize(2, 5);

        //// MASTER ////

        mesh->point(0) = {0.0, 0.0};
        mesh->point(1) = {1.0, 0.0};

        auto &e_m = mesh->elem(0);

        e_m.type = EDGE2;
        e_m.block = 1;
        e_m.global_idx = 0;
        e_m.nodes = {0, 1};
        e_m.is_affine = true;

        //// SLAVE ////
        mesh->point(2) = {1.0, 0.1};
        mesh->point(3) = {0.0, 0.1};
        mesh->point(4) = {0.5, 0.5};

        auto &e_s = mesh->elem(1);

        e_s.type = EDGE3;
        e_s.block = 0;
        e_s.global_idx = 1;
        e_s.nodes = {2, 3, 4};
        e_s.is_affine = false;

        //// DOFMAP ////

        space.dof_map().resize(2);

        auto &dof_m = space.dof_map().dof_object(0);
        dof_m.block = 1;
        dof_m.global_idx = 1;
        dof_m.element_dof = 1;
        dof_m.dofs = {0, 1};
        dof_m.type = EDGE2;

        ///////////////////////////////////////////////////

        auto &dof_s = space.dof_map().dof_object(1);
        dof_s.block = 0;
        dof_s.global_idx = 1;
        dof_s.element_dof = 1;
        dof_s.dofs = {2, 3, 4};
        dof_s.type = EDGE3;

        ///////////////////////////////////////////////////

        space.dof_map().set_n_local_dofs(5);
    } else {
        space.dof_map().set_n_local_dofs(0);
    }

    space.dof_map().set_n_dofs(5);

    mesh->finalize();

    MatlabScripter script;
    script.close_all();
    mesh->draw(script);
    script.save("mesh_contact.m");

    ParContact<Real, 2> par_contact(comm, false);
    par_contact.assemble({{1, 0}}, space, std::make_shared<moonolith::SearchRadius<Real>>(0.2), nullptr);
}

TEST(MeshTest, test_l2_transfer_lower_dim_2) {
    // logger() << "test_l2_transfer_lower_dim_2 " << std::endl;

    using MasterElem = moonolith::Edge2<Real, 2>;
    using SlaveElem = moonolith::Edge2<Real, 2>;

    MasterElem master;
    SlaveElem slave;

    master.make_reference();
    slave.make_reference();

    auto &p0 = slave.node(0);
    auto &p1 = slave.node(1);

    p0.x = 0.5;
    p0.y = -1.0;

    p1.x = 0.5;
    p1.y = 1.0;

    // Intersection is a point
    LowerDimL2Transfer<MasterElem, SlaveElem> algo;
    // algo.set_quadrature(q_rule);

    if (!algo.assemble(master, slave)) {
        ASSERT_TRUE(false);
    }

    // algo.describe(logger());
    // algo.mass_matrix().describe(logger());

    ASSERT_TRUE(approxeq(static_cast<Real>(1.0), algo.intersection_measure(), static_cast<Real>(1e-5)));
}

TEST(MeshTest, test_l2_transfer_lower_dim_3) {
    // logger() << "test_l2_transfer_lower_dim_3 " << std::endl;

    using MasterElem = moonolith::Tri3<Real, 3>;
    using SlaveElem = moonolith::Tri3<Real, 3>;

    MasterElem master;
    SlaveElem slave;

    master.make_reference();
    slave.make_reference();

    for (int i = 0; i < 3; ++i) {
        auto &p = slave.node(i);

        p.z = p.y;
        p.y = 0.5;
    }

    Quadrature1<Real> q_rule;
    Gauss::get(2, q_rule);

    // Intersection is a line
    LowerDimL2Transfer<MasterElem, SlaveElem> algo;
    algo.set_quadrature(q_rule);

    if (!algo.assemble(master, slave)) {
        ASSERT_TRUE(false);
    }

    ASSERT_TRUE(approxeq(static_cast<Real>(0.5), algo.intersection_measure(), static_cast<Real>(1e-5)));
}

TEST(MeshTest, test_l2_transfer_lower_dim_3_quad) {
    // logger() << "test_l2_transfer_lower_dim_3_quad " << std::endl;

    using MasterElem = moonolith::Quad4<Real, 3>;
    using SlaveElem = moonolith::Quad4<Real, 3>;

    MasterElem master;
    SlaveElem slave;

    master.make_reference();
    slave.make_reference();

    for (int i = 0; i < 4; ++i) {
        auto &p = slave.node(i);

        p.z = p.y;
        p.y = 0.5;
    }

    Quadrature1<Real> q_rule;
    Gauss::get(2, q_rule);

    // Intersection is a line
    LowerDimL2Transfer<MasterElem, SlaveElem> algo;
    algo.set_quadrature(q_rule);

    if (!algo.assemble(master, slave)) {
        ASSERT_TRUE(false);
    }

    ASSERT_TRUE(approxeq(static_cast<Real>(1.0), algo.intersection_measure(), static_cast<Real>(1e-5)));

    master.node(0) = {0.000000000000000013877787807814457, -0.3000000000000001, 0.0};
    master.node(1) = {-0.10000000000000002, -0.30000000000000004, 0};
    master.node(2) = {-0.10000000000000002, -0.40000000000000002, 0};
    master.node(3) = {0.000000000000000013877787807814457, -0.39999999999999974, 0.0};

    slave.node(0) = {0.0000000000000000061232339957367717, -0.3000000000000001, -0.10000000000000009};
    slave.node(1) = {0.0, -0.3000000000000001, 0.0};
    slave.node(2) = {0.0, -0.39999999999999974, 0.0};
    slave.node(3) = {0.0000000000000000061232339957367733, -0.39999999999999986, -0.10000000000000009};

    // {
    // 	MatlabScripter script;
    // 	script.close_all();
    // 	script.plot_polygon(master.nodes(), "b-");
    // 	script.hold_on();
    // 	script.plot_polygon(slave.nodes(), "r-");

    // 	script.save("polys1.m");
    // }

    algo.clear();
    if (!algo.assemble(master, slave)) {
        ASSERT_TRUE(false);
    }

    // algo.describe(logger());
    // algo.mass_matrix().describe(logger());

    ASSERT_TRUE(approxeq(static_cast<Real>(0.1), algo.intersection_measure(), static_cast<Real>(1e-5)));
}

TEST(MeshTest, test_bug) {
    // logger() << "test_bug" << std::endl;

    using Tri = moonolith::Tri3<Real, 2>;
    using Quad = moonolith::Quad4<Real, 2>;
    using Point = moonolith::Vector<Real, 2>;
    using Poly = moonolith::Polygon<Real, 2>;

    Storage<Point> ps(9);
    ps[0] = {8.04000e+02, 1.06348e+02};
    ps[1] = {8.09443e+02, 1.06313e+02};
    ps[2] = {8.09731e+02, 1.10760e+02};
    ps[3] = {8.04723e+02, 1.11144e+02};
    ps[4] = {8.09531e+02, 1.02033e+02};
    ps[5] = {8.05389e+02, 1.02251e+02};
    ps[6] = {8.13892e+02, 1.01074e+02};
    ps[7] = {8.14015e+02, 1.05440e+02};
    ps[8] = {8.14212e+02, 1.09964e+02};

    Storage<Quad> elem_slave(4);
    elem_slave[0].nodes() = {ps[0], ps[1], ps[2], ps[3]};
    elem_slave[1].nodes() = {ps[4], ps[1], ps[0], ps[5]};
    elem_slave[2].nodes() = {ps[6], ps[7], ps[1], ps[4]};
    elem_slave[3].nodes() = {ps[2], ps[1], ps[7], ps[8]};

    for (auto &e : elem_slave) {
        e.set_affine(false);
    }

    Storage<Point> pm(16);
    pm[0] = {806.558, 113.245};
    pm[1] = {800.308, 114.026};
    pm[2] = {800.352, 108.887};
    pm[3] = {819.102, 106.544};
    pm[4] = {812.852, 107.325};
    pm[5] = {812.896, 102.186};
    pm[6] = {819.146, 101.405};
    pm[7] = {812.94, 97.0467};
    pm[8] = {806.602, 108.106};
    pm[9] = {806.646, 102.967};
    pm[10] = {800.396, 103.748};
    pm[11] = {800.44, 98.6092};
    pm[12] = {806.69, 97.828};
    pm[13] = {819.19, 96.2655};
    pm[14] = {812.808, 112.464};
    pm[15] = {819.058, 111.683};

    Storage<Tri> elem_master(18);
    elem_master[0].nodes() = {pm[0], pm[1], pm[2]};
    elem_master[1].nodes() = {pm[3], pm[4], pm[5]};
    elem_master[2].nodes() = {pm[6], pm[5], pm[7]};
    elem_master[3].nodes() = {pm[3], pm[5], pm[6]};
    elem_master[4].nodes() = {pm[4], pm[8], pm[9]};
    elem_master[5].nodes() = {pm[8], pm[2], pm[10]};
    elem_master[6].nodes() = {pm[9], pm[10], pm[11]};
    elem_master[7].nodes() = {pm[8], pm[10], pm[9]};
    elem_master[8].nodes() = {pm[4], pm[9], pm[5]};
    elem_master[9].nodes() = {pm[9], pm[11], pm[12]};
    elem_master[10].nodes() = {pm[5], pm[12], pm[7]};
    elem_master[11].nodes() = {pm[9], pm[12], pm[5]};
    elem_master[12].nodes() = {pm[6], pm[7], pm[13]};
    elem_master[13].nodes() = {pm[0], pm[2], pm[8]};
    elem_master[14].nodes() = {pm[14], pm[8], pm[4]};
    elem_master[15].nodes() = {pm[0], pm[8], pm[14]};
    elem_master[16].nodes() = {pm[15], pm[4], pm[3]};
    elem_master[17].nodes() = {pm[14], pm[4], pm[15]};

    // {
    // 	MatlabScripter script;
    // 	script.close_all();
    // 	script.hold_on();
    // 	script.axis_equal();

    // 	for(auto &e : elem_master) {
    // 		script.plot_polygon(e.nodes(), "b-");
    // 	}

    // 	for(auto &e : elem_slave) {
    // 		script.plot_polygon(e.nodes(), "r-");
    // 	}

    // 	script.save("bug.m");
    // }

    {
        // MatlabScripter script_all;

        // script_all.close_all();
        // script_all.hold_on();
        // script_all.axis_equal();

        const std::size_t n_slave = elem_slave.size();
        const std::size_t n_master = elem_master.size();

        Quadrature2<Real> q_rule, q_out, q_slave;
        Gauss::get(6, q_rule);
        L2Transfer<Tri, Quad> algo;
        algo.set_quadrature(q_rule);

        Poly p_master, p_slave, result;
        IntersectConvexPolygons<Real, 2> isect;

        std::shared_ptr<Transform<Real, 2, 2>> trafo_slave;

        BuildQuadrature<Poly> build_q;

        for (std::size_t is = 0; is < n_slave; ++is) {
            for (std::size_t im = 0; im < n_master; ++im) {
                // auto im = 13;
                // auto is = 0;

                if (algo.assemble(elem_master[im], elem_slave[is])) {
                    {
                        elem_master[im].make(p_master);
                        elem_slave[is].make(p_slave);

                        isect.apply(p_master, p_slave, result);
                        build_q.apply(q_rule, p_master, p_slave, q_out);
                        make_transform(elem_slave[is], trafo_slave);

                        std::size_t n_qp = q_out.n_points();
                        q_slave.resize(n_qp);
                        for (std::size_t k = 0; k < n_qp; ++k) {
                            // if(k == 1) std::raise(SIGINT);

                            bool ok_slave = trafo_slave->apply_inverse(q_out.point(k), q_slave.point(k));
                            ASSERT_TRUE(ok_slave);

                            ASSERT_TRUE(q_slave.point(k).x >= 0.0);
                            ASSERT_TRUE(q_slave.point(k).y >= 0.0);

                            ASSERT_TRUE(q_slave.point(k).x <= 1.0);
                            ASSERT_TRUE(q_slave.point(k).y <= 1.0);
                        }

                        // MatlabScripter script;
                        // script.close_all();
                        // script.hold_on();
                        // script.axis_equal();

                        // script.plot(q_slave.points, "*k");
                        // script.save("ref.m");
                    }

                    // MatlabScripter script;
                    // script.close_all();
                    // script.hold_on();
                    // script.axis_equal();

                    // script.plot_polygon(elem_master[im].nodes(), "b-");
                    // script.plot_polygon(elem_slave[is].nodes(),  "r-");
                    // script.plot(result, "g-");

                    // script.plot(q_out.points, "*k");

                    // script.save("bug" + std::to_string(im) + "_" + std::to_string(is) + ".m");

                    // script_all.plot_polygon(elem_master[im].nodes(), "b-");
                    // script_all.plot_polygon(elem_slave[is].nodes(),  "r-");
                }
            }
        }

        // script_all.save("bug.m");
    }
}
