#ifndef FE_INTERFACES_2_CL


//Types
#include "fe_object.cl"

//Forward declarations of types


#ifndef CLIPP_HOST_CL
////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// general /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

void iso_parametric_transform_2(const FEObject *fe, const Scalar * points, Scalar * global_quad_points);
void iso_parametric_jacobian_2(const FEObject *fe, const Scalar * points, Scalar * jacobians);
static void make_inverse_affine_transform_2(const Scalar *A, const Scalar *b, Scalar *Ainv, Scalar *binv);
static void apply_affine_transform_2(const Scalar *A, const Scalar *b, const SizeType n_points, const Scalar *in, Scalar *out);
////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// triangle /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

//functions
void triangle_jacobian(const Scalar * points, const SizeType n_quad_points, const Scalar * quad_points, Scalar * jacobians);
void triangle_transform(const Scalar * points, const SizeType n_quad_points, const Scalar * quad_points, Scalar * global_quad_points);
void triangle_inverse_transform(const Scalar * points, const SizeType n_quad_points, const Scalar * global_quad_points, Scalar * quad_points);
void triangle_make_fe_object(const Scalar *points, const SizeType n_quad_points, const Scalar * quad_points, FEObject *object);

//Shape functions
void triangle_linear_shape_fun(const SizeType number, const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);
void triangle_linear_fun_0(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);
void triangle_linear_fun_1(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);
void triangle_linear_fun_2(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);

void triangle_linear_grad_0(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads);
void triangle_linear_grad_1(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads);
void triangle_linear_grad_2(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads);

////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// triangle 2 ///////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

void triangle_2_jacobian(const Scalar * points, const SizeType n_quad_points, const Scalar * quad_points, Scalar * jacobians);
void triangle_2_transform(const Scalar * points, const SizeType n_quad_points, const Scalar * quad_points, Scalar * global_quad_points);
void triangle_2_make_fe_object(const SizeType n_points, const Scalar *points, const SizeType n_quad_points, const Scalar * quad_points, FEObject *object);

//Shape functions
void triangle_quadratic_shape_fun(const SizeType number, const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);
void triangle_quadratic_fun_0(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);
void triangle_quadratic_fun_1(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);
void triangle_quadratic_fun_2(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);
void triangle_quadratic_fun_3(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);
void triangle_quadratic_fun_4(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);
void triangle_quadratic_fun_5(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);

void triangle_quadratic_grad_0(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads);
void triangle_quadratic_grad_1(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads);
void triangle_quadratic_grad_2(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads);
void triangle_quadratic_grad_3(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads);
void triangle_quadratic_grad_4(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads);
void triangle_quadratic_grad_5(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads);


////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// quad ///////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

void quad_transform(const Scalar * points, const SizeType n_quad_points, const Scalar * quad_points, Scalar * global_quad_points);
void quad_transform_jacobian(const Scalar * points, const SizeType n_quad_points, const Scalar * quad_points, Scalar * jacobians);
//Just uses the ones for the triangle (might fail if point ordering is not ccw)
void quad_affine_transform(const Scalar * points, const SizeType n_quad_points, const Scalar * quad_points, Scalar * global_quad_points);
void quad_affine_jacobian(const Scalar * points, const SizeType n_quad_points, const Scalar * quad_points, Scalar * jacobians);

//needs the basis functions to be initialized
void quad_iso_parametric_transform(const FEObject *object, const Scalar * points, Scalar * global_quad_points);
void quad_iso_parametric_jacobian(const FEObject *object, const Scalar * points, Scalar * jacobians);

void quad_inverse_transform(const Scalar * points, const SizeType n_quad_points, const Scalar * global_quad_points, Scalar * quad_points);
void quad_make_fe_object(const Scalar *points, const SizeType n_quad_points, const Scalar * quad_points, FEObject *object);

//Shape functions
void quad_bilinear_fun_0(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);
void quad_bilinear_fun_1(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);
void quad_bilinear_fun_2(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);
void quad_bilinear_fun_3(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs);


void quad_bilinear_grad_0(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads);
void quad_bilinear_grad_1(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads);
void quad_bilinear_grad_2(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads);
void quad_bilinear_grad_3(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads);

void triangle_transform_3(const Scalar * points, const SizeType n_quad_points, const Scalar * quad_points, Scalar * global_quad_points);
static void triangle_make_affine_transform_3(const Scalar * points, Scalar *A, Scalar *b);
void triangle_inverse_transform_3(const Scalar * points, const SizeType n_quad_points, const Scalar * global_quad_points, Scalar * quad_points, const bool preserve_z);
void triangle_normal(const Scalar *points, Scalar *normal);


void line_transform_2(const Scalar *line, const SizeType n_quad_points, const Scalar *quad_points, Scalar * global_points);
#endif //CLIPP_HOST_CL
#endif //FE_INTERFACES_2_CL
