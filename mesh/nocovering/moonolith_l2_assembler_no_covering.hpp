#ifndef MOONOLITH_L2_ASSEMBLER_NO_COVERING_HPP
#define MOONOLITH_L2_ASSEMBLER_NO_COVERING_HPP

#include "moonolith_elem_dual_partial.hpp"
#include "moonolith_intersect_polyhedra_impl.hpp"
#include "moonolith_l2_assembler.hpp"
#include "moonolith_matrix_buffers.hpp"

namespace moonolith {

    template <class MasterElem, class SlaveElem>
    class L2AssemblerNoCovering : public Transfer<typename SlaveElem::T, SlaveElem::PhysicalDim>, public Describable {
    public:
        using T = typename SlaveElem::T;

        static const int MasterDim = MasterElem::Dim;
        static const int SlaveDim = SlaveElem::Dim;
        static const int PhysicalDim = SlaveElem::PhysicalDim;
        static const int NMasterNodes = MasterElem::NNodes;
        static const int NSlaveNodes = SlaveElem::NNodes;

        using CoVector = moonolith::Vector<T, PhysicalDim>;
        using Algo = moonolith::BuildQuadratureAlgo<T, PhysicalDim, MasterDim, SlaveDim>;
        using MasterAffineTrafo = moonolith::AffineTransform<T, MasterDim, PhysicalDim>;
        using SlaveAffineTrafo = moonolith::AffineTransform<T, SlaveDim, PhysicalDim>;

        using QuadratureT = moonolith::Quadrature<T, SlaveDim>;

        L2AssemblerNoCovering() : intersection_measure_(0), dual_lagrange_multiplier_(true) {
            trafo_master = std::make_shared<MasterAffineTrafo>();
            trafo_slave = std::make_shared<SlaveAffineTrafo>();
        }

        void describe(std::ostream &) const override {}

        void set_quadrature(const QuadratureT &q_rule) { algo_.q_rule = q_rule; }

        void set_quadrature(Quadrature<T, SlaveDim> &&q_rule) { algo_.q_rule = std::move(q_rule); }

        template <class Elem>
        inline static int order_of_quadrature(const Elem &e) {
            if (e.is_simplex()) {
                return e.order();
            } else {
                return e.order() * Elem::Dim;
            }
        }

        void init_quadrature() {
            int master_order = order_of_quadrature(*master_elem[0]);
            int slave_order = order_of_quadrature(*slave_elem);

            int order = 0;
            if (slave_elem->is_affine()) {
                order = master_order + slave_order;
            } else {
                // assumes tensor product
                order = master_order + slave_order + (slave_order - 1) * 2;
            }

            if (order == current_quadrature_order_) return;

            Gauss::get(order, algo_.q_rule);
        }

        template <class ElemAdapter>
        inline bool apply(const Storage<ElemAdapter> &masters, const ElemAdapter &slave, MatrixBuffers<T> &buffers) {
            //////////////////////////////////////////////////////////////////////
            // Slave data
            auto &e_s = slave.elem();
            auto &m_s = slave.collection();
            auto &dof_obj_s = m_s.dof_map().dof_object(slave.element_handle());
            auto &dof_s = dof_obj_s.dofs;
            make(m_s, e_s, slave_elem);

            bool is_affine = slave_elem->is_affine();

            //////////////////////////////////////////////////////////////////////
            // Master data
            const std::size_t n_masters = masters.size();
            master_elem.resize(n_masters);

            for (std::size_t i = 0; i < n_masters; ++i) {
                auto &e_m = masters[i].elem();
                auto &m_m = masters[i].collection();
                make(m_m, e_m, master_elem[i]);

                is_affine = is_affine && master_elem[i]->is_affine();
            }

            //////////////////////////////////////////////////////////////////////

            init_quadrature();

            q_slave.resize(n_masters);
            q_master.resize(n_masters);

            //////////////////////////////////////////////////////////////////////
            // Intersection data slave

            make(*slave_elem, algo_.slave);

            if (is_affine) {
                make_transform(*slave_elem, *trafo_slave);
                algo_.trafo_slave = trafo_slave;
            }

            //////////////////////////////////////////////////////////////////////

            bool ok = true;
            bool has_intersection = false;
            for (std::size_t i = 0; i < n_masters; ++i) {
                make(*master_elem[i], algo_.master);

                // If they are all affine we save some calculations
                if (is_affine) {
                    make_transform(*master_elem[i], *trafo_master);
                    algo_.trafo_master = trafo_master;
                } else {
                    const bool is_pair_affine = slave_elem->is_affine() && master_elem[i]->is_affine();

                    if (is_pair_affine) {
                        make_transform(*master_elem[i], *trafo_master);
                        make_transform(*slave_elem, *trafo_slave);

                        algo_.trafo_master = trafo_master;
                        algo_.trafo_slave = trafo_slave;
                    } else {
                        make_transform(*master_elem[i], iso_trafo_master);
                        make_transform(*slave_elem, iso_trafo_slave);

                        algo_.trafo_master = iso_trafo_master;
                        algo_.trafo_slave = iso_trafo_slave;
                    }
                }

                if (algo_.compute()) {
                    q_slave[i] = algo_.q_slave;
                    q_master[i] = algo_.q_master;
                    has_intersection = true;
                } else {
                    q_slave[i].clear();
                }
            }

            if (!has_intersection || !ok) return false;

            // store dual weights
            dual_weights_partial_.weights(*slave_elem, q_slave, dual_fe_.weights());

            resize(mass_mat_, slave_elem->n_nodes(), slave_elem->n_nodes());
            mass_mat_.zero();

            for (std::size_t i = 0; i < n_masters; ++i) {
                if (q_slave[i].empty()) continue;

                master_fe_.init(*master_elem[i], q_master[i], true, false, false);
                slave_fe_.init(*slave_elem, q_slave[i], true, false, true);
                dual_fe_.init_with_stored_weights(slave_fe_);
                trafo_fe_.init(slave_fe_);

                // resize(coupling_mat_, slave_elem->n_nodes(), master_elem[i]->n_nodes());
                // coupling_mat_.zero();

                assemble_mass_matrix(master_fe_, dual_fe_, coupling_mat_);
                assemble_mass_matrix_add(trafo_fe_, dual_fe_, mass_mat_);

                const auto &m = masters[i];
                auto &m_m = m.collection();
                auto &dof_obj_m = m_m.dof_map().dof_object(m.element_handle());
                auto &dof_m = dof_obj_m.dofs;
                buffers.B.insert(dof_s, dof_m, coupling_mat_);
            }

            trafo_ = trafo_fe_.transformation();
            //!!! The global transformation needs the transpose of the basis transform
            make_transpose(trafo_);

            buffers.D.insert(dof_s, dof_s, mass_mat_);
            buffers.Q.insert(dof_s, dof_s, trafo_);

            auto meas = mass_mat_.sum();

            // logger() << "################\n";
            // mass_mat_.describe(logger());
            // logger() << "################\n";

            intersection_measure_ += meas;
            buffers.measure().insert(dof_obj_s.element_dof, meas);
            return true;
        }

        inline void clear() { intersection_measure_ = 0.0; }

        inline T intersection_measure() const { return intersection_measure_; }

    private:
        Algo algo_;
        T intersection_measure_;

        FE<MasterElem> master_fe_;
        FE<SlaveElem> slave_fe_;
        Dual<SlaveElem> dual_fe_;
        TransformedFE<SlaveElem> trafo_fe_;

        Matrix<T, NSlaveNodes, NMasterNodes> coupling_mat_;
        Matrix<T, NSlaveNodes, NSlaveNodes> mass_mat_;
        Matrix<T, NSlaveNodes, NSlaveNodes> trafo_;

        std::shared_ptr<MasterAffineTrafo> trafo_master;
        std::shared_ptr<SlaveAffineTrafo> trafo_slave;

        std::shared_ptr<Transform<T, MasterDim, PhysicalDim>> iso_trafo_master;
        std::shared_ptr<Transform<T, SlaveDim, PhysicalDim>> iso_trafo_slave;

        Storage<std::shared_ptr<MasterElem>> master_elem;
        std::shared_ptr<SlaveElem> slave_elem;

        bool dual_lagrange_multiplier_;

        // handling dual lagrange multiplier without covering

        DualWeightsPartial<SlaveElem> dual_weights_partial_;
        Storage<QuadratureT> q_slave;
        Storage<QuadratureT> q_master;
        int current_quadrature_order_{0};
    };

}  // namespace moonolith

#endif  // MOONOLITH_L2_ASSEMBLER_NO_COVERING_HPP
