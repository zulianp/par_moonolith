#ifndef MOONOLITH_CONTACT_HPP
#define MOONOLITH_CONTACT_HPP

#include "moonolith_affine_contact.hpp"
#include "moonolith_assembly.hpp"
#include "moonolith_elem_dual.hpp"
#include "moonolith_elem_shape.hpp"
#include "moonolith_fe.hpp"
#include "moonolith_iso_parametric_transform.hpp"
#include "moonolith_map_quadrature.hpp"
#include "moonolith_project_convex_polygons.hpp"
#include "moonolith_project_line_segments.hpp"
#include "moonolith_project_polygons.hpp"
#include "moonolith_project_polylines.hpp"
#include "moonolith_shape.hpp"
#include "moonolith_transform.hpp"
#include "moonolith_transformed_fe.hpp"
#include "moonolith_warped_contact.hpp"

#include <memory>

namespace moonolith {

    // TODO
    template <typename T, int PhysicalDim>
    class Contact {
    public:
        virtual ~Contact() {}
    };

    template <class MasterElem, class SlaveElem>
    class ContactMortar final : public Contact<typename SlaveElem::T, SlaveElem::PhysicalDim>, public Describable {
    public:
        using T = typename SlaveElem::T;
        static const int Dim = SlaveElem::Dim;
        static const int PhysicalDim = SlaveElem::PhysicalDim;
        static const int NMasterNodes = MasterElem::NNodes;
        static const int NSlaveNodes = SlaveElem::NNodes;

        using AffineTrafo = moonolith::AffineTransform<T, Dim, PhysicalDim>;
        using Shape = moonolith::Shape<T, Dim, PhysicalDim>;
        using Vector = moonolith::Vector<T, Dim>;
        using CoVector = moonolith::Vector<T, PhysicalDim>;
        using NormalVec = moonolith::Vector<T, StaticMax<NSlaveNodes * PhysicalDim, -1>::value>;

        ContactMortar(const bool invert_plane_dir = false) : projection_measure_(0.) {
            trafo_master = std::make_shared<AffineTrafo>();
            trafo_slave = std::make_shared<AffineTrafo>();
            affine_.trafo_master = trafo_master;
            affine_.trafo_slave = trafo_slave;

            warped_.invert_plane_dir = invert_plane_dir;
            affine_.invert_plane_dir = invert_plane_dir;
        }

        void clear() { projection_measure_ = 0.0; }

        bool assemble(MasterElem &master, SlaveElem &slave) {
            const bool is_affine = master.is_affine() && slave.is_affine();

            if (is_affine) {
                return assemble_affine(master, slave);
            } else {
                return assemble_warped(master, slave);
            }
        }

        Quadrature<T, Dim> &q_rule_affine() { return affine_.q_rule; }

        Quadrature<T, Dim> &q_rule_warped() { return warped_.q_rule; }

        void set_quadrature_affine(const Quadrature<T, Dim> &q_rule) { affine_.q_rule = q_rule; }

        void set_quadrature_affine(Quadrature<T, Dim> &&q_rule) { affine_.q_rule = std::move(q_rule); }

        void set_quadrature_warped(const Quadrature<T, Dim> &q_rule) { warped_.q_rule = q_rule; }

        void set_quadrature_warped(Quadrature<T, Dim> &&q_rule) { warped_.q_rule = std::move(q_rule); }

        bool assemble_affine(MasterElem &master, SlaveElem &slave) {
            assert(!affine_.q_rule.empty());

            make(master, affine_.master);
            make(slave, affine_.slave);

            make_transform(master, *trafo_master);
            make_transform(slave, *trafo_slave);

            if (affine_.compute()) {
                assemble(master, slave, affine_.q_master, affine_.q_slave, affine_.normal(), affine_.gap);

                return true;
            } else {
                return false;
            }
        }

        bool assemble_warped(MasterElem &master, SlaveElem &slave) {
            assert(!warped_.q_rule.empty());

            make(master, warped_.master);
            make(slave, warped_.slave);

            warped_.shape_master = make_shape(master);
            warped_.shape_slave = make_shape(slave);

            if (warped_.compute()) {
                assemble(master, slave, warped_.q_master, warped_.q_slave, warped_.normal(), warped_.gap);

                return true;
            } else {
                return false;
            }
        }

        void assemble(MasterElem &master,
                      SlaveElem &slave,
                      const Quadrature<T, Dim> &q_master,
                      const Quadrature<T, Dim> &q_slave,
                      const CoVector &normal,
                      const Storage<T> &gap) {
            master_fe_.init(master, q_master, true, false, false);
            slave_fe_.init(slave, q_slave, true, false, true);

            dual_fe_.init(slave_fe_);
            trafo_fe_.init(slave_fe_);

            assemble_mass_matrix(master_fe_, dual_fe_, coupling_mat_);
            assemble_mass_matrix(trafo_fe_, dual_fe_, mass_mat_);

            assemble_function(gap, dual_fe_, gap_vec_);
            assemble_constant_vector_function(normal, dual_fe_, normal_vec_);

            trafo_ = trafo_fe_.transformation();
            //!!! The global transformation needs the transpose of the basis transform
            make_transpose(trafo_);

            inverse_trafo_ = trafo_fe_.inverse_transformation();
            make_transpose(inverse_trafo_);

            projection_measure_ += mass_mat_.sum();
            assert(projection_measure_ > 0.0);
        }

        template <class QValues, class TestFE, class Vector>
        static void assemble_function(QValues &q_values, TestFE &test, Vector &vec) {
            Resize<Vector>::apply(vec, test.n_shape_functions());
            vec.zero();

            assert(static_cast<Integer>(test.n_shape_functions()) == static_cast<Integer>(vec.size()));
            assert(static_cast<Integer>(test.n_quad_points()) == static_cast<Integer>(q_values.size()));

            const int n_qps = test.n_quad_points();
            const int n_funs = test.n_shape_functions();

            assert(n_qps > 0);

            for (int i = 0; i < n_funs; ++i) {
                for (int k = 0; k < n_qps; ++k) {
                    vec[i] += test.fun(i, k) * q_values[k] * test.dx(k);
                }
            }
        }

        template <class TestFE, class Vector>
        static void assemble_constant_vector_function(const CoVector &v, TestFE &test, Vector &vec) {
            Resize<Vector>::apply(vec, test.n_shape_functions() * v.size());
            vec.zero();

            assert(static_cast<Integer>(test.n_shape_functions() * v.size()) == static_cast<Integer>(vec.size()));

            const int n_qps = test.n_quad_points();
            const int n_funs = test.n_shape_functions();
            const int dim = v.size();

            assert(n_qps > 0);

            for (int i = 0; i < n_funs; ++i) {
                const auto offset_i = i * dim;

                for (int k = 0; k < n_qps; ++k) {
                    for (int d = 0; d < dim; ++d) {
                        vec[offset_i + d] += test.fun(i, k) * v[d] * test.dx(k);
                    }
                }
            }
        }

        inline const Matrix<T, NSlaveNodes, NMasterNodes> &coupling_matrix() const { return coupling_mat_; }

        inline const Matrix<T, NSlaveNodes, NSlaveNodes> &mass_matrix() const { return mass_mat_; }

        inline const Matrix<T, NSlaveNodes, NSlaveNodes> &transformation() const { return trafo_; }

        inline const Matrix<T, NSlaveNodes, NSlaveNodes> &inverse_transformation() const { return inverse_trafo_; }

        inline const moonolith::Vector<T, NSlaveNodes> &gap_vector() const { return gap_vec_; }

        inline const NormalVec &normal_vector() const { return normal_vec_; }

        inline const T &projection_measure() const { return projection_measure_; }

        inline void describe(std::ostream &os) const {
            os << "projection_measure: " << projection_measure_ << std::endl;
        }

    private:
        AffineContact<T, PhysicalDim> affine_;
        WarpedContact<T, PhysicalDim> warped_;

        FE<MasterElem> master_fe_;
        FE<SlaveElem> slave_fe_;
        Dual<SlaveElem> dual_fe_;
        TransformedFE<SlaveElem> trafo_fe_;

        Matrix<T, NSlaveNodes, NMasterNodes> coupling_mat_;
        Matrix<T, NSlaveNodes, NSlaveNodes> mass_mat_;
        Matrix<T, NSlaveNodes, NSlaveNodes> trafo_;
        Matrix<T, NSlaveNodes, NSlaveNodes> inverse_trafo_;
        moonolith::Vector<T, NSlaveNodes> gap_vec_;
        NormalVec normal_vec_;
        T projection_measure_;

        // buffers
        std::shared_ptr<AffineTrafo> trafo_master, trafo_slave;
        std::shared_ptr<Shape> shape_master, shape_slave;
    };

}  // namespace moonolith

#endif  // MOONOLITH_CONTACT_HPP
