#include "moonolith_elem_dual.hpp"

namespace moonolith {

    // P elements
    template class Dual<Edge2<Real, 1>>;
    template class Dual<Edge2<Real, 2>>;

    template class Dual<Tri3<Real, 2>>;
    template class Dual<Tri3<Real, 3>>;

    template class Dual<Tet4<Real, 3>>;

    // Q elements
    template class Dual<Quad4<Real, 2>>;
    template class Dual<Quad4<Real, 3>>;

    template class Dual<Hex8<Real, 3>>;
}  // namespace moonolith
