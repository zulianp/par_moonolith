#ifndef MOONOLITH_BOUND_EXPAND_HPP	
#define MOONOLITH_BOUND_EXPAND_HPP

// #include "moonolith_mutator.hpp"
#include "moonolith_forward_declarations.hpp"

namespace moonolith {

	template <class Tree>
	class BoundExpand : public Mutator<typename Tree::Traits> {
	public:
		typedef typename Tree::Traits Traits;
		typedef typename Traits::DataType DataType;
		typedef typename Traits::Bound Bound;

		typedef moonolith::Node<Traits> Node;
		typedef moonolith::Branch<Traits> Branch;
		typedef moonolith::Root<Traits> Root;
		typedef moonolith::Leaf<Traits> Leaf;
		typedef moonolith::TreeMemory<Traits> TreeMemory;
		
		BoundExpand()
		: success_(false)
		{}

		///@brief expands the bound 
		///@returns true if the operation was successfully performed
		virtual bool expand(Bound &bound) = 0;
		virtual void mutate(Node &) const {}

		virtual NavigatorOption visit(Root &root, TreeMemory &) { 
			success_ = expand(root.bound());
			mutate(root);
			return CONTINUE;
		}

		inline NavigatorOption visit(Leaf &leaf, TreeMemory &) { 
			success_ = expand(leaf.bound());
			mutate(leaf);
			return CONTINUE;
		}

		inline NavigatorOption visit(Branch &branch, TreeMemory &) { 
			success_ = expand(branch.bound());
			mutate(branch);
			return CONTINUE;
		}

		virtual ~BoundExpand() {}

		inline bool success() const 
		{
			return success_;
		}

	private:
		bool success_;
	};
}

#endif //MOONOLITH_BOUND_EXPAND_HPP	
