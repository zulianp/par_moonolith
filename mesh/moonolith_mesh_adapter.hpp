#ifndef MOONOLITH_MESH_ADAPTER_HPP
#define MOONOLITH_MESH_ADAPTER_HPP

#include "moonolith_many_masters_one_slave_algorithm.hpp"
#include "moonolith_one_master_one_slave_algorithm.hpp"
#include "moonolith_single_collection_one_master_one_slave_algorithm.hpp"

#endif //MOONOLITH_MESH_ADAPTER_HPP
