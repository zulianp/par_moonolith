
#ifndef MOONOLITH_REDISTRIBUTE_HPP
#define MOONOLITH_REDISTRIBUTE_HPP

#include "moonolith_assign_functions.hpp"
#include "moonolith_communicator.hpp"
#include "moonolith_message_tag.hpp"
#include "moonolith_stream_utils.hpp"

#include <algorithm>

namespace moonolith {

    template <class SparseMatrix>
    class Redistribute {
    public:
        typedef typename SparseMatrix::EntryType EntryType;

        // Redistribute(const Communicator &comm) : comm_(comm.duplicate()) {}
        Redistribute(const Communicator &comm) : comm_(comm) {}

        Redistribute() : comm_() {}

        /// matrix has to have the global sizes
        template <class InsertMode>
        bool apply(SparseMatrix &matrix, const InsertMode &insert) {
            std::vector<Integer> offsets(comm_.size() + 1);

            const Integer rows = matrix.rows();
            assert(comm_.is_globally_exactly_equal(rows));

            const Integer per_proc = rows / comm_.size();
            const Integer remainder = rows % comm_.size();

            offsets[0] = 0;
            const Integer offsets_size = offsets.size();
            for (Integer i = 1; i < offsets_size; ++i) {
                offsets[i] = offsets[i - 1] + per_proc + (i <= remainder);
            }

            return apply(offsets, matrix, insert);
        }

        template <class InsertMode>
        bool apply_transpose(SparseMatrix &matrix, const InsertMode &insert) {
            SparseMatrix temp = matrix.local_transposed();
            if (!apply(matrix, insert)) return false;
            matrix = temp.local_transposed();
            return true;
        }

        bool apply(const std::vector<Integer> &offsets, SparseMatrix &matrix) {
            return apply(offsets, matrix, Assign<EntryType>());
        }

        Integer lower_bound(const std::vector<Integer> &array, const Integer value) {
            // FIXME use lower_bound
            const Integer n = static_cast<Integer>(array.size());
            for (Integer i = n - 1; i >= 0; --i) {
                if (array[i] <= value) {
                    return i;
                }
            }

            return -1;
        }

        template <class InsertMode>
        bool apply(const std::vector<Integer> &offsets, SparseMatrix &matrix, const InsertMode &insert) {
            if (comm_.is_alone()) return true;

            assert(!comm_.has_pending_requests());
            assert(std::is_sorted(offsets.begin(), offsets.end()));
            assert(offsets.size() == comm_.size() + 1);

            const Integer rank = comm_.rank();
            const Integer size = comm_.size();

            assert(offsets.size() - 1 == std::size_t(size));

            std::vector<ByteOutputBuffer> send_buffers(size);
            std::vector<ByteInputBuffer> recv_buffers(size);

            std::vector<Integer> rows_to_remove;

            // FIXME is proc_start = -1 the right value?
            Integer proc_start = -1, prev_row = -1;

            for (typename SparseMatrix::ConstIter it = matrix.iter(); it; ++it) {
                const Integer row = it.row();

                if (row != prev_row) {
                    proc_start = this->lower_bound(offsets, row);

                    if (proc_start == -1) {
                        logger() << "bound not found for " << row << std::endl;
                        logger() << "last offset == " << offsets[offsets.size() - 1] << std::endl;
                        continue;
                    }

                    if (offsets[proc_start + 1] == row) {
                        proc_start += 1;
                    }

                    if (proc_start == size)
                        logger() << "[" << rank << "]"
                                 << "->" << proc_start << " gets " << row << std::endl;

                    assert(proc_start < size);

                    prev_row = row;

                    if (proc_start != rank) rows_to_remove.push_back(row);
                }

                if (proc_start == rank) continue;

                assert(proc_start < size);
                assert(proc_start >= 0);

                const EntryType val = *it;
                const Integer col = it.col();

                assert(row < matrix.rows());
                assert(col < matrix.cols());
                assert(row >= 0);
                assert(col >= 0);

                assert(in_range_check(row, offsets[proc_start], offsets[proc_start + 1]));

                auto &sb = send_buffers[proc_start];

                sb << row;
                sb << col;
                sb << val;
            }

            const int comm_tag = MessageTag::next_unique_id();

            //const Integer n_incoming = 
	    comm_.i_send_recv_all(send_buffers, recv_buffers, comm_tag);

            matrix.remove_rows(rows_to_remove.begin(), rows_to_remove.end());

            comm_.wait_all();
            Integer bad_values = 0;
            for (Integer rank = 0; rank < size; ++rank) {
                if (recv_buffers[rank].empty()) continue;

                assert(rank != comm_.rank());
                // assert(rank >= 0);
                // assert(rank < comm_.size());

                // if (rank == comm_.rank()) {
                //     error_logger() << "[Error] message from same rank" << std::endl;
                //     i--;
                // }

                // assert(!recv_buffers[rank].empty());

                ByteInputBuffer &is(recv_buffers[rank]);

                // logger() << comm_ << " " << i << "/" << n_incoming << std::endl;

                const Integer this_begin = offsets[comm_.rank()];
                const Integer this_end = offsets[comm_.rank() + 1];

                while (is.good()) {
                    EntryType val;
                    Integer row, col;
                    is >> row;
                    assert(is.good());
                    is >> col;
                    assert(is.good());
                    is >> val;

                    assert(row < matrix.rows());
                    assert(col < matrix.cols());
                    assert(row >= 0);
                    assert(col >= 0);

                    assert(in_range_check(row, this_begin, this_end));

                    // FIXME highly inneficient (find cause and remove this)
                    if (!in_range(row, this_begin, this_end) || col >= matrix.cols() || col < 0) {
                        ++bad_values;
                    } else {
                        EntryType &val_ref = matrix.at(row, col);
                        insert(val_ref, val);
                    }
                }
            }

            // For some reason this code is buggy non deterministically
            // for (Integer i = 0; i < n_incoming; ++i) {
            //     Integer rank, index;
            //     while (!comm_.test_recv_any(&rank, &index)) {
            //     }

            //     assert(rank != comm_.rank());
            //     assert(rank >= 0);
            //     assert(rank < comm_.size());

            //     if (rank == comm_.rank()) {
            //         error_logger() << "[Error] message from same rank" << std::endl;
            //         i--;
            //     }

            //     assert(!recv_buffers[rank].empty());

            //     ByteInputBuffer &is(recv_buffers[rank]);

            //     // logger() << comm_ << " " << i << "/" << n_incoming << std::endl;

            //     const Integer this_begin = offsets[comm_.rank()];
            //     const Integer this_end = offsets[comm_.rank() + 1];

            //     while (is.good()) {
            //         EntryType val;
            //         Integer row, col;
            //         is >> row;
            //         assert(is.good());
            //         is >> col;
            //         assert(is.good());
            //         is >> val;

            //         assert(row < matrix.rows());
            //         assert(col < matrix.cols());
            //         assert(row >= 0);
            //         assert(col >= 0);

            //         assert(in_range_check(row, this_begin, this_end));

            //         // FIXME highly inneficient (find cause and remove this)
            //         if (!in_range(row, this_begin, this_end) || col >= matrix.cols() || col < 0) {
            //             ++bad_values;
            //         } else {
            //             EntryType &val_ref = matrix.at(row, col);
            //             insert(val_ref, val);
            //         }
            //     }
            // }

            // logger() << comm_ << " HERE " << std::endl;
            comm_.clear();

            if (bad_values) {
                error_logger() << comm_ << "Redistribute::apply(...) Bad values found. N = " << bad_values << std::endl;
            }

            return true;
        }

        bool in_range_check(const Integer &idx, const Integer &begin, const Integer &end) const {
            if (in_range(idx, begin, end)) {
                return true;
            }

            error_logger() << idx << " not in range [" << begin << ", " << end << ")" << std::endl;
            assert(idx >= begin);
            assert(idx < end);
            return false;
        }

        bool in_range(const Integer &idx, const Integer &begin, const Integer &end) const {
            if (idx >= begin && idx < end) {
                return true;
            }

            return false;
        }

        template <class InsertMode>
        bool apply_transpose(const std::vector<Integer> &offsets, SparseMatrix &matrix, const InsertMode &insert) {
            if (comm_.is_alone()) return true;

            SparseMatrix temp = matrix.local_transposed();
            if (!apply(offsets, temp, insert)) return false;

            matrix = temp.local_transposed();
            return true;
        }

    private:
        Communicator comm_;
    };

}  // namespace moonolith

#endif  // MOONOLITH_REDISTRIBUTE_HPP
