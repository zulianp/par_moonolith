#ifndef MOONOLITH_ISO_PARAMETRIC_TRANSFORM_HPP
#define MOONOLITH_ISO_PARAMETRIC_TRANSFORM_HPP

#include "moonolith_affine_transform.hpp"
#include "moonolith_transform.hpp"
#include "par_moonolith_config.hpp"

#include <iostream>
#include <memory>

namespace moonolith {

    template <class Elem>
    class IsoParametricTransform : public Transform<typename Elem::T, Elem::Dim, Elem::PhysicalDim> {
    public:
        using T = typename Elem::T;
        static const int Dim = Elem::Dim;
        static const int PhysicalDim = Elem::PhysicalDim;

        using super = moonolith::Transform<T, Dim, PhysicalDim>;
        using Vector = typename super::Vector;
        using CoVector = typename super::CoVector;
        using CoPoint = typename super::CoPoint;
        using Point = typename super::Point;

        virtual ~IsoParametricTransform() {}

        inline bool apply(const Point &in, CoPoint &out) override {
            elem_.point(in, out);
            return true;
        }

        inline void affine_apply_inverse(const CoPoint &in, Point &out) {
            mat_vec_mul<T, Dim, PhysicalDim>(J_inv, in, out);
            out += b_inv;
        }

        inline bool apply_inverse(const CoPoint &in, Point &out) override {
            affine_apply_inverse(in, out);

            if (elem_.is_affine()) {
                return true;
            }

            project(out);

            elem_.point(out, f_in);
            g = in - f_in;
            auto len_g = length(g);

            if (len_g < tol_) {
                return true;
            }

            auto diff = 1e6;
            for (Integer i = 0; i < max_it_; ++i) {
                elem_.jacobian(out, J);
                p_inv.apply(J, J_inv);

                mat_vec_mul<T, Dim, PhysicalDim>(J_inv, g, h);
                out += h;

                elem_.point(out, f_in);

                g = in - f_in;

                if (Dim != PhysicalDim) {
                    diff = length(h);
                } else {
                    diff = length(g);
                }

                if (verbose_) {
                    std::cout << i << "] " << diff << "\n";
                    std::cout << "\tg = ";
                    print(g);

                    std::cout << "\tx = ";
                    print(out);
                }

                if (diff < tol_) {
                    return true;
                }

                if (i >= max_it_) {
                    break;
                }
            }

            return diff < accept_tol_;
        }

        IsoParametricTransform(Elem &elem,
                               const T tol = GeometricTol<T>::value(),
                               const Integer max_it = 10000,
                               const T accept_tol = GeometricTol<T>::value_for_inv_transform(),
                               const bool verbose = false)
            : elem_(elem), tol_(tol), accept_tol_(accept_tol), max_it_(max_it), verbose_(verbose) {
            elem_.affine_approx_jacobian(J);
            p_inv.apply(J, J_inv);

            b = elem_.node(0);

            mat_vec_mul<T, Dim, PhysicalDim>(J_inv, b, b_inv);
            b_inv *= -1.0;

            lb.set(0.0);
            ub.set(1.0);
        }

        inline Vector &lower_bound() { return lb; }
        inline Vector &upper_bound() { return ub; }

        inline T estimate_condition_number() const override {
            // FIXME
            return 1.0;
        }

    private:
        Elem &elem_;
        T tol_, accept_tol_;
        Integer max_it_;
        bool verbose_;

        // buffers
        std::array<T, PhysicalDim * Dim> J;
        std::array<T, Dim * PhysicalDim> J_inv;
        CoVector g;
        CoVector f_in;
        Vector h;
        Vector lb, ub;

        PseudoInvert<T, PhysicalDim, Dim> p_inv;

        CoVector b;
        Vector b_inv;

        inline void project(Vector &x) const {
            for (int i = 0; i < Dim; ++i) {
                x = std::min(x[i], ub[i]);
                x = std::max(x[i], lb[i]);
            }
        }
    };

    template <class Elem>
    inline void make_transform(Elem &elem,
                               std::shared_ptr<Transform<typename Elem::T, Elem::Dim, Elem::PhysicalDim>> &trafo) {
        if (elem.is_affine()) {
            auto affine_trafo = std::make_shared<AffineTransform<typename Elem::T, Elem::Dim, Elem::PhysicalDim>>();
            make_transform(elem, *affine_trafo);

            trafo = affine_trafo;
        } else {
            trafo = std::make_shared<IsoParametricTransform<Elem>>(elem);
        }
    }
}  // namespace moonolith

#endif  // MOONOLITH_ISO_PARAMETRIC_TRANSFORM_HPP
