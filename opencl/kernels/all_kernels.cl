#ifndef ALL_KERNELS_CL
#define ALL_KERNELS_CL 

#include "all_interfaces.cl"

#include "algebra/algebra_implementation.cl"
#include "fe/fe_implementation.cl"
#include "assembly/assemble_implementation.cl"
#include "mortar/mortar_implementation.cl"
#include "elasticity/elasticity_implementation.cl"

#include "mortar_surface/mortar_surface.cl"
#include "mortar_volume_surface/mortar_volume_surface.cl"

#endif //ALL_KERNELS_CL
