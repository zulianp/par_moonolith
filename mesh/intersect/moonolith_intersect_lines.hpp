#ifndef MOONOLITH_INTERSECT_LINES_HPP
#define MOONOLITH_INTERSECT_LINES_HPP

#include "moonolith_line.hpp"


namespace moonolith {

	template<typename T, int Dim>
	class IntersectLines {
	public:
		using Line = moonolith::Line<T, Dim>;
		using Point = typename Line::Point;
		using Vector = typename Line::Point;

		///@brief only colliner lines are considered valid
		bool apply(const Line &line1, const Line &line2, Line &result);

		///@brief also considers lower dimensional intersections (i.e., points)
		bool apply(const Line &line1, const Line &line2, Storage<Point> &line_or_point);

	private:
		Vector u, v, w;
	};
}

#endif //MOONOLITH_INTERSECT_LINES_HPP
