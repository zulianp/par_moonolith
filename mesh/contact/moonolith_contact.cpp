#include "moonolith_contact.hpp"

namespace moonolith {
    template class AffineContact<Real, 2>;
    template class AffineContact<Real, 3>;

    template class WarpedContact<Real, 2>;
    template class WarpedContact<Real, 3>;
}  // namespace moonolith
