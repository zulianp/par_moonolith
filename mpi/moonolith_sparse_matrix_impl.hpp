#include "moonolith_sparse_matrix.hpp"

#include <cmath>
#include <limits>
#include <map>

namespace moonolith {

    template <typename EntryType>
    bool SparseMatrix<EntryType>::pedantic_check(const Integer i, const Integer j) const {
        if (i >= rows() && rows() != 0) {
            error_logger() << i << " " << j << std::endl;
            error_logger() << "size: " << rows() << " " << cols() << std::endl;

            auto it = data_.find(IntPair(i, j));
            if (it != data_.end()) {
                error_logger() << it->second << std::endl;
            }

            error_logger() << std::flush;
            return false;
        }

        return true;
    }

    template <typename EntryType>
    bool SparseMatrix<EntryType>::is_valid() const {
        Integer row_check = rows();
        Integer col_check = cols();

        row_check = row_check == 0 ? (std::numeric_limits<Integer>::max() / 2) : row_check;
        col_check = col_check == 0 ? (std::numeric_limits<Integer>::max() / 2) : col_check;

        bool valid = true;

        for (ConstMapIter vIt = data_.begin(); vIt != data_.end(); ++vIt) {
            const Integer i = vIt->first.first;
            const Integer j = vIt->first.second;

            if (i >= row_check || j >= col_check) {
                error_logger() << i << " " << j << std::endl;
                error_logger() << "size: " << rows() << " " << cols() << std::endl;
                error_logger() << std::flush;
                valid = false;
            }
        }

        return valid;
    }

}  // namespace moonolith