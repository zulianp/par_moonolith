#ifndef MOONOLITH_EMPIRICAL_TOL_HPP
#define MOONOLITH_EMPIRICAL_TOL_HPP

#include "moonolith_base.hpp"

namespace moonolith {

    template <typename T>
    class GeometricTol {};

    template <typename T>
    class StandardTol {};

    template <typename T>
    class CheckTol {};

    template <>
    class GeometricTol<double> {
    public:
        static constexpr double value() { return 1e-10; }
        static constexpr double value_for_inv_transform() { return 1e-6; }
    };

    template <>
    class StandardTol<double> {
    public:
        static constexpr double value() { return 1e-16; }
    };

    template <>
    class CheckTol<double> {
    public:
        static constexpr double half_value() { return 1e-8; }
        static constexpr double quarter_value() { return 1e-4; }
    };

    template <>
    class GeometricTol<float> {
    public:
        static constexpr float value() { return 1e-6f; }
        static constexpr double value_for_inv_transform() { return 1e-4f; }
    };

    template <>
    class StandardTol<float> {
    public:
        static constexpr float value() { return 1e-8f; }
    };

    template <>
    class CheckTol<float> {
    public:
        static constexpr float half_value() { return 1e-4f; }
        static constexpr float quarter_value() { return 1e-2f; }
    };

#ifdef MOONOLITH_HAVE_QUAD_PRECISION
    template <>
    class GeometricTol<long double> {
    public:
        static constexpr long double value() { return 1e-14; }
        static constexpr double value_for_inv_transform() { return 1e-9; }
    };

    template <>
    class StandardTol<long double> {
    public:
        static constexpr double value() { return 1e-24; }
    };

    template <>
    class CheckTol<long double> {
    public:
        static constexpr long double half_value() { return 1e-10; }
        static constexpr long double quarter_value() { return 1e-5; }
    };

#endif

}  // namespace moonolith

#endif  // MOONOLITH_EMPIRICAL_TOL_HPP
