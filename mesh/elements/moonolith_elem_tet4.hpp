#ifndef MOONOLITH_ELEM_TET4_HPP
#define MOONOLITH_ELEM_TET4_HPP

#include "moonolith_elem_tet.hpp"

namespace moonolith {

    template <typename T_, int PhysicalDim_ = 3>
    class Tet4 final : public Tet<T_, 1, PhysicalDim_> {
    public:
        static const int Order = 1;
        static const int Dim = 3;
        static const int PhysicalDim = PhysicalDim_;
        static const int NNodes = 4;

        using T = T_;

        using Vector = moonolith::Vector<T, Dim>;
        using CoVector = moonolith::Vector<T, PhysicalDim>;

        using Point = moonolith::Vector<T, Dim>;
        using CoPoint = moonolith::Vector<T, PhysicalDim>;

        using Super = moonolith::Tet<T_, 1, PhysicalDim_>;
        using Super::make;

    private:
        class Grad final {
        public:
            Grad() {
                f[0] = [](const Point &, Vector &g) {
                    g.x = -1.0;
                    g.y = -1.0;
                    g.z = -1.0;
                };

                f[1] = [](const Point &, Vector &g) {
                    g.x = 1;
                    g.y = 0.0;
                    g.z = 0.0;
                };

                f[2] = [](const Point &, Vector &g) {
                    g.x = 0.0;
                    g.y = 1.0;
                    g.z = 0.0;
                };

                f[3] = [](const Point &, Vector &g) {
                    g.x = 0.0;
                    g.y = 0.0;
                    g.z = 1.0;
                };
            }

            std::array<std::function<void(const Point &, Vector &)>, 4> f;
        };

        class Fun final {
        public:
            Fun() {
                f[0] = [](const Point &p) -> T { return 1.0 - p.x - p.y - p.z; };

                f[1] = [](const Point &p) -> T { return p.x; };

                f[2] = [](const Point &p) -> T { return p.y; };

                f[3] = [](const Point &p) -> T { return p.z; };
            }

            std::array<std::function<T(const Point &)>, 4> f;
        };

    public:
        T fun(const Integer i, const Point &p) const override { return fun_.f[i](p); }

        void grad(const Integer i, const Point &p, Vector &g) const override { grad_.f[i](p, g); }

        void hessian(const Integer, const Point &, std::array<T, Dim * Dim> &H) {
            std::fill(std::begin(H), std::end(H), 0.0);
        }

        void jacobian(const Point &p, std::array<T, PhysicalDim * Dim> &J) const override {
            (void)p;
            affine_approx_jacobian(J);
        }

        void affine_approx_jacobian(std::array<T, PhysicalDim * Dim> &J) const override {
            Affine<Tet4>::jacobian(*this, J);
        }

        CoPoint &node(const Integer i) override { return nodes_[i]; }

        const CoPoint &node(const Integer i) const override { return nodes_[i]; }

        void point(const Point &p, CoPoint &q) const override { IsoParametric<Tet4>::point(*this, p, q); }

        Tet4() {}

        bool is_affine() const override { return true; }
        inline bool is_simplex() const override { return true; }

        inline int n_nodes() const override { return NNodes; }
        inline int order() const override { return Order; }

        const std::array<CoPoint, NNodes> &nodes() const { return nodes_; }

        inline void make_reference() { Reference<Tet4>::points(nodes_); }

        inline ElemType type() const override { return TET4; }

        inline T measure() const override { return tetrahedron_volume(nodes_[0], nodes_[1], nodes_[2], nodes_[3]); }

        inline T reference_measure() const override { return 1.0 / 6.0; }

        inline void make(Polyhedron<T> &poly) const override {
            ConvexDecomposition<T, PhysicalDim>::make_tet(nodes(), 0, 1, 2, 3, poly);
        }

    private:
        std::array<CoPoint, 4> nodes_;
        const Fun fun_;
        const Grad grad_;
    };

    template <typename T, int PhysicalDim>
    class Reference<Tet4<T, PhysicalDim>> {
    public:
        using Point = typename Tet4<T, PhysicalDim>::Point;
        static const int NNodes = 4;

        template <std::size_t Size>
        static void points(std::array<Point, Size> &nodes) {
            static_assert(Size >= NNodes, "size must be at least 4");
            // p0
            nodes[0].x = 0.0;
            nodes[0].y = 0.0;
            nodes[0].z = 0.0;

            // p1
            nodes[1].x = 1.0;
            nodes[1].y = 0.0;
            nodes[1].z = 0.0;

            // p2
            nodes[2].x = 0.0;
            nodes[2].y = 1.0;
            nodes[2].z = 0.0;

            // p3
            nodes[3].x = 0.0;
            nodes[3].y = 0.0;
            nodes[3].z = 1.0;
        }

        static T measure() { return 1. / 6.; }
    };

    template <typename T, int PhysicalDim>
    class GaussQRule<Tet4<T, PhysicalDim>> {
    public:
        static bool get(const Integer order, Quadrature3<T> &q) { return Gauss::get(order, q); }
    };

}  // namespace moonolith

#endif  // MOONOLITH_ELEM_TET4_HPP
