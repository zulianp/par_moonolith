
#include "moonolith_base.hpp"
#include "moonolith_elem_dual.hpp"
#include "moonolith_elem_dual_partial.hpp"
#include "moonolith_gauss_quadrature_rule.hpp"
#include "moonolith_map_quadrature.hpp"
#include "moonolith_polygon.hpp"
#include "moonolith_stream_utils.hpp"
#include "par_moonolith_instance.hpp"

#include "moonolith_l2_assembler_no_covering.hpp"
#include "moonolith_par_l2_assembler_no_covering.hpp"

#include "gtest/gtest.h"

using namespace moonolith;

template <typename T, int N>
Matrix<T, N, N> el_transfer_matrix(const Matrix<T, N, N> &mass_matrix, const Matrix<T, N, N> &coupling_matrix) {
    Vector<Real, N> mass_vec;
    mass_matrix.sum_cols(mass_vec);
    auto inv_mass_vec = reciprocal(mass_vec);
    auto D_inv = diag(inv_mass_vec);
    return D_inv * coupling_matrix;
}

TEST(ExtendedDualTest, ParL2TransferNoCovering) {
    Communicator comm;
    ParL2TransferNoCovering<Real, 2> assembler(comm);
}

TEST(ExtendedDualTest, CompareAssemblers) {
    static const int Dim = 2;
    static const int NNodes = 3;
    using ElemT = moonolith::Tri3<Real, Dim>;
    using Quadrature = moonolith::Quadrature<Real, Dim>;
    using QArray = moonolith::Storage<Quadrature>;
    using FET = moonolith::FE<ElemT>;
    using DualFET = moonolith::Dual<ElemT>;
    using Transform = moonolith::AffineTransform<Real, 2>;
    using MatrixT = moonolith::Matrix<Real, NNodes, NNodes>;

    //////////////////////////////////////////////////////////////////////////////
    // Create element and cut data

    ElemT slave_e, master_e;
    slave_e.node(0) = {0.0, 0.0};
    slave_e.node(1) = {1.0, 0.0};
    slave_e.node(2) = {0.0, 1.0};

    master_e.node(0) = {0.0, 0.0};
    master_e.node(1) = {0.5, 0.0};
    master_e.node(2) = {0.0, 0.2};

    Polygon<Real, 2> doi;
    make(master_e, doi);

    Quadrature q_rule;
    Gauss::get(2, q_rule);

    Quadrature q_doi;
    map(q_rule,
        // rescale by inverse volume of e
        static_cast<Real>(1.),
        doi,
        q_doi);

    // Compute weights from cut quadrature

    QArray q_slave;
    q_slave.resize(1);
    Quadrature q_master;

    Transform trafo_slave, trafo_master;
    make_transform(slave_e, trafo_slave);
    make_transform(master_e, trafo_master);

    const Integer n_qp = q_doi.n_points();

    q_master.resize(n_qp);
    q_slave[0].resize(n_qp);

    const Real slave_measure = slave_e.measure();
    const Real master_measure = master_e.measure();

    for (Integer k = 0; k < n_qp; ++k) {
        trafo_master.apply_inverse(q_doi.point(k), q_master.point(k));
        trafo_slave.apply_inverse(q_doi.point(k), q_slave[0].point(k));

        q_master.weights[k] = q_doi.weights[k] / master_measure;
        q_slave[0].weights[k] = q_doi.weights[k] / slave_measure;
    }

    //////////////////////////////////////////////////////////////////////////////
    /////////// Biorthognal basis functions constructed using intersection ///////

    MatrixT w;
    DualWeightsPartial<ElemT>().weights(slave_e, q_slave, w);

    FET slave_fe;
    slave_fe.init(slave_e, q_slave[0], true, false, true);

    FET master_fe;
    master_fe.init(master_e, q_master, true, false, false);

    DualFET multiplier_fe;
    multiplier_fe.init(slave_fe, w);

    MatrixT mass_matrix;
    MatrixT coupling_matrix;

    mass_matrix.zero();
    assemble_mass_matrix(slave_fe, multiplier_fe, mass_matrix);

    coupling_matrix.zero();
    assemble_mass_matrix(master_fe, multiplier_fe, coupling_matrix);

    auto T = el_transfer_matrix(mass_matrix, coupling_matrix);

    MOONOLITH_UNUSED(T);  // FIXME

    // logger() << "---------------------\n";
    // T.describe(logger());
    // logger() << "---------------------\n";

    //////////////////////////////////////////////////////////////////////////////
    /////////// Usuall biorthognal basis functions, with row normalization ///////

    DualFET multiplier_fe_std;
    multiplier_fe_std.init(slave_fe);

    mass_matrix.zero();
    assemble_mass_matrix(slave_fe, multiplier_fe_std, mass_matrix);

    coupling_matrix.zero();
    assemble_mass_matrix(master_fe, multiplier_fe_std, coupling_matrix);

    auto T2 = el_transfer_matrix(mass_matrix, coupling_matrix);

    MOONOLITH_UNUSED(T2);  // FIXME

    // logger() << "---------------------\n";
    // T2.describe(logger());
    // logger() << "---------------------\n";

    //////////////////////////////////////////////////////////////////////////////

    // Moonolith::Abort();
}
