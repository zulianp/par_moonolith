#ifndef MOONOLITH_PROJECT_POLYGONS_IMPL_HPP
#define MOONOLITH_PROJECT_POLYGONS_IMPL_HPP

#include "moonolith_geo_algebra.hpp"
#include "moonolith_profiler.hpp"
#include "moonolith_project_points.hpp"
#include "moonolith_project_polygons.hpp"

namespace moonolith {

    template <typename T>
    bool ProjectPolygons<T>::project_intersect_and_map_quadrature(const Polygon3 &poly1,
                                                                  const Polygon3 &poly2,
                                                                  const Plane3 &plane,
                                                                  // ref-quad-rule
                                                                  const Quadrature2<T> &ref_q,
                                                                  Quadrature3<T> &plane_q) {
        intersection_ratio_1 = 0.;
        intersection_ratio_2 = 0.;

        if (plane.n.z == -1.0) {
            trafo.identity();
        } else {
            np1 = plane.n;
            np1.z += 1.;
            auto len = length(np1);
            np1 /= len;
            trafo.init(np1);
        }

        project_points(trafo, plane.p, poly1.points, projected_1.points);
        project_points(trafo, plane.p, poly2.points, projected_2.points);

        auto area_1 = projected_1.area();
        auto area_2 = projected_2.area();

        if (approxeq(area_1, static_cast<T>(0.), tol_) || approxeq(area_2, static_cast<T>(0.), tol_)) {
            return false;
        }

        if (area_1 < 0) {
            projected_1.reverse();
            area_1 = -area_1;
        }

        if (area_2 < 0) {
            projected_2.reverse();
            area_2 = -area_2;
        }

        // const Integer n_points_1 = poly1.points.size();
        // const Integer n_points_2 = poly2.points.size();

        result_2d.clear();

        if (!isect_polygons.apply(projected_1, projected_2, result_2d)) {
            return false;
        }

        const Integer n_islands = result_2d.size();
        Integer actual_n_islands = 0;

        triangulations.resize(n_islands);

        T scale = 0.;

        for (Integer k = 0; k < n_islands; ++k) {
            // result_2d[k].remove_collinear_points(1e-8);

            const T area = result_2d[k].area();
            if (!approxeq(area, static_cast<T>(0.), 10 * tol_)) {
                triangulate_poly.apply(result_2d[k], triangulations[k]);
                actual_n_islands++;
                scale += area;
            } else {
                result_2d[k].clear();
                triangulations[k].clear();
            }
        }

        if (actual_n_islands == 0) return false;

        composite_q_points_2d.clear();

        intersection_ratio_1 = scale / area_1;
        intersection_ratio_2 = scale / area_2;

        // if(intersection_ratio_1 < 0.001 || intersection_ratio_2 < 0.001) {
        // 	return false;
        // }

        map_quadrature_rule(
            ref_q.points, ref_q.weights, 1. / scale, result_2d, triangulations, composite_q_points_2d, plane_q.weights);

        // auto n_qps = composite_q_points_2d.size();
        unproject_points(trafo, plane.p, composite_q_points_2d, plane_q.points);
        return true;
    }

    template <typename T>
    void ProjectPolygons<T>::map_quadrature_rule(const Storage<Vector2> &q_points,
                                                 const Storage<T> &q_weights,
                                                 const T weight,
                                                 const Storage<Polygon2> &domain_of_integration,
                                                 const Storage<Storage<int>> &triangulations,
                                                 Storage<Vector2> &composite_q_points,
                                                 Storage<T> &composite_q_weights) {
        std::size_t n_qps_rule = q_points.size();
        std::size_t n_qps = 0;
        std::size_t n_islands = domain_of_integration.size();

        for (std::size_t k = 0; k < n_islands; ++k) {
            n_qps += triangulations[k].size() / 3 * n_qps_rule;
        }

        composite_q_points.resize(n_qps);
        composite_q_weights.resize(n_qps);

        triangle.points.resize(3);
        triangle.points[0] = {0., 0.};

        std::size_t qp = 0;
        for (std::size_t k = 0; k < n_islands; ++k) {
            const auto &tri = triangulations[k];
            const auto &points = domain_of_integration[k].points;

            const auto n_triangles = tri.size() / 3;

            for (std::size_t i = 0; i < n_triangles; ++i) {
                const auto i3 = i * 3;
                const auto &o = points[tri[i3]];

                u = points[tri[i3 + 1]] - o;
                v = points[tri[i3 + 2]] - o;

                triangle.points[1] = u;
                triangle.points[2] = v;

                auto scale = std::abs(triangle.area()) * weight;
                assert(scale > 0.);

                for (std::size_t j = 0; j < n_qps_rule; ++j) {
                    composite_q_points[qp] = o;
                    composite_q_points[qp] += q_points[j].x * u;
                    composite_q_points[qp] += q_points[j].y * v;

                    composite_q_weights[qp] = q_weights[j] * scale;
                    ++qp;
                }
            }
        }
    }

}  // namespace moonolith

#endif  // MOONOLITH_PROJECT_POLYGONS_IMPL_HPP
