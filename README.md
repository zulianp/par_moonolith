[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)
<!-- [![Build Status](https://travis-ci.com/zulianp/par_moonolith.svg?branch=master)](https://travis-ci.com/bitbucket/zulianp/par_moonolith) -->
[![Build status](https://ci.appveyor.com/api/projects/status/x8ddkg5jw11dv9a8/branch/master?svg=true)](https://ci.appveyor.com/project/zulianp/par-moonolith/branch/master)
[![CodeFactor](https://www.codefactor.io/repository/bitbucket/zulianp/par_moonolith/badge)](https://www.codefactor.io/repository/bitbucket/zulianp/par_moonolith)

# README #

Library for parallel intersection detection and automatic load-balancing developed at ICS (Institute of Computational Science, Universita della Svizzera italiana).

# Main Developer
Patrick Zulian ([USI/ICS](https://www.ics.usi.ch))

# Contributors #
Andreas Fink ([CSCS](https://www.cscs.ch))

# Paper:
http://epubs.siam.org/doi/abs/10.1137/15M1008361

# License

The software is realized with NO WARRANTY and it is licenzed under [BSD 3-Clause license](https://bitbucket.org/zulianp/par_moonolith/src/master/LICENSE.md).

For any derived work or outcome using this library, please cite the following paper [http://epubs.siam.org/doi/abs/10.1137/15M1008361.](http://epubs.siam.org/doi/abs/10.1137/15M1008361.)

# Installation

It requires mpi (e.g., openmpi or mpich) installed on your system. 

After downloading par_moonolith:

```
cd par_moonolith
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=<your moonolith installation directory> -DCMAKE_BUILD_TYPE=Release
make
make install
```

## CMake users
Once installed, the  moonolith library can be included in your project with the following cmake commands
```
find_package(ParMoonolith 0 REQUIRED)
add_executable(my_main my_main.cpp)

# import all the dependencies and targets
target_link_libraries(my_main ParMoonolith::par_moonolith)
```

and configure with

```
cmake .. -DParMoonolith_DIR=<your moonolith installation directory>/lib/cmake
```

## Makefile users
For users with **Makefile** projects use `make install_all` instead of `make install`. This command will install also the `moonolith-config.makefile` configuration file in your installation folder (i.e, <your moonolith installation directory>/config)

See the `examples` folder for full **Makefile** and **CMake** examples.

## Testing installation

For running unit tests and benchmarks configure with
```
cmake .. -DMOONOLITH_ENABLE_TESTING=ON -DMOONOLITH_ENABLE_BENCHMARK=ON
```

Then
```
make test
```

For checking that the installation is ok
```
make test_install
```

# Examples

For running the examples

```
make moonolith_examples

# list the examples
ls examples

# run for instance
mpiexec -np <number of processes> ./examples/example_variational_transfer 
```

# Wiki

For more information check out the [Wiki Page](https://bitbucket.org/zulianp/par_moonolith/wiki/Home)!

# Citing ParMOONoLith

Citing the paper

```bibtex
@article{zulian:2016,
	author = {Krause, R. and Zulian, P.},
	title = {A Parallel Approach to the Variational Transfer of Discrete Fields between Arbitrarily Distributed Unstructured Finite Element Meshes},
	journal = {SIAM Journal on Scientific Computing},
	volume = {38},
	number = {3},
	pages = {C307-C333},
	year = {2016},
	doi = {10.1137/15M1008361},
	url = {https://doi.org/10.1137/15M1008361}
}
```

Citing this repository
```bibtex
@misc{moonolithgit,
	author = {Patrick Zulian},
	title = {{P}ar{MOON}o{L}ith: parallel intersection detection and automatic load-balancing library. {G}it repository},
	url = {https://bitbucket.org/zulianp/par\_moonolith},
	howpublished = {https://bitbucket.org/zulianp/par\_moonolith},
	year = {2016}
}
```

