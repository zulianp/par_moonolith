#ifndef DEFORMATION_CL
#define DEFORMATION_CL 

#include "fe/fe_interfaces.cl"
#include "deformation_interface.cl"

void make_deformation(const SizeType dims, 
					  const FEFunction *displacement_gradient,
					  Deformation *deformation)
{
	make_def_grad(dims, displacement_gradient->grad, deformation->grad);

	const Scalar det = det_n(dims, deformation->grad);


	inverse_n(dims, deformation->grad, det, deformation->inverse_grad);
	deformation->grad_determinant = det;

	mat_times_self_transposed(dims, deformation->grad, deformation->cauchy_strain);
	transpose(dims, deformation->inverse_grad, deformation->inverse_grad_transposed);
}

void make_def_grad(const SizeType dim, const Scalar *displacement_gradient, Scalar * deformation)
{
	for(SizeType i = 0; i < dim; ++i) {
		const SizeType offset = i*dim;
		for(SizeType j = 0; j < dim; ++j) {
			const SizeType offset_p_j = offset+j;
			deformation[offset_p_j] = displacement_gradient[offset_p_j] + (Scalar)(i == j);
		}
	}
}

void deformation_print(const SizeType n_dims, Deformation *def)
{
	printf("grad_determinant: %g\n", def->grad_determinant);
	printf("grad\n");
	matrix_print(n_dims, n_dims, def->grad);

	printf("inverse_grad\n");
	matrix_print(n_dims, n_dims, def->inverse_grad);

	printf("inverse_grad_transposed\n");
	matrix_print(n_dims, n_dims, def->inverse_grad_transposed);

	printf("cauchy_strain\n");
	matrix_print(n_dims, n_dims, def->cauchy_strain);
}

#endif //DEFORMATION_CL
