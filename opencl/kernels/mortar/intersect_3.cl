#ifndef INTERSECT_3_CL
#define INTERSECT_3_CL

#include "intersect_interface.cl"
#include "kernel_base.cl"

#define NOT_INTERSECTED 0
#define INTERSECTED 1
#define CONTAINED_USE_ORIGINAL 2
#define ERROR_IN_ALGORITHM -3

#define INSIDE 1
#define ON_PLANE 2
#define OUTSIDE 0

#define TO_BE_REMOVED INVALID_INDEX
#define TO_BE_INTERSECTED -2
#define OK 0

static Scalar tetrahedron_volume(const Vector3 p1, const Vector3 p2, const Vector3 p3, const Vector3 p4) {
    return dot(p4 - p1, cross(p2 - p1, p3 - p1)) / 6.0;
}

static Scalar m_tetrahedron_volume(const Scalar *tet) {
    return tetrahedron_volume(vec_3(tet[0], tet[1], tet[2]),
                              vec_3(tet[3], tet[4], tet[5]),
                              vec_3(tet[6], tet[7], tet[8]),
                              vec_3(tet[9], tet[10], tet[11]));
}

static Scalar p_mesh_volume_3(const PMesh surf_mesh) {
    if (surf_mesh.n_nodes == 4) {
        const Vector3 p1 = vec_3(surf_mesh.points[0], surf_mesh.points[1], surf_mesh.points[2]);
        const Vector3 p2 = vec_3(surf_mesh.points[3], surf_mesh.points[4], surf_mesh.points[5]);
        const Vector3 p3 = vec_3(surf_mesh.points[6], surf_mesh.points[7], surf_mesh.points[8]);
        const Vector3 p4 = vec_3(surf_mesh.points[9], surf_mesh.points[10], surf_mesh.points[11]);
        return fabs(tetrahedron_volume(p1, p2, p3, p4));
    }

    Vector3 b = vec_3(0.0, 0.0, 0.0);
    Vector3 p1, p2, p3;

    for (SizeType i = 0; i < surf_mesh.n_nodes; ++i) {
        const SizeType offset_3 = i * 3;
        b += vec_3(surf_mesh.points[offset_3], surf_mesh.points[offset_3 + 1], surf_mesh.points[offset_3 + 2]);
    }

    b /= surf_mesh.n_nodes;

    Scalar result = 0;
    for (SizeType i = 0; i < surf_mesh.n_elements; ++i) {
        const SizeType begin = surf_mesh.el_ptr[i];
        const SizeType end = surf_mesh.el_ptr[i + 1];

        const SizeType n_triangles = end - begin - 2;

        KERNEL_ASSERT((end - begin) >= 3, "compute_normals: (end - begin) >= 3");

        const SizeType v1 = surf_mesh.el_index[begin] * 3;
        p1 = vec_3(surf_mesh.points[v1], surf_mesh.points[v1 + 1], surf_mesh.points[v1 + 2]);

        for (SizeType k = 1; k <= n_triangles; ++k) {
            const SizeType v2 = surf_mesh.el_index[begin + k] * 3;
            const SizeType v3 = surf_mesh.el_index[begin + k + 1] * 3;

            p2 = vec_3(surf_mesh.points[v2], surf_mesh.points[v2 + 1], surf_mesh.points[v2 + 2]);
            p3 = vec_3(surf_mesh.points[v3], surf_mesh.points[v3 + 1], surf_mesh.points[v3 + 2]);

            result += fabs(tetrahedron_volume(b, p1, p2, p3));
        }
    }

    return result;
}

static void p_mesh_print(const PMesh *mesh) {
    printf("----------------------------------------------------\n");

    printf("n_elements %d, n_nodes %d\n", mesh->n_elements, mesh->n_nodes);
#ifdef USE_OBJ_OUTPUT

    for (SizeType i = 0; i < mesh->n_nodes; ++i) {
        const SizeType offset = i * mesh->n_dims;
        printf("v ");
        for (SizeType d = 0; d < mesh->n_dims; ++d) {
            printf("%g ", mesh->points[offset + d]);
        }

        printf("\n");
    }

    for (SizeType e = 0; e < mesh->n_elements; ++e) {
        const SizeType begin = mesh->el_ptr[e];
        const SizeType end = mesh->el_ptr[e + 1];

        KERNEL_DEBUG(if (mesh->is_new_element[e]) printf("#new\n"));
#ifdef USE_TRIANGULATED_OBJ_OUTPUT
        const SizeType n_triangles = (end - begin) - 2;

        if (n_triangles > 1) printf("#");
#endif
        printf("f ");
        for (SizeType k = begin; k < end; ++k) {
            printf("%d ", mesh->el_index[k] + 1);
        }

        printf("\n");
#ifdef USE_TRIANGULATED_OBJ_OUTPUT

        if (n_triangles == 1) continue;
        for (SizeType k = 0; k < n_triangles; ++k) {
            printf("f ");
            printf("%d ", mesh->el_index[begin] + 1);

            for (SizeType j = 1; j < 3; ++j) {
                printf("%d ", mesh->el_index[begin + k + j] + 1);
            }

            printf("\n");
        }
#endif
    }

#else
    printf("elements: \n");
    for (SizeType e = 0; e < mesh->n_elements; ++e) {
        const SizeType begin = mesh->el_ptr[e];
        const SizeType end = mesh->el_ptr[e + 1];

        for (SizeType k = begin; k < end; ++k) {
            printf("%d\t", mesh->el_index[k]);
        }

        printf("\n");
    }

    printf("points: \n");

    for (SizeType i = 0; i < mesh->n_nodes; ++i) {
        const SizeType offset = i * mesh->n_dims;
        for (SizeType d = 0; d < mesh->n_dims; ++d) {
            printf("%g,\t", mesh->points[offset + d]);
        }

        printf("\n");
    }

#endif
    printf("----------------------------------------------------\n");
}

void p_mesh_crs_print(const PMesh *mesh) {
    printf("----------------------------------------------------\n");

    printf("n_elements %d, n_nodes %d\n", mesh->n_elements, mesh->n_nodes);

    printf("el_ptr: \n[\n");
    for (SizeType e = 0; e < mesh->n_elements + 1; ++e) {
        printf("%d\t", mesh->el_ptr[e]);
    }

    printf("\n]\nel_index: \n[\n");
    for (SizeType e = 0; e < mesh->n_elements; ++e) {
        const SizeType begin = mesh->el_ptr[e];
        const SizeType end = mesh->el_ptr[e + 1];

        for (SizeType k = begin; k < end; ++k) {
            printf("%d\t", mesh->el_index[k]);
        }

        printf("\n");
    }

    printf("]\npoints: \n");

    for (SizeType i = 0; i < mesh->n_nodes; ++i) {
        const SizeType offset = i * mesh->n_dims;
        for (SizeType d = 0; d < mesh->n_dims; ++d) {
            printf("%g,\t", mesh->points[offset + d]);
        }

        printf("\n");
    }

    printf("----------------------------------------------------\n");
}

static void p_mesh_copy(const PMesh *in, PMesh *out) {
    out->n_elements = in->n_elements;
    out->n_nodes = in->n_nodes;
    out->n_dims = in->n_dims;

    const SizeType n_ptrs = in->n_elements + 1;
    const SizeType n_entries = in->n_nodes * in->n_dims;
    generic_copy(n_ptrs, in->el_ptr, out->el_ptr);
    generic_copy(in->el_ptr[in->n_elements], in->el_index, out->el_index);
    generic_copy(n_entries, in->points, out->points);
}

/// Exodus 2 format ordering of half-faces
void make_faceted_tetrahedron_from_element(const Mesh mesh, const SizeType element, PMesh *result) {
    result->n_elements = 4;
    result->n_nodes = 4;
    result->n_dims = mesh.n_dims;
    mesh_points(mesh, element, result->points);

    result->el_ptr[0] = 0;
    result->el_ptr[1] = 3;
    result->el_ptr[2] = 6;
    result->el_ptr[3] = 9;
    result->el_ptr[4] = 12;

    result->el_index[0] = 0;
    result->el_index[1] = 1;
    result->el_index[2] = 3;

    result->el_index[3] = 1;
    result->el_index[4] = 2;
    result->el_index[5] = 3;

    result->el_index[6] = 0;
    result->el_index[7] = 2;
    result->el_index[8] = 3;

    result->el_index[9] = 0;
    result->el_index[10] = 1;
    result->el_index[11] = 2;
}

/// Exodus 2 format ordering of half-faces
void make_faceted_hexahedron_from_element(const Mesh mesh, const SizeType element, PMesh *result) {
    result->n_elements = 6;
    result->n_nodes = 8;
    result->n_dims = mesh.n_dims;
    mesh_points(mesh, element, result->points);

    result->el_ptr[0] = 0;
    result->el_ptr[1] = 4;
    result->el_ptr[2] = 8;
    result->el_ptr[3] = 12;
    result->el_ptr[4] = 16;
    result->el_ptr[5] = 20;
    result->el_ptr[6] = 24;

    result->el_index[0] = 0;
    result->el_index[1] = 1;
    result->el_index[2] = 5;
    result->el_index[3] = 4;

    result->el_index[4] = 1;
    result->el_index[5] = 2;
    result->el_index[6] = 6;
    result->el_index[7] = 5;

    result->el_index[8] = 2;
    result->el_index[9] = 3;
    result->el_index[10] = 7;
    result->el_index[11] = 6;

    result->el_index[12] = 3;
    result->el_index[13] = 0;
    result->el_index[14] = 4;
    result->el_index[15] = 7;

    result->el_index[16] = 0;
    result->el_index[17] = 3;
    result->el_index[18] = 2;
    result->el_index[19] = 1;

    result->el_index[20] = 4;
    result->el_index[21] = 5;
    result->el_index[22] = 6;
    result->el_index[23] = 7;
}

static void compute_normals(const PMesh surf_mesh, Scalar *normals) {
    KERNEL_ASSERT(surf_mesh.n_dims == 3, "compute_normals: surf_mesh.n_dims == 3");

    Scalar bary_temp[3];                                                            // TO REMOVE
    row_average(surf_mesh.n_nodes, surf_mesh.n_dims, surf_mesh.points, bary_temp);  // TO REMOVE
    Vector3 bary = vec_3(bary_temp[0], bary_temp[1], bary_temp[2]);                 // TO REMOVE

    // printf("barycenter\n");
    // matrix_print(1, 3, bary_temp);

    Vector3 o, u, v, n;
    for (SizeType i = 0; i < surf_mesh.n_elements; ++i) {
        const SizeType begin = surf_mesh.el_ptr[i];
        const SizeType offset_3 = i * surf_mesh.n_dims;

        KERNEL_ASSERT((surf_mesh.el_ptr[i + 1] - begin) >= 3, "compute_normals: (end - begin) >= 3");

        const SizeType v_o = surf_mesh.el_index[begin] * surf_mesh.n_dims;
        const SizeType v_u = surf_mesh.el_index[begin + 1] * surf_mesh.n_dims;
        ;
        const SizeType v_v = surf_mesh.el_index[begin + 2] * surf_mesh.n_dims;
        ;

        o = vec_3(surf_mesh.points[v_o], surf_mesh.points[v_o + 1], surf_mesh.points[v_o + 2]);
        u = vec_3(surf_mesh.points[v_u], surf_mesh.points[v_u + 1], surf_mesh.points[v_u + 2]);
        v = vec_3(surf_mesh.points[v_v], surf_mesh.points[v_v + 1], surf_mesh.points[v_v + 2]);

        u -= o;
        v -= o;

        n = cross(u, v);
        n = normalize(n);

        if (dot(n, u + o - bary) < 0) {
            n = -n;
        }

        KERNEL_ASSERT(fabs(length(n) - 1.0) < DEFAULT_TOLLERANCE, "compute_normals: normal is not normalized");
        KERNEL_ASSERT(dot(n, u + o - bary) > 0, "compute_normals: wrong ordering of faces");  // TO REMOVE

        generic_copy(3, n, &normals[offset_3]);
    }
}

static void compute_normals_and_fix_ordering(PMesh *surf_mesh, Scalar *normals) {
    KERNEL_ASSERT(surf_mesh->n_dims == 3, "compute_normals_and_fix_ordering: surf_mesh->n_dims == 3");

    Scalar bary_temp[3];  // TO REMOVE
    row_average(surf_mesh->n_nodes, surf_mesh->n_dims, surf_mesh->points, bary_temp);
    Vector3 bary = vec_3(bary_temp[0], bary_temp[1], bary_temp[2]);

    // printf("barycenter\n");
    // matrix_print(1, 3, bary_temp);

    Vector3 o, u, v, n;
    for (SizeType i = 0; i < surf_mesh->n_elements; ++i) {
        const SizeType begin = surf_mesh->el_ptr[i];
        const SizeType offset_3 = i * surf_mesh->n_dims;

        KERNEL_ASSERT((surf_mesh->el_ptr[i + 1] - begin) >= 3, "compute_normals: (end - begin) >= 3");

        const SizeType v_o = surf_mesh->el_index[begin] * surf_mesh->n_dims;
        const SizeType v_u = surf_mesh->el_index[begin + 1] * surf_mesh->n_dims;
        ;
        const SizeType v_v = surf_mesh->el_index[begin + 2] * surf_mesh->n_dims;
        ;

        o = vec_3(surf_mesh->points[v_o], surf_mesh->points[v_o + 1], surf_mesh->points[v_o + 2]);
        u = vec_3(surf_mesh->points[v_u], surf_mesh->points[v_u + 1], surf_mesh->points[v_u + 2]);
        v = vec_3(surf_mesh->points[v_v], surf_mesh->points[v_v + 1], surf_mesh->points[v_v + 2]);

        u -= o;
        v -= o;

        n = cross(u, v);
        n = normalize(n);

        // printf("%g\n", dot(n, o - bary) );

        if (dot(n, u + o - bary) < 0) {
            // if(dot(n, o - bary) < 0) {
            n = -n;

            if ((surf_mesh->el_ptr[i + 1] - begin) == 4) {
                // generic_swap(SizeType, surf_mesh->el_index[begin + 0], surf_mesh->el_index[begin + 3]);
                // generic_swap(SizeType, surf_mesh->el_index[begin + 1], surf_mesh->el_index[begin + 2]);
            } else {
                generic_swap(SizeType, surf_mesh->el_index[begin + 1], surf_mesh->el_index[begin + 2]);
            }
        }

        KERNEL_ASSERT(fabs(length(n) - 1.0) < DEFAULT_TOLLERANCE, "compute_normals: normal is not normalized");
        KERNEL_ASSERT(dot(n, u + o - bary) > 0, "compute_normals: wrong ordering of faces");  // TO REMOVE

        generic_copy(3, n, &normals[offset_3]);
    }
}

bool make_polyhedron_from_element(const Mesh mesh, const SizeType element, PMesh *result) {
    const SizeType begin = mesh.el_ptr[element];
    const SizeType end = mesh.el_ptr[element + 1];
    const SizeType n_nodes = end - begin;

    KERNEL_ASSERT(mesh.n_dims == 3, "make_polyhedron_from_element: mesh.n_dims == 3");

    switch (n_nodes) {
        // TETRAHEDRON
        case 4: {
            make_faceted_tetrahedron_from_element(mesh, element, result);
            break;
        }
        // HEXAHEDRON
        case 8: {
            make_faceted_hexahedron_from_element(mesh, element, result);
            break;
        }
        // UNSUPPORTED
        default: {
            KERNEL_ASSERT(false, "make_polyhedron_from_element: element type not supported");
            return false;
        }
    }

    return true;
}

static SizeType make_h_polyhedron_from_polyhedron(const PMesh polyhedron,
                                                  Scalar *plane_normals,
                                                  Scalar *plane_dists_from_origin) {
    KERNEL_ASSERT(polyhedron.n_dims == 3, "make_h_polyhedron_from_polyhedron: n_dims must be 3");

    Vector3 n, p;
    compute_normals(polyhedron, plane_normals);
    for (SizeType i = 0; i < polyhedron.n_elements; ++i) {
        const SizeType begin = polyhedron.el_ptr[i];
        KERNEL_ASSERT(polyhedron.el_ptr[i + 1] - begin >= 3,
                      "make_h_polyhedron_from_polyhedron: received malformed polyhedron");

        const SizeType i_offset = i * polyhedron.n_dims;
        const SizeType p_offset = polyhedron.el_index[begin] * polyhedron.n_dims;

        n = vec_3(plane_normals[i_offset], plane_normals[i_offset + 1], plane_normals[i_offset + 2]);
        p = vec_3(polyhedron.points[p_offset], polyhedron.points[p_offset + 1], polyhedron.points[p_offset + 2]);

        plane_dists_from_origin[i] = dot(n, p);
    }

    // #ifdef CLIPP_HOST_CL //HOST_DEBUGGING
    // 		geom::Meshd mesh;
    // 		from_cl_mesh_to_geom_mesh(polyhedron, plane_normals, mesh);
    // 		geom::plot(mesh, "hpoly", "0, 0, 255");
    // #endif

    return polyhedron.n_elements;
}

SizeType make_h_polyhedron_from_element(const Mesh mesh,
                                        const SizeType element,
                                        Scalar *plane_normals,
                                        Scalar *plane_dists_from_origin) {
    PMesh temp_poly;
    if (!make_polyhedron_from_element(mesh, element, &temp_poly)) {
        return 0;
    }

    return make_h_polyhedron_from_polyhedron(temp_poly, plane_normals, plane_dists_from_origin);
}

static bool intersect_convex_polyhedra(PMesh poly_1, const PMesh poly_2, PMesh *result) {
    Scalar normals_result[MAX_LOCAL_ELEMENTS * 3];
    Scalar normals_half_spaces[MAX_LOCAL_ELEMENTS * 3];
    Scalar dist_half_spaces[MAX_LOCAL_ELEMENTS];

    compute_normals_and_fix_ordering(&poly_1, normals_result);
    p_mesh_copy(&poly_1, result);

    // #ifdef CLIPP_HOST_CL //HOST_DEBUGGING
    // 	geom::Meshd mesh;
    // 	from_cl_mesh_to_geom_mesh(poly_1, normals_result, mesh);
    // 	geom::plot(mesh, "poly", "255, 0, 0");
    // #endif

    const SizeType n_half_spaces = make_h_polyhedron_from_polyhedron(poly_2, normals_half_spaces, dist_half_spaces);

    return hs_cut_polyhedron(n_half_spaces, normals_half_spaces, dist_half_spaces, result, normals_result);
}

#define mesh_for_each_node(mesh_ptr, code_on_node)                    \
    {                                                                 \
        for (SizeType node = 0; node < (mesh_ptr)->n_nodes; ++node) { \
            code_on_node;                                             \
        }                                                             \
    }

static Scalar point_plane_distance(const SizeType n_dims, const Scalar *normal, const Scalar d, const Scalar *point) {
    return dot_n(n_dims, normal, point) - d;
}

static bool plane_contains_point(const SizeType n_dims,
                                 const Scalar *normal,
                                 const Scalar d,
                                 const Scalar *point,
                                 const Scalar tol) {
    // printf("%g\n", fabs(point_plane_distance(n_dims, normal, d, point)) );
    return fabs(point_plane_distance(n_dims, normal, d, point)) < tol;
}

static SizeType edge_index(const SizeType n_vertices, const SizeType v1, const SizeType v2) {
    return (v1 < v2) ? (v1 * n_vertices + v2) : (v2 * n_vertices + v1);
}

static void bind_edge_to_id(const SizeType n_vertices,
                            const SizeType v1,
                            const SizeType v2,
                            const SizeType id,
                            SizeType *edge_map) {
    const SizeType index = edge_index(n_vertices, v1, v2);
    KERNEL_ASSERT(index < MAX_LOCAL_MESH_POINTS * MAX_LOCAL_MESH_POINTS, "bind_edge_to_id: not enough space for edge");
    edge_map[index] = id;
}

SizeType get_edge_id(const SizeType n_vertices, const SizeType v1, const SizeType v2, SizeType *edge_map) {
    KERNEL_ASSERT(edge_index(n_vertices, v1, v2) < MAX_LOCAL_MESH_POINTS * MAX_LOCAL_MESH_POINTS,
                  "get_edge_id: index out of bound");
    return edge_map[edge_index(n_vertices, v1, v2)];
}

SizeType make_edge_id(const SizeType n_vertices,
                      const SizeType v1,
                      const SizeType v2,
                      const SizeType n_ids,
                      SizeType *edge_map) {
    if (v1 < v2) {
        edge_map[edge_index(n_vertices, v1, v2)] = n_ids;
        return n_ids + 1;
    } else {
        return n_ids;
    }
}

static bool intersect_ray_with_plane_2(const Scalar *ray_origin,
                                       const Scalar *ray_dir,
                                       const Scalar *plane_normal,
                                       const Scalar plane_dist_from_origin,
                                       Scalar *delta,
                                       const Scalar tol) {
    const Scalar cos_angle = -dot_n(2, plane_normal, ray_dir);

    if (fabs(cos_angle) < tol) {
        *delta = 0;
        return false;
    }

    // distance from plane
    const Scalar dist = point_plane_distance(2, plane_normal, plane_dist_from_origin, ray_origin);

    // point in ray trajectory
    *delta = dist / cos_angle;
    return true;
}

bool intersect_ray_with_line_2(const Scalar *ray_origin, const Scalar *ray_dir, const Scalar *line, Scalar *delta) {
    // line normal
    const Vector2 n = normalize(vec_2(line[3] - line[1], line[0] - line[2]));
    const Scalar normal[2] = {n.x, n.y};
    const Scalar d = dot_n(2, line, normal);

    return intersect_ray_with_plane_2(ray_origin, ray_dir, normal, d, delta, DEFAULT_TOLLERANCE);
}

bool intersect_ray_with_plane_point_3(const Scalar *ray_origin,
                                      const Scalar *ray_dir,
                                      const Scalar *plane_normal,
                                      const Scalar plane_dist_from_origin,
                                      Scalar *intersection,
                                      const Scalar tol) {
    Scalar delta;
    if (!intersect_ray_with_plane_3(ray_origin, ray_dir, plane_normal, plane_dist_from_origin, &delta, tol)) {
        return false;
    }

    generic_copy(3, ray_origin, intersection);
    axpy(3, delta, ray_dir, intersection);
    return true;
}

static bool intersect_ray_with_plane_3(const Scalar *ray_origin,
                                       const Scalar *ray_dir,
                                       const Scalar *plane_normal,
                                       const Scalar plane_dist_from_origin,
                                       Scalar *delta,
                                       const Scalar tol) {
    const Scalar cos_angle = -dot_3(ray_dir, plane_normal);

    if (fabs(cos_angle) < tol) {
        printf("%g == 0\n", cos_angle);
        *delta = 0;
        return false;
    }

    const Scalar distance = point_plane_distance(3, plane_normal, plane_dist_from_origin, ray_origin);
    *delta = distance / cos_angle;
    return true;
}

static bool intersect_ray_with_plane_using_ray_o_dist_3(const Scalar *ray_origin,
                                                        const Scalar *ray_dir,
                                                        const Scalar *plane_normal,
                                                        const Scalar distance,
                                                        Scalar *intersection,
                                                        const Scalar tol) {
    const Scalar cos_angle = -(dot_3(ray_dir, plane_normal));

    if (fabs(cos_angle) < tol) {
        // coplanar
        return false;
    }

    const Scalar delta = distance / cos_angle;
    generic_copy(3, ray_origin, intersection);
    axpy(3, delta, ray_dir, intersection);
    return true;
}

// The mesh elements are the faces of the polyhedron
static short half_space_cut(const Scalar *normal,
                            const Scalar d,
                            const PMesh *in,
                            const Scalar *in_surface_normals,
                            Scalar *out_surface_normals,
                            PMesh *out,
                            const Scalar tol) {
    Scalar pp_distance[MAX_LOCAL_MESH_POINTS];
    short node_query[MAX_LOCAL_MESH_POINTS];
    short element_query[MAX_LOCAL_EL_POINTERS];
    SizeType map[MAX_LOCAL_MESH_POINTS];
    SizeType cut_face[MAX_LOCAL_MESH_POINTS];
    SizeType mutated_face[MAX_LOCAL_MESH_POINTS];

    // FIXME see if it is inefficient to create this storage
    SizeType edge_map[MAX_LOCAL_MESH_POINTS * (MAX_LOCAL_MESH_POINTS + 1) / 2];

    KERNEL_ASSERT(in->n_dims == 3, "half_space_cut: n_dims should be equal to 3");
    KERNEL_ASSERT(!has_nan_entries(in->n_elements * in->n_dims, in_surface_normals),
                  "half_space_cut: normals have nan entries");

    const SizeType n_dims = in->n_dims;

    //////////////////////////////////////////NODES/////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    mesh_for_each_node(in, (pp_distance[node] = point_plane_distance(n_dims, normal, d, &in->points[node * n_dims])));

    const Scalar min_dist = min_n(in->n_nodes, pp_distance);
    const Scalar max_dist = max_n(in->n_nodes, pp_distance);

    // printf("dists:\n");
    // matrix_print(1, in->n_nodes, pp_distance);
    // printf("normal:\n");
    // matrix_print(1, in->n_dims, normal);
    // printf("d=%g\n-------------\n", d);

    if (min_dist >= -tol * 100) {
        return NOT_INTERSECTED;
    }

    if (max_dist <= tol * 100) {
        return CONTAINED_USE_ORIGINAL;
    }

    // Init out
    out->n_dims = in->n_dims;
    out->n_nodes = 0;
    out->n_elements = 0;
    out->el_ptr[0] = 0;

    SizeType n_points_to_remove = 0;
    for (SizeType node = 0; node < in->n_nodes; ++node) {
        if (pp_distance[node] > tol) {
            ++n_points_to_remove;
            node_query[node] = OUTSIDE;
        } else {
            if (pp_distance[node] < -tol) {
                node_query[node] = INSIDE;
            } else {
                node_query[node] = ON_PLANE;
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////ELEMENTS/////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    SizeType size_map = 0;
    mesh_for_each_node(in, (map[size_map++] = (node_query[node] == OUTSIDE) ? INVALID_INDEX : (out->n_nodes++)));
    generic_set(in->n_elements, OK, element_query);

    SizeType n_elements_to_be_removed = 0;
    for (SizeType e = 0; e < in->n_elements; ++e) {
        const SizeType e_begin = in->el_ptr[e];
        const SizeType e_end = in->el_ptr[e + 1];
        const SizeType e_n_nodes = e_end - e_begin;

        const Scalar element_plane_angle = dot_n(n_dims, normal, &in_surface_normals[e * n_dims]);

        // printf("element_plane_angle < tol %g, %g\n", element_plane_angle, tol);
        if (fabs(element_plane_angle - 1) < tol) {
            // coplanar

            const Scalar plane_to_plane_dist = pp_distance[in->el_index[e_begin]];

            if (plane_to_plane_dist <= tol) {
                // assert(false);
                REPORT_ERROR("coplanar faces should have been handled already but ok...");
                return CONTAINED_USE_ORIGINAL;
            } else {
                element_query[e] = TO_BE_REMOVED;
                ++n_elements_to_be_removed;
                continue;
            }
        }

        SizeType n_outside_nodes = 0;
        SizeType n_on_plane_nodes = 0;

        for (SizeType k = e_begin; k != e_end; ++k) {
            const SizeType node_k = in->el_index[k];
            const SizeType node_kp1 = (k + 1 == e_end) ? in->el_index[e_begin] : in->el_index[k + 1];

            const SizeType nq_k = node_query[node_k];
            const SizeType nq_kp1 = node_query[node_kp1];

            n_outside_nodes += nq_k == OUTSIDE || (nq_k == ON_PLANE && nq_kp1 != INSIDE);
            n_on_plane_nodes += nq_k == ON_PLANE;
        }

        // printf("n_on_plane_nodes: %d, n_outside_nodes: %d\n", n_on_plane_nodes, n_outside_nodes);

        if (n_on_plane_nodes == e_n_nodes) {
            element_query[e] = OK;
        } else if (n_outside_nodes == e_n_nodes) {
            element_query[e] = TO_BE_REMOVED;
        } else {
            element_query[e] = TO_BE_INTERSECTED;
        }
    }

    // printf("[");
    // for(SizeType e = 0; e < in->n_elements; ++e) {
    // 	switch(element_query[e]) {
    // 		case OK: printf("OK\t"); break;
    // 		case TO_BE_REMOVED: printf("TO_BE_REMOVED\t"); break;
    // 		case TO_BE_INTERSECTED: printf("TO_BE_INTERSECTED\t"); break;
    // 		default: printf("WTF\t"); break;
    // 	}
    // }

    // printf("]\n");

    ///////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////INTERSECTIONS/////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    SizeType n_nodes_in_cut_face = 0;
    for (SizeType i = 0; i < in->n_nodes; ++i) {
        if (node_query[i] == ON_PLANE) {
            cut_face[n_nodes_in_cut_face++] = map[i];
        }
    }

    // printf("n_nodes_in_cut_face (before): %d\n", n_nodes_in_cut_face);

    const SizeType size_edge_mat = symmetric_matrix_n_entries(in->n_nodes);
    generic_set(size_edge_mat, INVALID_INDEX, edge_map);

    // SizeType n_added_points = 0;
    for (SizeType e = 0; e < in->n_elements; ++e) {
        const SizeType e_begin = in->el_ptr[e];
        const SizeType e_end = in->el_ptr[e + 1];
        const SizeType e_n_nodes = e_end - e_begin;

        switch (element_query[e]) {
            case OK: {
                KERNEL_ASSERT(out->n_elements < MAX_LOCAL_ELEMENTS, "n_elements is not valid");

                const SizeType offset = out->el_ptr[out->n_elements];

                KERNEL_ASSERT(offset < MAX_LOCAL_ELEMENTS, "offset is not valid");

                SizeType i = 0;
                for (SizeType k = e_begin; k != e_end; ++k) {
                    KERNEL_ASSERT(INVALID_INDEX != map[in->el_index[k]], "INVALID_INDEX != map[in->el_index[k]]");
                    out->el_index[offset + i++] = map[in->el_index[k]];
                }
                KERNEL_DEBUG(out->is_new_element[out->n_elements] = in->is_new_element[e]);
                generic_copy(n_dims, &in_surface_normals[e * n_dims], &out_surface_normals[out->n_elements * n_dims]);
                out->el_ptr[++out->n_elements] = offset + e_n_nodes;
                break;
            }

            case TO_BE_INTERSECTED: {
                SizeType n_nodes_in_mutated_face = 0;
                const SizeType first = in->el_index[e_begin];
                if (node_query[first] != OUTSIDE) {
                    KERNEL_ASSERT(map[first] != INVALID_INDEX,
                                  "half_space_cut: inconsistent state, INVALID_INDEX instead of node index");
                    mutated_face[n_nodes_in_mutated_face++] = map[first];
                }

                for (SizeType k = e_begin; k != e_end; ++k) {
                    const SizeType vk = in->el_index[k];
                    const bool is_first = k + 1 == e_end;
                    const SizeType vkp1 = is_first ? in->el_index[e_begin] : in->el_index[k + 1];

                    const Scalar dk = pp_distance[vk];
                    const Scalar dkp1 = pp_distance[vkp1];

                    const bool on_plane = node_query[vk] == ON_PLANE || node_query[vkp1] == ON_PLANE;

                    if (!on_plane && sign(dk) != sign(dkp1)) {
                        const SizeType edge_index = symmetric_matrix_index(vk, vkp1);
                        SizeType new_node = edge_map[edge_index];

                        // if(new_node != INVALID_INDEX) {
                        // 	printf("reusing already created vertex %d\n", new_node);
                        // } else {
                        // 	printf("creating new vertex %d\n", out->n_nodes+1);
                        // }

                        if (new_node == INVALID_INDEX) {
                            new_node = out->n_nodes++;
                            edge_map[edge_index] = new_node;

                            Scalar dir[MAX_N_DIMS];

                            vec_minus(n_dims, &in->points[vk * n_dims], &in->points[vkp1 * n_dims], dir);
                            vec_scale(n_dims, 1.0 / norm_n(3, dir), dir);

                            // const bool ok = intersect_ray_with_plane_point_3(&in->points[vk * n_dims], dir, normal,
                            // d, &out->points[new_node * n_dims], tol);

                            // printf("---------------------\n");
                            // matrix_print(3, 1, &in->points[vk * n_dims]);
                            // matrix_print(3, 1, dir);
                            // matrix_print(3, 1, normal);
                            // printf("%g\n", d);
                            // matrix_print(3, 1, &out->points[new_node * n_dims]);
                            // printf("---------------------\n");

                            const bool ok = intersect_ray_with_plane_using_ray_o_dist_3(&in->points[vk * n_dims],
                                                                                        dir,
                                                                                        normal,
                                                                                        pp_distance[vk],
                                                                                        &out->points[new_node * n_dims],
                                                                                        tol);

                            KERNEL_ASSERT(ok, "half_space_cut: should never enter here");
                            (void)ok;
                            KERNEL_ASSERT(plane_contains_point(3, normal, d, &out->points[new_node * n_dims], tol),
                                          "intersect_ray_with_plane_using_ray_o_dist_3 returned wrong result");

                            cut_face[n_nodes_in_cut_face++] = new_node;
                        }

                        mutated_face[n_nodes_in_mutated_face++] = new_node;
                    }

                    if (node_query[vkp1] != OUTSIDE && !is_first) {
                        KERNEL_ASSERT(map[vkp1] != INVALID_INDEX,
                                      "half_space_cut: inconsistent state, INVALID_INDEX instead of node index");
                        mutated_face[n_nodes_in_mutated_face++] = map[vkp1];
                    }
                }

                if (n_nodes_in_mutated_face >= 3) {
                    const SizeType offset = out->el_ptr[out->n_elements];

                    for (SizeType i = 0; i < n_nodes_in_mutated_face; ++i) {
                        KERNEL_ASSERT(INVALID_INDEX != mutated_face[i], "INVALID_INDEX != mutated_face[i]");
                        out->el_index[offset + i] = mutated_face[i];
                    }
                    KERNEL_DEBUG(out->is_new_element[out->n_elements] = in->is_new_element[e]);
                    generic_copy(
                        n_dims, &in_surface_normals[e * n_dims], &out_surface_normals[out->n_elements * n_dims]);
                    out->el_ptr[++out->n_elements] = offset + n_nodes_in_mutated_face;

                } else {
                    REPORT_ERROR("half_space_cut: degenerate face");
                }

                break;
            }

            case TO_BE_REMOVED: {
                // printf("TO_BE_REMOVED\n");
                break;
            }

            default: {
                break;
            }
        }
    }

    // printf("n_nodes_in_cut_face (after): %d\n", n_nodes_in_cut_face);

    for (SizeType i = 0; i < in->n_nodes; ++i) {
        if (map[i] != INVALID_INDEX) {
            const SizeType in_offset = i * n_dims;
            const SizeType out_offset = map[i] * n_dims;
            generic_copy(n_dims, &in->points[in_offset], &out->points[out_offset]);
        }
    }

    const SizeType out_el_offset = out->el_ptr[out->n_elements];

    for (SizeType i = 0; i < n_nodes_in_cut_face; ++i) {
        out->el_index[out_el_offset + i] = cut_face[i];
    }

    KERNEL_ASSERT(out->n_elements < MAX_LOCAL_ELEMENTS, "n_elements is not valid");

    generic_copy(n_dims, normal, &out_surface_normals[out->n_elements * n_dims]);

    // if the intersected surface is not closed
    if (n_nodes_in_cut_face < 3 && n_nodes_in_cut_face >= 0) {
        // REPORT_ERROR("should never happen\n");
        // return CONTAINED_USE_ORIGINAL;
        // return INTERSECTED;
        // p_mesh_print(out);
        return INTERSECTED;
    }

    if (!fix_polygon_ordering(n_nodes_in_cut_face, normal, out->points, &out->el_index[out->el_ptr[out->n_elements]])) {
        return NOT_INTERSECTED;
    }

    KERNEL_DEBUG(out->is_new_element[out->n_elements] = true);

    out->el_ptr[++out->n_elements] = out_el_offset + n_nodes_in_cut_face;

    return INTERSECTED;
}

// FIXME
static bool fix_polygon_ordering(const SizeType n_points, const Scalar *normal, const Scalar *points, SizeType *index) {
    Scalar local_points[MAX_LOCAL_MESH_POINTS * 2];
    Scalar local_value[MAX_LOCAL_MESH_POINTS];

    if (n_points == 3) {
        // trivial case

        const SizeType v0 = index[0] * 3;
        const SizeType v1 = index[1] * 3;
        const SizeType v2 = index[2] * 3;

        const Vector3 p0 = vec_3(points[v0], points[v0 + 1], points[v0 + 2]);
        const Vector3 p1 = vec_3(points[v1], points[v1 + 1], points[v1 + 2]);
        const Vector3 p2 = vec_3(points[v2], points[v2 + 1], points[v2 + 2]);

        if (dot(cross(p1 - p0, p2 - p0), vec_3(normal[0], normal[1], normal[2])) <= 0) {
            generic_swap(SizeType, index[1], index[2]);
        }

        return true;
    }

    // default is xy-plane
    SizeType coord_1 = 0;
    SizeType coord_2 = 1;

    bool transformed = false;

    if (fabs(normal[1]) > 0.9999) {
        // xz-plane
        coord_2 = 2;

    } else if (fabs(normal[0]) > 0.9999) {
        // yz-plane
        coord_1 = 1;
        coord_2 = 2;

    } else if (!(fabs(normal[2]) > 0.9999)) {
        // also not xy-plane
        transformed = true;
    }

    if (transformed) {
        // The bug is inside this branch
        // printf("householder_reflection_3: ROTATING POLY\n");
        // Rotation to the xy-plane is necessary
        Scalar H[3 * 3];
        Scalar v[3] = {normal[0], normal[1], normal[2] + (Scalar)1.0};
        const Scalar norm_v = norm_n(3, v);
        v[0] /= norm_v;
        v[1] /= norm_v;
        v[2] /= norm_v;

        householder_reflection_3(v, H);

        for (SizeType i = 0; i < n_points; ++i) {
            const SizeType global_offset = index[i] * 3;
            const SizeType local_offset = i * 2;
            mat_vec_mul(2, 3, H, &points[global_offset], &local_points[local_offset]);
        }

    } else {
        for (SizeType i = 0; i < n_points; ++i) {
            const SizeType global_offset = index[i] * 3;
            const SizeType local_offset = i * 2;

            local_points[local_offset] = points[global_offset + coord_1];
            local_points[local_offset + 1] = points[global_offset + coord_2];
        }
    }

    Scalar avg[2];
    row_average(n_points, 2, local_points, avg);
    row_subtract(n_points, 2, avg, local_points);

    // Project local points onto reference circle norm_2(p) = 1
    for (SizeType i = 0; i < n_points; ++i) {
        const SizeType offset_i = i * 2;
        const Scalar norm_p = norm_n(2, &local_points[offset_i]);
        local_points[offset_i] /= norm_p;
        local_points[offset_i + 1] /= norm_p;
    }

    SizeType i_min[2], i_max[2];
    row_arg_min(n_points, 2, local_points, i_min);
    row_arg_max(n_points, 2, local_points, i_max);

    if (i_min[0] == i_max[0] || i_min[1] == i_max[1]) {
        printf("[%d, %d], [%d, %d]\n", i_min[0], i_max[0], i_min[1], i_max[1]);
        matrix_print(n_points, 2, local_points);
        REPORT_ERROR("fix_polygon_ordering: degenerate input\n");
        return false;
    }

    const Vector2 left_bound = vec_2(local_points[i_min[0] * 2], local_points[i_min[0] * 2 + 1]);
    const Vector2 right_bound = vec_2(local_points[i_max[0] * 2], local_points[i_max[0] * 2 + 1]);

    Vector2 proj_vec = (right_bound - left_bound);
    proj_vec /= length(proj_vec);

    const Vector2 perp_proj_vec = vec_2(-proj_vec.y, proj_vec.x);

    avg[0] = left_bound.x;
    avg[1] = left_bound.y;

    row_subtract(n_points, 2, avg, local_points);

    //[-1, 0.0 + x ...., 3.0, 4.0 + x .......]
    for (SizeType i = 0; i < n_points; ++i) {
        if (i == i_min[0]) {
            local_value[i] = -1.0;  // put it far left
            continue;
        }

        if (i == i_max[0]) {
            local_value[i] = 3.0;  // put it in the middle
            continue;
        }

        const SizeType offset_i = i * 2;
        const Vector2 dir = vec_2(local_points[offset_i], local_points[offset_i + 1]);
        const Scalar proj_location = dot(proj_vec, dir);
        const Scalar dist = dot(dir, perp_proj_vec);

        if (dist > 0) {
            // It is in the upper hemisphere
            local_value[i] = 50.0 - proj_location;  // put it far right but the other way around
        } else if (dist < 0) {
            // It is in the lower hemisphere
            local_value[i] = proj_location;

        } else {
            REPORT_ERROR("fix_polygon_ordering: special case of collinear object not handled");
        }
    }

    // printf("----------\n");
    // printf("min %d, max %d\n",i_min[0], i_max[0]);
    // printf("> points:\n");
    // matrix_print(n_points, 3, points);
    // printf("> local_points:\n");
    // matrix_print(n_points, 2, local_points);
    // printf("> local_value:\n");
    // matrix_print(1, n_points, local_value);
    // printf("> perp_proj_vec:\n");
    // printf("[ %g, %g ]\n", perp_proj_vec[0], perp_proj_vec[1]);

    // get index mapping with resepct to local value

    // SizeType local_index[] = { 0, 1, 2, 3, 4, 5, 6, 7 };
    // Scalar local_fun_cp[40];
    // generic_copy(n_points, local_value, local_fun_cp);
    // sort_scalar_and_index_array(n_points, local_fun_cp, local_index);

    // for(SizeType i = 0; i < n_points; ++i) {
    // 	const SizeType k = local_index[i];
    // 	KERNEL_ASSERT(k >= 0, "k >= 0");

    // 	printf("%g, %g\n", local_points[k * 2], local_points[k*2 + 1]);
    // printf("%d -> %g, %g, %g\n", k, points[k * 3], points[k*3 + 1], points[k*3 + 2]);
    // printf("%g, %g, %g\n", k, points[k * 3], points[k*3 + 1], points[k*3 + 2]);

    // const SizeType v0 = local_index[0]   * 2;
    // const SizeType v1 = local_index[i+1] * 2;
    // const SizeType v2 = local_index[i+2] * 2;

    // const Vector3 p0 = vec_3(local_points[v0], local_points[v0+1], 0);
    // const Vector3 p1 = vec_3(local_points[v1], local_points[v1+1], 0);
    // const Vector3 p2 = vec_3(local_points[v2], local_points[v2+1], 0);

    // if(dot( cross(p1-p0, p2-p0), vec_3(0,0, 1) ) <= 0) {
    // 	printf("local ordering: error\n");
    // } else {
    // 	printf("local ordering: OK\n");
    // }
    // }

    // printf("----------\n");

    // for(SizeType i = 0; i < n_points; ++i) {
    // 	printf("%d \t", index[i]);
    // }
    // printf("\n");

    // vec_scale(n_points, -1, local_value);
    sort_scalar_and_index_array(n_points, local_value, index);

    // for(SizeType i = 0; i < n_points; ++i) {
    // 	printf("%d \t", index[i]);
    // }
    // printf("\n");

    return true;
}

// insertion sort
static void sort_scalar_and_index_array(const SizeType n_elements, Scalar *values, SizeType *indices) {
    for (SizeType i = 1; i < n_elements; ++i) {
        const Scalar value = values[i];
        const SizeType index = indices[i];

        SizeType j = i - 1;

        while (j >= 0 && values[j] > value) {
            const SizeType jp1 = j + 1;
            values[jp1] = values[j];
            indices[jp1] = indices[j];
            --j;
        }

        const SizeType jp1 = j + 1;
        values[jp1] = value;
        indices[jp1] = index;
    }
}

static bool hs_cut_polyhedron(const SizeType n_half_spaces,
                              const Scalar *planes_normals,
                              const Scalar *planes_dist_from_origin,
                              PMesh *polyhedron,
                              Scalar *normals) {
    // storage
    PMesh isect_polyhedron;
    Scalar isect_normals[MAX_LOCAL_ELEMENTS * MAX_N_DIMS];

    // handles
    PMesh *in_ptr = polyhedron;
    PMesh *out_ptr = &isect_polyhedron;
    Scalar *in_normals = normals;
    Scalar *out_normals = isect_normals;

    // p_mesh_print(in_ptr);

    for (SizeType j = 0; j < n_half_spaces; ++j) {
        const SizeType offset_j = 3 * j;

        // set debug values
        PMESH_DEBUG(out_ptr);

        switch (half_space_cut(&planes_normals[offset_j],
                               planes_dist_from_origin[j],
                               in_ptr,
                               in_normals,
                               out_normals,
                               out_ptr,
                               DEFAULT_TOLLERANCE)) {
            case CONTAINED_USE_ORIGINAL: {
                // printf("%d CONTAINED_USE_ORIGINAL\n", j);
                // p_mesh_print(in_ptr);
                break;
            }
            case INTERSECTED: {
                generic_ptr_swap(PMesh, in_ptr, out_ptr);
                generic_ptr_swap(Scalar, in_normals, out_normals);

                // printf("%d INTERSECTED\n", j);
                // p_mesh_print(in_ptr);

                // #ifdef CLIPP_HOST_CL //HOST_DEBUGGING
                // 				geom::Meshd mesh;
                // 				from_cl_mesh_to_geom_mesh(*in_ptr, in_normals, mesh);
                // 				mesh.makeTriMesh();
                // 				geom::plot(mesh, "isect/" + cutk::ToString(j), "0, 255, 0");
                // #endif
                break;
            }
            case NOT_INTERSECTED: {
                // printf("%d NOT_INTERSECTED\n", j);
                return false;
            }
            default: {
                KERNEL_ASSERT(false, "hs_cut_polyhedron: should not happen");
                break;
            }
        }
    }

    if (polyhedron != in_ptr) {
        p_mesh_copy(in_ptr, polyhedron);
        generic_copy(polyhedron->n_elements, in_normals, normals);
    }

    return polyhedron->n_elements >= 4;
}

m_kernel__ void intersect_mesh_with_half_spaces_and_compute_volume(
    // input
    // A) basic data
    const SizeType n_dims,      // 0)
    const SizeType n_elements,  // 1)

    // B) Mesh
    m_global__ SizeType *el_ptr,    // 2) //n_elements+1
    m_global__ SizeType *el_index,  // 3)
    m_global__ SizeType *el_type,   // 4)

    // C) point data
    SizeType n_nodes,           // 5)
    m_global__ Scalar *points,  // 6) N_DIMS x n_nodes
                                // D) half-space data
    const SizeType n_half_spaces,                      // 7)
    const m_global__ Scalar *planes_normals,           // 8)
    const m_global__ Scalar *planes_dist_from_origin,  // 9)
    // E) result
    m_global__ Scalar *volumes,       // 10)
    m_global__ SizeType *intersected  // 11)
) {
    Mesh mesh;
    mesh.n_dims = n_dims;
    mesh.n_elements = n_elements;
    mesh.el_ptr = el_ptr;
    mesh.el_index = el_index;
    mesh.el_type = el_type;
    mesh.n_nodes = n_nodes;
    mesh.points = points;

    // storage
    PMesh polyhedron;
    Scalar normals[MAX_LOCAL_ELEMENTS];

    Scalar p_planes_normals[MAX_N_HALF_SPACES * 3];
    Scalar p_planes_dist_from_origin[MAX_N_HALF_SPACES];

    KERNEL_ASSERT(n_half_spaces < MAX_N_HALF_SPACES, "n_half_spaces < MAX_N_HALF_SPACES");
    generic_copy(n_half_spaces, planes_normals, p_planes_normals);
    generic_copy(n_half_spaces * 3, planes_dist_from_origin, p_planes_dist_from_origin);

    Scalar result = 0;
    for (SizeType i = get_global_id(0); i < n_elements; i += get_global_size(0)) {
        if (make_polyhedron_from_element(mesh, i, &polyhedron)) {
            KERNEL_ASSERT(false, "intersect_mesh_with_half_spaces_and_compute_volume: failure");
            continue;
        }

        // compute_normals(polyhedron, normals);

        compute_normals_and_fix_ordering(&polyhedron, normals);

        if (hs_cut_polyhedron(n_half_spaces, p_planes_normals, p_planes_dist_from_origin, &polyhedron, normals)) {
            // do something with in_ptr and in_normals
            result += p_mesh_volume_3(polyhedron);
        }
    }

    intersected[get_global_id(0)] = result;
}

// clean up macros
#undef NOT_INTERSECTED
#undef INTERSECTED
#undef CONTAINED_USE_ORIGINAL
#undef ERROR_IN_ALGORITHM
#undef INSIDE
#undef ON_PLANE
#undef OUTSIDE
#undef TO_BE_REMOVED
#undef TO_BE_INTERSECTED
#undef OK

#endif  // INTERSECT_CL
