#ifndef MOONOLITH_INTERTSCT_POLYHEDRA_IMPL_HPP
#define MOONOLITH_INTERTSCT_POLYHEDRA_IMPL_HPP

#include "moonolith_intersect_polyhedra.hpp"

namespace moonolith {

    const static short TO_BE_REMOVED = -1;
    const static short TO_BE_INTERSECTED = -2;
    const static short TO_BE_KEPT = 0;

    template <typename T>
    void IntersectPolyhedronWithHalfSpace<T>::clear() {
        pp_distance.clear();
        node_query.clear();
        element_query.clear();
        map.clear();
        cut_face.clear();
        mutated_face.clear();
        edge_map.clear();
    }

    template <typename T>
    void IntersectPolyhedronWithHalfSpace<T>::fix_side_orientation(const int i,
                                                                   const Vector &normal,
                                                                   Polyhedron &polyhedron) {
        const auto begin = polyhedron.el_ptr[i];
        const auto end = polyhedron.el_ptr[i + 1];
        // const auto len = end - begin;

        const auto &points = polyhedron.points;
        auto &index = polyhedron.el_index;

        auto u = points[index[begin + 1]] - points[index[begin]];
        auto v = points[index[begin + 2]] - points[index[begin]];
        auto w = cross(u, v);

        if (dot(w, normal) < 0.) {
            std::reverse(index.begin() + begin, index.begin() + end);
        }
    }

    template <typename T>
    void IntersectPolyhedronWithHalfSpace<T>::fix_side(const int i,
                                                       const Vector &normal,
                                                       const bool remove_collinear,
                                                       Polyhedron &polyhedron,
                                                       const T tol) {
        const auto begin = polyhedron.el_ptr[i];
        const auto end = polyhedron.el_ptr[i + 1];
        const auto n = end - begin;

        polygon_buffer.resize(n);

        for (Integer k = 0; k < n; ++k) {
            polygon_buffer[k] = polyhedron.points[polyhedron.el_index[begin + k]];
        }

        int n_collinear = rcp.find(polygon_buffer, is_collinear, tol);

        if (n_collinear == 0) {
            fix_side_orientation(i, normal, polyhedron);
        } else if (n_collinear > 0 && remove_collinear) {
            for (int k = n - 1; k >= 0; --k) {
                if (is_collinear[k]) {
                    polyhedron.el_index.erase(polyhedron.el_index.begin() + begin + k);
                }
            }

            const auto len_el_ptr = polyhedron.el_ptr.size();
            for (std::size_t k = i + 1; k < len_el_ptr; ++k) {
                polyhedron.el_ptr[k] -= n_collinear;
            }

            fix_side_orientation(i, normal, polyhedron);
        } else {
            if (n_collinear > 0) {
                const auto begin = polyhedron.el_ptr[i];
                const auto end = polyhedron.el_ptr[i + 1];
                // const auto len = end - begin;

                const auto &points = polyhedron.points;
                auto &index = polyhedron.el_index;

                auto i0 = index[begin];
                auto i1 = index[begin + 1];
                auto i2 = index[begin + 2];

                if (is_collinear[1]) {
                    i1 = i2;
                    i2 = index[begin + 3];
                    assert(!is_collinear[2]);
                }

                auto u = points[i1] - points[i0];
                auto v = points[i2] - points[i0];

                auto w = cross(u, v);

                if (dot(w, normal) < 0.) {
                    std::reverse(index.begin() + begin, index.begin() + end);
                }
            }
        }
    }

    template <typename T>
    short IntersectPolyhedronWithHalfSpace<T>::apply(const HalfSpace &h,
                                                     const Polyhedron &in,
                                                     const Storage<Vector> &in_normals,
                                                     Polyhedron &out,
                                                     Storage<Vector> &out_normals,
                                                     const T tol) {
        // clean-up
        clear();
        out.clear();

        // static const Integer n_dims = 3;
        const Integer in_n_points = static_cast<Integer>(in.n_nodes());
        const Integer in_n_elements = static_cast<Integer>(in.n_elements());

        //////////////////////////////////////////NODES/////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////

        pp_distance.reserve(in_n_points);

        std::for_each(in.points.begin(), in.points.end(), [this, &h](const Point &p) {
            pp_distance.push_back(h.signed_dist(p));
        });

        assert(!pp_distance.empty());

        const T min_dist = *std::min_element(std::begin(pp_distance), std::end(pp_distance));
        const T max_dist = *std::max_element(std::begin(pp_distance), std::end(pp_distance));

        if (min_dist >= -tol * 100) {
            return NOT_INTERSECTED;
        }

        if (max_dist <= tol * 100) {
            return CONTAINED_USE_ORIGINAL;
        }

        node_query.resize(in_n_points);

        Integer n_points_to_remove = 0;
        for (Integer node = 0; node < in_n_points; ++node) {
            if (pp_distance[node] > tol) {
                ++n_points_to_remove;
                node_query[node] = HalfSpace::OUTSIDE;
            } else {
                if (pp_distance[node] < -tol) {
                    node_query[node] = HalfSpace::INSIDE;
                } else {
                    node_query[node] = HalfSpace::ON_PLANE;
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////ELEMENTS/////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////

        map.resize(in_n_points);

        Integer out_n_nodes = 0;
        for (Integer node = 0; node < in_n_points; ++node) {
            map[node] = (node_query[node] == HalfSpace::OUTSIDE) ? -1 : (out_n_nodes++);
        }

        for (Integer i = 0; i < in_n_points; ++i) {
            if (map[i] >= 0) {
                out.points.push_back(in.points[i]);
            }
        }

        element_query.resize(in_n_elements);
        std::fill(std::begin(element_query), std::end(element_query), TO_BE_KEPT);

        bool must_intersect = false;
        Integer n_elements_to_be_removed = 0;
        for (Integer e = 0; e < in_n_elements; ++e) {
            const Integer e_begin = in.el_ptr[e];
            const Integer e_end = in.el_ptr[e + 1];
            const Integer e_n_nodes = e_end - e_begin;

            Integer n_inside_nodes = 0;
            Integer n_outside_nodes = 0;
            Integer n_on_plane_nodes = 0;
            for (Integer k = e_begin; k != e_end; ++k) {
                const Integer node_k = in.el_index[k];
                const Integer node_kp1 = (k + 1 == e_end) ? in.el_index[e_begin] : in.el_index[k + 1];

                const Integer nq_k = node_query[node_k];
                const Integer nq_kp1 = node_query[node_kp1];

                n_outside_nodes +=
                    nq_k == HalfSpace::OUTSIDE || (nq_k == HalfSpace::ON_PLANE && nq_kp1 != HalfSpace::INSIDE);
                n_on_plane_nodes += nq_k == HalfSpace::ON_PLANE;
                n_inside_nodes += nq_k == HalfSpace::INSIDE || nq_k == HalfSpace::ON_PLANE;
            }

            if (n_inside_nodes == e_n_nodes) {
                element_query[e] = TO_BE_KEPT;
            } else if (n_on_plane_nodes == e_n_nodes) {
                element_query[e] = TO_BE_KEPT;
            } else if (n_outside_nodes == e_n_nodes) {
                element_query[e] = TO_BE_REMOVED;
                ++n_elements_to_be_removed;
            } else {
                element_query[e] = TO_BE_INTERSECTED;
                must_intersect = true;
            }
        }

        if (n_elements_to_be_removed == in_n_elements) {
            return NOT_INTERSECTED;
        }

        if (!must_intersect) {
            return CONTAINED_USE_ORIGINAL;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////INTERSECTIONS/////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////

        for (Integer i = 0; i < in_n_points; ++i) {
            if (node_query[i] == HalfSpace::ON_PLANE) {
                cut_face.push_back(map[i]);
            }
        }

        const auto size_edge_mat = symmetric_matrix_n_entries(in_n_points);
        edge_map.resize(size_edge_mat);
        std::fill(std::begin(edge_map), std::end(edge_map), -1);

        for (Integer e = 0; e < in_n_elements; ++e) {
            const Integer e_begin = in.el_ptr[e];
            const Integer e_end = in.el_ptr[e + 1];
            const Integer e_n_nodes = e_end - e_begin;

            switch (element_query[e]) {
                case TO_BE_KEPT: {
                    // face is kept in its entirety, only offsetting node ids
                    const Integer offset = out.el_ptr.back();

                    for (Integer k = e_begin; k != e_end; ++k) {
                        const auto idx = map[in.el_index[k]];
                        assert(idx >= 0);
                        out.el_index.push_back(idx);
                    }

                    out_normals.push_back(in_normals[e]);
                    out.el_ptr.push_back(offset + e_n_nodes);
                    break;
                }

                case TO_BE_INTERSECTED: {
                    mutated_face.clear();
                    const Integer first = in.el_index[e_begin];

                    if (node_query[first] != HalfSpace::OUTSIDE) {
                        assert(map[first] >=
                               0);  // "half_space_cut: inconsistent state, INVALID_INDEX instead of node index");
                        mutated_face.push_back(map[first]);
                    }

                    for (Integer k = e_begin; k != e_end; ++k) {
                        const Integer vk = in.el_index[k];
                        const bool is_first = k + 1 == e_end;
                        const Integer vkp1 = is_first ? in.el_index[e_begin] : in.el_index[k + 1];

                        const T dk = pp_distance[vk];
                        const T dkp1 = pp_distance[vkp1];

                        const bool on_plane =
                            node_query[vk] == HalfSpace::ON_PLANE || node_query[vkp1] == HalfSpace::ON_PLANE;

                        if (!on_plane && std::signbit(dk) != std::signbit(dkp1)) {
                            const Integer edge_index = symmetric_matrix_index(vk, vkp1);
                            Integer new_node = edge_map[edge_index];

                            if (new_node == -1) {
                                new_node = static_cast<Integer>(out.points.size());
                                edge_map[edge_index] = new_node;

                                Vector dir = normalize(in.points[vk] - in.points[vkp1]);

                                out.points.emplace_back();

                                Vector &isect = out.points.back();

                                const bool ok = h.intersect_plane(in.points[vk], dir, pp_distance[vk], isect, tol);

                                assert(ok);
                                (void)ok;
                                assert(h.on_plane(out.points.back(), tol));

                                cut_face.push_back(new_node);
                            }

                            mutated_face.push_back(new_node);
                        }

                        if (node_query[vkp1] != HalfSpace::OUTSIDE && !is_first) {
                            assert(map[vkp1] >= 0);
                            mutated_face.push_back(map[vkp1]);
                        }
                    }

                    Integer n_nodes_in_mutated_face = static_cast<Integer>(mutated_face.size());

                    if (n_nodes_in_mutated_face >= 3) {
                        const Integer offset = out.el_ptr.back();

                        for (Integer i = 0; i < n_nodes_in_mutated_face; ++i) {
                            assert(mutated_face[i] >= 0);
                            out.el_index.push_back(mutated_face[i]);
                        }

                        out_normals.push_back(in_normals[e]);
                        out.el_ptr.push_back(offset + n_nodes_in_mutated_face);

                    } else {
                        assert(false);
                        std::cerr << ("half_space_cut: degenerate face") << std::endl;
                    }

                    break;
                }

                case TO_BE_REMOVED: {
                    break;
                }

                default: {
                    break;
                }
            }
        }

        const Integer out_el_offset = out.el_ptr.back();

        Integer n_nodes_in_cut_face = static_cast<Integer>(cut_face.size());
        for (Integer i = 0; i < n_nodes_in_cut_face; ++i) {
            out.el_index.push_back(cut_face[i]);
        }

        out_normals.push_back(h.n);

        if (n_nodes_in_cut_face < 3 && n_nodes_in_cut_face >= 0) {
            assert(false);  // REMOVE ME
            return INTERSECTED;
        }

        // if(!sorted(
        if (!sort_polygon.apply(out.points, h.n, &out.el_index[out.el_ptr.back()], n_nodes_in_cut_face)) {
            assert(false);  // REMOVE ME
            return NOT_INTERSECTED;
        }

        out.el_ptr.push_back(out_el_offset + n_nodes_in_cut_face);

        fix_side(static_cast<int>(out.n_elements()) - 1, h.n, false, out, tol);
        assert(out.valid());
        return INTERSECTED;
    }

    // template<typename T>
    // bool IntersectPolygon3WithHPolyhedron<T>::apply(const Polygon3 &polygon, const HPolyhedron &h_poly, Polygon3
    // &result, const T &tol)
    // {
    // 	const std::size_t n_half_spaces = h_poly.half_spaces.size();
    // 	in = polygon;

    // 	for(std::size_t k = 0; k < n_half_spaces; ++k) {
    // 		const std::size_t n_points = in.points.size();
    // 		result.clear();

    // 		for(std::size_t i = 0; i < n_points; ++i) {
    // 			const auto ip1 = (i + 1 == n_points) ? 0 : (i+1);

    // 			line.p0 = in.points[i];
    // 			line.p1 = in.points[ip1];

    // 			int n_intersected = 0;
    // 			bool line_outside_h_poly = false;
    // 			bool aligned_with_separating_plane = false;

    // 			bool orginal_vertex[2] = { true, true };

    // 			auto ret = intersect_line_with_half_space.apply(line, h_poly.half_spaces[k], isect_line, tol);

    // 			switch(ret) {
    // 				case ISectLineHS::NO_INTERSECTION: {
    // 						//outside half-space
    // 					line_outside_h_poly = true;
    // 					break;
    // 				}
    // 				case ISectLineHS::SEGMENT_FROM_POINT_0:
    // 				{
    // 					orginal_vertex[1] = false;
    // 					line = isect_line;
    // 					++n_intersected;
    // 					break;
    // 				}
    // 				case ISectLineHS::SEGMENT_FROM_POINT_1:
    // 				{
    // 						//intersection with separating plane
    // 					orginal_vertex[0] = false;
    // 					line = isect_line;
    // 					++n_intersected;
    // 					break;
    // 				}

    // 				case ISectLineHS::INSIDE: {
    // 						//completely inside
    // 						// line3 = isect_line; //they are the same
    // 					break;
    // 				}

    // 				case ISectLineHS::ON_BOUNDARY: {
    // 					aligned_with_separating_plane = true;
    // 					break;
    // 				}
    // 			}

    // 			if(line_outside_h_poly) {
    // 					//the segment is to be skipped and not to be used
    // 				continue;
    // 			}

    // 			result.points.push_back(isect_line.p0);

    // 			if(n_intersected && !orginal_vertex[1]) {
    // 				result.points.push_back(isect_line.p1);
    // 			}
    // 		}

    // 		result.remove_duplicate_points(tol);
    // 		if(result.points.size() < 3) return false;

    // 		in = result;
    // 	}

    // 	return (result.points.size() >= 3);
    // }

    template <typename T>
    bool IntersectConvexPolyhedronWithHPolyhedron<T>::apply(const HPolyhedron &h_poly,
                                                            Polyhedron &in_polyhedron,
                                                            const T &tol) {
        const int n_half_spaces = static_cast<int>(h_poly.half_spaces.size());

        in_polyhedron.fix_ordering_and_get_normals(poly_normals);

        if (h_poly.all_outside(in_polyhedron.points, 0.0)) {
            return false;
        }

        // handles
        auto in_ptr = &in_polyhedron;
        auto out_ptr = &isect_polyhedron;
        auto in_normals = &poly_normals;
        auto out_normals = &isect_normals;

        // p_mesh_print(in_ptr);
        for (int j = 0; j < n_half_spaces; ++j) {
            switch (intersect_poly_with_halfspace.apply(
                h_poly.half_spaces[j], *in_ptr, *in_normals, *out_ptr, *out_normals, tol)) {
                case IsectPolyWithHSpace::CONTAINED_USE_ORIGINAL: {
                    break;
                }
                case IsectPolyWithHSpace::INTERSECTED: {
                    std::swap(in_ptr, out_ptr);
                    std::swap(in_normals, out_normals);
                    break;
                }
                case IsectPolyWithHSpace::NOT_INTERSECTED: {
                    return false;
                }
                default: {
                    assert(false);
                    break;
                }
            }
        }

        if (&in_polyhedron != in_ptr) {
            std::swap(in_polyhedron, isect_polyhedron);
            // std::swap(in_normals, isect_normals);
        }

        return in_polyhedron.n_elements() >= 4;
    }

}  // namespace moonolith

#endif  // MOONOLITH_INTERTSCT_POLYHEDRA_IMPL_HPP
