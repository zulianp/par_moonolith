#ifndef MOONOLITH_MATCH_HPP
#define MOONOLITH_MATCH_HPP

#include "moonolith_mutator.hpp"
#include "moonolith_node.hpp"
#include "moonolith_leaf.hpp"
#include "moonolith_branch.hpp"
#include "moonolith_iterable.hpp"

#include <memory>

namespace moonolith {

	template <class Tree>
	class Match : public Mutator<typename Tree::Traits> {
	public:
		typedef typename Tree::Traits Traits;
		typedef typename Traits::DataType DataType;
		typedef moonolith::Node<Traits> Node;
		typedef moonolith::Branch<Traits> Branch;
		typedef moonolith::Root<Traits> Root;
		typedef moonolith::Leaf<Traits> Leaf;
		typedef moonolith::Visitor<Traits> Visitor;

		typedef moonolith::TreeMemory<Traits> TreeMemory;

	private:
		typedef moonolith::IterableSet<DataHandle> IterableSet;
		typedef typename IterableSet::Container MatchSet;

	public:
		typedef typename MatchSet::const_iterator Iter;


		virtual bool match(Node &node) const = 0;
		virtual bool match(DataType &object) const = 0;

		// virtual bool mustContinue() const
		// {
		// 	return true;
		// }
		
		virtual bool can_refine(Leaf &leaf, TreeMemory &memory) const
		{
			if(leaf.empty()) return false;
			std::shared_ptr< Mutator<Traits> > m_can_refine = memory.mutator_factory()->new_can_refine();
			leaf.accept(*m_can_refine, memory);
			return m_can_refine->success();
		}

		inline bool success() const override { return !matches().empty(); }
		inline const Iter begin() { return matches().begin(); }
		inline const Iter end() { return matches().end(); }

		inline bool empty() const { return matches().empty(); }
		inline Integer n_matches() const { return matches().size(); }
		
		void insertMatch(const DataHandle &handle)
		{
			matches().insert(handle);
		}

		virtual NavigatorOption visit(Root &root, TreeMemory &memory) override { 
			return visit(static_cast< Branch &>(root), memory); 
		}

		inline NavigatorOption visit(Leaf &leaf, TreeMemory &memory) override { 
			if(leaf.empty()) {
				return CONTINUE;
			}

			if(!match(leaf)) {
				return CONTINUE;
			}

			if (can_refine(leaf, memory)) {
				return REFINE;
			}

			collect(leaf.data_begin(), leaf.data_end(), memory);
			return CONTINUE; 
		}

		inline NavigatorOption visit(Branch &branch, TreeMemory &memory) override { 
			if(!match(branch)) {
				return SKIP_SUBTREE;
			}

			collect(branch.data_begin(), branch.data_end(), memory);
			return CONTINUE; 
		}

		std::shared_ptr< Iterable<DataHandle> > result()
		{
			return _matches;
		}

		virtual ~Match() {}
		Match()
		:_matches(std::make_shared<IterableSet>())
		{}

	private:

		inline MatchSet &matches()
		{
			return _matches->container();
		}
		
		inline  const MatchSet &matches() const
		{
			return _matches->container();
		}
		
		std::shared_ptr<IterableSet> _matches;


		template<class DataIter>
		inline void collect(DataIter begin, DataIter end, TreeMemory &memory)
		{
			for (DataIter it = begin; it != end; ++it) {
				if(match(memory.data(*it))) { 
					matches().insert(*it);
				}
			}
		}
	};
}

#endif //MOONOLITH_MATCH_HPP
