#include "moonolith_map_quadrature_impl.hpp"

namespace moonolith {
    template class Quadrature<Real, 1>;
    template class Quadrature<Real, 2>;
    template class Quadrature<Real, 3>;
}  // namespace moonolith
