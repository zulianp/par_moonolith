// general
#include "par_moonolith.hpp"

// specific
#include "moonolith_mesh_io.hpp"
#include "moonolith_par_l2_assembler_no_covering.hpp"
#include "moonolith_vtu_mesh_writer.hpp"

// stl
#include <fstream>
#include <string>

template <typename T>
bool read_vector(const std::string &path, std::vector<T> &v) {
    v.clear();

    std::ifstream is(path.c_str());

    if (!is.good()) {
        is.close();
        moonolith::error_logger() << "Failed to read from " << path << std::endl;
        return false;
    }

    while (is.good()) {
        std::string line;
        std::getline(is, line);
        if (line.empty()) continue;

        v.push_back(atof(line.c_str()));
    }

    is.close();
    return true;
}

template <typename T>
bool write_vector(const std::string &path, std::vector<T> &v) {
    std::ofstream os(path.c_str());

    if (!os.good()) {
        os.close();
        moonolith::error_logger() << "Failed to write to " << path << std::endl;
        return false;
    }

    for (auto val : v) {
        os << val << "\n";
    }

    os.close();
    return true;
}

/// Compile examples with `make moonolith_examples`
/// Run for instance with  `mpiexec -np 2 ./examples/example_variational_transfer_no_covering`
/// Or
/// `./examples/example_variational_transfer_no_covering src_mesh.tri dest_mesh.tri src_function.txt dest_function.txt`
int main(int argc, char *argv[]) {
    using namespace moonolith;

    static const int Dim = 2;
    using MeshT = moonolith::Mesh<Real, Dim>;

    Moonolith::Init(argc, argv);
    {
        std::string master_path = "../examples/data/triangle_4.tri";
        std::string slave_path = "../examples/data/square_2.tri";
        std::string master_function_path = "";
        std::string slave_function_path = "";

        if (argc >= 3) {
            master_path = argv[1];
            slave_path = argv[2];
            logger() << "using:\nmaster:\n" << master_path << "\nslave:\n" << slave_path << std::endl;
        }

        bool function_io = false;
        if (argc == 5) {
            function_io = true;
            master_function_path = argv[3];
            slave_function_path = argv[4];
        }

        /////////////////////////////////////////////////
        // Set-up (Usually the mesh is created by converting the client code representation)
        /////////////////////////////////////////////////

        Communicator comm;
        auto master_mesh = std::make_shared<MeshT>(comm);
        auto slave_mesh = std::make_shared<MeshT>(comm);

        // Mesh is read only on one process
        if (comm.is_root()) {
            TriMeshReader<MeshT> reader;

            if (!reader.read(master_path, *master_mesh)) {
                error_logger() << "Could not read mesh" << std::endl;
                Moonolith::Abort();
            }

            if (!reader.read(slave_path, *slave_mesh)) {
                error_logger() << "Could not read mesh" << std::endl;
                Moonolith::Abort();
            }
        }

        // master_mesh->scale(1.05);

        master_mesh->finalize();
        slave_mesh->finalize();

        FunctionSpace<MeshT> master(master_mesh), slave(slave_mesh);

        //! make sure that this refelects the dof paritioning of your function space
        Integer n_master_local_dofs = master_mesh->n_nodes();
        Integer n_slave_local_dofs = slave_mesh->n_nodes();

        // Same parametrization for the space as for the mesh
        // In this case is linear triangular finite elements
        master.make_iso_parametric();
        slave.make_iso_parametric();

        master.dof_map().set_n_local_dofs(n_master_local_dofs);
        slave.dof_map().set_n_local_dofs(n_slave_local_dofs);

        /////////////////////////////////////////////////
        // Compute
        /////////////////////////////////////////////////

        // ParL2Transfer<Real, 2> transfer(comm); // (If the source domain covers the destination domain, use this
        // instead).
        ParL2TransferNoCovering<Real, 2> transfer(comm);
        transfer.assemble(master, slave);

        /////////////////////////////////////////////////
        // Post-process (Usually the matrices are converted and used in the client library representation)
        /////////////////////////////////////////////////

        // For P1 elements, transformation_matrix is the identity, for higher order it is a basis transformation
        const auto &mass_matrix = transfer.mass_matrix();
        const auto &coupling_matrix = transfer.coupling_matrix();

        auto inv_mass_matrix = mass_matrix;

        inv_mass_matrix.remove_zeros(1e-14);

        inv_mass_matrix.transform([](const Real &x) -> Real {
            if (x > 1e-14) {
                return 1. / x;
            } else {
                return 0.0;
            }
        });

        const auto &transformation_matrix = transfer.transformation_matrix();

        // inv_mass_matrix.synch_describe(logger());
        // coupling_matrix.synch_describe(logger());
        // transformation_matrix.synch_describe(logger());

        // The client library will have to convert the three matrices to its format
        // The transfer operator T(.) = transformation_matrix * (mass_matrix \ (coupling_matrix * ( . )))
        // When using the biorthognal basis mass_matrix is diagonal so T can be assembled explicitly
        // T can then be used for transfering fields from the master space to the slave space

        const Real sum_mm = mass_matrix.sum();
        const Real sum_cm = coupling_matrix.sum();
        const Real sum_tm = transformation_matrix.sum();

        if (comm.is_root()) {
            // These conditions must be true
            logger() << sum_mm << " == " << sum_cm << " == expected_value: 0.5" << std::endl;
            logger() << sum_tm << " <= expected_value: " << slave_mesh->n_nodes() << std::endl;
        }

        // only for serial meshes we can test this. No parallel matrix-vector multiplication is currently avaialble in
        // moonolith. Since the mesh is completely owned by the root process we can do the following
        if (comm.is_root()) {
            std::vector<Real> from(n_master_local_dofs, 1.0);
            std::vector<Real> to(n_slave_local_dofs, 0.0);
            std::vector<Real> temp(n_slave_local_dofs, 0.0);
            std::vector<Real> temp2(n_slave_local_dofs, 0.0);

            if (master_function_path.empty()) {
                for (Integer i = 0; i < master_mesh->n_nodes(); ++i) {
                    const auto &p = master_mesh->point(i);
                    from[i] = p(0) * p(0) + p(1) * p(1);
                }
            } else {
                if (read_vector(master_function_path, from)) {
                    std::cout << "Read input from " << master_function_path << " n  = " << from.size()
                              << " == " << n_master_local_dofs << std::endl;
                    assert(from.size() == n_master_local_dofs);
                }
            }

            coupling_matrix.serial_multiply(from, temp);
            inv_mass_matrix.serial_multiply(temp, temp2);
            transformation_matrix.serial_multiply(temp2, to);

            VTUMeshWriter<MeshT>().write("slave.vtu", *slave_mesh, to);
            VTUMeshWriter<MeshT>().write("master.vtu", *master_mesh, from);

            if (!slave_function_path.empty()) {
                if (write_vector(slave_function_path, to)) {
                    std::cout << "Wrote output to " << slave_function_path << std::endl;
                }
            }
        }
    }

    return Moonolith::Finalize();
}
