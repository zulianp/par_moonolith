#ifndef INTERPOLATION_CL
#define INTERPOLATION_CL 

#include "fe_interfaces.cl"

void integrate_value_at_quad_index_and_store_weight(const SizeType quad_index, const FEObject *fe, const Scalar value, Scalar *cumulative_result)
{
	for(SizeType i = 0; i < fe->n_shape_functions; ++i) {
		const Scalar w = fe->fun[i][quad_index] * fe->dx[quad_index];
		const SizeType i2 = i*2;
		cumulative_result[i2]     += w * value;
		cumulative_result[i2 + 1] += w;
	}
}

void integrate_vec_at_quad_index(const SizeType quad_index, const FEObject *fe, const SizeType n_codims, const Scalar *value, Scalar *cumulative_result)
{
	for(SizeType i = 0; i < fe->n_shape_functions; ++i) {
		const Scalar w = fe->fun[i][quad_index] * fe->dx[quad_index];
		const SizeType offset = i * n_codims;

		for(SizeType d = 0; d < n_codims; ++d) {
			cumulative_result[offset + d] += w * value[d];
		}
	}
}

#endif //INTERPOLATION_CL

