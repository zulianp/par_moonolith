#include "moonolith_mesh.hpp"

namespace moonolith {
    template class Mesh<Real, 1>;
    template class Mesh<Real, 2>;
    template class Mesh<Real, 3>;
}  // namespace moonolith
