#ifndef MOONOLITH_SEARCH_RADIUS_HPP
#define MOONOLITH_SEARCH_RADIUS_HPP

#include "par_moonolith_config.hpp"
#include "moonolith_describable.hpp"
#include <map>

namespace moonolith {

    template<typename T>
    class SearchRadius final : public Describable {
    public:

        inline SearchRadius(const T &default_radius = 0.01)
        : default_radius_(default_radius) {}

        inline T get(const Integer tag) const
        {
            auto it = radii_.find(tag);
            if(it == radii_.end()) {
                return default_radius_;
            } else {
                return it->second;
            }
        }

        inline void insert(const Integer tag, T radius)
        {
            radii_[tag] = radius;
        }


        inline void describe(std::ostream &os) const override
        {
            os << "default-radius: " << default_radius_ << "\n";
            for(auto p : radii_) {
                os << p.first << ") " << p.second << "\n";
            }
        }

    public:
        T default_radius_;
        std::map<Integer, T> radii_;
    };

}

#endif //MOONOLITH_SEARCH_RADIUS_HPP
