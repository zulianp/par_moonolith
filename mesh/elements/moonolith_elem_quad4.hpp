#ifndef MOONOLITH_ELEM_QUAD_4_HPP
#define MOONOLITH_ELEM_QUAD_4_HPP

#include "moonolith_elem_quad.hpp"

namespace moonolith {

    template <typename T_, int PhysicalDim_ = 2>
    class Quad4 final : public Quad<T_, 1, PhysicalDim_> {
    public:
        static const int Order = 1;
        static const int Dim = 2;
        static const int PhysicalDim = PhysicalDim_;
        static const int NNodes = 4;

        using T = T_;

        using Vector = moonolith::Vector<T, Dim>;
        using CoVector = moonolith::Vector<T, PhysicalDim>;

        using Point = moonolith::Vector<T, Dim>;
        using CoPoint = moonolith::Vector<T, PhysicalDim>;

        using Super = moonolith::Quad<T_, 1, PhysicalDim_>;
        using Polygon = typename Super::Polygon;

    private:
        class Fun final {
        public:
            Fun() {
                f[0] = [](const Point &p) -> T { return (1.0 - p.x) * (1.0 - p.y); };

                f[1] = [](const Point &p) -> T { return p.x * (1.0 - p.y); };

                f[2] = [](const Point &p) -> T { return p.x * p.y; };

                f[3] = [](const Point &p) -> T { return (1.0 - p.x) * p.y; };
            }

            std::array<std::function<T(const Point &)>, 4> f;
        };

        class Grad final {
        public:
            Grad() {
                // f:= (1.0 - p.x) * (1.0 - p.y)
                f[0] = [](const Point &p, Vector &g) {
                    g.x = p.y - 1.0;
                    g.y = p.x - 1.0;
                };

                // f:= p.x * (1.0 - p.y)
                f[1] = [](const Point &p, Vector &g) {
                    g.x = 1.0 - p.y;
                    g.y = -p.x;
                };

                // f := p.x * p.y
                f[2] = [](const Point &p, Vector &g) {
                    g.x = p.y;
                    g.y = p.x;
                };

                // f := (1.0 - p.x) * p.y
                f[3] = [](const Point &p, Vector &g) {
                    g.x = -p.y;
                    g.y = 1.0 - p.x;
                };
            }

            std::array<std::function<void(const Point &, Vector &)>, 4> f;
        };

        class Hessian final {
        public:
            Hessian() {
                // f:= (1.0 - p.x) * (1.0 - p.y)
                // g.x = p.y - 1.0;
                // g.y = p.x - 1.0;
                f[0] = [](const Point &, std::array<T, Dim * Dim> &H) {
                    H[0] = 0.0;
                    H[1] = 1.0;
                    H[2] = 1.0;
                    H[3] = 0.0;
                };

                // f:= p.x * (1.0 - p.y)
                // g.x = 1.0 - p.y;
                // g.y = -p.x;
                f[1] = [](const Point &, std::array<T, Dim * Dim> &H) {
                    H[0] = 0.0;
                    H[1] = -1.0;
                    H[2] = -1.0;
                    H[3] = 0.0;
                };

                // g.x = p.y;
                // g.y = p.x;
                f[2] = [](const Point &, std::array<T, Dim * Dim> &H) {
                    H[0] = 0.0;
                    H[1] = 1.0;
                    H[2] = 1.0;
                    H[3] = 0.0;
                };

                // f := (1.0 - p.x) * p.y
                // g.x = -p.y;
                // g.y =  1.0 - p.x;
                f[3] = [](const Point &, std::array<T, Dim * Dim> &H) {
                    H[0] = 0.0;
                    H[1] = -1.0;
                    H[2] = -1.0;
                    H[3] = 0.0;
                };
            }

            std::array<std::function<void(const Point &, std::array<T, Dim * Dim> &)>, 4> f;
        };

    public:
        T fun(const Integer i, const Point &p) const override { return fun_.f[i](p); }

        void grad(const Integer i, const Point &p, Vector &g) const override { grad_.f[i](p, g); }

        void hessian(const Integer i, const Point &p, std::array<T, Dim * Dim> &H) { hessian_.f[i](p, H); }

        void jacobian(const Point &p, std::array<T, PhysicalDim * Dim> &J) const override {
            if (is_affine()) {
                affine_approx_jacobian(J);
                return;
            }

            IsoParametric<Quad4>::jacobian(*this, p, J);
        }

        void affine_approx_jacobian(std::array<T, PhysicalDim * Dim> &J) const override {
            const auto &p0 = node(0);

            for (int i = 0; i < PhysicalDim; ++i) {
                const int i_offset = i * Dim;
                J[i_offset] = node(1)[i] - p0[i];
                J[i_offset + 1] = node(3)[i] - p0[i];
            }
        }

        CoPoint &node(const Integer i) override { return nodes_[i]; }

        const CoPoint &node(const Integer i) const override { return nodes_[i]; }

        void point(const Point &p, CoPoint &q) const override { IsoParametric<Quad4>::point(*this, p, q); }

        Quad4() : is_affine_(false) {}

        bool is_affine() const override { return is_affine_; }
        inline int order() const override { return Order; }

        void set_affine(const bool val) { is_affine_ = val; }

        inline void make_reference() {
            Reference<Quad4>::points(nodes_);
            set_affine(true);
        }

        inline int n_nodes() const override { return NNodes; }

        inline ElemType type() const override { return QUAD4; }

        inline bool is_simplex() const override { return false; }

        inline T measure() const override {
            if (is_affine()) {
                return trapezoid_area(nodes_[0], nodes_[1], nodes_[3]);
            } else {
                Quadrature<T, Dim> q;
                Gauss::Quad::get(1, q);
                return IsoParametric<Quad4>::measure(*this, q);
            }
        }

        inline T approx_measure() const override { return trapezoid_area(nodes_[0], nodes_[1], nodes_[3]); }

        inline T reference_measure() const override { return 1.0; }

        inline void make(Polygon &poly) const override {
            poly.resize(4);

            for (int i = 0; i < 4; ++i) {
                poly[i] = node(i);
            }
        }

        inline std::array<CoPoint, 4> &nodes() { return nodes_; }

    private:
        std::array<CoPoint, 4> nodes_;
        const Fun fun_;
        const Grad grad_;
        const Hessian hessian_;
        bool is_affine_;
    };

    template <typename T, int PhysicalDim>
    class Reference<Quad4<T, PhysicalDim>> {
    public:
        // using Point = typename Quad4<T, 1, PhysicalDim>::Point;
        static const int NNodes = 4;

        template <class Point, std::size_t Size>
        static void points(std::array<Point, Size> &nodes) {
            static_assert(Size >= 4, "size must be at least 4");
            // p0
            nodes[0].x = 0.0;
            nodes[0].y = 0.0;

            // p1
            nodes[1].x = 1.0;
            nodes[1].y = 0.0;

            // p2
            nodes[2].x = 1.0;
            nodes[2].y = 1.0;

            // p3
            nodes[3].x = 0.0;
            nodes[3].y = 1.0;
        }

        inline static T measure() { return 1.0; }
    };

    template <typename T, int PhysicalDim>
    class GaussQRule<Quad4<T, PhysicalDim>> {
    public:
        static bool get(const Integer order, Quadrature2<T> &q) { return Gauss::Quad::get(order, q); }
    };

}  // namespace moonolith

#endif  // MOONOLITH_ELEM_QUAD_4_HPP
