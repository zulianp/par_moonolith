#ifndef FE_INTERFACES_CL




#include "fe_interfaces_2.cl"
#include "fe_interfaces_3.cl"
#include "mesh_interface.cl"
#include "fe_values_interface.cl"

//Types
#include "fe_object.cl"


#ifndef CLIPP_HOST_CL
//functions
void init_invalid(FEObject * fe);
void fe_object_print(FEObject *fe);
void make_inverses_and_dets_2(const SizeType n_quad_points, const Scalar * jacobians, Scalar *inverse_jacobians, Scalar *dets);
void make_inverses_and_dets_3(const SizeType n_quad_points, const Scalar * jacobians, Scalar *inverse_jacobians, Scalar *dets);
void transform_gradients(const SizeType dims, const SizeType n_quad_points, const Scalar *inverse_jacobians, Scalar *gradient);

void make_dx_d(const Scalar * weights, FEObject * fe);

void integrate_value_at_quad_index_and_store_weight(const SizeType quad_index, const FEObject *fe, const Scalar value, Scalar *cumulative_result);
void integrate_vec_at_quad_index(const SizeType quad_index, const FEObject *fe, const SizeType n_codims, const Scalar *value, Scalar *cumulative_result);


bool make_fe_object_from_values(
	const SizeType n_dims,
	const SizeType n_shape_functions,
	const SizeType n_quad_points,
	const m_global__ Scalar   * functions,		
	const m_global__ Scalar   * gradients,			
	const m_global__ Scalar   * jacobians,			
	const m_global__ Scalar   * global_points,     
	const m_global__ Scalar   * dxs,	
	FEObject * fe);




//Deformation.cl
void make_def_grad(const SizeType dim, const Scalar *displacement_gradient, Scalar * deformation_gradient);



#endif //CLIPP_HOST_CL
#endif //FE_INTERFACES_CL
