#ifndef MOONOLITH_NTREE_WITH_TAG_INSERT_HPP
#define MOONOLITH_NTREE_WITH_TAG_INSERT_HPP 

#include "moonolith_n_tree_with_span_insert.hpp"

#include <algorithm> 

namespace moonolith {

	template<class Tree>
	class NTreeWithTagsInsert : public NTreeWithSpanInsert<Tree> {
	public:
		typedef typename Tree::Traits Traits;
		typedef typename Traits::DataType DataType;

		typedef moonolith::Node<Traits> Node;
		typedef moonolith::Branch<Traits> Branch;
		typedef moonolith::Root<Traits> Root;
		typedef moonolith::Leaf<Traits> Leaf;
		typedef moonolith::Visitor<Traits> Visitor;

		typedef moonolith::TreeMemory<Traits> TreeMemory;

		virtual bool match(const Node &node) const override
		{
			return this->object().bound().static_bound().intersects(node.bound().static_bound());
		}

		inline bool has_tag(const Node &node, const Integer tag) const 
		{
			return std::find (node.tags().begin(), node.tags().end(), tag) != node.tags().end();
		}

		virtual bool must_insert(Leaf &, TreeMemory &) override {
			return true;
		}

		virtual void mutate(Node &node, TreeMemory &memory) const override {
			NTreeWithSpanInsert<Tree>::mutate(node, memory);
			
			if(!has_tag(node, this->object().tag())) {
				node.tags().push_back(this->object().tag());
			}
		}

		explicit NTreeWithTagsInsert(const DataHandle &handle, const DataType &obj)
		: NTreeWithSpanInsert<Tree>(handle, obj)
		{}
	};
	
}

#endif //MOONOLITH_NTREE_WITH_TAG_INSERT_HPP
