#ifndef MOONOLITH_ROOT_HPP
#define MOONOLITH_ROOT_HPP

#include "moonolith_forward_declarations.hpp"

namespace moonolith {

    template <class Traits>
    class Root : public Branch<Traits> {
    private:
        typedef moonolith::Branch<Traits> super;

    public:
        using super::accept;

        NavigatorOption accept(Visitor<Traits> &visitor, TreeMemory<Traits> &memory) override {
            return visitor.visit(*this, memory);
        }

        bool is_root() const override { return true; }

        Root() {
            this->set_level(0);
            this->set_handle(0);
            this->set_global_handle(0);
        }

        std::string get_class() const override { return "Root"; }
    };
}  // namespace moonolith

#endif  // MOONOLITH_ROOT_HPP
