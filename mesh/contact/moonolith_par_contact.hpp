#ifndef MOONOLITH_PAR_CONTACT_HPP
#define MOONOLITH_PAR_CONTACT_HPP

#include "moonolith_duplicate_intersection_avoidance.hpp"
#include "moonolith_one_master_one_slave_algorithm.hpp"
#include "moonolith_single_collection_one_master_one_slave_algorithm.hpp"
#include "moonolith_sparse_matrix.hpp"
#include "par_moonolith_config.hpp"

#include "moonolith_collection_manager.hpp"
#include "moonolith_contact.hpp"
#include "moonolith_function_space.hpp"
#include "moonolith_gauss_quadrature_rule.hpp"
#include "moonolith_incomplete_intersection_remover.hpp"
#include "moonolith_is_glue.hpp"
#include "moonolith_keast_quadrature_rule.hpp"
#include "moonolith_matrix_inserter.hpp"
#include "moonolith_search_radius.hpp"

#include <tuple>
#include <vector>

#include <mpi.h>

namespace moonolith {

    template <typename T>
    class ContactBuffers {
    public:
        ContactBuffers(Communicator &comm)
            : B(comm),
              D(comm),
              Q(comm, false),
              Q_inv(comm, false),
              gap(comm),
              normal(comm),
              is_glue(comm, false),
              is_contact(comm, false),
              incomplete_remover(comm) {}

        void clear() {
            B.clear();
            D.clear();
            Q.clear();
            Q_inv.clear();
            gap.clear();
            normal.clear();
            is_glue.clear();
            incomplete_remover.clear();
            is_contact.clear();
        }

        MatrixInserter<T> B, D, Q, Q_inv;
        MatrixInserter<T> gap, normal;
        MatrixInserter<T> is_glue;
        MatrixInserter<T> is_contact;

        inline MatrixInserter<T> &measure() { return incomplete_remover.measure; }

        // used to remove incomplete intersections
        IncompleteIntersectionRemover<T> incomplete_remover;
    };

    template <class MasterElem, class SlaveElem>
    class LocalMortarContactAssembler {
    public:
        using T = typename SlaveElem::T;
        static const int Dim = SlaveElem::Dim;
        // static const int PhysicalDim = SlaveElem::PhysicalDim;

        using QuadratureT = moonolith::Quadrature<T, Dim>;
        using ContactMortarT = moonolith::ContactMortar<MasterElem, SlaveElem>;

        template <class Elem>
        static int order_of_quadrature(const Elem &e) {
            if (e.is_simplex()) {
                return e.order();
            } else {
                return e.order() * Dim;
            }
        }

        void init_quadrature() {
            int master_order = order_of_quadrature(*master_elem);
            int slave_order = order_of_quadrature(*slave_elem);

            int order = 0;
            if (slave_elem->is_affine()) {
                order = master_order + slave_order;
            } else {
                // assumes tensor product
                order = master_order + slave_order + (slave_order - 1) * 2;
            }

            if (order == current_quadrature_order_) return;

            if (slave_elem->is_affine() && master_elem->is_affine()) {
                Gauss::get(order, algo.q_rule_affine());
                assert(!algo.q_rule_affine().empty());
            } else {
                Gauss::get(order, algo.q_rule_warped());
                assert(!algo.q_rule_warped().empty());
            }
        }

        template <class ElemAdapter>
        inline bool operator()(const ElemAdapter &master, const ElemAdapter &slave) {
            auto &e_m = master.elem();
            auto &e_s = slave.elem();

            auto &m_m = master.collection();
            auto &m_s = slave.collection();

            const auto &dof_object_s = m_s.dof_map().dof_object(slave.element_handle());

            auto &dof_m = m_m.dof_map().dofs(master.element_handle());
            auto &dof_s = dof_object_s.dofs;

            make(m_m, e_m, master_elem);
            make(m_s, e_s, slave_elem);

            init_quadrature();

            if (algo.assemble(*master_elem, *slave_elem)) {
                const auto &B_e = algo.coupling_matrix();
                const auto &D_e = algo.mass_matrix();
                const auto &Q_e = algo.transformation();
                const auto &Q_inv_e = algo.inverse_transformation();
                const auto &g_e = algo.gap_vector();
                const auto &n_e = algo.normal_vector();

                buffers.B.insert(dof_s, dof_m, B_e);
                buffers.D.insert(dof_s, dof_s, D_e);
                buffers.Q.insert(dof_s, dof_s, Q_e);
                buffers.Q_inv.insert(dof_s, dof_s, Q_inv_e);

                // measure of intersection
                buffers.measure().insert(dof_object_s.element_dof, D_e.sum());

                // gap and normal
                buffers.gap.insert(dof_s, g_e);
                buffers.normal.insert_tensor_product_idx(dof_s, slave_elem->codim(), n_e);

                auto master_tag = master.tag();
                auto slave_tag = slave.tag();

                if (is_glue_ && (*is_glue_)(master_tag, slave_tag)) {
                    buffers.is_glue.insert(dof_s, 1.0);
                }

                return true;
            } else {
                return false;
            }
        }

        void clear() { algo.clear(); }

        void set_is_glue(const std::shared_ptr<IsGlue> &is_glue) { is_glue_ = is_glue; }

        LocalMortarContactAssembler(ContactBuffers<T> &buffers, const bool invert_plane_dir)
            : algo(invert_plane_dir), buffers(buffers), current_quadrature_order_(-1) {}

        ContactMortarT algo;

    private:
        QuadratureT q_rule;
        std::shared_ptr<MasterElem> master_elem;
        std::shared_ptr<SlaveElem> slave_elem;
        ContactBuffers<T> &buffers;
        int current_quadrature_order_;

        std::shared_ptr<IsGlue> is_glue_;
    };

    template <typename T, Integer PhysicalDim>
    class ParContact {
    public:
        using MeshT = moonolith::Mesh<T, PhysicalDim>;
        using SpaceT = moonolith::FunctionSpace<MeshT>;

        using SpaceAlgorithmT = moonolith::OneMasterOneSlaveAlgorithm<PhysicalDim, SpaceT>;
        using SingleSpaceAlgorithmT = moonolith::SingleCollectionOneMasterOneSlaveAlgorithm<PhysicalDim, SpaceT>;

        using ElemAdapter = typename SpaceAlgorithmT::Adapter;
        using SingleElemAdapter = typename SingleSpaceAlgorithmT::Adapter;

        using MasterElem = moonolith::Elem<T, PhysicalDim - 1, PhysicalDim>;
        using SlaveElem = moonolith::Elem<T, PhysicalDim - 1, PhysicalDim>;

        ParContact(Communicator &comm, const bool invert_plane_dir)
            : comm(comm), buffers(comm), local_assembler(buffers, invert_plane_dir) {}

        void post_process(const SpaceT &space) {
            auto &remover = buffers.incomplete_remover;
            remover.determine_complete(space);

            const Integer n_local_dofs = static_cast<Integer>(space.dof_map().n_local_dofs());

            // first finalize the buffers
            buffers.B.finalize(n_local_dofs, n_local_dofs);
            buffers.D.finalize(n_local_dofs, n_local_dofs);
            buffers.Q.finalize(n_local_dofs, n_local_dofs);
            buffers.Q_inv.finalize(n_local_dofs, n_local_dofs);
            buffers.B.finalize(n_local_dofs, n_local_dofs);
            buffers.gap.finalize(n_local_dofs);
            buffers.is_glue.finalize(n_local_dofs);
            buffers.normal.finalize(n_local_dofs * PhysicalDim);

            remover.remove_incomplete(buffers.B.get());
            remover.remove_incomplete(buffers.D.get());
            remover.remove_incomplete(buffers.Q.get());
            remover.remove_incomplete(buffers.Q_inv.get());
            remover.remove_incomplete(buffers.gap.get());
            remover.remove_incomplete(buffers.is_glue.get());
            remover.remove_incomplete_tensor_product(PhysicalDim, buffers.normal.get());

            remover.mark_complete(space, buffers.is_contact);
        }

        void clear() {
            buffers.clear();
            local_assembler.clear();
        }

        bool assemble(const std::vector<std::pair<int, int>> &tags,
                      const SpaceT &space,
                      const T search_radius,
                      const std::shared_ptr<IsGlue> &is_glue = nullptr) {
            return assemble(tags, space, std::make_shared<moonolith::SearchRadius<T>>(search_radius), is_glue);
        }

        bool assemble(const std::vector<std::pair<int, int>> &tags,
                      const SpaceT &space,
                      const std::shared_ptr<moonolith::SearchRadius<T>> search_radius,
                      const std::shared_ptr<IsGlue> &is_glue = nullptr) {
            comm.barrier();
            T elapsed = MPI_Wtime();

            clear();

            SingleSpaceAlgorithmT algo(comm, moonolith::make_unique<CollectionManager<SpaceT>>(comm));

            algo.init(space, tags, search_radius);

            ////////////////////////////////////////////////////
            /////////////////// pair-wise method ///////////////////////

            local_assembler.set_is_glue(is_glue);

            algo.compute([&](const ElemAdapter &master, const ElemAdapter &slave) -> bool {
                if (local_assembler(master, slave)) {
                    return true;
                }

                return false;
            });

            post_process(space);

            T meas = local_assembler.algo.projection_measure();

            comm.all_reduce(&meas, 1, MPISum());

            T sum_mat_B = buffers.B.get().sum();
            T sum_mat_D = buffers.D.get().sum();
            T sum_mat_Q = buffers.Q.get().sum();
            T sum_mat_Q_inv = buffers.Q_inv.get().sum();

            comm.barrier();
            elapsed = MPI_Wtime() - elapsed;

            if (Moonolith::instance().verbose()) {
                logger() << "time ParContact::assemble: " << elapsed << std::endl;
                logger() << "meas: " << meas << " sum(B): " << sum_mat_B << " sum(D): " << sum_mat_D
                         << " sum(Q): " << sum_mat_Q << " sum(Q_inv): " << sum_mat_Q_inv << std::endl;
            }

            return meas > 0.0;
        }

        Communicator &comm;
        ContactBuffers<T> buffers;
        LocalMortarContactAssembler<MasterElem, SlaveElem> local_assembler;
    };

}  // namespace moonolith

#endif  // MOONOLITH_PAR_CONTACT_HPP
