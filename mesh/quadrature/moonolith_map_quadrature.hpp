#ifndef MOONOLITH_MAP_QUADRATURE_HPP
#define MOONOLITH_MAP_QUADRATURE_HPP

#include "moonolith_expanding_array.hpp"
#include "moonolith_line.hpp"
#include "moonolith_polygon.hpp"
#include "moonolith_polyhedron.hpp"
#include "moonolith_vector.hpp"

namespace moonolith {

    template <typename T, int Dim>
    class Quadrature {
    public:
        using Point = moonolith::Vector<T, Dim>;

        inline Integer n_points() const { return static_cast<Integer>(points.size()); }

        inline void rescale_weights(const T &scale) {
            for (auto &w : weights) {
                w *= scale;
            }
        }

        inline void normalize(const T &scale = 1.0) { rescale_weights(1. / measure(*this) * scale); }

        inline void resize(const std::size_t &n) {
            points.resize(n);
            weights.resize(n);
        }

        inline bool empty() const { return points.empty(); }

        inline Point &point(const std::size_t i) {
            assert(i < n_points());
            return points[i];
        }

        inline const Point &point(const std::size_t i) const {
            assert(i < n_points());
            return points[i];
        }

        void clear() {
            points.clear();
            weights.clear();
        }

        Storage<Point> points;
        Storage<T> weights;
    };

    // template<typename T>
    // class Quadrature<T, 1> {
    // public:
    // 	using Point = T;

    // 	inline std::size_t n_points() const
    // 	{
    // 		return points.size();
    // 	}

    // 	inline void resize(const std::size_t &n)
    // 	{
    // 		points.resize(n);
    // 		weights.resize(n);
    // 	}

    // 	Storage<Point> points;
    // 	Storage<T>	   weights;
    // };

    template <typename T>
    class Quadrature<T, 0> final {
    public:
        inline static bool empty() { return false; }
    };

    template <typename T>
    using Quadrature0 = moonolith::Quadrature<T, 0>;

    template <typename T>
    using Quadrature1 = moonolith::Quadrature<T, 1>;

    template <typename T>
    using Quadrature2 = moonolith::Quadrature<T, 2>;

    template <typename T>
    using Quadrature3 = moonolith::Quadrature<T, 3>;

    template <typename T>
    using Quadrature4 = moonolith::Quadrature<T, 4>;

    template <typename T, int Dim>
    void map(const Quadrature3<T> &ref, const T &scale, const PMesh<T, Dim> &poly, Quadrature<T, Dim> &out);

    // template<typename T>
    // void map(
    // 	const Quadrature3<T> &ref,
    // 	const T &scale,
    // 	const Polyhedron4<T> &poly,
    // 	Quadrature4<T> &out
    // );

    template <int Dim, typename T>
    void map(const Quadrature2<T> &ref, const T &scale, const Polygon<T, Dim> &poly, Quadrature<T, Dim> &out);

    template <int Dim, typename T>
    void map(const Quadrature2<T> &ref, const T &scale, const Storage<Polygon<T, Dim>> &poly, Quadrature<T, Dim> &out);

    template <int Dim, typename T>
    void map(const Quadrature<T, 1> &ref, const T &scale, const Line<T, Dim> &poly, Quadrature<T, Dim> &out);

    template <int Dim, typename T>
    void map(const Quadrature2<T> &ref,
             const T &scale,
             const Storage<Vector<T, Dim>> &points,
             const Storage<int> &tri,
             Quadrature<T, Dim> &out);

    template <typename T, int Dim>
    inline T measure(const Quadrature<T, Dim> &q) {
        T ret = 0.0;

        for (auto w : q.weights) {
            ret += w;
        }

        return ret;
    }
}  // namespace moonolith

#endif  // MOONOLITH_MAP_QUADRATURE_HPP
