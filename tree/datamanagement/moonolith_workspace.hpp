#ifndef MOONOLITH_WORKSPACE_HPP
#define MOONOLITH_WORKSPACE_HPP

#include "moonolith_asynch_lookup_table_builder.hpp"
#include "moonolith_check_can_refine.hpp"
#include "moonolith_collector.hpp"
#include "moonolith_describe.hpp"
#include "moonolith_exchange.hpp"
#include "moonolith_load_balancer.hpp"
#include "moonolith_lookup_table_builder.hpp"
#include "moonolith_n_tree_bound_synchronize.hpp"
#include "moonolith_node_and_proc_collector.hpp"
#include "moonolith_profiler.hpp"
#include "moonolith_synch_lookup_table_builder.hpp"

#include <cmath>
#include <set>
#include <sstream>

namespace moonolith {

    template <class NTreeT>
    class Workspace : public VerboseObject {
    public:
        typedef typename NTreeT::Traits Traits;
        typedef typename Traits::Bound::StaticBound StaticBound;
        typedef typename Traits::DataType Adapter;

        typedef std::vector<std::shared_ptr<WorkBatch> > BatchArrayT;
        typedef std::tuple<Integer, Integer, Integer> IntTripleT;
        typedef std::vector<Adapter> DataContainer;
        typedef std::set<DataHandle>::const_iterator SelectionIter;

        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        std::vector<BatchArrayT> batches;
        std::map<IntTripleT, BatchArrayT> workspace;
        std::map<Integer, std::shared_ptr<StaticBound> > bounds;
        std::map<Integer, std::shared_ptr<DataContainer> > remote_data;

        //        WorkStats stats;
        /////////////////////////////////////////////////////////////////////////////////////////////////////////

        void clear() {
            workspace.clear();
            batches.clear();
            remote_data.clear();
            bounds.clear();
        }

        template <class Map, class Key, class Value>
        static Value find_and_check(const Map &map, const Key &key, const Value &default_value) {
            auto it = map.find(key);
            assert(it != map.end());
            if (it == map.end()) return default_value;

            return it->second;
        }

        void initialize(Communicator &comm, const WorkTable &worktable) {
            const auto &work = worktable.work();
            clear();

            batches.resize(comm.size());

            // reorganize work batches
            for (auto w : work) {
                if (w->worker() == comm.rank()) {
                    // Add to local workspace
                    const Integer gId = w->global_handle();
                    const Integer rank_1 = w->rank() < comm.rank() ? w->rank() : comm.rank();
                    const Integer rank_2 = w->rank() > comm.rank() ? w->rank() : comm.rank();

                    workspace[std::make_tuple(gId, rank_1, rank_2)].push_back(w);

                    assert(workspace[std::make_tuple(gId, rank_1, rank_2)].size() <= 2);

                } else {
                    assert(w->worker() < comm.size());
                    assert(w->worker() >= 0);
                    batches[w->worker()].push_back(w);
                }
            }

            if (this->verbose()) {
                root_describe("Workspace: initialization complete", comm, logger());
            }
        }

        template <class Read, class Write>
        void exchange(Communicator &comm, const NTreeT &tree, Read read, Write write) {
            assert(!comm.has_pending_requests());

            std::vector<ByteOutputBuffer> os(comm.size());
            std::vector<Integer> mapping(tree.memory().n_data());

            if (this->verbose()) {
                root_describe("Workspace: exchange begin", comm, logger());
            }

            for (Integer r = 0; r < comm.size(); ++r) {
                if (batches[r].empty()) continue;

                std::set<DataHandle> selection;

                Integer nIndices = 0;
                for (auto w : batches[r]) {
                    nIndices += w->n_elements();
                    for (auto it = w->begin(); it != w->end(); ++it) {
                        selection.insert(*it);
                    }
                }

                Integer index = 0;
                for (auto k : selection) {
                    mapping[k] = index++;
                }

                ByteOutputBuffer &out = os[r];

                auto req_space = 3 * sizeof(Integer) + nIndices * sizeof(Integer) +
                                 batches[r].size() * (sizeof(Integer) + NTreeT::Traits::Dimension * 2 * sizeof(double));

                out.request_space(static_cast<Integer>(req_space));

                ///////////////////////////////////////////////////////////////////////
                const Integer n_batches = static_cast<Integer>(batches[r].size());
                out << n_batches;
                for (auto w : batches[r]) {
                    out << w->rank();
                    out << w->global_handle();
                    out << tree.node(w->node())->bound().static_bound();

                    out << Integer(w->n_elements());
                    for (auto it = w->begin(); it != w->end(); ++it) {
                        out << mapping[*it];
                    }
                }

                std::vector<Integer> handles;
                handles.reserve(selection.size());

                for (auto it = selection.begin(); it != selection.end(); ++it) {
                    handles.push_back(tree.data(*it).handle());
                }

                write(comm.rank(), r, handles.begin(), handles.end(), tree.memory().data(), out);
                ///////////////////////////////////////////////////////////////////////
            }

            std::vector<ByteInputBuffer> is(comm.size());
            const Integer n_incoming = comm.i_send_recv_all(os, is);

            for (Integer k = 0; k < n_incoming; ++k) {
                Integer rank, index;
                while (!comm.test_recv_any(&rank, &index)) {
                }

                ByteInputBuffer &in = is[rank];

                ///////////////////////////////////////////////////////////////////////
                Integer n_batches;
                in >> n_batches;

                for (Integer b = 0; b < n_batches; ++b) {
                    StaticBound bound;
                    std::shared_ptr<WorkBatch> w = std::make_shared<WorkBatch>();

                    Integer relatedRank;
                    NodeHandle gId;

                    in >> relatedRank;
                    in >> gId;
                    in >> bound;

                    auto &boundptr = bounds[gId];
                    if (!boundptr) {
                        boundptr = std::make_shared<StaticBound>(std::move(bound));
                    }

                    w->set_rank(relatedRank);
                    w->set_owner_rank(rank);

                    Integer n_elements;
                    std::vector<Integer> elements;
                    in >> n_elements;
                    for (Integer e = 0; e < n_elements; ++e) {
                        Integer eId;
                        in >> eId;
                        w->insert(eId);
                    }

                    const Integer rank_1 = w->rank() < rank ? w->rank() : rank;
                    const Integer rank_2 = w->rank() > rank ? w->rank() : rank;

                    workspace[std::make_tuple(gId, rank_1, rank_2)].push_back(w);
                    assert(workspace[std::make_tuple(gId, rank_1, rank_2)].size() <= 2);
                }

                std::shared_ptr<DataContainer> remote_data_for_rank = std::make_shared<DataContainer>();
                read(rank, rank, false, *remote_data_for_rank, in);
                ///////////////////////////////////////////////////////////////////////

                remote_data[rank] = remote_data_for_rank;
                in.clear();
            }

            comm.wait_all();
            is.clear();
            os.clear();

            if (this->verbose()) {
                root_describe("Workspace: exchange end", comm, logger());
            }
        }

        template <class Function>
        Integer apply(Communicator &comm, NTreeT &tree, const std::shared_ptr<Predicate> &predicate, Function fun) {
            Integer n_success = 0;
            exec(comm,
                 tree,
                 [predicate, &fun, &n_success](const Integer /*rank_1*/,
                                               DataContainer &data1,
                                               const SelectionIter &begin1,
                                               const SelectionIter &end1,
                                               const Integer /*rank_2*/,
                                               DataContainer &data2,
                                               const SelectionIter &begin2,
                                               const SelectionIter &end2,
                                               const StaticBound &bound,
                                               const bool is_self_intersection) {
                     n_success += determine_and_apply_aux(
                         data1, begin1, end1, data2, begin2, end2, predicate, bound, fun, is_self_intersection);
                 });

            return n_success;
        }

        class ProcPairCandidates : public Describable {
        public:
            typedef std::vector<std::pair<Integer, Integer> > CandidateVector;

            ProcPairCandidates(const Integer rank_1, const Integer rank_2, const bool is_originated_locally = true)
                : rank_1_(rank_1), rank_2_(rank_2), is_originated_locally_(is_originated_locally) {}

            inline Integer rank_1() const { return rank_1_; }

            inline Integer rank_2() const { return rank_2_; }

            CandidateVector &candidates() { return candidates_; }

            void describe(std::ostream &os) const {
                os << "ranks: {" << rank_1() << ", " << rank_2() << "}\n";
                os << "npairs: " << candidates_.size() << "\n";
            }

            inline bool is_originated_locally() const { return is_originated_locally_; }

        private:
            Integer rank_1_, rank_2_;
            CandidateVector candidates_;
            bool is_originated_locally_;
        };

        class Candidates {
        public:
            std::map<std::pair<Integer, Integer>, std::shared_ptr<ProcPairCandidates> > rank_2_candidates;
            std::vector<std::vector<std::shared_ptr<ProcPairCandidates> > > assigned_cands;

            void finalize_on(Communicator &comm) {
                Integer local_candidates = 0;
                for (auto r_2_c : rank_2_candidates) {
                    local_candidates += static_cast<Integer>(r_2_c.second->candidates().size());
                }

                Integer cum_sum = 0;
                comm.exscan(&local_candidates, &cum_sum, 1, MPISum());
                Integer total = cum_sum + local_candidates;
                Integer realTotal = local_candidates;
                comm.all_reduce(&realTotal, 1, MPISum());
                comm.broadcast(&total, 1, comm.size() - 1);
                const Integer avg = total / comm.size();
                const Integer remainder = total % comm.size();

                // const Integer r = comm.rank();
                // const Integer begin = avg * r + std::min(r, remainder);
                // const Integer end = avg * (r + 1) + std::min(r + 1, remainder);

                Integer current = cum_sum;

                auto r_2_c_iter = rank_2_candidates.begin();
                Integer offset = 0;

                assigned_cands.clear();
                assigned_cands.resize(comm.size());

                for (Integer rr = 0; rr < comm.size() && r_2_c_iter != rank_2_candidates.end(); ++rr) {
                    const Integer remote_begin = avg * rr + std::min(rr, remainder);
                    const Integer remote_end = avg * (rr + 1) + std::min(rr + 1, remainder);
                    // const Integer range_size = remote_end - remote_begin;

                    const Integer rank_1 = r_2_c_iter->first.first;
                    const Integer rank_2 = r_2_c_iter->first.second;

                    auto cand_ptr = std::make_shared<ProcPairCandidates>(rank_1, rank_2);

                    for (; r_2_c_iter != rank_2_candidates.end();) {
                        if (current >= remote_begin && current < remote_end) {
                            auto &cand_to_move = r_2_c_iter->second->candidates();
                            auto &candidates = cand_ptr->candidates();

                            const Integer nmax = static_cast<Integer>(cand_to_move.size());
                            const Integer n = std::min(remote_end - current, nmax - offset);
                            // Move to other proc
                            candidates.insert(
                                candidates.end(), cand_to_move.begin() + offset, cand_to_move.begin() + offset + n);

                            current += n;
                            offset += n;

                            if (offset == nmax) {
                                ++r_2_c_iter;
                                offset = 0;

                                assigned_cands[rr].push_back(cand_ptr);

                                if (r_2_c_iter != rank_2_candidates.end()) {
                                    cand_ptr = std::make_shared<ProcPairCandidates>(r_2_c_iter->first.first,
                                                                                    r_2_c_iter->first.second);
                                } else {
                                    cand_ptr = nullptr;
                                }
                            }
                        } else {
                            // nothing to do here
                            break;
                        }
                    }

                    if (cand_ptr && !cand_ptr->candidates().empty()) {
                        assigned_cands[rr].push_back(cand_ptr);
                    }
                }

                rank_2_candidates.clear();
            }
        };

        template <class PairsIter>
        static void create_separate_index(const PairsIter &begin,
                                          const PairsIter &end,
                                          std::set<Integer> &firsts,
                                          std::set<Integer> &seconds) {
            for (PairsIter it = begin; it != end; ++it) {
                firsts.insert(it->first);
                seconds.insert(it->second);
            }
        }

        static void create_mapping(const std::set<Integer> &set, std::vector<Integer> &mapping) {
            if (set.empty()) return;
            mapping.resize(*set.rbegin() + 1, -1);

            Integer index = 0;
            for (auto i : set) {
                assert(std::size_t(i) < mapping.size());
                assert(i >= 0);

                mapping[i] = index++;
            }
        }

        template <class CandidateVector, class Data1, class Data2, class Function>
        Integer compute_for_candidates(CandidateVector &candidates,
                                       Data1 &data1,
                                       Data2 &data2,
                                       const std::shared_ptr<Predicate> &predicate,
                                       Function fun) {
            Integer n_success = 0;
            for (auto pair : candidates) {
                auto &a1 = data1.at(pair.first);
                auto &a2 = data2.at(pair.second);

                assert(predicate);

                if (predicate && !predicate->are_master_and_slave(a1.tag(), a2.tag())) {
                    n_success += fun(a2, a1);
                } else {
                    n_success += fun(a1, a2);
                }
            }

            return n_success;
        }

        template <class Function>
        Integer compute(Communicator &comm,
                        NTreeT &tree,
                        Candidates &candidates,
                        const std::shared_ptr<Predicate> &predicate,
                        Function fun) {
            Integer total = 0;
            Integer n_success = 0;
            for (auto cand_ptr : candidates.assigned_cands[comm.rank()]) {
                if (!cand_ptr) {
                    std::cerr << "Workspace::compute(...) should never happen" << std::endl;
                    continue;
                }

                const Integer rank_1 = cand_ptr->rank_1();
                const Integer rank_2 = cand_ptr->rank_2();

                if (cand_ptr->is_originated_locally()) {
                    if (rank_1 == comm.rank()) {
                        if (rank_2 == comm.rank()) {
                            n_success += compute_for_candidates(
                                cand_ptr->candidates(), tree.memory().data(), tree.memory().data(), predicate, fun);
                        } else {
                            auto data_2_ptr = remote_data[rank_2];
                            n_success += compute_for_candidates(
                                cand_ptr->candidates(), tree.memory().data(), *data_2_ptr, predicate, fun);
                        }
                    } else if (rank_2 == comm.rank()) {
                        auto data_1_ptr = remote_data[rank_1];
                        n_success += compute_for_candidates(
                            cand_ptr->candidates(), *data_1_ptr, tree.memory().data(), predicate, fun);

                    } else {
                        auto data_1_ptr = remote_data[rank_1];
                        auto data_2_ptr = remote_data[rank_2];

                        assert(bool(data_1_ptr));
                        assert(bool(data_2_ptr));

                        n_success +=
                            compute_for_candidates(cand_ptr->candidates(), *data_1_ptr, *data_2_ptr, predicate, fun);
                    }
                } else {
                    auto data_1_ptr = remote_data[rank_1];
                    auto data_2_ptr = remote_data[rank_2];

                    assert(bool(data_1_ptr));
                    assert(bool(data_2_ptr));

                    n_success +=
                        compute_for_candidates(cand_ptr->candidates(), *data_1_ptr, *data_2_ptr, predicate, fun);
                }

                total += static_cast<Integer>(cand_ptr->candidates().size());
            }

            return n_success;
        }

        template <class MigrateRead, class MigrateWrite>
        void migrate(Communicator &comm,
                     const NTreeT &tree,
                     Candidates &candidates,
                     MigrateRead migrate_read,
                     MigrateWrite migrate_write) {
            std::vector<ByteOutputBuffer> outputs(comm.size());
            std::vector<std::set<Integer> > buffer(comm.size());
            std::vector<std::vector<Integer> > mappings(comm.size());

            for (Integer r = 0; r < comm.size(); ++r) {
                if (r == comm.rank()) continue;

                auto cand_vector = candidates.assigned_cands[r];

                if (cand_vector.empty()) continue;

                std::set<Integer> touched;
                Integer n_batches = 0;
                for (auto cand_ptr : cand_vector) {
                    if (cand_ptr->candidates().empty()) continue;

                    create_separate_index(cand_ptr->candidates().begin(),
                                          cand_ptr->candidates().end(),
                                          buffer[cand_ptr->rank_1()],
                                          buffer[cand_ptr->rank_2()]);

                    touched.insert(cand_ptr->rank_1());
                    touched.insert(cand_ptr->rank_2());
                    ++n_batches;
                }

                if (touched.empty()) continue;

                for (auto t : touched) {
                    create_mapping(buffer[t], mappings[t]);
                }

                // Start writing
                auto &os = outputs[r];

                const Integer nMeshes = static_cast<Integer>(touched.size());
                os << nMeshes;  // WRITE 1

                for (auto t : touched) {
                    os << t;  // WRITE 2

                    if (t == comm.rank()) {
                        assert(!buffer[t].empty());

                        std::vector<Integer> handles;
                        handles.reserve(buffer[t].size());

                        for (auto it = buffer[t].begin(); it != buffer[t].end(); ++it) {
                            handles.push_back(tree.data(*it).handle());
                            assert(handles.back() >= 0);
                        }
                        migrate_write(t, r, handles.begin(), handles.end(), tree.memory().data(), os);  // WRITE 3
                    } else {
                        assert(remote_data[t]);
                        assert(!buffer[t].empty());

                        std::vector<Integer> handles(buffer[t].begin(), buffer[t].end());
                        migrate_write(t, r, handles.begin(), handles.end(), *remote_data[t], os);  // WRITE 3
                    }
                }

                Integer count = 0;

                os << n_batches;  // WRITE 4
                for (auto cand_ptr : cand_vector) {
                    if (cand_ptr->candidates().empty()) continue;

                    const auto rank_1 = cand_ptr->rank_1();
                    const auto rank_2 = cand_ptr->rank_2();

                    auto &map_first = mappings[rank_1];
                    auto &map_second = mappings[rank_2];

                    const Integer nCandidates = static_cast<Integer>(cand_ptr->candidates().size());
                    count += nCandidates;

                    assert(nCandidates > 0);

                    os << rank_1 << rank_2 << nCandidates;  // WRITE 5 6 7
                    for (auto canditer : cand_ptr->candidates()) {
                        assert(canditer.first >= 0);
                        assert(std::size_t(canditer.first) < map_first.size());

                        assert(canditer.second >= 0);
                        assert(std::size_t(canditer.second) < map_second.size());

                        const Integer first = map_first[canditer.first];
                        const Integer second = map_second[canditer.second];
                        os << first;   // WRITE 8
                        os << second;  // WRITE 9
                    }
                }

                // clean after ourselves
                for (auto t : touched) {
                    buffer[t].clear();
                    mappings[t].clear();
                }
            }

            std::vector<ByteInputBuffer> inputs(comm.size());
            const Integer n_incoming = comm.i_send_recv_all(outputs, inputs);

            std::vector<Integer> offsets(comm.size(), 0);

            for (Integer k = 0; k < n_incoming; ++k) {
                Integer rank, index;
                while (!comm.test_recv_any(&rank, &index)) {
                }
                auto &is = inputs[rank];

                assert(rank != comm.rank());

                Integer nMeshes;

                is >> nMeshes;  // READ 1

                for (Integer i = 0; i < nMeshes; ++i) {
                    Integer r;
                    is >> r;  // READ 2

                    auto &data = remote_data[r];
                    if (!data) {
                        data = std::make_shared<DataContainer>();
                    }

                    const Integer offset = static_cast<Integer>(data->size());

                    assert(is.good());

                    migrate_read(r, rank, true, *data, is);  // READ 3
                    offsets[r] = offset;
                }

                Integer n_batches;
                is >> n_batches;  // READ 4

                for (Integer i = 0; i < n_batches; ++i) {
                    Integer rank_1, rank_2, nCandidates;
                    is >> rank_1;       // READ 5
                    is >> rank_2;       // READ 6
                    is >> nCandidates;  // READ 7
                    const Integer offset1 = offsets[rank_1];
                    const Integer offset2 = offsets[rank_2];

                    auto cand_ptr = std::make_shared<ProcPairCandidates>(rank_1, rank_2, false);
                    cand_ptr->candidates().reserve(nCandidates);

                    for (Integer j = 0; j < nCandidates; ++j) {
                        Integer index1, index2;
                        is >> index1;  // READ 8
                        is >> index2;  // READ 9
                        cand_ptr->candidates().push_back(std::make_pair(index1 + offset1, index2 + offset2));
                    }

                    candidates.assigned_cands[comm.rank()].push_back(cand_ptr);
                }
            }

            comm.wait_all();
            comm.clear();
        }

        void determine_and_collect(Communicator &comm,
                                   NTreeT &tree,
                                   const std::shared_ptr<Predicate> &predicate,
                                   Candidates &candidates) {
            exec(comm,
                 tree,
                 [predicate, &candidates](const Integer rank_1,
                                          DataContainer &data1,
                                          const SelectionIter &begin1,
                                          const SelectionIter &end1,
                                          const Integer rank_2,
                                          DataContainer &data2,
                                          const SelectionIter &begin2,
                                          const SelectionIter &end2,
                                          const StaticBound &bound,
                                          const bool is_self_intersection) {
                     using std::make_pair;
                     using std::make_shared;

                     auto ret = candidates.rank_2_candidates.insert(
                         make_pair(make_pair(rank_1, rank_2), make_shared<ProcPairCandidates>(rank_1, rank_2)));

                     auto it = ret.first;
                     determine_and_collect_aux(data1,
                                               begin1,
                                               end1,
                                               data2,
                                               begin2,
                                               end2,
                                               predicate,
                                               bound,
                                               it->second->candidates(),
                                               is_self_intersection);
                 });

            candidates.finalize_on(comm);
        }

        template <class DataT>
        static bool must_discard_pair(DataT &a1,
                                      DataT &a2,
                                      const std::shared_ptr<Predicate> &predicate,
                                      const StaticBound &bound) {
            const auto &bound_1 = a1.bound().static_bound();
            const auto &bound_2 = a2.bound().static_bound();

            // skip duplicate intersections (pair will be computed in adjacent node)
            for (Integer d = 0; d < bound_1.n_dims(); ++d) {
                const auto m1 = bound_1.min(d);
                const auto m2 = bound_2.min(d);

                if (m1 <= bound.min(d) && m2 <= bound.min(d)) {
                    return true;
                }
            }

            if (!a1.bound().intersects(a2.bound())) {
                return true;
            }

            if (predicate && !predicate->tags_are_related(a1.tag(), a2.tag())) {
                return true;
            }

            return false;
        }

        static void determine_and_collect_aux(DataContainer &data1,
                                              const SelectionIter &begin1,
                                              const SelectionIter &end1,
                                              DataContainer &data2,
                                              const SelectionIter &begin2,
                                              const SelectionIter &end2,
                                              const std::shared_ptr<Predicate> &predicate,
                                              const StaticBound &bound,
                                              std::vector<std::pair<Integer, Integer> > &candidates,
                                              const bool is_self_intersection) {
            if (is_self_intersection) {
                assert(begin1 == begin2);
                assert(end1 == end2);

                for (SelectionIter it1 = begin1; it1 != end1; ++it1) {
                    auto &a1 = data1[*it1];

                    SelectionIter it2 = it1;
                    for (++it2; it2 != end2; ++it2) {
                        auto &a2 = data2[*it2];

                        if (must_discard_pair(a1, a2, predicate, bound)) {
                            continue;
                        }

                        candidates.push_back(std::pair<Integer, Integer>(*it1, *it2));
                    }
                }

            } else {
                for (SelectionIter it1 = begin1; it1 != end1; ++it1) {
                    auto &a1 = data1[*it1];

                    for (SelectionIter it2 = begin2; it2 != end2; ++it2) {
                        auto &a2 = data2[*it2];

                        if (must_discard_pair(a1, a2, predicate, bound)) {
                            continue;
                        }

                        candidates.push_back(std::pair<Integer, Integer>(*it1, *it2));
                    }
                }
            }
        }

        template <class Function>
        static Integer determine_and_apply_aux(DataContainer &data1,
                                               const SelectionIter &begin1,
                                               const SelectionIter &end1,
                                               DataContainer &data2,
                                               const SelectionIter &begin2,
                                               const SelectionIter &end2,
                                               const std::shared_ptr<Predicate> &predicate,
                                               const StaticBound &bound,
                                               Function fun,
                                               const bool is_self_intersection) {
            Integer n_success = 0;
            if (is_self_intersection) {
                assert(begin1 == begin2);
                assert(end1 == end2);

                for (SelectionIter it1 = begin1; it1 != end1; ++it1) {
                    auto &a1 = data1[*it1];

                    SelectionIter it2 = it1;
                    for (++it2; it2 != end2; ++it2) {
                        auto &a2 = data2[*it2];

                        if (must_discard_pair(a1, a2, predicate, bound)) {
                            continue;
                        }

                        assert(predicate);

                        if (predicate && !predicate->are_master_and_slave(a1.tag(), a2.tag())) {
                            n_success += fun(a2, a1);
                        } else {
                            n_success += fun(a1, a2);
                        }
                    }
                }

            } else {
                for (SelectionIter it1 = begin1; it1 != end1; ++it1) {
                    auto &a1 = data1[*it1];

                    for (SelectionIter it2 = begin2; it2 != end2; ++it2) {
                        auto &a2 = data2[*it2];

                        if (must_discard_pair(a1, a2, predicate, bound)) {
                            continue;
                        }

                        assert(predicate);

                        if (predicate && !predicate->are_master_and_slave(a1.tag(), a2.tag())) {
                            n_success += fun(a2, a1);

                        } else {
                            n_success += fun(a1, a2);
                        }
                    }
                }
            }

            return n_success;
        }

        template <typename Function>
        void exec(Communicator &comm, NTreeT &tree, Function fun) {
            for (auto &worktuple : workspace) {
                std::shared_ptr<WorkBatch> w1;
                std::shared_ptr<WorkBatch> w2;
                bool is_self_intersection = false;

                assert(worktuple.second.size() == 2 || worktuple.second.size() == 1);
                if (worktuple.second.size() == 2) {
                    w1 = worktuple.second[0];
                    w2 = worktuple.second[1];
                } else if (worktuple.second.size() == 1) {
                    w1 = worktuple.second[0];
                    w2 = worktuple.second[0];
                    is_self_intersection = true;
                } else {
                    std::cerr << comm << "[Error] Workspace<Tree>, Wrong number of batches: " << worktuple.second.size()
                              << std::endl;
                    assert(false);
                    return;
                }

                const Integer nodeId = std::get<0>(worktuple.first);
                const Integer rank_1 = w1->owner_rank();
                const Integer rank_2 = w2->owner_rank();

                auto bIt = bounds.find(nodeId);
                assert(bIt != bounds.end() || (rank_1 == comm.rank() && rank_2 == comm.rank()));

                const bool isLocal = bIt == bounds.end();

                const StaticBound &bound = isLocal ? tree.node(w1->node())->bound().static_bound() : (*(bIt->second));

                DataContainer *data1 = NULL;
                DataContainer *data2 = NULL;

                if (rank_1 == comm.rank()) {
                    data1 = &tree.memory().data();
                } else {
                    assert(remote_data[rank_1]);
                    data1 = &(*remote_data[rank_1]);
                }

                if (rank_2 == comm.rank()) {
                    data2 = &tree.memory().data();
                } else {
                    assert(remote_data[rank_2]);
                    data2 = &(*remote_data[rank_2]);
                }

                fun(rank_1,
                    *data1,
                    w1->begin(),
                    w1->end(),
                    rank_2,
                    *data2,
                    w2->begin(),
                    w2->end(),
                    bound,
                    is_self_intersection);
            }
        }
    };
}  // namespace moonolith

#endif  // MOONOLITH_WORKSPACE_HPP
