#ifndef MOONOLITH_ELEM_HEX_HPP
#define MOONOLITH_ELEM_HEX_HPP

#include "moonolith_elem.hpp"
#include "moonolith_polyhedral_element.hpp"
#include "moonolith_gauss_quadrature_rule.hpp"

namespace moonolith {
    
    template<typename T, int Order_, int PhysicalDim_ = 3>
    class Hex : public PolyhedralElement<T, PhysicalDim_> {
    public:
        virtual ~Hex() {}
    };

    template<typename T, int Dim, std::size_t Size>
    inline T approx_hexahedron_volume(const std::array<Vector<T, Dim>, Size> &points)
    {
        static_assert(Size >= 8, "must be a hexahedron");
        Vector<T, Dim> u = points[1] - points[0];
        T l1 = length(u);

        u = points[3] - points[0];
        T l2 = length(u);

        u = points[4] - points[0];
        T l3 = length(u);
        return l1*l2*l3;
    }


    template<typename T, int Order, int PhysicalDim>
    class GaussQRule< Hex<T, Order, PhysicalDim> > {
    public:
        static bool get(const Integer order, Quadrature3<T> &q) {
            return Gauss::Hex::get(order, q);
        }
    };

}

#endif //MOONOLITH_ELEM_HEX_HPP
