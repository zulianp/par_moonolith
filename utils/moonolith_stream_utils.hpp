#ifndef MOONOLITH_STREAM_UTISL_HPP
#define MOONOLITH_STREAM_UTISL_HPP

#include "moonolith_input_stream.hpp"
#include "moonolith_output_stream.hpp"
#include <fstream>

namespace moonolith {
	class Logger {
	public:

		inline static Logger &instance()
		{
			static Logger instance_;
			return instance_;
		}

		std::ostream &stream();

		inline void set_use_file_stream(const std::string &file_name)
		{
			use_file_stream_ = true;
			file_name_ = file_name;
		}

		Logger()
		: use_file_stream_(false), stream_open_(false)
		{}

		~Logger() 
		{
			stream() << std::flush;

			if(use_file_stream_) {
				file_stream_.close();
			}
		}

	private:
		bool use_file_stream_;
		bool stream_open_;
		std::string file_name_;
		std::ofstream file_stream_;
	};


	inline std::ostream &logger()
	{
		return Logger::instance().stream();
	}

	inline std::ostream &error_logger()
	{
		return std::cerr;
	}

	template<class MapT>
	void read_map(MapT &map, InputStream &is)
	{
		Integer n;
		is >> n;
		if(!n) return;

		for(Integer i = 0; i < n; ++i) {
			typename MapT::key_type key;
			is >> key;
			is >> map[key];
		}
	}

	template<class MapT>
	void write_map(const MapT &map, OutputStream &os)
	{
		os << Integer(map.size());
		os.write(map.begin(), map.end());
	}


	template<class SetT>
	void read_set(SetT &set, InputStream &is)
	{
		Integer n;
		is >> n;
		if(!n) return;

		for(Integer i = 0; i < n; ++i) {
			typename SetT::value_type value;
			is >> value;
			set.insert(value);
		}
	}

	template<class SetT>
	void write_set(const SetT &set, OutputStream &os)
	{	
		os << Integer(set.size());
		os.write(set.begin(), set.end());

	}

	template<class Vector>
	void write_vector(const Vector &vec, OutputStream &os)
	{
		os << Integer(vec.size());
		if(vec.empty()) return;

		os.write(&vec[0], vec.size());
	}

	template<class Vector>
	void read_vector(Vector &vec, InputStream &is)
	{
		Integer n;
		is >> n;
		if(n == 0) return;
		vec.resize(n);
		is.read(&vec[0], vec.size());
	}
}

#endif //MOONOLITH_STREAM_UTISL_HPP
