
#include "moonolith_elem_node1.hpp"
#include "moonolith_interpolator_impl.hpp"
#include "moonolith_stream_utils.hpp"

#include "gtest/gtest.h"

using namespace moonolith;

TEST(TestInterpolator, reference_interpolate) {
    using MasterElem = moonolith::Quad4<Real>;
    using SlaveElem1 = moonolith::Node1<Real, 2>;
    using SlaveElem2 = moonolith::Tri3<Real>;

    MasterElem master;
    SlaveElem1 slave1;
    SlaveElem2 slave2;

    master.make_reference();
    slave1.make_reference();
    slave2.make_reference();

    Interpolator<MasterElem, SlaveElem1> interp1;
    ASSERT_TRUE(interp1.assemble(master, slave1));

    // interp1.coupling_matrix().describe(logger());

    Interpolator<MasterElem, SlaveElem2> interp2;
    ASSERT_TRUE(interp2.assemble(master, slave2));

    // interp2.coupling_matrix().describe(logger());
}
