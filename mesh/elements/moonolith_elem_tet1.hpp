#ifndef MOONOLITH_ELEM_TET1_HPP
#define MOONOLITH_ELEM_TET1_HPP

#include "moonolith_elem_tet.hpp"
#include "moonolith_elem_tet4.hpp"

namespace moonolith {

    template <typename T_, int PhysicalDim_ = 3>
    class Tet1 final : public Tet<T_, 0, PhysicalDim_> {
    public:
        static const int Order = 0;
        static const int Dim = 3;
        static const int PhysicalDim = PhysicalDim_;
        static const int NNodes = 1;

        using T = T_;

        using Vector = moonolith::Vector<T, Dim>;
        using CoVector = moonolith::Vector<T, PhysicalDim>;

        using Point = moonolith::Vector<T, Dim>;
        using CoPoint = moonolith::Vector<T, PhysicalDim>;

        using Super = moonolith::Tet<T_, 0, PhysicalDim_>;
        using Super::make;

    private:
        class Grad final {
        public:
            Grad() {
                f[0] = [](const Point &, Vector &g) {
                    g.x = 0.0;
                    g.y = 0.0;
                    g.z = 0.0;
                };
            }

            std::array<std::function<void(const Point &, Vector &)>, NNodes> f;
        };

        class Fun final {
        public:
            Fun() {
                f[0] = [](const Point &) -> T { return 1.0; };
            }

            std::array<std::function<T(const Point &)>, NNodes> f;
        };

    public:
        T fun(const Integer i, const Point &p) const override { return fun_.f[i](p); }

        void grad(const Integer i, const Point &p, Vector &g) const override { grad_.f[i](p, g); }

        void hessian(const Integer, const Point &, std::array<T, Dim * Dim> &H) {
            std::fill(std::begin(H), std::end(H), 0.0);
        }

        void jacobian(const Point &, std::array<T, PhysicalDim * Dim> &J) const override { affine_approx_jacobian(J); }

        void affine_approx_jacobian(std::array<T, PhysicalDim * Dim> &J) const override {
            Affine<Tet1>::jacobian(points_, J);
        }

        CoPoint &node(const Integer i) override { return nodes_[i]; }

        const CoPoint &node(const Integer i) const override { return nodes_[i]; }

        void point(const Point &p, CoPoint &q) const override { Affine<Tet1>::point(points_, p, q); }

        inline CoPoint &point(const Integer idx) { return points_[idx]; }

        inline const CoPoint &point(const Integer idx) const { return points_[idx]; }

        Tet1() {}

        bool is_affine() const override { return true; }
        inline bool is_simplex() const override { return true; }

        inline int n_nodes() const override { return NNodes; }
        inline int order() const override { return Order; }

        const std::array<CoPoint, NNodes> &nodes() const { return nodes_; }
        const std::array<CoPoint, 4> &points() const { return points_; }

        inline void make_reference() {
            Reference<Tet4<T, PhysicalDim>>::points(points_);
            Reference<Tet1>::points(nodes_);
        }

        inline ElemType type() const override { return TET1; }

        inline T approx_measure() const override {
            return tetrahedron_volume(points_[0], points_[1], points_[2], points_[3]);
        }

        inline T measure() const override { return tetrahedron_volume(points_[0], points_[1], points_[2], points_[3]); }

        inline T reference_measure() const override { return 1.0 / 6.0; }

        inline void make(Polyhedron<T> &poly) const override {
            ConvexDecomposition<T, PhysicalDim>::make_tet(points(), 0, 1, 2, 3, poly);
        }

    private:
        std::array<CoPoint, NNodes> nodes_;
        std::array<CoPoint, 4> points_;
        const Fun fun_;
        const Grad grad_;
    };

    template <typename T, int PhysicalDim>
    class Reference<Tet1<T, PhysicalDim>> {
    public:
        using Point = typename Tet1<T, PhysicalDim>::Point;
        static const int NNodes = 1;

        template <std::size_t Size>
        static void points(std::array<Point, Size> &nodes) {
            static_assert(Size >= NNodes, "size must be at least 4");
            // p0 (TODO check if it is correct)
            auto x = std::sqrt(3) / 4.0;
            nodes[0].x = x;
            nodes[0].y = x;
            nodes[0].z = x;
        }

        static T measure() { return 1. / 6.; }
    };

    template <typename T, int PhysicalDim>
    class GaussQRule<Tet1<T, PhysicalDim>> {
    public:
        static bool get(const Integer order, Quadrature3<T> &q) { return Gauss::get(order, q); }
    };

}  // namespace moonolith

#endif  // MOONOLITH_ELEM_TET1_HPP
