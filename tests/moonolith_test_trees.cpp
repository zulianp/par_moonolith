#include "moonolith_test_trees.hpp"
#include "moonolith_asynch_lookup_table_builder.hpp"
#include "moonolith_synch_lookup_table_builder.hpp"
#include "par_moonolith.hpp"

#include "gtest/gtest.h"

using namespace moonolith;

template <class TreeT, int DIM>
bool test_local_tree(const bool verbose = false) {
    typedef typename TreeT::DataType DataT;
    std::shared_ptr<TreeT> tree = build_test_tree<TreeT, DIM>(10);

    tree->refine();

    MakeImmutable<TreeT> mImmutable;
    tree->walk(mImmutable);

    if (verbose) {
        DescribeWithGeom<TreeT> desc(logger());
        tree->walk(desc);
    }

    std::shared_ptr<CheckCanRefine<TreeT> > check = std::make_shared<CheckCanRefine<TreeT> >();
    tree->apply(check);

    assert(!check->success());

    DataT &d1 = tree->data(0);
    auto match = std::static_pointer_cast<Match<TreeT> >(tree->mutator_factory()->new_match(d1));

    tree->apply(match);
    auto ret = match->result();
    ret->reset();
    bool ok = match->success();

    assert(ok);
    return ok;
}

template <class TreeT, int DIM>
void test_lookup_table_builder(const bool verbose = false) {
    // logger() << "test_lookup_table_builder" << std::endl;

    Communicator world;
    world.barrier();

    std::shared_ptr<TreeT> tree = build_test_tree<TreeT, DIM>(10);

    AsynchLookUpTableBuilder<TreeT> builder;
    // SynchLookUpTableBuilder<TreeT> builder;
    builder.set_verbose(verbose);

    std::shared_ptr<LookUpTable> table = std::make_shared<LookUpTable>();
    bool ok = builder.build_for(world, *tree, table);

    assert(table->check_consistency(world));

    if (!ok) {
        if (world.is_alone()) {
            // std::cerr << "[Warning] Test needs more than one process" << std::endl;
            return;
        }
        // std::cerr << "[Error] Something wrong happened" << std::endl;
        return;
    }

    if (table->empty()) {
        // logger() << "[Status] No intersecting subregions in other procs" << std::endl;
    }

    world.barrier();

    if (world.is_root() && verbose) {
        // logger() << "LookUpTable : NodeToProc" << std::endl;
    }

    if (verbose) {
        world.barrier();

        for (int i = 0; i < world.size(); ++i) {
            if (i == world.rank()) {
                logger() << "--------------------------------------------------------------------------------"
                         << std::endl;
                tree->memory().compute_statistics();
                tree->memory().describe(logger());
                table->describe(logger());
                logger() << std::flush;
                logger() << "--------------------------------------------------------------------------------"
                         << std::endl;
            }

            world.barrier();
        }

        world.barrier();
    }
}

template <class TreeT, int DIM>
bool test_exchange() {
    typedef typename TreeT::DataType DataT;

    // logger() << "test_exchange" << std::endl;

    bool print_all_info = false;

    Communicator world;
    world.barrier();

    std::shared_ptr<TreeT> tree = build_test_tree<TreeT, DIM>(50);

    AsynchLookUpTableBuilder<TreeT> builder;
    std::shared_ptr<LookUpTable> table = std::make_shared<LookUpTable>();
    bool ok = builder.build_for(world, *tree, table);

    assert(ok);
    assert(table->check_consistency(world));
    table->compute_statistics(world);

    if (print_all_info) {
        table->describe_stats(logger());
    }

    Exchange<TreeT> ex(table, world);
    std::vector<std::vector<DataT> > resources;
    ex.apply(*tree, resources);

    if (print_all_info) {
        world.barrier();

        for (int i = 0; i < world.size(); ++i) {
            if (i == world.rank()) {
                logger() << "--------------------------------------------------------------------------------"
                         << std::endl;
                for (auto &r : resources) {
                    for (auto r1 : r) {
                        logger() << r1 << " ";
                    }
                    logger() << std::endl;
                }

                logger() << std::flush;
                logger() << "--------------------------------------------------------------------------------"
                         << std::endl;
            }

            world.barrier();
        }

        world.barrier();
    }

    return ok;
}

template <class TreeT, int DIM>
void test_search_and_compute() {
    typedef typename TreeT::DataType DataT;
    typedef typename TreeT::DataContainer DataContainer;

    Communicator world;
    world.barrier();

    std::shared_ptr<TreeT> tree = build_test_tree<TreeT, DIM>(50);
    std::shared_ptr<Predicate> predicate = std::make_shared<Predicate>();

    SearchSettings ss;
    ss.verbosity_level = 0;

    Integer n_isect = 0;
    Integer n_false_positives = 0;
    search_and_compute(
        world,
        tree,
        predicate,
        [](const Integer owner_rank,
           const Integer sender_rank,
           const bool is_forwarding,
           DataContainer &remote_data_for_rank,
           InputStream &is) {
            (void)owner_rank;
            (void)sender_rank;
            (void)is_forwarding;

            Integer n;
            is >> n;

            const Integer offset = remote_data_for_rank.size();
            remote_data_for_rank.resize(offset + n);

            for (Integer i = 0; i < n; ++i) {
                auto &d = remote_data_for_rank[offset + i];
                is >> d;

                // for simplifying the writing i change the handle
                // by adding the offset such that the handle is unique
                // and aligned with the data position in the container
                d.set_handle(offset + i);
            }
        },
        [](const Integer owner_rank,
           const Integer recv_rank,
           const std::vector<Integer>::const_iterator &begin,
           const std::vector<Integer>::const_iterator &end,
           const DataContainer &data_container,
           OutputStream &os) {
            (void)owner_rank;
            (void)recv_rank;

            const Integer n = std::distance(begin, end);
            os << n;
            for (auto it = begin; it != end; ++it) {
                os << data_container[*it];
            }
        },
        [&n_isect, &n_false_positives](const DataT &left, const DataT &right) {
            if (left.bound().intersects(right.bound())) {
                ++n_isect;
                return true;
            } else {
                ++n_false_positives;
                return false;
            }
        },
        ss);
}

TEST(TreeTest, local_tree_1) {
    static const int DIM = 1;
    typedef TestTree<DIM> TreeT;
    test_local_tree<TreeT, DIM>();
}

TEST(TreeTest, local_tree_2) {
    static const int DIM = 2;
    typedef TestTree<DIM> TreeT;
    test_local_tree<TreeT, DIM>();
}

TEST(TreeTest, local_tree_3) {
    static const int DIM = 3;
    typedef TestTree<DIM> TreeT;
    test_local_tree<TreeT, DIM>();
}

TEST(TreeTest, span_lookup_table_1) {
    static const int DIM = 1;
    typedef TestTreeWithSpan<DIM> TreeT;
    test_lookup_table_builder<TreeT, DIM>();
}

TEST(TreeTest, span_lookup_table_2) {
    static const int DIM = 2;
    typedef TestTreeWithSpan<DIM> TreeT;
    test_lookup_table_builder<TreeT, DIM>();
}

TEST(TreeTest, span_lookup_table_3) {
    static const int DIM = 3;
    typedef TestTreeWithSpan<DIM> TreeT;
    test_lookup_table_builder<TreeT, DIM>();
}

TEST(TreeTest, span_intersection_exchange_1) {
    static const int DIM = 1;
    typedef TestTreeWithSpan<DIM> TreeT;
    test_exchange<TreeT, DIM>();
}

TEST(TreeTest, span_intersection_exchange_2) {
    static const int DIM = 2;
    typedef TestTreeWithSpan<DIM> TreeT;
    test_exchange<TreeT, DIM>();
}

TEST(TreeTest, span_intersection_exchange_3) {
    static const int DIM = 3;
    typedef TestTreeWithSpan<DIM> TreeT;
    test_exchange<TreeT, DIM>();
}

TEST(TreeTest, span_intersection_exchange_4) {
    static const int DIM = 4;
    typedef TestTreeWithSpan<DIM> TreeT;
    test_exchange<TreeT, DIM>();
}

TEST(TreeTest, span_search_and_compute_2) {
    static const int DIM = 2;
    typedef TestTreeWithSpan<DIM> TreeT;
    test_search_and_compute<TreeT, DIM>();
}
