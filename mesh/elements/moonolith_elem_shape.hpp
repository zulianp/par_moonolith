#ifndef MOONOLITH_ELEM_SHAPE_HPP
#define MOONOLITH_ELEM_SHAPE_HPP

#include "moonolith_elem.hpp"
#include "moonolith_elem_quad.hpp"
#include "moonolith_elem_segment.hpp"
#include "moonolith_elem_triangle.hpp"
#include "moonolith_iso_parametric_transform_for_surface.hpp"
#include "moonolith_make_unique.hpp"
#include "moonolith_shape.hpp"

#include "moonolith_elem_edge.hpp"
#include "moonolith_elem_edge1.hpp"
#include "moonolith_elem_edge2.hpp"
#include "moonolith_elem_edge3.hpp"

#include "moonolith_elem_tri.hpp"
#include "moonolith_elem_tri1.hpp"
#include "moonolith_elem_tri3.hpp"
#include "moonolith_elem_tri6.hpp"

#include "moonolith_elem_quad.hpp"
#include "moonolith_elem_quad1.hpp"
#include "moonolith_elem_quad4.hpp"
#include "moonolith_elem_quad8.hpp"
#include "moonolith_elem_quad9.hpp"

#include <iostream>

namespace moonolith {

    template <class Elem>
    class ElemShape final : public Shape<typename Elem::T, Elem::Dim, Elem::PhysicalDim> {
    public:
        static const int Dim = Elem::Dim;
        static const int PhysicalDim = Elem::PhysicalDim;
        using T = typename Elem::T;

        using super = Shape<T, Dim, PhysicalDim>;

        using Point = typename super::Point;
        using CoPoint = typename super::CoPoint;

        using Vector = typename super::Vector;
        using CoVector = typename super::CoVector;

        using Jacobian = std::array<T, PhysicalDim * Dim>;
        using SolutionVector = moonolith::Vector<T, PhysicalDim>;
        using SolutionMatrix = std::array<T, PhysicalDim * PhysicalDim>;

        ~ElemShape() {}

        ElemShape(Elem &elem,
                  const T tol = 1e-10,
                  const Integer max_it = 10000,
                  const T accept_tol = 1e-6,
                  const bool verbose = false)
            : elem_(&elem), tol_(tol), max_it_(max_it), accept_tol_(accept_tol), verbose_(verbose) {}

        void init(Elem &elem) { elem_ = &elem; }

        bool intersect(const Ray<T, PhysicalDim> &ray, T &t) override {
            Point x_param;
            return intersect(ray, t, x_param);
        }

        bool intersect(const Ray<T, PhysicalDim> &ray, T &t, Point &x_param) override {
            return intersect_gradient_descent(ray, t, x_param);
        }

    private:
        Elem *elem_;
        std::unique_ptr<IsoParametricTransformForSurface<Elem>> transform_;
        T tol_;
        Integer max_it_;
        T accept_tol_;
        bool verbose_;

        // buffers
        CoPoint isect, p, g;
        Jacobian J;
        std::array<T, Elem::NNodes> c;
        // Hessian of ref-element, Jacobian^T * Jacobian
        std::array<T, Dim * Dim> H, JtJ;

        // Jacobian^T * g, Jacobian^T * -ray.dir
        Vector Jtg, Jtndir;

        // Gradient and solution of opt-problem
        SolutionVector b, x;

        // Hessian of opt-problem
        SolutionMatrix A, A_inv;

        // g is the difference between the intersection and the point on the surface
        void assemble_gradient(const Ray<T, PhysicalDim> &ray,
                               const CoVector &g,
                               const Jacobian &J,
                               SolutionVector &b) {
            mat_T_vec_mul<T, PhysicalDim, Dim>(J, g, Jtg);

            for (int i = 0; i < Dim; ++i) {
                b[i] = Jtg[i];
            }

            b[Dim] = -dot(ray.dir, g);
        }

        void assemble_hessian(const Ray<T, PhysicalDim> &ray,
                              const Jacobian &J,
                              const CoVector &g,
                              const T nTn,
                              const Point &x_param,
                              SolutionMatrix &A) {
            A[Dim * PhysicalDim + Dim] = nTn;

            mat_T_mat_mul<T, Dim, PhysicalDim>(J, JtJ);
            mat_T_vec_mul(J, ray.dir, Jtndir);

            Jtndir *= -1.0;
            for (int i = 0; i < Dim; ++i) {
                A[i * PhysicalDim + Dim] = Jtndir[i];
                A[Dim * PhysicalDim + i] = Jtndir[i];
            }

            for (int i = 0; i < Elem::NNodes; ++i) {
                c[i] = dot(g, elem_->node(i));
            }

            IsoParametric<Elem>::directional_second_derivative(*elem_, x_param, c, H);

            for (int i = 0; i < Dim; ++i) {
                const auto offset_i_dim = i * Dim;
                const auto offset_i_codim = i * PhysicalDim;

                for (int j = 0; j < Dim; ++j) {
                    const auto offset_ij = offset_i_dim + j;
                    A[offset_i_codim + j] = JtJ[offset_ij] + H[offset_ij];
                }
            }
        }

        bool intersect_gradient_descent(const Ray<T, PhysicalDim> &ray, T &t, Point &x_param) {
            set_initial_guess(ray, t, x_param);

            g = isect - p;
            auto len_g = length(g);

            if (len_g < tol_) {
                return true;
            }

            for (Integer i = 0; i < max_it_; ++i) {
                elem_->jacobian(x_param, J);
                assemble_gradient(ray, g, J, b);

                for (int d = 0; d < Dim; ++d) {
                    x_param[d] += b[d];
                }

                t += b[Dim];

                elem_->point(x_param, p);
                ray.make(t, isect);
                g = isect - p;

                auto len_g = length(g);

                if (verbose_) {
                    std::cout << "-----------------------------\n";
                    std::cout << i << ") " << len_g << std::endl;
                    print(x_param);
                    print(p);
                    print(isect);
                    std::cout << "-----------------------------\n";
                }

                if (len_g < tol_) {
                    return true;
                }
            }

            return len_g < accept_tol_;
        }

        void set_initial_guess(const Ray<T, PhysicalDim> &ray, T &t, Point &x_param) {
            ray.make(t, isect);

            if (!transform_) {
                transform_ = moonolith::make_unique<IsoParametricTransformForSurface<Elem>>(
                    *elem_, ray.dir, tol_, max_it_, accept_tol_, verbose_);
            } else {
                transform_->init(*elem_, ray.dir);
            }

            // projection on surface
            fill(x_param, static_cast<T>(0.0));
            if (!transform_->apply_inverse(isect, x_param)) {
                fill(x_param, static_cast<T>(0.0));
            }

            transform_->apply(x_param, p);
            t = dot(p - ray.o, ray.dir);
            ray.make(t, isect);
        }
    };

    template <class Elem>
    class MakeShape {
    public:
        inline static std::unique_ptr<ElemShape<Elem>> apply(Elem &elem) {
            return moonolith::make_unique<ElemShape<Elem>>(elem);
        }
    };

    template <typename T, int PhysicalDim>
    class MakeShape<Elem<T, 1, PhysicalDim>> {
    public:
        inline static std::unique_ptr<Shape<T, 1, PhysicalDim>> apply(Elem<T, 1, PhysicalDim> &elem) {
            switch (elem.type()) {
                case EDGE2: {
                    auto e_ptr = dynamic_cast<Edge2<T, PhysicalDim> *>(&elem);
                    assert(e_ptr);
                    return moonolith::make_unique<ElemShape<Edge2<T, PhysicalDim>>>(*e_ptr);
                }
                case EDGE3: {
                    auto e_ptr = dynamic_cast<Edge3<T, PhysicalDim> *>(&elem);
                    assert(e_ptr);
                    return moonolith::make_unique<ElemShape<Edge3<T, PhysicalDim>>>(*e_ptr);
                }
                default: {
                    assert(false && "implement me");
                    return nullptr;
                }
            }
        }
    };

    template <typename T, int PhysicalDim>
    class MakeShape<Elem<T, 2, PhysicalDim>> {
    public:
        inline static std::unique_ptr<Shape<T, 2, PhysicalDim>> apply(Elem<T, 2, PhysicalDim> &elem) {
            switch (elem.type()) {
                case TRI3: {
                    auto e_ptr = dynamic_cast<Tri3<T, PhysicalDim> *>(&elem);
                    assert(e_ptr);
                    return moonolith::make_unique<ElemShape<Tri3<T, PhysicalDim>>>(*e_ptr);
                }
                case TRI6: {
                    auto e_ptr = dynamic_cast<Tri6<T, PhysicalDim> *>(&elem);
                    assert(e_ptr);
                    return moonolith::make_unique<ElemShape<Tri6<T, PhysicalDim>>>(*e_ptr);
                }
                case QUAD4: {
                    auto e_ptr = dynamic_cast<Quad4<T, PhysicalDim> *>(&elem);
                    assert(e_ptr);
                    return moonolith::make_unique<ElemShape<Quad4<T, PhysicalDim>>>(*e_ptr);
                }
                case QUAD8: {
                    auto e_ptr = dynamic_cast<Quad8<T, PhysicalDim> *>(&elem);
                    assert(e_ptr);
                    return moonolith::make_unique<ElemShape<Quad8<T, PhysicalDim>>>(*e_ptr);
                }
                default: {
                    assert(false && "implement me");
                    return nullptr;
                }
            }
        }
    };

    template <class Elem>
    auto make_shape(Elem &elem) -> decltype(MakeShape<Elem>::apply(elem)) {
        return MakeShape<Elem>::apply(elem);
    }

    template <class Elem>
    class OwnedElemShape final : public Shape<typename Elem::T, Elem::Dim, Elem::PhysicalDim> {
    public:
        static const int Dim = Elem::Dim;
        static const int PhysicalDim = Elem::PhysicalDim;
        using T = typename Elem::T;
        using super = Shape<T, Dim, PhysicalDim>;
        using Point = typename super::Point;
        using CoPoint = typename super::CoPoint;

        bool intersect(const Ray<T, PhysicalDim> &ray, T &t) override { return shape_.intersect(ray, t); }

        bool intersect(const Ray<T, PhysicalDim> &ray, T &t, Point &x_param) override {
            return shape_.intersect(ray, t, x_param);
        }

        Elem &elem() { return elem_; }

        void init() { shape_.init(elem_); }

        OwnedElemShape(const T tol = 1e-10,
                       const Integer max_it = 10000,
                       const T accept_tol = 1e-6,
                       const bool verbose = false)
            : elem_(), shape_(elem_, tol, max_it, accept_tol, verbose) {}

    public:
        Elem elem_;
        ElemShape<Elem> shape_;
    };
}  // namespace moonolith

#endif  // MOONOLITH_ELEM_SHAPE_HPP
