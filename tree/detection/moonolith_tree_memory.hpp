#ifndef MOONOLITH_TREE_MEMORY_HPP
#define MOONOLITH_TREE_MEMORY_HPP

// #include "moonolith_node.hpp"
// #include "moonolith_branch.hpp"
// #include "moonolith_root.hpp"
// #include "moonolith_leaf.hpp"

#include "moonolith_forward_declarations.hpp"
#include "moonolith_synched_describable.hpp"

#include <memory>
#include <vector>

namespace moonolith {

    class TreeStats : public Describable {
    public:
        void clear() {
            n_levels_ = 0;
            n_pointers = 0;
            avg_n_pointers = 0;
            data_redundancy = 0;
            data_node_ratio = 0;
        }

        Integer n_levels() const { return n_levels_; }

        void set_n_levels(const Integer n_levels) { n_levels_ = n_levels; }

        void describe(std::ostream &os) const {
            os << "n_levels: " << n_levels_ << "\n";
            os << "n_pointers: " << n_pointers << "\n";
            os << "avg_n_pointers: " << avg_n_pointers << "\n";
            os << "data_redundancy: " << data_redundancy << "\n";
            os << "data_node_ratio: " << data_node_ratio << "\n";
        }

        TreeStats() { clear(); }

        Integer n_pointers;
        float avg_n_pointers, data_redundancy, data_node_ratio;

    private:
        Integer n_levels_;
    };

    template <class Traits>
    class TreeMemory : public SynchedDescribable {
    public:
        typedef typename Traits::DataType DataType;

        typedef moonolith::Node<Traits> Node;
        typedef moonolith::Branch<Traits> Branch;
        typedef moonolith::Leaf<Traits> Leaf;
        typedef moonolith::Root<Traits> Root;
        typedef std::shared_ptr<Node> NodePtr;

        typedef std::vector<DataType> DataContainer;
        typedef std::vector<NodePtr> NodeContainer;
        typedef std::vector<Integer> IntArray;

        inline std::shared_ptr<Branch> new_branch() { return std::make_shared<Branch>(); }

        inline std::shared_ptr<Leaf> new_leaf() { return std::make_shared<Leaf>(); }

        inline std::shared_ptr<Root> newRoot() { return std::make_shared<Root>(); }

        std::shared_ptr<Leaf> new_leaf_for(const Branch &parent) {
            std::shared_ptr<Leaf> l = new_leaf();
            l->set_level(parent.level() + 1);
            add_node(l);
            l->set_parent(parent.handle());
            stats_.set_n_levels(std::max(l->level(), stats_.n_levels()));
            return l;
        }

        inline std::shared_ptr<Branch> new_branch_from(const Leaf &leaf) {
            std::shared_ptr<Branch> b = new_branch();
            b->copy_meta_state(leaf);
            return b;
        }

        TreeStats &stats() { return stats_; }

        const TreeStats &stats() const { return stats_; }

        const std::shared_ptr<MutatorFactory<Traits> > &mutator_factory() const { return mutator_factory_; }

        inline DataContainer &data() { return data_; }

        inline const DataContainer &data() const { return data_; }

        inline NodeContainer &nodes() { return nodes_; }

        inline DataType &data(const DataHandle &handle) { return data_[handle]; }

        inline const DataType &data(const DataHandle &handle) const { return data_[handle]; }

        inline DataHandle add_data(const DataType &data) {
            const DataHandle handle = static_cast<Integer>(data_.size());
            data_.push_back(data);
            return handle;
        }

        inline const NodePtr &node(const NodeHandle &handle) const { return nodes_[handle]; }

        inline void set_node(const NodeHandle &handle, const NodePtr &node) { nodes_[handle] = node; }

        inline NodeHandle add_node(const NodePtr &node_ptr) {
            const NodeHandle handle = static_cast<Integer>(nodes_.size());
            node_ptr->set_handle(handle);
            nodes_.push_back(node_ptr);
            return handle;
        }

        template <typename Functor>
        void each(const NodeHandle &handle, Functor fun) {
            NodePtr node_ptr = node(handle);
            for (auto it = node_ptr->data_begin(); it != node_ptr->data_end(); ++it) {
                fun(*it, data(*it));
            }
        }

        inline void reserve(const Integer n_data) { data_.reserve(n_data); }

        void clear() {
            data_.clear();
            nodes_.clear();
            stats_.clear();
            init();
        }

        inline void set_mutator_factory(const std::shared_ptr<MutatorFactory<Traits> > &mutator_factory) {
            mutator_factory_ = mutator_factory;
        }

        TreeMemory() { init(); }
        virtual ~TreeMemory() {}

        void describe(std::ostream &os) const {
            os << "TreeMemory\n";
            os << "|Data|: " << data_.size() << "\n";
            os << "|Nodes|: " << nodes_.size() << "\n";
            stats_.describe(os);
        }

        inline Integer n_data() const { return static_cast<Integer>(data_.size()); }

        inline Integer n_nodes() const { return static_cast<Integer>(nodes_.size()); }

        void compute_statistics() {
            Integer n_nodes = static_cast<Integer>(nodes_.size());
            stats_.n_pointers = 0;
            for (Integer i = 0; i < n_nodes; ++i) {
                stats_.n_pointers += nodes_[i]->size();
            }

            stats_.avg_n_pointers = stats_.n_pointers / float(n_nodes);
            stats_.data_redundancy = stats_.n_pointers / float(data_.size());
            stats_.data_node_ratio = data_.size() / float(nodes_.size());
        }

        IntArray &linear_index() { return linear_index_; }

        const IntArray &linear_index() const { return linear_index_; }

        void synchronize(Communicator &comm) {
            Integer n_levels = stats_.n_levels();
            comm.all_reduce(&n_levels, 1, moonolith::MPIMax());
            stats_.set_n_levels(n_levels);
        }

    private:
        DataContainer data_;
        NodeContainer nodes_;

        std::shared_ptr<MutatorFactory<Traits> > mutator_factory_;
        TreeStats stats_;
        IntArray linear_index_;

        void init() {
            nodes_.resize(1);
            nodes_[0] = newRoot();
        }
    };
}  // namespace moonolith

#endif  // MOONOLITH_TREE_MEMORY_HPP
