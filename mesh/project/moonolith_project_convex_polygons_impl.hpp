#ifndef MOONOLITH_PROJECT_CONVEX_POLYGONS_IMPL_HPP
#define MOONOLITH_PROJECT_CONVEX_POLYGONS_IMPL_HPP

#include <cassert>
#include "moonolith_project_convex_polygons.hpp"
#include "moonolith_project_points.hpp"

namespace moonolith {

    template <typename T>
    bool ProjectConvexPolygons<T>::project_intersect_and_map_quadrature(const Polygon3 &poly1,
                                                                        const Polygon3 &poly2,
                                                                        const Plane3 &plane,
                                                                        // ref-quad-rule
                                                                        const Quadrature2<T> &ref_q,
                                                                        Quadrature3<T> &plane_q) {
        u = poly1[1] - poly1[0];
        v = poly1[2] - poly1[0];
        projected_normal = normalize(cross(u, v));

        u = poly2[1] - poly2[0];
        v = poly2[2] - poly2[0];
        onto_normal = normalize(cross(u, v));

        auto angle1 = dot(projected_normal, plane.n);
        auto angle2 = dot(onto_normal, plane.n);

        if (angle1 * angle2 >= 0) {
            return false;
        }
        if (std::abs(angle1) < std::abs(accept_angle)) {
            return false;
        }
        if (std::abs(angle2) < std::abs(accept_angle)) {
            return false;
        }

        v = plane.n;
        v.z += 1.;
        v /= length(v);

        householder_trafo.init(v);

        project_points(householder_trafo, plane.p, poly1.points, ref_poly_projected.points);

        project_points(householder_trafo, plane.p, poly2.points, ref_poly_onto.points);

        if (ref_poly_projected.area() < 0) {
            ref_poly_projected.reverse();
        }

        if (ref_poly_onto.area() < 0) {
            ref_poly_onto.reverse();
        }

        // intersect
        if (!isect_convex_poly.apply(ref_poly_projected, ref_poly_onto, isect)) {
            return false;
        }

        unproject_points(householder_trafo, plane.p, isect.points, isect_onto.points);

        map(ref_q,
            static_cast<T>(1.),  // left unscaled
            isect_onto,
            plane_q);

        return true;
    }

    template <typename T>
    bool ProjectConvexPolygons<T>::project_intersect_and_map_quadrature(const Polygon3 &projected,
                                                                        const Polygon3 &onto,
                                                                        const Quadrature2<T> &ref_q,
                                                                        Quadrature3<T> &projected_q,
                                                                        Quadrature3<T> &onto_q) {
        bool ok;

        u = onto[1] - onto[0];
        v = onto[2] - onto[0];
        onto_normal = normalize(cross(u, v));

        u = projected[1] - projected[0];
        v = projected[2] - projected[0];
        projected_normal = normalize(cross(u, v));

        auto angle = dot(onto_normal, projected_normal);

        if (angle >= accept_angle) {
            return false;
        }

        v = onto[0] + onto_normal;
        ok = make(onto[0], onto[1], onto[2], v, trafo);
        assert(ok);

        Integer proj_n_points = projected.size();
        ref_poly_projected.resize(proj_n_points);

        for (Integer i = 0; i < proj_n_points; ++i) {
            trafo.apply_inverse(projected[i], v);
            // remove last dimension for orthogonal projection
            ref_poly_projected[i].x = v.x;
            ref_poly_projected[i].y = v.y;
        }

        const T area = ref_poly_projected.area();
        if (area < 0) {
            ref_poly_projected.reverse();
        }

        assert(ref_poly_projected.area() > 0.);

        Integer onto_n_points = onto.size();
        ref_poly_onto.resize(onto_n_points);

        for (Integer i = 0; i < onto_n_points; ++i) {
            trafo.apply_inverse(onto[i], v);
            // remove last dimension for orthogonal projection
            ref_poly_onto[i].x = v.x;
            ref_poly_onto[i].y = v.y;
        }

        assert(ref_poly_onto.area() > 0.);

        // intersect
        if (!isect_convex_poly.apply(ref_poly_projected, ref_poly_onto, isect)) {
            return false;
        }

        Integer isect_n_points = isect.size();

        isect_projected.resize(isect_n_points);
        isect_onto.resize(isect_n_points);

        for (Integer i = 0; i < isect_n_points; ++i) {
            // add last dimension
            v.x = isect[i].x;
            v.y = isect[i].y;
            v.z = 0.;

            // ref-coords
            isect_projected[i] = v;

            // global coords
            trafo.apply(v, isect_onto[i]);
        }

        // construct 3D plane of the projected in the "onto" reference system
        trafo.apply_inverse(projected[0], projected_plane.p);
        trafo.apply_inverse_to_direction(projected_normal, projected_plane.n);
        projected_plane.n /= length(projected_plane.n);

        const auto line_proj = projected_plane.n[2];

        // project isect back to master
        for (Integer i = 0; i < isect_n_points; ++i) {
            // isect_projected
            v = isect_projected[i];
            const T point_dist = projected_plane.signed_dist(v);
            v.z = -point_dist / line_proj;

            // global coords
            trafo.apply(v, isect_projected[i]);
        }

        // map quadrature points to global coordinates
        map(ref_q, static_cast<T>(1.) / onto.area(), isect_onto, onto_q);

        map(ref_q, static_cast<T>(1.) / projected.area(), isect_projected, projected_q);

        return true;
    }

}  // namespace moonolith

#endif  // MOONOLITH_PROJECT_CONVEX_POLYGONS_IMPL_HPP
