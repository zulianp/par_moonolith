#ifndef MOONOLITH_BIORTHOGONAL_ELEMENT_HPP
#define MOONOLITH_BIORTHOGONAL_ELEMENT_HPP

#include "moonolith_elem.hpp"
#include "moonolith_matrix.hpp"
// #include "moonolith_elem_segment.hpp"
// #include "moonolith_elem_triangle.hpp"
// #include "moonolith_elem_quad.hpp"
// #include "moonolith_elem_tetrahedron.hpp"
// #include "moonolith_elem_hexahedron.hpp"

#include "moonolith_elem_edge.hpp"
#include "moonolith_elem_edge1.hpp"
#include "moonolith_elem_edge2.hpp"
#include "moonolith_elem_edge3.hpp"

#include "moonolith_elem_tri.hpp"
#include "moonolith_elem_tri1.hpp"
#include "moonolith_elem_tri3.hpp"
#include "moonolith_elem_tri6.hpp"

#include "moonolith_elem_quad.hpp"
#include "moonolith_elem_quad1.hpp"
#include "moonolith_elem_quad4.hpp"
#include "moonolith_elem_quad8.hpp"
#include "moonolith_elem_quad9.hpp"

#include "moonolith_elem_tet.hpp"
#include "moonolith_elem_tet1.hpp"
#include "moonolith_elem_tet10.hpp"
#include "moonolith_elem_tet4.hpp"

#include "moonolith_elem_hex.hpp"
#include "moonolith_elem_hex1.hpp"
#include "moonolith_elem_hex27.hpp"
#include "moonolith_elem_hex8.hpp"

#include "moonolith_elem_prism.hpp"
#include "moonolith_elem_prism6.hpp"

#include "moonolith_fe.hpp"

namespace moonolith {

    template <class Elem>
    class DualWeights {};

    template <typename T, int PhysicalDim>
    class DualWeights<Edge2<T, PhysicalDim>> {
    public:
        inline static void weights(Matrix<T, 2, 2> &mat) {
            mat.values[0] = 2.0;
            mat.values[1] = -1.0;
            mat.values[2] = -1.0;
            mat.values[3] = 2.0;
        }

        inline static void basis_transformation(Matrix<T, 2, 2> &mat) { mat.identity(); }

        inline static void weights(Matrix<T, -1, -1> &mat) {
            Matrix<T, 2, 2> mat_buff;
            weights(mat_buff);
            mat = mat_buff;
        }

        inline static void basis_transformation(Matrix<T, -1, -1> &mat) {
            Matrix<T, 2, 2> mat_buff;
            basis_transformation(mat_buff);
            mat = mat_buff;
        }

        template <class MatrixT>
        inline static void inverse_basis_transformation(MatrixT &mat) {
            basis_transformation(mat);
        }
    };

    template <typename T, int PhysicalDim>
    class DualWeights<Edge3<T, PhysicalDim>> {
    public:
        inline static void weights(Matrix<T, 3, 3> &mat) {
            mat.values[0] = 2.7;
            mat.values[1] = 0.9;
            mat.values[2] = -0.45;

            mat.values[3] = 0.9;
            mat.values[4] = 2.7;
            mat.values[5] = -0.45;

            mat.values[6] = -2.6;
            mat.values[7] = -2.6;
            mat.values[8] = 1.9;
        }

        inline static void basis_transformation(Matrix<T, 3, 3> &mat) { basis_transformation(1. / 5., mat); }

        inline static void basis_transformation(const T alpha, Matrix<T, 3, 3> &mat) {
            mat.zero();
            mat.values[0] = 1;
            mat.values[4] = 1;
            mat.values[2] = alpha;
            mat.values[5] = alpha;
            mat.values[8] = (1 - 2 * alpha);
        }

        inline static void weights(Matrix<T, -1, -1> &mat) {
            Matrix<T, 3, 3> mat_buff;
            weights(mat_buff);
            mat = mat_buff;
        }

        inline static void basis_transformation(Matrix<T, -1, -1> &mat) {
            Matrix<T, 3, 3> mat_buff;
            basis_transformation(mat_buff);
            mat = mat_buff;
        }

        inline static void inverse_basis_transformation(const T alpha, Matrix<T, 3, 3> &mat) {
            mat.zero();
            mat.values[0] = 1;
            mat.values[4] = 1;
            mat.values[2] = alpha / (1.0 - 2.0 * alpha);
            mat.values[5] = alpha / (1.0 - 2.0 * alpha);
            mat.values[8] = 1. / (1 - 2 * alpha);
        }

        inline static void inverse_basis_transformation(Matrix<T, 3, 3> &mat) {
            inverse_basis_transformation(1. / 5., mat);
        }

        inline static void inverse_basis_transformation(Matrix<T, -1, -1> &mat) {
            Matrix<T, 3, 3> mat_buff;
            inverse_basis_transformation(mat_buff);
            mat = mat_buff;
        }
    };

    template <typename T, int PhysicalDim>
    class DualWeights<Tri3<T, PhysicalDim>> {
    public:
        inline static void weights(Matrix<T, 3, 3> &mat) {
            mat.values[0] = 3.0;
            mat.values[1] = -1.0;
            mat.values[2] = -1.0;

            mat.values[3] = -1.0;
            mat.values[4] = 3.0;
            mat.values[5] = -1.0;

            mat.values[6] = -1.0;
            mat.values[7] = -1.0;
            mat.values[8] = 3.0;
        }

        inline static void basis_transformation(Matrix<T, 3, 3> &mat) { mat.identity(); }

        inline static void weights(Matrix<T, -1, -1> &mat) {
            Matrix<T, 3, 3> mat_buff;
            weights(mat_buff);
            mat = mat_buff;
        }

        inline static void basis_transformation(Matrix<T, -1, -1> &mat) {
            Matrix<T, 3, 3> mat_buff;
            basis_transformation(mat_buff);
            mat = mat_buff;
        }

        template <class MatrixT>
        inline static void inverse_basis_transformation(MatrixT &mat) {
            basis_transformation(mat);
        }
    };

    template <typename T, int PhysicalDim>
    class DualWeights<Tri6<T, PhysicalDim>> {
    public:
        ///@brief mat = W * T(alpha), where W is the coefficients computed on the transformed basis with alpha = 1/5
        inline static void weights(Matrix<T, 6, 6> &mat) {
            // hard-coded alpha 1./5.
            mat.values = {4.8, 0.8,  0.8,  -0.2,   0.8,  -0.2,   0.8,  4.8,  0.8,  -0.2,   -0.2,   0.8,
                          0.8, 0.8,  4.8,  0.8,    -0.2, -0.2,   -3.3, -3.3, 1.2,  3.45,   -1.425, -1.425,
                          1.2, -3.3, -3.3, -1.425, 3.45, -1.425, -3.3, 1.2,  -3.3, -1.425, -1.425, 3.45};
        }

        inline static void basis_transformation(Matrix<T, 6, 6> &mat) { basis_transformation(1. / 5., mat); }

        inline static void basis_transformation(const T alpha, Matrix<T, 6, 6> &mat) {
            mat.zero();

            mat.values[0] = 1;
            mat.values[3] = alpha;
            mat.values[5] = alpha;

            mat.values[7] = 1;
            mat.values[9] = alpha;
            mat.values[10] = alpha;

            mat.values[14] = 1;
            mat.values[16] = alpha;
            mat.values[17] = alpha;

            mat.values[21] = (1 - 2 * alpha);
            mat.values[28] = (1 - 2 * alpha);
            mat.values[35] = (1 - 2 * alpha);

            // make_transpose(mat);
        }

        inline static void weights(Matrix<T, -1, -1> &mat) {
            Matrix<T, 6, 6> mat_buff;
            weights(mat_buff);
            mat = mat_buff;
        }

        inline static void basis_transformation(Matrix<T, -1, -1> &mat) {
            Matrix<T, 6, 6> mat_buff;
            basis_transformation(mat_buff);
            mat = mat_buff;
        }

        inline static void inverse_basis_transformation(const T alpha, Matrix<T, 6, 6> &mat) {
            mat.zero();

            mat.values[0] = 1;
            mat.values[3] = (1 - 2 * alpha);
            mat.values[5] = (1 - 2 * alpha);

            mat.values[7] = 1;
            mat.values[9] = (1 - 2 * alpha);
            mat.values[10] = (1 - 2 * alpha);

            mat.values[14] = 1;
            mat.values[16] = (1 - 2 * alpha);
            mat.values[17] = (1 - 2 * alpha);

            mat.values[21] = 1. / (1 - 2 * alpha);
            mat.values[28] = 1. / (1 - 2 * alpha);
            mat.values[35] = 1. / (1 - 2 * alpha);
        }

        inline static void inverse_basis_transformation(Matrix<T, 6, 6> &mat) {
            inverse_basis_transformation(1. / 5., mat);
        }

        inline static void inverse_basis_transformation(Matrix<T, -1, -1> &mat) {
            Matrix<T, 6, 6> mat_buff;
            inverse_basis_transformation(mat_buff);
            mat = mat_buff;
        }
    };

    template <typename T, int PhysicalDim>
    class DualWeights<Quad4<T, PhysicalDim>> {
    public:
        inline static void weights(Matrix<T, 4, 4> &mat) {
            mat.values[0] = 4.0;
            mat.values[1] = -2.0;
            mat.values[2] = 1.0;
            mat.values[3] = -2.0;

            mat.values[4] = -2.0;
            mat.values[5] = 4.0;
            mat.values[6] = -2.0;
            mat.values[7] = 1.0;

            mat.values[8] = 1.0;
            mat.values[9] = -2.0;
            mat.values[10] = 4.0;
            mat.values[11] = -2.0;

            mat.values[12] = -2.0;
            mat.values[13] = 1.0;
            mat.values[14] = -2.0;
            mat.values[15] = 4.0;

            // make_transpose(mat);
        }

        inline static void basis_transformation(Matrix<T, 4, 4> &mat) { mat.identity(); }

        inline static void weights(Matrix<T, -1, -1> &mat) {
            Matrix<T, 4, 4> mat_buff;
            weights(mat_buff);
            mat = mat_buff;
        }

        inline static void basis_transformation(Matrix<T, -1, -1> &mat) {
            Matrix<T, 4, 4> mat_buff;
            basis_transformation(mat_buff);
            mat = mat_buff;
        }

        template <class MatrixT>
        inline static void inverse_basis_transformation(MatrixT &mat) {
            basis_transformation(mat);
        }
    };

    template <typename T, int PhysicalDim>
    class DualWeights<Quad8<T, PhysicalDim>> {
    public:
        ///@brief mat = W * T(alpha), where W is the coefficients computed on the transformed basis with alpha = 1/5
        inline static void weights(Matrix<T, 8, 8> &mat) {
            mat.values = {2.8,   0.1,  -0.8, 0.1,  -0.05, 0.4,   0.4,  -0.05, 0.1,   2.8,   0.1,  -0.8, -0.05,
                          -0.05, 0.4,  0.4,  -0.8, 0.1,   2.8,   0.1,  0.4,   -0.05, -0.05, 0.4,  0.1,  -0.8,
                          0.1,   2.8,  0.4,  0.4,  -0.05, -0.05, -4.2, -4.2,  3.6,   3.6,   4.8,  -1.8, -0.9,
                          -1.8,  3.6,  -4.2, -4.2, 3.6,   -1.8,  4.8,  -1.8,  -0.9,  3.6,   3.6,  -4.2, -4.2,
                          -0.9,  -1.8, 4.8,  -1.8, -4.2,  3.6,   3.6,  -4.2,  -1.8,  -0.9,  -1.8, 4.8};
        }

        inline static void basis_transformation(Matrix<T, 8, 8> &mat) { basis_transformation(1. / 5, mat); }

        inline static void basis_transformation(const T alpha, Matrix<T, 8, 8> &mat) {
            mat.zero();

            mat.values[0] = 1;
            mat.values[4] = alpha;
            mat.values[7] = alpha;

            mat.values[9] = 1;
            mat.values[12] = alpha;
            mat.values[13] = alpha;

            mat.values[18] = 1;
            mat.values[21] = alpha;
            mat.values[22] = alpha;

            mat.values[27] = 1;
            mat.values[30] = alpha;
            mat.values[31] = alpha;

            mat.values[36] = (1 - 2 * alpha);
            mat.values[45] = (1 - 2 * alpha);
            mat.values[54] = (1 - 2 * alpha);
            mat.values[63] = (1 - 2 * alpha);

            // make_transpose(mat);
        }

        inline static void weights(Matrix<T, -1, -1> &mat) {
            Matrix<T, 8, 8> mat_buff;
            weights(mat_buff);
            mat = mat_buff;
        }

        inline static void basis_transformation(Matrix<T, -1, -1> &mat) {
            Matrix<T, 8, 8> mat_buff;
            basis_transformation(mat_buff);
            mat = mat_buff;
        }

        inline static void inverse_basis_transformation(const T alpha, Matrix<T, 8, 8> &mat) {
            mat.zero();

            mat.values[0] = 1;
            mat.values[4] = (1 - 2 * alpha);
            mat.values[7] = (1 - 2 * alpha);

            mat.values[9] = 1;
            mat.values[12] = (1 - 2 * alpha);
            mat.values[13] = (1 - 2 * alpha);

            mat.values[18] = 1;
            mat.values[21] = (1 - 2 * alpha);
            mat.values[22] = (1 - 2 * alpha);

            mat.values[27] = 1;
            mat.values[30] = (1 - 2 * alpha);
            mat.values[31] = (1 - 2 * alpha);

            mat.values[36] = 1. / (1 - 2 * alpha);
            mat.values[45] = 1. / (1 - 2 * alpha);
            mat.values[54] = 1. / (1 - 2 * alpha);
            mat.values[63] = 1. / (1 - 2 * alpha);
        }

        inline static void inverse_basis_transformation(Matrix<T, 8, 8> &mat) {
            inverse_basis_transformation(1. / 5., mat);
        }

        inline static void inverse_basis_transformation(Matrix<T, -1, -1> &mat) {
            Matrix<T, 8, 8> mat_buff;
            inverse_basis_transformation(mat_buff);
            mat = mat_buff;
        }
    };

    template <typename T, int PhysicalDim>
    class DualWeights<Quad9<T, PhysicalDim>> {
    public:
        inline static void weights(Matrix<T, 9, 9> &mat) {
            mat.values = {2.25,   0.75,   0.25,   0.75,   -0.375, -0.125, -0.125, -0.375, 0.0625, 0.75,   2.25,
                          0.75,   0.25,   -0.375, -0.375, -0.125, -0.125, 0.0625, 0.25,   0.75,   2.25,   0.75,
                          -0.125, -0.375, -0.375, -0.125, 0.0625, 0.75,   0.25,   0.75,   2.25,   -0.125, -0.125,
                          -0.375, -0.375, 0.0625, -1.5,   -1.5,   -0.5,   -0.5,   2.25,   0.25,   0.75,   0.25,
                          -0.375, -0.5,   -1.5,   -1.5,   -0.5,   0.25,   2.25,   0.25,   0.75,   -0.375, -0.5,
                          -0.5,   -1.5,   -1.5,   0.75,   0.25,   2.25,   0.25,   -0.375, -1.5,   -0.5,   -0.5,
                          -1.5,   0.25,   0.75,   0.25,   2.25,   -0.375, 1.0,    1.0,    1.0,    1.0,    -1.5,
                          -1.5,   -1.5,   -1.5,   2.25};
        }

        inline static void basis_transformation(Matrix<T, 9, 9> &mat) { mat.identity(); }

        inline static void weights(Matrix<T, -1, -1> &mat) {
            Matrix<T, 9, 9> mat_buff;
            weights(mat_buff);
            mat = mat_buff;
        }

        inline static void basis_transformation(Matrix<T, -1, -1> &mat) {
            Matrix<T, 9, 9> mat_buff;
            basis_transformation(mat_buff);
            mat = mat_buff;
        }

        template <class MatrixT>
        inline static void inverse_basis_transformation(MatrixT &mat) {
            basis_transformation(mat);
        }
    };

    template <typename T, int PhysicalDim>
    class DualWeights<Tet4<T, PhysicalDim>> {
    public:
        inline static void weights(Matrix<T, 4, 4> &mat) {
            mat.values[0] = 4.0;
            mat.values[1] = -1.0;
            mat.values[2] = -1.0;
            mat.values[3] = -1.0;

            mat.values[4] = -1.0;
            mat.values[5] = 4.0;
            mat.values[6] = -1.0;
            mat.values[7] = -1.0;

            mat.values[8] = -1.0;
            mat.values[9] = -1.0;
            mat.values[10] = 4.0;
            mat.values[11] = -1.0;

            mat.values[12] = -1.0;
            mat.values[13] = -1.0;
            mat.values[14] = -1.0;
            mat.values[15] = 4.0;
        }

        inline static void basis_transformation(Matrix<T, 4, 4> &mat) { mat.identity(); }

        inline static void weights(Matrix<T, -1, -1> &mat) {
            Matrix<T, 4, 4> mat_buff;
            weights(mat_buff);
            mat = mat_buff;
        }

        inline static void basis_transformation(Matrix<T, -1, -1> &mat) {
            Matrix<T, 4, 4> mat_buff;
            basis_transformation(mat_buff);
            mat = mat_buff;
        }

        template <class MatrixT>
        inline static void inverse_basis_transformation(MatrixT &mat) {
            basis_transformation(mat);
        }
    };

    template <typename T>
    class ConstantElemDualWeights {
    public:
        inline static void weights(Matrix<T, 1, 1> &mat) { mat.values[0] = 1.0; }

        inline static void basis_transformation(Matrix<T, 1, 1> &mat) { mat.values[0] = 1.0; }

        inline static void weights(Matrix<T, -1, -1> &mat) {
            mat.resize(1, 1);
            mat.values[0] = 1.0;
        }

        inline static void basis_transformation(Matrix<T, -1, -1> &mat) {
            mat.resize(1, 1);
            mat.values[0] = 1.0;
        }

        template <class MatrixT>
        inline static void inverse_basis_transformation(MatrixT &mat) {
            basis_transformation(mat);
        }
    };

    template <typename T, int PhysicalDim>
    class DualWeights<Hex1<T, PhysicalDim>> : public ConstantElemDualWeights<T> {};

    template <typename T, int PhysicalDim>
    class DualWeights<Tet1<T, PhysicalDim>> : public ConstantElemDualWeights<T> {};

    template <typename T, int PhysicalDim>
    class DualWeights<Quad1<T, PhysicalDim>> : public ConstantElemDualWeights<T> {};

    template <typename T, int PhysicalDim>
    class DualWeights<Tri1<T, PhysicalDim>> : public ConstantElemDualWeights<T> {};

    template <typename T, int PhysicalDim>
    class DualWeights<Edge1<T, PhysicalDim>> : public ConstantElemDualWeights<T> {};

    template <typename T, int PhysicalDim>
    class DualWeights<Hex8<T, PhysicalDim>> {
    public:
        inline static void weights(Matrix<T, 8, 8> &mat) {
            mat.values = {8.0,  -4.0, 2.0,  -4.0, -4.0, 2.0,  -1.0, 2.0,  -4.0, 8.0,  -4.0, 2.0,  2.0,
                          -4.0, 2.0,  -1.0, 2.0,  -4.0, 8.0,  -4.0, -1.0, 2.0,  -4.0, 2.0,  -4.0, 2.0,
                          -4.0, 8.0,  2.0,  -1.0, 2.0,  -4.0, -4.0, 2.0,  -1.0, 2.0,  8.0,  -4.0, 2.0,
                          -4.0, 2.0,  -4.0, 2.0,  -1.0, -4.0, 8.0,  -4.0, 2.0,  -1.0, 2.0,  -4.0, 2.0,
                          2.0,  -4.0, 8.0,  -4.0, 2.0,  -1.0, 2.0,  -4.0, -4.0, 2.0,  -4.0, 8.0};
        }

        inline static void basis_transformation(Matrix<T, 8, 8> &mat) { mat.identity(); }

        inline static void weights(Matrix<T, -1, -1> &mat) {
            Matrix<T, 8, 8> mat_buff;
            weights(mat_buff);
            mat = mat_buff;
        }

        inline static void basis_transformation(Matrix<T, -1, -1> &mat) {
            Matrix<T, 8, 8> mat_buff;
            basis_transformation(mat_buff);
            mat = mat_buff;
        }

        template <class MatrixT>
        inline static void inverse_basis_transformation(MatrixT &mat) {
            basis_transformation(mat);
        }
    };

    template <typename T, int PhysicalDim>
    class DualWeights<Hex27<T, PhysicalDim>> {
    public:
        inline static void weights(Matrix<T, 27, 27> &mat) {
            mat.values = {3.375,   1.125,   0.375,   1.125,   1.125,   0.375,   0.125,   0.375,   -0.5625,
                          -0.1875, -0.1875, -0.5625, -0.5625, -0.1875, -0.0625, -0.1875, -0.1875, -0.0625,
                          -0.0625, -0.1875, 0.09375, 0.09375, 0.03125, 0.03125, 0.09375, 0.03125, -0.015625,
                          1.125,   3.375,   1.125,   0.375,   0.375,   1.125,   0.375,   0.125,   -0.5625,
                          -0.5625, -0.1875, -0.1875, -0.1875, -0.5625, -0.1875, -0.0625, -0.1875, -0.1875,
                          -0.0625, -0.0625, 0.09375, 0.09375, 0.09375, 0.03125, 0.03125, 0.03125, -0.015625,
                          0.375,   1.125,   3.375,   1.125,   0.125,   0.375,   1.125,   0.375,   -0.1875,
                          -0.5625, -0.5625, -0.1875, -0.0625, -0.1875, -0.5625, -0.1875, -0.0625, -0.1875,
                          -0.1875, -0.0625, 0.09375, 0.03125, 0.09375, 0.09375, 0.03125, 0.03125, -0.015625,
                          1.125,   0.375,   1.125,   3.375,   0.375,   0.125,   0.375,   1.125,   -0.1875,
                          -0.1875, -0.5625, -0.5625, -0.1875, -0.0625, -0.1875, -0.5625, -0.0625, -0.0625,
                          -0.1875, -0.1875, 0.09375, 0.03125, 0.03125, 0.09375, 0.09375, 0.03125, -0.015625,
                          1.125,   0.375,   0.125,   0.375,   3.375,   1.125,   0.375,   1.125,   -0.1875,
                          -0.0625, -0.0625, -0.1875, -0.5625, -0.1875, -0.0625, -0.1875, -0.5625, -0.1875,
                          -0.1875, -0.5625, 0.03125, 0.09375, 0.03125, 0.03125, 0.09375, 0.09375, -0.015625,
                          0.375,   1.125,   0.375,   0.125,   1.125,   3.375,   1.125,   0.375,   -0.1875,
                          -0.1875, -0.0625, -0.0625, -0.1875, -0.5625, -0.1875, -0.0625, -0.5625, -0.5625,
                          -0.1875, -0.1875, 0.03125, 0.09375, 0.09375, 0.03125, 0.03125, 0.09375, -0.015625,
                          0.125,   0.375,   1.125,   0.375,   0.375,   1.125,   3.375,   1.125,   -0.0625,
                          -0.1875, -0.1875, -0.0625, -0.0625, -0.1875, -0.5625, -0.1875, -0.1875, -0.5625,
                          -0.5625, -0.1875, 0.03125, 0.03125, 0.09375, 0.09375, 0.03125, 0.09375, -0.015625,
                          0.375,   0.125,   0.375,   1.125,   1.125,   0.375,   1.125,   3.375,   -0.0625,
                          -0.0625, -0.1875, -0.1875, -0.1875, -0.0625, -0.1875, -0.5625, -0.1875, -0.1875,
                          -0.5625, -0.5625, 0.03125, 0.03125, 0.03125, 0.09375, 0.09375, 0.09375, -0.015625,
                          -2.25,   -2.25,   -0.75,   -0.75,   -0.75,   -0.75,   -0.25,   -0.25,   3.375,
                          0.375,   1.125,   0.375,   0.375,   0.375,   0.125,   0.125,   1.125,   0.125,
                          0.375,   0.125,   -0.5625, -0.5625, -0.0625, -0.1875, -0.0625, -0.1875, 0.09375,
                          -0.75,   -2.25,   -2.25,   -0.75,   -0.25,   -0.75,   -0.75,   -0.25,   0.375,
                          3.375,   0.375,   1.125,   0.125,   0.375,   0.375,   0.125,   0.125,   1.125,
                          0.125,   0.375,   -0.5625, -0.0625, -0.5625, -0.0625, -0.1875, -0.1875, 0.09375,
                          -0.75,   -0.75,   -2.25,   -2.25,   -0.25,   -0.25,   -0.75,   -0.75,   1.125,
                          0.375,   3.375,   0.375,   0.125,   0.125,   0.375,   0.375,   0.375,   0.125,
                          1.125,   0.125,   -0.5625, -0.1875, -0.0625, -0.5625, -0.0625, -0.1875, 0.09375,
                          -2.25,   -0.75,   -0.75,   -2.25,   -0.75,   -0.25,   -0.25,   -0.75,   0.375,
                          1.125,   0.375,   3.375,   0.375,   0.125,   0.125,   0.375,   0.125,   0.375,
                          0.125,   1.125,   -0.5625, -0.0625, -0.1875, -0.0625, -0.5625, -0.1875, 0.09375,
                          -2.25,   -0.75,   -0.25,   -0.75,   -2.25,   -0.75,   -0.25,   -0.75,   0.375,
                          0.125,   0.125,   0.375,   3.375,   1.125,   0.375,   1.125,   0.375,   0.125,
                          0.125,   0.375,   -0.0625, -0.5625, -0.1875, -0.1875, -0.5625, -0.0625, 0.09375,
                          -0.75,   -2.25,   -0.75,   -0.25,   -0.75,   -2.25,   -0.75,   -0.25,   0.375,
                          0.375,   0.125,   0.125,   1.125,   3.375,   1.125,   0.375,   0.375,   0.375,
                          0.125,   0.125,   -0.0625, -0.5625, -0.5625, -0.1875, -0.1875, -0.0625, 0.09375,
                          -0.25,   -0.75,   -2.25,   -0.75,   -0.25,   -0.75,   -2.25,   -0.75,   0.125,
                          0.375,   0.375,   0.125,   0.375,   1.125,   3.375,   1.125,   0.125,   0.375,
                          0.375,   0.125,   -0.0625, -0.1875, -0.5625, -0.5625, -0.1875, -0.0625, 0.09375,
                          -0.75,   -0.25,   -0.75,   -2.25,   -0.75,   -0.25,   -0.75,   -2.25,   0.125,
                          0.125,   0.375,   0.375,   1.125,   0.375,   1.125,   3.375,   0.125,   0.125,
                          0.375,   0.375,   -0.0625, -0.1875, -0.1875, -0.5625, -0.5625, -0.0625, 0.09375,
                          -0.75,   -0.75,   -0.25,   -0.25,   -2.25,   -2.25,   -0.75,   -0.75,   1.125,
                          0.125,   0.375,   0.125,   0.375,   0.375,   0.125,   0.125,   3.375,   0.375,
                          1.125,   0.375,   -0.1875, -0.5625, -0.0625, -0.1875, -0.0625, -0.5625, 0.09375,
                          -0.25,   -0.75,   -0.75,   -0.25,   -0.75,   -2.25,   -2.25,   -0.75,   0.125,
                          1.125,   0.125,   0.375,   0.125,   0.375,   0.375,   0.125,   0.375,   3.375,
                          0.375,   1.125,   -0.1875, -0.0625, -0.5625, -0.0625, -0.1875, -0.5625, 0.09375,
                          -0.25,   -0.25,   -0.75,   -0.75,   -0.75,   -0.75,   -2.25,   -2.25,   0.375,
                          0.125,   1.125,   0.125,   0.125,   0.125,   0.375,   0.375,   1.125,   0.375,
                          3.375,   0.375,   -0.1875, -0.1875, -0.0625, -0.5625, -0.0625, -0.5625, 0.09375,
                          -0.75,   -0.25,   -0.25,   -0.75,   -2.25,   -0.75,   -0.75,   -2.25,   0.125,
                          0.375,   0.125,   1.125,   0.375,   0.125,   0.125,   0.375,   0.375,   1.125,
                          0.375,   3.375,   -0.1875, -0.0625, -0.1875, -0.0625, -0.5625, -0.5625, 0.09375,
                          1.5,     1.5,     1.5,     1.5,     0.5,     0.5,     0.5,     0.5,     -2.25,
                          -2.25,   -2.25,   -2.25,   -0.25,   -0.25,   -0.25,   -0.25,   -0.75,   -0.75,
                          -0.75,   -0.75,   3.375,   0.375,   0.375,   0.375,   0.375,   1.125,   -0.5625,
                          1.5,     1.5,     0.5,     0.5,     1.5,     1.5,     0.5,     0.5,     -2.25,
                          -0.25,   -0.75,   -0.25,   -2.25,   -2.25,   -0.75,   -0.75,   -2.25,   -0.25,
                          -0.75,   -0.25,   0.375,   3.375,   0.375,   1.125,   0.375,   0.375,   -0.5625,
                          0.5,     1.5,     1.5,     0.5,     0.5,     1.5,     1.5,     0.5,     -0.25,
                          -2.25,   -0.25,   -0.75,   -0.75,   -2.25,   -2.25,   -0.75,   -0.25,   -2.25,
                          -0.25,   -0.75,   0.375,   0.375,   3.375,   0.375,   1.125,   0.375,   -0.5625,
                          0.5,     0.5,     1.5,     1.5,     0.5,     0.5,     1.5,     1.5,     -0.75,
                          -0.25,   -2.25,   -0.25,   -0.75,   -0.75,   -2.25,   -2.25,   -0.75,   -0.25,
                          -2.25,   -0.25,   0.375,   1.125,   0.375,   3.375,   0.375,   0.375,   -0.5625,
                          1.5,     0.5,     0.5,     1.5,     1.5,     0.5,     0.5,     1.5,     -0.25,
                          -0.75,   -0.25,   -2.25,   -2.25,   -0.75,   -0.75,   -2.25,   -0.25,   -0.75,
                          -0.25,   -2.25,   0.375,   0.375,   1.125,   0.375,   3.375,   0.375,   -0.5625,
                          0.5,     0.5,     0.5,     0.5,     1.5,     1.5,     1.5,     1.5,     -0.75,
                          -0.75,   -0.75,   -0.75,   -0.25,   -0.25,   -0.25,   -0.25,   -2.25,   -2.25,
                          -2.25,   -2.25,   1.125,   0.375,   0.375,   0.375,   0.375,   3.375,   -0.5625,
                          -1.0,    -1.0,    -1.0,    -1.0,    -1.0,    -1.0,    -1.0,    -1.0,    1.5,
                          1.5,     1.5,     1.5,     1.5,     1.5,     1.5,     1.5,     1.5,     1.5,
                          1.5,     1.5,     -2.25,   -2.25,   -2.25,   -2.25,   -2.25,   -2.25,   3.375};
        }

        inline static void basis_transformation(Matrix<T, 27, 27> &mat) { mat.identity(); }

        inline static void weights(Matrix<T, -1, -1> &mat) {
            Matrix<T, 27, 27> mat_buff;
            weights(mat_buff);
            mat = mat_buff;
        }

        inline static void basis_transformation(Matrix<T, -1, -1> &mat) {
            Matrix<T, 27, 27> mat_buff;
            basis_transformation(mat_buff);
            mat = mat_buff;
        }

        template <class MatrixT>
        inline static void inverse_basis_transformation(MatrixT &mat) {
            basis_transformation(mat);
        }
    };

    template <typename T, int PhysicalDim>
    class DualWeights<Tet10<T, PhysicalDim>> {
    public:
        inline static void weights(Matrix<T, 10, 10> &mat) {
            mat.values = {
                7.0,    0.7,    0.7,    0.7,   0.175,  0.7,    0.175,  0.175,  0.7,    0.7,    0.7,  7.0,    0.7,
                0.7,    0.175,  0.175,  0.7,   0.7,    0.175,  0.7,    0.7,    0.7,    7.0,    0.7,  0.7,    0.175,
                0.175,  0.7,    0.7,    0.175, 0.7,    0.7,    0.7,    7.0,    0.7,    0.7,    0.7,  0.175,  0.175,
                0.175,  -3.9,   -3.9,   1.2,   1.2,    5.55,   -1.875, -1.875, -1.875, -1.875, 1.2,  1.2,    -3.9,
                -3.9,   1.2,    -1.875, 5.55,  -1.875, 1.2,    -1.875, -1.875, -3.9,   1.2,    -3.9, 1.2,    -1.875,
                -1.875, 5.55,   -1.875, 1.2,   -1.875, -3.9,   1.2,    1.2,    -3.9,   -1.875, 1.2,  -1.875, 5.55,
                -1.875, -1.875, 1.2,    -3.9,  1.2,    -3.9,   -1.875, -1.875, 1.2,    -1.875, 5.55, -1.875, 1.2,
                1.2,    -3.9,   -3.9,   1.2,   -1.875, -1.875, -1.875, -1.875, 5.55,
            };
        }

        inline static void basis_transformation(Matrix<T, 10, 10> &mat) {
            mat.values = {1, 0, 0, 0, 0.2, 0,   0.2, 0.2, 0,   0,   0, 1, 0, 0, 0.2, 0.2, 0, 0,   0.2, 0,
                          0, 0, 1, 0, 0,   0.2, 0.2, 0,   0,   0.2, 0, 0, 0, 1, 0,   0,   0, 0.2, 0.2, 0.2,
                          0, 0, 0, 0, 0.6, 0,   0,   0,   0,   0,   0, 0, 0, 0, 0,   0.6, 0, 0,   0,   0,
                          0, 0, 0, 0, 0,   0,   0.6, 0,   0,   0,   0, 0, 0, 0, 0,   0,   0, 0.6, 0,   0,
                          0, 0, 0, 0, 0,   0,   0,   0,   0.6, 0,   0, 0, 0, 0, 0,   0,   0, 0,   0,   0.6};

            // make_transpose(mat);
        }

        inline static void weights(Matrix<T, -1, -1> &mat) {
            Matrix<T, 10, 10> mat_buff;
            weights(mat_buff);
            mat = mat_buff;
        }

        inline static void basis_transformation(Matrix<T, -1, -1> &mat) {
            Matrix<T, 10, 10> mat_buff;
            basis_transformation(mat_buff);
            mat = mat_buff;
        }

        inline static void inverse_basis_transformation(const T alpha, Matrix<T, 10, 10> &mat) {
            mat.zero();

            mat.values[0] = 1;
            mat.values[4] = (1 - 2 * alpha);
            mat.values[6] = (1 - 2 * alpha);
            mat.values[7] = (1 - 2 * alpha);

            mat.values[11] = 1;
            mat.values[14] = (1 - 2 * alpha);
            mat.values[15] = (1 - 2 * alpha);
            mat.values[18] = (1 - 2 * alpha);

            mat.values[22] = 1;
            mat.values[25] = (1 - 2 * alpha);
            mat.values[26] = (1 - 2 * alpha);
            mat.values[29] = (1 - 2 * alpha);

            mat.values[33] = 1;
            mat.values[37] = (1 - 2 * alpha);
            mat.values[38] = (1 - 2 * alpha);
            mat.values[39] = (1 - 2 * alpha);

            mat.values[44] = 1. / (1 - 2 * alpha);
            mat.values[55] = 1. / (1 - 2 * alpha);
            mat.values[66] = 1. / (1 - 2 * alpha);
            mat.values[77] = 1. / (1 - 2 * alpha);
            mat.values[88] = 1. / (1 - 2 * alpha);
            mat.values[99] = 1. / (1 - 2 * alpha);
        }

        inline static void inverse_basis_transformation(Matrix<T, 10, 10> &mat) {
            inverse_basis_transformation(1. / 5., mat);
        }

        inline static void inverse_basis_transformation(Matrix<T, -1, -1> &mat) {
            Matrix<T, 10, 10> mat_buff;
            inverse_basis_transformation(mat_buff);
            mat = mat_buff;
        }
    };

    template <typename T, int PhysicalDim>
    class DualWeights<Prism6<T, PhysicalDim>> {
    public:
        inline static void weights(Matrix<T, 6, 6> &mat) {
            mat.values = {6.0,  -2.0, -2.0, -3.0, 1.0, 1.0,  -2.0, 6.0, -2.0, 1.0,  -3.0, 1.0,
                          -2.0, -2.0, 6.0,  1.0,  1.0, -3.0, -3.0, 1.0, 1.0,  6.0,  -2.0, -2.0,
                          1.0,  -3.0, 1.0,  -2.0, 6.0, -2.0, 1.0,  1.0, -3.0, -2.0, -2.0, 6.0};
        }

        inline static void basis_transformation(Matrix<T, 6, 6> &mat) { mat.identity(); }

        inline static void weights(Matrix<T, -1, -1> &mat) {
            Matrix<T, 6, 6> mat_buff;
            weights(mat_buff);
            mat = mat_buff;
        }

        inline static void basis_transformation(Matrix<T, -1, -1> &mat) {
            Matrix<T, 6, 6> mat_buff;
            basis_transformation(mat_buff);
            mat = mat_buff;
        }

        inline static void inverse_basis_transformation(const T /*alpha*/, Matrix<T, 6, 6> &mat) { mat.identity(); }

        inline static void inverse_basis_transformation(Matrix<T, 6, 6> &mat) { mat.identity(); }

        inline static void inverse_basis_transformation(Matrix<T, -1, -1> &mat) {
            Matrix<T, 6, 6> mat_buff;
            inverse_basis_transformation(mat_buff);
            mat = mat_buff;
        }
    };

    template <typename T, int Dim, int PhysicalDim>
    class DualWeights<Elem<T, Dim, PhysicalDim>> {
    public:
        inline static void weights(Matrix<T, -1, -1> &mat) {
            assert(false && "It should never come here");
            mat.values = {};
        }

        inline static void basis_transformation(Matrix<T, -1, -1> &mat) {
            assert(false && "It should never come here");
            mat.values = {};
        }

        template <class MatrixT>
        inline static void inverse_basis_transformation(MatrixT &mat) {
            assert(false && "It should never come here");
            mat.values = {};
        }
    };

    template <class Elem>
    class DualWeightsWithType {
    public:
        inline static void weights(const FEType &, Matrix<typename Elem::T, Elem::NNodes, Elem::NNodes> &mat) {
            DualWeights<Elem>::weights(mat);
        }

        inline static void basis_transformation(const FEType &,
                                                Matrix<typename Elem::T, Elem::NNodes, Elem::NNodes> &mat) {
            DualWeights<Elem>::basis_transformation(mat);
        }

        inline static void inverse_basis_transformation(const FEType &,
                                                        Matrix<typename Elem::T, Elem::NNodes, Elem::NNodes> &mat) {
            DualWeights<Elem>::inverse_basis_transformation(mat);
        }
    };

    template <typename T, int Dim, int PhysicalDim>
    class DualWeightsWithType<Elem<T, Dim, PhysicalDim>> {
    public:
        inline static void weights(const FEType &type, Matrix<T, -1, -1> &mat) {
            switch (type) {
                case EDGE1: {
                    DualWeights<Edge1<T, PhysicalDim>>::weights(mat);
                    break;
                }
                case EDGE2: {
                    DualWeights<Edge2<T, PhysicalDim>>::weights(mat);
                    break;
                }
                case EDGE3: {
                    DualWeights<Edge3<T, PhysicalDim>>::weights(mat);
                    break;
                }
                case TRI1: {
                    DualWeights<Tri1<T, PhysicalDim>>::weights(mat);
                    break;
                }
                case TRI3: {
                    DualWeights<Tri3<T, PhysicalDim>>::weights(mat);
                    break;
                }
                case TRI6: {
                    DualWeights<Tri6<T, PhysicalDim>>::weights(mat);
                    break;
                }
                case QUAD4: {
                    DualWeights<Quad4<T, PhysicalDim>>::weights(mat);
                    break;
                }
                case QUAD8: {
                    DualWeights<Quad8<T, PhysicalDim>>::weights(mat);
                    break;
                }
                case QUAD9: {
                    DualWeights<Quad9<T, PhysicalDim>>::weights(mat);
                    break;
                }
                case TET4: {
                    DualWeights<Tet4<T, PhysicalDim>>::weights(mat);
                    break;
                }
                case TET10: {
                    DualWeights<Tet10<T, PhysicalDim>>::weights(mat);
                    break;
                }
                case HEX8: {
                    DualWeights<Hex8<T, PhysicalDim>>::weights(mat);
                    break;
                }
                case HEX27: {
                    DualWeights<Hex27<T, PhysicalDim>>::weights(mat);
                    break;
                }
                case HEX1: {
                    DualWeights<Hex1<T, PhysicalDim>>::weights(mat);
                    break;
                }
                case PRISM6: {
                    DualWeights<Prism6<T, PhysicalDim>>::weights(mat);
                    break;
                }
                default: {
                    assert(false && "implement me");
                    break;
                }
            }
        }

        inline static void basis_transformation(const FEType &type, Matrix<T, -1, -1> &mat) {
            switch (type) {
                case EDGE1: {
                    DualWeights<Edge1<T, PhysicalDim>>::basis_transformation(mat);
                    break;
                }
                case EDGE2: {
                    DualWeights<Edge2<T, PhysicalDim>>::basis_transformation(mat);
                    break;
                }
                case EDGE3: {
                    DualWeights<Edge3<T, PhysicalDim>>::basis_transformation(mat);
                    break;
                }
                case TRI1: {
                    DualWeights<Tri1<T, PhysicalDim>>::basis_transformation(mat);
                    break;
                }
                case TRI3: {
                    DualWeights<Tri3<T, PhysicalDim>>::basis_transformation(mat);
                    break;
                }
                case TRI6: {
                    DualWeights<Tri6<T, PhysicalDim>>::basis_transformation(mat);
                    break;
                }
                case QUAD1: {
                    DualWeights<Quad1<T, PhysicalDim>>::basis_transformation(mat);
                    break;
                }
                case QUAD4: {
                    DualWeights<Quad4<T, PhysicalDim>>::basis_transformation(mat);
                    break;
                }
                case QUAD8: {
                    DualWeights<Quad8<T, PhysicalDim>>::basis_transformation(mat);
                    break;
                }
                case QUAD9: {
                    DualWeights<Quad9<T, PhysicalDim>>::basis_transformation(mat);
                    break;
                }
                case TET4: {
                    DualWeights<Tet4<T, PhysicalDim>>::basis_transformation(mat);
                    break;
                }
                case TET10: {
                    DualWeights<Tet10<T, PhysicalDim>>::basis_transformation(mat);
                    break;
                }
                case TET1: {
                    DualWeights<Tet1<T, PhysicalDim>>::basis_transformation(mat);
                    break;
                }
                case HEX8: {
                    DualWeights<Hex8<T, PhysicalDim>>::basis_transformation(mat);
                    break;
                }
                case HEX27: {
                    DualWeights<Hex27<T, PhysicalDim>>::basis_transformation(mat);
                    break;
                }
                case HEX1: {
                    DualWeights<Hex1<T, PhysicalDim>>::basis_transformation(mat);
                    break;
                }
                case PRISM6: {
                    DualWeights<Prism6<T, PhysicalDim>>::basis_transformation(mat);
                    break;
                }
                default: {
                    assert(false && "implement me");
                    break;
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////

        inline static void inverse_basis_transformation(const FEType &type, Matrix<T, -1, -1> &mat) {
            switch (type) {
                case EDGE1: {
                    DualWeights<Edge1<T, PhysicalDim>>::inverse_basis_transformation(mat);
                    break;
                }
                case EDGE2: {
                    DualWeights<Edge2<T, PhysicalDim>>::inverse_basis_transformation(mat);
                    break;
                }
                case EDGE3: {
                    DualWeights<Edge3<T, PhysicalDim>>::inverse_basis_transformation(mat);
                    break;
                }
                case TRI1: {
                    DualWeights<Tri1<T, PhysicalDim>>::inverse_basis_transformation(mat);
                    break;
                }
                case TRI3: {
                    DualWeights<Tri3<T, PhysicalDim>>::inverse_basis_transformation(mat);
                    break;
                }
                case TRI6: {
                    DualWeights<Tri6<T, PhysicalDim>>::inverse_basis_transformation(mat);
                    break;
                }
                case QUAD1: {
                    DualWeights<Quad1<T, PhysicalDim>>::inverse_basis_transformation(mat);
                    break;
                }
                case QUAD4: {
                    DualWeights<Quad4<T, PhysicalDim>>::inverse_basis_transformation(mat);
                    break;
                }
                case QUAD8: {
                    DualWeights<Quad8<T, PhysicalDim>>::inverse_basis_transformation(mat);
                    break;
                }
                case QUAD9: {
                    DualWeights<Quad9<T, PhysicalDim>>::inverse_basis_transformation(mat);
                    break;
                }
                case TET4: {
                    DualWeights<Tet4<T, PhysicalDim>>::inverse_basis_transformation(mat);
                    break;
                }
                case TET10: {
                    DualWeights<Tet10<T, PhysicalDim>>::inverse_basis_transformation(mat);
                    break;
                }
                case TET1: {
                    DualWeights<Tet1<T, PhysicalDim>>::inverse_basis_transformation(mat);
                    break;
                }
                case HEX8: {
                    DualWeights<Hex8<T, PhysicalDim>>::inverse_basis_transformation(mat);
                    break;
                }
                case HEX27: {
                    DualWeights<Hex27<T, PhysicalDim>>::inverse_basis_transformation(mat);
                    break;
                }
                case HEX1: {
                    DualWeights<Hex1<T, PhysicalDim>>::inverse_basis_transformation(mat);
                    break;
                }
                case PRISM6: {
                    DualWeights<Prism6<T, PhysicalDim>>::inverse_basis_transformation(mat);
                    break;
                }
                default: {
                    assert(false && "implement me");
                    break;
                }
            }
        }
    };

    template <class Elem, bool NATIVE = true>
    class Dual final : public FEInterface<typename Elem::T, Elem::Dim, Elem::PhysicalDim> {
    public:
        static const int Dim = Elem::Dim;
        static const int PhysicalDim = Elem::PhysicalDim;
        static const int NNodes = Elem::NNodes;

        using T = typename Elem::T;
        using FE = moonolith::FEInterface<T, Dim, PhysicalDim>;

        using Point = typename Elem::Point;
        using Vector = typename Elem::Vector;

        using CoPoint = typename Elem::CoPoint;
        using CoVector = typename Elem::CoVector;

        using uint = unsigned int;

        template <typename E>
        using Array = typename StorageTraits<E, NNodes>::Type;

        Dual()
            : primal_(nullptr) /*, alpha_(alpha)*/
        {
            if (NNodes > 0) {
                init_weights();
            }
        }

        void init(FE &primal) {
            primal_ = &primal;
            init();
        }

        void init(FE &primal, const Matrix<T, NNodes, NNodes> &w) {
            this->w = w;
            init_with_stored_weights(primal);
        }

        void init_with_stored_weights(FE &primal) {
            primal_ = &primal;
            set_sizes(primal_->n_shape_functions());
            reinit();
        }

        void reinit() {
            assert(primal_ && "init has to be called at least once");

            const uint n = n_quad_points();
            const uint n_qp = n_quad_points();
            const uint n_fun = n_shape_functions();

            if (has_fun()) {
                for (uint i = 0; i < n_fun; ++i) {
                    const auto offset_i = i * n_fun;

                    fun_[i].resize(n);

                    for (uint k = 0; k < n_qp; ++k) {
                        fun_[i][k] = 0.;

                        for (uint j = 0; j < n_fun; ++j) {
                            fun_[i][k] += w.values[offset_i + j] * primal_->fun(j, k);
                        }
                    }
                }
            }

            if (has_grad()) {
                Vector g;

                for (uint i = 0; i < n_fun; ++i) {
                    const auto offset_i = i * n_fun;

                    grad_[i].resize(n_qp);

                    for (uint k = 0; k < n_qp; ++k) {
                        moonolith::fill(grad_[i][k], static_cast<T>(0.));

                        for (uint j = 0; j < n_fun; ++j) {
                            primal_->grad(j, k, g);
                            grad_[i][k] += w.values[offset_i + j] * g;
                        }
                    }
                }
            }
        }

        inline T fun(const Integer i, const Integer qp) override { return fun_[i][qp]; }

        inline void grad(const Integer i, const Integer qp, Vector &g) override { g = grad_[i][qp]; }

        inline void jacobian(const Integer qp, Matrix<T, PhysicalDim, Dim> &J) override {
            assert(primal_);
            primal_->jacobian(qp, J);
        }

        inline T dx(const Integer qp) override {
            assert(primal_);
            return primal_->dx(qp);
        }

        inline uint n_shape_functions() const override { return static_cast<uint>(fun_.size()); }

        inline uint n_quad_points() const override {
            assert(primal_);
            return static_cast<uint>(primal_->n_quad_points());
        }

        inline bool has_fun() const override {
            assert(primal_);
            return primal_->has_fun();
        }

        inline bool has_grad() const override {
            assert(primal_);
            return primal_->has_grad();
        }

        inline bool has_jacobian() const override {
            assert(primal_);
            return primal_->has_jacobian();
        }

        inline FEType type() const override {
            assert(primal_);
            return primal_->type();
        }

        inline Matrix<T, NNodes, NNodes> &weights() { return w; }

    private:
        FE *primal_;
        // T alpha_;
        Matrix<T, NNodes, NNodes> w;
        Array<Storage<T>> fun_;
        Array<Storage<Vector>> grad_;

        void init_weights() { DualWeights<Elem>::weights(w); }

        void init_weights_with_type() { DualWeightsWithType<Elem>::weights(type(), w); }

        inline void set_sizes(const int n_funs) {
            Resize<Array<Storage<T>>>::apply(fun_, n_funs);
            Resize<Array<Storage<Vector>>>::apply(grad_, n_funs);
        }

        void init() {
            assert(primal_);
            set_sizes(primal_->n_shape_functions());

            if (NNodes <= 0) {
                init_weights_with_type();
            }

            reinit();
        }
    };

}  // namespace moonolith

#endif  // MOONOLITH_BIORTHOGONAL_ELEMENT_HPP
