#ifndef MOONOLITH_ELEM_SEGMENT_HPP
#define MOONOLITH_ELEM_SEGMENT_HPP

#include "moonolith_elem_edge.hpp"

namespace moonolith {

    template <typename T_, int PhysicalDim_ = 1>
    class Edge2 final : public Edge<T_, 1, PhysicalDim_> {
    public:
        static const int Order = 1;
        static const int Dim = 1;
        static const int PhysicalDim = PhysicalDim_;
        static const int NNodes = 2;

        using T = T_;

        using Vector = moonolith::Vector<T, Dim>;
        using CoVector = moonolith::Vector<T, PhysicalDim>;

        using Point = moonolith::Vector<T, Dim>;
        using CoPoint = moonolith::Vector<T, PhysicalDim>;

    private:
        class Grad final {
        public:
            Grad() {
                f[0] = [](const Point &, Vector &g) { g.x = -1.0; };

                f[1] = [](const Point &, Vector &g) { g.x = 1; };
            }

            std::array<std::function<void(const Point &, Vector &)>, 2> f;
        };

        class Fun final {
        public:
            Fun() {
                f[0] = [](const Point &p) -> T { return 1.0 - p.x; };

                f[1] = [](const Point &p) -> T { return p.x; };
            }

            std::array<std::function<T(const Point &)>, 2> f;
        };

    public:
        T fun(const Integer i, const Point &p) const override { return fun_.f[i](p); }

        void hessian(const Integer, const Point &, std::array<T, Dim * Dim> &H) {
            std::fill(std::begin(H), std::end(H), 0.0);
        }

        void grad(const Integer i, const Point &p, Vector &g) const override { grad_.f[i](p, g); }

        void jacobian(const Point &p, std::array<T, PhysicalDim * Dim> &J) const override {
            (void)p;
            affine_approx_jacobian(J);
        }

        void affine_approx_jacobian(std::array<T, PhysicalDim * Dim> &J) const override {
            Affine<Edge2>::jacobian(*this, J);
        }

        CoPoint &node(const Integer i) override { return nodes_[i]; }

        const CoPoint &node(const Integer i) const override { return nodes_[i]; }

        void point(const Point &p, CoPoint &q) const override { IsoParametric<Edge2>::point(*this, p, q); }

        Edge2() {}

        bool is_affine() const override { return true; }
        inline bool is_simplex() const override { return true; }

        inline int n_nodes() const override { return NNodes; }
        const std::array<CoPoint, 2> &nodes() const { return nodes_; }
        inline int order() const override { return Order; }

        const Fun &fun() const { return fun_; }

        void make_reference() { Reference<Edge2>::points(nodes_); }

        inline ElemType type() const override { return EDGE2; }

        inline T measure() const override { return distance(nodes_[0], nodes_[1]); }

        inline T reference_measure() const override { return 1.0; }

        inline void make(Line<T, PhysicalDim_> &line) const override {
            line.p0 = node(0);
            line.p1 = node(1);
        }

        inline void make(PolyLine<T, PhysicalDim_> &poly_line) const override {
            poly_line.points.resize(2);
            poly_line.points[0] = node(0);
            poly_line.points[1] = node(1);
        }

    private:
        std::array<CoPoint, 2> nodes_;
        const Fun fun_;
        const Grad grad_;
    };

    template <typename T, int PhysicalDim>
    class Reference<Edge2<T, PhysicalDim>> {
    public:
        static const int NNodes = 2;

        template <class Point, std::size_t Size>
        static void points(std::array<Point, Size> &nodes) {
            static_assert(Size >= NNodes, "size must be at least 2");
            // p0
            nodes[0].x = 0.0;

            // p1
            nodes[1].x = 1.0;
        }

        static T measure() { return 1.0; }
    };

    template <typename T, int PhysicalDim>
    inline void make(const Edge2<T, PhysicalDim> &elem, PolyLine<T, PhysicalDim> &poly_line) {
        poly_line.points.resize(2);
        poly_line.points[0] = elem.node(0);
        poly_line.points[1] = elem.node(1);
    }

    template <typename T, int PhysicalDim>
    class GaussQRule<Edge2<T, PhysicalDim>> {
    public:
        static bool get(const Integer order, Quadrature1<T> &q) { return Gauss::get(order, q); }
    };
}  // namespace moonolith

#endif  // MOONOLITH_ELEM_SEGMENT_HPP
