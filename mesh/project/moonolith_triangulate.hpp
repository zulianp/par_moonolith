#ifndef MOONOLITH_TRIANGULATE_HPP
#define MOONOLITH_TRIANGULATE_HPP

#include "moonolith_empirical_tol.hpp"
#include "moonolith_polygon.hpp"

namespace moonolith {

    template <typename T>
    class TriangulatePolygon {
    public:
        TriangulatePolygon(const T tol = GeometricTol<T>::value()) : tol(tol) {}
        void apply(const Polygon<T, 2> &in_polygon, Storage<int> &result);

    private:
        using Point2 = moonolith::Vector<T, 2>;
        using Polygon2 = moonolith::Polygon<T, 2>;
        using Triangle2 = Point2[3];

        T signed_triangle_area(const Point2 &a, const Point2 &b, const Point2 &c);

        T triangle_area(const Point2 &a, const Point2 &b, const Point2 &c);

        bool ccw(const Point2 &a, const Point2 &b, const Point2 &c);
        bool cw(const Point2 &a, const Point2 &b, const Point2 &c);
        bool point_in_triangle(const Point2 &p, const Triangle2 &t);
        bool ear_q(int i, int j, int k, const Polygon2 &p);
        void add_triangle(int i, int j, int k, Storage<int> &t);

        // buffers
        Storage<int> l, r;
        Triangle2 t;
        T tol;
    };

    template <typename T>
    void triangulate_polygon(const Polygon<T, 2> &p, Storage<int> &t) {
        TriangulatePolygon<T>().apply(p, t);
    }
}  // namespace moonolith

#endif  // MOONOLITH_TRIANGULATE_HPP
