#ifndef MOONOLITH_OBSTACLE_HPP
#define MOONOLITH_OBSTACLE_HPP

#include "moonolith_function_space.hpp"
#include "moonolith_iso_parametric_transform.hpp"
#include "moonolith_matrix_inserter.hpp"
#include "moonolith_ray_casting_tree.hpp"

#include "moonolith_assembly.hpp"
#include "moonolith_elem_dual.hpp"
#include "moonolith_fe.hpp"
#include "moonolith_transformed_fe.hpp"

#include "par_moonolith_instance.hpp"

#include <unordered_set>
#include <vector>

namespace moonolith {

    template <typename T, int PhysicalDim>
    class Obstacle : public SynchedDescribable {
    public:
        using Tree = moonolith::RayCastingTree<T, PhysicalDim>;
        using Collection = moonolith::Mesh<T, PhysicalDim>;
        using SpaceT = moonolith::FunctionSpace<Collection>;
        using Adapter = typename Tree::DataType;
        using Elem = moonolith::Elem<T, PhysicalDim - 1, PhysicalDim>;
        using QuadratureT = moonolith::Quadrature<T, PhysicalDim - 1>;
        using PhysicalQuadratureT = moonolith::Quadrature<T, PhysicalDim>;
        using Transform = moonolith::Transform<T, PhysicalDim - 1, PhysicalDim>;

        using NormalVec = moonolith::Vector<T, -1>;
        using GapVec = moonolith::Vector<T, -1>;
        using ElemMatrix = moonolith::Matrix<T, -1, -1>;

        class Buffers {
        public:
            Buffers(const Communicator &comm)
                : mass_matrix(comm),
                  trafo(comm),
                  inverse_trafo(comm),
                  gap(comm),
                  normal(comm),
                  is_contact(comm, false) {}

            MatrixInserter<T> mass_matrix, trafo, inverse_trafo;
            MatrixInserter<T> gap, normal;
            MatrixInserter<T> is_contact;
        };

        Obstacle(std::shared_ptr<Tree> tree) : tree_(std::move(tree)) {}

        inline const Buffers &buffers() const { return *buffers_; }

        inline void set_gap_bounds(T gap_negative_bound, T gap_positive_bound) {
            gap_negative_bound_ = gap_negative_bound;
            gap_positive_bound_ = gap_positive_bound;
        }

        inline void set_max_gap(T max_gap) {
            gap_negative_bound_ = -max_gap;
            gap_positive_bound_ = max_gap;
        }

        bool accept_direction(const Vector<T, PhysicalDim> &dir) const {
            if (skip_dir_ == -1) return true;

            T cos_angle = dir[skip_dir_];

            if (1 - std::abs(cos_angle) < skip_dir_tol_) {
                return false;
            } else {
                return true;
            }
        }

        void assemble(const std::unordered_set<int> &tags, SpaceT &space) {
            if (extend_contact_surface_) {
                assemble_with_avg(tags, space);
                return;
            }

            static const bool plot_rays = false;

            auto &mesh = space.mesh();
            const Integer n_elements = mesh.n_elements();

            is_elem_in_contact_.resize(n_elements);

            buffers_ = std::make_shared<Buffers>(mesh.comm());

            std::shared_ptr<Elem> elem;
            std::shared_ptr<Transform> trafo;

            MatlabScripter script;
            Line<T, PhysicalDim> line;

            if (plot_rays)
            //
            {
                script.hold_on();
            }

            Integer n_isect_rays = 0;

            Vector<T, PhysicalDim> p;
            Ray<T, PhysicalDim> ray;
            for (Integer i = 0; i < n_elements; ++i) {
                if (!tags.empty() && tags.find(mesh.elem(i).block) == tags.end()) continue;

                element_set_up(space, i, elem, trafo);

                Integer n_qp = physical_quadrature_.n_points();
                const Integer n_funs = fe_.n_shape_functions();

                bool contact_found = true;
                for (Integer qp = 0; qp < n_qp; ++qp) {
                    if (!accept_direction(normals_[qp])) {
                        contact_found = false;
                        break;
                    }

                    ray.o = physical_quadrature_.point(qp);
                    ray.dir = normals_[qp];

                    line.p0 = ray.o;

                    // int n_intersected_q = 0;

                    T t;
                    if (!tree_->intersect(ray, t, p) || t >= gap_positive_bound_ || t <= gap_negative_bound_) {
                        t = far_away_;

                        if (plot_rays)
                        //
                        {
                            line.p1 = ray.o + 0.5 * ray.dir;
                            script.plot(line, "\'r-o\'");
                        }

                        contact_found = false;

                        if (!plot_rays) {
                            break;
                        }

                    } else {
                        ++n_isect_rays;
                        // ++n_intersected_q;

                        if (plot_rays)
                        //
                        {
                            line.p1 = p;
                            script.plot(line, "\'g-o\'");
                        }
                    }

                    for (Integer fi = 0; fi < n_funs; ++fi) {
                        gap_vec_[fi] += dual_fe_.fun(fi, qp) * t * dual_fe_.dx(qp);
                    }

                    for (Integer fi = 0; fi < n_funs; ++fi) {
                        auto offset_f = fi * PhysicalDim;
                        for (int d = 0; d < PhysicalDim; ++d) {
                            normal_vec_[offset_f + d] += dual_fe_.fun(fi, qp) * ray.dir[d] * dual_fe_.dx(qp);
                        }
                    }
                }

                is_elem_in_contact_[i] = contact_found;

                if (contact_found) {
                    add_to_matrix(space, i);
                }
            }

            // logger() << "rays " << n_isect_rays << "\n";

            if (plot_rays)
            //
            {
                script.save("rays.m");
            }

            finalize(space);
        }

        // Linear
        //  template<typename T>
        void diff_linear_fit(const T x, const T y, const T d, const T *g, T *grad, T *Hessian) {
            T x0 = x + y - 1;
            T x1 = d + g[0] * x0 - g[1] * x - g[2] * y;
            T x2 = -x0;
            T x3 = x * x2;
            T x4 = x2 * y;
            T x5 = x * y;
            grad[0] += x0 * x1;
            grad[1] += -x * x1;
            grad[2] += -x1 * y;
            Hessian[0] += pow(x0, 2);
            Hessian[1] += x3;
            Hessian[2] += x4;
            Hessian[3] += x3;
            Hessian[4] += pow(x, 2);
            Hessian[5] += x5;
            Hessian[6] += x4;
            Hessian[7] += x5;
            Hessian[8] += pow(y, 2);
        }

        // Bilinear
        //  template<typename T>
        void diff_bilinear_fit(const T x, const T y, const T d, const T *g, T *grad, T *Hessian) {
            T x0 = x * y;
            T x1 = y - 1;
            T x2 = x * x1;
            T x3 = x - 1;
            T x4 = x1 * x3;
            T x5 = d - g[0] * x4 + g[1] * x2 - g[2] * x0 + g[3] * x2;
            T x6 = x2 * x5;
            T x7 = pow(x1, 2);
            T x8 = -x * x3 * x7;
            T x9 = x2 * x3 * y;
            T x10 = pow(x, 2);
            T x11 = x10 * x7;
            T x12 = -x1 * x10 * y;
            grad[0] += -x4 * x5;
            grad[1] += x6;
            grad[2] += -x0 * x5;
            grad[3] += x6;
            Hessian[0] += pow(x3, 2) * x7;
            Hessian[1] += x8;
            Hessian[2] += x9;
            Hessian[3] += x8;
            Hessian[4] += x8;
            Hessian[5] += x11;
            Hessian[6] += x12;
            Hessian[7] += x11;
            Hessian[8] += x9;
            Hessian[9] += x12;
            Hessian[10] += x10 * pow(y, 2);
            Hessian[11] += x12;
            Hessian[12] += x8;
            Hessian[13] += x11;
            Hessian[14] += x12;
            Hessian[15] += x11;
        }

        void constant_fit(int n_qp, int n_intersected_q, const Storage<int> &intersected, Storage<Real> &ts) {
            Real avg_t = 0.;
            for (Integer qp = 0; qp < n_qp; ++qp) {
                if (intersected[qp]) avg_t += ts[qp];
            }

            avg_t /= n_intersected_q;

            for (Integer qp = 0; qp < n_qp; ++qp) {
                if (!intersected[qp]) ts[qp] = avg_t;
            }
        }

        void fit(int n_qp, int n_intersected_q, const Storage<int> &intersected, Storage<Real> &ts) {
            // constant_fit(n_qp, n_intersected_q, intersected, ts);
            // return;

            if (n_intersected_q < 3 || PhysicalDim < 3) {
                constant_fit(n_qp, n_intersected_q, intersected, ts);
                return;
            }

            Real avg_t = 0.;
            for (Integer qp = 0; qp < n_qp; ++qp) {
                if (intersected[qp]) avg_t += ts[qp];
            }

            // if (n_intersected_q >= 3) {
            Matrix<T, 3, 3> Hessian;
            Matrix<T, 3, 3> Hessian_inv;
            Vector<T, 3> grad;
            Vector<T, 3> u = {avg_t, avg_t, avg_t};

            for (int k = 0; k < 20; ++k) {
                // 1) Assemble linear system

                Hessian.zero();
                grad.zero();

                for (Integer qp = 0; qp < n_qp; ++qp) {
                    if (intersected[qp]) {
                        diff_linear_fit(
                            quadrature_.point(qp)[0], quadrature_.point(qp)[1], ts[qp], &u[0], &grad[0], &Hessian.values[0]);
                    }
                }

                // 2) Solve linear system
                T detH = measure(Hessian);

                if (detH == 0.) {
                    // If it fails we go constant
                    assert(false);
                    u[0] = avg_t;
                    u[1] = avg_t;
                    u[2] = avg_t;
                    break;
                }

                invert3(Hessian.values, Hessian_inv.values, detH);

                // 3) Update solution
                for (int d1 = 0; d1 < 3; ++d1) {
                    for (int d2 = 0; d2 < 3; ++d2) {
                        u[d1] -= Hessian_inv(d1, d2) * grad[d2];
                    }
                }

                if(StandardTol<T>::value() > length(grad)) {
                    // std::cout << "Converged! Newton iterations: "  << k << " " << length(grad) << std::endl;
                    break;
                }
            }

            // if(StandardTol<T>::value() < length(grad)) {
            //     std::cout << "Did not converge! Residual: "  << length(grad) << std::endl;
            // }

            for (Integer qp = 0; qp < n_qp; ++qp) {
                if (!intersected[qp]) {
                    T x = quadrature_.point(qp)[0];
                    T y = quadrature_.point(qp)[1];
                    ts[qp] = (1 - x - y) * u[0] + x * u[1] + y * u[2];
                }
            }
            // }
        }

        void assemble_with_avg(const std::unordered_set<int> &tags, SpaceT &space) {
            static const bool plot_rays = false;

            auto &mesh = space.mesh();
            const Integer n_elements = mesh.n_elements();

            is_elem_in_contact_.resize(n_elements);

            buffers_ = std::make_shared<Buffers>(mesh.comm());

            std::shared_ptr<Elem> elem;
            std::shared_ptr<Transform> trafo;

            MatlabScripter script;
            Line<T, PhysicalDim> line;

            if (plot_rays)
            //
            {
                script.hold_on();
            }

            Storage<int> intersected;
            Storage<Real> ts;

            Integer n_isect_rays = 0;

            Vector<T, PhysicalDim> p;
            Ray<T, PhysicalDim> ray;
            for (Integer i = 0; i < n_elements; ++i) {
                if (!tags.empty() && tags.find(mesh.elem(i).block) == tags.end()) continue;

                element_set_up(space, i, elem, trafo);

                Integer n_qp = physical_quadrature_.n_points();
                const Integer n_funs = fe_.n_shape_functions();

                intersected.resize(n_qp);
                ts.resize(n_qp);

                for (auto &ii : intersected) {
                    ii = 0.;
                }

                for (auto &tt : ts) {
                    tt = 0.;
                }

                int n_intersected_q = 0;

                bool contact_found = false;
                for (Integer qp = 0; qp < n_qp; ++qp) {
                    if (!accept_direction(normals_[qp])) {
                        break;
                    }

                    ray.o = physical_quadrature_.point(qp);
                    ray.dir = normals_[qp];

                    line.p0 = ray.o;

                    T t;
                    if (!tree_->intersect(ray, t, p) || t >= gap_positive_bound_ || t <= gap_negative_bound_) {
                        t = far_away_;

                        if (plot_rays)
                        //
                        {
                            line.p1 = ray.o + 0.5 * ray.dir;
                            script.plot(line, "\'r-o\'");
                        }

                    } else {
                        ++n_isect_rays;
                        ++n_intersected_q;

                        if (plot_rays)
                        //
                        {
                            line.p1 = p;
                            script.plot(line, "\'g-o\'");
                        }

                        contact_found = true;

                        ts[qp] = t;
                        intersected[qp] = true;
                    }
                }

                if (contact_found) {
                    if (n_qp != n_intersected_q) {
                        // Handle partial contacts by extending the contact surface
                        Real avg_t = 0.;
                        for (Integer qp = 0; qp < n_qp; ++qp) {
                            if (intersected[qp]) avg_t += ts[qp];
                        }

                        avg_t /= n_intersected_q;

                        for (Integer qp = 0; qp < n_qp; ++qp) {
                            if (!intersected[qp]) ts[qp] = avg_t;
                        }

                        fit(n_qp, n_intersected_q, intersected, ts);
                    }

                    is_elem_in_contact_[i] = contact_found;

                    for (Integer qp = 0; qp < n_qp; ++qp) {
                        for (Integer fi = 0; fi < n_funs; ++fi) {
                            gap_vec_[fi] += dual_fe_.fun(fi, qp) * ts[qp] * dual_fe_.dx(qp);
                        }

                        for (Integer fi = 0; fi < n_funs; ++fi) {
                            auto offset_f = fi * PhysicalDim;
                            for (int d = 0; d < PhysicalDim; ++d) {
                                normal_vec_[offset_f + d] += dual_fe_.fun(fi, qp) * ray.dir[d] * dual_fe_.dx(qp);
                            }
                        }
                    }

                    add_to_matrix(space, i);
                }
            }

            logger() << "rays " << n_isect_rays << "\n";

            if (plot_rays)
            //
            {
                script.save("rays.m");
            }

            finalize(space);
        }

        void element_set_up(SpaceT &space,
                            const int i,
                            std::shared_ptr<Elem> &elem,
                            std::shared_ptr<Transform> &trafo) {
            auto &mesh = space.mesh();

            make(space, mesh.elem(i), elem);
            make_transform(*elem, trafo);
            init_quadrature(*elem);
            transform_quadrature(*trafo);
            init_normals(*elem);

            fe_.init(*elem, quadrature_, true, false, true);
            dual_fe_.init(fe_);
            trafo_fe_.init(fe_);

            const Integer n_funs = fe_.n_shape_functions();

            Resize<NormalVec>::apply(normal_vec_, n_funs * PhysicalDim);
            Resize<GapVec>::apply(gap_vec_, n_funs);
            Resize<ElemMatrix>::apply(mass_matrix_, n_funs, n_funs);

            normal_vec_.set(0.0);
            gap_vec_.set(0.0);
        }

        void add_to_matrix(SpaceT &space, const int i) {
            assemble_mass_matrix(trafo_fe_, dual_fe_, mass_matrix_);

            trafo_ = trafo_fe_.transformation();
            make_transpose(trafo_);
            inverse_trafo_ = trafo_fe_.inverse_transformation();
            make_transpose(inverse_trafo_);

            auto &dofs = space.dof_map().dofs(i);

            buffers_->mass_matrix.insert(dofs, dofs, mass_matrix_);
            buffers_->gap.insert(dofs, gap_vec_);
            buffers_->normal.insert_contiguous(dofs, PhysicalDim, normal_vec_);
            buffers_->trafo.insert(dofs, dofs, trafo_);
            buffers_->inverse_trafo.insert(dofs, dofs, inverse_trafo_);
            buffers_->is_contact.insert(dofs, 1.0);
        }

        static int order_of_quadrature(const Elem &e) {
            if (e.is_simplex()) {
                return e.order();
            } else {
                return e.order() * (PhysicalDim - 1);
            }
        }

        void init_quadrature(const Elem &e) {
            int order = order_of_quadrature(e);

            if (extend_contact_surface_) {
                order *= 2;
            }

            if (!e.is_affine()) {
                // assumes tensor product
                order = order + (order - 1) * 2;
            }

            if (order == current_quadrature_order_) return;

            if (e.is_affine()) {
                Gauss::get(order, quadrature_);
                assert(!quadrature_.empty());

                physical_quadrature_.resize(quadrature_.n_points());
                normals_.resize(quadrature_.n_points());
            }
        }

        void transform_quadrature(Transform &t) {
            Integer np = quadrature_.n_points();

            for (Integer i = 0; i < np; ++i) {
                t.apply(quadrature_.point(i), physical_quadrature_.point(i));
            }
        }

        void init_normals(const Elem &e) {
            Integer np = quadrature_.n_points();

            Vector<T, PhysicalDim> cols[PhysicalDim - 1];

            for (Integer i = 0; i < np; ++i) {
                e.jacobian(quadrature_.point(i), J_);

                for (int col_i = 0; col_i < PhysicalDim - 1; ++col_i) {
                    for (int row_i = 0; row_i < PhysicalDim; ++row_i) {
                        cols[col_i][row_i] = J_[row_i * (PhysicalDim - 1) + col_i];
                    }

                    cols[col_i] /= length(cols[col_i]);
                }

                normal(cols, normals_[i]);
            }

            if (snap_to_canonical_vectors_) {
                for (Integer i = 0; i < np; ++i) {
                    int dir = 0;
                    T max_dot = std::abs(normals_[i][0]);

                    for (int d = 1; d < PhysicalDim; ++d) {
                        T abs_dot = std::abs(normals_[i][d]);
                        if (abs_dot > max_dot) {
                            dir = d;
                            max_dot = abs_dot;
                        }
                    }

                    int sign = normals_[i][dir] > 0 ? 1 : -1;
                    normals_[i].set(0.);
                    normals_[i][dir] = sign;
                }
            }
        }

        static void normal(const Vector<T, 2> *cols, Vector<T, 2> &n) {
            perp(cols[0], n);
            assert(length(n) != 0.);
            // Seems that a sign inversion is required...
            n /= -length(n);
        }

        static void normal(const Vector<T, 3> *cols, Vector<T, 3> &n) {
            n = cross(cols[0], cols[1]);
            assert(length(n) != 0.);
            n /= length(n);
        }

        void describe(std::ostream &os) const override { buffers_->gap.get().describe(os); }

        void snap_to_canonical_vectors(const bool val) { snap_to_canonical_vectors_ = val; }

        void skip_dir(const int dir) { skip_dir_ = dir; }

        void skip_dir_tol(T tol) { skip_dir_tol_ = tol; }

        void extend_contact_surface(const bool val) { extend_contact_surface_ = val; }

    private:
        std::shared_ptr<Tree> tree_;
        int current_quadrature_order_{-1};
        QuadratureT quadrature_;
        PhysicalQuadratureT physical_quadrature_;
        std::vector<Vector<T, PhysicalDim>> normals_;
        std::array<T, PhysicalDim *(PhysicalDim - 1)> J_;
        T far_away_{1e6};

        T gap_positive_bound_{1e6};
        T gap_negative_bound_{-1e-6};

        FE<Elem> fe_;
        Dual<Elem> dual_fe_;
        TransformedFE<Elem> trafo_fe_;

        NormalVec normal_vec_;
        GapVec gap_vec_;
        ElemMatrix mass_matrix_, trafo_, inverse_trafo_;

        std::vector<bool> is_elem_in_contact_;

        std::shared_ptr<Buffers> buffers_;

        bool snap_to_canonical_vectors_{false};
        bool extend_contact_surface_{true};
        int skip_dir_{-1};
        T skip_dir_tol_{0.2};

        void finalize(SpaceT &space) {
            const Integer n_local_dofs = static_cast<Integer>(space.dof_map().n_local_dofs());

            // first finalize the buffers_
            buffers_->mass_matrix.finalize(n_local_dofs, n_local_dofs);
            buffers_->trafo.finalize(n_local_dofs, n_local_dofs);
            buffers_->inverse_trafo.finalize(n_local_dofs, n_local_dofs);
            buffers_->gap.finalize(n_local_dofs);

            buffers_->normal.finalize(n_local_dofs);
            buffers_->is_contact.finalize(n_local_dofs);

            T sum_mass_matrix = buffers_->mass_matrix.get().sum();
            T sum_trafo = buffers_->trafo.get().sum();
            T sum_inverse_trafo = buffers_->inverse_trafo.get().sum();

            if (Moonolith::instance().verbose()) {
                logger() << " sum(mass_matrix): " << sum_mass_matrix << " sum(trafo): " << sum_trafo
                         << " sum(inverse_trafo): " << sum_inverse_trafo << std::endl;
            }
        }
    };
}  // namespace moonolith

#endif  // MOONOLITH_OBSTACLE_HPP
