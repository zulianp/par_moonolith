#include "moonolith_par_l2_assembler_no_covering.hpp"

namespace moonolith {
    template class ParL2TransferNoCovering<Real, 1, 1, 1>;
    template class ParL2TransferNoCovering<Real, 2, 2, 2>;
    template class ParL2TransferNoCovering<Real, 3, 3, 3>;

    template class ParL2TransferNoCovering<Real, 2, 1, 1>;
    // template class ParL2TransferNoCovering<Real, 2, 2, 1>;

    template class ParL2TransferNoCovering<Real, 3, 2, 2>;
    // template class ParL2TransferNoCovering<Real, 3, 3, 2>;
}  // namespace moonolith