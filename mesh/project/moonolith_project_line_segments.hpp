#ifndef MOONOLITH_PROJECT_LINE_SEGMENTS_HPP
#define MOONOLITH_PROJECT_LINE_SEGMENTS_HPP

#include "moonolith_empirical_tol.hpp"
#include "moonolith_line.hpp"
#include "moonolith_map_quadrature.hpp"
#include "moonolith_simplex_transform.hpp"

namespace moonolith {

    template <typename T>
    class ProjectLineSegments {
    public:
        using Vector2 = moonolith::Vector<T, 2>;
        using Line2 = moonolith::Line<T, 2>;

        ProjectLineSegments(const T &accept_angle = -0.5) : accept_angle(accept_angle) {}

        static void make_normal(const Vector2 &p0, const Vector2 &p1, Vector2 &n) {
            n = p1 - p0;
            n /= length(n);
            std::swap(n.x, n.y);
            n.x = -n.x;
        }

        bool apply(const Line2 &projected,
                   const Line2 &onto,
                   Line2 &result_projected,
                   Line2 &result_onto,
                   const T tol = GeometricTol<T>::value()) {
            using std::max;
            using std::min;

            make_normal(onto.p0, onto.p1, onto_normal);
            make_normal(projected.p0, projected.p1, projected_normal);

            auto angle = dot(onto_normal, projected_normal);

            if (angle >= accept_angle) {
                return false;
            }

            //////////////////////////////////////////////////////////////////////////////////////////
            // computing geometric surface projection
            // moving from global space to reference space

            make(onto.p0, onto.p1, trafo);
            trafo.apply_inverse(projected.p0, ref_projected.p0);
            trafo.apply_inverse(projected.p1, ref_projected.p1);

            T x_min;  //, y_min;
            T x_max;  //, y_max;

            if (ref_projected.p0.x < ref_projected.p1.x) {
                x_min = ref_projected.p0.x;
                // y_min = ref_projected.p0.y;

                x_max = ref_projected.p1.x;
                // y_max = ref_projected.p1.y;

            } else {
                x_min = ref_projected.p1.x;
                // y_min = ref_projected.p1.y;

                x_max = ref_projected.p0.x;
                // y_max = ref_projected.p0.y;
            }

            // check if there is any intersection
            if (x_max - tol <= 0) {
                return false;
            }

            if (x_min + tol >= 1) {
                return false;
            }

            if (x_max <= x_min + tol) {
                return false;
            }

            //////////////////////////////////////////////////////////////////////////////////////////
            // construct projected plane

            projected_plane.p = ref_projected.p0;
            make_normal(ref_projected.p0, ref_projected.p1, projected_plane.n);

            //////////////////////////////////////////////////////////////////////////////////////////

            const T x_min_isect = max(x_min, (T)0);
            const T x_max_isect = min(x_max, (T)1);

            ref_onto.p0.x = x_min_isect;
            ref_onto.p0.y = 0.0;
            ref_onto.p1.x = x_max_isect;
            ref_onto.p1.y = 0.0;

            //////////////////////////////////////////////////////////////////////////////////////////
            // Ray casting to master element plane
            ref_projected.p0.x = x_min_isect;
            ref_projected.p0.y = -projected_plane.signed_dist(ref_onto.p0) / projected_plane.n.y;

            ref_projected.p1.x = x_max_isect;
            ref_projected.p1.y = -projected_plane.signed_dist(ref_onto.p1) / projected_plane.n.y;
            //////////////////////////////////////////////////////////////////////////////////////////

            // move back to global coordinates
            trafo.apply(ref_projected.p0, result_projected.p0);
            trafo.apply(ref_projected.p1, result_projected.p1);

            // move back to global coordinates
            trafo.apply(ref_onto.p0, result_onto.p0);
            trafo.apply(ref_onto.p1, result_onto.p1);
            return true;
        }

        bool project_intersect_and_map_quadrature(const Line2 &projected,
                                                  const Line2 &onto,
                                                  // ref-quad-rule
                                                  const Quadrature1<T> &q_ref,
                                                  Quadrature2<T> &projected_q,
                                                  Quadrature2<T> &onto_q,
                                                  const T tol = GeometricTol<T>::value()) {
            if (!apply(projected, onto, result_projected_buff, result_onto_buff, tol)) {
                return false;
            }

            map(q_ref, 1 / measure(projected), result_projected_buff, projected_q);
            map(q_ref, 1 / measure(onto), result_onto_buff, onto_q);
            return true;
        }

    private:
        AffineTransform<T, 2, 2> trafo;
        Line<T, 2> ref_projected, ref_onto;
        Line2 result_projected_buff, result_onto_buff;
        Plane<T, 2> projected_plane;
        T accept_angle;
        Vector2 onto_normal, projected_normal;
    };

}  // namespace moonolith

#endif  // MOONOLITH_PROJECT_LINE_SEGMENTS_HPP
