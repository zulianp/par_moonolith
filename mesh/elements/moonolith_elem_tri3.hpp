#ifndef MOONOLITH_ELEM_TRI_3_HPP
#define MOONOLITH_ELEM_TRI_3_HPP

#include "moonolith_elem_tri.hpp"

namespace moonolith {

    template <typename T_, int PhysicalDim_ = 2>
    class Tri3 final : public Tri<T_, 1, PhysicalDim_> {
    public:
        static const int Order = 1;
        static const int Dim = 2;
        static const int PhysicalDim = PhysicalDim_;
        static const int NNodes = 3;

        using T = T_;

        using Vector = moonolith::Vector<T, Dim>;
        using CoVector = moonolith::Vector<T, PhysicalDim>;

        using Point = moonolith::Vector<T, Dim>;
        using CoPoint = moonolith::Vector<T, PhysicalDim>;

        using Super = moonolith::Tri<T_, 1, PhysicalDim_>;
        using Polygon = typename Super::Polygon;

        static_assert(PhysicalDim >= 2, "cannot be in R^1");

    private:
        class Grad final {
        public:
            Grad() {
                f[0] = [](const Point &, Vector &g) {
                    g.x = -1.0;
                    g.y = -1.0;
                };

                f[1] = [](const Point &, Vector &g) {
                    g.x = 1;
                    g.y = 0.0;
                };

                f[2] = [](const Point &, Vector &g) {
                    g.x = 0.0;
                    g.y = 1.0;
                };
            }

            std::array<std::function<void(const Point &, Vector &)>, 3> f;
        };

        class Fun final {
        public:
            Fun() {
                f[0] = [](const Point &p) -> T { return 1.0 - p.x - p.y; };

                f[1] = [](const Point &p) -> T { return p.x; };

                f[2] = [](const Point &p) -> T { return p.y; };
            }

            std::array<std::function<T(const Point &)>, 3> f;
        };

    public:
        T fun(const Integer i, const Point &p) const override { return fun_.f[i](p); }

        void grad(const Integer i, const Point &p, Vector &g) const override { grad_.f[i](p, g); }

        void hessian(const Integer, const Point &, std::array<T, Dim * Dim> &H) {
            std::fill(std::begin(H), std::end(H), 0.0);
        }

        void jacobian(const Point &p, std::array<T, PhysicalDim * Dim> &J) const override {
            (void)p;
            affine_approx_jacobian(J);
        }

        void affine_approx_jacobian(std::array<T, PhysicalDim * Dim> &J) const override {
            Affine<Tri3>::jacobian(*this, J);
        }

        CoPoint &node(const Integer i) override { return nodes_[i]; }

        const CoPoint &node(const Integer i) const override { return nodes_[i]; }

        inline std::array<CoPoint, 3> &nodes() { return nodes_; }

        inline const std::array<CoPoint, 3> &nodes() const { return nodes_; }

        void point(const Point &p, CoPoint &q) const override { IsoParametric<Tri3>::point(*this, p, q); }

        Tri3() {}

        bool is_affine() const override { return true; }
        inline bool is_simplex() const override { return true; }

        inline int n_nodes() const override { return NNodes; }
        inline int order() const override { return Order; }

        inline void make_reference() { Reference<Tri3>::points(nodes_); }

        inline ElemType type() const override { return TRI3; }

        inline T measure() const override { return 0.5 * trapezoid_area(nodes_[0], nodes_[1], nodes_[2]); }

        inline T reference_measure() const override { return 0.5; }

        inline void make(Polygon &poly) const override {
            poly.resize(3);

            for (int i = 0; i < 3; ++i) {
                poly[i] = node(i);
            }
        }

    private:
        std::array<CoPoint, 3> nodes_;
        const Fun fun_;
        const Grad grad_;
    };

    template <typename T, int PhysicalDim>
    class Reference<Tri3<T, PhysicalDim>> {
    public:
        // using Point = typename Tri3<T, 1, PhysicalDim>::Point;
        static const int NNodes = 3;

        template <class Point, std::size_t Size>
        static void points(std::array<Point, Size> &nodes) {
            static_assert(Size >= 3, "size must be at least 3");
            // p0
            nodes[0].x = 0.0;
            nodes[0].y = 0.0;

            // p1
            nodes[1].x = 1.0;
            nodes[1].y = 0.0;

            // p2
            nodes[2].x = 0.0;
            nodes[2].y = 1.0;
        }

        inline static T measure() { return 0.5; }
    };

    template <typename T, int PhysicalDim>
    class GaussQRule<Tri3<T, PhysicalDim>> {
    public:
        static bool get(const Integer order, Quadrature2<T> &q) { return Gauss::get(order, q); }
    };

}  // namespace moonolith

#endif  // MOONOLITH_ELEM_TRI_3_HPP
