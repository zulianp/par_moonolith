#ifndef MOONOLITH_INTERSECT_PLANE_IMPL_HPP
#define MOONOLITH_INTERSECT_PLANE_IMPL_HPP

#include "moonolith_intersect_plane.hpp"

namespace moonolith {

	template<typename T, int Dim>
	short IntersectLineWithPlane<T, Dim>::apply(const Line &line, const Plane &plane, T &t, const T tol)
	{
		ray_dir = line.p1 - line.p0;
		auto len = length(ray_dir);
		ray_dir /= len;

		const T cos_angle = dot(plane.n, ray_dir);

		if(fabs(cos_angle) < tol) {
			t = 0;
			return COPLANAR;
		}

			//distance from plane
		const T dist = plane.signed_dist(line.p0);

			//point in ray trajectory
		t = dist/cos_angle;
		t /= len;

		if(dist <= 0. && cos_angle > 0.) {
				//intersection with ray coming from behind the plane
			t *= -1.;
		} else if(dist > 0. && cos_angle < 0.) {
				//intersection with ray coming from the front of the plane
			t *= -1;
		}

		if(t >= -tol && t <= 1. + tol) {
				//inside segment
			return INSIDE;
		} else {
				//outside segment
			return OUTSIDE;
		}
	}

}

#endif //MOONOLITH_INTERSECT_PLANE_IMPL_HPP
