#ifndef MOONOLITH_MAP_SPARSE_MATRIX_HPP
#define MOONOLITH_MAP_SPARSE_MATRIX_HPP

#include "moonolith_check_stream.hpp"
#include "moonolith_forward_declarations.hpp"
#include "moonolith_redistribute.hpp"
#include "moonolith_synched_describable.hpp"

#include <cmath>
#include <map>

namespace moonolith {

    template <typename _EntryType>
    class SparseMatrix : public SynchedDescribable, public Serializable {
    public:
        typedef _EntryType EntryType;

    private:
        typedef std::pair<Integer, Integer> IntPair;
        typedef std::map<IntPair, EntryType> Map;

        Communicator comm_;
        Map data_;
        Integer rows_, cols_, local_rows_, local_cols_;

        typedef typename Map::const_iterator ConstMapIter;
        typedef typename Map::iterator MapIter;

    public:
        using SynchedDescribable::synch_describe;

        SparseMatrix(const Communicator &comm)
            : comm_(comm), data_(), rows_(0), cols_(0), local_rows_(0), local_cols_(0) {}

        SparseMatrix(const Communicator &comm,
                     const Integer rows,
                     const Integer cols,
                     const Integer local_rows,
                     const Integer local_cols)
            : comm_(comm), data_(), rows_(rows), cols_(cols), local_rows_(local_rows), local_cols_(local_cols) {}

        inline Integer rows() const { return rows_; }
        inline Integer cols() const { return cols_; }

        inline Integer local_rows() const { return local_rows_; }
        inline Integer local_cols() const { return local_cols_; }

        inline Integer n_local_entries() const { return static_cast<Integer>(data_.size()); }
        inline const Communicator &comm() const { return comm_; }

        // distribution is of size: rows() + 1
        template <class InsertMode>
        bool finalize(const InsertMode &insert_mode, const std::vector<Integer> &distribution) {
            assert(is_valid());
            local_rows_ = distribution[comm_.rank() + 1];
            Redistribute<SparseMatrix> redist(comm_);
            return redist.apply(distribution, *this, insert_mode);
        }

        template <class InsertMode>
        bool finalize(const InsertMode &insert_mode, const Integer rows, const Integer local_rows) {
            rows_ = rows;
            local_rows_ = local_rows;

            std::vector<Integer> distribution(comm_.size() + 1, 0);

            distribution[comm_.rank() + 1] = local_rows;

            comm_.all_reduce(&distribution[0], distribution.size(), MPIMax());

            // skip first since it is always zero
            for (Integer i = 1; i < comm_.size(); ++i) {
                distribution[i + 1] += distribution[i];
            }

            return finalize(insert_mode, distribution);
        }

        template <class InsertMode>
        bool finalize(const InsertMode &insert_mode) {
            assert(rows_ > 0);
            assert(local_rows_ > 0);

            return finalize(insert_mode, rows_, local_rows_);
        }

        inline void resize(const Integer rows, const Integer cols, const Integer local_rows, const Integer local_cols) {
            set_size(rows, cols, local_rows, local_cols);
            data_.clear();
        }

        inline void set_size(const Integer rows, const Integer cols) {
            rows_ = rows;
            cols_ = cols;

            local_rows_ = -1;
            local_cols_ = -1;
        }

        inline void set_size(const Integer rows,
                             const Integer cols,
                             const Integer local_rows,
                             const Integer local_cols) {
            rows_ = rows;
            cols_ = cols;

            local_rows_ = local_rows;
            local_cols_ = local_cols;
        }

        inline void clear() { data_.clear(); }

        inline void nullify() {
            set_size(0, 0, 0, 0);
            clear();
        }

        void finalize_local_structure() {
            for (ConstMapIter it = data_.begin(); it != data_.end(); ++it) {
                rows_ = std::max(it->first.first + 1, rows_);
                cols_ = std::max(it->first.second + 1, cols_);
            }

            assert(rows_ > 0 || data_.empty());
            assert(cols_ > 0 || data_.empty());
        }

        void finalize_structure() {
            finalize_local_structure();

            Integer structure[2] = {rows_, cols_};
            comm_.all_reduce(structure, 2, MPIMax());

            rows_ = structure[0];
            cols_ = structure[1];
        }

        // synched
        bool is_diagonal() const {
            int ret = 1;
            for (ConstMapIter it = data_.begin(); it != data_.end(); ++it) {
                if (it->first.first != it->first.second) {
                    ret = 0;
                    break;
                }
            }

            comm_.all_reduce(&ret, 1, MPIMin());
            return ret;
        }

        inline bool is_zero() const { return (rows_ == 0) | (cols_ == 0); }

        inline EntryType &add(const Integer i, const Integer j, const EntryType value) { return at(i, j) += value; }

        inline EntryType &push_entry(const Integer i, const Integer j, const EntryType &entry) {
            return (at(i, j) = entry);
        }

        inline void set(const Integer i, const Integer j, const EntryType &entry) { push_entry(i, j, entry); }

        inline EntryType operator()(const Integer i, const Integer j) const { return at(i, j); }

        inline EntryType at(const Integer i, const Integer j) const {
            assert(i < rows() || rows() == 0);
            assert(j < cols() || cols() == 0);
            assert(i >= 0);
            assert(j >= 0);

            ConstMapIter it = data_.find(IntPair(i, j));
            if (it == data_.end()) {
                return 0;
            }
            return it->second;
        }

        inline void remove_zeros(const EntryType tol) {
            for (MapIter it = data_.begin(); it != data_.end();) {
                if (std::abs(it->second) < tol) {
                    data_.erase(it++);
                    continue;
                }
                ++it;
            }
        }

        ///@brief values in range begin end needs to be sorted in ascending order
        template <class Iterator>
        inline void remove_rows(const Iterator &begin, const Iterator &end) {
            if (begin == end) return;

            assert(std::is_sorted(begin, end));

            Iterator rem_it = begin;
            for (MapIter it = data_.begin(); it != data_.end() && rem_it != end;) {
                const Integer r = it->first.first;
                const Integer row = *rem_it;

                if (row > r) {
                    ++it;
                    continue;
                }

                if (row < r) {
                    ++rem_it;
                    continue;
                }

                data_.erase(it++);
            }
        }

        bool pedantic_check(const Integer i, const Integer j) const;
        bool is_valid() const;

        inline EntryType &at(const Integer i, const Integer j) {
            assert(pedantic_check(i, j));
            assert(i < rows() || rows() == 0);
            assert(j < cols() || cols() == 0);
            assert(i >= 0);
            assert(j >= 0);

            return data_[IntPair(i, j)];
        }

        inline void set_identity() {
            Integer n = std::min(rows(), cols());
            clear();
            for (Integer i = 0; i < n; ++i) {
                push_entry(i, i, 1);
            }
        }

        inline void set_identity_at(const std::vector<bool> &row_selector) {
            for (MapIter it = data_.begin(); it != data_.end();) {
                const Integer row = it->first.first;
                if (row_selector[row]) {
                    const Integer col = it->first.second;
                    if (row == col) {
                        it->second = 1;
                    } else {
                        // it->second = 0;
                        data_.erase(it++);
                        continue;
                    }
                }

                ++it;
            }
        }

        inline void set_zero_rows_at(const std::vector<bool> &row_selector) {
            for (MapIter it = data_.begin(); it != data_.end();) {
                const Integer row = it->first.first;
                if (row_selector[row]) {
                    data_.erase(it++);
                    continue;
                }
                ++it;
            }
        }

        template <class SparseMatrix>
        SparseMatrix &operator+=(const SparseMatrix &expr) {
            for (auto it = expr.iter(); it; ++it) {
                const EntryType value = *it;
                // if(value != 0) {
                add(it.row(), it.col(), value);
                // }
            }

            return *this;
        }

        template <class Aggregator>
        SparseMatrix &aggregate(const SparseMatrix &expr, const Aggregator &aggr) {
            for (auto it = expr.iter(); it; ++it) {
                const EntryType value = *it;
                EntryType &val = at(it.row(), it.col());
                EntryType temp = val;
                aggr(temp, value);
                // if(temp != 0) {
                val = temp;
                // }
            }
            return *this;
        }

        SparseMatrix &operator+=(const SparseMatrix &expr) {
            for (auto it = expr.iter(); it; ++it) {
                const EntryType value = *it;
                // if(value != 0) {
                add(it.row(), it.col(), value);
                // }
            }
            return *this;
        }

        inline const Map &data() const { return data_; }

        inline Map &data() { return data_; }

        inline bool empty() const { return data_.empty(); }

        void write(OutputStream &os) const override {
            CHECK_STREAM_WRITE_BEGIN("SparseMatrix", os);

            os << Integer(data_.size());
            os << rows_;
            os << cols_;

            assert(rows() != 0 || data_.empty());
            assert(cols() != 0 || data_.empty());

            for (ConstMapIter vIt = data_.begin(); vIt != data_.end(); ++vIt) {
                const Integer row = vIt->first.first;
                const Integer col = vIt->first.second;
                os << row << col << vIt->second;
            }

            CHECK_STREAM_WRITE_END("SparseMatrix", os);
        }

        void read(InputStream &is) override {
            CHECK_STREAM_READ_BEGIN("SparseMatrix", is);

            Integer n_values;

            is >> n_values;
            is >> rows_;
            is >> cols_;

            assert(rows() != 0 || n_values == 0);
            assert(cols() != 0 || n_values == 0);

            Integer row, col;
            for (Integer v = 0; v < n_values; ++v) {
                is >> row >> col;
                is >> at(row, col);
            }

            CHECK_STREAM_READ_END("SparseMatrix", is);
        }

        SparseMatrix &operator*=(const EntryType &scale_factor) {
            for (MapIter vIt = data_.begin(); vIt != data_.end(); ++vIt) {
                vIt->second *= scale_factor;
            }

            return *this;
        }

        template <class Iterator, typename OtherEntryType>
        void copy_rows(const Iterator &begin, const Iterator &end, SparseMatrix<OtherEntryType> &result) const {
            if (begin == end) return;
            assert(std::is_sorted(begin, end));

            Iterator rem_it = begin;
            for (ConstMapIter it = data_.begin(); it != data_.end() && rem_it != end;) {
                const Integer r = it->first.first;
                const Integer row = *rem_it;

                if (row > r) {
                    ++it;
                    continue;
                }

                if (row < r) {
                    ++rem_it;
                    continue;
                }

                result.push_entry(row, it->first.second, it->second);
                ++it;
            }
        }

        inline SparseMatrix local_transposed() const {
            SparseMatrix mat(comm_, rows(), cols(), local_rows(), local_cols());

            for (auto it = iter(); it; ++it) {
                mat.push_entry(it.col(), it.row(), *it);
            }

            return mat;
        }

        virtual const std::string name() const { return "SparseMatrix"; }

        // inline SparseMatrix lumped() const
        // {
        //  SparseMatrix ret(rows(), cols());

        //  for(ConstMapIter it = data_.begin(); it != data_.end(); ++it) {
        //      ret.add(it->first.first, it->first.first, it->second);
        //  }

        //  return ret;
        // }

        inline std::vector<EntryType> sum_cols() const {
            std::vector<EntryType> ret(local_rows(), 0);

            if (data().empty()) return ret;

            auto offset = data().begin()->first.first;

            for (ConstMapIter it = data().begin(); it != data().end(); ++it) {
                ret[it->first.first - offset] += it->second;
            }

            return ret;
        }

        class ConstIter : public std::iterator<std::forward_iterator_tag, EntryType> {
        public:
            typedef _EntryType EntryType;

        private:
            typedef typename SparseMatrix::ConstMapIter Iter;

            const SparseMatrix &expr_;
            Iter it_;

        public:
            inline ConstIter(const SparseMatrix &expr) : expr_(expr), it_(expr.data().begin()) {}

            inline ConstIter(const ConstIter &other) : expr_(other.expr_), it_(other.it_) {}

            inline ConstIter(const SparseMatrix &expr, const Iter &begin) : expr_(expr), it_(begin) {}

            inline Integer index() const {
                using std::distance;
                return static_cast<Integer>(distance(expr_.data().begin(), it_));
            }

            friend Integer distance(const ConstIter &begin, const ConstIter &end) {
                using std::distance;
                return static_cast<Integer>(distance(begin.it_, end.it_));
            }

            inline operator bool() const { return it_ != expr_.data().end(); }

            inline const EntryType &operator*() const { return it_->second; }

            inline Integer row() const { return it_->first.first; }

            inline Integer col() const { return it_->first.second; }

            inline ConstIter next_row() const {
                Iter result = it_;
                while (result != expr_.data().end()) {
                    if (result->first.first != it_->first.first) {
                        break;
                    }

                    ++result;
                }
                return ConstIter(expr_, result);
            }

            inline void set(const ConstIter &other) {
                // must be the same object iter
                assert(&expr_ == &other.expr_);
                it_ = other.it_;
            }

            inline ConstIter &operator++() {
                ++it_;
                return *this;
            }

            inline bool operator!=(const ConstIter &other) const { return it_ != other.it_; }

            inline bool operator==(const ConstIter &other) const { return it_ == other.it_; }

            inline bool operator<(const ConstIter &other) const { return it_->first < other.it_->first; }
        };

        inline ConstIter iter() const { return ConstIter(*this); }

        Integer local_n_non_zero_rows() const {
            ConstIter it = iter();

            if (!it) return 0;

            Integer lastRow = it.row();
            Integer result = bool(it);

            for (; it; ++it) {
                const Integer row = it.row();
                if (row != lastRow) {
                    lastRow = row;
                    ++result;
                }
            }

            return result;
        }

        // synched
        inline Integer n_non_zero_rows() const {
            Integer res = local_n_non_zero_rows();
            comm_.all_reduce(&res, 1, MPISum());
            return res;
        }

        Integer local_max_entries_x_col() const {
            ConstIter it = iter();

            if (!it) return 0;

            Integer lastRow = it.row();
            Integer result = 0;

            Integer current_n_cols = 0;
            for (; it; ++it) {
                const Integer row = it.row();
                if (row != lastRow) {
                    lastRow = row;
                    result = std::max(current_n_cols, result);
                    current_n_cols = 0;
                }

                ++current_n_cols;
            }

            return result;
        }

        // synched
        inline Integer max_entries_x_col() const {
            Integer res = local_max_entries_x_col();
            comm_.all_reduce(&res, 1, MPIMax());
            return res;
        }

        template <typename AssociativeOperator>
        inline const EntryType local_reduce(AssociativeOperator op) const {
            return local_reduce_aux(op, 0);
        }

        template <typename AssociativeOperator>
        inline EntryType local_reduce_aux(AssociativeOperator op, const EntryType initial_value) const {
            EntryType result = initial_value;

            for (auto it = iter(); it; ++it) {
                result = op(result, *it);
            }

            return result;
        }

        // inline EntryType local_max() const {
        //     return this->template local_reduce(
        //         [](const EntryType left, const EntryType right) -> EntryType { return std::max(left, right); });
        // }

        // inline EntryType local_min() const {
        //     return this->template local_reduce(
        //         [](const EntryType left, const EntryType right) -> EntryType { return std::min(left, right); });
        // }

        // inline EntryType local_abs_max() const {
        //     return this->template local_reduce([](const EntryType left, const EntryType right) -> EntryType {
        //         return std::max(std::abs(left), std::abs(right));
        //     });
        // }

        // inline EntryType local_abs_min() const {
        //     return this->template local_reduce([](const EntryType left, const EntryType right) -> EntryType {
        //         return std::min(std::abs(left), std::abs(right));
        //     });
        // }

        inline EntryType local_abs_sum() const {
            EntryType result = 0.0;
            for (auto it = iter(); it; ++it) {
                result += std::abs(*it);
            }

            return result;
        }

        inline EntryType local_sum() const {
            // Visual Studio does not like this for some reason
            // return this->template local_reduce(
            //     [](const EntryType left, const EntryType right) -> EntryType { return left + right; });

            EntryType result = 0.0;
            for (auto it = iter(); it; ++it) {
                result += *it;
            }

            return result;
        }

        // synched
        inline EntryType sum() const {
            EntryType res = local_sum();
            comm_.all_reduce(&res, 1, MPISum());
            return res;
        }

        void describe(std::ostream &os) const override {
            for (auto it = iter(); it; ++it) {
                os << it.row() << ", " << it.col() << "\t-> " << *it << "\n";
            }
        }

        void synch_describe(std::ostream &os) const {
            Communicator comm_copy(comm_);
            SynchedDescribable::synch_describe(comm_copy, os);
        }

        template <class VectorIn, class VectorOut>
        void serial_multiply(const VectorIn &in, VectorOut &out) const {
            // assert(comm_.size() == 1 && "only works in serial");
            assert(rows_ > 0 && "must have a structure");

            out.resize(rows_);
            for (auto &v : out) {
                v = 0.0;
            }

            for (auto it = iter(); it; ++it) {
                out[it.row()] += (*it) * in[it.col()];
            }
        }

        template <typename F>
        inline void transform(F f) {
            for (MapIter vIt = data_.begin(); vIt != data_.end(); ++vIt) {
                vIt->second = f(vIt->second);
            }
        }
    };
}  // namespace moonolith
#endif  // MOONOLITH_MAP_SPARSE_MATRIX_HPP