#ifndef MOONOLITH_PROJECT_POLY_LINES_HPP
#define MOONOLITH_PROJECT_POLY_LINES_HPP

#include "moonolith_empirical_tol.hpp"
#include "moonolith_householder.hpp"
#include "moonolith_line.hpp"
#include "moonolith_map_quadrature.hpp"
#include "moonolith_plane.hpp"
#include "moonolith_project_points.hpp"
#include "moonolith_simplex_transform.hpp"

#include <cmath>

namespace moonolith {

    template <typename T>
    class ProjectPolyLines {
    public:
        using Line2 = moonolith::Line<T, 2>;
        using Point2 = moonolith::Vector<T, 2>;
        using Plane2 = moonolith::Plane<T, 2>;
        using PolyLine = moonolith::PolyLine<T, 2>;

        using Point1 = moonolith::Vector<T, 1>;

        bool project_intersect_and_map_quadrature(const PolyLine &poly_line_1,
                                                  const PolyLine &poly_line_2,
                                                  const Plane2 &plane,
                                                  // ref-quad-rule
                                                  const Quadrature1<T> &q_ref,
                                                  Quadrature2<T> &q_plane,
                                                  const T tol = GeometricTol<T>::value()) {
            return project_intersect_and_map_quadrature(
                poly_line_1.points, poly_line_2.points, plane, q_ref, q_plane, tol);
        }

        bool project_intersect_and_map_quadrature(const Storage<Point2> &poly_line_1,
                                                  const Storage<Point2> &poly_line_2,
                                                  const Plane2 &plane,
                                                  // ref-quad-rule
                                                  const Quadrature1<T> &q_ref,
                                                  Quadrature2<T> &q_plane,
                                                  const T tol = GeometricTol<T>::value()) {
            if (!apply(poly_line_1, poly_line_2, plane, line_buff, tol)) {
                return false;
            }

            map(q_ref,
                static_cast<T>(1.),  // left unscaled
                line_buff,
                q_plane);

            return true;
        }

        bool apply(const Storage<Point2> &poly_line_1,
                   const Storage<Point2> &poly_line_2,
                   const Plane2 &plane,
                   Line2 &result,
                   const T tol = GeometricTol<T>::value()) {
            v = plane.n;
            v.y += 1.;

            if (std::abs(v.y) < 1e-10) {
                householder_trafo.flip(1);
            } else {
                v /= length(v);
                householder_trafo.init(v);
            }

            project_points(householder_trafo, plane.p, poly_line_1, ref_poly_1);

            project_points(householder_trafo, plane.p, poly_line_2, ref_poly_2);

            T min_1 = ref_poly_1[0].x;
            T max_1 = min_1;
            T min_2 = ref_poly_2[0].x;
            T max_2 = min_2;

            auto n_points_1 = ref_poly_1.size();
            for (std::size_t i = 1; i < n_points_1; ++i) {
                min_1 = std::min(min_1, ref_poly_1[i].x);
                max_1 = std::max(max_1, ref_poly_1[i].x);
            }

            auto n_points_2 = ref_poly_2.size();
            for (std::size_t i = 1; i < n_points_2; ++i) {
                min_2 = std::min(min_2, ref_poly_2[i].x);
                max_2 = std::max(max_2, ref_poly_2[i].x);
            }

            if (min_1 + tol >= max_2 || min_2 + tol >= max_1) return false;
            if (std::abs(max_1 - min_1) < tol) return false;
            if (std::abs(max_2 - min_2) < tol) return false;

            ref_isect.resize(2);
            ref_isect[0].x = std::max(min_1, min_2);
            ref_isect[1].x = std::min(max_1, max_2);

            auto len = ref_isect[1].x - ref_isect[0].x;

            intersection_ratio_1 = len / (max_1 - min_1);
            intersection_ratio_2 = len / (max_2 - min_2);

            assert(!std::isnan(intersection_ratio_1));
            assert(!std::isnan(intersection_ratio_2));

            unproject_points(householder_trafo, plane.p, ref_isect, isect);

            result.p0 = isect[0];
            result.p1 = isect[1];
            return true;
        }

        T intersection_ratio_1;
        T intersection_ratio_2;

    private:
        Vector<T, 2> v;
        HouseholderTransformation<T, 2> householder_trafo;
        Storage<Point1> ref_poly_1, ref_poly_2;
        Storage<Point1> ref_isect;
        Storage<Point2> isect;
        Line2 line_buff;
    };

}  // namespace moonolith

#endif  // MOONOLITH_PROJECT_POLY_LINES_HPP
