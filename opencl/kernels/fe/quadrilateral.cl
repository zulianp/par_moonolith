#ifndef QUADRILATERAL_CL
#define QUADRILATERAL_CL 

#include "kernel_base.cl"
#include "fe_object.cl"

#include "fe_interfaces_2.cl"
#include "transform.cl"

#define F_MINUS(x) (1.0-x)
#define F_PLUS(x)  (x)
#define D_MINUS(x) (-1.0)
#define D_PLUS(x) (1.0)

// #define AFFINE_QUAD

void quad_make_fe_object(const Scalar *points, const SizeType n_quad_points, const Scalar * quad_points, FEObject *object)
{
	object->n_dims = 2;
	object->n_quad_points = n_quad_points;
	object->reference_volume = 1;
	object->n_shape_functions = 4;

	static_assert(MAX_SHAPE_FUNCS >= 4, "check kernel_base for MAX_SHAPE_FUNCS");
	
	quad_bilinear_fun_0(n_quad_points, quad_points, object->fun[0]);
	quad_bilinear_fun_1(n_quad_points, quad_points, object->fun[1]);
	quad_bilinear_fun_2(n_quad_points, quad_points, object->fun[2]);
	quad_bilinear_fun_3(n_quad_points, quad_points, object->fun[3]);


	quad_bilinear_grad_0(n_quad_points, quad_points, object->grad[0]);
	quad_bilinear_grad_1(n_quad_points, quad_points, object->grad[1]);
	quad_bilinear_grad_2(n_quad_points, quad_points, object->grad[2]);
	quad_bilinear_grad_3(n_quad_points, quad_points, object->grad[3]);

#ifndef AFFINE_QUAD
	quad_iso_parametric_transform(object, points, object->point);	
	quad_iso_parametric_jacobian (object, points, object->jacobian);	
#else
	quad_affine_transform(points, n_quad_points, quad_points, object->point);	
	quad_affine_jacobian(points, n_quad_points, quad_points,  object->jacobian);	
#endif //AFFINE_QUAD
}

void quad_affine_transform(const Scalar * points, const SizeType n_quad_points, const Scalar * quad_points, Scalar * global_quad_points)
{
	triangle_transform(points, n_quad_points, quad_points, global_quad_points);
}

void quad_iso_parametric_transform(const FEObject *fe, const Scalar * points, Scalar * global_quad_points)
{
	iso_parametric_transform_2(fe, points, global_quad_points);
}

//layout of point data [x1, y1, x2, y2, z1, z2, ...]
//layout of the jacobian data [ f1_x1, f2_x1, f1_x2, f2_x2, ... ]
//@param n_quad_points  should be 1
void quad_affine_jacobian(const Scalar * points, const SizeType n_quad_points, const Scalar * quad_points, Scalar * jacobians)
{
	triangle_jacobian(points, n_quad_points, quad_points, jacobians);
}

void quad_iso_parametric_jacobian(const FEObject *fe, const Scalar * points, Scalar * jacobians)
{
	iso_parametric_jacobian_2(fe, points, jacobians);
}

void quad_transform(const Scalar * points, const SizeType n_quad_points, const Scalar * quad_points, Scalar * global_quad_points)
{	
	Scalar f0, f1, f2, f3;
    const Vector2 p0 = vec_2(points[0], points[1]);
    const Vector2 p1 = vec_2(points[2], points[3]);
    const Vector2 p2 = vec_2(points[4], points[5]);
    const Vector2 p3 = vec_2(points[6], points[7]);

    for(SizeType q = 0; q < n_quad_points; ++q) {   
        const SizeType q2 	= q*2;
        const SizeType q2p1 = q2+1;

        quad_bilinear_fun_0(1, &quad_points[q2], &f0);
        quad_bilinear_fun_1(1, &quad_points[q2], &f1);
        quad_bilinear_fun_2(1, &quad_points[q2], &f2);
        quad_bilinear_fun_3(1, &quad_points[q2], &f3);

        Vector2 global_point = f0 * p0;
        global_point        += f1 * p1;
        global_point        += f2 * p2;
        global_point        += f3 * p3;

        global_quad_points[q2]   = global_point[0];
        global_quad_points[q2p1] = global_point[1];
    }
}

void quad_transform_jacobian(const Scalar * points, const SizeType n_quad_points, const Scalar * quad_points, Scalar * jacobians)
{
    Scalar outer_product_buff[2*2];
    Scalar grad 			 [2];

    const SizeType n_quad_points_x_d = n_quad_points*4;
   
    set(n_quad_points_x_d, 0, jacobians);
    
    for(SizeType q = 0; q < n_quad_points; ++q) {
    	const SizeType q2 	 = q * 2;
        const SizeType q_jac = q * 4;

        const Scalar *quad_point = &quad_points[q2];

        quad_bilinear_grad_0(1, quad_point, grad);
        outer_product(2, 2, &points[0], grad, outer_product_buff);    
        axpy(4, 1.0, outer_product_buff, &jacobians[q_jac]);


        quad_bilinear_grad_1(1, quad_point, grad);
        outer_product(2, 2, &points[2], grad, outer_product_buff);    
        axpy(4, 1.0, outer_product_buff, &jacobians[q_jac]);

        quad_bilinear_grad_2(1, quad_point, grad);
        outer_product(2, 2, &points[4], grad, outer_product_buff);    
        axpy(4, 1.0, outer_product_buff, &jacobians[q_jac]);

        quad_bilinear_grad_3(1, quad_point, grad);
        outer_product(2, 2, &points[6], grad, outer_product_buff);    
        axpy(4, 1.0, outer_product_buff, &jacobians[q_jac]);
    }
}

void quad_inverse_transform(const Scalar * points, const SizeType n_quad_points, const Scalar * global_quad_points, Scalar * quad_points)
{
    const Scalar triangle[3*2] = { 
        points[0], points[1],
        points[2], points[3],
        points[6], points[7]
    };

    triangle_inverse_transform(triangle, n_quad_points, global_quad_points, quad_points);

#ifndef AFFINE_QUAD      
    //newton iteration to deal with non affine quads
    const SizeType max_newton_steps = 10;
    
    Scalar increment[2];
    Scalar gradient [2];
    Scalar hessian  [2*2];
    
    for(SizeType q = 0; q < n_quad_points; ++q) {
        const SizeType q2 = q * 2;
       
        Scalar *quad_point = &quad_points[q2]; 
        const Scalar *global_point = &global_quad_points[q2];
        
        for(SizeType i = 0; i < max_newton_steps; ++i) {
            quad_transform(points, 1, quad_point, gradient);

            gradient[0] -= global_point[0];
            gradient[1] -= global_point[1];

            quad_transform_jacobian(points, 1, quad_point, hessian);
            solve_2x2(hessian, gradient, increment);

            quad_point[0] -= increment[0];
            quad_point[1] -= increment[1];

            if(norm_n(2, increment) < DEFAULT_TOLLERANCE) {
                break;
            }
        }
    }
 #endif //AFFINE_QUAD  

}

void quad_bilinear_fun_0(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		const SizeType q2 = q*2;
		funs[q] = F_MINUS(quad_points[q2]) * F_MINUS(quad_points[q2+1]);
	}
}

void quad_bilinear_grad_0(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads)
{
	const SizeType n_quad_points_x_d = n_quad_points*2;
	for(SizeType q = 0; q < n_quad_points_x_d; q+=2) {	
		const Scalar x = quad_points[q];
		const Scalar y = quad_points[q+1];

		grads[q]   = D_MINUS(x) * F_MINUS(y);
		grads[q+1] = F_MINUS(x) * D_MINUS(y);
	}
}

void quad_bilinear_fun_1(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		const SizeType q2 = q*2;
		funs[q] = F_PLUS(quad_points[q2]) * F_MINUS(quad_points[q2+1]);
	}
}

void quad_bilinear_grad_1(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads)
{
	const SizeType n_quad_points_x_d = n_quad_points*2;
	for(SizeType q = 0; q < n_quad_points_x_d; q+=2) {	
		const Scalar x = quad_points[q];
		const Scalar y = quad_points[q+1];

		grads[q]   = D_PLUS(x) * F_MINUS(y);
		grads[q+1] = F_PLUS(x) * D_MINUS(y);
	}

}

void quad_bilinear_fun_2(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		const SizeType q2 = q*2;
		funs[q] = F_PLUS(quad_points[q2]) * F_PLUS(quad_points[q2+1]);
	}
}

void quad_bilinear_grad_2(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads)
{
	const SizeType n_quad_points_x_d = n_quad_points*2;
	for(SizeType q = 0; q < n_quad_points_x_d; q+=2) {	
		const Scalar x = quad_points[q];
		const Scalar y = quad_points[q+1];

		grads[q]   = D_PLUS(x) * F_PLUS(y);
		grads[q+1] = F_PLUS(x) * D_PLUS(y);
	}
}

void quad_bilinear_fun_3(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		const SizeType q2 = q*2;
		funs[q] = F_MINUS(quad_points[q2]) * F_PLUS(quad_points[q2+1]);
	}
}

void quad_bilinear_grad_3(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads)
{
	const SizeType n_quad_points_x_d = n_quad_points*2;
	for(SizeType q = 0; q < n_quad_points_x_d; q+=2) {	
		const Scalar x = quad_points[q];
		const Scalar y = quad_points[q+1];

		grads[q]   = D_MINUS(x) * F_PLUS(y);
		grads[q+1] = F_MINUS(x) * D_PLUS(y);
	}
}

//clean macros
#undef F_MINUS
#undef F_PLUS
#undef D_MINUS
#undef D_PLUS

#endif //QUADRILATERAL_CL

