#ifndef MOONOLITH_INTERSECT_RAY_WITH_POLYGON_HPP
#define MOONOLITH_INTERSECT_RAY_WITH_POLYGON_HPP

#include "moonolith_plane.hpp"
#include "moonolith_polygon.hpp"
#include "moonolith_ray.hpp"

#include "moonolith_geo_algebra.hpp"

namespace moonolith {

    template <typename Real, int Dim>
    class IntersectRayWithPoly {};

    template <typename Real>
    class IntersectRayWithPolygon {
    public:
        enum Culling { BACK_FACE_CULLING = 0, FRONT_FACE_CULLING = 1, NONE = 2 };

        using Point = moonolith::Vector<Real, 3>;

        bool apply(const Ray<Real, 3> &ray, const Polygon<Real, 3> &poly, Real &t, Point &p) {
            assert(length(ray.dir) != 0.);

            u = poly[1] - poly[0];
            v = poly[2] - poly[0];

            u /= length(u);
            v /= length(v);

            plane.p = poly[0];
            plane.n = cross(u, v);
            plane.n /= length(plane.n);

            if (BACK_FACE_CULLING == culling_) {
                if (dot(plane.n, ray.dir) >= 0.0) return false;
            } else if (FRONT_FACE_CULLING == culling_) {
                if (dot(plane.n, ray.dir) <= 0.0) return false;
            }

            if (!plane.intersect(ray, t)) {
                return t;
            }

            p = ray.o + t * ray.dir;

            return contains(poly, plane.n, p);
        }

        bool contains(const Polygon<Real, 3> &poly, const Point &normal, const Point &p) {
            const Integer n = poly.size();
            for (Integer i = 0; i < n; ++i) {
                Integer ip1 = (i + 1 == n) ? 0 : i + 1;
                u = poly[i] - p;
                v = poly[ip1] - p;

                if (dot(cross(u, v), normal) < -tol_) {
                    return false;
                }
            }

            return true;
        }

        void set_culling(const Culling &opt) { culling_ = opt; }
        void set_tol(const Real tol) { tol_ = tol; }

    private:
        Plane<Real, 3> plane;
        Point u, v;

        Culling culling_{BACK_FACE_CULLING};
        Real tol_{GeometricTol<Real>::value()};
    };

    template <typename Real>
    class IntersectRayWithPolyline {
    public:
        enum Culling { BACK_FACE_CULLING = 0, FRONT_FACE_CULLING = 1, NONE = 2 };
        using Point = moonolith::Vector<Real, 2>;

        bool apply(const Ray<Real, 2> &ray, const Storage<Point> &poly, Real &t, Point &p) {
            assert(int(poly.size()) == 2 && "IMPLEMENT ME polyline");

            assert(length(ray.dir) != 0.);

            plane.p = poly[1] - poly[0];
            Real len_segment = length(plane.p);

            perp(plane.p, plane.n);
            plane.n /= -length(plane.n);
            plane.p = poly[0];

            if (BACK_FACE_CULLING == culling_) {
                if (dot(plane.n, ray.dir) >= 0.0) return false;
            } else if (FRONT_FACE_CULLING == culling_) {
                if (dot(plane.n, ray.dir) <= 0.0) return false;
            }

            if (!plane.intersect(ray, t)) {
                return false;
            }

            p = ray.o + t * ray.dir;

            return contains(poly[0], poly[1], p);
        }

        void set_culling(const Culling &opt) { culling_ = opt; }
        void set_tol(const Real tol) { tol_ = tol; }

    private:
        Plane<Real, 2> plane;
        Culling culling_{BACK_FACE_CULLING};
        Real tol_{GeometricTol<Real>::value()};

        bool contains(const Point &p1, const Point &p2, const Point &p) {
            Point u = p2 - p1;
            Real len_u = length(u);
            u /= len_u;

            Point v = p - p1;
            Real projection = dot(v, u);
            return (projection > -tol_) && (projection < len_u + tol_);
        }
    };

    template <typename Real>
    class IntersectRayWithPoly<Real, 3> : public IntersectRayWithPolygon<Real> {
    public:
        using Poly = moonolith::Polygon<Real, 3>;
    };

    template <typename Real>
    class IntersectRayWithPoly<Real, 2> : public IntersectRayWithPolyline<Real> {
    public:
        using Point = moonolith::Vector<Real, 2>;
        using Poly = moonolith::Storage<Point>;
    };

}  // namespace moonolith

#endif  // MOONOLITH_INTERSECT_RAY_WITH_POLYGON_HPP
