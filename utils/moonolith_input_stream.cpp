#include "moonolith_input_stream.hpp"

namespace moonolith {

	ByteInputBuffer::ByteInputBuffer()
		: super(), storage_()
	{
		super::set_storage(&storage_);
	}

	ByteInputBuffer::ByteInputBuffer(const Integer size)
		: super(), storage_(size)
	{
		super::set_storage(&storage_);
	}

	void ByteInputBuffer::resize(const Integer new_size) 
	{
		storage_.resize(new_size);
		assert(Integer(storage_.size()) == new_size);
		assert(Integer(storage_.size()) == size());
	}

	Integer ByteInputBuffer::size() const 
	{
		return super::allocated_size();
	}

	moonolith::byte &ByteInputBuffer::operator[](const Integer index)
	{
		return storage_[index];
	}

	const moonolith::byte &ByteInputBuffer::operator[](const Integer index) const
	{
		return storage_[index];
	}

	ByteInputBuffer & ByteInputBuffer::operator=(const ByteInputBuffer &other)
	{
		super::operator=(other);
		storage_ = other.storage_;
		super::set_storage(&storage_);
		return *this;
	}

	ByteInputBuffer::ByteInputBuffer(const ByteInputBuffer &other)
	: super(other), storage_(other.storage_)
	{
		super::set_storage(&storage_);
	}

} /* moonolith */
