#ifndef MOONOLITH_INTERSECT_PLANE_HPP
#define MOONOLITH_INTERSECT_PLANE_HPP

#include "moonolith_plane.hpp"
#include "moonolith_line.hpp"

namespace moonolith {

	template<typename T, int Dim>
	class IntersectLineWithPlane {
	public:
		using Plane = moonolith::Plane<T, Dim>;
		using Line  = moonolith::Line<T, Dim>;
		using Vector = typename Plane::Vector;

		static const short INSIDE   = 1;
		static const short OUTSIDE  = -1;
		static const short COPLANAR = 0;

		short apply(const Line &line, const Plane &plane, T &t, const T tol = 1e-10);

	private:
		Vector ray_dir;
	};

}

#endif //MOONOLITH_INTERSECT_PLANE_HPP
