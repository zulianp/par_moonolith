# Support

For reporting bugs, requesting features, usage examples, ..., please use the bitbucket issue system at [https://bitbucket.org/zulianp/par_moonolith/issues](https://bitbucket.org/zulianp/par_moonolith/issues)


# Where to report bugs

Create a new [ISSUE](https://bitbucket.org/zulianp/par_moonolith/issues). 

# Required information #

1. Explain how the bug was produced.
2. Which git branch and version of utopia have been used.
3. If the bug was produced by launching the executable on a super-computing cluster with SLURM (or equivalent) also provide the SLURM script or command.

# Build system bugs #

1. Provide the complete sequence of commands that generated the bugs (e.g. CMake .., Make)

# Runtime bugs #
The more information that you convey about a bug, the easier it will be for us to target the problem. We suggest providing the following information:

1. Stack trace of at the point of failure (with `gdb` you can generate it from a core file that is generate when a crash happens.)
2. Number of processes used
3. Machine used (if laptop also system and version)
4. Indicate whether you are using a particular available installation on a super-computer (specific path of installation)


## Stack trace with gdb using core files##

A core file typically has a format such as `core.[0-9]+` (for instance `core.1234`)
```
#!bash

gdb <exectuable_path> <core_file_path>
```
For instance

```
#!bash

gdb ./my_exe core.4321
```


Inside the gdb environment type


```
#!bash

where
```