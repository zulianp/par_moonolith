#ifndef MOONOLITH_COLLECTOR_HPP
#define MOONOLITH_COLLECTOR_HPP

// #include "moonolith_traversal_listener.hpp"
// #include "moonolith_breadth_first_navigator.hpp"
#include "moonolith_data_handle.hpp"
#include "moonolith_forward_declarations.hpp"

#include <assert.h>

namespace moonolith {

    template <class Tree>
    class Collector : public TraversalListener<Tree>, public Visitor<typename Tree::Traits>, public Describable {
    public:
        typedef typename Tree::Traits Traits;
        typedef typename Tree::Node Node;
        typedef typename Tree::Leaf Leaf;
        typedef typename Tree::Branch Branch;
        typedef typename Tree::DataType DataType;

        typedef moonolith::TreeMemory<Traits> TreeMemory;
        typedef moonolith::RemoteNode<Node> RemoteNode;

        virtual ~Collector() {}

        virtual void entry_added(Tree &tree, Node &node, const RemoteNode &remote_node, const Integer remote_rank) {
            assert(remote_rank >= 0);

            remote_node_ = &remote_node;
            remote_rank_ = remote_rank;

            if (lookup_table_->data_2_proc().empty() ||
                Integer(lookup_table_->data_2_proc().size()) != tree.memory().n_data()) {
                // lazy initialization
                lookup_table_->data_2_proc().resize(tree.memory().n_data());
            }

            BreadthFirstNavigator<Tree> nav;
            nav.walk(node, *this, tree.memory());

            remote_node_ = NULL;
            remote_rank_ = -1;
        }

        virtual void traversal_terminated() {}

        inline Integer remote_rank() const {
            assert(remote_rank_ >= 0);
            return remote_rank_;
        }

        void collect_data(Node &node, TreeMemory &memory) {
            memory.each(node.handle(), [this](const DataHandle &handle, DataType &data) {
                if (remote_node_->match(data)) {
                    this->lookup_table_->data_2_proc()[handle].insert(this->remote_rank());
                }
            });
        }

        NavigatorOption visit(Leaf &leaf, TreeMemory &memory) {
            if (leaf.empty()) {
                return CONTINUE;
            }

            if (!remote_node_->match(leaf)) {
                return CONTINUE;
            }

            collect_data(leaf, memory);

            return CONTINUE;
        }

        NavigatorOption visit(Branch &branch, TreeMemory &memory) {
            if (!remote_node_->match(branch)) {
                return SKIP_SUBTREE;
            }

            collect_data(branch, memory);
            return CONTINUE;
        }

        Collector() : remote_node_(NULL), remote_rank_(-1) {}

        void set_remote_rank(const Integer rank) { remote_node_ = rank; }

        void describe(std::ostream &os) const { os << "Collecting for rank " << remote_rank_ << "\n"; }

        void set_lookup_table(const std::shared_ptr<LookUpTable> &lookup_table) { lookup_table_ = lookup_table; }

    private:
        const RemoteNode *remote_node_;
        Integer remote_rank_;
        std::shared_ptr<LookUpTable> lookup_table_;
    };
}  // namespace moonolith

#endif  // MOONOLITH_COLLECTOR_HPP
