#include "par_moonolith.hpp"

#include "moonolith_hash_grid.hpp"
#include "moonolith_serial_hash_grid.hpp"

#include "gtest/gtest.h"

using namespace moonolith;

TEST(SerialHashGridTest, Basic) {
    SerialHashGrid<3>::Box box;
    box.set_min(0, 1);
    box.set_min(1, 1);
    box.set_min(2, 1);
    box.set_max(0, 2);
    box.set_max(1, 2);
    box.set_max(2, 2);

    SerialHashGrid<3> hash_grid;

    std::vector<Integer> pairs;
    ASSERT_TRUE(hash_grid.detect({box}, {box}, pairs));
    ASSERT_TRUE(pairs.size() == 2);
}

TEST(UniformHashGrid, Basic) { UniformHashGrid<2, Real> hash_grid; }
