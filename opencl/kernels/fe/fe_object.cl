#ifndef FE_OBJECT_CL
#define FE_OBJECT_CL 

#include "kernel_base.cl"

typedef struct {
	//Meta
	SizeType n_dims;
	SizeType n_quad_points;
	SizeType n_shape_functions;

	//Geometry
	Scalar reference_volume;

	Scalar jacobian[MAX_QUAD_POINTS * MAX_N_DIMS * MAX_N_DIMS];
	Scalar jacobian_determinant[MAX_QUAD_POINTS];													///NOT ALWAYS AVAILABLE
	Scalar inverse_jacobian[MAX_QUAD_POINTS * MAX_N_DIMS * MAX_N_DIMS];								///NOT ALWAYS AVAILABLE
	
	Scalar dx[MAX_QUAD_POINTS];
	Scalar point[MAX_QUAD_POINTS * MAX_N_DIMS];

	//Basis functions
	Scalar fun[MAX_SHAPE_FUNCS][MAX_QUAD_POINTS];
	Scalar grad[MAX_SHAPE_FUNCS][MAX_QUAD_POINTS * MAX_N_DIMS];
} FEObject;

#endif //FE_OBJECT_CL

