#include "mortar_interfaces.cl"

#include "intersect.cl"
#include "intersect_3.cl"
#include "mortar.cl"
#include "mortar_assemble_for_multidomain_mesh.cl"
#include "make_quadrature_from_poly.cl"
