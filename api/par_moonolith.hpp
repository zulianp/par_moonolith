#ifndef PAR_MOONOLITH_HPP
#define PAR_MOONOLITH_HPP

#include "par_moonolith_instance.hpp"

/*! @file par_moonolith.hpp
 *  Include this file and only this file for using par_moonolith.
 */

/*! @mainpage
 * \tableofcontents
 *
 *
 * @section intro A quick introduction to Moonolith
 *
 * ParMoonolith is a library that allows for parallel intersection and proximity detection, and automatic
 * load-balancing. The library is designed to allow deep integration with discretization codes such as MFEM and libMesh.
 *
 * @section api API
 * The main entry point for moonolith is the
 * moonolith::search_and_compute()
 *
 * Here is an example usage of such function using example trees defined in mooonolith_test_trees.hpp.
 *
 * @code
 *        typedef typename TreeT::DataType DataT;
 *        typedef typename TreeT::DataContainer DataContainer;
 *
 *        Communicator world;
 *        world.barrier();
 *
 *        std::shared_ptr<TreeT> tree = build_test_tree<TreeT, DIM>(50);
 *        std::shared_ptr<Predicate> predicate = std::make_shared<Predicate>();
 *
 *        SearchSettings ss;
 *        ss.verbosity_level = 2;
 *
 *        Integer n_isect = 0;
 *        Integer n_false_positives = 0;
 *        search_and_compute(world, tree, predicate,
 *          [](const Integer owner_rank,
 *             const Integer sender_rank,
 *             const bool is_forwarding,
 *             DataContainer &remote_data_for_rank,
 *             InputStream &is) {
 *
 *              (void) owner_rank;
 *              (void) sender_rank;
 *              (void) is_forwarding;
 *
 *              Integer n;
 *              is >> n;
 *
 *              const Integer offset = remote_data_for_rank.size();
 *              remote_data_for_rank.resize(offset + n);
 *
 *              for(Integer i = 0; i < n; ++i) {
 *                  auto &d = remote_data_for_rank[offset + i];
 *                  is >> d;
 *
 *                  //for simplifying the writing i change the handle
 *                  //by adding the offset such that the handle is unique
 *                  //and aligned with the data position in the container
 *                  d.set_handle(offset + i);
 *              }
 *          },
 *          [](const Integer owner_rank,
 *             const Integer recv_rank,
 *             const std::vector<Integer>::const_iterator &begin,
 *             const std::vector<Integer>::const_iterator &end,
 *             const DataContainer &data_container,
 *             OutputStream &os) {
 *
 *              (void) owner_rank;
 *              (void) recv_rank;
 *
 *              const Integer n = std::distance(begin, end);
 *              os << n;
 *              for(auto it = begin; it != end; ++it) {
 *                  os << data_container[*   it];
 *              }
 *          },
 *          [&n_isect, &n_false_positives](const DataT &left, const DataT &right) {
 *              if(left.bound().intersects(right.bound())) {
 *                  ++n_isect;
 *                  return true;
 *              } else {
 *                  ++n_false_positives;
 *                  return false;
 *              }
 *          },
 *          ss
 *          );
 *
 *        logger() << world << " n_isect: " << n_isect << ", n_false_positives: " << n_false_positives << std::endl;
 * @endcode
 */

#include "moonolith_asynch_lookup_table_builder.hpp"
#include "moonolith_communicator.hpp"
#include "moonolith_load_balancer.hpp"
#include "moonolith_lookup_table.hpp"
#include "moonolith_node_and_proc_collector.hpp"
#include "moonolith_predicate.hpp"
#include "moonolith_profiler.hpp"
#include "moonolith_synch_lookup_table_builder.hpp"
#include "moonolith_work_table.hpp"
#include "moonolith_workspace.hpp"
#include "par_moonolith_config.hpp"

#include <assert.h>
#include <memory>

/**     @defgroup   api API
 */

namespace moonolith {

    class SearchSettings {
    public:
        bool use_synch;
        bool enable_stats;
        int verbosity_level;
        bool disable_data_lookup;
        bool use_linear_cost;
        bool disable_redistribution;
        int max_depth;
        int max_elements;

        SearchSettings()
            : use_synch(false),
              enable_stats(false),
              verbosity_level(0),
              disable_data_lookup(false),
              use_linear_cost(false),
              disable_redistribution(false),
              max_depth(DEFAULT_REFINE_DEPTH),
              max_elements(DEFAULT_REFINE_MAX_ELEMENTS) {}
    };

    template <class NTreeT>
    std::shared_ptr<LookUpTable> make_lookup_table(Communicator &comm,
                                                   const std::shared_ptr<NTreeT> &tree,
                                                   const SearchSettings &settings = SearchSettings()) {
        const bool use_synch = settings.use_synch;
        const bool enable_stats = settings.enable_stats;
        const int verbosity_level = settings.verbosity_level;
        const bool disable_data_lookup = settings.disable_data_lookup;

        // Clock clock;

        std::shared_ptr<LookUpTableBuilder<NTreeT> > builder;

        if (use_synch) {
            builder = std::make_shared<SynchLookUpTableBuilder<NTreeT> >();
        } else {
            builder = std::make_shared<AsynchLookUpTableBuilder<NTreeT> >();
        }

        if (verbosity_level >= 3) {
            builder->set_verbose(true);
        }

        if (disable_data_lookup) {
            builder->set_must_build_data_lookup(false);
        }

        MOONOLITH_EVENT_BEGIN("middle_phase_detection");
        ////////////////////////////////////////////////////////////////////////////////
        std::shared_ptr<LookUpTable> table = std::make_shared<LookUpTable>();
        bool ok = builder->build_for(comm, *tree, table);
        ////////////////////////////////////////////////////////////////////////////////
        MOONOLITH_EVENT_END("middle_phase_detection");

        if (!ok && verbosity_level >= 1) {
            if (comm.is_root()) std::cerr << "[Status] make_lookup_table: NOTHING TO BUILD " << std::endl;
        }

        /// asserting correctness
        assert(table->check_consistency(comm));

        // if (verbosity_level >= 1) {
        //     comm.barrier();
        //     clock.tick();
        //     if (comm.is_root()) logger() << "[Status] make_lookup_table time:\n" << clock << std::endl;
        // }

        if (enable_stats) {
            table->compute_statistics(comm);
            table->synch_describe_stats(comm, logger());
            tree->memory().compute_statistics();
            tree->memory().synch_describe(comm, logger());
        }

        return table;
    }

    // FIXME still creating look up table
    template <class NTreeT>
    std::shared_ptr<WorkTable> make_work_table(Communicator &comm,
                                               const std::shared_ptr<NTreeT> &tree,
                                               const std::shared_ptr<Predicate> &predicate,
                                               const SearchSettings &settings = SearchSettings()) {
        const bool use_synch = settings.use_synch;
        const bool enable_stats = settings.enable_stats;
        const int verbosity_level = settings.verbosity_level;
        const bool use_linear_cost = settings.use_linear_cost;

        // Clock clock;

        std::shared_ptr<LookUpTableBuilder<NTreeT> > builder;
        if (use_synch) {
            builder = std::make_shared<SynchLookUpTableBuilder<NTreeT> >();
        } else {
            builder = std::make_shared<AsynchLookUpTableBuilder<NTreeT> >();
        }

        if (verbosity_level >= 3) {
            builder->set_verbose(true);
        }

        std::shared_ptr<WorkTable> worktable = std::make_shared<WorkTable>();
        auto collector = std::make_shared<NodeAndProcCollector<NTreeT> >(worktable);
        collector->set_owner_rank(comm.rank());
        builder->add_traversal_listener(collector);
        builder->set_must_build_data_lookup(false);

        MOONOLITH_EVENT_BEGIN("middle_phase_detection");
        ////////////////////////////////////////////////////////////////////////////////
        std::shared_ptr<LookUpTable> table = std::make_shared<LookUpTable>();
        table->set_verbose(verbosity_level >= 1);
        bool ok = builder->build_for(comm, *tree, table);
        assert(ok);
        ////////////////////////////////////////////////////////////////////////////////

        // LOCAL PORTION should be moved before
        // Find local intersections cells if any and add them to the work table
        if (predicate) {
            if (predicate->match_local_data()) {
                tree->refine();
                for (auto node_ptr : tree->memory().nodes()) {
                    // must compute intersection for local related data
                    if (node_ptr->has_data() && predicate->tags_are_related(node_ptr->tags().begin(),
                                                                            node_ptr->tags().end(),
                                                                            node_ptr->tags().begin(),
                                                                            node_ptr->tags().end())) {
                        collector->add_local_entry(*tree, *node_ptr);
                    }
                }
            }
        } else {
            tree->refine();
            for (auto node_ptr : tree->memory().nodes()) {
                // must compute intersection for local related data
                if (node_ptr->has_data() && predicate->tags_are_related(node_ptr->tags().begin(),
                                                                        node_ptr->tags().end(),
                                                                        node_ptr->tags().begin(),
                                                                        node_ptr->tags().end())) {
                    collector->add_local_entry(*tree, *node_ptr);
                }
            }
        }

        MOONOLITH_EVENT_END("middle_phase_detection");

        if (!ok && verbosity_level >= 1) {
            if (comm.is_root()) std::cerr << "make_work_table: NOTHING TO BUILD " << std::endl;
        }

        // if (verbosity_level >= 1) {
        //     comm.barrier();
        //     clock.tock();

        //     if (comm.is_root()) logger() << "make_work_table build time:\n" << clock << std::endl;

        //     clock.tick();
        // }

        assert(table->check_consistency(comm));

        MOONOLITH_EVENT_BEGIN("load_balancing");
        tree->memory().synchronize(comm);
        ////////////////////////////////////////////////////////////////////////////////
        LoadBalancer<NTreeT> balancer;
        balancer.set_use_linear_cost(use_linear_cost);
        balancer.balance(comm, *tree, *worktable);
        ////////////////////////////////////////////////////////////////////////////////
        MOONOLITH_EVENT_END("load_balancing");

        // if (verbosity_level >= 1) {
        //     comm.barrier();
        //     clock.tock();
        //     if(comm.is_root()) logger() << "LoadBalancer time:\n" << clock << std::endl;
        // }

        if (enable_stats) {
            table->compute_statistics(comm);
            table->synch_describe_stats(comm, logger());
            tree->memory().compute_statistics();
            tree->memory().synch_describe(comm, logger());
        }

        if (verbosity_level >= 3) {
            worktable->synch_describe(comm, logger());
        }

        return worktable;
    }

    /// read:  [...](const Integer rank, NTreeT::DataContainerT &data, cutk::InputStream &in) {}
    /// write: [...](const Integer rank, const SelectionIter begin, const SelectionIter end, cutk::OutputStream &out) {}
    /// fun:   [...](const NTreeT::Traits::DataType &master, const  NTreeT::Traits::DataType &slave, Result &isect) ->
    /// bool {}
    template <class NTreeT, class Read, class Write, class Fun>
    void compute(Communicator &comm,
                 const std::shared_ptr<NTreeT> &tree,
                 const std::shared_ptr<Predicate> &predicate,
                 const WorkTable &worktable,
                 Read read,
                 Write write,
                 Fun fun,
                 const SearchSettings &settings = SearchSettings()) {
        // typedef typename NTreeT::Traits::Bound::StaticBound StaticBound;
        // typedef typename NTreeT::DataType Adapter;
        typedef moonolith::Workspace<NTreeT> WorkspaceT;
        // typedef typename WorkspaceT::SelectionIter SelectionIter;
        // typedef typename WorkspaceT::DataContainer DataContainerT;

        // static const int Dimensions = NTreeT::Traits::Dimension;

        const bool verbosity_level_1 = settings.verbosity_level >= 1;
        const bool verbosity_level_2 = settings.verbosity_level >= 2;
        // const bool verbosity_level_3 = settings.verbosity_level >= 3;

        const bool enable_stats = settings.enable_stats;
        const bool must_redistribute = !settings.disable_redistribution;

        if (verbosity_level_1) {
            comm.barrier();
            if (comm.is_root()) {
                logger() << "Creating workspace" << std::endl;
            }
            comm.barrier();
        }

        MOONOLITH_EVENT_BEGIN("communicate_and_organize_dependencies");
        ////////////////////////////////////////////////////////////////////////////////
        WorkspaceT wp;
        wp.set_verbose(verbosity_level_2);
        wp.initialize(comm, worktable);
        wp.exchange(comm, *tree, read, write);
        ////////////////////////////////////////////////////////////////////////////////
        MOONOLITH_EVENT_END("communicate_and_organize_dependencies");

        if (verbosity_level_1 && comm.is_root()) logger() << "[Status] Computing....." << std::endl;

        Integer n_success = 0;
        if (!must_redistribute || comm.is_alone()) {
            MOONOLITH_EVENT_BEGIN("z_compute_results");
            ////////////////////////////////////////////////////////////////////////////////
            n_success = wp.apply(comm, *tree, predicate, fun);
            ////////////////////////////////////////////////////////////////////////////////
            MOONOLITH_EVENT_END("z_compute_results");
        } else {
            MOONOLITH_EVENT_BEGIN("rebalance");
            typename WorkspaceT::Candidates candidates;
            wp.determine_and_collect(comm, *tree, predicate, candidates);
            wp.migrate(comm, *tree, candidates, read, write);
            MOONOLITH_EVENT_END("rebalance");
            MOONOLITH_EVENT_BEGIN("z_compute_results");
            n_success = wp.compute(comm, *tree, candidates, predicate, fun);
            MOONOLITH_EVENT_END("z_compute_results");
        }

        if (verbosity_level_1) {
            if (comm.is_root()) logger() << "[Status] Done computing." << std::endl;
        }

        if (enable_stats) {
            synch_describe(std::to_string(n_success) + " results", comm, logger());

            comm.all_reduce(&n_success, 1, MPISum());

            if (comm.is_root()) {
                logger() << "[Stats] total results: " << n_success << std::endl;
            }
        }
    }

    /**
     * @ingroup     api
     * @brief The search and compute function allows looking for intersecting elements
     *  with automatically load-balancing the computation performed by the fun parameter.
     * @param comm the mpi communicator.
     * @param tree the (quad/)octree data-structure used for performing the search.
     *  see the file moonolith_test_trees.hpp for example tree types.
     * @param  predicate allows to match and discriminate between tagged objects
     * (for instance when we need to distinguish between disjoint sets of objects)
     * @param read a user implemented function that reads from an InputStream object (used for communication).
     * See example usage of search_and_compute in moonolith_test_trees.cpp.
     * @param write a user implemented function that writes to an OutputStream object (used for communication)
     * See example usage of search_and_compute in moonolith_test_trees.cpp.
     * @param fun a user implemented function that given two data adapters performs an operation on them
     * (such as computing actual intersections, assembling interpolation operators, etc...)
     * @param settings the user can provide options for verbosity, search strategies, enabling statiscs.
     */
    template <class NTreeT, class Read, class Write, class Fun>
    void search_and_compute(Communicator &comm,
                            const std::shared_ptr<NTreeT> &tree,
                            const std::shared_ptr<Predicate> &predicate,
                            Read read,
                            Write write,
                            Fun fun,
                            const SearchSettings &settings) {
        const bool enable_stats = settings.enable_stats;
        const bool verbosity_level_1 = settings.verbosity_level >= 1;

        if (comm.is_alone() && tree->memory().n_data() == 0) return;

        if (enable_stats || verbosity_level_1) {
            Integer n_elements = tree->memory().n_data();
            comm.all_reduce(&n_elements, 1, MPISum());
            if (comm.is_root()) {
                logger() << "[Stats] n adapters: " << n_elements << std::endl;
            }
        }

        std::shared_ptr<WorkTable> worktable = make_work_table(comm, tree, predicate, settings);
        compute(comm, tree, predicate, *worktable, read, write, fun, settings);
    }

}  // namespace moonolith

#endif  // PAR_MOONOLITH_HPP
