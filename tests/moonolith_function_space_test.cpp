
#include "moonolith_par_l2_transfer.hpp"
#include "moonolith_par_volume_surface_l2_transfer.hpp"

#include "gtest/gtest.h"

using namespace moonolith;

TEST(FunctionSpaceTest, Construct) {
    Communicator comm;
    auto master_mesh = std::make_shared<Mesh<Real, 2>>(comm);
    auto slave_mesh = std::make_shared<Mesh<Real, 2>>(comm);

    FunctionSpace<Mesh<Real, 2>> master(master_mesh), slave(slave_mesh);

    if (comm.is_root()) {
        ///////////////////////////////////////////////////
        //// MESH ////
        master_mesh->resize(1, 3);

        master_mesh->point(0) = {0.0, 0.0};
        master_mesh->point(1) = {1.0, 0.0};
        master_mesh->point(2) = {1.0, 1.0};

        auto &e_m = master_mesh->elem(0);

        e_m.type = TRI3;
        e_m.block = 0;
        e_m.global_idx = 0;
        e_m.nodes = {0, 1, 2};
        e_m.is_affine = true;

        //// DOFMAP ////

        master.dof_map().resize(1);

        auto &dof_m = master.dof_map().dof_object(0);
        dof_m.block = 0;
        dof_m.global_idx = 0;
        dof_m.element_dof = 0;
        dof_m.dofs = {0, 1, 2};
        dof_m.type = TRI3;

        master.dof_map().set_n_local_dofs(3);

        ///////////////////////////////////////////////////
        //// MESH ////

        slave_mesh->resize(1, 3);
        slave_mesh->point(0) = {0.0, 0.0};
        slave_mesh->point(1) = {1.0, 1.0};
        slave_mesh->point(2) = {0.5, 0.5};

        auto &e_s = slave_mesh->elem(0);

        e_s.type = EDGE3;
        e_s.block = 1;
        e_s.global_idx = 0;
        e_s.nodes = {0, 1, 2};
        e_s.is_affine = true;

        //// DOFMAP ////

        slave.dof_map().resize(1);
        auto &dof_s = slave.dof_map().dof_object(0);
        dof_s.block = 1;
        dof_s.global_idx = 0;
        dof_s.element_dof = 0;
        dof_s.dofs = {0, 1, 2};
        dof_s.type = EDGE3;

        slave.dof_map().set_n_local_dofs(3);

        ///////////////////////////////////////////////////
    } else {
        master.dof_map().set_n_local_dofs(0);
        slave.dof_map().set_n_local_dofs(0);
    }

    master_mesh->finalize();
    slave_mesh->finalize();

    // ParL2Transfer<Real, 2, 2, 1> transfer(comm);
    ParVolumeSurfaceL2Transfer<Real, 2> transfer(comm);
    transfer.assemble(master, slave);
}
