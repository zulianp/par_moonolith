#ifndef MOONOLITH_AFFINE_TRANSFORM_HPP
#define MOONOLITH_AFFINE_TRANSFORM_HPP

#include <array>
#include "moonolith_expanding_array.hpp"
#include "moonolith_matrix.hpp"
#include "moonolith_transform.hpp"
#include "moonolith_vector.hpp"

#include "moonolith_eigen_values.hpp"

namespace moonolith {

    template <typename T, int Dim, int PhysicalDim = Dim>
    class AffineTransform final : public Transform<T, Dim, PhysicalDim> {
    public:
        using Super = moonolith::Transform<T, Dim, PhysicalDim>;
        using Vector = moonolith::Vector<T, Dim>;
        using CoVector = moonolith::Vector<T, PhysicalDim>;
        using Point = moonolith::Vector<T, Dim>;
        using CoPoint = moonolith::Vector<T, PhysicalDim>;

        using Super::apply_inverse;

        bool apply(const Point &in, CoPoint &out) override {
            for (int i = 0; i < PhysicalDim; ++i) {
                const auto offset = i * Dim;
                out[i] = mat[offset] * in[0] + b[i];

                for (int j = 1; j < Dim; ++j) {
                    out[i] += mat[offset + j] * in[j];
                }
            }

            return true;
        }

        /// @brief out = A^T in
        void apply_to_direction(const Vector &in, CoVector &out) const {
            for (int i = 0; i < PhysicalDim; ++i) {
                out[i] = 0.;
            }

            for (int i = 0; i < Dim; ++i) {
                const auto offset = i * Dim;
                for (int j = 0; j < PhysicalDim; ++j) {
                    out[j] += mat_inv[offset + j] * in[i];
                }
            }
        }

        /// @brief out = A^-T in
        void apply_inverse_to_direction(const CoVector &in, Vector &out) const {
            for (int i = 0; i < Dim; ++i) {
                out[i] = 0.;
            }

            for (int i = 0; i < PhysicalDim; ++i) {
                const auto offset = i * Dim;
                for (int j = 0; j < Dim; ++j) {
                    out[j] += mat[offset + j] * in[i];
                }
            }
        }

        bool apply_inverse(const CoPoint &in, Point &out) override {
            for (int i = 0; i < Dim; ++i) {
                const auto offset = i * PhysicalDim;
                out[i] = mat_inv[offset] * in[0] + b_inv[i];

                for (int j = 1; j < PhysicalDim; ++j) {
                    out[i] += mat_inv[offset + j] * in[j];
                }
            }

            return true;
        }

        bool apply_inverse(const Storage<CoPoint> &in, Storage<Point> &out) {
            auto n = in.size();
            out.resize(n);

            for (std::size_t i = 0; i < n; ++i) {
                apply_inverse(in[i], out[i]);
            }

            return true;
        }

        inline T estimate_condition_number() const override {
            // FIXME
            return 1.0;
        }

        // private:
        std::array<T, Dim * PhysicalDim> mat, mat_inv;
        CoVector b;
        Vector b_inv;
    };

    template <typename T, int Dim>
    class AffineTransform<T, Dim, Dim> : public Transform<T, Dim, Dim> {
    public:
        using Super = moonolith::Transform<T, Dim, Dim>;
        using Vector = moonolith::Vector<T, Dim>;
        using Point = moonolith::Vector<T, Dim>;

        using Super::apply_inverse;

        bool apply(const Point &in, Point &out) override {
            for (int i = 0; i < Dim; ++i) {
                const auto offset = i * Dim;
                out[i] = mat[offset] * in[0] + b[i];

                for (int j = 1; j < Dim; ++j) {
                    out[i] += mat[offset + j] * in[j];
                }
            }

            return true;
        }

        /// @brief out = A^T in
        void apply_to_direction(const Vector &in, Vector &out) const {
            for (int i = 0; i < Dim; ++i) {
                out[i] = 0.;
            }

            for (int i = 0; i < Dim; ++i) {
                const auto offset = i * Dim;
                for (int j = 0; j < Dim; ++j) {
                    out[j] += mat_inv[offset + j] * in[i];
                }
            }
        }

        /// @brief out = A^-T in
        void apply_inverse_to_direction(const Vector &in, Vector &out) const {
            for (int i = 0; i < Dim; ++i) {
                out[i] = 0.;
            }

            for (int i = 0; i < Dim; ++i) {
                const auto offset = i * Dim;
                for (int j = 0; j < Dim; ++j) {
                    out[j] += mat[offset + j] * in[i];
                }
            }
        }

        bool apply_inverse(const Point &in, Point &out) override {
            for (int i = 0; i < Dim; ++i) {
                const auto offset = i * Dim;
                out[i] = mat_inv[offset] * in[0] + b_inv[i];

                for (int j = 1; j < Dim; ++j) {
                    out[i] += mat_inv[offset + j] * in[j];
                }
            }

            return true;
        }

        bool apply_inverse(const Storage<Point> &in, Storage<Point> &out) {
            auto n = in.size();
            out.resize(n);

            for (std::size_t i = 0; i < n; ++i) {
                apply_inverse(in[i], out[i]);
            }

            return true;
        }

        inline T estimate_condition_number() const override {
            T values[Dim];
            EigenValues<std::array<T, Dim * Dim>>::apply(mat, values);

            T min_val = std::abs(values[0]);
            T max_val = std::abs(values[0]);

            for (int i = 1; i < Dim; ++i) {
                min_val = std::min(min_val, std::abs(values[i]));
                max_val = std::max(max_val, std::abs(values[i]));
            }

            return max_val / min_val;
        }

        // private:
        std::array<T, Dim * Dim> mat, mat_inv;
        Vector b, b_inv;
    };

    template <typename T>
    bool make(const moonolith::Vector<T, 2> &p0,
              const moonolith::Vector<T, 2> &p1,
              const moonolith::Vector<T, 2> &p2,
              AffineTransform<T, 2> &t) {
        auto &mat = t.mat;
        auto &mat_inv = t.mat_inv;
        auto &b = t.b;
        auto &b_inv = t.b_inv;

        // row 0
        mat[0] = p1.x - p0.x;
        mat[1] = p2.x - p0.x;

        // row 1
        mat[2] = p1.y - p0.y;
        mat[3] = p2.y - p0.y;

        b = p0;

        const T det = moonolith::det(mat);
        assert(det > 0.);

        if (det <= 0.) {
            return false;
        }

        invert(mat, mat_inv, det);

        b_inv.x = -(mat_inv[0] * b.x + mat_inv[1] * b.y);
        b_inv.y = -(mat_inv[2] * b.x + mat_inv[3] * b.y);
        return true;
    }

    template <typename T>
    bool make(const moonolith::Vector<T, 3> &p0,
              const moonolith::Vector<T, 3> &p1,
              const moonolith::Vector<T, 3> &p2,
              const moonolith::Vector<T, 3> &p3,
              AffineTransform<T, 3> &t) {
        auto &mat = t.mat;
        auto &mat_inv = t.mat_inv;
        auto &b = t.b;
        auto &b_inv = t.b_inv;

        // row 0
        mat[0] = p1.x - p0.x;
        mat[1] = p2.x - p0.x;
        mat[2] = p3.x - p0.x;

        // row 1
        mat[3] = p1.y - p0.y;
        mat[4] = p2.y - p0.y;
        mat[5] = p3.y - p0.y;

        // row 2
        mat[6] = p1.z - p0.z;
        mat[7] = p2.z - p0.z;
        mat[8] = p3.z - p0.z;

        b = p0;

        if (!invert(mat, mat_inv)) {
            return false;
        }

        b_inv.x = -(mat_inv[0] * b.x + mat_inv[1] * b.y + mat_inv[2] * b.z);
        b_inv.y = -(mat_inv[3] * b.x + mat_inv[4] * b.y + mat_inv[5] * b.z);
        b_inv.z = -(mat_inv[6] * b.x + mat_inv[7] * b.y + mat_inv[8] * b.z);
        return true;
    }

    template <typename T>
    bool make(const moonolith::Vector<T, 3> &p0,
              const moonolith::Vector<T, 3> &p1,
              const moonolith::Vector<T, 3> &p2,
              AffineTransform<T, 2, 3> &t) {
        auto &mat = t.mat;
        auto &mat_inv = t.mat_inv;
        auto &b = t.b;
        auto &b_inv = t.b_inv;

        // row 0
        mat[0] = p1.x - p0.x;
        mat[1] = p2.x - p0.x;

        // row 1
        mat[2] = p1.y - p0.y;
        mat[3] = p2.y - p0.y;

        // row 2
        mat[4] = p1.z - p0.z;
        mat[5] = p2.z - p0.z;

        b = p0;

        if (!pseudo_invert<T, 3, 2>(mat, t.mat_inv)) {
            assert(false);
            return false;
        }

        b_inv.x = -(mat_inv[0] * b.x + mat_inv[1] * b.y + mat_inv[2] * b.z);
        b_inv.y = -(mat_inv[3] * b.x + mat_inv[4] * b.y + mat_inv[5] * b.z);
        return true;
    }

    template <typename T>
    bool make(const moonolith::Vector<T, 4> &p0,
              const moonolith::Vector<T, 4> &p1,
              const moonolith::Vector<T, 4> &p2,
              const moonolith::Vector<T, 4> &p3,
              const moonolith::Vector<T, 4> &p4,
              AffineTransform<T, 4> &t) {
        auto &mat = t.mat;
        auto &mat_inv = t.mat_inv;
        auto &b = t.b;
        auto &b_inv = t.b_inv;

        mat[0] = p1.x - p0.x;
        mat[1] = p2.x - p0.x;
        mat[2] = p3.x - p0.x;
        mat[3] = p4.x - p0.x;

        mat[4] = p1.y - p0.y;
        mat[5] = p2.y - p0.y;
        mat[6] = p3.y - p0.y;
        mat[7] = p4.y - p0.y;

        mat[8] = p1.z - p0.z;
        mat[9] = p2.z - p0.z;
        mat[10] = p3.z - p0.z;
        mat[11] = p4.z - p0.z;

        mat[12] = p1.w - p0.w;
        mat[13] = p2.w - p0.w;
        mat[14] = p3.w - p0.w;
        mat[15] = p4.w - p0.w;

        b.x = p0.x;
        b.y = p0.y;
        b.z = p0.z;
        b.w = p0.w;

        if (!invert(mat, mat_inv)) {
            return false;
        }

        b_inv.x = -(mat_inv[0] * b.x + mat_inv[1] * b.y + mat_inv[2] * b.z + mat_inv[3] * b.w);
        b_inv.y = -(mat_inv[4] * b.x + mat_inv[5] * b.y + mat_inv[6] * b.z + mat_inv[7] * b.w);
        b_inv.z = -(mat_inv[8] * b.x + mat_inv[9] * b.y + mat_inv[10] * b.z + mat_inv[11] * b.w);
        b_inv.w = -(mat_inv[12] * b.x + mat_inv[13] * b.y + mat_inv[14] * b.z + mat_inv[15] * b.w);
        return true;
    }

    template <typename T>
    bool make(const moonolith::Vector<T, 2> &p0, const moonolith::Vector<T, 2> &p1, AffineTransform<T, 1, 2> &t) {
        auto &mat = t.mat;
        auto &mat_inv = t.mat_inv;
        auto &b = t.b;
        auto &b_inv = t.b_inv;

        mat[0] = p1.x - p0.x;
        mat[1] = p1.y - p0.y;

        b = p0;

        if (!pseudo_invert<T, 2, 1>(mat, t.mat_inv)) {
            assert(false);
            return false;
        }

        b_inv.x = -(mat_inv[0] * b.x + mat_inv[1] * b.y);
        return true;
    }

    template <typename T>
    bool make(const moonolith::Vector<T, 2> &p0, const moonolith::Vector<T, 2> &p1, AffineTransform<T, 2, 2> &t) {
        auto &mat = t.mat;
        auto &mat_inv = t.mat_inv;
        auto &b = t.b;
        auto &b_inv = t.b_inv;

        auto u = p1 - p0;
        Vector<T, 2> n(-u.y, u.x);
        n /= length(n);

        mat[0] = u.x;
        mat[1] = n.x;
        mat[2] = u.y;
        mat[3] = n.y;

        b = p0;

        const T det = moonolith::det(mat);
        assert(det > 0.);

        if (det <= 0.) {
            return false;
        }

        invert(mat, mat_inv, det);

        b_inv.x = -(mat_inv[0] * b.x + mat_inv[1] * b.y);
        b_inv.y = -(mat_inv[2] * b.x + mat_inv[3] * b.y);
        return true;
    }

    template <typename T>
    bool make(const moonolith::Vector<T, 4> &p0,
              const moonolith::Vector<T, 4> &p1,
              const moonolith::Vector<T, 4> &p2,
              const moonolith::Vector<T, 4> &p3,
              AffineTransform<T, 3, 4> &t) {
        auto &mat = t.mat;
        auto &mat_inv = t.mat_inv;
        auto &b = t.b;
        auto &b_inv = t.b_inv;

        mat[0] = p1.x - p0.x;
        mat[1] = p2.x - p0.x;
        mat[2] = p3.x - p0.x;

        mat[3] = p1.y - p0.y;
        mat[4] = p2.y - p0.y;
        mat[5] = p3.y - p0.y;

        mat[6] = p1.z - p0.z;
        mat[7] = p2.z - p0.z;
        mat[8] = p3.z - p0.z;

        mat[9] = p1.w - p0.w;
        mat[10] = p2.w - p0.w;
        mat[11] = p3.w - p0.w;

        b = p0;

        if (!pseudo_invert<T, 4, 3>(mat, t.mat_inv)) {
            assert(false);
            return false;
        }

        b_inv.x = -(mat_inv[0] * b.x + mat_inv[1] * b.y + mat_inv[2] * b.z + mat_inv[3] * b.w);
        b_inv.y = -(mat_inv[4] * b.x + mat_inv[5] * b.y + mat_inv[6] * b.z + mat_inv[7] * b.w);
        b_inv.z = -(mat_inv[8] * b.x + mat_inv[9] * b.y + mat_inv[10] * b.z + mat_inv[11] * b.w);
        return true;
    }

    template <typename T, int Dim, int PhysicalDim>
    void init_inverse(AffineTransform<T, Dim, PhysicalDim> &t) {
        PseudoInvert<T, PhysicalDim, Dim>().apply(t.mat, t.mat_inv);
        mat_vec_mul<T, Dim, PhysicalDim>(t.mat_inv, t.b, t.b_inv);
        t.b_inv = -t.b_inv;
    }

    template <typename T, int Dim>
    T measure(const AffineTransform<T, Dim, Dim> &trafo) {
        return det(trafo.mat);
    }

    template <typename T, int Dim, int PhysicalDim>
    T measure(const AffineTransform<T, Dim, PhysicalDim> &trafo) {
        return pseudo_det<T, PhysicalDim, Dim>(trafo.mat);
    }

}  // namespace moonolith

#endif  // MOONOLITH_AFFINE_TRANSFORM_HPP
