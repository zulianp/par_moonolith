#ifndef MOONOLITH_WARPED_CONTACT_HPP
#define MOONOLITH_WARPED_CONTACT_HPP

#include "moonolith_map_quadrature.hpp"
#include "moonolith_map_quadrature_impl.hpp"
#include "moonolith_project_convex_polygons.hpp"
#include "moonolith_project_line_segments.hpp"
#include "moonolith_project_polygons.hpp"
#include "moonolith_project_polylines.hpp"
#include "moonolith_shape.hpp"
#include "moonolith_transform.hpp"

#include <memory>

namespace moonolith {

    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////// WARPED Algorithms ///////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////

    template <typename T, int Dim>
    class WarpedContact;

    template <typename T, int Dim>
    class WarpedContact {};

    namespace private_ {
        template <typename T, int Dim>
        class WarpedContactAlgorithm {
        public:
            static bool compute(WarpedContact<T, Dim> &o) {
                o.init();

                // init plane
                o.make_plane(o.slave, o.plane);
                o.make_plane(o.master, o.master_plane);

                if (dot(o.plane.n, o.master_plane.n) >= o.accept_angle) {
                    return false;
                }

                // onto general plane
                if (!o.project.project_intersect_and_map_quadrature(o.master, o.slave, o.plane, o.q_rule, o.q_plane)) {
                    return false;
                }

                o.q_plane.normalize();

                const std::size_t n_qp = o.q_plane.n_points();
                o.q_master.resize(n_qp);
                o.q_slave.resize(n_qp);
                o.gap.resize(n_qp);

                o.ray.dir = o.plane.n;

                bool ok_master = false, ok_slave = false;
                bool skipped_qp = false;
                for (std::size_t k = 0; k < n_qp; ++k) {
                    auto w = o.q_plane.weights[k];
                    o.ray.o = o.q_plane.points[k];

                    // initial guesses with discrete approximation
                    T t_master_approx = 0., t_slave_approx = 0.;
                    o.approximate_intersect(o.ray, t_master_approx, t_slave_approx);

                    T t_master = t_master_approx;
                    T t_slave = t_slave_approx;
                    ok_master = o.shape_master->intersect(o.ray, t_master, o.q_master.points[k]);
                    assert(ok_master);
                    ok_slave = o.shape_slave->intersect(o.ray, t_slave, o.q_slave.points[k]);
                    assert(ok_slave);

                    o.gap[k] = t_master - t_slave;

                    if (!ok_master || !ok_slave) {
                        // fallback (maybe normalize quad weights if this is found?)
                        w = 0.;
                        skipped_qp = true;
                    }

                    o.q_master.weights[k] = w * o.project.intersection_ratio_1;
                    o.q_slave.weights[k] = w * o.project.intersection_ratio_2;
                }

                if (skipped_qp) {
                    std::cerr << "[Warning] skipped_qp" << std::endl;
                    // TODO normalize quad rule
                }

                return true;
            }
        };
    }  // namespace private_

    template <typename T>
    class WarpedContact<T, 2> {
    public:
        using Point = moonolith::Vector<T, 2>;
        using Vector = moonolith::Vector<T, 2>;
        using PolyLine = moonolith::PolyLine<T, 2>;
        friend class private_::WarpedContactAlgorithm<T, 2>;

        //////////////////////////////////////////////////////////////////
        // INPUT
        PolyLine master, slave;
        Quadrature<T, 1> q_rule;
        std::shared_ptr<Shape<T, 1, 2>> shape_master, shape_slave;
        bool invert_plane_dir;
        T accept_angle = -0.5;

        // OUTPUT
        Quadrature<T, 1> q_master, q_slave;
        Storage<T> gap;

        inline const Vector &normal() const { return plane.n; }

        //////////////////////////////////////////////////////////////////

        WarpedContact() : invert_plane_dir(false) {}

        void init() {}

        ///@brief first initialize master, slave, andq_rule, objects
        bool compute() { return private_::WarpedContactAlgorithm<T, 2>::compute(*this); }

        void make_plane(const PolyLine &poly, Plane<T, 2> &plane) { make_plane(poly.points, plane); }

        void make_plane(const Storage<Point> &poly, Plane<T, 2> &plane) {
            plane.n.x = 0.0;
            plane.n.y = 0.0;

            const Integer n_points = static_cast<Integer>(poly.size());
            const Integer n_segments = n_points - 1;

            for (Integer i = 0; i < n_segments; ++i) {
                const auto &p1 = poly[i];
                const auto &p2 = poly[i + 1];

                u = p2 - p1;
                u /= length(u);

                n.x = -u.y;
                n.y = u.x;
                plane.n += n;
            }

            auto len_n = length(plane.n);
            assert(len_n > 0.);

            plane.n /= len_n;

            plane.p.x = 0.0;
            plane.p.y = 0.0;

            for (Integer i = 0; i < n_points; ++i) {
                plane.p += poly[i];
            }

            plane.p /= static_cast<T>(n_points);

            if (invert_plane_dir) {
                plane.n = -plane.n;
            }
        }

        bool approximate_intersect(const Ray<T, 2> &ray, T &t_master_approx, T &t_slave_approx) {
            bool ok_master = master.intersect(ray, t_master_approx);  // assert(ok_master);
            bool ok_slave = slave.intersect(ray, t_slave_approx);     // assert(ok_slave);
            return ok_master && ok_slave;
        }

    private:
        Quadrature<T, 2> q_plane;
        Vector u, n;
        Ray<T, 2> ray;
        Plane<T, 2> plane, master_plane;
        ProjectPolyLines<T> project;
    };

    template <typename T>
    class WarpedContact<T, 3> {
    public:
        using Point = moonolith::Vector<T, 3>;
        using Vector = moonolith::Vector<T, 3>;
        friend class private_::WarpedContactAlgorithm<T, 3>;

        //////////////////////////////////////////////////////////////////
        // INPUT
        Polygon<T, 3> master, slave;
        Quadrature<T, 2> q_rule;
        std::shared_ptr<Shape<T, 2, 3>> shape_master, shape_slave;
        bool invert_plane_dir;
        T accept_angle = -0.5;

        // OUTPUT
        Storage<T> gap;
        Quadrature<T, 2> q_master, q_slave;

        inline const Vector &normal() const { return plane.n; }

        //////////////////////////////////////////////////////////////////

        WarpedContact() : invert_plane_dir(false) {}

        void init() {
            slave.remove_collinear_points(1e-10);
            master.remove_collinear_points(1e-10);
        }

        void make_plane(const Polygon<T, 3> &poly, Plane<T, 3> &plane) {
            plane.n.x = 0.0;
            plane.n.y = 0.0;
            plane.n.z = 0.0;

            Integer n_tris = poly.size() - 2;

            for (Integer i = 0; i < n_tris; ++i) {
                const auto &p1 = poly[i];
                const auto &p2 = poly[i + 1];
                const auto &p3 = poly[i + 2];

                u = p2 - p1;
                v = p3 - p1;

                n = cross(u, v);
                // n /= length(n); //use area of the parallelgram has scaling

                plane.n += n;
            }

            auto len_n = length(plane.n);
            assert(len_n > 0.);

            plane.n /= len_n;
            plane.p = poly.barycenter();

            if (invert_plane_dir) {
                plane.n = -plane.n;
            }
        }

        bool approximate_intersect(const Ray<T, 3> &ray, T &t_master_approx, T &t_slave_approx) {
            bool ok_master = master_plane.intersect(ray, t_master_approx);  // assert(ok_master);
            t_slave_approx = 0.;
            return ok_master;
        }

        ///@brief first initialize master, slave, andq_rule, objects
        bool compute() { return private_::WarpedContactAlgorithm<T, 3>::compute(*this); }

    private:
        Quadrature<T, 3> q_plane;
        Plane<T, 3> plane, master_plane;
        ProjectPolygons<T> project;
        Vector u, v, n;
        Ray<T, 3> ray;
    };

}  // namespace moonolith

#endif  // MOONOLITH_WARPED_CONTACT_HPP
