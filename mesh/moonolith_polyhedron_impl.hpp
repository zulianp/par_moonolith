#ifndef MOONOLITH_POLYHEDRON_IMPL_HPP
#define MOONOLITH_POLYHEDRON_IMPL_HPP

#include "moonolith_polyhedron.hpp"

namespace moonolith {

	template<typename T>
	T tetrahedron_volume(const Vector<T, 3> p1, const Vector<T, 3> p2, const Vector<T, 3> p3, const Vector<T, 3> p4)
	{
		return dot(p4 - p1, cross(p2-p1, p3-p1)) / 6.0;
	}

}


#endif //MOONOLITH_POLYHEDRON_IMPL_HPP
