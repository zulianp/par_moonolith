#ifndef MOONOLITH_LOCAL_GATHER_HPP
#define MOONOLITH_LOCAL_GATHER_HPP

#include "moonolith_forward_declarations.hpp"
#include "moonolith_mutator.hpp"

namespace moonolith {
    template<class Traits>
    class LocalGather : public Mutator<Traits>{
    public:
        typedef moonolith::Node<Traits> Node;
        typedef moonolith::TreeMemory<Traits> TreeMemory;

        virtual ~LocalGather() {}
        
        virtual bool success() const override
        {
            return true;
        }

        virtual void read(InputStream &) override
        {
            //implement me
            assert(false);
        }

        virtual void write(OutputStream &) const override
        {
            //implement me
            assert(false);
        }

        virtual void clear() override {}


        virtual std::string name() const override
        {
            return "LocalGather";
        }

        virtual NavigatorOption visit(Node &, TreeMemory &) override
        {
            return STOP;
        }
    };

}



#endif //MOONOLITH_LOCAL_GATHER_HPP
