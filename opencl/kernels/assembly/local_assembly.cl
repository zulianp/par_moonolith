#ifndef LOCAL_ASSEMBLY_CL 
#define LOCAL_ASSEMBLY_CL

#include "fe/fe_interfaces.cl"
#include "assembly_macros.cl"
#include "local_and_global_blocks.cl"

void linear_elasticity_assemble_local(
	const Scalar mu, const Scalar lambda, 
	FEObject *trial, FEObject *test, Scalar * matrix)
{
	Matrixdxd trial_grad[MAX_SHAPE_FUNCS * MAX_CODIM * MAX_CODIM];
	Matrixdxd test_grad [MAX_SHAPE_FUNCS * MAX_CODIM * MAX_CODIM];
	Scalar trial_trace	[MAX_SHAPE_FUNCS * MAX_CODIM];
	Scalar test_trace 	[MAX_SHAPE_FUNCS * MAX_CODIM];

	const SizeType n_entries_trial = trial->n_dims * trial->n_shape_functions;
	const SizeType n_entries_test  = test->n_dims  * test->n_shape_functions;
	
	const SizeType size_grad = trial->n_dims * test->n_dims;
	set(n_entries_trial * n_entries_test, 0.0, matrix);

// #define DISABLE_SYMMETRIC_ASSEMBLY //if you turn it on it will cost twice 
#ifdef DISABLE_SYMMETRIC_ASSEMBLY
	if(false) 
#else	
	if(trial == test) 
#endif //DISABLE_SYMMETRIC_ASSEMBLY
	{
		for(SizeType q = 0; q < test->n_quad_points; ++q) {
			make_grad_dxd_at_quad_index(q, test,  test_grad);
			symmetrize(test->n_dims,  n_entries_test,  test_grad);
			compute_traces(test->n_dims,  n_entries_test,  test_grad,  test_trace);

			symmetric_assemble_block_ij(test, matrix, (
				(
					2.0 * mu *  dot_n(size_grad, 
						test_grad[j].entries,
						test_grad[i].entries) 
					+ 
					lambda * 
					test_trace[j] * 
					test_trace[i]
					) * test->dx[q]) 
			);
		}

		block_matrix_copy_upper_to_lower(test->n_shape_functions, test->n_dims, matrix);

	} else {
		for(SizeType q = 0; q < test->n_quad_points; ++q) {
			make_grad_dxd_at_quad_index(q, trial, trial_grad);
			make_grad_dxd_at_quad_index(q, test,  test_grad);

			symmetrize(trial->n_dims, n_entries_trial, trial_grad);
			symmetrize(test->n_dims,  n_entries_test,  test_grad);

			compute_traces(trial->n_dims, n_entries_trial, trial_grad, trial_trace);
			compute_traces(test->n_dims,  n_entries_test,  test_grad,  test_trace);

			assemble_block_ij(trial, test, matrix, (
				(
					2.0 * mu *  dot_n(size_grad, 
						trial_grad[j].entries,
						test_grad[i].entries) 
					+ 
					lambda * 
					trial_trace[j] * 
					test_trace [i]
					) * test->dx[q]) 
			);
		}
	}
}

void block_mass_matrix_assemble_local(FEObject *trial, FEObject *test, Scalar * matrix)
{
	if(trial == test) {
		for(SizeType i = 0; i < test->n_shape_functions; ++i) {
			//diagonal block
			{
				Scalar value = 0;
				for(SizeType q = 0; q < test->n_quad_points; ++q) {
					value += test->fun[i][q] * test->fun[i][q] * test->dx[q];
				}
				set_block_to_identity(i, i, test->n_dims, test->n_dims, test->n_shape_functions, trial->n_shape_functions, value, matrix);
			}

			for(SizeType j = i+1; j < test->n_shape_functions; ++j) {
				Scalar value = 0;
				for(SizeType q = 0; q < test->n_quad_points; ++q) {
					value += test->fun[j][q] * test->fun[i][q] * test->dx[q];
				}

				//kronecker product
				set_block_to_identity(i, j, test->n_dims, test->n_dims, test->n_shape_functions, test->n_shape_functions, value, matrix);
				set_block_to_identity(j, i, test->n_dims, test->n_dims, test->n_shape_functions, test->n_shape_functions, value, matrix);
			}
		}
	} else {
		for(SizeType i = 0; i < test->n_shape_functions; ++i) {
			for(SizeType j = 0; j < trial->n_shape_functions; ++j) {
				Scalar value = 0;
				for(SizeType q = 0; q < test->n_quad_points; ++q) {
					value += trial->fun[j][q] * test->fun[i][q] * test->dx[q];
				}

				//kronecker product
				set_block_to_identity(i, j, test->n_dims, trial->n_dims, test->n_shape_functions, trial->n_shape_functions, value, matrix);
			}
		}
	}
}

void laplacian_assemble_local(FEObject *trial, FEObject *test, Scalar *matrix)
{
	assemble_ijq(trial, test, matrix, (dot_n(test->n_dims, &trial->grad[j][grad_q], &test->grad[i][grad_q]) * test->dx[q]) );
}


void mass_matrix_assemble_local(FEObject *trial, FEObject *test, Scalar * matrix)
{
	assemble_ijq(trial, test, matrix, (test->fun[i][q] * trial->fun[j][q] * test->dx[q]) );
}

#endif //LOCAL_ASSEMBLY_CL

