#ifndef MOONOLITH_REFINE_ALL_HPP
#define MOONOLITH_REFINE_ALL_HPP

#include "moonolith_mutator.hpp"
#include "moonolith_forward_declarations.hpp"

namespace moonolith {

	template<class Traits>
	class RefineAll : public Mutator<Traits> {
	public:
		typedef moonolith::Node<Traits> Node;
		typedef moonolith::Leaf<Traits> Leaf;
		typedef moonolith::TreeMemory<Traits> TreeMemory;

		virtual NavigatorOption visit(Node &, TreeMemory &) {
			return CONTINUE;
		}

		virtual NavigatorOption visit(Leaf &node, TreeMemory &memory) {
            if(can_refine(node, memory)) {
                return REFINE;
            } else {
                return CONTINUE;
            }
		}

		virtual bool can_refine(Leaf &leaf, TreeMemory &memory) const
		{
			if(leaf.empty()) return false;
			std::shared_ptr< Mutator<Traits> > m_can_refine = memory.mutator_factory()->new_can_refine();
			leaf.accept(*m_can_refine, memory);
			return m_can_refine->success();
		}

		virtual bool success() const 
		{
			return true;
		}
	};

}


#endif //MOONOLITH_REFINE_ALL_HPP

