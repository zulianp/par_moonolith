// #ifndef MOONOLITH_ELEM_TRIANGLE_HPP
// #define MOONOLITH_ELEM_TRIANGLE_HPP

// #include "moonolith_elem.hpp"
// #include "moonolith_gauss_quadrature_rule.hpp"
// #include "moonolith_polygon_impl.hpp"

// namespace moonolith {

//     template<typename T, int Order_, int PhysicalDim_ = 2>
//     class Triangle : public Elem<T, 2, PhysicalDim_> {};


//     template<typename T_, int PhysicalDim_>
//     class Triangle<T_, 0, PhysicalDim_> final : public Elem<T_, 2, PhysicalDim_> {
//     public:
//         static const int Order = 0;
//         static const int Dim   = 2;
//         static const int PhysicalDim = PhysicalDim_;
//         static const int NNodes = 1;

//         using T = T_;

//         using Vector   = moonolith::Vector<T, Dim>;
//         using CoVector = moonolith::Vector<T, PhysicalDim>;

//         using Point   = moonolith::Vector<T, Dim>;
//         using CoPoint = moonolith::Vector<T, PhysicalDim>;

//     private:

//         class Grad final {
//         public:
//             Grad()
//             {
//                 f[0] = [](const Point &, Vector &g) {
//                     g.x = 0.0;
//                 };

//             }

//             std::array<std::function<void (const Point &, Vector &)>, NNodes> f;
//         };

//         class Fun final {
//         public:

//             Fun()
//             {
//                 f[0] = [](const Point &) -> T {
//                     return 1.0;
//                 };
//             }

//             std::array<std::function<T (const Point &)>, NNodes> f;
//         };

//     public:

//         T fun(const Integer i, const Point &p) const override
//         {
//             return fun_.f[i](p);
//         }

//         void grad(const Integer i, const Point &p, Vector &g) const override
//         {
//             grad_.f[i](p, g);
//         }

//        void hessian(const Integer i, const Point &p, std::array<T, Dim*Dim> &H)
//        {
//            std::fill(std::begin(H), std::end(H), 0.0);
//        }

//         void jacobian(const Point &p, std::array<T, PhysicalDim*Dim> &J) const override
//         {
//             (void)p;
//             affine_approx_jacobian(J);
//         }

//         void affine_approx_jacobian(std::array<T, PhysicalDim*Dim> &J) const override
//         {
//             Affine<Triangle>::jacobian(points_, J);
//         }

//         CoPoint &node(const Integer i) override
//         {
//             return nodes_[i];
//         }

//         const CoPoint &node(const Integer i) const override
//         {
//             return nodes_[i];
//         }

//         void point(const Point &p, CoPoint &q) const override
//         {
//             Affine<Triangle>::point(points_, p, q);
//         }

//         inline CoPoint &point(const Integer idx)
//         {
//             return points_[idx];
//         }

//         inline const CoPoint &point(const Integer idx) const
//         {
//             return points_[idx];
//         }


//         Triangle() {}

//         bool is_affine() const override { return true; }

//         inline int n_nodes() const override { return NNodes; }
//         inline int order() const override { return Order; }

//         inline const std::array<CoPoint, 3> &points() const
//         {
//             return points_;
//         }

//         inline void make_reference()
//         {
//             Reference<Triangle>::points(nodes_);
//             Reference<Triangle<T, 1, PhysicalDim>>::points(points_);
//         }

//         inline ElemType type() const override
//         {
//             return TRI1;
//         }

//         inline bool is_simplex() const override { return true; }

//         inline T measure() const override
//         {
//             return 0.5 * trapezoid_area(points_[0], points_[1], points_[2]);
//         }

//         inline T reference_measure() const override { return 0.5; }


//     private:
//         std::array<CoPoint, NNodes> nodes_;
//         std::array<CoPoint, 3> points_;
//         const Fun  fun_;
//         const Grad grad_;
//     };

//     template<typename T, int PhysicalDim>
//     class Reference<Triangle<T, 0, PhysicalDim>> {
//     public:
//         static const int NNodes = 1;
        
//         template<class Point, std::size_t Size>
//         static void points(std::array<Point, Size> &nodes)
//         {
//             static_assert(Size >= NNodes, "size must be at least 1");
//             //p0
//             auto x = sqrt(2.)/4.0;
//             nodes[0].x = x;
//             nodes[0].y = x;
//         }

//         inline static T measure() { return 0.5; }
//     };

//     //////////////////////////////////////////////////////////////////////////////////////////

//     template<typename T_, int PhysicalDim_>
//     class Triangle<T_, 1, PhysicalDim_> final : public Elem<T_, 2, PhysicalDim_> {
//     public:
//         static const int Order = 1;
//         static const int Dim   = 2;
//         static const int PhysicalDim = PhysicalDim_;
//         static const int NNodes = 3;

//         using T = T_;

//         using Vector   = moonolith::Vector<T, Dim>;
//         using CoVector = moonolith::Vector<T, PhysicalDim>;

//         using Point   = moonolith::Vector<T, Dim>;
//         using CoPoint = moonolith::Vector<T, PhysicalDim>;

//     private:

//         class Grad final {
//         public:
//             Grad()
//             {
//                 f[0] = [](const Point &p, Vector &g) {
//                     g.x = -1.0;
//                     g.y = -1.0;
//                 };

//                 f[1] = [](const Point &p, Vector &g) {
//                     g.x = 1;
//                     g.y = 0.0;
//                 };

//                 f[2] = [](const Point &p, Vector &g) {
//                     g.x = 0.0;
//                     g.y = 1.0;
//                 };
//             }

//             std::array<std::function<void (const Point &, Vector &)>, 3> f;
//         };

//         class Fun final {
//         public:

//             Fun()
//             {
//                 f[0] = [](const Point &p) -> T {
//                     return 1.0 - p.x - p.y;
//                 };

//                 f[1] = [](const Point &p) -> T {
//                     return p.x;
//                 };

//                 f[2] = [](const Point &p) -> T {
//                     return p.y;
//                 };
//             }

//             std::array<std::function<T (const Point &)>, 3> f;
//         };

//     public:

//         T fun(const Integer i, const Point &p) const override
//         {
//             return fun_.f[i](p);
//         }

//         void grad(const Integer i, const Point &p, Vector &g) const override
//         {
//             grad_.f[i](p, g);
//         }

//        void hessian(const Integer i, const Point &p, std::array<T, Dim*Dim> &H)
//        {
//            std::fill(std::begin(H), std::end(H), 0.0);
//        }

//         void jacobian(const Point &p, std::array<T, PhysicalDim*Dim> &J) const override
//         {
//             (void)p;
//             affine_approx_jacobian(J);
//         }

//         void affine_approx_jacobian(std::array<T, PhysicalDim*Dim> &J) const override
//         {
//             Affine<Triangle>::jacobian(*this, J);
//         }

//         CoPoint &node(const Integer i) override
//         {
//             return nodes_[i];
//         }

//         const CoPoint &node(const Integer i) const override
//         {
//             return nodes_[i];
//         }

//         void point(const Point &p, CoPoint &q) const override
//         {
//             IsoParametric<Triangle>::point(*this, p, q);
//         }

//         Triangle() {}

//         bool is_affine() const override { return true; }
//         inline bool is_simplex() const override { return true; }

//         inline int n_nodes() const override { return NNodes; }
//         inline int order() const override { return Order; }

//         inline void make_reference()
//         {
//             Reference<Triangle>::points(nodes_);
//         }

//         inline ElemType type() const override
//         {
//             return TRI3;
//         }

//         inline T measure() const override
//         {
//             return 0.5 * trapezoid_area(nodes_[0], nodes_[1], nodes_[2]);
//         }

//         inline T reference_measure() const override { return 0.5; }

//     private:
//         std::array<CoPoint, 3> nodes_;
//         const Fun  fun_;
//         const Grad grad_;
//     };


//     template<typename T, int PhysicalDim>
//     class Reference<Triangle<T, 1, PhysicalDim>> {
//     public:
//         // using Point = typename Triangle<T, 1, PhysicalDim>::Point;
//         static const int NNodes = 3;
        
//         template<class Point, std::size_t Size>
//         static void points(std::array<Point, Size> &nodes)
//         {
//             static_assert(Size >= 3, "size must be at least 3");
//             //p0
//             nodes[0].x = 0.0;
//             nodes[0].y = 0.0;

//             //p1
//             nodes[1].x = 1.0;
//             nodes[1].y = 0.0;

//             //p2
//             nodes[2].x = 0.0;
//             nodes[2].y = 1.0;
//         }

//         inline static T measure() { return 0.5; }
//     };

//     //////////////////////////////////////////////////////////////////////////////////////////

//     template<typename T_, int PhysicalDim_>
//     class Triangle<T_, 2, PhysicalDim_> final : public Elem<T_, 2, PhysicalDim_> {
//     public:
//         static const int Order = 2;
//         static const int Dim   = 2;
//         static const int PhysicalDim = PhysicalDim_;
//         static const int NNodes = 6;

//         using T = T_;

//         using Vector   = moonolith::Vector<T, Dim>;
//         using CoVector = moonolith::Vector<T, PhysicalDim>;

//         using Point   = moonolith::Vector<T, Dim>;
//         using CoPoint = moonolith::Vector<T, PhysicalDim>;

//     private:

//         class Fun final {
//         public:

//             Fun()
//             {
//                 f[0] = [](const Point &p) -> T {
//                     return 1.0 - (3.0 * p.x) - (3.0 * p.y) + (2.0 * p.x * p.x) + (2.0 * p.y * p.y) + (4.0 * p.x * p.y);
//                 };

//                 f[1] = [](const Point &p) -> T {
//                     return (2.0 * p.x * p.x) - p.x;
//                 };

//                 f[2] = [](const Point &p) -> T {
//                     return (2.0 * p.y * p.y) - p.y;
//                 };

//                 f[3] = [](const Point &p) -> T {
//                     return (4.0 * p.x)- (4.0 * p.x * p.x) - (4.0 * p.x * p.y);
//                 };

//                 f[4] = [](const Point &p) -> T {
//                     return (4.0 * p.x * p.y);
//                 };

//                 f[5] = [](const Point &p) -> T {
//                     return (4.0 * p.y) - (4.0 * p.x * p.y) - (4.0 * p.y * p.y);
//                 };
//             }

//             std::array<std::function<T (const Point &)>, 6> f;
//         };

//         class Grad final {
//         public:
//             Grad()
//             {
//                 f[0] = [](const Point &p, Vector &g) {
//                     g.x = -3.0 + (4.0 * p.x) + (4.0 * p.y);
//                     g.y = -3.0 + (4.0 * p.y) + (4.0 * p.x);
//                 };

//                 f[1] = [](const Point &p, Vector &g) {
//                     g.x = (4.0 * p.x) - 1.0;
//                     g.y = 0.0;
//                 };

//                 f[2] = [](const Point &p, Vector &g) {
//                     g.x = 0.0;
//                     g.y = (4.0 * p.y) - 1.0;
//                 };

//                 f[3] = [](const Point &p, Vector &g) {
//                     g.x = 4.0 - 8.0 * p.x - 4.0 * p.y;
//                     g.y = -4.0 * p.x;
//                 };

//                 f[4] = [](const Point &p, Vector &g) {
//                     g.x = (4.0 * p.y);
//                     g.y = (4.0 * p.x);
//                 };

//                 f[5] = [](const Point &p, Vector &g) {
//                     g.x = -4.0 * p.y;
//                     g.y = 4.0 - 8.0 * p.y - 4.0 * p.x;
//                 };
//             }

//             std::array<std::function<void (const Point &, Vector &)>, 6> f;
//         };

//         class Hessian {
//         public:

//             Hessian()
//             {   
//                 // g.x = -3.0 + (4.0 * p.x) + (4.0 * p.y);
//                 // g.y = -3.0 + (4.0 * p.y) + (4.0 * p.x);
//                 f[0] = [](const Point &, std::array<T, Dim*Dim> &H) {
//                     H[0] = 4.0;
//                     H[1] = 4.0;
//                     H[2] = 4.0;
//                     H[3] = 4.0;
//                 };

//                 // g.x = (4.0 * p.x) - 1.0;
//                 // g.y = 0.0;
//                 f[1] = [](const Point &, std::array<T, Dim*Dim> &H) {
//                     H[0] = 4.0;
//                     H[1] = 0.0;
//                     H[2] = 0.0;
//                     H[3] = 0.0;
//                 };

//                 // g.x = 0.0;
//                 // g.y = (4.0 * p.y) - 1.0;
//                 f[2] = [](const Point &, std::array<T, Dim*Dim> &H) {
//                     H[0] = 0.0;
//                     H[1] = 0.0;
//                     H[2] = 0.0;
//                     H[3] = 4.0;
//                 };

//                 // g.x = 4.0 - 8.0 * p.x - 4.0 * p.y;
//                 // g.y = -4.0 * p.x;
//                 f[3] = [](const Point &, std::array<T, Dim*Dim> &H) {
//                     H[0] = 8.0;
//                     H[1] = -4.0;
//                     H[2] = -4.0;
//                     H[3] = 0.0;
//                 };

//                 // g.x = (4.0 * p.y);
//                 // g.y = (4.0 * p.x);
//                 f[4] = [](const Point &, std::array<T, Dim*Dim> &H) {
//                     H[0] = 0.0;
//                     H[1] = 4.0;
//                     H[2] = 4.0;
//                     H[3] = 0.0;
//                 };

//                 // g.x = -4.0 * p.y;
//                 // g.y = 4.0 - 8.0 * p.y - 4.0 * p.x;
//                 f[5] = [](const Point &, std::array<T, Dim*Dim> &H) {
//                     H[0] = 0.0;
//                     H[1] = -4.0;
//                     H[2] = -4.0;
//                     H[3] = 8.0;
//                 };
//             }   


//             // H = [ f_xx, f_xy; f_yx, f_yy ];
//             std::array<std::function<void (const Point &, std::array<T, Dim*Dim> &)>, 6> f;
//         };

//     public:

//         T fun(const Integer i, const Point &p) const override
//         {
//             return fun_.f[i](p);
//         }

//         void grad(const Integer i, const Point &p, Vector &g) const override
//         {
//             grad_.f[i](p, g);
//         }

//         void hessian(const Integer i, const Point &p, std::array<T, Dim*Dim> &H)
//         {
//             hessian_.f[i](p, H);
//         }

//         void jacobian(const Point &p, std::array<T, PhysicalDim*Dim> &J) const override
//         {
//             if(is_affine()) {
//                 affine_approx_jacobian(J);
//                 return;
//             }

//             IsoParametric<Triangle>::jacobian(*this, p, J);
//         }

//         void affine_approx_jacobian(std::array<T, PhysicalDim*Dim> &J) const override
//         {
//            Affine<Triangle>::jacobian(*this, J);
//         }

//         CoPoint &node(const Integer i) override
//         {
//             return nodes_[i];
//         }

//         const CoPoint &node(const Integer i) const override
//         {
//             return nodes_[i];
//         }

//         void point(const Point &p, CoPoint &q) const override
//         {
//             IsoParametric<Triangle>::point(*this, p, q);
//         }

//         Triangle() : is_affine_(false) {}

//         bool is_affine() const override { return is_affine_; }
//         inline bool is_simplex() const override { return true; }
//         inline int order() const override { return Order; }

//         inline void set_affine(const bool val)
//         {
//             is_affine_ = val;
//         }

//         inline int n_nodes() const override { return NNodes; }

//         inline void make_reference()
//         {
//             Reference<Triangle>::points(nodes_);
//             set_affine(true);
//         }

//         inline ElemType type() const override
//         {
//             return TRI6;
//         }

//         inline T approx_measure() const override
//         {
//             if(is_affine()) {
//                 return 0.5 * trapezoid_area(nodes_[0], nodes_[1], nodes_[2]);
//             } else {
//                 return 0.5 * (
//                     trapezoid_area(nodes_[0], nodes_[3], nodes_[5]) +
//                     trapezoid_area(nodes_[3], nodes_[1], nodes_[4]) +
//                     trapezoid_area(nodes_[5], nodes_[4], nodes_[2]) +
//                     trapezoid_area(nodes_[3], nodes_[4], nodes_[5]) 
//                 );
//             }
//         }

//         inline T measure() const override
//         {
//             if(is_affine()) {
//                 return 0.5 * trapezoid_area(nodes_[0], nodes_[1], nodes_[2]);
//             } else {
//                 Quadrature<T, Dim> q;
//                 Gauss::get(2, q);
//                 return IsoParametric<Triangle>::measure(*this, q);
//             }
//         }

//         inline T reference_measure() const override { return 0.5; }

//     private:
//         bool is_affine_;
//         std::array<CoPoint, 6> nodes_;
        
//         const Fun  fun_;
//         const Grad grad_;
//         const Hessian hessian_;
//     };

//     template<typename T, int PhysicalDim>
//     class Reference<Triangle<T, 2, PhysicalDim>> {
//     public:
//         // using Point = typename Triangle<T, 2, PhysicalDim>::Point;
//         static const int NNodes = 6;
        
//         template<class Point, std::size_t Size>
//         static void points(std::array<Point, Size> &nodes)
//         {
//             static_assert(Size >= 6, "size must be at least 6");
          
//              Reference<Triangle<T, 1, PhysicalDim>>::points(nodes);

//              //p3
//              nodes[3].x = 0.5;
//              nodes[3].y = 0.0;

//              //p4
//              nodes[4].x = 0.5;
//              nodes[4].y = 0.5;

//              //p5
//              nodes[5].x = 0.0;
//              nodes[5].y = 0.5;
//         }

//         inline static T measure() { return 0.5; }
//     };

//     template<typename T, int PhysicalDim>
//     class MakePolygon<Triangle<T, 0, PhysicalDim>> {
//     public:
//         static void apply(const Triangle<T, 0, PhysicalDim> &elem, Polygon<T, PhysicalDim> &poly) {
//             poly.resize(3);

//             for(int i = 0; i < 3; ++i) {
//                 poly[i] = elem.points()[i];
//             }   
//         }

//         template<class Any>
//         static void apply(const Elem<T, 2, PhysicalDim> &e, Any &poly)
//         {
//             auto e_ptr = dynamic_cast<const Triangle<T, 0, PhysicalDim> *>(&e); assert(e_ptr);
//             if(e_ptr) {
//                 apply(*e_ptr, poly);
//             }
//         }
//     };

//     template<typename T, int PhysicalDim>
//     class MakePolygon<Triangle<T, 1, PhysicalDim>> {
//     public:
//         static void apply(const Triangle<T, 1, PhysicalDim> &elem, Polygon<T, PhysicalDim> &poly) {
//             poly.resize(3);

//             for(int i = 0; i < 3; ++i) {
//                 poly[i] = elem.node(i);
//             }   
//         }

//         template<class Any>
//         static void apply(const Elem<T, 2, PhysicalDim> &e, Any &poly)
//         {
//             auto e_ptr = dynamic_cast<const Triangle<T, 1, PhysicalDim> *>(&e); assert(e_ptr);
//             if(e_ptr) {
//                 apply(*e_ptr, poly);
//             }
//         }
//     };

//     template<typename T, int PhysicalDim>
//     class MakePolygon<Triangle<T, 2, PhysicalDim>> {
//     public:
//         static void apply(const Triangle<T, 2, PhysicalDim> &elem, Polygon<T, PhysicalDim> &poly) {

//             if(elem.is_affine()) {
//                 poly.resize(3);

//                 for(int i = 0; i < 3; ++i) {
//                     poly[i] = elem.node(i);
//                 }  
//             } else {
//                 poly.resize(6);

//                 for(int i = 0; i < 3; ++i) {
//                     const auto i2 = i * 2;
//                     poly[i2]   = elem.node(i);
//                     poly[i2+1] = elem.node(3 + i);
//                 }  
//             }
//         }

//         template<class Any>
//         static void apply(const Elem<T, 2, PhysicalDim> &e, Any &poly)
//         {
//             auto e_ptr = dynamic_cast<const Triangle<T, 2, PhysicalDim> *>(&e); assert(e_ptr);
//             if(e_ptr) {
//                 apply(*e_ptr, poly);
//             }
//         }
//     };

//     template<typename T, int PhysicalDim>
//     class InvertOrientation< Triangle<T, 2, PhysicalDim> > {
//     public:
        
//         inline static void apply(Triangle<T, 2, PhysicalDim> &elem)
//         {
//             std::swap(elem.node(1), elem.node(2));
//             std::swap(elem.node(3), elem.node(5));
//         }
//     };


//     template<typename T, int Order, int PhysicalDim>
//     class GaussQRule< Triangle<T, Order, PhysicalDim> > {
//     public:
//         static bool get(const Integer order, Quadrature2<T> &q) {
//             return Gauss::get(order, q);
//         }
//     };

// }

// #endif //MOONOLITH_ELEM_TRIANGLE_HPP
