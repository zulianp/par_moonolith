#ifndef MOONOLITH_DUPLICATE_INTERSECTION_AVOIDANCE_HPP
#define MOONOLITH_DUPLICATE_INTERSECTION_AVOIDANCE_HPP

#include "moonolith_dual_graph.hpp"
#include "moonolith_empirical_tol.hpp"
#include "moonolith_expanding_array.hpp"
#include "moonolith_intersect_polygon_with_h_polyhedron.hpp"
#include "moonolith_intersect_polyline_with_convex_polygon.hpp"
#include "moonolith_line.hpp"
#include "moonolith_polygon.hpp"
#include "par_moonolith_config.hpp"

namespace moonolith {

    template <typename T, Integer Dim, class VolumePoly, class SurfacePoly, class Algorithm>
    class DuplicateIntersectionAvoidanceAux {
    public:
        bool apply(const T tol = GeometricTol<T>::value()) {
            auto n = master_poly.size();

            isect.resize(n);
            intersected.resize(n);
            side_intersected.resize(n);
            measures.resize(n);

            Integer n_intersected = 0;

            bool has_side_isect = false;

            for (std::size_t i = 0; i < n; ++i) {
                const bool yes = algo.apply(slave_poly, master_poly[i], isect[i], tol);
                intersected[i] = yes;
                n_intersected += yes;

                if (yes) {
                    measures[i] = moonolith::measure(isect[i]);
                    side_intersected[i] = algo.side_num();
                    has_side_isect = has_side_isect || algo.has_side_intersection();
                } else {
                    side_intersected[i] = -1;
                }
            }

            if (n_intersected == 0) {
                return false;
            }

            if (n_intersected == 1) {
                return true;
            }

            // if(!has_side_isect) {
            // 	return true;
            // }

            // discover redundant intersections
            dual_graph.clear();

            for (std::size_t i = 0; i < n; ++i) {
                if (!intersected[i]) continue;

                for (std::size_t j = 0; j < n; ++j) {
                    if (!intersected[j] || i == j) continue;

                    // auto side_i = side_intersected[i];
                    // auto side_j = side_intersected[j];

                    // if(side_i >= 0 && side_j >= 0) {

                    // 	//remove when dealing with non-conforming meshes
                    // 	assert(
                    // 		approxeq(
                    // 			measures[i],
                    // 			measures[j],
                    // 			1e-6)
                    // 	);

                    // 	//there might be a degenerate case where the intersection
                    // 	//might not be redundant but will be detected as such
                    // 	//The most robust test is to check if one intersection contains
                    // 	//the other (still there are false negatives for side_intersected)

                    // 	//check that side is actually the same geometrically
                    // 	if(dual_graph.sides_are_coplanar(master_poly[i], side_i, master_poly[j], side_j, 1e-8)) {
                    // 		//remove the shortest of the two intersections
                    // 		if(measures[i] < measures[j]) {
                    // 			intersected[i] = false;

                    // 			std::cout << "!!removed(" << j << ") " << i << std::endl;
                    // 		} else {
                    // 			intersected[j] = false;

                    // 			std::cout << "!!removed(" << i << ") " << j << std::endl;
                    // 		}

                    // 		--n_intersected;

                    // 		assert(n_intersected > 0);
                    // 	}

                    // } else

                    if (approxeq(measures[i], measures[j], 10 * tol)) {
                        if (approxeq(isect[i], isect[j], 100 * tol)) {
                            intersected[j] = false;
                            --n_intersected;

                            // std::cout << "removed(" << i << ") " << j << std::endl;
                            // return true;
                        }
                    }
                }
            }

            return true;
        }

        bool is_slave_covered() const {
            auto slave_measure = measure(slave_poly);
            T master_overall_measure = 0.0;

            const std::size_t n = measures.size();
            for (std::size_t i = 0; i < n; ++i) {
                if (intersected[i]) {
                    master_overall_measure += measures[i];
                }
            }

            assert(approxeq(slave_measure, master_overall_measure, static_cast<T>(1e-8)));
            return approxeq(slave_measure, master_overall_measure, static_cast<T>(1e-8));
        }

        // INPUT
        // set these from outside then call apply
        Storage<VolumePoly> master_poly;
        SurfacePoly slave_poly;

        // OUTPUT
        // results will be stored here
        Storage<SurfacePoly> isect;
        Storage<bool> intersected;
        Storage<T> measures;

    private:
        Storage<Integer> side_intersected;
        Algorithm algo;
        DualGraph<T, Dim> dual_graph;
    };

    template <class MasterPoly, class SlavePoly>
    class DuplicateIntersectionAvoidance {};

    template <typename T>
    class DuplicateIntersectionAvoidance<Polygon<T, 2>, Line<T, 2>>
        : public DuplicateIntersectionAvoidanceAux<T,
                                                   2,
                                                   Polygon<T, 2>,
                                                   Line<T, 2>,
                                                   IntersectPolylineWithConvexPolygon2<T>> {};

    template <typename T>
    class DuplicateIntersectionAvoidance<Polyhedron<T>, Polygon<T, 3>>
        : public DuplicateIntersectionAvoidanceAux<T,
                                                   3,
                                                   Polyhedron<T>,
                                                   Polygon<T, 3>,
                                                   IntersectPolygonWithHPolyhedron<T>> {};

    template <typename T, Integer Dim>
    class DuplicateIntersectionAvoidanceFromDim {
    private:
        DuplicateIntersectionAvoidanceFromDim() {
            // static_assert(false, "should instantiate the specialization!");
        }
    };

    template <typename T>
    class DuplicateIntersectionAvoidanceFromDim<T, 2>
        : public DuplicateIntersectionAvoidanceAux<T,
                                                   2,
                                                   Polygon<T, 2>,
                                                   Line<T, 2>,
                                                   IntersectPolylineWithConvexPolygon2<T>> {};

    template <typename T>
    class DuplicateIntersectionAvoidanceFromDim<T, 3>
        : public DuplicateIntersectionAvoidanceAux<T,
                                                   3,
                                                   Polyhedron<T>,
                                                   Polygon<T, 3>,
                                                   IntersectPolygonWithHPolyhedron<T>> {};
}  // namespace moonolith

#endif  // MOONOLITH_DUPLICATE_INTERSECTION_AVOIDANCE_HPP