#include "moonolith_stream_utils.hpp"


namespace moonolith {

	std::ostream & Logger::stream()
	{
		if(use_file_stream_) {
			if(!stream_open_) {
				file_stream_.open(file_name_);
				stream_open_ = true;
				
				if(!file_stream_.good()) {
					std::cerr << "[Error] Unable to open file stream using standard output." << std::endl;
					use_file_stream_ = false;
				}
			}
			
			return file_stream_;
		} else {
			return std::cout;
		}
	}
}