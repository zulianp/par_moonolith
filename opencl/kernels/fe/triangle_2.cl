#ifndef TRIANGLE_2_CL
#define TRIANGLE_2_CL 

#include "kernel_base.cl"
#include "fe_object.cl"
#include "triangle.cl"
#include "transform.cl"

void triangle_2_make_fe_object(const SizeType n_points, const Scalar *points, const SizeType n_quad_points, const Scalar * quad_points, FEObject *object)
{
	object->n_dims = 2;
	object->n_quad_points = n_quad_points;
	object->reference_volume = 0.5;
	object->n_shape_functions = 6;

	static_assert(MAX_SHAPE_FUNCS >= 6, "check kernel_base for MAX_SHAPE_FUNCS");
	
	triangle_quadratic_fun_0(n_quad_points, quad_points, object->fun[0]);
	triangle_quadratic_fun_1(n_quad_points, quad_points, object->fun[1]);
	triangle_quadratic_fun_2(n_quad_points, quad_points, object->fun[2]);
	triangle_quadratic_fun_3(n_quad_points, quad_points, object->fun[3]);
	triangle_quadratic_fun_4(n_quad_points, quad_points, object->fun[4]);
	triangle_quadratic_fun_5(n_quad_points, quad_points, object->fun[5]);

	triangle_quadratic_grad_0(n_quad_points, quad_points, object->grad[0]);
	triangle_quadratic_grad_1(n_quad_points, quad_points, object->grad[1]);
	triangle_quadratic_grad_2(n_quad_points, quad_points, object->grad[2]);
	triangle_quadratic_grad_3(n_quad_points, quad_points, object->grad[3]);
	triangle_quadratic_grad_4(n_quad_points, quad_points, object->grad[4]);
	triangle_quadratic_grad_5(n_quad_points, quad_points, object->grad[5]);

	if(n_points == 3) {
		triangle_transform(points, n_quad_points, quad_points, object->point);
		triangle_jacobian(points, n_quad_points, quad_points, object->jacobian);
	} else {
		KERNEL_ASSERT(n_points == 6, "triangle_2_make_fe_object: 6 points needed for quadratic triangle");
		iso_parametric_transform_2(object, points, object->point);
		iso_parametric_jacobian_2(object, points, object->jacobian);
	}
}

//macros for simpler pattern matching
#define FUN_2(x) (2.0 * x * x)
#define FUN_3(x) (3.0 * x)
#define FUN_4(x) (4.0 * x)

#define D_2(x) (4.0 * x)
#define D_3(x) (3.0)
#define D_4(x) (4.0)

#define B_FUN_4(x, y) (4.0 * x * y)
#define D_B4dx(x, y)(4.0 * y)
#define D_B4dy(x, y)(4.0 * x)

#define FUN_4XX(x) B_FUN_4(x, x)
#define D_4XX(x) (8.0*x)


void triangle_quadratic_fun_0(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		const SizeType q2 = q*2;
		const Scalar x = quad_points[q2];
		const Scalar y = quad_points[q2+1];
		funs[q] = 1.0 - FUN_3(x) - FUN_3(y) + FUN_2(x) + FUN_2(y) + B_FUN_4(x, y);
	}
}

void triangle_quadratic_grad_0(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads)
{
	const SizeType n_quad_points_x_d = n_quad_points*2;
	for(SizeType q = 0; q < n_quad_points_x_d; q+=2) {	
		const Scalar x = quad_points[q];
		const Scalar y = quad_points[q+1];

		grads[q]   = -D_3(x) + D_2(x) + D_B4dx(x, y);
		grads[q+1] = -D_3(y) + D_2(y) + D_B4dy(x, y);
	}
}

void triangle_quadratic_fun_1(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		const SizeType q2 = q*2;
		const Scalar x = quad_points[q2];
		funs[q] = FUN_2(x) - x;
	}
}

void triangle_quadratic_grad_1(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads)
{
	const SizeType n_quad_points_x_d = n_quad_points*2;
	for(SizeType q = 0; q < n_quad_points_x_d; q+=2) {	
		const Scalar x = quad_points[q];
		grads[q]   = D_2(x) - 1.0;
		grads[q+1] = 0.0;
	}
}

void triangle_quadratic_fun_2(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		const SizeType q2 = q*2;
		const Scalar y = quad_points[q2+1];// 

		funs[q] = FUN_2(y) - y;
	}
}

void triangle_quadratic_grad_2(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads)
{
	const SizeType n_quad_points_x_d = n_quad_points*2;
	for(SizeType q = 0; q < n_quad_points_x_d; q+=2) {	
		const Scalar y = quad_points[q+1];

		grads[q]   = 0.0;
		grads[q+1] = D_2(y) - 1.0;
	}
}

void triangle_quadratic_fun_3(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		const SizeType q2 = q*2;
		const Scalar x = quad_points[q2];
		const Scalar y = quad_points[q2+1];// 

		funs[q] = FUN_4(x) - FUN_4XX(x) - B_FUN_4(x, y);
	}
}

void triangle_quadratic_grad_3(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads)
{
	const SizeType n_quad_points_x_d = n_quad_points*2;
	for(SizeType q = 0; q < n_quad_points_x_d; q+=2) {	
		const Scalar x = quad_points[q];
		const Scalar y = quad_points[q+1];

		grads[q]   = D_4(x) - D_4XX(x) - D_B4dx(x, y);
		grads[q+1] = D_B4dy(x, y);
	}
}

void triangle_quadratic_fun_4(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		const SizeType q2 = q*2;
		const Scalar x = quad_points[q2];
		const Scalar y = quad_points[q2+1];// 
		funs[q] = B_FUN_4(x, y);
	}
}

void triangle_quadratic_grad_4(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads)
{
	const SizeType n_quad_points_x_d = n_quad_points*2;
	for(SizeType q = 0; q < n_quad_points_x_d; q+=2) {	
		const Scalar x = quad_points[q];
		const Scalar y = quad_points[q+1];

		grads[q]   = D_B4dx(x, y);
		grads[q+1] = D_B4dy(x, y);
	}
}

void triangle_quadratic_fun_5(const SizeType n_quad_points, const Scalar * quad_points, Scalar * funs)
{
	for(SizeType q = 0; q < n_quad_points; ++q) {	
		const SizeType q2 = q*2;
		const Scalar x = quad_points[q2];
		const Scalar y = quad_points[q2+1];
		funs[q] = FUN_4(y) - B_FUN_4(x, y) - FUN_4XX(y);
	}
}

void triangle_quadratic_grad_5(const SizeType n_quad_points, const Scalar * quad_points, Scalar * grads)
{
	const SizeType n_quad_points_x_d = n_quad_points*2;
	for(SizeType q = 0; q < n_quad_points_x_d; q+=2) {	
		const Scalar x = quad_points[q];
		const Scalar y = quad_points[q+1];

		grads[q]   = D_B4dx(x, y);
		grads[q+1] = D_4(y) - D_B4dy(x, y) - D_4XX(y);
	}
}

//clean up macros
#undef FUN_2
#undef FUN_3
#undef FUN_4

#undef D_2
#undef D_3
#undef D_4

#undef B_FUN_4
#undef D_B4dx
#undef D_B4dy

#undef FUN_4XX
#undef D_4XX

#endif //TRIANGLE_2_CL



