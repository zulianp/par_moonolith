#ifndef MOONOLITH_RAY_CASTING_TREE_HPP
#define MOONOLITH_RAY_CASTING_TREE_HPP

#include "moonolith_aabb.hpp"
#include "moonolith_collection_manager.hpp"
#include "moonolith_element_adapter.hpp"
#include "moonolith_intersect_ray_with_polygon.hpp"
#include "moonolith_mesh.hpp"
#include "moonolith_n_tree_mutator_factory.hpp"
#include "moonolith_ray_match.hpp"
#include "moonolith_tree.hpp"

namespace moonolith {

    template <typename T, int Dimension_>
    struct RayCastingTraits {
        enum { Dimension = Dimension_ };
        using Mesh = moonolith::Mesh<T, Dimension>;
        using Bound = moonolith::AABB<Dimension, T>;
        using DataType = moonolith::ElementAdapter<Bound, Mesh>;
    };

    template <typename T, int Dimension_>
    class RayCastingTree : public moonolith::Tree<RayCastingTraits<T, Dimension_>> {
    public:
        enum { Dimension = Dimension_ };
        using Point = moonolith::Vector<T, Dimension>;
        using Mesh = moonolith::Mesh<T, Dimension>;
        using Adapter = typename RayCastingTraits<T, Dimension>::DataType;
        using Isect = moonolith::IntersectRayWithPoly<T, Dimension>;
        using Poly = typename Isect::Poly;

        static std::shared_ptr<RayCastingTree> New() {
            std::shared_ptr<RayCastingTree> ret = std::make_shared<RayCastingTree>();
            auto factory = std::make_shared<moonolith::NTreeMutatorFactory<RayCastingTree>>();

            factory->set_refine_params(20, 6);
            ret->set_mutator_factory(factory);
            return ret;
        }

        void init(const Mesh &mesh) {
            for (Integer i = 0; i < mesh.n_elements(); ++i) {
                Adapter a(&mesh, i, i, 0, 0);
                this->insert(a);
            }

            this->refine();
        }

        bool intersect(const Ray<T, Dimension_> &ray, T &t) {
            Point p;
            return intersect(ray, t, p);
        }

        bool intersect(const Ray<T, Dimension_> &ray, T &t, Point &p) {
            RayMatch<RayCastingTree<T, Dimension>> match(ray);
            this->walk(match);

            if (!match.success()) {
                return false;
            }

            t = std::numeric_limits<T>::max();

            bool intersected = false;
            for (auto m_i : match.matches()) {
                auto &d = (*this)[m_i];

                const auto &e = d.elem();
                const auto &m = d.collection();

                int nn = e.nodes.size();
                poly.resize(nn);
                for (int i = 0; i < nn; ++i) {
                    poly[i] = m.point(e.nodes[i]);
                }

                T t_i;
                if (isect.apply(ray, poly, t_i, p_i)) {
                    intersected = true;

                    if (std::abs(t_i) < std::abs(t)) {
                        t = t_i;
                        p = p_i;
                    }
                }
            }

            return intersected;
        }

        RayCastingTree() {}

        inline void set_tol(const Real tol) { isect.set_tol(tol); }

    private:
        Poly poly;
        Isect isect;
        Point p_i;
    };

}  // namespace moonolith

#endif  // MOONOLITH_RAY_CASTING_TREE_HPP
