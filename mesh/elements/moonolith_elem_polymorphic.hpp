#ifndef MOONOLITH_ELEM_POLYMORPHIC_HPP
#define MOONOLITH_ELEM_POLYMORPHIC_HPP

#include "moonolith_elem.hpp"
#include "moonolith_elem_segment.hpp"
#include "moonolith_elem_triangle.hpp"
#include "moonolith_elem_quad.hpp"
#include "moonolith_elem_tetrahedron.hpp"
#include "moonolith_elem_hexahedron.hpp"

#include "moonolith_elem_edge.hpp"
#include "moonolith_elem_edge1.hpp"
#include "moonolith_elem_edge2.hpp"
#include "moonolith_elem_edge3.hpp"

#include "moonolith_elem_tri.hpp"
#include "moonolith_elem_tri1.hpp"
#include "moonolith_elem_tri3.hpp"
#include "moonolith_elem_tri6.hpp"

#include "moonolith_elem_quad.hpp"
#include "moonolith_elem_quad1.hpp"
#include "moonolith_elem_quad4.hpp"
#include "moonolith_elem_quad8.hpp"
#include "moonolith_elem_quad9.hpp"

#include "moonolith_elem_tet.hpp"
#include "moonolith_elem_tet1.hpp"
#include "moonolith_elem_tet4.hpp"
#include "moonolith_elem_tet10.hpp"

#include "moonolith_elem_hex.hpp"
#include "moonolith_elem_hex1.hpp"
#include "moonolith_elem_hex8.hpp"
#include "moonolith_elem_hex27.hpp"

namespace moonolith {

}

#endif //MOONOLITH_ELEM_POLYMORPHIC_HPP
