#ifndef MOONOLITH_N_TREE_REFINE_HPP
#define MOONOLITH_N_TREE_REFINE_HPP

#include "moonolith_n_tree_bound_partition.hpp"
#include "moonolith_refine.hpp"

namespace moonolith {

    template <class Tree>
    class NTreeRefine : public Refine<Tree> {
    public:
        enum { Dimension = Tree::Traits::Dimension };

        typedef typename Tree::Traits Traits;
        typedef typename Traits::DataType DataType;
        typedef moonolith::Node<Traits> Node;
        typedef moonolith::Branch<Traits> Branch;
        typedef moonolith::Leaf<Traits> Leaf;
        typedef moonolith::Root<Traits> Root;
        typedef moonolith::TreeMemory<Traits> TreeMemory;
        typedef std::make_unsigned<Integer>::type UnsignedInteger;

        static const Integer K = StaticPow<2, Dimension>::value;
        static const Integer ID_SHIFT = StaticUpperBound<K, 10>::value;

        inline static Integer get_id_shift() { return ID_SHIFT; }

        /**
         * @brief use memory.new_leaf_for(branch) to allocate the new leaf and call @a make_leaf yourself on the newly
         * created leaf.
         */
        bool make_branch(Branch &branch, TreeMemory &memory) const override {
            typedef NTreeBoundPartition<typename Traits::Bound, Dimension> BoundPartitioner;

            assert(!branch.bound().empty());
            if (branch.bound().empty()) {
                return false;
            }

            branch.set_number_of_children(K);
            for (Integer i = 0; i < K; i++) {
                std::shared_ptr<Leaf> leaf = memory.new_leaf_for(branch);

                leaf->set_bound(BoundPartitioner::new_sub_cell(branch.bound(), i));
                branch.set_child_at(i, *leaf);

                auto parent = memory.node(leaf->parent());
                leaf->set_global_handle(encode(parent->global_handle(), i));
            }

            return true;
        }

        inline Integer encode(const Integer parentCode, const Integer childNumber) const {
            return MortonEncoder<Integer, Dimension>::encode_child(parentCode, childNumber);
        }

        /**
         * @brief yourself on the newly created leaf in @a make_branch
         * this method requires you to set the globalHandle which has to be uniquely identifiable in all procs. If the
         * node has aliases all aliases have to have the same global id in the handle
         */
        bool make_leaf(Leaf &, TreeMemory &) const override {
            // std::shared_ptr<Branch> parent = std::static_pointer_cast<Branch>( memory.node( leaf.parent() ) );
            // leaf.set_global_handle( parent->global_handle().id() *
            // 	get_id_shift() +
            // 	(leaf.getPositionInParent() + 1)
            // 	);
            assert(false && "Cannot call this method, ... sorry!");
            return true;
        }
    };
}  // namespace moonolith

#endif  // MOONOLITH_N_TREE_REFINE_HPP
