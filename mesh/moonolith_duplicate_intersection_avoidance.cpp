#include "moonolith_duplicate_intersection_avoidance.hpp"

namespace moonolith {
    template class DuplicateIntersectionAvoidance<Polygon<Real, 2>, Line<Real, 2>>;
    template class DuplicateIntersectionAvoidance<Polyhedron<Real>, Polygon<Real, 3>>;
}  // namespace moonolith
