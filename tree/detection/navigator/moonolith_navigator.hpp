#ifndef MOONOLITH_NAVIGATOR_HPP
#define MOONOLITH_NAVIGATOR_HPP

#include "moonolith_node_navigator.hpp"
#include "moonolith_visitor.hpp"

namespace moonolith {

	template <class Tree>
	class Navigator : public NodeNavigator<typename Tree::Traits> {
	public:
		typedef typename Tree::Traits Traits;
		typedef moonolith::Visitor<Traits> Visitor;

		using NodeNavigator<typename Tree::Traits>::walk;

		virtual ~Navigator() {}
		virtual bool walk(Tree &tree, Visitor &visitor) = 0;
	};
}

#endif //MOONOLITH_NAVIGATOR_HPP
