#ifndef MOONOLITH_NODE_HPP
#define MOONOLITH_NODE_HPP

#include "moonolith_data_handle.hpp"
#include "moonolith_forward_declarations.hpp"
#include "moonolith_navigator_option.hpp"
#include "moonolith_node_handle.hpp"

#include <assert.h>
#include <algorithm>
#include <vector>

namespace moonolith {

    template <class _Traits>
    class Node : public Describable {
    private:
        typedef std::vector<DataHandle> DataContainer;

    public:
        typedef _Traits Traits;
        typedef typename Traits::Bound Bound;
        typedef typename DataContainer::const_iterator ConstDataIter;

        inline ConstDataIter data_begin() { return _data.begin(); }

        inline ConstDataIter data_end() { return _data.end(); }

        inline void set_handle(const NodeHandle &handle) { _handle = handle; }

        const NodeHandle &handle() const { return _handle; }

        inline const Bound &bound() const { return _bound; }

        inline Bound &bound() { return _bound; }

        inline void set_bound(const Bound &bound) { _bound = bound; }

        inline Integer level() const { return _level; }

        inline void set_level(const Integer level) { _level = level; }

        inline NodeHandle parent() const { return _parent; }

        inline void set_parent(const NodeHandle &parent) { _parent = parent; }

        inline Integer position_in_parent() const { return _positionInParent; }

        inline void set_position_in_parent(const Integer positionInParent) { _positionInParent = positionInParent; }

        virtual NavigatorOption accept(Visitor<Traits> &visitor, TreeMemory<Traits> &memory) = 0;
        // {
        // 	return visitor.visit(*this, memory);
        // }

        virtual bool empty() const = 0;
        virtual bool is_root() const { return false; }
        virtual bool is_branch() const { return false; }
        virtual bool is_leaf() const { return false; }
        virtual bool is_visitable() const = 0;

        inline NodeHandle global_handle() const { return _globalHandle; }

        inline void set_global_handle(const NodeHandle &handle) { _globalHandle = handle; }

        virtual std::string get_class() const { return "Node"; }

        virtual void clear() {
            _data.clear();
            _handle = NodeHandle();
            _parent = NodeHandle();
            _bound.clear();
            _level = -1;
            _positionInParent = -1;
            _tags.clear();
        }

        virtual void describe(std::ostream &os) const {
            os << get_class() << "\n";
            os << "Handle: " << _handle << "\n";
            os << "GlobalHandle: " << _globalHandle << "\n";
            os << "Parent: " << _parent << "\n";
            os << "Level: " << _level << "\n";
            os << "|Data|: " << _data.size() << "\n";

            if (!_tags.empty()) {
                os << "Tags:";
                for (auto t : _tags) {
                    os << " " << t;
                }

                os << "\n";
            }
        }

        virtual void copy_meta_state(const Node &other) {
            _handle = other._handle;
            _parent = other._parent;
            _level = other._level;
            _bound = other._bound;
            _globalHandle = other._globalHandle;
            _tags = other._tags;
        }

        virtual void insert(const DataHandle &handle) {
            assert(!_immutable);
            _data.push_back(handle);
        }

        inline Integer size() const { return static_cast<Integer>(_data.size()); }

        inline bool has_data() const { return !_data.empty(); }

        MOONOLITH_DEBUG(void make_immutable() { _immutable = true; });

        Node() { MOONOLITH_DEBUG(_immutable = false); }

        std::vector<Integer> &tags() { return _tags; }

        const std::vector<Integer> &tags() const { return _tags; }

        inline bool has_tag(const Integer tag) const {
            return std::find(tags().begin(), tags().end(), tag) != tags().end();
        }

    private:
        DataContainer _data;

        NodeHandle _handle;
        NodeHandle _parent;
        NodeHandle _globalHandle;

        Bound _bound;

        Integer _level;
        Integer _positionInParent;

        MOONOLITH_DEBUG(bool _immutable;)

        std::vector<Integer> _tags;
    };
}  // namespace moonolith

#endif  // MOONOLITH_NODE_HPP
