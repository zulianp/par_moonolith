#ifndef MOONOLITH_PAR_L2_TRANSFER_IMPL_HPP
#define MOONOLITH_PAR_L2_TRANSFER_IMPL_HPP

#include "moonolith_par_l2_transfer.hpp"
#include "par_moonolith_config.hpp"

#include "moonolith_duplicate_intersection_avoidance.hpp"
#include "moonolith_many_masters_one_slave_algorithm.hpp"
#include "moonolith_one_master_one_slave_algorithm.hpp"
#include "moonolith_single_collection_one_master_one_slave_algorithm.hpp"
#include "moonolith_sparse_matrix.hpp"

#include "moonolith_collection_manager.hpp"
#include "moonolith_function_space.hpp"
#include "moonolith_gauss_quadrature_rule.hpp"
#include "moonolith_incomplete_intersection_remover.hpp"
#include "moonolith_keast_quadrature_rule.hpp"
#include "moonolith_l2_assembler.hpp"
#include "moonolith_matrix_buffers.hpp"
#include "moonolith_matrix_inserter.hpp"

#include <tuple>
#include <vector>

#include <mpi.h>

namespace moonolith {

    template <class MasterElem, class SlaveElem>
    void LocalL2Assembler<MasterElem, SlaveElem>::init_quadrature() {
        int master_order = order_of_quadrature(*master_elem);
        int slave_order = order_of_quadrature(*slave_elem);

        int order = 0;
        if (slave_elem->is_affine()) {
            order = master_order + slave_order;
        } else {
            // assumes tensor product
            order = master_order + slave_order + (slave_order - 1) * 2;
        }

        if (order == current_quadrature_order_) return;

        Gauss::get(order, q_rule);

        assert(!q_rule.empty());
        algo.set_quadrature(q_rule);
    }

    template <class MasterElem, class SlaveElem>
    void LocalL2Assembler<MasterElem, SlaveElem>::clear() {
        algo.clear();
    }

    template <class MasterElem, class SlaveElem>
    LocalL2Assembler<MasterElem, SlaveElem>::LocalL2Assembler(MatrixBuffers<T> &buffers)
        : buffers(buffers), current_quadrature_order_(-1) {}

    template <class MasterElem, class SlaveElem>
    void LocalL2Assembler<MasterElem, SlaveElem>::dual_lagrange_multiplier(const bool val) {
        algo.dual_lagrange_multiplier(val);
    }

    template <class MasterElem, class SlaveElem>
    bool LocalL2Assembler<MasterElem, SlaveElem>::dual_lagrange_multiplier() const {
        return algo.dual_lagrange_multiplier();
    }

    template <typename T, Integer PhysicalDim, Integer MasterDim, Integer SlaveDim>
    ParL2Transfer<T, PhysicalDim, MasterDim, SlaveDim>::ParL2Transfer(Communicator &comm)
        : comm(comm), buffers(comm), local_assembler(buffers), remove_incomplete_intersections_(false) {}

    template <typename T, Integer PhysicalDim, Integer MasterDim, Integer SlaveDim>
    void ParL2Transfer<T, PhysicalDim, MasterDim, SlaveDim>::post_process(const SpaceT &master, const SpaceT &slave) {
        Integer n_master_dofs = static_cast<Integer>(master.dof_map().n_local_dofs());
        Integer n_slave_dofs = static_cast<Integer>(slave.dof_map().n_local_dofs());

        buffers.B.finalize(n_slave_dofs, n_master_dofs);
        buffers.D.finalize(n_slave_dofs, n_slave_dofs);

        if (dual_lagrange_multiplier()) {
            buffers.Q.finalize(n_slave_dofs, n_slave_dofs);
        }

        if (remove_incomplete_intersections_) {
            auto &remover = buffers.incomplete_remover;
            remover.determine_complete(slave);

            remover.remove_incomplete(buffers.B.get());
            remover.remove_incomplete(buffers.D.get());

            if (dual_lagrange_multiplier()) {
                remover.remove_incomplete(buffers.Q.get());
            }
        }
    }

    template <typename T, Integer PhysicalDim, Integer MasterDim, Integer SlaveDim>
    bool ParL2Transfer<T, PhysicalDim, MasterDim, SlaveDim>::assemble(const SpaceT &master,
                                                                      const SpaceT &slave,
                                                                      const T box_tol) {
        Real elapsed = MPI_Wtime();

        SpaceAlgorithmT algo(comm, moonolith::make_unique<CollectionManager<SpaceT>>(comm, true));

        algo.init_simple(master, slave, box_tol);

        elapsed = MPI_Wtime() - elapsed;

        if (Moonolith::instance().verbose() && !comm.rank()) {
            logger() << "init: " << elapsed << std::endl;
        }

        ////////////////////////////////////////////////////
        /////////////////// pair-wise method ///////////////////////
        elapsed = MPI_Wtime();

        local_assembler.clear();

        algo.compute([&](const ElemAdapter &master, const ElemAdapter &slave) -> bool {
            if (local_assembler(master, slave)) {
                return true;
            }

            return false;
        });

        Real vol = local_assembler.algo.intersection_measure();

        comm.all_reduce(&vol, 1, MPISum());

        Real sum_mat_B = buffers.B.m_matrix.sum();
        Real sum_mat_D = buffers.D.m_matrix.sum();

        post_process(master, slave);

        elapsed = MPI_Wtime() - elapsed;

        if (Moonolith::instance().verbose() && !comm.rank()) {
            logger() << "time ParL2Transfer<T, PhysicalDim, MasterDim, SlaveDim>::assemble: " << elapsed << std::endl;
            logger() << "vol: " << vol << " sum(B): " << sum_mat_B << " sum(D): " << sum_mat_D << std::endl;
        }

        return vol > 0.0;
    }

    template <typename T, Integer PhysicalDim, Integer MasterDim, Integer SlaveDim>
    bool ParL2Transfer<T, PhysicalDim, MasterDim, SlaveDim>::assemble(const SpaceT &master,
                                                                      const SpaceT &slave,
                                                                      const std::vector<std::pair<int, int>> &tags,
                                                                      const T box_tol) {
        Real elapsed = MPI_Wtime();

        SpaceAlgorithmT algo(comm, moonolith::make_unique<CollectionManager<SpaceT>>(comm, false));

        algo.init(master, slave, tags, box_tol);

        elapsed = MPI_Wtime() - elapsed;

        if (Moonolith::instance().verbose() && !comm.rank()) {
            logger() << "init: " << elapsed << std::endl;
        }

        ////////////////////////////////////////////////////
        /////////////////// pair-wise method ///////////////////////
        elapsed = MPI_Wtime();

        local_assembler.clear();

        algo.compute([&](const ElemAdapter &master, const ElemAdapter &slave) -> bool {
            if (local_assembler(master, slave)) {
                return true;
            }

            return false;
        });

        Real vol = local_assembler.algo.intersection_measure();

        comm.all_reduce(&vol, 1, MPISum());

        Real sum_mat_B = buffers.B.m_matrix.sum();
        Real sum_mat_D = buffers.D.m_matrix.sum();

        post_process(master, slave);

        elapsed = MPI_Wtime() - elapsed;

        if (Moonolith::instance().verbose() && !comm.rank()) {
            logger() << "time ParL2Transfer<T, PhysicalDim, MasterDim, SlaveDim>::assemble: " << elapsed << std::endl;
            logger() << "vol: " << vol << " sum(B): " << sum_mat_B << " sum(D): " << sum_mat_D << std::endl;
        }

        return vol > 0.0;
    }

    template <typename T, Integer PhysicalDim, Integer MasterDim, Integer SlaveDim>
    bool ParL2Transfer<T, PhysicalDim, MasterDim, SlaveDim>::assemble(const SpaceT &space,
                                                                      const std::vector<std::pair<int, int>> &tags,
                                                                      const T box_tol) {
        Real elapsed = MPI_Wtime();

        SingleSpaceAlgorithmT algo(comm, moonolith::make_unique<CollectionManager<SpaceT>>(comm));

        algo.init(space, tags, box_tol);

        elapsed = MPI_Wtime() - elapsed;

        if (Moonolith::instance().verbose() && !comm.rank()) {
            logger() << "init: " << elapsed << std::endl;
        }

        ////////////////////////////////////////////////////
        /////////////////// pair-wise method ///////////////////////
        elapsed = MPI_Wtime();

        local_assembler.clear();

        algo.compute([&](const ElemAdapter &master, const ElemAdapter &slave) -> bool {
            if (local_assembler(master, slave)) {
                return true;
            }

            return false;
        });

        Real vol = local_assembler.algo.intersection_measure();

        comm.all_reduce(&vol, 1, MPISum());

        Real sum_mat_B = buffers.B.m_matrix.sum();
        Real sum_mat_D = buffers.D.m_matrix.sum();

        post_process(space, space);

        elapsed = MPI_Wtime() - elapsed;
        if (Moonolith::instance().verbose() && !comm.rank()) {
            logger() << "time ParL2Transfer<T, PhysicalDim, MasterDim, SlaveDim>::assemble: " << elapsed << std::endl;
            logger() << "vol: " << vol << " sum(B): " << sum_mat_B << " sum(D): " << sum_mat_D << std::endl;
        }
        return vol > 0.0;
    }

    template <typename T, Integer PhysicalDim, Integer MasterDim, Integer SlaveDim>
    void ParL2Transfer<T, PhysicalDim, MasterDim, SlaveDim>::dual_lagrange_multiplier(const bool val) {
        local_assembler.dual_lagrange_multiplier(val);
    }

    template <typename T, Integer PhysicalDim, Integer MasterDim, Integer SlaveDim>
    bool ParL2Transfer<T, PhysicalDim, MasterDim, SlaveDim>::dual_lagrange_multiplier() const {
        return local_assembler.dual_lagrange_multiplier();
    }

}  // namespace moonolith

#endif  // MOONOLITH_PAR_L2_TRANSFER_IMPL_HPP
