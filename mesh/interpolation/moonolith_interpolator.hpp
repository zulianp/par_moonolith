#ifndef MOONOLITH_INTERPOLATOR_HPP
#define MOONOLITH_INTERPOLATOR_HPP

// FIXME
#include <ostream>

#include "moonolith_empirical_tol.hpp"
#include "moonolith_h_polytope.hpp"
#include "moonolith_l2_assembler.hpp"

namespace moonolith {

    template <typename T, int Dim, int PhysicalDim>
    class Contains {};

    template <class ContaintsT>
    class AuxContains {
    public:
        template <class Elem>
        inline static bool apply(ContaintsT &c,
                                 const Elem &elem,
                                 Vector<int, Elem::NNodes> &is_contained,
                                 const typename Elem::T &tol) {
            const Integer n_nodes = elem.n_nodes();
            Resize<Vector<int, Elem::NNodes>>::apply(is_contained, n_nodes);

            bool ret = false;
            for (Integer i = 0; i < n_nodes; ++i) {
                is_contained[i] = c.contains(elem.node(i), tol);
                ret = ret || is_contained[i];
            }

            return ret;
        }
    };

    template <typename T>
    class Contains<T, 2, 2> {
    public:
        using Point = moonolith::Vector<T, 2>;

        template <class Elem>
        inline void init(const Elem &elem) {
            make(elem, poly_);
            h_poly_.make(poly_);
        }

        template <class Elem>
        inline bool apply(const Elem &elem,
                          Vector<int, Elem::NNodes> &is_contained,
                          const T tol = GeometricTol<T>::value()) {
            return AuxContains<Contains>::apply(*this, elem, is_contained, tol);
        }

        inline bool contains(const Point &p, const T tol = GeometricTol<T>::value()) const {
            return h_poly_.contains(p, tol);
        }

    private:
        HPolytope<T, 2> h_poly_;
        Polygon<T, 2> poly_;
    };

    template <typename T>
    class Contains<T, 3, 3> {
    public:
        using Point = moonolith::Vector<T, 3>;

        template <class Elem>
        inline void init(const Elem &elem) {
            make(elem, poly_);
            h_poly_.make(poly_);
        }

        template <class Elem>
        inline bool apply(const Elem &elem,
                          Vector<int, Elem::NNodes> &is_contained,
                          const T tol = GeometricTol<T>::value()) {
            return AuxContains<Contains>::apply(*this, elem, is_contained, tol);
        }

        inline bool contains(const Point &p, const T tol = GeometricTol<T>::value()) const {
            return h_poly_.contains(p, tol);
        }

    private:
        HPolytope<T, 3> h_poly_;
        Polyhedron<T> poly_;
    };

    template <class MasterElem, class SlaveElem>
    class Interpolator : public Transfer<typename SlaveElem::T, SlaveElem::PhysicalDim>, public Describable {
    public:
        using T = typename SlaveElem::T;

        static const int MasterDim = MasterElem::Dim;
        static const int SlaveDim = SlaveElem::Dim;
        static const int PhysicalDim = SlaveElem::PhysicalDim;
        static const int NMasterNodes = MasterElem::NNodes;
        static const int NSlaveNodes = SlaveElem::NNodes;

        static_assert(PhysicalDim == MasterElem::PhysicalDim, "elements must have same physical dimension");

        using MasterPoint = moonolith::Vector<T, MasterDim>;
        using CoVector = moonolith::Vector<T, PhysicalDim>;
        using MasterAffineTrafo = moonolith::AffineTransform<T, MasterDim, PhysicalDim>;

        bool assemble(MasterElem &master, SlaveElem &slave);

        Interpolator();
        ~Interpolator();

        void clear();

        inline const Matrix<T, NSlaveNodes, NMasterNodes> &coupling_matrix() const { return coupling_mat_; }

        void describe(std::ostream &os) const override;

    private:
        Contains<T, MasterDim, PhysicalDim> contains_;
        Vector<int, NSlaveNodes> is_contained_;
        MasterPoint p_ref;

        Matrix<T, NSlaveNodes, NMasterNodes> coupling_mat_;
        std::shared_ptr<MasterAffineTrafo> trafo_master;
        std::shared_ptr<Transform<T, MasterDim, PhysicalDim>> iso_trafo_master;
    };

}  // namespace moonolith

#endif  // MOONOLITH_INTERPOLATOR_HPP
