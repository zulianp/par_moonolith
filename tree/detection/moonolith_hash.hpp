#ifndef MOONOLITH_HASH_HPP
#define MOONOLITH_HASH_HPP

#include "moonolith_forward_declarations.hpp"

namespace moonolith {
	
	template<class Traits>
	class Hash {
	public:
		typedef moonolith::Node<Traits> Node;
		typedef moonolith::TreeMemory<Traits> TreeMemory;

		virtual ~Hash() {}
		
		virtual std::string name() const
		{
			return "Hash";
		}

		virtual Integer flattened_hash(const Node &node, const TreeMemory &memory) const = 0;
	};	
}

#endif //MOONOLITH_HASH_HPP
