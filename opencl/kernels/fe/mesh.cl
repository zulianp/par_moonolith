#ifndef MESH_CL
#define MESH_CL 

#include "kernel_base.cl"
#include "assembly/quadrature.cl"

#include "fe_object.cl"
#include "fe_interfaces_2.cl"
#include "fe_interfaces_3.cl"
#include "mesh_interface.cl"

m_constant__ static const int ELEMENT_TYPE_VERTEX = 1;
m_constant__ static const int ELEMENT_TYPE_POLY_VERTEX = 2;
m_constant__ static const int ELEMENT_TYPE_LINE = 3;
m_constant__ static const int ELEMENT_TYPE_POLY_LINE = 4;
m_constant__ static const int ELEMENT_TYPE_TRIANGLE = 5;
m_constant__ static const int ELEMENT_TYPE_TRIANGLE_ORDER_2 = 105;
m_constant__ static const int ELEMENT_TYPE_TRIANLE_STRIP = 6;
m_constant__ static const int ELEMENT_TYPE_POLYGON = 7;
m_constant__ static const int ELEMENT_TYPE_PIXEL = 8;
m_constant__ static const int ELEMENT_TYPE_QUAD = 9;
m_constant__ static const int ELEMENT_TYPE_TETRA = 10;
m_constant__ static const int ELEMENT_TYPE_VOXEL = 11;
m_constant__ static const int ELEMENT_TYPE_HEXAHEDRON = 12;
m_constant__ static const int ELEMENT_TYPE_WEDGE = 13;
m_constant__ static const int ELEMENT_TYPE_PYRAMID = 14;


SizeType mesh_n_points(const Mesh mesh, const SizeType element)
{
	const SizeType begin = mesh.el_ptr[element];
	const SizeType end   = mesh.el_ptr[element+1]; 
	return end - begin;
}

void mesh_points(const Mesh mesh, const SizeType element, Scalar * points) {
	KERNEL_ASSERT(element < mesh.n_elements, "mesh_points: element is not less than mesh.n_elements");

	const SizeType begin = mesh.el_ptr[element];
	const SizeType end   = mesh.el_ptr[element+1]; 
	
	KERNEL_ASSERT(begin >= 0, "mesh_points: begin is less than 0");
	KERNEL_ASSERT(end < mesh.n_elements * LARGE_INTEGER, "mesh_points: end is not bounded");

	SizeType index = 0;
	for(SizeType i = begin; i < end; ++i) {
		const SizeType offset = mesh.el_index[i] * mesh.n_dims;
		
		for(SizeType d = 0; d < mesh.n_dims; ++d) {
			points[index++] = mesh.points[offset+d];
		}
	}

	// printf(" %d -> %d \n", begin, end );
	// printf("[%d %d %d]\n", mesh.el_index[begin], mesh.el_index[begin+1], mesh.el_index[begin+2]);
	// matrix_print(end-begin, mesh.n_dims, points);
}

bool make_fe_object(Mesh mesh, const SizeType element, const SizeType order, FEObject * fe)
{
	KERNEL_ASSERT(element < mesh.n_elements, "make_fe_object: trying to access out of bound object");
	KERNEL_ASSERT(order >= 0 && order < LARGE_INTEGER, "make_fe_object: order of quadrature not bounded");


	switch(mesh.n_dims) {
		case 2: { return make_fe_object_2(mesh, element, order, fe); }
		case 3: { return make_fe_object_3(mesh, element, order, fe); }
		default: { 
			KERNEL_ASSERT(false, "make_fe_object: not defined for requested dimension");
			return false; 
		}
	}

	return false;
}



bool make_fe_object_2(Mesh mesh, const SizeType element, const SizeType order, FEObject * fe)
{
	Scalar quad_points[MAX_QUAD_POINTS*2];
	Scalar quad_weights[MAX_QUAD_POINTS];

	bool ok = false;
	switch(mesh.el_type[element]) {
		case ELEMENT_TYPE_TRIANGLE: 
		{
			Scalar points[3*2];
			mesh_points(mesh, element, points);

			const SizeType quadrature_order = find_compatible_quad_rule_2(max_order_for_triangle_rules, triangle_rules, order);
			const SizeType n_quad_points = triangle_rules[quadrature_order].size;

			KERNEL_ASSERT(n_quad_points > 0, "quadrature rule is not valid");
			KERNEL_ASSERT(n_quad_points <= MAX_QUAD_POINTS, "quadrature rule is larger than allocated space");

			generic_copy(n_quad_points*2, triangle_rules[quadrature_order].points, quad_points);
			generic_copy(n_quad_points, triangle_rules[quadrature_order].weights, quad_weights);

			triangle_make_fe_object(points, n_quad_points, quad_points, fe);
			
			make_inverses_and_dets_2(fe->n_quad_points, fe->jacobian, fe->inverse_jacobian, fe->jacobian_determinant);
			make_dx_d(quad_weights, fe);

			ok = true;
			break;
		}

		case ELEMENT_TYPE_QUAD:
		{
			Scalar points[4*2];
			mesh_points(mesh, element, points);

			const SizeType quadrature_order = find_compatible_quad_rule_2(max_order_for_quad_rules, quad_rules, order);
			const SizeType n_quad_points = quad_rules[quadrature_order].size;

			KERNEL_ASSERT(n_quad_points > 0, "quadrature rule is not valid");
			KERNEL_ASSERT(n_quad_points <= MAX_QUAD_POINTS, "quadrature rule is larger than allocated space");

			generic_copy(n_quad_points*2, quad_rules[quadrature_order].points,  quad_points);
			generic_copy(n_quad_points,   quad_rules[quadrature_order].weights, quad_weights);

			quad_make_fe_object(points, n_quad_points, quad_points, fe);

			make_inverses_and_dets_2(fe->n_quad_points, fe->jacobian, fe->inverse_jacobian, fe->jacobian_determinant);
			make_dx_d(quad_weights, fe);
			ok = true;
			break;
		}

		case ELEMENT_TYPE_TRIANGLE_ORDER_2:
		{
			Scalar points[6*2];
			mesh_points(mesh, element, points);
			const SizeType n_points = mesh_n_points(mesh, element);
			
			const SizeType quadrature_order = find_compatible_quad_rule_2(max_order_for_triangle_rules, triangle_rules, order);
			const SizeType n_quad_points = triangle_rules[quadrature_order].size;

			KERNEL_ASSERT(n_quad_points > 0, "quadrature rule is not valid");
			KERNEL_ASSERT(n_quad_points <= MAX_QUAD_POINTS, "quadrature rule is larger than allocated space");

			generic_copy(n_quad_points * 2, triangle_rules[quadrature_order].points, quad_points);
			generic_copy(n_quad_points, triangle_rules[quadrature_order].weights, quad_weights);

			triangle_2_make_fe_object(n_points, points, n_quad_points, quad_points, fe);

			make_inverses_and_dets_2(fe->n_quad_points, fe->jacobian, fe->inverse_jacobian, fe->jacobian_determinant);
			make_dx_d(quad_weights, fe);
			ok = true;
			break;
		}

		default:
		{
			break;
		}
	}

	if(ok) {
		for(SizeType i = 0; i < fe->n_shape_functions; ++i) {
			transform_gradients(2, fe->n_quad_points, fe->inverse_jacobian, fe->grad[i]);
		}

		return true;
	} else {
		return false;
	}
}

bool make_fe_object_3(Mesh mesh, const SizeType element, const SizeType order, FEObject * fe)
{
	Scalar quad_points[MAX_QUAD_POINTS*3];
	Scalar quad_weights[MAX_QUAD_POINTS];

	bool ok = false;
	switch(mesh.el_type[element]) {
		case ELEMENT_TYPE_TETRA: 
		{
			Scalar points[4*3];
			mesh_points(mesh, element, points);

			const SizeType n_quad_points = tetrahedron_rules[order].size;

			KERNEL_ASSERT(n_quad_points > 0, "quadrature rule is not valid");
			KERNEL_ASSERT(n_quad_points <= MAX_QUAD_POINTS, "quadrature rule is larger than allocated space");

			generic_copy(n_quad_points*3, tetrahedron_rules[order].points, quad_points);
			generic_copy(n_quad_points,   tetrahedron_rules[order].weights, quad_weights);

			tetrahedron_make_fe_object(points, n_quad_points, quad_points, fe);
			make_inverses_and_dets_3(fe->n_quad_points, fe->jacobian, fe->inverse_jacobian, fe->jacobian_determinant);
			make_dx_d(quad_weights, fe);
			ok = true;
			break;
		}

		case ELEMENT_TYPE_HEXAHEDRON:
		{
			Scalar points[8*3];
			mesh_points(mesh, element, points);

			const SizeType n_quad_points = hex_rules[order].size;

			KERNEL_ASSERT(n_quad_points > 0, "quadrature rule is not valid");
			KERNEL_ASSERT(n_quad_points <= MAX_QUAD_POINTS, "quadrature rule is larger than allocated space");

			generic_copy(n_quad_points*3, hex_rules[order].points, quad_points);
			generic_copy(n_quad_points,   hex_rules[order].weights, quad_weights);

			hex_make_fe_object(points, n_quad_points, quad_points, fe);
			// fe_object_print(fe);
			
			make_inverses_and_dets_3(fe->n_quad_points, fe->jacobian, fe->inverse_jacobian, fe->jacobian_determinant);
			make_dx_d(quad_weights, fe);
			ok = true;
			break;
		}

		default:
		{
			break;
		}
	}

	for(SizeType i = 0; i < fe->n_shape_functions; ++i) {
		transform_gradients(3, fe->n_quad_points, fe->inverse_jacobian, fe->grad[i]);
	}

	return ok;
}


void mesh_print(Mesh *mesh) {
	if(get_global_id(0) != 0) return;

	printf("Mesh:\n");
	printf("n_elements: %d\n", mesh->n_elements);
	printf("n_nodes: %d\n", mesh->n_nodes);
	printf("n_dims: %d\n", mesh->n_dims);
	printf("\n");
	printf("elements: \n[\n"); //]

	for(SizeType i = 0; i < mesh->n_elements; ++i) {
		for(SizeType el = mesh->el_ptr[i]; el < mesh->el_ptr[i+1]; ++el) {
			printf("\t%d\t", mesh->el_index[el]);	
		}
		printf("\n");
	}
	//[
	printf("]\n");

	printf("points: \n[\n"); //]
	for(SizeType i = 0; i < mesh->n_nodes; ++i) {
		for(SizeType d = 0; d < mesh->n_dims; ++d) {
			printf("\t%g\t", mesh->points[i*mesh->n_dims+d]);
		}
		printf("\n");
	}

	//[
	printf("]\n");
}

bool make_fe_object_from_global_quad(const Mesh mesh, const SizeType element, const SizeType n_quad_points, const Scalar *quad_points, const Scalar *quad_weights, FEObject *fe)
{
	switch(mesh.n_dims) {
		case 2: return make_fe_object_from_global_quad_2(mesh, element, n_quad_points, quad_points, quad_weights, fe);
		case 3: return make_fe_object_from_global_quad_3(mesh, element, n_quad_points, quad_points, quad_weights, fe);
		default: { KERNEL_ASSERT(false, "dims not supported"); return false; }
	}
}


bool make_fe_object_from_global_quad_2(const Mesh mesh, const SizeType element, const SizeType n_quad_points, const Scalar *quad_points, const Scalar *quad_weights, FEObject *fe)
{

	Scalar points_on_ref_element[MAX_QUAD_POINTS * MAX_N_DIMS];

	bool ok = false;
	switch(mesh.el_type[element]) {
		case ELEMENT_TYPE_TRIANGLE: 
		{
			Scalar points[3*2];
			mesh_points(mesh, element, points);

			triangle_inverse_transform(points, n_quad_points, quad_points, points_on_ref_element);
			triangle_make_fe_object(points, n_quad_points, points_on_ref_element, fe);
			
			make_inverses_and_dets_2(fe->n_quad_points, fe->jacobian, fe->inverse_jacobian, fe->jacobian_determinant);
			make_dx_d(quad_weights, fe);
			ok = true;
			break;
		}

		case ELEMENT_TYPE_QUAD:
		{
			Scalar points[4*2];
			mesh_points(mesh, element, points);

			quad_inverse_transform(points, n_quad_points, quad_points, points_on_ref_element);			
			quad_make_fe_object(points, n_quad_points, points_on_ref_element, fe);

			make_inverses_and_dets_2(fe->n_quad_points, fe->jacobian, fe->inverse_jacobian, fe->jacobian_determinant);
			make_dx_d(quad_weights, fe);
			ok = true;
			break;
		}

		default:
		{
			KERNEL_ASSERT(false, "no match for type");
			break;
		}
	}

	if(ok) {
		for(SizeType i = 0; i < fe->n_shape_functions; ++i) {
			transform_gradients(2, fe->n_quad_points, fe->inverse_jacobian, fe->grad[i]);
		}

		return true;
	} else {
		return false;
	}
}


bool make_fe_object_from_global_quad_3(const Mesh mesh, const SizeType element, const SizeType n_quad_points, 
	const Scalar *quad_points, const Scalar *quad_weights, FEObject *fe)
{
	
	Scalar points_on_ref_element[MAX_QUAD_POINTS * MAX_N_DIMS];

	bool ok = false;
	switch(mesh.el_type[element]) {
		case ELEMENT_TYPE_TETRA: 
		{
			Scalar points[4*3];
			mesh_points(mesh, element, points);

			tetrahedron_inverse_transform(points, n_quad_points, quad_points, points_on_ref_element);
			tetrahedron_make_fe_object(points, n_quad_points, points_on_ref_element, fe);
			
			make_inverses_and_dets_3(fe->n_quad_points, fe->jacobian, fe->inverse_jacobian, fe->jacobian_determinant);
			make_dx_d(quad_weights, fe);
			ok = true;
			break;
		}
		
		case ELEMENT_TYPE_HEXAHEDRON:
		{
			Scalar points[8*3];
			mesh_points(mesh, element, points);

			hex_inverse_transform(points, n_quad_points, quad_points, points_on_ref_element);
			hex_make_fe_object(points, n_quad_points, points_on_ref_element, fe);
			
			make_inverses_and_dets_3(fe->n_quad_points, fe->jacobian, fe->inverse_jacobian, fe->jacobian_determinant);
			make_dx_d(quad_weights, fe);
			ok = true;
			break;
		}

		default:
		{
			KERNEL_ASSERT(false, "no match for type");
			break;
		}
	}

	if(ok) {
		for(SizeType i = 0; i < fe->n_shape_functions; ++i) {
			transform_gradients(3, fe->n_quad_points, fe->inverse_jacobian, fe->grad[i]);
		}

		return true;
	} else {
		return false;
	}
}

bool inverse_transform_3(const Mesh mesh, const SizeType element, const SizeType n_quad_points, const Scalar *global_quad_points, Scalar *quad_points)
{
	switch(mesh.el_type[element]) {
		case ELEMENT_TYPE_TETRA: 
		{
			Scalar points[4 * 3];
			mesh_points(mesh, element, points);
			tetrahedron_inverse_transform(points, n_quad_points, global_quad_points, quad_points);
			return true;
		}
		
		case ELEMENT_TYPE_HEXAHEDRON:
		{
			Scalar points[8 * 3];
			mesh_points(mesh, element, points);
			hex_inverse_transform(points, n_quad_points, global_quad_points, quad_points);
			return true;
		}

		default:
		{
			KERNEL_ASSERT(false, "no match for type");
			return false;
			break;
		}
	}
}


bool transform_2(const Mesh mesh, const SizeType element, const SizeType n_quad_points, const Scalar *quad_points, Scalar *global_quad_points)
{
	switch(mesh.el_type[element]) {
		case ELEMENT_TYPE_TRIANGLE: 
		{
			Scalar points[3 * 2];
			mesh_points(mesh, element, points);
			triangle_transform(points, n_quad_points, quad_points, global_quad_points);
			return true;
		}
		
		default:
		{
			KERNEL_ASSERT(false, "no match for type");
			return false;
			break;
		}
	}
}


bool inverse_transform_2(const Mesh mesh, const SizeType element, const SizeType n_quad_points, const Scalar *global_quad_points, Scalar *quad_points)
{
	switch(mesh.el_type[element]) {
		case ELEMENT_TYPE_TRIANGLE: 
		{
			Scalar points[3 * 2];
			mesh_points(mesh, element, points);
			triangle_inverse_transform(points, n_quad_points, global_quad_points, quad_points);
			return true;
		}
		
		default:
		{
			KERNEL_ASSERT(false, "no match for type");
			return false;
			break;
		}
	}
}

bool transform_3(const Mesh mesh, const SizeType element, const SizeType n_quad_points, const Scalar *quad_points, Scalar *global_quad_points)
{
	switch(mesh.el_type[element]) {
		case ELEMENT_TYPE_TETRA: 
		{
			Scalar points[4 * 3];
			mesh_points(mesh, element, points);
			tetrahedron_transform(points, n_quad_points, quad_points, global_quad_points);
			return true;
		}
		
		case ELEMENT_TYPE_HEXAHEDRON:
		{
			Scalar points[8 * 3];
			mesh_points(mesh, element, points);
			hex_transform(points, n_quad_points, quad_points, global_quad_points);
			return true;
		}

		default:
		{
			KERNEL_ASSERT(false, "no match for type");
			return false;
			break;
		}
	}
}



bool make_fe_object_from_quad_3(Mesh mesh, const SizeType element, const SizeType n_quad_points, 
	const Scalar *quad_points, const Scalar * quad_weights, 
	FEObject * fe)
{
	bool ok = false;
	switch(mesh.el_type[element]) {
		case ELEMENT_TYPE_TETRA: 
		{
			Scalar points[4*3];
			mesh_points(mesh, element, points);
			tetrahedron_make_fe_object(points, n_quad_points, quad_points, fe);
			make_inverses_and_dets_3(fe->n_quad_points, fe->jacobian, fe->inverse_jacobian, fe->jacobian_determinant);
			make_dx_d(quad_weights, fe);
			ok = true;
			break;
		}

		case ELEMENT_TYPE_HEXAHEDRON:
		{
			Scalar points[8*3];
			mesh_points(mesh, element, points);
			hex_make_fe_object(points, n_quad_points, quad_points, fe);			
			make_inverses_and_dets_3(fe->n_quad_points, fe->jacobian, fe->inverse_jacobian, fe->jacobian_determinant);
			make_dx_d(quad_weights, fe);
			ok = true;
			break;
		}

		default:
		{
			break;
		}
	}

	if(ok) {
		for(SizeType i = 0; i < fe->n_shape_functions; ++i) {
			transform_gradients(3, fe->n_quad_points, fe->inverse_jacobian, fe->grad[i]);
		}
	}

	return ok;
}

bool make_fe_object_from_quad_2(
	Mesh mesh, const SizeType element, const SizeType n_quad_points, 
	const Scalar *quad_points, const Scalar * quad_weights, 
	FEObject * fe)
{
	bool ok = false;
	switch(mesh.el_type[element]) {
		case ELEMENT_TYPE_TRIANGLE: 
		{
			Scalar points[3*2];
			mesh_points(mesh, element, points);
			triangle_make_fe_object(points, n_quad_points, quad_points, fe);
			make_inverses_and_dets_2(fe->n_quad_points, fe->jacobian, fe->inverse_jacobian, fe->jacobian_determinant);
			make_dx_d(quad_weights, fe);
			ok = true;
			break;
		}

		default:
		{
			break;
		}
	}

	if(ok) {
		for(SizeType i = 0; i < fe->n_shape_functions; ++i) {
			transform_gradients(2, fe->n_quad_points, fe->inverse_jacobian, fe->grad[i]);
		}
	}

	return ok;
}

#endif //MESH_CL
