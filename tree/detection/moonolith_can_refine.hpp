#ifndef MOONOLITH_CAN_REFINE_HPP
#define MOONOLITH_CAN_REFINE_HPP

// #include "moonolith_mutator.hpp"
#include "moonolith_forward_declarations.hpp"
#include "moonolith_navigator_option.hpp"

namespace moonolith {

    template <class Tree>
    class CanRefine : public Mutator<typename Tree::Traits> {
    public:
        typedef typename Tree::Traits Traits;
        typedef typename Traits::DataType DataType;
        typedef typename Traits::Bound Bound;

        typedef moonolith::Node<Traits> Node;
        typedef moonolith::Branch<Traits> Branch;
        typedef moonolith::Root<Traits> Root;
        typedef moonolith::Leaf<Traits> Leaf;
        typedef moonolith::TreeMemory<Traits> TreeMemory;

        CanRefine() : success_(false) {}

        ///@brief expands the bound
        ///@returns true if the operation was successfully performed
        virtual bool can_refine(const Leaf &leaf, const TreeMemory &memory) const = 0;

        virtual NavigatorOption visit(Root &, TreeMemory &) {
            success_ = true;
            return STOP;
        }

        inline NavigatorOption visit(Leaf &leaf, TreeMemory &memory) {
            success_ = can_refine(leaf, memory);
            return STOP;
        }

        inline NavigatorOption visit(Branch &, TreeMemory &) {
            success_ = true;
            return STOP;
        }

        virtual ~CanRefine() {}

        inline bool success() const { return success_; }

        inline void clear() { success_ = false; }

    private:
        bool success_;
    };
}  // namespace moonolith

#endif  // MOONOLITH_CAN_REFINE_HPP
