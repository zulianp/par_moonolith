#ifndef MOONOLITH_VERBOSE_OBJECT_HPP
#define MOONOLITH_VERBOSE_OBJECT_HPP

namespace moonolith {
	class VerboseObject {
	public:
		VerboseObject()
			: verbose_(false) 
		{}

		inline bool verbose() const { return verbose_; }
		inline void set_verbose(const bool verbose)
		{
			verbose_ = verbose;
		}

		virtual ~VerboseObject() {}
	
	private:
		bool verbose_;
	};
}


#endif //MOONOLITH_VERBOSE_OBJECT_HPP

